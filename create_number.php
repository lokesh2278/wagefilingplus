<script type="text/javascript">
function GenerateApplicationNo() {
 var dt = new Date();
 var seed = dt.getYear() + dt.getDay() + dt.getMonth() + dt.getHours() + dt.getMinutes() + dt.getSeconds();
 var holdrand = ((seed * 214013 + 2531011) >> 16) & 0x7fff; 
 var holdrand2 = ((holdrand * 214013 + 2531011) >> 16) & 0x7fff; 
 this.getField("ApplicationNo").value = util.printf("%05d%05d", holdrand, holdrand2);
}
</script>