<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h2>Form Data of 1099</h2>
            <ul class="header-dropdown m-r--5">
              <button id="export" class="btn btn-lg btn-primary waves-effect">Export</button>
              <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
          </div>
          <!-- /.box-header -->
          <div class="body table-responsive">
            <div class="row com-row">

                          <form id="form-filter" class="form-horizontal">
                            <div class="col-md-3">
                              <select id="selectcityAmin" name="selectcityAmin" class="filter-field form-control uniform-multiselect">
                                    <option value="all">All </option>
                                    <?php if (isset($c_data) && !empty($c_data)) {

                                  foreach ($c_data as $cid => $daaacc) {
                                      $s = $this->uri->segment(3);
                                      ?><option <?php if ($s == $daaacc->FileSequence) {echo 'selected="selected" ';}?> value="<?php echo isset($daaacc->FileSequence) ? $daaacc->FileSequence : ''; ?>" > <!-- <?php //echo isset($daaacc->c_name) ? $daaacc->c_name : ''; ?> --> <?php echo isset($daaacc->FileName) ? $daaacc->FileName : ''; ?> (<?php echo isset($daaacc->TaxYear) ? $daaacc->TaxYear : ''; ?>)</option>
                                  <?php }
                              }?>
                                  </select>
                            </div>
                            <div class="col-md-3">
                              <select id="selectfiletype" name="selectfiletype" class="filter-field form-control uniform-multiselect">
                                <option value="">--Select Filed Type---</option>
                                <option value="E">E-Filed</option>
                                <option value="PM">E-Filed, Print, Mail</option>
                              </select>
                            </div>
                            <div class="col-md-2">
                              <input type="date" class="form-control" name="startdate" id="startdate" />
                            </div>
                            <div class="col-md-1" align="center"><span>To</span></div>
                            <div class="col-md-2">
                              <input type="date" class="form-control" name="enddate" id="enddate" />
                            </div>
                            <div class="col-sm-1">
                            <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                        </div>
                        <div class="col-md-2">
                              <select id="status" name="status" class="filter-field form-control uniform-multiselect">
                                <option value="0">--Select Filed Type---</option>
                                <option value="exported">Exported</option>
                                <option value="unexported">Un-Exported</option>
                              </select>
                            </div>
                        </form>
                            </div>
            <table id="example1" class="table table-bordered table-striped table-hover delSelTable">
              <thead>
                <tr>
<th>No.</th>
<th>Id</th>
<th>Recipient No</th>
<th>Address</th>
<th>Company Id</th>
<th><?php echo lang('create_date') ?></th>
<th><?php echo lang('Action'); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
           
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
  <!-- /.content -->
<!--End Modal Crud -->
<script>
$(document).ready(function() {
var table;

var url = "<?php echo base_url(); ?>";
    //datatables
    table = $('#example1').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": url + "user/ajax_list",
            "type": "POST",
            "data": function ( data ) {
                data.FileSequence = $('#selectcityAmin').val();
                data.Filetype = $('#selectfiletype').val();
                data.startdate = $('#startdate').val();
                data.enddate = $('#enddate').val();
                data.status = $('#status').val();
            }
        }
    });
$('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });

      $('#export').click( function () {
          var id = $('#selectcityAmin').val();
          var efile = $('#selectfiletype').val();// get selected value
          var from = $('#startdate').val();
          var to = $('#enddate').val();
          var status = $('#status').val();
          if (id && id!='all') { // require a URL
      url ='<?php echo base_url(); ?>user/export1099CSV/'+ id +'/'+ efile +'/'+from+'/'+to+'/'+status;
          window.location.href = url; // redirect
          }else{
        window.location.href ='<?php echo base_url(); ?>user/export1099CSV';
    }
          return false;
      });
});
</script>