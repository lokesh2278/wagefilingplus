<?php
defined('BASEPATH') or exit('No direct script access allowed ');
class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->CustID = isset($this->session->get_userdata()['user_details'][0]->CustID) ? $this->session->get_userdata()['user_details'][0]->CustID : '';
        $this->lang->load('user', getSetting('language'));
        $this->load->library('excel');

    }

    /**
     * This function is redirect to users profile page
     * @return Void
     */
    public function index()
    {
        if (isLogin()) {
            redirect(base_url() . 'dashboard', 'refresh');
        }
    }

    /**
     * This function to load dashboard
     * @return Void
     */
    public function dashboard()
    {
        isLogin();
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        //print_r($CustID);die;
        $sqlUser = "SELECT * FROM `users` WHERE CustID= '$CustID'";
        $data = $this->User_model->getQrResult($sqlUser);
        // print_r($data);die;
        $todayDate = date('Y-m-d');
        //echo $todayDate;die;
        if ($data[0]->user_type == 'user') {
            $data[] = '';

            $qr = " SELECT count(*) AS `ia` FROM `Files` WHERE `CustID` = $CustID";
            $data['Total_companies'] = $this->User_model->getQrResult($qr);

            $qr = " SELECT count(*) AS `ia` FROM `Files` WHERE `CustID` = $CustID  AND `created_at` LIKE '%$todayDate%'";
            $data['Today_Registered_data'] = $this->User_model->getQrResult($qr);

            $qr = " SELECT count(*) AS `ia` FROM `Files`   WHERE  `Files`.`is_deleted` = 0 ";
            $data['Active_Users_data'] = $this->User_model->getQrResult($qr);

            $query = "SELECT MONTH(`UDate`) as `months`, '1' AS `mka_sum` FROM  `users`  WHERE YEAR(`UDate`) = '" . date("Y") . "'  ";
            $data['Bar_1'] = $this->User_model->getBarChartData($query);
            $query = "SELECT * FROM `Files` WHERE `CustID` = $CustID ORDER BY `CustID` DESC LIMIT 5 ";
            $data['last_10_registration_list'] = $this->User_model->getQrResult($query);
            $this->load->view('include/header');
            $this->load->view('dashboard', $data);
            $this->load->view('include/footer');
        } else {
            $data[] = '';

            $qr = " SELECT count(*) AS `ia` FROM `users`   ";
            $data['Total_Users_data'] = $this->User_model->getQrResult($qr);

            $qr = " SELECT count(*) AS `ia` FROM `users`   ";
            $data['Today_Registered_data'] = $this->User_model->getQrResult($qr);

            $qr = " SELECT count(*) AS `ia` FROM `users`   WHERE  `users`.`status` = 'active' ";
            $data['Active_Users_data'] = $this->User_model->getQrResult($qr);

            $query = "SELECT MONTH(`UDate`) as `months`, '1' AS `mka_sum` FROM  `users`  WHERE YEAR(`UDate`) = '" . date("Y") . "'  ";
            $data['Bar_1'] = $this->User_model->getBarChartData($query);
            $query = "SELECT * FROM  `users`   WHERE  `users`.`user_type` != 'admin' ORDER BY `CustID` DESC LIMIT 10 ";
            $data['last_10_registration_list'] = $this->User_model->getQrResult($query);
            $this->load->view('include/header');
            $this->load->view('dashboard', $data);
            $this->load->view('include/footer');
        }
    }

    /**
     * This function is used to load login view page
     * @return Void
     */
    public function login()
    {
        if (isset($_SESSION['user_details'])) {
            redirect(base_url() . 'dashboard', 'refresh');
        }
        $this->load->view('login');
    }

    /**
     * This function is used to logout user
     * @return Void
     */
    public function logout()
    {
        isLogin();
        $this->session->unset_userdata('user_details');
        redirect(base_url() . 'user/login', 'refresh');
    }

    /**
     * This function is used to registr user
     * @return Void
     */
    public function registration()
    {
        if (isset($_SESSION['user_details'])) {
            redirect(base_url() . 'profile', 'refresh');
        }
        //Check if admin allow to registration for user
        if (getSetting('register_allowed') == 1) {
            if ($this->input->post()) {
                $this->addEdit();
                $art_msg['msg'] = lang('successfully_registered');
                $art_msg['type'] = 'success';
                $this->session->set_userdata('alert_msg', $art_msg);
            } else {
                $this->load->view('register');
            }
        } else {
            $art_msg['msg'] = lang('registration_not_allowed');
            $art_msg['type'] = 'warning';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        }
    }

    /**
     * This function is used for user authentication ( Working in login process )
     * @return Void
     */
    public function authUser($page = '')
    {
        $return = $this->User_model->authUser();
        if (empty($return)) {
            $art_msg['msg'] = lang('invalid_details');
            $art_msg['type'] = 'warning';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        } else {
            if ($return == 'not_varified') {
                $art_msg['msg'] = lang('this_account_is_not_verified_please_contact_to_your_admin');
                $art_msg['type'] = 'danger';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . 'user/login', 'refresh');
            } else {
                $this->session->set_userdata('user_details', $return);
            }
            redirect(base_url() . 'dashboard', 'refresh');
        }
    }

    /**
     * This function is used send mail in forget password
     * @return Void
     */
    public function forgetPassword()
    {
        $page['title'] = lang('forgot_password');
        if ($this->input->post()) {
            $setting = settings();
            $res = $this->User_model->getDataBy('users', $this->input->post('email'), 'email', 1);
            if (isset($res[0]->CustID) && $res[0]->CustID != '') {
                $var_key = $this->getVerificationCode();
                $this->User_model->updateRow('users', 'CustID', $res[0]->CustID, array('var_key' => $var_key));
                $sub = "Reset password";
                $email = $this->input->post('email');
                $data = array(
                    'user_name' => $res[0]->name,
                    'action_url' => base_url(),
                    'sender_name' => $setting['FileName'],
                    'website_name' => $setting['website'],
                    'varification_link' => base_url() . 'user/mailVerify?code=' . $var_key,
                    'url_link' => base_url() . 'user/mailVerify?code=' . $var_key,
                );
                $body = $this->User_model->getTemplate('forgot_password');
                $body = $body->html;
                foreach ($data as $key => $value) {
                    $body = str_replace('{var_' . $key . '}', $value, $body);
                }
                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
                    // content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                if ($emm) {
                    $art_msg['msg'] = lang('to_reset_your_password_link_has_been_sent_to_your_email');
                    $art_msg['type'] = 'info';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url() . 'user/login', 'refresh');
                }
            } else {
                $art_msg['msg'] = lang('this_account_does_not_exist');
                $art_msg['type'] = 'danger';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . "user/forgetpassword");
            }
        } else {
            $this->load->view('forget_password');
        }
    }

    /**
     * This function is used to load view of reset password and varify user too
     * @return : void
     */
    public function mailVerify()
    {
        $return = $this->User_model->mailVerify();
        if ($return) {
            $data['email'] = $return;
            $this->load->view('set_password', $data);
        } else {
            $data['email'] = 'allredyUsed';
            $this->load->view('set_password', $data);
        }
    }

    /**
     * This function is used to reset password in forget password process
     * @return : void
     */
    public function reset_password()
    {
        $return = $this->User_model->ResetPpassword();
        if ($return) {
            $art_msg['msg'] = lang('password_changed_successfully');
            $art_msg['type'] = 'success';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        } else {
            $art_msg['msg'] = lang('unable_to_update_password');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        }
    }

    /**
     * This function is generate hash code for random string
     * @return string
     */
    public function getVerificationCode()
    {
        $pw = $this->randomString();
        return $varificat_key = password_hash($pw, PASSWORD_DEFAULT);
    }

    /**
     * This function is used for show users list
     * @return Void
     */
    public function userlist()
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            $this->load->view('include/header');
            $this->load->view('user_table');
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }
    /**
     * This function is used for show users list
     * @return Void
     */
    public function companylist()
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            $filter_coo = '';
            if (isset($_COOKIE[strtolower('filter_coo')])) {
                $filter_coo = json_decode($_COOKIE[strtolower('filter_coo')]);
            }
            $sel = '';
            if (isset($filter_coo->Files___CustID___filter)) {
                $sel = $filter_coo->Files___CustID___filter;
            }
            $sel = $this->input->get('u');
            $data['f_option'] = $this->get_filter_dropdown_options('name', 'users', 'name', $sel, 'CustID');
            $this->load->view('include/header');
            $this->load->view('company_table', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }

    public function set_filter_cookie()
    {
        if (!empty($this->input->post(strtolower('filter_coo')))) {
            setcookie(strtolower('filter_coo'), json_encode($this->input->post(strtolower('filter_coo'))), time() + (86400 * 30 * 365), "/");
        }
    }

    /**
     * This function is used for show users list
     * @return Void
     */
    public function vendorlist()
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            $filter_coo = '';
            if (isset($_COOKIE[strtolower('filter_coo')])) {
                $filter_coo = json_decode($_COOKIE[strtolower('filter_coo')]);
            }
            $sel = '';
            if (isset($filter_coo->ia_vendors___c_id___filter)) {
                $sel = $filter_coo->ia_vendors___c_id___filter;
            }
            $sel = $this->input->get('c');
            $data['f_option'] = $this->get_filter_dropdown_options('c_name', 'Files', 'c_name', $sel, 'FileSequence');
            $this->load->view('include/header');
            $this->load->view('vendor_table', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }

    public function get_filter_dropdown_options($field, $rel_table, $rel_col, $selected, $tb_id)
    {
        $res = $this->User_model->get_filter_dropdown_options($field, $rel_table, $rel_col, $tb_id);
        $option = '';
        if (isset($res) && is_array($res) && !empty($res)) {
            foreach ($res as $key => $value) {
                if ($rel_table != '') {
                    $col = $tb_id;
                    $se = '';
                    if ($selected == $value->$col) {
                        $se = 'selected';
                    }
                    $option .= '<option ' . $se . ' value="' . $value->$col . '">' . $value->$rel_col . '</option>';
                } else {
                    $se = '';
                    if ($selected == $value->$field) {
                        $se = 'selected';
                    }
                    $option .= '<option ' . $se . ' value="' . $value->$field . '">' . $value->$field . '</option>';
                }
            }
        }
        return $option;
    }
    /**
     * This function is used to create datatable in users list page
     * @return Void
     */
    public function userTable()
    {
        isLogin();
        $table = 'users';
        $primaryKey = 'users_id';
        $columns = array(
            array('db' => 'users.CustID AS CustID', 'field' => 'CustID', 'dt' => 0),
            array('db' => 'users.CustID AS CustID', 'field' => 'CustID', 'dt' => 1),
            // array('db' => 'ia_users.status AS status', 'field' => 'status', 'dt' => 2),
            array('db' => 'users.UEmail AS UEmail', 'field' => 'UEmail', 'dt' => 2),
            array('db' => 'users.visible_password AS visible_password', 'field' => 'visible_password', 'dt' => 3),
            // array('db' => 'ia_users.user_type AS user_type', 'field' => 'user_type', 'dt' => 5),
            array('db' => 'users.UDate AS UDate', 'field' => 'UDate', 'dt' => 4),
            array('db' => 'users.Ucompany AS Ucompany', 'field' => 'Ucompany', 'dt' => 5),
            // array('db' => 'ia_users.status AS status', 'field' => 'status', 'dt' => 2),
            array('db' => 'users.name AS name', 'field' => 'name', 'dt' => 6, 'formatter' => function ($d, $row) {
                return '<a href="' . base_url() . 'user/companylist?u=' . $row['CustID'] . '">' . $d . '</a>';
            }),

        );

        $joinQuery = "FROM `" . $table . "` ";
        $cf = getCustomField('user');
        if (is_array($cf) && !empty($cf)) {
            foreach ($cf as $cfkey => $cfvalue) {
                array_push($columns, array('db' => "ia_custom_fields_values_" . $cfkey . ".value AS cfv_" . $cfkey, 'field' => "cfv_" . $cfkey, 'dt' => count($columns)));
                $joinQuery .= " LEFT JOIN `ia_custom_fields_values` AS ia_custom_fields_values_" . $cfkey . "  ON  `users`.`CustID` = `ia_custom_fields_values_" . $cfkey . "`.`rel_crud_id` AND `ia_custom_fields_values_" . $cfkey . "`.`cf_id` =  '" . $cfvalue->custom_fields_id . "' ";
            }
        }
        array_push($columns, array('db' => 'users.CustID AS CustID', 'field' => 'CustID', 'dt' => count($columns)));

        $j = 0;
        if (strpos($joinQuery, 'JOIN') > 0) {
            $j = 1;
        }
        $where = SSP::iaFilter($_GET, $columns, $j);
        if ($where == '') {
            $where .= ' WHERE ';
        } else {
            $where .= ' AND ';
        }
        if (CheckPermission("user", "all_read") || $this->session->get_userdata()['user_details'][0]->user_type == 'admin') {
            $where .= "  `users`.`user_type` != 'admin' AND `users`.`CustID` != $this->CustID";
        } else {
            $where .= "  `users`.`user_type` != 'admin' AND `users`.`CustID` = $this->CustID";
        }

        $limit = SSP::limit($_GET, $columns);
        $order = SSP::iaOrder($_GET, $columns, $j);
        if (trim($order) == 'ORDER BY') {
            $order = '';
        }
        $col = SSP::pluck($columns, 'db', $j);

        $query = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $col) . " " . $joinQuery . " " . $where . " " . $order . " " . $limit . " ";
        $res = $this->db->query($query);
        $res = $res->result_array();
        $recordsTotal = $this->db->select("count('CustID') AS c")->get('users')->row()->c;
        $res = SSP::iaDataOutput($columns, $res, $j);

        $output_arr['draw'] = intval($_GET['draw']);
        $output_arr['recordsTotal'] = intval($recordsTotal);
        $output_arr['recordsFiltered'] = intval($recordsTotal);
        $output_arr['data'] = $res;

        foreach ($output_arr['data'] as $key => $value) {
            $id = $output_arr['data'][$key][count($output_arr['data'][$key]) - 1];
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] = '';
            if (CheckPermission('user', "all_update")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            } else if (CheckPermission('user', "own_update") && (CheckPermission('user', "all_update") != true)) {
                $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
                if ($CustID == $this->CustID) {
                    $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
                }
            }

            if (CheckPermission('user', "all_delete")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';} else if (CheckPermission('user', "own_delete") && (CheckPermission('user', "all_delete") != true)) {
                $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
                if ($CustID == $this->CustID) {
                    $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';
                }
            }

            $output_arr['data'][$key][1] = '<a href="' . base_url("userLogin") . '/' . $output_arr['data'][$key][1] . '" >Login</a>';

            $output_arr['data'][$key][0] = '<input type="checkbox" id="basic_checkbox_' . $key . '" name="selData" value="' . $output_arr['data'][$key][0] . '"><label for="basic_checkbox_' . $key . '"></label>';
        }
        echo json_encode($output_arr);
    }

    /**
     * This function is used to create datatable in companies list page
     * @return Void
     */
    public function companyTable()
    {
        isLogin();
        $table = 'Files';
        $primaryKey = 'FileSequence';
        $columns = array(
            array('db' => 'Files.FileSequence AS FileSequence', 'field' => 'FileSequence', 'dt' => 0),
            array('db' => 'Files.c_name AS c_name', 'field' => 'c_name', 'dt' => 1, 'formatter' => function ($d, $row) {
                //return '<a href="' . base_url() . 'user/vendorlist?c=' . $row['c_id'] . '">' . $d . '</a>';
                return '<a href="' . base_url() . 'user/viewCompany/' . $row['FileSequence'] . '" title="Login As User"> ' . $d . '</a>';
                //return '<a href="#">' . $d.'</a>';
            }),
            // array('db' => 'Files.c_email AS c_email', 'field' => 'c_email', 'dt' => 2),//TaxYear
            array('db' => 'Files.TaxYear AS TaxYear', 'field' => 'TaxYear', 'dt' => 2), //TaxYear
            array('db' => 'Files.c_employee_ein AS c_employee_ein', 'field' => 'c_employee_ein', 'dt' => 3),
            array('db' => 'Files.c_telephone AS c_telephone', 'field' => 'c_telephone', 'dt' => 4),
            array('db' => 'Files.FormType AS FormType', 'field' => 'FormType', 'dt' => 5),
            array('db' => "DATE_FORMAT(Files.created_at, '%d/%m/%Y') AS created_at", 'field' => 'created_at', 'dt' => 6),

        );

        $joinQuery = "FROM `" . $table . "` ";
        $cf = getCustomField('user');
        if (is_array($cf) && !empty($cf)) {
            foreach ($cf as $cfkey => $cfvalue) {
                array_push($columns, array('db' => "ia_custom_fields_values_" . $cfkey . ".value AS cfv_" . $cfkey, 'field' => "cfv_" . $cfkey, 'dt' => count($columns)));
                $joinQuery .= " LEFT JOIN `ia_custom_fields_values` AS ia_custom_fields_values_" . $cfkey . "  ON  `Files`.`FileSequence` = `ia_custom_fields_values_" . $cfkey . "`.`rel_crud_id` AND `ia_custom_fields_values_" . $cfkey . "`.`cf_id` =  '" . $cfvalue->custom_fields_id . "' ";
            }
        }
        array_push($columns, array('db' => 'Files.FileSequence AS FileSequence', 'field' => 'FileSequence', 'dt' => count($columns)));

        $j = 0;
        if (strpos($joinQuery, 'JOIN') > 0) {
            $j = 1;
        }
        $where = SSP::iaFilter($_GET, $columns, $j);
        if ($this->input->get('Files___CustID___filter') && $this->input->get('Files___CustID___filter') != 'all') {
            $and = '';
            if ($where != '') {
                $and = ' AND ';
            } else {
                $where .= 'WHERE';
            }
            $where .= $and . " `Files`.`CustID` = '" . $this->input->get('Files___CustID___filter') . "' AND `Files`.`is_deleted` = '0' ";
        }

        // $where .= "  `Files`.`is_deleted` = 0";

        $limit = SSP::limit($_GET, $columns);
        $order = SSP::iaOrder($_GET, $columns, $j);
        if (trim($order) == 'ORDER BY') {
            $order = '';
        }
        $col = SSP::pluck($columns, 'db', $j);

        $query = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $col) . " " . $joinQuery . " " . $where . " " . $order . " " . $limit . " ";
        $res = $this->db->query($query);
        $res = $res->result_array();

        $recordsTotal = $this->db->query("SELECT SQL_CALC_FOUND_ROWS count('c_id') AS `c` " . $joinQuery . " " . $where . " " . $order . "")->row();
        if ($recordsTotal) {
            $recordsTotal = $recordsTotal->c;
        }
        $res = SSP::iaDataOutput($columns, $res, $j);

        $output_arr['draw'] = intval($_GET['draw']);
        $output_arr['recordsTotal'] = intval($recordsTotal);
        $output_arr['recordsFiltered'] = intval($recordsTotal);
        $output_arr['data'] = $res;

        foreach ($output_arr['data'] as $key => $value) {
            $id = $output_arr['data'][$key][count($output_arr['data'][$key]) - 1];
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] = '';
            /*if (CheckPermission('user', "all_update")) {
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            } else if (CheckPermission('user', "own_update") && (CheckPermission('user', "all_update") != true)) {
            $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
            if ($CustID == $this->CustID) {
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            }
            }*/
            if (CheckPermission('user', "all_update")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" class="hrefid mClass1" href="' . base_url() . 'user/viewCompany/' . $id . '"  title="View"><i class="material-icons font-20">visibility</i></a>';
            }
            if (CheckPermission('user', "all_delete")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setIdCompany(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';} else if (CheckPermission('user', "own_delete") && (CheckPermission('user', "all_delete") != true)) {
                $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
                if ($CustID == $this->CustID) {
                    $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setIdCompany(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';
                }
            }

            $output_arr['data'][$key][0] = '<input type="checkbox" id="basic_checkbox_' . $key . '" name="selData" value="' . $output_arr['data'][$key][0] . '"><label for="basic_checkbox_' . $key . '"></label>';
        }
        echo json_encode($output_arr);
    }

    /**
     * This function is used to create datatable in vendors list page
     * @return Void
     */
    public function vendorTable()
    {
        isLogin();
        $table = 'ia_vendors';
        $primaryKey = 'v_id';
        $columns = array(
            array('db' => 'ia_vendors.v_id AS v_id', 'field' => 'v_id', 'dt' => 0), array('db' => 'ia_vendors.v_name AS v_name', 'field' => 'v_name', 'dt' => 1),
            array('db' => 'ia_vendors.v_email AS v_email', 'field' => 'v_email', 'dt' => 2),
            array('db' => 'ia_vendors.pdf AS pdf', 'field' => 'pdf', 'dt' => 3),
            array('db' => 'ia_vendors.v_status AS v_status', 'field' => 'v_status', 'dt' => 4),
            array('db' => "DATE_FORMAT(ia_vendors.created_at, '%d/%m/%Y') AS created_at", 'field' => 'created_at', 'dt' => 5),

        );

        $joinQuery = "FROM `" . $table . "` ";
        $cf = getCustomField('user');
        if (is_array($cf) && !empty($cf)) {
            foreach ($cf as $cfkey => $cfvalue) {
                array_push($columns, array('db' => "ia_custom_fields_values_" . $cfkey . ".value AS cfv_" . $cfkey, 'field' => "cfv_" . $cfkey, 'dt' => count($columns)));
                $joinQuery .= " LEFT JOIN `ia_custom_fields_values` AS ia_custom_fields_values_" . $cfkey . "  ON  `ia_vendors`.`v_id` = `ia_custom_fields_values_" . $cfkey . "`.`rel_crud_id` AND `ia_custom_fields_values_" . $cfkey . "`.`cf_id` =  '" . $cfvalue->custom_fields_id . "' ";
            }
        }
        array_push($columns, array('db' => 'ia_vendors.v_id AS v_id', 'field' => 'v_id', 'dt' => count($columns)));

        $j = 0;
        if (strpos($joinQuery, 'JOIN') > 0) {
            $j = 1;
        }
        $where = SSP::iaFilter($_GET, $columns, $j);
        if ($this->input->get('ia_vendors___c_id___filter') && $this->input->get('ia_vendors___c_id___filter') != 'all') {
            $and = '';
            if ($where != '') {
                $and = ' AND ';
            } else {
                $where .= 'WHERE';
            }
            $where .= $and . " `ia_vendors`.`FileSequence` = '" . $this->input->get('ia_vendors___c_id___filter') . "' ";
        }
        /*if ($where == '') {
        $where .= '  ';
        } else {
        $where .= ' AND ';
        }*/

        // $where .= "  `ia_vendors`.`is_deleted` = 0";

        $limit = SSP::limit($_GET, $columns);
        $order = SSP::iaOrder($_GET, $columns, $j);
        if (trim($order) == 'ORDER BY') {
            $order = '';
        }
        $col = SSP::pluck($columns, 'db', $j);

        $query = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $col) . " " . $joinQuery . " " . $where . " " . $order . " " . $limit . " ";
        $res = $this->db->query($query);
        $res = $res->result_array();
        /*$recordsTotal = $this->db->select("count('v_id') AS c")->get('ia_vendors')->row()->c;*/
        $recordsTotal = $this->db->query("SELECT SQL_CALC_FOUND_ROWS count('v_id') AS `c` " . $joinQuery . " " . $where . " " . $order . "")->row();
        if ($recordsTotal) {
            $recordsTotal = $recordsTotal->c;
        }
        $res = SSP::iaDataOutput($columns, $res, $j);

        $output_arr['draw'] = intval($_GET['draw']);
        $output_arr['recordsTotal'] = intval($recordsTotal);
        $output_arr['recordsFiltered'] = intval($recordsTotal);
        $output_arr['data'] = $res;

        foreach ($output_arr['data'] as $key => $value) {
            if (!empty($output_arr['data'][$key][3])) {
                $output_arr['data'][$key][3] = '<a href="' . $output_arr['data'][$key][3] . '" download><i class="material-icons" style="float: left;">file_download</i><div style="display: inline-block;float: left;">PDF</div></a>';
            }
            $id = $output_arr['data'][$key][count($output_arr['data'][$key]) - 1];
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] = '';
            /*if (CheckPermission('user', "all_update")) {
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            } else if (CheckPermission('user', "own_update") && (CheckPermission('user', "all_update") != true)) {
            $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
            if ($CustID == $this->CustID) {
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            }
            }*/

            if (CheckPermission('user', "all_delete")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setIdCompany(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';} else if (CheckPermission('user', "own_delete") && (CheckPermission('user', "all_delete") != true)) {
                $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
                if ($CustID == $this->CustID) {
                    $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setIdVendor(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';
                }
            }

            $output_arr['data'][$key][0] = '<input type="checkbox" id="basic_checkbox_' . $key . '" name="selData" value="' . $output_arr['data'][$key][0] . '"><label for="basic_checkbox_' . $key . '"></label>';
        }
        echo json_encode($output_arr);
    }

    /**
     * This function is Showing users profile
     * @return Void
     */
    public function profile($id = '')
    {
        isLogin();
        if (!isset($id) || $id == '') {
            $id = $this->session->userdata('user_details')[0]->CustID;
        }
        $data['user_data'] = $this->User_model->getUsers($id);
        $this->load->view('include/header');
        $this->load->view('profile', $data);
        $this->load->view('include/footer');
    }

    /**
     * This function is used to show popup of user to add and update
     * @return Void
     */
    public function getModal()
    {
        isLogin();
        if ($this->input->post('id')) {
            $data['userData'] = getDataByid('users', $this->input->post('id'), 'CustID');
            echo $this->load->view('add_user', $data, true);
        } else {
            echo $this->load->view('add_user', '', true);
        }
        exit;
    }

    /**
     * This function is used to add and update users
     * @return Void
     */
    public function addEdit($id = '')
    {
        $data = $this->input->post();
        $profile_pic = 'user.png';
        if ($this->input->post('CustID')) {
            $id = $this->input->post('CustID');
        }
        if (isset($this->session->userdata('user_details')[0]->CustID)) {
            if ($this->input->post('CustID') == $this->session->userdata('user_details')[0]->CustID) {
                $redirect = 'profile';
                $setSession = true;
            } else {
                $redirect = 'user/userlist';
                $setSession = false;
            }
        } else {
            $redirect = 'user/login';
        }
        if ($this->input->post('fileOld')) {
            $newname = $this->input->post('fileOld');
            $profile_pic = $newname;
        } else {
            $profile_pic = 'user.png';
        }
        foreach ($_FILES as $name => $fileInfo) {
            if (!empty($_FILES[$name]['name'])) {
                if ($newname = upload($name)) {
                    $data[$name] = $newname;
                    $profile_pic = $newname;
                }
            } else {
                if ($this->input->post('fileOld')) {
                    $newname = $this->input->post('fileOld');
                    $data[$name] = $newname;
                    $profile_pic = $newname;
                } else {
                    $data[$name] = '';
                    $profile_pic = 'user.png';
                }
            }
        }
        if ($id != '') {
            $data = $this->input->post();
            if ($this->input->post('status') != '') {
                $data['status'] = $this->input->post('status');
            }
            if ($this->input->post('CustID') == 1) {
                $data['user_type'] = 'admin';
                $data['status'] = 'active';
            }
            $checkValue = $this->User_model->checkExists('users', 'email', $this->input->post('email'), $id, 'CustID');
            if ($checkValue == false) {
                $art_msg['msg'] = lang('this_email_already_registered_with_us');
                $art_msg['type'] = 'info';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . 'user/userlist', 'refresh');
            }

            if ($this->input->post('password') != '') {
                if ($this->input->post('currentpassword') != '') {
                    $old_row = getDataByid('users', $this->input->post('CustID'), 'CustID');
                    if (password_verify($this->input->post('currentpassword'), $old_row->password)) {
                        if ($this->input->post('password') == $this->input->post('confirmPassword')) {
                            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                            $data['password'] = $password;
                            $data['visible_password'] = $this->input->post('password');
                        } else {
                            $art_msg['msg'] = lang('password_and_confirm_password_should_be_same');
                            $art_msg['type'] = 'warning';
                            $this->session->set_userdata('alert_msg', $art_msg);
                            redirect(base_url() . $redirect, 'refresh');
                        }
                    } else {
                        $art_msg['msg'] = lang('enter_valid_current_password');
                        $art_msg['type'] = 'warning';
                        $this->session->set_userdata('alert_msg', $art_msg);
                        redirect(base_url() . $redirect, 'refresh');
                    }
                } else {
                    $art_msg['msg'] = lang('current_password_is_required');
                    $art_msg['type'] = 'warning';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url() . $redirect, 'refresh');
                }
            }
            $id = $this->input->post('CustID');
            unset($data['fileOld']);
            unset($data['currentpassword']);
            unset($data['confirmPassword']);
            unset($data['CustID']);
            if (isset($data['edit'])) {
                unset($data['edit']);
            }
            if (isset($data['password']) && $data['password'] == '') {
                unset($data['password']);
            }
            if (isset($data['mkacf'])) {
                $custom_fields = $data['mkacf'];
                unset($data['mkacf']);
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $cfkey => $cfvalue) {
                        $qr = "SELECT * FROM `ia_custom_fields_values` WHERE `rel_crud_id` = '" . $id . "' AND `cf_id` = '" . $cfkey . "'";
                        $cf_data = $this->User_model->getQrResult($qr);
                        if (is_array($cf_data) && !empty($cf_data)) {
                            $d = array(
                                "value" => $custom_fields[$cf_data[0]->cf_id],
                            );
                            $this->User_model->updateRow('ia_custom_fields_values', 'ia_custom_fields_values_id', $cf_data[0]->ia_custom_fields_values_id, $d);
                        } else {
                            $d = array(
                                "rel_crud_id" => $id,
                                "cf_id" => $cfkey,
                                "curd" => 'user',
                                "value" => $cfvalue,
                            );
                            $this->User_model->insertRow('ia_custom_fields_values', $d);
                        }
                    }
                }
            }
            $data['profile_pic'] = $profile_pic;
            foreach ($data as $dkey => $dvalue) {
                if (is_array($dvalue)) {
                    $data[$dkey] = implode(',', $dvalue);
                }
            }
            $this->User_model->updateRow('users', 'CustID', $id, $data);
            $new_data = $this->User_model->getDataBy('users', $id, 'CustID');
            if (isset($setSession) && $setSession == true) {
                $this->session->set_userdata('user_details', $new_data);
            }

            $art_msg['msg'] = lang('your_data_updated_successfully');
            $art_msg['type'] = 'success';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . $redirect, 'refresh');
        } else {
            if ($this->input->post('user_type') != 'admin') {
                $data = $this->input->post();
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $checkValue = $this->User_model->checkExists('users', 'email', $this->input->post('email'));
                if ($checkValue == false) {
                    $art_msg['msg'] = lang('this_email_already_registered_with_us');
                    $art_msg['type'] = 'warning';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url() . 'user/userlist', 'refresh');
                }

                $data['status'] = 'active';
                if (getSetting('admin_approval') == 1) {
                    $data['status'] = 'pending';
                }

                if ($this->input->post('status') != '') {
                    $data['status'] = $this->input->post('status');
                }

                $data['CustID'] = $this->CustID;
                $data['password'] = $password;
                $data['profile_pic'] = $profile_pic;
                $data['is_deleted'] = 0;
                $data['create_date'] = date('Y-m-d');
                if (isset($data['password_confirmation'])) {
                    unset($data['password_confirmation']);
                }
                if (isset($data['call_from'])) {
                    unset($data['call_from']);
                }
                unset($data['submit']);
                $custom_fields = array();
                if (isset($data['mkacf'])) {
                    $custom_fields = $data['mkacf'];
                    unset($data['mkacf']);
                }

                foreach ($data as $dkey => $dvalue) {
                    if (is_array($dvalue)) {
                        $data[$dkey] = implode(',', $dvalue);
                    }
                }
                if ($redirect == 'login') {
                    $data['status'] = 'pending';
                    $var_key = $this->getVerificationCode();
                    $data['var_key'] = $var_key;
                }
                $last_id = $this->User_model->insertRow('users', $data);
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $cfkey => $cfvalue) {
                        $d = array(
                            "rel_crud_id" => $last_id,
                            "cf_id" => $cfkey,
                            "curd" => 'user',
                            "value" => $cfvalue,
                        );
                        $this->User_model->insertRow('ia_custom_fields_values', $d);
                    }
                }
                if ($redirect == 'login') {
                    $this->registerMail($data);
                }
                redirect(base_url() . $redirect, 'refresh');
            } else {
                $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
                $art_msg['type'] = 'danger';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . 'user/registration', 'refresh');
            }
        }

    }

    /**
     * This function is used to send verification mail for registeration of ia_users
     * @return Void
     */

    public function registerMail($data1)
    {
        $setting = settings();
        $res = $this->User_model->getDataBy('users', $data1['email'], 'UEmail', 1);
        if (isset($res[0]->CustID) && $res[0]->CustID != '') {
            $sub = "Varify your account";
            $email = $data1['email'];

            $data = array(
                'user_name' => $res[0]->name,
                'varification_link' => base_url() . 'user/registrationMailVerify?code=' . $data1['var_key'],
                'website_name' => $setting['website'],
            );
            $body = $this->User_model->getTemplate('registration');
            $body = $body->html;
            foreach ($data as $key => $value) {
                $body = str_replace('{var_' . $key . '}', $value, $body);
            }

            if ($setting['mail_setting'] == 'php_mailer') {
                $this->load->library("send_mail");
                $emm = $this->send_mail->email($sub, $body, $email, $setting);
            } else {
                // content-type is required when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                $emm = mail($email, $sub, $body, $headers);
            }
            if ($emm) {
                $art_msg['msg'] = lang('successfully_registered_check_your_mail_for_varification');
                $art_msg['type'] = 'success';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . 'user/login', 'refresh');
            }
        }

    }

    public function registrationMailVerify()
    {
        $return = $this->User_model->registrationMailVerify();
        if ($return) {
            $art_msg['msg'] = lang('successfully_verified') . '!';
            $art_msg['type'] = 'success';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        } else {
            $art_msg['msg'] = lang('invalid_link');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/registration', 'refresh');
        }
    }

    /**
     * This function is used to delete users
     * @return Void
     */
    public function delete($id)
    {
        isLogin();
        $ids = explode('-', $id);
        foreach ($ids as $id) {
            $this->User_model->delete($id);
        }
        redirect(base_url() . 'user/userlist', 'refresh');
    }

    /**
     * This function is used to delete users
     * @return Void
     */
    public function deletecompany($id)
    {
        isLogin();
        $ids = explode('-', $id);
        foreach ($ids as $id) {
            $this->User_model->deleteCompany($id);
        }
        redirect(base_url() . 'user/companylist', 'refresh');
    }

    /**
     * This function is used to delete users
     * @return Void
     */
    public function deletevendor($id)
    {
        isLogin();
        $ids = explode('-', $id);
        foreach ($ids as $id) {
            $this->User_model->deleteVendor($id);
        }
        redirect(base_url() . 'user/vendorlist', 'refresh');
    }
    /**
     * This function is used to send invitation mail to users for registration
     * @return Void
     */
    public function InvitePeople()
    {
        isLogin();
        if ($this->input->post('emails')) {
            $setting = settings();
            $var_key = $this->randomString();
            $emailArray = explode(',', $this->input->post('emails'));
            $emailArray = array_map('trim', $emailArray);
            $body = $this->User_model->getTemplate('invitation');
            $result['existCount'] = 0;
            $result['seccessCount'] = 0;
            $result['invalidEmailCount'] = 0;
            $result['noTemplate'] = 0;
            if (isset($body->html) && $body->html != '') {
                $body = $body->html;
                foreach ($emailArray as $mailKey => $mailValue) {
                    if (filter_var($mailValue, FILTER_VALIDATE_EMAIL)) {
                        $res = $this->User_model->getDataBy('ia_users', $mailValue, 'email');
                        if (is_array($res) && empty($res)) {
                            $link = (string) '<a href="' . base_url() . 'user/registration?invited=' . $var_key . '">Click here</a>';
                            $data = array('var_user_email' => $mailValue, 'var_inviation_link' => $link);
                            foreach ($data as $key => $value) {
                                $body = str_replace('{' . $key . '}', $value, $body);
                            }
                            if ($setting['mail_setting'] == 'php_mailer') {
                                $this->load->library("send_mail");
                                $emm = $this->send_mail->email('Invitation for registration', $body, $mailValue, $setting);
                            } else {
                                // content-type is required when sending HTML email
                                $headers = "MIME-Version: 1.0" . "\r\n";
                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                                $emm = mail($mailValue, 'Invitation for registration', $body, $headers);
                            }
                            if ($emm) {
                                $darr = array('email' => $mailValue, 'var_key' => $var_key);
                                $this->User_model->insertRow('ia_users', $darr);
                                $result['seccessCount'] += 1;
                            }
                        } else {
                            $result['existCount'] += 1;
                        }
                    } else {
                        $result['invalidEmailCount'] += 1;
                    }
                }
            } else {
                $result['noTemplate'] = lang('no_email_template_availabale') . '.';
            }
        }
        echo json_encode($result);
        exit;
    }

    /**
     * This function is used to Check invitation code for user registration
     * @return TRUE/FALSE
     */
    public function chekInvitation()
    {
        if ($this->input->post('code') && $this->input->post('code') != '') {
            $res = $this->User_model->getDataBy('users', $this->input->post('code'), 'var_key');
            $result = array();
            if (is_array($res) && !empty($res)) {
                $result['email'] = $res[0]->email;
                $result['CustID'] = $res[0]->CustID;
                $result['result'] = 'success';
            } else {
                $art_msg['msg'] = lang('this_code_is_not_valid');
                $art_msg['type'] = 'warning';
                $this->session->set_userdata('alert_msg', $art_msg);
                $result['result'] = 'error';
            }
        }
        echo json_encode($result);
        exit;
    }

    /**
     * This function is used to registr invited user
     * @return Void
     */
    public function registerInvited($id)
    {
        $data = $this->input->post();
        $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        $data['Upassword'] = $password;
        $data['var_key'] = null;
        $data['is_deleted'] = 0;
        $data['status'] = 'active';
        $data['CustID'] = 1;
        if (isset($data['password_confirmation'])) {
            unset($data['password_confirmation']);
        }
        if (isset($data['call_from'])) {
            unset($data['call_from']);
        }
        if (isset($data['submit'])) {
            unset($data['submit']);
        }
        $this->User_model->updateRow('users', 'CustID', $id, $data);
        $art_msg['msg'] = lang('successfully_registered');
        $art_msg['type'] = 'success';
        $this->session->set_userdata('alert_msg', $art_msg);
        redirect(base_url() . 'user/login', 'refresh');
    }

    /**
     * This function is used to check email is alredy exist or not
     * @return TRUE/FALSE
     */
    public function checEmailExist()
    {
        $result = 1;
        $res = $this->User_model->getDataBy('users', $this->input->post('email'), 'email');
        if (!empty($res)) {
            if ($res[0]->CustID != $this->input->post('uId')) {
                $result = 0;
            }
        }
        echo $result;
        exit;
    }

    /**
     * This function is used to Generate a random string
     * @return String
     */
    public function randomString()
    {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = $alpha . $alpha_upper . $numeric;
        $pw = '';
        $chars = str_shuffle($chars);
        $pw = substr($chars, 8, 8);
        return $pw;
    }

    /**
     * This function is used for show f1099data list
     * @return Void
     */
    public function f1099data($id = '')
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            if (isset($id) && !empty($id)) {
                $id = $id;
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = $id  AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = $id  AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else {
                $sql = "SELECT * FROM `ia_1099_misc` WHERE  `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            }
            $C_sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0  AND `FormType` = '1099-Misc' ORDER BY `Files`.`FileSequence` DESC";
            $data['f1099data'] = $this->User_model->getQrResult($sql);
            $data['c_data'] = $this->User_model->getQrResult($C_sql);

            $this->load->view('include/header');
            $this->load->view('f1099data', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }

    public function viewForm($cid = '', $id = '')
    {
        isLogin();
        $data['c_details'] = $this->User_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->User_model->getData('ia_1099_misc', $id, 'misc_id');
        $this->load->view('include/header');
        if (!empty($data['c_details'])) {
            $this->load->view('viewForm', $data);
        } else {redirect(base_url() . 'user/f1099data', 'refresh');}
        $this->load->view('include/footer');
    }

    /**
     * This function is used for show w2data list
     * @return Void
     */
    public function w2data($id = '')
    {

        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                //print_r($id);die;
                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = $id AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else if ($id && !empty($id)) {

                $id = $id;
                //print_r($id);die;
                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = $id  AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";

            } else {
                $sql = "SELECT * FROM `ia_w2` WHERE  `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            }
            $C_sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0 AND `FormType` = 'W-2' ORDER BY `Files`.`FileSequence` DESC";
            $data['w2data'] = $this->User_model->getQrResult($sql);
            $data['c_data'] = $this->User_model->getQrResult($C_sql);

            $this->load->view('include/header');
            $this->load->view('w2data', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }
    public function w2bydate($date = '')
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            if (isset($date) && !empty($date)) {
                $date = $date;
                $sql = "SELECT * FROM `ia_w2` WHERE `created_at` = $date  AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['date']) && !empty($_REQUEST['date'])) {
                $date = $_REQUEST['date'];
                $sql = "SELECT * FROM `ia_w2` WHERE `created_at` = $date  AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else {
                $sql = "SELECT * FROM `ia_w2` WHERE  `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            }
            $C_sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0 AND `FormType` = 'W-2' ORDER BY `Files`.`FileSequence` DESC";
            $data['w2data'] = $this->User_model->getQrResult($sql);
            $data['c_data'] = $this->User_model->getQrResult($C_sql);

            $this->load->view('include/header');
            $this->load->view('w2data', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }
    public function viewW2($cid = '', $id = '')
    {
        isLogin();
        $data['c_details'] = $this->User_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->User_model->getData('ia_w2', $id, 'w2_id');
        $this->load->view('include/header');
        if (!empty($data['c_details'])) {
            $this->load->view('w2form', $data);
        } else {redirect(base_url() . 'user/f1099data', 'refresh');}
        $this->load->view('include/footer');
    }

    /**
     * This function is used for show view company
     * @return Void
     */
    public function viewCompany($id = '')
    {
        isLogin();

        if ((CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) && !empty($id)) {
            //echo $id;die;
            $data['c_companies'] = $this->User_model->getDataBy('Files', $id, 'FileSequence');
            $this->load->view('include/header');
            $this->load->view('viewCompany', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }

    public function export1099CSV()
    {
        require_once '../application/third_party/PHPExcel/IOFactory.php';
        $this->load->library('excel');

        $file = 'http://dev1.ibrinfotech.com/wagefiling_old/wagefilingold/assets/template/1099-Misc-Payee.xlsx';
        $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
        $excel2 = $excel2->load($file);
        $excel2->setActiveSheetIndex(0);
        $excel2->getActiveSheet()->setTitle('1099-Misc');

        $this->load->database();
        $this->load->model('User_model');

        $users = $this->User_model->get1099Details();
        // print_r($users);die;
        $count = count($users);
        $row = 12;
        foreach ($users as $value) {
            //echo $num;
            $excel2->getActiveSheet()->setCellValue('A' . $row, $value['FormType']);
            $excel2->getActiveSheet()->setCellValue('B' . $row, $value['c_ssn_ein']);
            $excel2->getActiveSheet()->setCellValue('C' . $row, $value['c_name']);
            $excel2->getActiveSheet()->setCellValue('D' . $row, $value['c_address']);
            $excel2->getActiveSheet()->setCellValue('E' . $row, $value['c_address_2']);
            $excel2->getActiveSheet()->setCellValue('F' . $row, $value['c_city']);
            $excel2->getActiveSheet()->setCellValue('G' . $row, $value['c_state']);
            $excel2->getActiveSheet()->setCellValue('H' . $row, $value['c_zip']);
            $excel2->getActiveSheet()->setCellValue('I' . $row, $value['c_telephone']);
            $excel2->getActiveSheet()->setCellValue('J' . $row, $value['c_extention']);
            $excel2->getActiveSheet()->setCellValue('K' . $row, $value['recepient_id_no']);
            $excel2->getActiveSheet()->setCellValue('L' . $row, $value['first_name']);
            $excel2->getActiveSheet()->setCellValue('M' . $row, $value['last_name']);
            $excel2->getActiveSheet()->setCellValue('O' . $row, $value['address']);
            $excel2->getActiveSheet()->setCellValue('P' . $row, $value['city']);
            $excel2->getActiveSheet()->setCellValue('Q' . $row, $value['state']);
            $excel2->getActiveSheet()->setCellValue('R' . $row, $value['zipcode']);
            $excel2->getActiveSheet()->setCellValue('T' . $row, $value['city']);
            $excel2->getActiveSheet()->setCellValue('W' . $row, $value['account_number']);
            $excel2->getActiveSheet()->setCellValue('Z' . $row, $value['rents']);
            $excel2->getActiveSheet()->setCellValue('AA' . $row, $value['royalties']);
            $excel2->getActiveSheet()->setCellValue('AB' . $row, $value['other_income']);
            $excel2->getActiveSheet()->setCellValue('AC' . $row, $value['fed_income_with_held']);
            $excel2->getActiveSheet()->setCellValue('AD' . $row, $value['fishing_boat_proceed']);
            $excel2->getActiveSheet()->setCellValue('AE' . $row, $value['mad_helthcare_pmts']);
            $excel2->getActiveSheet()->setCellValue('AF' . $row, $value['non_emp_compansation']);
            $excel2->getActiveSheet()->setCellValue('AG' . $row, $value['pmts_in_lieu']);
            $excel2->getActiveSheet()->setCellValue('AH' . $row, $value['consumer_products_for_resale']);
            $excel2->getActiveSheet()->setCellValue('AI' . $row, $value['crop_Insurance_proceeds']);
            $excel2->getActiveSheet()->setCellValue('AJ' . $row, $value['excess_golden_par_pmts']);
            $excel2->getActiveSheet()->setCellValue('AK' . $row, $value['gross_paid_to_an_attomey']);
            $excel2->getActiveSheet()->setCellValue('AL' . $row, $value['sec_409a_deferrals']);
            $excel2->getActiveSheet()->setCellValue('AM' . $row, $value['sec_409a_Income']);
            $excel2->getActiveSheet()->setCellValue('AN' . $row, $value['state_tax_with_held']);
            $excel2->getActiveSheet()->setCellValue('AP' . $row, $value['payer_state_no']);
            $excel2->getActiveSheet()->setCellValue('AQ' . $row, $value['state_Income']);
            $row++;
        }
        //$excel2->getActiveSheet()->fromArray($users, null, 'A12');

        $filename = '1099-Misc,' . date('Y-m-d') . '.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
        $objWriter->save('php://output', 'w');
        exit;
    }
    public function exportW2CSV($id = '')
    {
        require_once '../application/third_party/PHPExcel/IOFactory.php';
        $this->load->library('excel');

        $file = 'http://dev1.ibrinfotech.com/wagefiling_old/wagefilingold/assets/template/W2-payee.xlsx';
        $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
        $excel2 = $excel2->load($file); // Empty Sheet
        $excel2->setActiveSheetIndex(0);
        $excel2->getActiveSheet()->setTitle('W-2');
        //load our new PHPExcel library

        $this->load->database();
        $this->load->model('User_model');

        $users = $this->User_model->getW2Details();
        // print_r($users);die;
        $row = 12;
        foreach ($users as $value) {
            //echo $num;
            $excel2->getActiveSheet()->setCellValue('A' . $row, $value['FormType']);
            $excel2->getActiveSheet()->setCellValue('B' . $row, $value['c_employee_ein']);
            $excel2->getActiveSheet()->setCellValue('C' . $row, $value['c_name']);
            $excel2->getActiveSheet()->setCellValue('D' . $row, $value['c_address']);
            $excel2->getActiveSheet()->setCellValue('E' . $row, $value['c_address_2']);
            $excel2->getActiveSheet()->setCellValue('F' . $row, $value['c_city']);
            $excel2->getActiveSheet()->setCellValue('G' . $row, $value['c_state']);
            $excel2->getActiveSheet()->setCellValue('H' . $row, $value['c_zip']);
            $excel2->getActiveSheet()->setCellValue('I' . $row, $value['c_telephone']);
            $excel2->getActiveSheet()->setCellValue('J' . $row, $value['c_extention']);
            $excel2->getActiveSheet()->setCellValue('K' . $row, $value['SSN']);
            $excel2->getActiveSheet()->setCellValue('L' . $row, $value['Fname']);
            $excel2->getActiveSheet()->setCellValue('M' . $row, $value['Lname']);
            $excel2->getActiveSheet()->setCellValue('O' . $row, $value['addr1']);
            $excel2->getActiveSheet()->setCellValue('Q' . $row, $value['city']);
            $excel2->getActiveSheet()->setCellValue('R' . $row, $value['state']);
            $excel2->getActiveSheet()->setCellValue('S' . $row, $value['FCode']);
            $excel2->getActiveSheet()->setCellValue('T' . $row, $value['zip1']);
            $excel2->getActiveSheet()->setCellValue('W' . $row, $value['boxA']);
            $excel2->getActiveSheet()->setCellValue('X' . $row, $value['box1']);
            $excel2->getActiveSheet()->setCellValue('Y' . $row, $value['box2']);
            $excel2->getActiveSheet()->setCellValue('Z' . $row, $value['box3']);
            $excel2->getActiveSheet()->setCellValue('AA' . $row, $value['box4']);
            $excel2->getActiveSheet()->setCellValue('AB' . $row, $value['box5']);
            $excel2->getActiveSheet()->setCellValue('AC' . $row, $value['box6']);
            $excel2->getActiveSheet()->setCellValue('AD' . $row, $value['box7']);
            $excel2->getActiveSheet()->setCellValue('AE' . $row, $value['box8']);
            $excel2->getActiveSheet()->setCellValue('AG' . $row, $value['box10']);
            $excel2->getActiveSheet()->setCellValue('AH' . $row, $value['box11']);
            $excel2->getActiveSheet()->setCellValue('AI' . $row, $value['box12A']);
            $excel2->getActiveSheet()->setCellValue('AJ' . $row, $value['box12AM']);
            $excel2->getActiveSheet()->setCellValue('AK' . $row, $value['box12B']);
            $excel2->getActiveSheet()->setCellValue('AL' . $row, $value['box12BM']);
            $excel2->getActiveSheet()->setCellValue('AM' . $row, $value['box12C']);
            $excel2->getActiveSheet()->setCellValue('AN' . $row, $value['box12CM']);
            $excel2->getActiveSheet()->setCellValue('AO' . $row, $value['box12D']);
            $excel2->getActiveSheet()->setCellValue('AP' . $row, $value['box12DM']);
            $excel2->getActiveSheet()->setCellValue('AQ' . $row, $value['statutory_retirement_thirdparty']);
            $excel2->getActiveSheet()->setCellValue('AT' . $row, $value['box14A']);
            $excel2->getActiveSheet()->setCellValue('AU' . $row, $value['box14B']);
            $excel2->getActiveSheet()->setCellValue('AV' . $row, $value['box14C']);
            $excel2->getActiveSheet()->setCellValue('AW' . $row, $value['box15A']);
            $excel2->getActiveSheet()->setCellValue('AX' . $row, $value['box21A']);
            $excel2->getActiveSheet()->setCellValue('AY' . $row, $value['box15B']);
            $excel2->getActiveSheet()->setCellValue('AZ' . $row, $value['box16']);
            $excel2->getActiveSheet()->setCellValue('BA' . $row, $value['box17']);
            $excel2->getActiveSheet()->setCellValue('BB' . $row, $value['box18']);
            $excel2->getActiveSheet()->setCellValue('BC' . $row, $value['box19']);
            $excel2->getActiveSheet()->setCellValue('BD' . $row, $value['box20']);
            $excel2->getActiveSheet()->setCellValue('BE' . $row, $value['box21A']);
            $excel2->getActiveSheet()->setCellValue('BG' . $row, $value['box22']);
            $excel2->getActiveSheet()->setCellValue('BH' . $row, $value['box23']);
            $excel2->getActiveSheet()->setCellValue('BJ' . $row, $value['box24']);
            $excel2->getActiveSheet()->setCellValue('BK' . $row, $value['box25']);
            $excel2->getActiveSheet()->setCellValue('BL' . $row, $value['box26']);

            $row++;
        }

        $filename = 'W-2,' . date('Y-m-d') . '.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
        $objWriter->save('php://output', 'w');
        exit;
    }
    public function viewImport()
    {
        isLogin();
        $this->load->view('include/header');
        $this->load->view('importe_excel');
        $this->load->view('include/footer');
    }

    public function importExcel()
    {
        if (isset($_FILES["file"]["name"])) {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            // print_r($object);die;
            foreach ($object->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $c_employee_ein = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $status = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $data[] = array(
                        'c_employee_ein' => $c_employee_ein,
                        'status' => $status,
                    );
                }
            }
            $this->session->set_flashdata('success', 'Data has been import succesfully');
            $this->User_model->insertImprot($data);
            redirect(base_url() . 'user/viewImport');
        }
    }

}
