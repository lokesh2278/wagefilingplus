<?php
defined('BASEPATH') or exit('No direct script access allowed ');
class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->CustID = isset($this->session->get_userdata()['user_details'][0]->CustID) ? $this->session->get_userdata()['user_details'][0]->CustID : '';
        $this->lang->load('user', getSetting('language'));
        $this->load->library('excel');
        $this->load->helper('url');

    }

    /**
     * This function is redirect to users profile page
     * @return Void
     */
    public function index()
    {
        if (isLogin()) {
            redirect(base_url() . 'dashboard', 'refresh');
        }
    }

    /**
     * This function to load dashboard
     * @return Void
     */
    public function dashboard()
    {
        isLogin();
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        //print_r($CustID);die;
        $sqlUser = "SELECT * FROM `users` WHERE CustID= '$CustID'";
        $data = $this->User_model->getQrResult($sqlUser);
        // print_r($data);die;
        $todayDate = date('Y-m-d');
        //echo $todayDate;die;
        if ($data[0]->user_type == 'user') {
            $data[] = '';

            $qr = " SELECT count(*) AS `ia` FROM `Files` WHERE `CustID` = $CustID";
            $data['Total_companies'] = $this->User_model->getQrResult($qr);

            $qr = " SELECT count(*) AS `ia` FROM `Files` WHERE `CustID` = $CustID  AND `created_at` LIKE '%$todayDate%'";
            $data['Today_Registered_data'] = $this->User_model->getQrResult($qr);

            $qr = " SELECT count(*) AS `ia` FROM `Files`   WHERE  `Files`.`is_deleted` = 0 ";
            $data['Active_Users_data'] = $this->User_model->getQrResult($qr);

            $query = "SELECT MONTH(`UDate`) as `months`, '1' AS `mka_sum` FROM  `users`  WHERE YEAR(`UDate`) = '" . date("Y") . "'  ";
            $data['Bar_1'] = $this->User_model->getBarChartData($query);
            $query = "SELECT * FROM `Files` WHERE `CustID` = $CustID ORDER BY `CustID` DESC LIMIT 10 ";
            $data['last_10_registration_list'] = $this->User_model->getQrResult($query);
            $this->load->view('include/header');
            $this->load->view('dashboard', $data);
            $this->load->view('include/footer');
        } else {
            $data[] = '';

            $qr = " SELECT count(*) AS `ia` FROM `users` WHERE  `users`.`user_type` != 'admin'";
            $data['Total_Users_data'] = $this->User_model->getQrResult($qr);

            $qr = " SELECT count(*) AS `ia` FROM `users` WHERE `UDate` LIKE '%$todayDate%'";
            $data['Today_Registered_data'] = $this->User_model->getQrResult($qr);

            $qr = " SELECT count(*) AS `ia` FROM `users`   WHERE  `users`.`status` = 'active' ";
            $data['Active_Users_data'] = $this->User_model->getQrResult($qr);

            $query = "SELECT MONTH(`UDate`) as `months`, '1' AS `mka_sum` FROM  `users`  WHERE YEAR(`UDate`) = '" . date("Y") . "'  ";
            $data['Bar_1'] = $this->User_model->getBarChartData($query);
            $query = "SELECT * FROM  `users`   WHERE  `users`.`user_type` != 'admin' ORDER BY `CustID` DESC LIMIT 10 ";
            $data['last_10_registration_list'] = $this->User_model->getQrResult($query);
            $this->load->view('include/header');
            $this->load->view('dashboard', $data);
            $this->load->view('include/footer');
        }
    }

    /**
     * This function is used to load login view page
     * @return Void
     */
    public function login()
    {
        if (isset($_SESSION['user_details'])) {
            redirect(base_url() . 'dashboard', 'refresh');
        }
        $this->load->view('login');
    }

    /**
     * This function is used to logout user
     * @return Void
     */
    public function logout()
    {
        isLogin();
        $this->session->unset_userdata('user_details');
        redirect(base_url() . 'user/login', 'refresh');
    }

    /**
     * This function is used to registr user
     * @return Void
     */
    public function registration()
    {
        if (isset($_SESSION['user_details'])) {
            redirect(base_url() . 'profile', 'refresh');
        }
        //Check if admin allow to registration for user
        if (getSetting('register_allowed') == 1) {
            if ($this->input->post()) {
                $this->addEdit();
                $art_msg['msg'] = lang('successfully_registered');
                $art_msg['type'] = 'success';
                $this->session->set_userdata('alert_msg', $art_msg);
            } else {
                $this->load->view('register');
            }
        } else {
            $art_msg['msg'] = lang('registration_not_allowed');
            $art_msg['type'] = 'warning';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        }
    }

    /**
     * This function is used for user authentication ( Working in login process )
     * @return Void
     */
    public function authUser($page = '')
    {
        $return = $this->User_model->authUser();
        if (empty($return)) {
            $art_msg['msg'] = lang('invalid_details');
            $art_msg['type'] = 'warning';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        } else {
            if ($return == 'not_varified') {
                $art_msg['msg'] = lang('this_account_is_not_verified_please_contact_to_your_admin');
                $art_msg['type'] = 'danger';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . 'user/login', 'refresh');
            } else {
                $this->session->set_userdata('user_details', $return);
            }
            redirect(base_url() . 'dashboard', 'refresh');
        }
    }

    /**
     * This function is used send mail in forget password
     * @return Void
     */
    public function forgetPassword()
    {
        $page['title'] = lang('forgot_password');
        if ($this->input->post()) {
            $setting = settings();
            $res = $this->User_model->getDataBy('users', $this->input->post('email'), 'email', 1);
            if (isset($res[0]->CustID) && $res[0]->CustID != '') {
                $var_key = $this->getVerificationCode();
                $this->User_model->updateRow('users', 'CustID', $res[0]->CustID, array('var_key' => $var_key));
                $sub = "Reset password";
                $email = $this->input->post('email');
                $data = array(
                    'user_name' => $res[0]->name,
                    'action_url' => base_url(),
                    'sender_name' => $setting['FileName'],
                    'website_name' => $setting['website'],
                    'varification_link' => base_url() . 'user/mailVerify?code=' . $var_key,
                    'url_link' => base_url() . 'user/mailVerify?code=' . $var_key,
                );
                $body = $this->User_model->getTemplate('forgot_password');
                $body = $body->html;
                foreach ($data as $key => $value) {
                    $body = str_replace('{var_' . $key . '}', $value, $body);
                }
                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
                    // content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                if ($emm) {
                    $art_msg['msg'] = lang('to_reset_your_password_link_has_been_sent_to_your_email');
                    $art_msg['type'] = 'info';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url() . 'user/login', 'refresh');
                }
            } else {
                $art_msg['msg'] = lang('this_account_does_not_exist');
                $art_msg['type'] = 'danger';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . "user/forgetpassword");
            }
        } else {
            $this->load->view('forget_password');
        }
    }

    /**
     * This function is used to load view of reset password and varify user too
     * @return : void
     */
    public function mailVerify()
    {
        $return = $this->User_model->mailVerify();
        if ($return) {
            $data['email'] = $return;
            $this->load->view('set_password', $data);
        } else {
            $data['email'] = 'allredyUsed';
            $this->load->view('set_password', $data);
        }
    }

    /**
     * This function is used to reset password in forget password process
     * @return : void
     */
    public function reset_password()
    {
        $return = $this->User_model->ResetPpassword();
        if ($return) {
            $art_msg['msg'] = lang('password_changed_successfully');
            $art_msg['type'] = 'success';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        } else {
            $art_msg['msg'] = lang('unable_to_update_password');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        }
    }

    /**
     * This function is generate hash code for random string
     * @return string
     */
    public function getVerificationCode()
    {
        $pw = $this->randomString();
        return $varificat_key = password_hash($pw, PASSWORD_DEFAULT);
    }

    /**
     * This function is used for show users list
     * @return Void
     */
    public function userlist()
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            $this->load->view('include/header');
            $this->load->view('user_table');
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }
    /**
     * This function is used for show users list
     * @return Void
     */
    public function companylist()
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            $filter_coo = '';
            if (isset($_COOKIE[strtolower('filter_coo')])) {
                $filter_coo = json_decode($_COOKIE[strtolower('filter_coo')]);
            }
            $sel = '';
            if (isset($filter_coo->Files___CustID___filter)) {
                $sel = $filter_coo->Files___CustID___filter;
            }
            $sel = $this->input->get('u');
            $data['f_option'] = $this->get_filter_dropdown_options('name', 'users', 'name', $sel, 'CustID');
            $this->load->view('include/header');
            $this->load->view('company_table', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }

    public function set_filter_cookie()
    {
        if (!empty($this->input->post(strtolower('filter_coo')))) {
            setcookie(strtolower('filter_coo'), json_encode($this->input->post(strtolower('filter_coo'))), time() + (86400 * 30 * 365), "/");
        }
    }

    /**
     * This function is used for show users list
     * @return Void
     */
    public function vendorlist()
    {

        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            $C_sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0  AND `FormType` = 'W-9s' ORDER BY `Files`.`FileSequence` DESC";
            //$data['f1099data'] = $this->User_model->getQrResult($sql);
            $data['c_data'] = $this->User_model->getQrResult($C_sql);
            $this->load->view('include/header');
            $this->load->view('vendor_table', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }

    }

    public function get_filter_dropdown_options($field, $rel_table, $rel_col, $selected, $tb_id)
    {
        $res = $this->User_model->get_filter_dropdown_options($field, $rel_table, $rel_col, $tb_id);
        $option = '';
        if (isset($res) && is_array($res) && !empty($res)) {
            foreach ($res as $key => $value) {
                if ($rel_table != '') {
                    $col = $tb_id;
                    $se = '';
                    if ($selected == $value->$col) {
                        $se = 'selected';
                    }
                    $option .= '<option ' . $se . ' value="' . $value->$col . '">' . $value->$rel_col . '</option>';
                } else {
                    $se = '';
                    if ($selected == $value->$field) {
                        $se = 'selected';
                    }
                    $option .= '<option ' . $se . ' value="' . $value->$field . '">' . $value->$field . '</option>';
                }
            }
        }
        return $option;
    }
    /**
     * This function is used to create datatable in users list page
     * @return Void
     */
    public function userTable()
    {
        isLogin();
        $table = 'users';
        $primaryKey = 'users_id';
        $columns = array(
            array('db' => 'users.CustID AS CustID', 'field' => 'CustID', 'dt' => 0),
            array('db' => 'users.CustID AS CustID', 'field' => 'CustID', 'dt' => 1),
            // array('db' => 'ia_users.status AS status', 'field' => 'status', 'dt' => 2),
            array('db' => 'users.UEmail AS UEmail', 'field' => 'UEmail', 'dt' => 2),
            array('db' => 'users.visible_password AS visible_password', 'field' => 'visible_password', 'dt' => 3),
            // array('db' => 'ia_users.user_type AS user_type', 'field' => 'user_type', 'dt' => 5),
            array('db' => 'users.UDate AS UDate', 'field' => 'UDate', 'dt' => 4),
            array('db' => 'users.Ucompany AS Ucompany', 'field' => 'Ucompany', 'dt' => 5),
            // array('db' => 'ia_users.status AS status', 'field' => 'status', 'dt' => 2),
            array('db' => 'users.Ucontact AS Ucontact', 'field' => 'Ucontact', 'dt' => 6, 'formatter' => function ($d, $row) {
                return '<a href="' . base_url() . 'user/companylist?u=' . $row['CustID'] . '">' . $d . '</a>';
            }),

        );

        $joinQuery = "FROM `" . $table . "` ";
        $cf = getCustomField('user');
        if (is_array($cf) && !empty($cf)) {
            foreach ($cf as $cfkey => $cfvalue) {
                array_push($columns, array('db' => "ia_custom_fields_values_" . $cfkey . ".value AS cfv_" . $cfkey, 'field' => "cfv_" . $cfkey, 'dt' => count($columns)));
                $joinQuery .= " LEFT JOIN `ia_custom_fields_values` AS ia_custom_fields_values_" . $cfkey . "  ON  `users`.`CustID` = `ia_custom_fields_values_" . $cfkey . "`.`rel_crud_id` AND `ia_custom_fields_values_" . $cfkey . "`.`cf_id` =  '" . $cfvalue->custom_fields_id . "' ";
            }
        }
        array_push($columns, array('db' => 'users.CustID AS CustID', 'field' => 'CustID', 'dt' => count($columns)));

        $j = 0;
        if (strpos($joinQuery, 'JOIN') > 0) {
            $j = 1;
        }
        $where = SSP::iaFilter($_GET, $columns, $j);
        if ($where == '') {
            $where .= ' WHERE ';
        } else {
            $where .= ' AND ';
        }
        if (CheckPermission("user", "all_read") || $this->session->get_userdata()['user_details'][0]->user_type == 'admin') {
            $where .= "  `users`.`user_type` != 'admin' AND `users`.`CustID` != $this->CustID";
        } else {
            $where .= "  `users`.`user_type` != 'admin' AND `users`.`CustID` = $this->CustID";
        }

        $limit = SSP::limit($_GET, $columns);
        $order = SSP::iaOrder($_GET, $columns, $j);
        if (trim($order) == 'ORDER BY') {
            $order = '';
        }
        $col = SSP::pluck($columns, 'db', $j);

        $query = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $col) . " " . $joinQuery . " " . $where . " " . $order . " " . $limit . " ";
        $res = $this->db->query($query);
        $res = $res->result_array();
        $recordsTotal = $this->db->select("count('CustID') AS c")->get('users')->row()->c;
        $res = SSP::iaDataOutput($columns, $res, $j);

        $output_arr['draw'] = intval($_GET['draw']);
        $output_arr['recordsTotal'] = intval($recordsTotal);
        $output_arr['recordsFiltered'] = intval($recordsTotal);
        $output_arr['data'] = $res;

        foreach ($output_arr['data'] as $key => $value) {
            $id = $output_arr['data'][$key][count($output_arr['data'][$key]) - 1];
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] = '';
            if (CheckPermission('user', "all_update")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            } else if (CheckPermission('user', "own_update") && (CheckPermission('user', "all_update") != true)) {
                $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
                if ($CustID == $this->CustID) {
                    $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
                }
            }

            if (CheckPermission('user', "all_delete")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';} else if (CheckPermission('user', "own_delete") && (CheckPermission('user', "all_delete") != true)) {
                $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
                if ($CustID == $this->CustID) {
                    $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setId(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';
                }
            }

            $output_arr['data'][$key][1] = '<a href="' . base_url("userLogin") . '/' . $output_arr['data'][$key][1] . '" >Login</a>';

            $output_arr['data'][$key][0] = '<input type="checkbox" id="basic_checkbox_' . $key . '" name="selData" value="' . $output_arr['data'][$key][0] . '"><label for="basic_checkbox_' . $key . '"></label>';
        }
        echo json_encode($output_arr);
    }

    /**
     * This function is used to create datatable in companies list page
     * @return Void
     */
    public function companyTable()
    {
        isLogin();
        $table = 'Files';
        $primaryKey = 'FileSequence';
        $columns = array(
            array('db' => 'Files.FileSequence AS FileSequence', 'field' => 'FileSequence', 'dt' => 0),
            array('db' => 'Files.FileName AS FileName', 'field' => 'FileName', 'dt' => 1, 'formatter' => function ($d, $row) {
                //return '<a href="' . base_url() . 'user/vendorlist?c=' . $row['c_id'] . '">' . $d . '</a>';
                return '<a href="' . base_url() . 'user/viewCompany/' . $row['FileSequence'] . '" title="Login As User"> ' . $d . '</a>';
                //return '<a href="#">' . $d.'</a>';
            }),
            // array('db' => 'Files.c_email AS c_email', 'field' => 'c_email', 'dt' => 2),//TaxYear
            array('db' => 'Files.TaxYear AS TaxYear', 'field' => 'TaxYear', 'dt' => 2), //TaxYear
            
            //array('db' => 'Files.c_ssn_ein AS c_ssn_ein', 'field' => 'c_ssn_ein', 'dt' => 3),
            array('db' => 'Files.c_ssn_ein AS c_ssn_ein', 'field' => 'c_ssn_ein', 'dt' => 3, 'formatter' => function ($d, $row) {
               if($row['FormType'] == '1099-Misc') {
                return $d;
                } else {
                    return $row['c_employee_ein'];

                }

            }),
            //array('db' => 'Files.c_ssn_ein AS c_ssn_ein', 'field' => 'c_ssn_ein', 'dt' => 3),

            array('db' => 'Files.c_telephone AS c_telephone', 'field' => 'c_telephone', 'dt' => 4),
            array('db' => 'Files.FormType AS FormType', 'field' => 'FormType', 'dt' => 5),
            array('db' => 'Files.c_employee_ein AS c_employee_ein', 'field' => 'c_employee_ein', 'dt' => 6),
            array('db' => "DATE_FORMAT(Files.created_at, '%d/%m/%Y') AS created_at", 'field' => 'created_at', 'dt' => 7),

        );

        $joinQuery = "FROM `" . $table . "` ";
        $cf = getCustomField('user');
        if (is_array($cf) && !empty($cf)) {
            foreach ($cf as $cfkey => $cfvalue) {
                array_push($columns, array('db' => "ia_custom_fields_values_" . $cfkey . ".value AS cfv_" . $cfkey, 'field' => "cfv_" . $cfkey, 'dt' => count($columns)));
                $joinQuery .= " LEFT JOIN `ia_custom_fields_values` AS ia_custom_fields_values_" . $cfkey . "  ON  `Files`.`FileSequence` = `ia_custom_fields_values_" . $cfkey . "`.`rel_crud_id` AND `ia_custom_fields_values_" . $cfkey . "`.`cf_id` =  '" . $cfvalue->custom_fields_id . "' ";
            }
        }
        array_push($columns, array('db' => 'Files.FileSequence AS FileSequence', 'field' => 'FileSequence', 'dt' => count($columns)));

        $j = 0;
        if (strpos($joinQuery, 'JOIN') > 0) {
            $j = 1;
        }
        $where = SSP::iaFilter($_GET, $columns, $j);
        if ($this->input->get('Files___CustID___filter') && $this->input->get('Files___CustID___filter') != 'all') {
            $and = '';
            if ($where != '') {
                $and = ' AND ';
            } else {
                $where .= 'WHERE';
            }
            $where .= $and . " `Files`.`CustID` = '" . $this->input->get('Files___CustID___filter') . "' AND `Files`.`is_deleted` = '0' ";
        }

        // $where .= "  `Files`.`is_deleted` = 0";

        $limit = SSP::limit($_GET, $columns);
        $order = SSP::iaOrder($_GET, $columns, $j);
        if (trim($order) == 'ORDER BY') {
            $order = '';
        }
        $col = SSP::pluck($columns, 'db', $j);

        $query = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $col) . " " . $joinQuery . " " . $where . " " . $order . " " . $limit . " ";
        $res = $this->db->query($query);
        $res = $res->result_array();

        $recordsTotal = $this->db->query("SELECT SQL_CALC_FOUND_ROWS count('c_id') AS `c` " . $joinQuery . " " . $where . " " . $order . "")->row();
        if ($recordsTotal) {
            $recordsTotal = $recordsTotal->c;
        }
        $res = SSP::iaDataOutput($columns, $res, $j);

        $output_arr['draw'] = intval($_GET['draw']);
        $output_arr['recordsTotal'] = intval($recordsTotal);
        $output_arr['recordsFiltered'] = intval($recordsTotal);
        $output_arr['data'] = $res;

        foreach ($output_arr['data'] as $key => $value) {
            $id = $output_arr['data'][$key][count($output_arr['data'][$key]) - 1];
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] = '';
            /*if (CheckPermission('user', "all_update")) {
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            } else if (CheckPermission('user', "own_update") && (CheckPermission('user', "all_update") != true)) {
            $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
            if ($CustID == $this->CustID) {
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            }
            }*/
            if (CheckPermission('user', "all_update")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" class="hrefid mClass1" href="' . base_url() . 'user/viewCompany/' . $id . '"  title="View"><i class="material-icons font-20">visibility</i></a>';
            }
            if (CheckPermission('user', "all_delete")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setIdCompany(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';} else if (CheckPermission('user', "own_delete") && (CheckPermission('user', "all_delete") != true)) {
                $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
                if ($CustID == $this->CustID) {
                    $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setIdCompany(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';
                }
            }

            $output_arr['data'][$key][0] = '<input type="checkbox" id="basic_checkbox_' . $key . '" name="selData" value="' . $output_arr['data'][$key][0] . '"><label for="basic_checkbox_' . $key . '"></label>';
        }
        echo json_encode($output_arr);
    }

    /**
     * This function is used to create datatable in vendors list page
     * @return Void
     */
    public function vendorTable()
    {
        isLogin();
        $table = 'ia_vendors';
        $primaryKey = 'v_id';
        $columns = array(
            array('db' => 'ia_vendors.v_id AS v_id', 'field' => 'v_id', 'dt' => 0), array('db' => 'ia_vendors.v_name AS v_name', 'field' => 'v_name', 'dt' => 1),
            array('db' => 'ia_vendors.v_email AS v_email', 'field' => 'v_email', 'dt' => 2),
            array('db' => 'ia_vendors.pdf AS pdf', 'field' => 'pdf', 'dt' => 3),
            array('db' => 'ia_vendors.v_status AS v_status', 'field' => 'v_status', 'dt' => 4),
            array('db' => "DATE_FORMAT(ia_vendors.created_at, '%d/%m/%Y') AS created_at", 'field' => 'created_at', 'dt' => 5),

        );

        $joinQuery = "FROM `" . $table . "` ";
        $cf = getCustomField('user');
        if (is_array($cf) && !empty($cf)) {
            foreach ($cf as $cfkey => $cfvalue) {
                array_push($columns, array('db' => "ia_custom_fields_values_" . $cfkey . ".value AS cfv_" . $cfkey, 'field' => "cfv_" . $cfkey, 'dt' => count($columns)));
                $joinQuery .= " LEFT JOIN `ia_custom_fields_values` AS ia_custom_fields_values_" . $cfkey . "  ON  `ia_vendors`.`v_id` = `ia_custom_fields_values_" . $cfkey . "`.`rel_crud_id` AND `ia_custom_fields_values_" . $cfkey . "`.`cf_id` =  '" . $cfvalue->custom_fields_id . "' ";
            }
        }
        array_push($columns, array('db' => 'ia_vendors.v_id AS v_id', 'field' => 'v_id', 'dt' => count($columns)));

        $j = 0;
        if (strpos($joinQuery, 'JOIN') > 0) {
            $j = 1;
        }
        $where = SSP::iaFilter($_GET, $columns, $j);
        if ($this->input->get('ia_vendors___c_id___filter') && $this->input->get('ia_vendors___c_id___filter') != 'all') {
            $and = '';
            if ($where != '') {
                $and = ' AND ';
            } else {
                $where .= 'WHERE';
            }
            $where .= $and . " `ia_vendors`.`c_id` = '" . $this->input->get('ia_vendors___c_id___filter') . "' ";
        }
        /*if ($where == '') {
        $where .= '  ';
        } else {
        $where .= ' AND ';
        }*/

        // $where .= "  `ia_vendors`.`is_deleted` = 0";

        $limit = SSP::limit($_GET, $columns);
        $order = SSP::iaOrder($_GET, $columns, $j);
        if (trim($order) == 'ORDER BY') {
            $order = '';
        }
        $col = SSP::pluck($columns, 'db', $j);

        $query = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $col) . " " . $joinQuery . " " . $where . " " . $order . " " . $limit . " ";
        $res = $this->db->query($query);
        $res = $res->result_array();
        /*$recordsTotal = $this->db->select("count('v_id') AS c")->get('ia_vendors')->row()->c;*/
        $recordsTotal = $this->db->query("SELECT SQL_CALC_FOUND_ROWS count('v_id') AS `c` " . $joinQuery . " " . $where . " " . $order . "")->row();
        if ($recordsTotal) {
            $recordsTotal = $recordsTotal->c;
        }
        $res = SSP::iaDataOutput($columns, $res, $j);

        $output_arr['draw'] = intval($_GET['draw']);
        $output_arr['recordsTotal'] = intval($recordsTotal);
        $output_arr['recordsFiltered'] = intval($recordsTotal);
        $output_arr['data'] = $res;

        foreach ($output_arr['data'] as $key => $value) {
            if (!empty($output_arr['data'][$key][3])) {
                $output_arr['data'][$key][3] = '<a href="' . $output_arr['data'][$key][3] . '" download><i class="material-icons" style="float: left;">file_download</i><div style="display: inline-block;float: left;">PDF</div></a>';
            }
            $id = $output_arr['data'][$key][count($output_arr['data'][$key]) - 1];
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] = '';
            /*if (CheckPermission('user', "all_update")) {
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            } else if (CheckPermission('user', "own_update") && (CheckPermission('user', "all_update") != true)) {
            $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
            if ($CustID == $this->CustID) {
            $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a id="btnEditRow" class="modalButtonUser mClass"  href="javascript:;" type="button" data-src="' . $id . '" title="Edit"><i class="material-icons font-20">mode_edit</i></a>';
            }
            }*/
            if (CheckPermission('user', "all_update")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" class="mClass" href="' . base_url() . 'user/viewVendor/' . $id . '"  title="View"><i class="material-icons font-20">visibility</i></a>';
            }

            if (CheckPermission('user', "all_delete")) {
                $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setIdVendor(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';} else if (CheckPermission('user', "own_delete") && (CheckPermission('user', "all_delete") != true)) {
                $CustID = getRowByTableColomId($table, $id, 'CustID', 'CustID');
                if ($CustID == $this->CustID) {
                    $output_arr['data'][$key][count($output_arr['data'][$key]) - 1] .= '<a style="cursor:pointer;" data-toggle="modal" class="mClass" onclick="setIdVendor(' . $id . ', \'user\')" data-target="#cnfrm_delete" title="delete"><i class="material-icons col-red font-20">delete</i></a>';
                }
            }

            $output_arr['data'][$key][0] = '<input type="checkbox" id="basic_checkbox_' . $key . '" name="selData" value="' . $output_arr['data'][$key][0] . '"><label for="basic_checkbox_' . $key . '"></label>';
        }
        echo json_encode($output_arr);
    }

    /**
     * This function is Showing users profile
     * @return Void
     */
    public function profile($id = '')
    {
        isLogin();
        if (!isset($id) || $id == '') {
            $id = $this->session->userdata('user_details')[0]->CustID;
        }
        $data['user_data'] = $this->User_model->getUsers($id);
        $this->load->view('include/header');
        $this->load->view('profile', $data);
        $this->load->view('include/footer');
    }

    /**
     * This function is used to show popup of user to add and update
     * @return Void
     */
    public function getModal()
    {
        isLogin();
        if ($this->input->post('id')) {
            $data['userData'] = getDataByid('users', $this->input->post('id'), 'CustID');
            echo $this->load->view('add_user', $data, true);
        } else {
            echo $this->load->view('add_user', '', true);
        }
        exit;
    }

    /**
     * This function is used to add and update users
     * @return Void
     */
    public function addEdit($id = '')
    {
        $data = $this->input->post();
        $profile_pic = 'user.png';
        if ($this->input->post('CustID')) {
            $id = $this->input->post('CustID');
        }
        if (isset($this->session->userdata('user_details')[0]->CustID)) {
            if ($this->input->post('CustID') == $this->session->userdata('user_details')[0]->CustID) {
                $redirect = 'profile';
                $setSession = true;
            } else {
                $redirect = 'user/userlist';
                $setSession = false;
            }
        } else {
            $redirect = 'user/login';
        }
        if ($this->input->post('fileOld')) {
            $newname = $this->input->post('fileOld');
            $profile_pic = $newname;
        } else {
            $profile_pic = 'user.png';
        }
        foreach ($_FILES as $name => $fileInfo) {
            if (!empty($_FILES[$name]['name'])) {
                if ($newname = upload($name)) {
                    $data[$name] = $newname;
                    $profile_pic = $newname;
                }
            } else {
                if ($this->input->post('fileOld')) {
                    $newname = $this->input->post('fileOld');
                    $data[$name] = $newname;
                    $profile_pic = $newname;
                } else {
                    $data[$name] = '';
                    $profile_pic = 'user.png';
                }
            }
        }
        if ($id != '') {
            $data = $this->input->post();
            if ($this->input->post('status') != '') {
                $data['status'] = $this->input->post('status');
            }
            if ($this->input->post('CustID') == 1) {
                $data['user_type'] = 'admin';
                $data['status'] = 'active';
            }
            $checkValue = $this->User_model->checkExists('users', 'email', $this->input->post('email'), $id, 'CustID');
            if ($checkValue == false) {
                $art_msg['msg'] = lang('this_email_already_registered_with_us');
                $art_msg['type'] = 'info';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . 'user/userlist', 'refresh');
            }

            if ($this->input->post('password') != '') {
                if ($this->input->post('currentpassword') != '') {
                    $old_row = getDataByid('users', $this->input->post('CustID'), 'CustID');
                    if (password_verify($this->input->post('currentpassword'), $old_row->password)) {
                        if ($this->input->post('password') == $this->input->post('confirmPassword')) {
                            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                            $data['password'] = $password;
                            $data['visible_password'] = $this->input->post('password');
                        } else {
                            $art_msg['msg'] = lang('password_and_confirm_password_should_be_same');
                            $art_msg['type'] = 'warning';
                            $this->session->set_userdata('alert_msg', $art_msg);
                            redirect(base_url() . $redirect, 'refresh');
                        }
                    } else {
                        $art_msg['msg'] = lang('enter_valid_current_password');
                        $art_msg['type'] = 'warning';
                        $this->session->set_userdata('alert_msg', $art_msg);
                        redirect(base_url() . $redirect, 'refresh');
                    }
                } else {
                    $art_msg['msg'] = lang('current_password_is_required');
                    $art_msg['type'] = 'warning';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url() . $redirect, 'refresh');
                }
            }
            $id = $this->input->post('CustID');
            unset($data['fileOld']);
            unset($data['currentpassword']);
            unset($data['confirmPassword']);
            unset($data['CustID']);
            if (isset($data['edit'])) {
                unset($data['edit']);
            }
            if (isset($data['password']) && $data['password'] == '') {
                unset($data['password']);
            }
            if (isset($data['mkacf'])) {
                $custom_fields = $data['mkacf'];
                unset($data['mkacf']);
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $cfkey => $cfvalue) {
                        $qr = "SELECT * FROM `ia_custom_fields_values` WHERE `rel_crud_id` = '" . $id . "' AND `cf_id` = '" . $cfkey . "'";
                        $cf_data = $this->User_model->getQrResult($qr);
                        if (is_array($cf_data) && !empty($cf_data)) {
                            $d = array(
                                "value" => $custom_fields[$cf_data[0]->cf_id],
                            );
                            $this->User_model->updateRow('ia_custom_fields_values', 'ia_custom_fields_values_id', $cf_data[0]->ia_custom_fields_values_id, $d);
                        } else {
                            $d = array(
                                "rel_crud_id" => $id,
                                "cf_id" => $cfkey,
                                "curd" => 'user',
                                "value" => $cfvalue,
                            );
                            $this->User_model->insertRow('ia_custom_fields_values', $d);
                        }
                    }
                }
            }
            $data['profile_pic'] = $profile_pic;
            foreach ($data as $dkey => $dvalue) {
                if (is_array($dvalue)) {
                    $data[$dkey] = implode(',', $dvalue);
                }
            }
            $this->User_model->updateRow('users', 'CustID', $id, $data);
            $new_data = $this->User_model->getDataBy('users', $id, 'CustID');
            if (isset($setSession) && $setSession == true) {
                $this->session->set_userdata('user_details', $new_data);
            }

            $art_msg['msg'] = lang('your_data_updated_successfully');
            $art_msg['type'] = 'success';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . $redirect, 'refresh');
        } else {
            if ($this->input->post('user_type') != 'admin') {
                $data = $this->input->post();
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $checkValue = $this->User_model->checkExists('users', 'email', $this->input->post('email'));
                if ($checkValue == false) {
                    $art_msg['msg'] = lang('this_email_already_registered_with_us');
                    $art_msg['type'] = 'warning';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url() . 'user/userlist', 'refresh');
                }

                $data['status'] = 'active';
                if (getSetting('admin_approval') == 1) {
                    $data['status'] = 'panding';
                }

                if ($this->input->post('status') != '') {
                    $data['status'] = $this->input->post('status');
                }

                $data['CustID'] = $this->CustID;
                $data['password'] = $password;
                $data['profile_pic'] = $profile_pic;
                $data['is_deleted'] = 0;
                $data['create_date'] = date('Y-m-d');
                if (isset($data['password_confirmation'])) {
                    unset($data['password_confirmation']);
                }
                if (isset($data['call_from'])) {
                    unset($data['call_from']);
                }
                unset($data['submit']);
                $custom_fields = array();
                if (isset($data['mkacf'])) {
                    $custom_fields = $data['mkacf'];
                    unset($data['mkacf']);
                }

                foreach ($data as $dkey => $dvalue) {
                    if (is_array($dvalue)) {
                        $data[$dkey] = implode(',', $dvalue);
                    }
                }
                if ($redirect == 'login') {
                    $data['status'] = 'active';
                    $var_key = $this->getVerificationCode();
                    $data['var_key'] = $var_key;
                }
                $last_id = $this->User_model->insertRow('users', $data);
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $cfkey => $cfvalue) {
                        $d = array(
                            "rel_crud_id" => $last_id,
                            "cf_id" => $cfkey,
                            "curd" => 'user',
                            "value" => $cfvalue,
                        );
                        $this->User_model->insertRow('ia_custom_fields_values', $d);
                    }
                }
                if ($redirect == 'login') {
                    $this->registerMail($data);
                }
                redirect(base_url() . $redirect, 'refresh');
            } else {
                $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
                $art_msg['type'] = 'danger';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . 'user/registration', 'refresh');
            }
        }

    }

    /**
     * This function is used to send verification mail for registeration of ia_users
     * @return Void
     */

    public function registerMail($data1)
    {
        $setting = settings();
        $res = $this->User_model->getDataBy('users', $data1['email'], 'UEmail', 1);
        if (isset($res[0]->CustID) && $res[0]->CustID != '') {
            $sub = "Varify your account";
            $email = $data1['email'];

            $data = array(
                'user_name' => $res[0]->name,
                'varification_link' => base_url() . 'user/registrationMailVerify?code=' . $data1['var_key'],
                'website_name' => $setting['website'],
            );
            $body = $this->User_model->getTemplate('registration');
            $body = $body->html;
            foreach ($data as $key => $value) {
                $body = str_replace('{var_' . $key . '}', $value, $body);
            }

            if ($setting['mail_setting'] == 'php_mailer') {
                $this->load->library("send_mail");
                $emm = $this->send_mail->email($sub, $body, $email, $setting);
            } else {
                // content-type is required when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                $emm = mail($email, $sub, $body, $headers);
            }
            if ($emm) {
                $art_msg['msg'] = lang('successfully_registered_check_your_mail_for_varification');
                $art_msg['type'] = 'success';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url() . 'user/login', 'refresh');
            }
        }

    }

    public function registrationMailVerify()
    {
        $return = $this->User_model->registrationMailVerify();
        if ($return) {
            $art_msg['msg'] = lang('successfully_verified') . '!';
            $art_msg['type'] = 'success';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/login', 'refresh');
        } else {
            $art_msg['msg'] = lang('invalid_link');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'user/registration', 'refresh');
        }
    }

    /**
     * This function is used to delete users
     * @return Void
     */
    public function delete($id)
    {
        isLogin();
        $ids = explode('-', $id);
        foreach ($ids as $id) {
            $this->User_model->delete($id);
        }
        redirect(base_url() . 'user/userlist', 'refresh');
    }

    /**
     * This function is used to delete users
     * @return Void
     */
    public function deletecompany($id)
    {
        isLogin();
        $ids = explode('-', $id);
        foreach ($ids as $id) {
            $this->User_model->deleteCompany($id);
        }
        redirect(base_url() . 'user/companylist', 'refresh');
    }

    /**
     * This function is used to delete users
     * @return Void
     */
    public function deletevendor($id)
    {
        isLogin();
        $ids = explode('-', $id);
        foreach ($ids as $id) {
            $this->User_model->deleteVendor($id);
        }
        redirect(base_url() . 'user/vendorlist', 'refresh');
    }
    /**
     * This function is used to send invitation mail to users for registration
     * @return Void
     */
    public function InvitePeople()
    {
        isLogin();
        if ($this->input->post('emails')) {
            $setting = settings();
            $var_key = $this->randomString();
            $emailArray = explode(',', $this->input->post('emails'));
            $emailArray = array_map('trim', $emailArray);
            $body = $this->User_model->getTemplate('invitation');
            $result['existCount'] = 0;
            $result['seccessCount'] = 0;
            $result['invalidEmailCount'] = 0;
            $result['noTemplate'] = 0;
            if (isset($body->html) && $body->html != '') {
                $body = $body->html;
                foreach ($emailArray as $mailKey => $mailValue) {
                    if (filter_var($mailValue, FILTER_VALIDATE_EMAIL)) {
                        $res = $this->User_model->getDataBy('ia_users', $mailValue, 'email');
                        if (is_array($res) && empty($res)) {
                            $link = (string) '<a href="' . base_url() . 'user/registration?invited=' . $var_key . '">Click here</a>';
                            $data = array('var_user_email' => $mailValue, 'var_inviation_link' => $link);
                            foreach ($data as $key => $value) {
                                $body = str_replace('{' . $key . '}', $value, $body);
                            }
                            if ($setting['mail_setting'] == 'php_mailer') {
                                $this->load->library("send_mail");
                                $emm = $this->send_mail->email('Invitation for registration', $body, $mailValue, $setting);
                            } else {
                                // content-type is required when sending HTML email
                                $headers = "MIME-Version: 1.0" . "\r\n";
                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                                $emm = mail($mailValue, 'Invitation for registration', $body, $headers);
                            }
                            if ($emm) {
                                $darr = array('email' => $mailValue, 'var_key' => $var_key);
                                $this->User_model->insertRow('ia_users', $darr);
                                $result['seccessCount'] += 1;
                            }
                        } else {
                            $result['existCount'] += 1;
                        }
                    } else {
                        $result['invalidEmailCount'] += 1;
                    }
                }
            } else {
                $result['noTemplate'] = lang('no_email_template_availabale') . '.';
            }
        }
        echo json_encode($result);
        exit;
    }

    /**
     * This function is used to Check invitation code for user registration
     * @return TRUE/FALSE
     */
    public function chekInvitation()
    {
        if ($this->input->post('code') && $this->input->post('code') != '') {
            $res = $this->User_model->getDataBy('users', $this->input->post('code'), 'var_key');
            $result = array();
            if (is_array($res) && !empty($res)) {
                $result['email'] = $res[0]->email;
                $result['CustID'] = $res[0]->CustID;
                $result['result'] = 'success';
            } else {
                $art_msg['msg'] = lang('this_code_is_not_valid');
                $art_msg['type'] = 'warning';
                $this->session->set_userdata('alert_msg', $art_msg);
                $result['result'] = 'error';
            }
        }
        echo json_encode($result);
        exit;
    }

    /**
     * This function is used to registr invited user
     * @return Void
     */
    public function registerInvited($id)
    {
        $data = $this->input->post();
        $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        $data['Upassword'] = $password;
        $data['var_key'] = null;
        $data['is_deleted'] = 0;
        $data['status'] = 'active';
        $data['CustID'] = 1;
        if (isset($data['password_confirmation'])) {
            unset($data['password_confirmation']);
        }
        if (isset($data['call_from'])) {
            unset($data['call_from']);
        }
        if (isset($data['submit'])) {
            unset($data['submit']);
        }
        $this->User_model->updateRow('users', 'CustID', $id, $data);
        $art_msg['msg'] = lang('successfully_registered');
        $art_msg['type'] = 'success';
        $this->session->set_userdata('alert_msg', $art_msg);
        redirect(base_url() . 'user/login', 'refresh');
    }

    /**
     * This function is used to check email is alredy exist or not
     * @return TRUE/FALSE
     */
    public function checEmailExist()
    {
        $result = 1;
        $res = $this->User_model->getDataBy('users', $this->input->post('email'), 'email');
        if (!empty($res)) {
            if ($res[0]->CustID != $this->input->post('uId')) {
                $result = 0;
            }
        }
        echo $result;
        exit;
    }

    /**
     * This function is used to Generate a random string
     * @return String
     */
    public function randomString()
    {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = $alpha . $alpha_upper . $numeric;
        $pw = '';
        $chars = str_shuffle($chars);
        $pw = substr($chars, 8, 8);
        return $pw;
    }
    public function f1099data()
    {
        $C_sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0  AND `FormType` = '1099-Misc' ORDER BY `Files`.`FileSequence` DESC";
        $data['c_data'] = $this->User_model->getQrResult($C_sql);
        $this->load->view("include/header");
        $this->load->view('f1099data', $data);
        $this->load->view("include/footer");
    }

    public function misc1099data($id = '', $type = '', $status = '', $from = '', $to = '')
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && empty($_REQUEST['type']) && empty($_REQUEST['status']) && empty($_REQUEST['from']) && empty($_REQUEST['to'])) {
                $id = $_REQUEST['id'];
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {

                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `export_status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `export_status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                $type = $_REQUEST['type'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileType` = '$type' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `export_status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && empty($_REQUEST['id'])) {
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `export_status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && empty($_REQUEST['type']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `export_status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && !empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && empty($_REQUEST['status']) && isset($_REQUEST['type']) && !empty($_REQUEST['id']) && isset($_REQUEST['from']) && !empty($_REQUEST['from']) && isset($_REQUEST['to']) && !empty($_REQUEST['to'])) {
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to') ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && empty($_REQUEST['type']) && empty($_REQUEST['id']) && !empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $status = $_REQUEST['status'];
                $sql = "SELECT * FROM `ia_1099_misc` WHERE `export_status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (empty($_REQUEST['id']) && empty($_REQUEST['type']) && empty($_REQUEST['status']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (!empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to']) && empty($_REQUEST['type']) && empty($_REQUEST['status'])) {
                $id = $_REQUEST['id'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to']) && empty($_REQUEST['type'])) {
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `FileSequence` = '$id' AND `export_status` = '$status' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (empty($_REQUEST['type']) && empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                $status = $_REQUEST['status'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `export_status` = '$status' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && !empty($_REQUEST['type']) && !empty($_REQUEST['type']) && empty($_REQUEST['from']) && empty($_REQUEST['to'])) {

                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_1099_misc` WHERE `export_status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else {

                $sql = "SELECT * FROM `ia_1099_misc` WHERE  `is_deleted` = 0 AND `FileType` IS NOT NULL ORDER BY `FileSequence` DESC";
            }

            $C_sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0 AND `FormType` = '1099-Misc' OR `FormType` = '1099-Div' ORDER BY `Files`.`FileSequence` DESC";

            $data['misc1099'] = $this->User_model->getQrResult($sql);
            $data['c_data'] = $this->User_model->getQrResult($C_sql);

            $this->load->view('include/header');
            $this->load->view('f1099data', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }

    public function div1099data($id = '')
    {
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && empty($_REQUEST['type']) && empty($_REQUEST['status']) && empty($_REQUEST['from']) && empty($_REQUEST['to'])) {
                //print_r('1');die;
                $id = $_REQUEST['id'];
                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                //print_r('2');die;
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
                //die('3');
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && empty($_REQUEST['id'])) {
                //print_r('4');die;
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_1099_div` WHERE `status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                //die('5');
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && empty($_REQUEST['type']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                // die('6');
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && !empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                // die('7');
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && empty($_REQUEST['status']) && isset($_REQUEST['type']) && !empty($_REQUEST['id']) && isset($_REQUEST['from']) && !empty($_REQUEST['from']) && isset($_REQUEST['to']) && !empty($_REQUEST['to'])) {
                //die('id type from to controller');
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];
                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to') ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                //die('7hgfg');
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && empty($_REQUEST['type']) && empty($_REQUEST['id']) && !empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                //die('8');
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $status = $_REQUEST['status'];
                $sql = "SELECT * FROM `ia_1099_div` WHERE `status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (empty($_REQUEST['id']) && empty($_REQUEST['type']) && empty($_REQUEST['status']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                //print_r('9');die;
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_div` WHERE `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (!empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to']) && empty($_REQUEST['type']) && empty($_REQUEST['status'])) {
                //print_r('fgfdgfgdfgdfgdfg');die;
                $id = $_REQUEST['id'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to']) && empty($_REQUEST['type'])) {
                //print_r('10');die;
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_div` WHERE `FileSequence` = '$id' AND `status` = '$status' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (empty($_REQUEST['type']) && empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                //print_r('11');die;
                $status = $_REQUEST['status'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_1099_div` WHERE `status` = '$status' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['status']) && !empty($_REQUEST['status']) && !empty($_REQUEST['type']) && !empty($_REQUEST['type'])) {
                //print_r('12');die;
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_1099_div` WHERE `status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else {

                $sql = "SELECT * FROM `ia_1099_div` WHERE  `is_deleted` = 0 AND `FileType` IS NOT NULL ORDER BY `FileSequence` DESC";
            }

            $C_sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0 AND `FormType` = '1099-Div' OR `FormType` = '1099-Misc' ORDER BY `Files`.`FileSequence` DESC";

            $data['div1099'] = $this->User_model->getQrResult($sql);
            $data['c_data'] = $this->User_model->getQrResult($C_sql);

            $this->load->view('include/header');
            $this->load->view('div1099data', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }
    /**
     * This function is used for show f1099data list
     * @return Void
     */
    public function ajax_list()
    {
        $base_url = base_url();
        $list = $this->User_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $blank = " ";
        foreach ($list as $payee) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $payee->recepient_id_no;
            $row[] = $payee->first_name;
            $row[] = $payee->address;
            $row[] = $payee->FileSequence;
            $row[] = $payee->created_at;
            $row[] = '<a style="cursor:pointer;" class="hrefid mClass1" href="' . $base_url . 'user/viewForm/' . $payee->FileSequence . '/' . $payee->misc_id . '" title="View"><i class="material-icons font-20">visibility</i></a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->User_model->count_all(),
            "recordsFiltered" => $this->User_model->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);

    }

    public function viewForm($cid = '', $id = '')
    {
        isLogin();
        $data['c_details'] = $this->User_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->User_model->getData('ia_1099_misc', $id, 'misc_id');
        $this->load->view('include/header');
        if (!empty($data['c_details'])) {
            $this->load->view('viewForm', $data);
        } else {redirect(base_url() . 'user/f1099data', 'refresh');}
        $this->load->view('include/footer');
    }

    public function viewDivdata($cid = '', $id = '')
    {
        isLogin();
        $data['c_details'] = $this->User_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->User_model->getData('ia_1099_div', $id, 'div_id');
        $this->load->view('include/header');
        if (!empty($data['c_details'])) {
            $this->load->view('view_1099div_data', $data);
        } else {redirect(base_url() . 'user/f1099data', 'refresh');}
        $this->load->view('include/footer');
    }

    public function viewVendor($id = '')
    {
        isLogin();
        //$data['c_details'] = $this->User_model->getData('Files', $cid, 'FileSequence');
        $data['vendors'] = $this->User_model->getDataBy('ia_vendors', $id, 'v_id');
        $this->load->view('include/header');
        if (!empty($data['vendors'])) {
            $this->load->view('viewVendor', $data);
        } else {
            redirect(base_url() . 'user/vendorlist', 'refresh');
        }
        $this->load->view('include/footer');
    }
    /**
     * This function is used for show w2data list
     * @return Void
     */

    public function w2data($id = '', $type = '', $status = '', $from = '', $to = '')
    {
        // print_r($status);
        // die;
        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && empty($_REQUEST['type']) && empty($_REQUEST['status']) && empty($_REQUEST['from']) && empty($_REQUEST['to'])) {
                // print_r('1');die;
                $id = $_REQUEST['id'];
                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                //print_r('2');die;
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
                //die('3');
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && empty($_REQUEST['type']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && empty($_REQUEST['id'])) {
                // print_r('4');die;
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_w2` WHERE `status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                //die('5');
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && empty($_REQUEST['type']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                // die('6');
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && !empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                // die('7');
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && empty($_REQUEST['status']) && isset($_REQUEST['type']) && !empty($_REQUEST['id']) && isset($_REQUEST['from']) && !empty($_REQUEST['from']) && isset($_REQUEST['to']) && !empty($_REQUEST['to'])) {
                //die('id type from to controller');
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];
                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `FileType` = '$type' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to') ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && !empty($_REQUEST['type']) && empty($_REQUEST['id']) && empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                //die('7hgfg');
                $type = $_REQUEST['type'];
                $sql = "SELECT * FROM `ia_w2` WHERE `FileType` = '$type' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['type']) && empty($_REQUEST['type']) && empty($_REQUEST['id']) && !empty($_REQUEST['status']) && empty($_REQUEST['form']) && empty($_REQUEST['to'])) {
                // die('8');
                $id = $_REQUEST['id'];
                $type = $_REQUEST['type'];
                $status = $_REQUEST['status'];
                $sql = "SELECT * FROM `ia_w2` WHERE `status` = '$status' AND `is_deleted` = 0  ORDER BY `FileSequence` DESC";
            } else if (empty($_REQUEST['id']) && empty($_REQUEST['type']) && empty($_REQUEST['status']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                //print_r('9');die;
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_w2` WHERE `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (!empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to']) && empty($_REQUEST['type']) && empty($_REQUEST['status'])) {
                //print_r('fgfdgfgdfgdfgdfg');die;
                $id = $_REQUEST['id'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['status']) && !empty($_REQUEST['status']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to']) && empty($_REQUEST['type'])) {
                //print_r('10');die;
                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_w2` WHERE `FileSequence` = '$id' AND `status` = '$status' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (empty($_REQUEST['type']) && empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status']) && !empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
                //print_r('11');die;
                $status = $_REQUEST['status'];
                $from = $_REQUEST['from'];
                $to = $_REQUEST['to'];

                $sql = "SELECT * FROM `ia_w2` WHERE `status` = '$status' AND `is_deleted` = 0 AND  (`created_at` BETWEEN '$from' AND '$to')  ORDER BY `FileSequence` DESC";
            } else if (isset($_REQUEST['status']) && !empty($_REQUEST['status']) && !empty($_REQUEST['type']) && !empty($_REQUEST['type'])) {
                //print_r('12');die;
                $status = $_REQUEST['status'];
                $type = $_REQUEST['type'];

                $sql = "SELECT * FROM `ia_w2` WHERE `status` = '$status' AND `FileType` = '$type' AND `is_deleted` = 0 ORDER BY `FileSequence` DESC";
            } else {

                $sql = "SELECT * FROM `ia_w2` WHERE  `is_deleted` = 0 AND `FileType` IS NOT NULL ORDER BY `FileSequence` DESC";
            }
            $C_sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0 AND `FormType` = 'W-2' ORDER BY `Files`.`FileSequence` DESC";
            $data['w2data'] = $this->User_model->getQrResult($sql);
            $data['c_data'] = $this->User_model->getQrResult($C_sql);
            // print_r($status);
            // die;

            $this->load->view('include/header');
            $this->load->view('w2data', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }
    public function viewW2($cid = '', $id = '')
    {
        isLogin();
        $data['c_details'] = $this->User_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->User_model->getData('ia_w2', $id, 'w2_id');
        $this->load->view('include/header');
        if (!empty($data['c_details'])) {
            $this->load->view('w2form', $data);
        } else {redirect(base_url() . 'user/f1099data', 'refresh');}
        $this->load->view('include/footer');
    }

    public function bulktinreq($id = '', $status = '')
    {

        isLogin();
        if (CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) {
            if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && empty($_REQUEST['status'])) {
                //die('Sharma');
                $id = $_REQUEST['id'];
                $sql = "SELECT * FROM `bult_tin_request` WHERE `CustID` = '$id' ORDER BY `CustID` DESC";
            } else if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status'])) {

                $id = $_REQUEST['id'];
                $status = $_REQUEST['status'];
                $sql = "SELECT * FROM `bult_tin_request` WHERE `CustID` = '$id' AND `status` = '$status' ORDER BY `CustID` DESC";
            } else if (isset($_REQUEST['status']) && !empty($_REQUEST['status']) && empty($_REQUEST['id'])) {
                $status = $_REQUEST['status'];
                $sql = "SELECT * FROM `bult_tin_request` WHERE `status` = '$status' ORDER BY `CustID` DESC";
            } else {

                $sql = "SELECT * FROM `bult_tin_request` ORDER BY `CustID` DESC";
            }
            $C_sql = "SELECT * FROM `users` WHERE `is_deleted` = 0 ORDER BY `users`.`CustID` DESC";
            $data['w2data'] = $this->User_model->getQrResult($sql);
            $data['c_data'] = $this->User_model->getQrResult($C_sql);

            $this->load->view('include/header');
            $this->load->view('bulk_tin_request', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }

// Bulk Request change status
    public function changeBulkStatus($id = '', $status = '')
    {
        isLogin();
        $this->User_model->updateRow('bult_tin_request', 'bulk_req_id', $id, array('status' => $status));
        redirect(base_url() . 'bulktinreq', 'refresh');
    }

//Download Bult Request file
    public function bulkTinDownload($id)
    {
        isFrontLogin();
        $fileid = encrypt_decrypt('decrypt', $id);
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $UserType = $this->session->userdata('user_details')[0]->user_type;
        if (!empty($fileid)) {
            $this->load->helper('download');
            $fileInfo = $this->User_model->getRows(array('bulk_req_id' => $fileid));
            $file = 'wage_uploads/' . $fileInfo['bulk_tin_file'];
            $uid = $fileInfo['CustID'];
            //print_r($uid);die;
            if ($CustID == $uid || $UserType == 'admin') {
                force_download($file, null);
            } else {
                $this->load->view('access_denide');
            }
        }
    }

    public function downloadTinresponse($id)
    {
        isFrontLogin();
        $fileid = encrypt_decrypt('decrypt', $id);
        //print_r($fileid);die;
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $UserType = $this->session->userdata('user_details')[0]->user_type;
        if (!empty($fileid)) {
            $this->load->helper('download');
            $fileInfo = $this->User_model->getRows(array('bulk_req_id' => $fileid));
            $file = 'wage_uploads/' . $fileInfo['status_file'];
            $uid = $fileInfo['CustID'];

            if ($CustID == $uid || $UserType == 'admin') {
                force_download($file, null);
            } else {
                $this->load->view('access_denide');
            }
        }
    }
    /**
     * This function is used for show view company
     * @return Void
     */
    public function viewCompany($id = '')
    {
        isLogin();

        if ((CheckPermission("user", "own_read") || CheckPermission("user", "all_read")) && !empty($id)) {
            //echo $id;die;
            $data['c_companies'] = $this->User_model->getDataBy('Files', $id, 'FileSequence');
            $this->load->view('include/header');
            $this->load->view('viewCompany', $data);
            $this->load->view('include/footer');
        } else {
            $art_msg['msg'] = lang('you_do_not_have_permission_to_access');
            $art_msg['type'] = 'danger';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'profile', 'refresh');
        }
    }

    public function export1099CSV()
    {
        $id = $_REQUEST['id'];
        $status = $_REQUEST['status'];
        $efile = $_REQUEST['type'];
        $from = $_REQUEST['from'];
        $to = $_REQUEST['to'];
        //print_r($status);die;
        $files = glob('/home/admin/web/wagefilingplus.com/public_html/assets/template/exported_file/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
                //print_r($file);
            }

        }
        $this->load->library("excel");
        $excel2 = new PHPExcel();

        $file = '/home/admin/web/wagefilingplus.com/public_html/assets/template/1099-MISC-Payee.xlsx';
        $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
        $excel2 = $excel2->load($file);
        $excel2->setActiveSheetIndex(0);
        $excel2->getActiveSheet()->setTitle('1099-MISC');

        $this->load->database();
        $this->load->model('User_model');
        if (isset($id) && isset($status) && isset($efile) && isset($from) && isset($to)) {
            //print_r('Lokesh');die;
            $id = $_REQUEST['id'];
            $status = $_REQUEST['status'];
            $efile = $_REQUEST['type'];
            $from = $_REQUEST['from'];
            $to = $_REQUEST['to'];

            $users = $this->User_model->get1099Details($id, $efile, $from, $to, $status);
            foreach ($users as $key => $value) {
                $miscid = $value['misc_id'];
                $this->User_model->updateRow('ia_1099_misc', 'misc_id', $miscid, array('export_status' => 'exported'));
            }

            $datetoday = date('Y-m-d');
            $exportdata = $this->User_model->getData('export_details', $datetoday, 'export_date');
            if (empty($exportdata)) {
                $data = array(
                    "export_date" => $datetoday,
                    "export_no" => 00001,
                );
                $this->User_model->insertRow('export_details', $data);
                $client_sequence_number = '00001';
            } else {
                $no = $exportdata[0]->export_no + 1;
                $this->User_model->updateRow('export_details', 'export_date', $datetoday, array('export_no' => $no));
                $client_sequence_number = str_pad($no, 5, 0, STR_PAD_LEFT);
            }
        }
        // else {
        //     print_r($efile);die;

        //     $users = $this->User_model->get1099Details();
        //     foreach ($users as $key => $value) {
        //         $miscid = $value['misc_id'];
        //         $this->User_model->updateRow('ia_1099_misc', 'misc_id', $miscid, array('export_status' => 'exported'));
        //     }
        // }
        // echo '<pre>';
        // print_r($users);die;
        $count = count($users);
        $row = 12;
        foreach ($users as $value) {
            if ($value['corrected'] == 1) {
                $corrected = 'TRUE';
            } else {
                $corrected = 'FALSE';
            }
            // $num = $value['c_ssn_ein'];
            // $numlength = strlen((string)$num);
            // print_r($numlength); die;
            if ($value['consumer_products_for_resale'] == 1) {
                $consumer_products_for_resale = 'TRUE';
            } else { $consumer_products_for_resale = 'FALSE';}
            $form_type = strtoupper($value['FormType']);
            $c_ssn_ein = preg_replace("/^(\d{3})(\d{2})(\d{4})$/", "$1-$2-$3", $value['c_ssn_ein']);
            $c_telephone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $value['c_telephone']);
            $recepient_id_no = preg_replace("/^(\d{3})(\d{2})(\d{4})$/", "$1-$2-$3", $value['recepient_id_no']);
            // if ($value['c_ssn_ein']) {
            //     $r_zipcode = preg_replace("/^(\d{5})(\d{4})$/", "$1-$2", $value['c_ssn_ein']);
            // }

            $excel2->getActiveSheet()->setCellValue('A' . $row, $form_type);
            $excel2->getActiveSheet()->setCellValue('B' . $row, $c_ssn_ein);
            $excel2->getActiveSheet()->setCellValue('C' . $row, $value['c_name']);
            $excel2->getActiveSheet()->setCellValue('D' . $row, $value['c_address']);
            $excel2->getActiveSheet()->setCellValue('E' . $row, $value['c_address_2']);
            $excel2->getActiveSheet()->setCellValue('F' . $row, $value['c_city']);
            $excel2->getActiveSheet()->setCellValue('G' . $row, $value['c_state']);
            $excel2->getActiveSheet()->setCellValue('H' . $row, $value['c_zip']);
            $excel2->getActiveSheet()->setCellValue('I' . $row, $c_telephone);
            $excel2->getActiveSheet()->setCellValue('J' . $row, $value['c_extention']);
            $excel2->getActiveSheet()->setCellValue('K' . $row, $recepient_id_no);
            $excel2->getActiveSheet()->setCellValue('L' . $row, $value['first_name']);
            $excel2->getActiveSheet()->setCellValue('M' . $row, $value['dba']);
            $excel2->getActiveSheet()->setCellValue('O' . $row, $value['address']);
            $excel2->getActiveSheet()->setCellValue('P' . $row, $value['address']);
            $excel2->getActiveSheet()->setCellValue('Q' . $row, $value['city']);
            $excel2->getActiveSheet()->setCellValue('R' . $row, $value['state']);
            $excel2->getActiveSheet()->setCellValue('S' . $row, 'US');
            $excel2->getActiveSheet()->setCellValue('T' . $row, $value['zipcode']);
            $excel2->getActiveSheet()->setCellValue('W' . $row, $value['account_number']);
            $excel2->getActiveSheet()->setCellValue('X' . $row, 'FALSE');
            $excel2->getActiveSheet()->setCellValue('Y' . $row, 'FALSE');
            $excel2->getActiveSheet()->setCellValue('Z' . $row, $value['rents']);
            $excel2->getActiveSheet()->setCellValue('AA' . $row, $value['royalties']);
            $excel2->getActiveSheet()->setCellValue('AB' . $row, $value['other_income']);
            $excel2->getActiveSheet()->setCellValue('AC' . $row, $value['fed_income_with_held']);
            $excel2->getActiveSheet()->setCellValue('AD' . $row, $value['fishing_boat_proceed']);
            $excel2->getActiveSheet()->setCellValue('AE' . $row, $value['mad_helthcare_pmts']);
            $excel2->getActiveSheet()->setCellValue('AG' . $row, $value['pmts_in_lieu']);
            $excel2->getActiveSheet()->setCellValue('AF' . $row, $value['non_emp_compansation']);
            $excel2->getActiveSheet()->setCellValue('AH' . $row, $consumer_products_for_resale);
            $excel2->getActiveSheet()->setCellValue('AI' . $row, $value['crop_Insurance_proceeds']);
            $excel2->getActiveSheet()->setCellValue('AJ' . $row, $value['excess_golden_par_pmts']);
            $excel2->getActiveSheet()->setCellValue('AK' . $row, $value['gross_paid_to_an_attomey']);
            $excel2->getActiveSheet()->setCellValue('AL' . $row, $value['sec_409a_deferrals']);
            $excel2->getActiveSheet()->setCellValue('AM' . $row, $value['sec_409a_Income']);
            $excel2->getActiveSheet()->setCellValue('AN' . $row, $value['state_tax_with_held']);
            $excel2->getActiveSheet()->setCellValue('AO' . $row, $value['state']);
            $excel2->getActiveSheet()->setCellValue('AP' . $row, $value['payer_state_no']);
            $excel2->getActiveSheet()->setCellValue('AQ' . $row, $value['state_Income']);
            $excel2->getActiveSheet()->setCellValue('AR' . $row, 'FALSE');
            $excel2->getActiveSheet()->setCellValue('AS' . $row, $value['state_tax_with_held']);
            $excel2->getActiveSheet()->setCellValue('AT' . $row, $value['state']);
            $excel2->getActiveSheet()->setCellValue('AU' . $row, $value['payer_state_no']);
            $excel2->getActiveSheet()->setCellValue('AV' . $row, $value['state_Income']);
            $excel2->getActiveSheet()->setCellValue('AW' . $row, 'FALSE');
            $excel2->getActiveSheet()->setCellValue('AX' . $row, $corrected);
            $row++;
        }

        if (empty($efile)) {
            $efile = 'PM';
        }
        $filename = 'Wage_' . $efile . ' ' . $client_sequence_number . ' ' . '1099-MISC' . ' ' . date('d_m_y') . '.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
        ob_end_clean();
        $newpath = '/home/admin/web/wagefilingplus.com/public_html/assets/template/' . $filename;
        $objWriter->save($newpath);
        redirect('https://www.wagefilingplus.com/assets/template/' . $filename);
        exit;
    }
    public function export1099div()
    {
        $id = $_REQUEST['id'];
        $status = $_REQUEST['status'];
        $efile = $_REQUEST['type'];
        $from = $_REQUEST['from'];
        $to = $_REQUEST['to'];
        $files = glob('/home/admin/web/wagefilingplus.com/public_html/assets/template/exported_file/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
                //print_r($file);
            }

        }
        //die;
        $this->load->library("excel");
        $excel2 = new PHPExcel();
        $file = '/home/admin/web/wagefilingplus.com/public_html/assets/template/1099-div-template.xlsx';
        //print_r($file);die;
        $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
        $excel2 = $excel2->load($file);

        $excel2->setActiveSheetIndex(0);
        $excel2->getActiveSheet()->setTitle('1099-DIV');
        $this->load->database();
        $this->load->model('User_model');

        $id = $_REQUEST['id'];
        $status = $_REQUEST['status'];
        $efile = $_REQUEST['type'];
        $from = $_REQUEST['from'];
        $to = $_REQUEST['to'];
        $users = $this->User_model->get1099Div($id, $efile, $status, $from, $to);
        foreach ($users as $key => $value) {
            $divid = $value['div_id'];
            $this->User_model->updateRow('ia_1099_div', 'div_id', $divid, array('status' => 'exported'));
        }

        $datetoday = date('Y-m-d');
        $exportdata = $this->User_model->getData('export_details', $datetoday, 'export_date');
        if (empty($exportdata)) {
            $data = array(
                "export_date" => $datetoday,
                "export_no" => 00001,
            );
            $this->User_model->insertRow('export_details', $data);
            $client_sequence_number = '00001';
        } else {
            $no = $exportdata[0]->export_no + 1;
            $this->User_model->updateRow('export_details', 'export_date', $datetoday, array('export_no' => $no));
            $client_sequence_number = str_pad($no, 5, 0, STR_PAD_LEFT);
        }

        // else {

        //     $users = $this->User_model->get1099Div();
        //     foreach ($users as $key => $value) {
        //         $divid = $value['div_id'];
        //         $this->User_model->updateRow('ia_1099_div', 'div_id', $divid, array('status' => 'exported'));
        //     }
        // }
        $row = 12;
        foreach ($users as $value) {
            $form_type = strtoupper($value['FormType']);
            $c_ssn_ein = preg_replace("/^(\d{3})(\d{2})(\d{4})$/", "$1-$2-$3", $value['c_ssn_ein']);
            $c_telephone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $value['c_telephone']);
            $recepient_id_no = preg_replace("/^(\d{3})(\d{2})(\d{4})$/", "$1-$2-$3", $value['recepient_id_no']);
            $zip_len = strlen($value['c_zip']);
            if ($zip_len > 5) {
                $zip = preg_replace("/^(\d{5})(\d{4})$/", "$1-$2", $value['c_zip']);
            } else {
                $zip = $value['c_zip'];
            }

            $excel2->getActiveSheet()->setCellValue('A' . $row, $form_type);
            $excel2->getActiveSheet()->setCellValue('B' . $row, $c_ssn_ein);
            $excel2->getActiveSheet()->setCellValue('C' . $row, $value['c_name']);
            $excel2->getActiveSheet()->setCellValue('D' . $row, $value['c_address']);
            $excel2->getActiveSheet()->setCellValue('E' . $row, $value['c_address_2']);
            $excel2->getActiveSheet()->setCellValue('F' . $row, $value['c_city']);
            $excel2->getActiveSheet()->setCellValue('G' . $row, $value['c_state']);
            $excel2->getActiveSheet()->setCellValue('H' . $row, $zip);
            $excel2->getActiveSheet()->setCellValue('I' . $row, $c_telephone);
            $excel2->getActiveSheet()->setCellValue('J' . $row, $value['c_extention']);
            $excel2->getActiveSheet()->setCellValue('K' . $row, $recepient_id_no);
            $excel2->getActiveSheet()->setCellValue('L' . $row, $value['first_name']);
            $excel2->getActiveSheet()->setCellValue('M' . $row, $value['last_name']);
            $excel2->getActiveSheet()->setCellValue('O' . $row, $value['address']);
            $excel2->getActiveSheet()->setCellValue('P' . $row, $value['address2']);
            $excel2->getActiveSheet()->setCellValue('Q' . $row, $value['city']);
            $excel2->getActiveSheet()->setCellValue('R' . $row, $value['state']);
            $excel2->getActiveSheet()->setCellValue('S' . $row, 'US');
            $excel2->getActiveSheet()->setCellValue('T' . $row, $value['zipcode']);
            $excel2->getActiveSheet()->setCellValue('W' . $row, $value['account_number']);
            $excel2->getActiveSheet()->setCellValue('Z' . $row, $value['box1a']);
            $excel2->getActiveSheet()->setCellValue('AA' . $row, $value['box1b']);
            $excel2->getActiveSheet()->setCellValue('AB' . $row, $value['box2a']);
            $excel2->getActiveSheet()->setCellValue('AC' . $row, $value['box2b']);
            $excel2->getActiveSheet()->setCellValue('AD' . $row, $value['box2c']);
            $excel2->getActiveSheet()->setCellValue('AE' . $row, $value['box2d']);
            $excel2->getActiveSheet()->setCellValue('AE' . $row, $value['box3']);
            $excel2->getActiveSheet()->setCellValue('AG' . $row, $value['box4']);
            $excel2->getActiveSheet()->setCellValue('AH' . $row, $value['box5']);
            $excel2->getActiveSheet()->setCellValue('AI' . $row, $value['box6']);
            $excel2->getActiveSheet()->setCellValue('AJ' . $row, $value['box7']);
            $excel2->getActiveSheet()->setCellValue('AK' . $row, $value['box8']);
            $excel2->getActiveSheet()->setCellValue('AL' . $row, $value['box9']);
            $excel2->getActiveSheet()->setCellValue('AM' . $row, $value['box10']);
            $excel2->getActiveSheet()->setCellValue('AN' . $row, $value['box11']);
            $excel2->getActiveSheet()->setCellValue('AO' . $row, $value['box12']);
            $excel2->getActiveSheet()->setCellValue('AP' . $row, $value['box13']);
            $excel2->getActiveSheet()->setCellValue('AQ' . $row, $value['box14']);

            $row++;
        }

        $filename = 'Wage_' . 'PM' . ' ' . $client_sequence_number . ' ' . '1099-Div' . ' ' . date('d_m_y') . '.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
        ob_end_clean();
        $newpath = '/home/admin/web/wagefilingplus.com/public_html/assets/template/exported_file/' . $filename;
        $objWriter->save($newpath);
        redirect('https://www.wagefilingplus.com/assets/template/exported_file/' . $filename);
        exit;
        // unlink('https://www.wagefilingplus.com/assets/template/' . $filename);

    }
    public function exportW2CSV($id = '', $efile = '', $status = '', $from = '', $to = '')
    {
        //print_r($_REQUEST['from']);die;
        $files = glob('/home/admin/web/wagefilingplus.com/public_html/assets/template/exported_file/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
                //print_r($file);
            }

        }
        $this->load->library("excel");
        $excel2 = new PHPExcel();

        $file = '/home/admin/web/wagefilingplus.com/public_html/assets/template/W2-payee.xlsx';
        $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
        $excel2 = $excel2->load($file);
        $excel2->setActiveSheetIndex(0);
        $excel2->getActiveSheet()->setTitle('W-2');

        $this->load->database();
        $this->load->model('User_model');

        $id = $_REQUEST['id'];
        $status = $_REQUEST['status'];
        $efile = $_REQUEST['type'];
        $from = $_REQUEST['from'];
        $to = $_REQUEST['to'];
        $users = $this->User_model->getW2Details($id, $efile, $status, $from, $to);
        //print_r($users);die;
        foreach ($users as $key => $value) {
            $miscid = $value['w2_id'];
            $this->User_model->updateRow('ia_w2', 'w2_id', $miscid, array('status' => 'exported'));
        }
        //die;
        $datetoday = date('Y-m-d');
        $exportdata = $this->User_model->getData('export_details', $datetoday, 'export_date');
        if (empty($exportdata)) {
            $data = array(
                "export_date" => $datetoday,
                "export_no" => 00001,
            );
            $this->User_model->insertRow('export_details', $data);
            $client_sequence_number = '00001';
        } else {
            $no = $exportdata[0]->export_no + 1;
            $this->User_model->updateRow('export_details', 'export_date', $datetoday, array('export_no' => $no));
            $client_sequence_number = str_pad($no, 5, 0, STR_PAD_LEFT);
        }

        $row = 12;
        foreach ($users as $value) {
            $c_emp_ein = preg_replace("/^(\d{2})(\d{7})$/", "$1-$2", $value['c_employee_ein']);
            if ($value['SSN']) {
                $c_ssn_ein = preg_replace("/^(\d{3})(\d{2})(\d{4})$/", "$1-$2-$3", $value['SSN']);
            }
            $zip_len = strlen($value['c_zip']);
            if ($zip_len > 5) {
                $zip = preg_replace("/^(\d{5})(\d{4})$/", "$1-$2", $value['c_zip']);
            } else {
                $zip = $value['c_zip'];
            }
            $c_telephone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $value['c_telephone']);

            if ($value['statutory'] > 0) {
                $statutory = 'TRUE';
            } else {
                $statutory = 'FALSE';
            }
            if ($value['retirement'] > 0) {
                $retirement = 'TRUE';
            } else {
                $retirement = 'FALSE';
            }
            if ($value['thirdparty'] > 0) {
                $thirdparty = 'TRUE';
            } else {
                $thirdparty = 'FALSE';
            }

            $excel2->getActiveSheet()->setCellValue('A' . $row, $value['FormType']);
            $excel2->getActiveSheet()->setCellValue('B' . $row, $c_emp_ein);
            $excel2->getActiveSheet()->setCellValue('C' . $row, $value['c_name']);
            $excel2->getActiveSheet()->setCellValue('D' . $row, $value['c_address']);
            $excel2->getActiveSheet()->setCellValue('E' . $row, $value['c_address_2']);
            $excel2->getActiveSheet()->setCellValue('F' . $row, $value['c_city']);
            $excel2->getActiveSheet()->setCellValue('G' . $row, $value['c_state']);
            $excel2->getActiveSheet()->setCellValue('H' . $row, $zip);
            $excel2->getActiveSheet()->setCellValue('I' . $row, $c_telephone);
            $excel2->getActiveSheet()->setCellValue('J' . $row, $value['c_extention']);
            $excel2->getActiveSheet()->setCellValue('K' . $row, $c_ssn_ein);
            $excel2->getActiveSheet()->setCellValue('L' . $row, $value['Fname']);
            $excel2->getActiveSheet()->setCellValue('M' . $row, $value['Lname']);
            $excel2->getActiveSheet()->setCellValue('O' . $row, $value['addr1']);
            $excel2->getActiveSheet()->setCellValue('P' . $row, $value['addr1']);
            $excel2->getActiveSheet()->setCellValue('Q' . $row, $value['city']);
            $excel2->getActiveSheet()->setCellValue('R' . $row, $value['state']);
            $excel2->getActiveSheet()->setCellValue('S' . $row, 'US');
            $excel2->getActiveSheet()->setCellValue('T' . $row, $value['zip1']);
            $excel2->getActiveSheet()->setCellValue('W' . $row, $value['boxA']);
            $excel2->getActiveSheet()->setCellValue('X' . $row, $value['box1']);
            $excel2->getActiveSheet()->setCellValue('Y' . $row, $value['box2']);
            $excel2->getActiveSheet()->setCellValue('Z' . $row, $value['box3']);
            $excel2->getActiveSheet()->setCellValue('AA' . $row, $value['box4']);
            $excel2->getActiveSheet()->setCellValue('AB' . $row, $value['box5']);
            $excel2->getActiveSheet()->setCellValue('AC' . $row, $value['box6']);
            $excel2->getActiveSheet()->setCellValue('AD' . $row, $value['box7']);
            $excel2->getActiveSheet()->setCellValue('AE' . $row, $value['box8']);
            $excel2->getActiveSheet()->setCellValue('AG' . $row, $value['box10']);
            $excel2->getActiveSheet()->setCellValue('AH' . $row, $value['box11']);
            $excel2->getActiveSheet()->setCellValue('AI' . $row, $value['box12A']);
            $excel2->getActiveSheet()->setCellValue('AJ' . $row, $value['box12AM']);
            $excel2->getActiveSheet()->setCellValue('AK' . $row, $value['box12B']);
            $excel2->getActiveSheet()->setCellValue('AL' . $row, $value['box12BM']);
            $excel2->getActiveSheet()->setCellValue('AM' . $row, $value['box12C']);
            $excel2->getActiveSheet()->setCellValue('AN' . $row, $value['box12CM']);
            $excel2->getActiveSheet()->setCellValue('AO' . $row, $value['box12D']);
            $excel2->getActiveSheet()->setCellValue('AP' . $row, $value['box12DM']);
            $excel2->getActiveSheet()->setCellValue('AQ' . $row, $statutory);
            $excel2->getActiveSheet()->setCellValue('AR' . $row, $retirement);
            $excel2->getActiveSheet()->setCellValue('AS' . $row, $thirdparty);
            $excel2->getActiveSheet()->setCellValue('AT' . $row, $value['box14A']);
            $excel2->getActiveSheet()->setCellValue('AU' . $row, $value['box14B']);
            $excel2->getActiveSheet()->setCellValue('AV' . $row, $value['box14C']);
            $excel2->getActiveSheet()->setCellValue('AW' . $row, $value['box15A']);
            $excel2->getActiveSheet()->setCellValue('AX' . $row, $value['box21A']);
            $excel2->getActiveSheet()->setCellValue('AY' . $row, $value['box15B']);
            $excel2->getActiveSheet()->setCellValue('AZ' . $row, $value['box16']);

            $excel2->getActiveSheet()->setCellValue('BA' . $row, $value['box15A']);
            $excel2->getActiveSheet()->setCellValue('BB' . $row, $value['box15B']);
            $excel2->getActiveSheet()->setCellValue('BC' . $row, $value['box16']);
            $excel2->getActiveSheet()->setCellValue('BD' . $row, $value['box17']);
            $excel2->getActiveSheet()->setCellValue('BE' . $row, $value['box18']);
            $excel2->getActiveSheet()->setCellValue('BF' . $row, $value['box19']);
            $excel2->getActiveSheet()->setCellValue('BG' . $row, $value['box20']);

            $excel2->getActiveSheet()->setCellValue('BH' . $row, $value['box21A']);
            $excel2->getActiveSheet()->setCellValue('BJ' . $row, $value['box21B']);
            $excel2->getActiveSheet()->setCellValue('BI' . $row, 'FALSE');
            $excel2->getActiveSheet()->setCellValue('BK' . $row, $value['box22']);
            $excel2->getActiveSheet()->setCellValue('BL' . $row, $value['box23']);
            $excel2->getActiveSheet()->setCellValue('BM' . $row, $value['box24']);
            $excel2->getActiveSheet()->setCellValue('BN' . $row, $value['box25']);
            $excel2->getActiveSheet()->setCellValue('BO' . $row, $value['box26']);

            $excel2->getActiveSheet()->setCellValue('BP' . $row, $value['box21A']);
            $excel2->getActiveSheet()->setCellValue('BQ' . $row, $value['box21B']);
            $excel2->getActiveSheet()->setCellValue('BR' . $row, 'FALSE');
            $excel2->getActiveSheet()->setCellValue('BS' . $row, $value['box22']);
            $excel2->getActiveSheet()->setCellValue('BT' . $row, $value['box23']);
            $excel2->getActiveSheet()->setCellValue('BU' . $row, $value['box24']);
            $excel2->getActiveSheet()->setCellValue('BV' . $row, $value['box25']);
            $excel2->getActiveSheet()->setCellValue('BW' . $row, $value['box26']);

            $excel2->getActiveSheet()->setCellValue('BX' . $row, $value['box21A']);
            $excel2->getActiveSheet()->setCellValue('BY' . $row, $value['box21B']);
            $excel2->getActiveSheet()->setCellValue('BZ' . $row, 'FALSE');
            $excel2->getActiveSheet()->setCellValue('CA' . $row, $value['box22']);
            $excel2->getActiveSheet()->setCellValue('CB' . $row, $value['box23']);
            $excel2->getActiveSheet()->setCellValue('CD' . $row, $value['box24']);
            $excel2->getActiveSheet()->setCellValue('CE' . $row, $value['box25']);
            $excel2->getActiveSheet()->setCellValue('CF' . $row, 'A');

            $excel2->getActiveSheet()->setCellValue('CG' . $row, 'N');
            $excel2->getActiveSheet()->setCellValue('CH' . $row, '12345.6');
            $excel2->getActiveSheet()->setCellValue('CI' . $row, 'FALSE');

            $row++;
        }

        $filename = 'Wage_' . 'PM' . ' ' . $client_sequence_number . ' ' . 'W-2' . ' ' . date('d_m_y') . '.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
        ob_end_clean();
        $newpath = '/home/admin/web/wagefilingplus.com/public_html/assets/template/' . $filename;
        $objWriter->save($newpath);
        redirect('https://www.wagefilingplus.com/assets/template/' . $filename);
        exit;
    }
    public function viewTfpData()
    {
        isLogin();
        $this->load->view('include/header');
        $this->load->view('import_tfp_file');
        $this->load->view('include/footer');
    }
    public function viewImport()
    {
        isLogin();
        $this->load->view('include/header');
        $this->load->view('importe_excel');
        $this->load->view('include/footer');
    }
    public function importUserdata()
    {
        isLogin();
        $this->load->view('include/header');
        $this->load->view('importe_userdata');
        $this->load->view('include/footer');
    }

    public function importStatusfile()
    {
        // die('ddddd');
        $setting = settings();
        if (isset($_FILES['file']["name"])) {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            //print_r($object);die;
            $payee = $this->User_model->getData('Files', 0, 'eFiled');
            $misc_recipients = $this->User_model->getData('ia_1099_misc');
            $div_recipients = $this->User_model->getData('ia_1099_div');
            $w2_recipients = $this->User_model->getData('ia_w2');

            foreach ($object->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for ($row = 3; $row <= $highestRow; $row++) {

                    $recipient_name = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $recipient_id = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $formtype = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $mailed_date = date('d/m/Y', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $efile_status = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $efiled_date = date('d/m/Y', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(9, $row)->getValue()));
                    $irs_file_number = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $irs_accepted_date = date('d/m/Y', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(11, $row)->getValue()));

                    if ($irs_file_number != '') {

                        $payee_tin[] = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        //$ptin = array_unique($payee_tin);
                        //print_r(date('d/m/Y', PHPExcel_Shared_Date::ExcelToPHP($efiled_date)));die;
                        if ($formtype == '1099-MISC') {
                            foreach ($misc_recipients as $key => $value) {
                                $res_no = substr_replace($value->recepient_id_no, 'XXX-XX-', 0, 5);
                                if ($res_no == $recipient_id) {
                                    $this->User_model->updateRow('ia_1099_misc', 'recepient_id_no', $value->recepient_id_no, array('irs_file_number' => $irs_file_number, 'efile_date' => $efiled_date, 'mailed_date' => $mailed_date, 'efile_status' => $efile_status, 'irs_accepted_date' => $irs_accepted_date));

                                }
                            }
                        } elseif ($formtype == '1099-DIV') {
                            foreach ($div_recipients as $key => $value) {
                                $res_no = substr_replace($value->recepient_id_no, 'XXX-XX-', 0, 5);
                                if ($res_no == $recipient_id) {
                                    $this->User_model->updateRow('ia_1099_div', 'recepient_id_no', $value->recepient_id_no, array('irs_file_number' => $irs_file_number, 'efile_date' => $efiled_date, 'mailed_date' => $mailed_date, 'efile_status' => $efile_status, 'irs_accepted_date' => $irs_accepted_date));

                                }
                            }
                        } else {
                            foreach ($w2_recipients as $key => $value) {
                                $res_no = substr_replace($value->recepient_id_no, 'XXX-XX-', 0, 5);
                                if ($res_no == $recipient_id) {
                                    $this->User_model->updateRow('ia_w2', 'SSN', $value->SSN, array('irs_file_number' => $irs_file_number, 'efile_date' => $efiled_date, 'mailed_date' => $mailed_date, 'efile_status' => $efile_status, 'irs_accepted_date' => $irs_accepted_date));

                                }
                            }
                        }
                        // echo '<pre>';
                        // print_r($irs_file_number);die;
                        $this->session->set_flashdata('success', 'Data has been import succesfully');
                    } else {
                        $this->session->set_flashdata('warning', 'Invalid file formate');
                    }
                }

            }

            $ptin = array_unique($payee_tin);
            foreach ($ptin as $key => $value) {
                foreach ($payee as $k => $v) {
                    $payee_no = substr_replace($v->c_ssn_ein, 'XXX-XX-', 0, 5);

                    if ($payee_no == $value) {
                        //print_r($value);die;
                        $payee_staus = $this->User_model->updateRow('Files', 'c_ssn_ein', $v->c_ssn_ein, array('eFiled' => 1,
                            'chDate' => date('Y-m-d')));
                        if ($payee_staus) {
                            $sub = "IRS File number Genrated";
                            $email = $v->c_email;
                            $setting['FileName'] = 'WageFilingPlus.com';
                            $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hi ' . $v->c_name . ',</p><p>Thank you for your recent filing at <a href="' . base_url() . '">WageFilingPlus.com</a>. Your files have been accepted and processed by the IRS and you can view you confirmation number by <a href="' . base_url() . 'login">logging in</a>.</p><br><br>Thank you<br>WageFilingPlus.com, LLC<br><a href="tel:616.325.9332">616.325.9332</a></p></div></div></div>';
                            if ($setting['mail_setting'] == 'php_mailer') {
                                $this->load->library("send_mail");
                                $emm = $this->send_mail->email($sub, $body, $email, $setting);
                            } else {
                                $headers = "MIME-Version: 1.0" . "\r\n";
                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                                $emm = mail($email, $sub, $body, $headers);
                            }
                        }
                    }
                }
                redirect(base_url() . 'user/viewTfpData');
            }

        }
        //die;

    }
    public function importExcel()
    {
        if (isset($_FILES["file"]["name"])) {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            // print_r($object);die;
            foreach ($object->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $FileSequence = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $CustID = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $Paid = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $eFiled = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $status = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $BatchFolder = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $FileName = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $chDate = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $TaxYear = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $FormType = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $FileStr = $worksheet->getCellByColumnAndRow(10, $row)->getValue();

                    if ($worksheet->getCellByColumnAndRow(10, $row)->getValue() instanceof PHPExcel_RichText) {
                        $rows[0]['FileStr'] = $worksheet->getCellByColumnAndRow(10, $row)->getValue()->getPlainText();
                        $file = $rows[0]['FileStr'];
                        $fd = explode('B|A|', $file);
                        // print_r($fd);die;
                        // echo '<pre>';

                        // $filerow = explode("A|", $fd[1]);
                        //print_r($fd);
                        foreach ($fd as $key => $value) {
                            $ddd = array_filter(explode("|", $value));
                            if ($key !== 0) {
                                //print_r($ddd);die;
                                $d = array(

                                    "recepient_id_no" => $ddd[0],
                                    "void" => isset($ddd[1]) ? $ddd[1] : '',
                                    "corrected" => isset($ddd[2]) ? $ddd[2] : '',
                                    "misc_delete" => isset($ddd[3]) ? $ddd[3] : '',
                                    "first_name" => isset($ddd[5]) ? $ddd[5] : '',
                                    //"last_name" => $data['last_name'],
                                    "dba" => isset($ddd[6]) ? $ddd[6] : '',
                                    //"street" => $ddd['street'],
                                    "city" => isset($ddd[8]) ? $ddd[8] : '',
                                    "state" => isset($ddd[9]) ? $ddd[9] : '',
                                    "zipcode" => isset($ddd[10]) ? $ddd[10] : '',
                                    "address" => isset($ddd[7]) ? $ddd[7] : '',
                                    "rents" => isset($ddd[12]) ? $ddd[12] : '',
                                    "royalties" => isset($ddd[13]) ? $ddd[13] : '',
                                    "other_income" => isset($ddd[14]) ? $ddd[14] : '',
                                    "fed_income_with_held" => isset($ddd[15]) ? $ddd[15] : '',
                                    "fishing_boat_proceed" => isset($ddd[16]) ? $ddd[16] : '',
                                    "mad_helthcare_pmts" => isset($ddd[17]) ? $ddd[17] : '',
                                    "non_emp_compansation" => isset($ddd[18]) ? $ddd[18] : '',
                                    "pmts_in_lieu" => isset($ddd[19]) ? $ddd[19] : '',
                                    "crop_Insurance_proceeds" => isset($ddd[20]) ? $ddd[20] : '',
                                    "excess_golden_par_pmts" => isset($ddd[21]) ? $ddd[21] : '',
                                    "gross_paid_to_an_attomey" => isset($ddd[22]) ? $ddd[22] : '',
                                    "sec_409a_deferrals" => isset($ddd[23]) ? $ddd[23] : '',
                                    "sec_409a_Income" => isset($ddd[25]) ? $ddd[25] : '',
                                    "state_tax_with_held" => isset($ddd[27]) ? $ddd[26] : '',
                                    "payer_state_no" => isset($ddd[17]) ? $ddd[17] : '',
                                    "state_Income" => isset($ddd[17]) ? $ddd[17] : '',
                                    "account_number" => isset($ddd[11]) ? $ddd[11] : '',
                                    "foreign_add" => isset($ddd[17]) ? $ddd[17] : '',
                                    "consumer_products_for_resale" => isset($ddd[17]) ? $ddd[17] : '',
                                    "FormType" => '1099-Misc',
                                    "FileSequence" => $FileSequence,
                                    "CustID" => $CustID,
                                    "created_at" => date('Y-m-d h:i:s'),
                                    "export_status" => 'unexported',
                                );
                                $this->User_model->insertRow('ia_1099_misc', $d);
                            }
                        }
                        $fd1 = explode('B|A|', $file);
                        $ddd1 = array_filter(explode("|", $fd1[0]));
                        //$telephone = ;die;
                        $filedata[] = array(
                            'FileSequence' => $FileSequence,
                            'CustID' => $CustID,
                            'Paid' => $Paid,
                            'eFiled' => $eFiled,
                            'status' => $status,
                            'BatchFolder' => $BatchFolder,
                            'FileName' => $FileName,
                            'chDate' => $chDate,
                            'TaxYear' => $TaxYear,
                            'FormType' => "1099-Misc",
                            //'FileStr' => $FileStr,
                            "is_deleted" => 0,
                            "created_at" => date('Y-m-d h:i:s'),
                            'c_ssn_ein' => isset($ddd1[2]) ? $ddd1[2] : '',
                            'c_name' => isset($ddd1[3]) ? $ddd1[3] : '',
                            'c_address' => isset($ddd1[5]) ? $ddd1[5] : '',
                            'c_city' => isset($ddd1[6]) ? $ddd1[6] : '',
                            'c_state' => isset($ddd1[7]) ? $ddd1[7] : '',
                            'c_zip' => isset($ddd1[8]) ? $ddd1[8] : '',
                            'c_telephone' => isset($ddd1[9]) ? str_replace("-", "", $ddd1[9]) : '',
                            'c_filer_name_2' => isset($ddd1[10]) ? $ddd1[10] : '',
                            'c_address_2' => isset($ddd1[11]) ? $ddd1[11] : '',
                        );
                        //print_r($filedata);die;
                    }

                }
            }
            $q = CheckExistorNot('Files', 'FileSequence', $FileSequence);
            //print_r($q);die;
            if (!empty($q)) {
                if (empty($filedata)) {
                    $this->session->set_flashdata('danger', 'Invalid data formate for import');
                } else {
                    $this->session->set_flashdata('danger', 'File is not imported becouse you have import duplicate entry into database for FileSequence: <b>' . $FileSequence . '</b>');
                }
                redirect(base_url() . 'user/viewImport');
            } else {
                if (empty($filedata)) {
                    $this->session->set_flashdata('danger', 'Invalid data formate for import');
                } else {
                    $this->session->set_flashdata('success', 'Data has been import succesfully');
                    $insertid = $this->User_model->insertImprot($filedata);
                }
                redirect(base_url() . 'user/viewImport');
            }
        }
    }
    public function importUser()
    {
        if (isset($_FILES["file"]["name"])) {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            // print_r($object);die;
            foreach ($object->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $CustID = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $UEmail = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $Upassword = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $Udate = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $Ucompany = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $Ucontact = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $Utitle = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $Uaddr = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $Ucity = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $Ustate = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $Uzip = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $Uphone = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $Ufax = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $Ures1 = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                    $userdata[] = array(
                        'CustID' => $CustID,
                        'UEmail' => $UEmail,
                        'Upassword' => password_hash($Upassword, PASSWORD_DEFAULT),
                        'Udate' => $Udate,
                        'Ucompany' => $Ucompany,
                        'Ucontact' => $Ucontact,
                        'Utitle' => $Utitle,
                        'Uaddr' => $Uaddr,
                        'Ucity' => $Ucity,
                        'Ustate' => $Ustate,
                        'Uzip' => $Uzip,
                        'Uphone' => $Uphone,
                        'Ufax' => $Ufax,
                        'Ures1' => $Ures1,
                        'name' => $Ucontact,
                        'visible_password' => $Upassword,
                        'user_type' => 'user',
                        'validate' => '0',
                        'status' => 'active',
                        'is_deleted' => '0',
                    );
                }
            }
            $q = CheckExistorNot('users', 'CustID', $CustID);

            if (empty($q)) {
                if (empty($userdata)) {
                    $this->session->set_flashdata('danger', 'Invalid data formate for import');
                } else {

                    $this->session->set_flashdata('success', 'Data has been import succesfully');
                    $this->User_model->insertImprotUser($userdata);
                }
                redirect(base_url() . 'user/importUserdata');
            } else {
                if (empty($userdata)) {
                    $this->session->set_flashdata('danger', 'Invalid data formate for import');

                } else {
                    $this->session->set_flashdata('danger', 'File is not imported becouse you have import duplicate entry into database');
                }
                redirect(base_url() . 'user/importUserdata');
            }

        }
    }
//Bulk Tin Response Upload
    public function do_upload($id = '')
    {
        // print_r($this->input->post('fileid'));
        // die;
        isFrontLogin();
        $setting = settings();
        $fileid = $this->input->post('fileid');
        $email = $this->input->post('email');
        $status = $this->input->post('status-type');
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $sqlUser = "SELECT * FROM `users` WHERE CustID= '$CustID'";
        $user_d = $this->User_model->getQrResult($sqlUser);

        $config = array(
            'upload_path' => "wage_uploads/",
            'allowed_types' => "xls|xlsx|csv|tab|png|txt",
            'overwrite' => true,
        );

        $this->load->library('upload', $config);
        if ($this->upload->do_upload()) {
            $filedata = $this->upload->data();
            $d = array(
                "status_file" => $filedata['file_name'],
                "status" => $status,
            );
            //print_r($email);die;
            $sql = $this->User_model->updateRow('bult_tin_request', 'bulk_req_id', $fileid, $d);
            if ($sql) {
                require_once './application/libraries/phpmailer/class.phpmailer.php';
                $to = $email;
                $subject = 'Bulk Tin Response';

                // $msg_body = 'Name: ' . $user_d[0]->name . '<br>';
                // $msg_body .= 'Email: ' . $user_d[0]->UEmail . '<br>';
                // $msg_body .= 'Phone: ' . $user_d[0]->Uphone . '<br>';
                // $msg_body .= 'File Name: ' . $filedata['file_name'] . '<br><br>';
                $msg_body .= 'Hello,<br>The results of your TIN checking are complete. <a href="https://wagefilingplus.com/login">Click here</a> to login and download your results from &quot;Bulk Tin Checking&quot; page.<br> If you need to re-issue any W-9 forms based on errors, try our electronic W-9 request. It&rsquo;s fast, easy and no scans or pdf fillers required.<br><br>
                    Thank you,<br><br>
                    WageFilingPlus, LLC<br>
                    <a href="tel:616.325.9332">616.325.9332</a><br>
                    support@WageFilingPlus.com<br>
                    www.WageFilingPlus.com';
                $msg = $msg_body;
                $mail = new PHPMailer();
                $mail->IsSMTP();
                $mail->SMTPDebug = 1;
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = 'ssl';
                $mail->Host = "smtpout.secureserver.net";
                $mail->Port = 465;
                $mail->IsHTML(true);
                $mail->Username = "w9@wagefiling.com";
                $mail->Password = "w9online19";
                //$mail->SetFrom("Wagefilingplus.com");
                $mail->Subject = $subject;
                $mail->Body = $msg;
                $mail->AddAddress($to);
                $mail->Send();
            }
            $art_msg['msg'] = lang('status_updated_successfully');
            $art_msg['type'] = 'success';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'bulktinreq', 'refresh');
        } else {
            //die($this->upload->display_errors());
            $art_msg['msg'] = $this->upload->display_errors();
            $art_msg['type'] = 'warning';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url() . 'bulktinreq', 'refresh');
        }

    }

}
