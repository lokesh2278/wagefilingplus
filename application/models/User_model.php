<?php
class User_model extends CI_Model
{
    public $table = 'ia_1099_misc';
    public $column_order = array(null, 'recepient_id_no', 'address', 'FileSequence', 'created_at'); //set column field database for datatable orderable
    public $column_search = array('FileSequence', 'Filetype', 'created_at', 'export_status'); //set column field database for datatable searchable
    public $order = array('misc_id' => 'asc'); // default order
    private $_batchImport;
    public function __construct()
    {
        parent::__construct();
        $this->CustID = isset($this->session->get_userdata()['user_details'][0]->CustID) ? $this->session->get_userdata()['user_details'][0]->CustID : '0';
    }

    /**
     * This function is used authenticate user at login
     */
    public function authUser()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $this->db->where("is_deleted='0' AND (name='$email' OR UEmail='$email')");
        $result = $this->db->get('users')->result();
        if (!empty($result)) {
            if (password_verify($password, $result[0]->Upassword)) {
                if ($result[0]->status != 'active') {
                    return 'not_varified';
                }
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete user
     * @param: $id - id of user table
     */
    public function delete($id = '')
    {
        $this->db->where('CustID', $id);
        $this->db->delete('users');
    }

    /**
     * This function is used to delete companies
     * @param: $id - id of companies table
     */
    public function deleteCompany($id = '')
    {
        $this->db->where('FormType', $id);
        $this->db->delete('Files');
    }

    /**
     * This function is used to delete vendors
     * @param: $id - id of vendors table
     */
    public function deleteVendor($id = '')
    {
        $this->db->where('v_id', $id);
        $this->db->delete('ia_vendors');
    }

    /**
     * This function is used to load view of reset password and varify user too
     */
    public function mailVerify()
    {
        $ucode = $this->input->get('code');
        $this->db->select('UEmail as e_mail');
        $this->db->from('users');
        $this->db->where('var_key', $ucode);
        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result->e_mail)) {
            return $result->e_mail;
        } else {
            return false;
        }
    }

    /**
     * This function is used Reset password
     */
    public function ResetPpassword()
    {
        $email = $this->input->post('email');
        if ($this->input->post('password_confirmation') == $this->input->post('password')) {
            $npass = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $data['Upassword'] = $npass;
            $data['visible_password'] = $this->input->post('password');
            $data['var_key'] = '';
            return $this->db->update('users', $data, "UEmail = '$email'");
        }
    }

    /**
     * This function is used to select data form table
     */
    public function getDataBy($tableName = '', $value = '', $colum = '', $condition = '')
    {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    public function getRows($params = array())
    {
        $this->db->select('*');
        $this->db->from('bult_tin_request');
        $this->db->order_by('created_at', 'desc');
        if (array_key_exists('bulk_req_id', $params) && !empty($params['bulk_req_id'])) {
            $this->db->where('bulk_req_id', $params['bulk_req_id']);
            //get records
            $query = $this->db->get();
            $result = ($query->num_rows() > 0) ? $query->row_array() : false;
        } else {
            //set start and limit
            if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $this->db->limit($params['limit'], $params['start']);
            } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $this->db->limit($params['limit']);
            }
            //get records
            $query = $this->db->get();
            $result = ($query->num_rows() > 0) ? $query->result_array() : false;
        }
        //return fetched data
        return $result;
    }
    /**
     * This function is used to check user is alredy exist or not
     */
    public function checkExists($table = '', $colom = '', $colomValue = '', $id = '', $id_CheckCol = '')
    {
        $this->db->where($colom, $colomValue);
        if (!empty($id) && !empty($id_CheckCol)) {
            $this->db->where($id_CheckCol . ' !=', $id);
        }
        $res = $this->db->get($table)->row();
        if (!empty($res)) {return false;} else {return true;}
    }

    /**
     * This function is used to get users detail
     */
    public function getUsers($userID = '')
    {
        $this->db->where('is_deleted', '0');
        if (isset($userID) && $userID != '') {
            $this->db->where('CustID', $userID);
        } else if ($this->session->userdata('user_details')[0]->user_type == 'admin') {
            $this->db->where('user_type', 'admin');
        } else {
            $this->db->where('users.CustID !=', '1');
        }
        $result = $this->db->get('users')->result();
        return $result;
    }

    /**
     * This function is used to get email template
     */
    public function getTemplate($code)
    {
        $this->db->where('code', $code);
        return $this->db->get('ia_email_templates')->row();
    }

    /**
     * This function is used to Insert record in table
     */
    public function insertRow($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * This function is used to Update record in table
     */
    public function updateRow($table, $col, $colVal, $data)
    {
        $this->db->where($col, $colVal);
        $this->db->update($table, $data);
        return true;
    }

/*      public function get_list_box_data($qr) {
$exe = $this->db->query($qr);
return $exe->result();
}
 */
    public function getQrResult($qr)
    {
        // echo"<pre>"; print_r($qr); die;
        $exe = $this->db->query($qr);
        return $exe->result();
    }

    public function getQrResults($qr)
    {
        // echo"<pre>"; print_r($qr); die;
        $exe = $this->db->query($qr);
        return $exe->result_array();
    }

    public function getBarChartData($qr)
    {
        $exe = $this->db->query($qr);
        $res = $exe->result();
        $result = [];
        $i = 1;
        while ($i <= 12) {
            $result[$i] = 0;
            foreach ($res as $key => $value) {
                if ($value->months == $i) {
                    //$result[$i] += $value->mka_sum;
                    $result[$i] += (int) str_replace(',', '', $value->mka_sum);
                }
            }
            $i++;
        }
        return implode(',', $result);
    }

    public function registrationMailVerify()
    {
        $ucode = $this->input->get('code');
        if ($ucode == '') {
            return false;
        }
        $this->db->select('UEmail as e_mail');
        $this->db->from('users');
        $this->db->where('var_key', $ucode);
        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result->e_mail)) {
            $data['var_key'] = '';
            $data['status'] = 'active';
            if (getSetting('admin_approval') == 1) {
                $data['status'] = 'varified';
            }
            $this->db->update('users', $data, "UEmail = '$result->e_mail'");
            return $result->e_mail;
        } else {
            return false;
        }
    }

    public function get_filter_dropdown_options($fild, $rel_table, $rel_col, $tb_id)
    {
        if ($rel_table != '') {
            $r = $this->db->select($tb_id)
                ->select($rel_col)
                ->from($rel_table)
                ->get()
                ->result();
        } else {
            $r = $this->db->select($fild)
                ->from('test')
                ->group_by($fild)
                ->get()
                ->result();
        }
        return $r;
    }

    /**
     * This function is used to select data form table
     */
    public function getData($tableName = '', $value = '', $colum = '', $condition = '')
    {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        if ($condition == true) {
            //$CustID = $this->session->userdata('user_details')[0]->CustID;
            // $this->db->where('CustID', $CustID);
        }
        //$this->db->where('is_deleted', 0);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    public function get1099Details($id = '', $efile = '', $from = '', $to = '', $status = '')
    {
        //print_r($efile);die;

        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
        $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : '';
        $efile = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        $from = isset($_REQUEST['from']) ? $_REQUEST['from'] : '';
        $to = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';

        $this->db->select('ia_1099_misc.FormType,Files.c_ssn_ein,Files.c_name,Files.c_address,Files.c_address_2,Files.c_city,Files.c_state,Files.c_zip,Files.c_telephone,Files.c_extention,ia_1099_misc.recepient_id_no,ia_1099_misc.first_name,ia_1099_misc.dba,ia_1099_misc.address,ia_1099_misc.city,ia_1099_misc.state,ia_1099_misc.zipcode,ia_1099_misc.account_number,ia_1099_misc.rents,ia_1099_misc.royalties,ia_1099_misc.other_income,ia_1099_misc.fed_income_with_held,,ia_1099_misc.fishing_boat_proceed,ia_1099_misc.mad_helthcare_pmts,ia_1099_misc.non_emp_compansation,ia_1099_misc.pmts_in_lieu,ia_1099_misc.consumer_products_for_resale,ia_1099_misc.crop_Insurance_proceeds,ia_1099_misc.excess_golden_par_pmts,ia_1099_misc.gross_paid_to_an_attomey,ia_1099_misc.sec_409a_deferrals,ia_1099_misc.sec_409a_Income,ia_1099_misc.state_tax_with_held,ia_1099_misc.payer_state_no,ia_1099_misc.state_Income,ia_1099_misc.misc_id,ia_1099_misc.corrected,ia_1099_misc.misc_id');
        $this->db->from('ia_1099_misc');
        if (!empty($id)) {
            //print_r($id);die;
            $this->db->where('ia_1099_misc.FileSequence', $id);
        }
        if (!empty($efile)) {
            //print_r($efile);die;
            $this->db->where('ia_1099_misc.Filetype', $efile);
        }
        if ((!empty($from)) && (!empty($to))) {
            //print_r($from);die;
            //$this->db->where("'ia_1099_misc.created_at' BETWEEN '$from' AND '$to'");
            $this->db->where('ia_1099_misc.created_at >=', $from);
            $this->db->where('ia_1099_misc.created_at <=', $to);

        }
        if (!empty($status)) {
            //print_r($status);die;
            //$this->db->where("'ia_1099_misc.created_at' BETWEEN '$from' AND '$to'");
            $this->db->where('ia_1099_misc.export_status', $status);

        }
        $this->db->where('ia_1099_misc.FormType', '1099-Misc');
        $this->db->join('Files', 'Files.FileSequence = ia_1099_misc.FileSequence');

        $q = $this->db->get();

        $response = $q->result_array();
        // echo '<pre>';
        // print_r($response);die;
        return $response;
    }
    public function get1099div($id = '', $efile = '', $status = '', $from = '', $to = '')
    {
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
        $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : '';
        $efile = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        $from = isset($_REQUEST['from']) ? $_REQUEST['from'] : '';
        $to = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';

        //print_r($status);die;
        $this->db->select('ia_1099_div.FormType,Files.c_ssn_ein,Files.c_name,Files.c_address,Files.c_address_2,Files.c_city,Files.c_state,Files.c_zip,Files.c_telephone,Files.c_extention,ia_1099_div.recepient_id_no,ia_1099_div.first_name,ia_1099_div.last_name,ia_1099_div.address,ia_1099_div.address2,ia_1099_div.city,ia_1099_div.state,ia_1099_div.zipcode,ia_1099_div.account_number,ia_1099_div.box1a,ia_1099_div.box1b,ia_1099_div.box2a,ia_1099_div.box2b,ia_1099_div.box2c,ia_1099_div.box2d,ia_1099_div.box3,ia_1099_div.box3,ia_1099_div.box4,ia_1099_div.box5,ia_1099_div.box6,ia_1099_div.box7,ia_1099_div.box8,ia_1099_div.box9,ia_1099_div.box10,ia_1099_div.box11,ia_1099_div.box12,ia_1099_div.box13,ia_1099_div.box14,ia_1099_div.div_id,ia_1099_div.Filetype,ia_1099_div.created_at');
        $this->db->from('ia_1099_div');
        if (!empty($id) && isset($id)) {
            $this->db->where('ia_1099_div.FileSequence', $id);
        }
        if (!empty($efile) && isset($efile)) {
            $this->db->where('ia_1099_div.Filetype', $efile);
        }
        if ((!empty($from)) && (!empty($to))) {
            //$this->db->where("'ia_1099_div.created_at' BETWEEN '$from' AND '$to'");
            $this->db->where('ia_1099_div.created_at >=', $from);
            $this->db->where('ia_1099_div.created_at <=', $to);
        }
        if (!empty($status)) {
            //$this->db->where("'ia_1099_div.created_at' BETWEEN '$from' AND '$to'");
            $this->db->where('ia_1099_div.status', $status);
        }
        $this->db->where('ia_1099_div.FormType', '1099-Div');
        $this->db->join('Files', 'Files.FileSequence = ia_1099_div.FileSequence');

        $q = $this->db->get();
        $response = $q->result_array();
        return $response;
    }
    public function getW2Details($id = '', $efile = '', $status = '', $from = '', $to = '')
    {
        //print_r($id . ' ' . $efile . ' ' . $status);
        $id = $_REQUEST['id'];
        $status = $_REQUEST['status'];
        $efile = $_REQUEST['type'];
        $from = $_REQUEST['from'];
        $to = $_REQUEST['to'];
        $this->db->select('ia_w2.FormType,Files.c_employee_ein,Files.c_name,Files.c_address,Files.c_address_2,Files.c_city,Files.c_state,Files.c_zip,Files.c_telephone,Files.c_extention,ia_w2.SSN,ia_w2.Fname,ia_w2.Lname,ia_w2.addr1,ia_w2.city,ia_w2.state,ia_w2.zip1,ia_w2.FCode,ia_w2.boxA,ia_w2.box1,ia_w2.box2,ia_w2.box3,ia_w2.box4,ia_w2.box5,ia_w2.box6,ia_w2.box7,ia_w2.box8,ia_w2.box10,ia_w2.box11,ia_w2.box12A,ia_w2.box12AM,ia_w2.box12B,ia_w2.box12BM,ia_w2.box12C,ia_w2.box12CM,ia_w2.box12D,ia_w2.box12DM,ia_w2.statutory,ia_w2.retirement,ia_w2.thirdparty,ia_w2.box14A,ia_w2.box14B,ia_w2.box14C,ia_w2.box15A,ia_w2.box15B,ia_w2.box16,ia_w2.box17,ia_w2.box18,ia_w2.box19,ia_w2.box20,ia_w2.box21A,ia_w2.box21B,ia_w2.box22,ia_w2.box23,ia_w2.box24,ia_w2.box25,ia_w2.box26,ia_w2.w2_id');
        $this->db->from('ia_w2');
        if (!empty($id)) {
            $this->db->where('ia_w2.FileSequence', $id);
        }
        if (!empty($efile)) {
            $this->db->where('ia_w2.Filetype', $efile);
        }
        if ((!empty($from)) && (!empty($to))) {
            //$this->db->where("'ia_w2.created_at' BETWEEN '$from' AND '$to'");
            $this->db->where('ia_w2.created_at >=', $from);
            $this->db->where('ia_w2.created_at <=', $to);
        }
        if (!empty($status)) {
            //$this->db->where("'ia_w2.created_at' BETWEEN '$from' AND '$to'");
            $this->db->where('ia_w2.status', $status);
        }
        $this->db->where('ia_w2.FormType', 'W-2');
        $this->db->join('Files', 'Files.FileSequence = ia_w2.FileSequence');
        $q = $this->db->get();
        $response = $q->result_array();
        //print_r($response);die;
        return $response;
    }
    public function insertImprotbk($data)
    {
        foreach ($data as $key => $value) {
            $this->db->where('c_employee_ein', $value['c_employee_ein']);
            $this->db->update('Files', array('e_filed' => 0, 'status' => $value['status']));
        }
        //die;
        //$this->db->insert_batch('Files', $data);
    }
    public function insertImprot($filedata)
    {
        $this->db->insert_batch('Files', $filedata);
        return $this->db->insert_id();

    }
    public function insertImprotUser($userdata)
    {
        //$userdata = $this->_batchImport;
        $this->db->insert_batch('users', $userdata);
        //$this->db->insert('users', $data);

    }
    private function _get_datatables_query()
    {
        $this->db->join('Files', 'Files.FileSequence=ia_1099_misc.FileSequence');
        if (!empty($this->input->post('selectcityAmin'))) {
            $this->db->where('ia_1099_misc.FileSequence', $this->input->post('selectcityAmin'));
        }
        if (!empty($this->input->post('selectfiletype'))) {
            $this->db->where('ia_1099_misc.Filetype', $this->input->post('selectfiletype'));
        }
        if (!empty($this->input->post('startdate') && $this->input->post('enddate'))) {
            //$this->db->Where("'sales_date_closed' BETWEEN $startdate AND $enddate");
            $this->db->where('ia_1099_misc.created_at >=', $this->input->post('startdate'));
            $this->db->where('ia_1099_misc.created_at <=', $this->input->post('enddate'));
        }
        if (!empty($this->input->post('status'))) {
            $this->db->where('ia_1099_misc.export_status', $this->input->post('status'));
        }
        $this->db->where('Files.FormType', '1099-Misc');
        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                {
                    $this->db->group_end();
                }

            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
