<?php
class User_model extends CI_Model
{
    private $_batchImport;
    public function __construct()
    {
        parent::__construct();
        $this->CustID = isset($this->session->get_userdata()['user_details'][0]->CustID) ? $this->session->get_userdata()['user_details'][0]->CustID : '0';
    }

    /**
     * This function is used authenticate user at login
     */
    public function authUser()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $this->db->where("is_deleted='0' AND (name='$email' OR UEmail='$email')");
        $result = $this->db->get('users')->result();
        if (!empty($result)) {
            if (password_verify($password, $result[0]->Upassword)) {
                if ($result[0]->status != 'active') {
                    return 'not_varified';
                }
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete user
     * @param: $id - id of user table
     */
    public function delete($id = '')
    {
        $this->db->where('CustID', $id);
        $this->db->delete('users');
    }

    /**
     * This function is used to delete companies
     * @param: $id - id of companies table
     */
    public function deleteCompany($id = '')
    {
        $this->db->where('FormType', $id);
        $this->db->delete('Files');
    }

    /**
     * This function is used to delete vendors
     * @param: $id - id of vendors table
     */
    public function deleteVendor($id = '')
    {
        $this->db->where('v_id', $id);
        $this->db->delete('ia_vendors');
    }

    /**
     * This function is used to load view of reset password and varify user too
     */
    public function mailVerify()
    {
        $ucode = $this->input->get('code');
        $this->db->select('UEmail as e_mail');
        $this->db->from('users');
        $this->db->where('var_key', $ucode);
        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result->e_mail)) {
            return $result->e_mail;
        } else {
            return false;
        }
    }

    /**
     * This function is used Reset password
     */
    public function ResetPpassword()
    {
        $email = $this->input->post('email');
        if ($this->input->post('password_confirmation') == $this->input->post('password')) {
            $npass = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $data['Upassword'] = $npass;
            $data['visible_password'] = $this->input->post('password');
            $data['var_key'] = '';
            return $this->db->update('users', $data, "UEmail = '$email'");
        }
    }

    /**
     * This function is used to select data form table
     */
    public function getDataBy($tableName = '', $value = '', $colum = '', $condition = '')
    {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * This function is used to check user is alredy exist or not
     */
    public function checkExists($table = '', $colom = '', $colomValue = '', $id = '', $id_CheckCol = '')
    {
        $this->db->where($colom, $colomValue);
        if (!empty($id) && !empty($id_CheckCol)) {
            $this->db->where($id_CheckCol . ' !=', $id);
        }
        $res = $this->db->get($table)->row();
        if (!empty($res)) {return false;} else {return true;}
    }

    /**
     * This function is used to get users detail
     */
    public function getUsers($userID = '')
    {
        $this->db->where('is_deleted', '0');
        if (isset($userID) && $userID != '') {
            $this->db->where('CustID', $userID);
        } else if ($this->session->userdata('user_details')[0]->user_type == 'admin') {
            $this->db->where('user_type', 'admin');
        } else {
            $this->db->where('users.CustID !=', '1');
        }
        $result = $this->db->get('users')->result();
        return $result;
    }

    /**
     * This function is used to get email template
     */
    public function getTemplate($code)
    {
        $this->db->where('code', $code);
        return $this->db->get('ia_email_templates')->row();
    }

    /**
     * This function is used to Insert record in table
     */
    public function insertRow($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * This function is used to Update record in table
     */
    public function updateRow($table, $col, $colVal, $data)
    {
        $this->db->where($col, $colVal);
        $this->db->update($table, $data);
        return true;
    }

/*      public function get_list_box_data($qr) {
$exe = $this->db->query($qr);
return $exe->result();
}
 */
    public function getQrResult($qr)
    {
        // echo"<pre>"; print_r($qr); die;
        $exe = $this->db->query($qr);
        return $exe->result();
    }

    public function getQrResults($qr)
    {
        // echo"<pre>"; print_r($qr); die;
        $exe = $this->db->query($qr);
        return $exe->result_array();
    }

    public function getBarChartData($qr)
    {
        $exe = $this->db->query($qr);
        $res = $exe->result();
        $result = [];
        $i = 1;
        while ($i <= 12) {
            $result[$i] = 0;
            foreach ($res as $key => $value) {
                if ($value->months == $i) {
                    //$result[$i] += $value->mka_sum;
                    $result[$i] += (int) str_replace(',', '', $value->mka_sum);
                }
            }
            $i++;
        }
        return implode(',', $result);
    }

    public function registrationMailVerify()
    {
        $ucode = $this->input->get('code');
        if ($ucode == '') {
            return false;
        }
        $this->db->select('UEmail as e_mail');
        $this->db->from('users');
        $this->db->where('var_key', $ucode);
        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result->e_mail)) {
            $data['var_key'] = '';
            $data['status'] = 'active';
            if (getSetting('admin_approval') == 1) {
                $data['status'] = 'varified';
            }
            $this->db->update('users', $data, "UEmail = '$result->e_mail'");
            return $result->e_mail;
        } else {
            return false;
        }
    }

    public function get_filter_dropdown_options($fild, $rel_table, $rel_col, $tb_id)
    {
        if ($rel_table != '') {
            $r = $this->db->select($tb_id)
                ->select($rel_col)
                ->from($rel_table)
                ->get()
                ->result();
        } else {
            $r = $this->db->select($fild)
                ->from('test')
                ->group_by($fild)
                ->get()
                ->result();
        }
        return $r;
    }

    /**
     * This function is used to select data form table
     */
    public function getData($tableName = '', $value = '', $colum = '', $condition = '')
    {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        if ($condition == true) {
            //$CustID = $this->session->userdata('user_details')[0]->CustID;
            // $this->db->where('CustID', $CustID);
        }
        //$this->db->where('is_deleted', 0);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    public function get1099Details()
    {

        //$response = array();

        // // Select record
        // $this->db->select('recepient_id_no,first_name,last_name,address,city,state,zipcode,account_number,fed_income_with_held,state_tax_with_held,c_id');
        // $q = $this->db->get('ia_1099_misc');
        // $response = $q->result_array();
        // return $response;

        $this->db->select('Files.FormType,Files.c_ssn_ein,Files.c_name,Files.c_address,Files.c_address_2,Files.c_city,Files.c_state,Files.c_zip,Files.c_telephone,Files.c_extention,ia_1099_misc.recepient_id_no,ia_1099_misc.first_name,ia_1099_misc.dba,ia_1099_misc.address,ia_1099_misc.city,ia_1099_misc.state,ia_1099_misc.zipcode,ia_1099_misc.account_number,ia_1099_misc.rents,ia_1099_misc.royalties,ia_1099_misc.other_income,ia_1099_misc.fed_income_with_held,,ia_1099_misc.fishing_boat_proceed,ia_1099_misc.mad_helthcare_pmts,ia_1099_misc.non_emp_compansation,ia_1099_misc.pmts_in_lieu,ia_1099_misc.consumer_products_for_resale,ia_1099_misc.crop_Insurance_proceeds,ia_1099_misc.excess_golden_par_pmts,ia_1099_misc.gross_paid_to_an_attomey,ia_1099_misc.sec_409a_deferrals,ia_1099_misc.sec_409a_Income,ia_1099_misc.state_tax_with_held,ia_1099_misc.payer_state_no,ia_1099_misc.state_Income');
        $this->db->from('ia_1099_misc');
        $this->db->join('Files', 'Files.FileSequence = ia_1099_misc.FileSequence');

        $q = $this->db->get();
        $response = $q->result_array();
        return $response;
    }

    public function getW2Details()
    {

        $this->db->select('Files.FormType,Files.c_employee_ein,Files.c_name,Files.c_address,Files.c_address_2,Files.c_city,Files.c_state,Files.c_zip,Files.c_telephone,Files.c_extention,ia_w2.SSN,ia_w2.Fname,ia_w2.Lname,ia_w2.addr1,ia_w2.city,ia_w2.state,ia_w2.zip1,ia_w2.FCode,ia_w2.boxA,ia_w2.box1,ia_w2.box2,ia_w2.box3,ia_w2.box4,ia_w2.box5,ia_w2.box6,ia_w2.box7,ia_w2.box8,ia_w2.box10,ia_w2.box11,ia_w2.box12A,ia_w2.box12AM,ia_w2.box12B,ia_w2.box12BM,ia_w2.box12C,ia_w2.box12CM,ia_w2.box12D,ia_w2.box12DM,ia_w2.statutory_retirement_thirdparty,ia_w2.box14A,ia_w2.box14B,ia_w2.box14C,ia_w2.box15A,ia_w2.box15B,ia_w2.box16,ia_w2.box17,ia_w2.box18,ia_w2.box19,ia_w2.box20,ia_w2.box21A,ia_w2.box22,ia_w2.box23,ia_w2.box24,ia_w2.box25,ia_w2.box26');
        $this->db->from('ia_w2');
        $this->db->join('Files', 'Files.FormType = ia_w2.FormType');

        $q = $this->db->get();
        $response = $q->result_array();
        return $response;
    }
    public function insertImprotbk($data)
    {
        foreach ($data as $key => $value) {
            $this->db->where('c_employee_ein', $value['c_employee_ein']);
            $this->db->update('Files', array('e_filed' => 0, 'status' => $value['status']));
        }
        //die;
        //$this->db->insert_batch('Files', $data);
    }
    public function insertImprot($filedata)
    {
        $this->db->insert_batch('Files', $filedata);
        return $this->db->insert_id();

    }
    public function insertImprotUser($userdata)
    {
        //$userdata = $this->_batchImport;
        $this->db->insert_batch('users', $userdata);
        //$this->db->insert('users', $data);

    }
}
