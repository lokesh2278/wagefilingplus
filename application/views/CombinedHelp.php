
<html>
<!-- Filename CombinedHelp.asp-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Combined Federal State Filing Program - Help</title>
</head>

<body onload="window.width=600; window.height=500" style="font-family: Verdana; font-size: x-small;">

<span Style="font-family: Verdana; font-size: medium;">
Automatic Filing of 1099 Forms to the States
</span>
<br>
<br>
We recommend clicking <b>YES.</b> There is no extra cost. The IRS will 
automatically forward a copy of your 1099s to the 
<b>&quot;participating states&quot;</b> shown below. (This happens after we 
e-File your 1099s to the IRS). The state is determined based on the payee's 
address. The IRS will forward both original and corrected 
1099 forms. This IRS program is called the <b>&quot;Combined Federal/State Filing Program&quot; 
or&nbsp; CFSF.</b><br>
<br>
We recommend that you select <b>YES</b> to this option for the following 
reasons: <br>
<ul>
<li>If your payees reside in California, for example, and you <b>DO NOT</b> choose this 
option, you could be liable for a $100 penalty from the State of California. 
Other states may have similar penalties.</li><li>We believe this program is the best and easiest way to comply with the states 
who wish to receive the 1099 forms.
<li>If you check YES, and all payees are in a state with no state income tax, such as Texas, nothing happens, and there is no 
problem.<li>If you have payees in some participating and non-participating states, only 
those forms in the participating states will be forwarded.</ul> <B>The following 34 states participate in the CFSF: </B>&nbsp;<P><B>Alabama, Arizona, Arkansas, California, Colorado, Connecticut, Delaware, DC, Georgia, 
Hawaii, Idaho, Indiana, Iowa, Kansas, Louisiana, Maine, Maryland, Massachusetts, 
Michigan, Minnesota, 
Mississippi, Missouri, Montana, Nebraska, New Jersey, New Mexico, North Carolina, 
North Dakota, Ohio, South Carolina, Utah, Vermont, Virginia, Wisconsin.</B> 
<br><br>
Problems:<br>
<br>
State law is continually changing. You should check the state's web site to be 
sure. We believe <b>Illinois, Kentucky, New York, West Virginia, and Rhode 
Island,</b> who are not participants, probably do <b>not want copies</b> of 
1099-Miscellaneous forms, unless there is withholding in Box 4. Because state 
laws are changing, check states' web sites. For other of 1099 forms, such as 
1099-R, most states may have additional requirements.<br>
<br>
<b>Oregon</b> is a problem and <b>does require</b>, special, direct e-Filing if 
you have more than 10 forms. The Oregon web site is:&nbsp;&nbsp;
<a href="http://www.oregon.gov/DOR/BUS/Pages/iwire-income-wage-information-return-eservices.aspx">
http://www.oregon.gov/DOR/BUS/Pages/iwire-income-wage-information-return-eservices.aspx</a> <br>
<br>
<b>Massachusetts</b> may require direct e-Filing if you have more that a certain 
number of forms. The Massachusetts web site is:
<a href="http://www.mass.gov/dor/businesses/filing-and-reporting/form-1099-reporting.html">
http://www.mass.gov/dor/businesses/filing-and-reporting/form-1099-reporting.html</a> <br>
<br>
If you have payees in states states which require special, custom e-Filing, 
please call us for a quote, but keep in mind that custom/special state 
compliance/filing takes time, labor and special work and can be expensive. So, 
check the state's web site, especially, Oregon and Massachusetts, to see if you 
can send Copy 2 for the state (printed from WageFiling.com) to the state on 
paper.<br>
<br>
State laws change each year, and some states have special filing requirements unique to that state.
Another problem is conflicting rules: For example,
The IRS says Massachusetts participates in the CF/SF, but their web site said you must 
also e-File to Massachusetts 
directly and file special Massachusetts forms, when over a certain number of forms. 
This is an obvious conflict.<br>
<br>
In addition to the States, there may be certain <b>cities, towns, districts</b>, 
and taxing jurisdictions with income taxes that require 1099 reporting. These 
are NOT covered by WageFiling.com and we do not e-File to these jurisdictions.&nbsp; <br>
<br>
Please remember, that you the customer, are always responsible for compliance with 
all Federal, State, local or other laws and rules. This includes making sure your forms 
are delivered to the states when they are due, using the delivery method the 
states require (either electronic or paper). WageFiling.com and/or WageFiling, 
LLC, it's employees, owners, and assigns are never responsible for penalties, or other sanctions you may receive 
for failure to comply with any 
jurisdiction, whether Federal, State or Local, and there is no guarantee or 
promise that by clicking YES to the CFSF program you will be in compliance with 
some states.</p>
</body>
</html>