<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h4>1099-Misc Payee Form Data</h4>
            <ul class="header-dropdown m-r--5">
            <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
            </div>
          <!-- /.box-header -->
          <body>
<script type="text/javascript">
              $(document).ready(function(){
                $("#clear").click(function(){
                    $(".textEntry").val(" ");
                    $('.check').prop('checked', false);
                });
            });
            var FormChanged = false;

            function removechar(str,c)
            { // this is good
            var str; var ln; var pos;
            ln=str.length-1; pos=str.indexOf(c);
            if (pos>=0) {str=str.substr(0,pos)+str.substr(pos+1,ln-pos);}
            return str
            }

            function valssn()
            {
             var fm; var x; var i; var c; var str; var n; var err;
              fm=document.RcmForm;            // get form for conveninece
               str=fm.recepient_id_no.value;
            err=0; n=0;
            for (i=0; i<=str.length-1; i++) {c=str.charAt(i); if (c=="-"){n++}}
            if (n>2) {alert("Too many dashes"); return false;}
            if (n==1) {if (str.charAt(2)!="-") {err++} }
            if (n==2) {
               if (str.charAt(3)!="-") {err++}
               if (str.charAt(6)!="-") {err++}
                     }
            if (err>0)
             {alert("If using optional dashes, format as xx-xxxxxxx or xxx-xx-xxxx");return false;}

            // Pull out the dashes, the check the length...

               str=removechar(str,'-');   str=removechar(str,'-'); // remove up to two dashes...
               i=str.length;
               if ( i!=9 ) {alert('ID Number not 9 digits'); return false;}
            // Now check for all numerics
            //  str=fm.ssn.value;
            //for (i=0; i<=fm.ssn.value.length-1; i++)
              for (i=0; i<=8; i++)
              {
              c=str.charAt(i);    // like pascal c:=str[i];
            //  if (c<'-' || c>'9')  {alert("SSN contains non-numerics"); return false;}

              if (isNaN(c) )  {alert("TaxID contains non-numerics"); return false;}
              if (c==" ") {alert("TaxID contains blanks"); return false;}
              }

              if (str=='123456789') {alert('123456789 is an invalid TaxID number'); return false;}

            // check to see if this is all the same number like all zeros.
            // for j:=2 to 9 do begin if ssn[1]<>ssn[j] then goto 10; end;
              for (i=1; i<=8; i++)
              {
               if (str.charAt(i)!=str.charAt(0)) break;
               if (i==8) {alert('TaxID cannot be all the same numbers '+str); return false;}
              }
              return true          // true and false are reserved words.
            }  // end funcion



            function valzip()
              {
               var fm; var ln; var c;
               if (document.RcmForm.foreign_add.checked) {return true} // exit if foreign. No Checking
               fm=document.RcmForm
               ln=fm.zipcode.value.length
               if (ln!=5 && ln!=9 && ln!=10)
               {   alert("Wrong length zipcode");    return false;    }

               // make sure all digits, no alpha
               str=fm.zipcode.value
              for (i=0; i<=ln-1; i++)
               {
                c=str.substr(i,1);
                if (c<"-" || c>"9") {alert("zipcode contains non-numerics"); return false;}
               }
               return true
              } // end valzip

            function valstate()
            {
             var fm; var str; var ln; var lst; var found;
              if (document.RcmForm.foreign_add.checked) {return true} // exit if foreign. No Checking
              fm=document.RcmForm;    ln=fm.state.value.length;
              if (ln!=2) {alert("State Code not 2 chars"); return false}
              str=fm.state.value.toUpperCase()
              document.RcmForm.state.value=str
              found=false
              // now look up state
              for (i=0; i<=59-1; i++)
              {
              lst=document.forms[0].stateList.options[i].text; // get list box items i=0,1... from list
              lst=lst.substr(0,2) // get left part...
              if (lst==str) {found=true; break;}
              }
              if (found==false) {alert("Invalid state code"); }
              return found; // returns either true of false...
            } // end function


            function put()
            {var txt;
            txt=document.forms[0].stateList.options[document.forms[0].stateList.selectedIndex].text
            txt=txt.substr(0,2);
            document.forms[0].state.value=txt
            }
            var sum = '';
            function valmoney2(str)
            { if (str=="") {return true;}
            //  str=removechar(str,","); // remove commas a common thing.
            //  str=removechar(str,"$"); // remove dollar signs.
              if (isNaN(str)) {alert("Value in a money box contains comma, letters or other Non-Numerics:  " + str); return false;}
              sum=sum+str; // sum all money for later to see if they are all zeros..
              return true;
            }

            function valall()
            { var i;
            // if (FormChanged==false) {window.location.href='dsprecs.asp'; return false;} // no keydown dont post
             sum = '';

             if (valssn()==false)   {return false}
             if (valzip()==false)   {return false}
             if (valstate()==false) {return false}

             i = document.RcmForm.first_name.value.length;
             if (i>40) {alert("Name1 more than 40 characters"); return false;}
             if (i<1)  {alert("Name1 field empty"); return false;}

             i = document.RcmForm.dba.value.length;
             if (i>40) {alert("Dba, partners etc. more than 40 characters"); return false;}

             i = document.RcmForm.address.value.length;
             if (i>40) {alert("Address more than 40 characters"); return false;}
             if (i<1)  {alert("Address field empty"); return false;}

             i = document.RcmForm.city.value.length;
             if (i>40) {alert("City more than 40 characters"); return false;}
             if (i<1)  {alert("City field empty"); return false;}

             if (valmoney2(document.RcmForm.rents.value)==false) {return false;}
             if (valmoney2(document.RcmForm.royalties.value)==false) {return false;}
             if (valmoney2(document.RcmForm.other_income.value)==false) {return false;}
             if (valmoney2(document.RcmForm.fed_income_with_held.value)==false) {return false;}
             if (valmoney2(document.RcmForm.fishing_boat_proceed.value)==false) {return false;}
             // if (valmoney2(document.RcmForm.box5.value)==false) {return false;}
             if (valmoney2(document.RcmForm.mad_helthcare_pmts.value)==false) {return false;}
             if (valmoney2(document.RcmForm.non_emp_compansation.value)==false) {return false;}
             if (valmoney2(document.RcmForm.pmts_in_lieu.value)==false) {return false;}
             // box9 not a money amount..
             if (valmoney2(document.RcmForm.crop_Insurance_proceeds.value)==false) {return false;}
             // Boxes 11 and 12 are greyed out on the form may come back in future.
             if (valmoney2(document.RcmForm.excess_golden_par_pmts.value)==false) {return false;}
             if (valmoney2(document.RcmForm.gross_paid_to_an_attomey.value)==false) {return false;}
             if (valmoney2(document.RcmForm.sec_409a_deferrals.value)==false) {return false;}
             if (valmoney2(document.RcmForm.sec_409a_Income.value)==false) {return false;}
             if (valmoney2(document.RcmForm.state_tax_with_held.value)==false) {return false;}  // box16 state incometax withheld money.
             // Box 17 state payers no not money..
             if (valmoney2(document.RcmForm.state_Income.value)==false) {return false;}  // box18 is state income money.

             if (document.RcmForm.misc_delete.checked==true)
             {
             var r=confirm('This record will be deleted if you continue!');
             if (r==false)
                {document.RcmForm.misc_delete.value="0"
                 return false;
                }
             document.RcmForm.misc_delete.value="13"
             }
              if (sum=='') {alert("No money in any box"); return false;}
               return true
            } // end function

            function ProgramI()
            {
              document.RcmForm.recepient_id_no.focus()
            }

            function consumer_products_for_resale()
            {// When they click the box9, we set the return Html value=""
              var b9;
              b9=document.RcmForm.consumer_products_for_resale;
              if (b9.checked==true) {b9.value='1'}    // this works great...
              else {b9.value=''}
            }

            function voidclick()
            {
              var vbox;
              vbox=document.RcmForm.void;
              if (vbox.checked==true) {vbox.value='1'}
              else {vbox.value=''}
            }

            function help14()
            {
            myWindow=window.open('<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/help.html','','width=600,height=400,scrollbars=yes');
            myWindow.focus();
            }

            function corclick()
            {
              var cbox;
              cbox=document.RcmForm.corrected;
              if (cbox.checked==true) {
              cbox.value='1'
              var r=confirm('Do not check this box unless:\n - You have carefully read Topic No. 14 in HELP!\n Read Help NOW???');
              if (r==true) {help14();}
              }
              else
              {cbox.value=''}
            }

            function forclick()
            {
              var fbox;
              fbox=document.RcmForm.foreign_add;
              if (fbox.checked==true) {fbox.value='1'}
              else {fbox.value=''}
            }
          </script>

<div class="page">
        <div class="row">
            <div class="col-xs-12">
                <div class="containerApp">
<form name="RcmForm" method="post"  action="addform"
      onsubmit="return valall();"
      onKeydown="FormChanged=true;">    <div>
        <div class="maxwidth1320">
            <div class="clear"></div>
            <div class="clear spacer30 hidden-xs"></div>
            <div class="nameDiv" style="width: 100%;"  id="DivDocumentName">
                            <table class="displayTable">
                                <tbody>
                                <tr align="center">
                                    <td><input style="margin-right: 5px;" type="checkbox" class="check" name="void" value="1"<?php echo !empty($v_details) && !empty($v_details[0]->void) ? 'checked' : ''; ?>  onClick="voidclick()" /><label> Void</label></td>
                                    <td><input style="margin-right: 5px;" type="checkbox" class="check" name="corrected" value="1" <?php echo !empty($v_details) && !empty($v_details[0]->corrected) ? 'checked' : ''; ?>  onClick="corclick()" /><label> Corrected</label></td>
                                    <td><input style="margin-right: 5px;" class="check" type="checkbox" value="3" name="misc_delete" <?php if (isset($v_details[0]->misc_delete) && $v_details[0]->misc_delete == '0') {echo '';} else {echo 'disabled';}?>><label> Delete</label></td>
                                </tr>
                            </tbody></table>
                        </div>
            <div class="clearfix"></div>

            <div class="clear spacer10"></div>
            <div class="clear spacer10"></div>
            <div class="formFill form1099MISC">
                <div class="row2  borderB1024N borderL borderL980N disFlex1024N floatL ">
                    <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 formRightHalfTab">
                        <div class="">
                            <div class="row border-bottom">
                                <div style="font-size: 17px !important;" class="col-lg-12 col-md-6 col-sm-12 mis_income floatR">
                                    <h3 class="red">Miscellaneous Income</h3>
                                </div>
                            </div>
                            <div class="row formNY show640 taC borderT pad0">
                                <span>OMB No. 1545-0115</span>
                                <img src="https://secure.expressefile.com/Content/Images/2017Y.png">
                                <p>Form <b>1099-MISC</b></p>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6  pad0 formLeftHalf borderR980 borderT borderL980">
                        <div class="signleBox" id="DivAddress1">
                            <label class="formLabel"> PAYER’S name, street address, city or town, state or province, country, ZIP or foreign postal code, and telephone no.</label>
                            <div class="">
                                <div class="pos-rel pLeft10 pRight10">
                                    <?php echo !empty($c_details[0]->c_name) ? $c_details[0]->c_name : ''; ?>
                                </div>
                                <div class="pos-rel pLeft10 pRight10">
                                    <?php echo !empty($c_details[0]->c_address) ? $c_details[0]->c_address : ''; ?> <?php echo !empty($c_details[0]->c_address_2) ? ', ' . $c_details[0]->c_address_2 : ''; ?>
                                </div>
                                <div class="pos-rel pLeft10 pRight10">
                                    <?php echo !empty($c_details[0]->c_city) ? $c_details[0]->c_city : ''; ?>, <?php echo !empty($c_details[0]->c_state) ? $c_details[0]->c_state : ''; ?> <?php echo !empty($c_details[0]->c_zip) ? $c_details[0]->c_zip : ''; ?>
                                </div>

                            </div>
                        </div>
                        <div class="signleBox1 disFlex1">
                            <div class="col-md-6 col-sm-6 pad0 borderR1" id="DivPayerEINNumber">
                                <div class="pBot5">
                                    <label class="formLabel">
                                        PAYER’S federal identification number
                                    </label>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <?php echo !empty($c_details[0]->c_ssn_ein) ? $c_details[0]->c_ssn_ein : ''; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 pad0" id="DivRecipientEINNumber">
                                <div class="pBot5">
                                    <label class="formLabel">
                                        RECIPIENT’S identification number
                                    </label>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <input  maxlength="11" size="20" id="recepient_id_no" name="recepient_id_no" placeholder="SSN" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->recepient_id_no) ? $v_details[0]->recepient_id_no : ''; ?>" />
                                        <label id="SSNNumberEr" style="display:none" class="error"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="signleBox" id="DivRecipientName">
                            <label class="formLabel"> RECIPIENT’S name</label>
                            <div class="pLeft10 pRight10">
                                <input class="mBot5" maxlength="40" value="<?php echo !empty($v_details) && !empty($v_details[0]->first_name) ? $v_details[0]->first_name : ''; ?>" name="first_name" placeholder="First & Last Name" type="text" />
                                <label id="Recipient_NameEr" style="display:none" class="error"></label>
                            </div>
                            <div class="pLeft10 pRight10">
                                <input class="mBot5" id="Recipient_Name" maxlength="80" value="<?php echo !empty($v_details) && !empty($v_details[0]->dba) ? $v_details[0]->dba : ''; ?>" name="dba" onchange="ChangeDocumentName()" placeholder="dba, partners, etc." type="text" />
                                <label id="Recipient_NameEr" style="display:none" class="error"></label>
                            </div>
                        </div>
                        <div class="signleBox" id="DivStreetAddress">
                            <label class="formLabel"> Street address (including apt. no.)</label>
                            <div class="pLeft10 pRight10">
                                <input class="mBot5" id="RecipientAddress1" maxlength="20" placeholder="Address1" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->address) ? $v_details[0]->address : ''; ?>" name="address" />
                                <label id="RecipientAddress1Er" style="display:none" class="error"></label>
                            </div>
                        </div>
                        <div class="signleBox">
                            <label class="formLabel"> City or town, state or province, country, and ZIP or foreign postal code</label>
                            <div>
                                <div class="pLeft10 pRight10 pos-rel">
                                    <input class="mBot5" id="RecipientCity" maxlength="22" placeholder="City" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->city) ? $v_details[0]->city : ''; ?>" name="city" />
                                    <label id="RecipientCityEr" style="display:none" class="error"></label>
                                </div>
                                    <div class="pLeft10 pRight10 pos-rel">
                                       <input class="mBot5" data-val="true" data-val-number="The field StateId must be a number." data-val-required="The StateId field is required." id="RecipientStateId" placeholder="State" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->state) ? $v_details[0]->state : '' ?>" name="state" />
                                        <label id="RecipientStateIdEr" style="display:none" class="error"></label>
                                    </div>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <input  class="mBot5" id="RecipientZip" minlength="5" placeholder="Zip Code" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->zipcode) ? $v_details[0]->zipcode : ''; ?>" name="zipcode" />
                                        <label id="RecipientZipEr" style="display:none" class="error"></label>
                                    </div>
                            </div>
                        </div>
                        <div class="signleBox1 disFlex1 borderBN mBot0">
                            <div class="col-md-12 col-sm-12 col-xs-12 pad0 borderR1" id="DivAccountNum">
                                <div class="pBot5">
                                    <label class="formLabel"> Account number (see instructions)</label>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <input id="AcctNumber" maxlength="20" name="account_number" placeholder="Account Number" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->account_number) ? $v_details[0]->account_number : ''; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 borderT1024 borderT floatL borderL980">
                        <div class="row">
                            <div class="col-md-12 col-xs-12 pad0 disFlex1 borderR1024 borderB borderRT">
                                <div class="col-md-6 col-sm-6 borderR640 pad0">
                                    <div class="rightSingleBox borderR1" id="DivRents">
                                        <label class="formLabel"><span class="mRight5">1 </span>Rents</label>
                                        <div class="pLeft10 pRight10">
                                            <input class="B1Rents taR redDollar" data-val="true" data-val-number="The field B1Rents must be a number." data-val-required="The B1Rents field is required." id="FormDetails_B1Rents" maxlength="13" type="text" value="<?php echo isset($v_details[0]->rents) ? $v_details[0]->rents : ''; ?>"  name="rents">
                                        </div>
                                    </div>
                                    <div class="rightSingleBox borderR1 borderBN" id="DivRoyalties">
                                        <label class="formLabel"><span class="mRight5">2 </span>Royalties</label>
                                        <div class="pLeft10 pRight10">
                                            <input class="B2Royalties taR redDollar" data-val="true" data-val-number="The field B2Royalties must be a number." data-val-required="The B2Royalties field is required." id="FormDetails_B2Royalties" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->royalties) ? $v_details[0]->royalties : ''; ?>" name="royalties" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 borderR1024N formNY taC padtop0 hide640 ">
                                    <h5 class="red">OMB No. 1545-0115<h5>
                                    <h2 style="font-weight: 900;" class="red"><?php echo !empty($c_details[0]->TaxYear) ? $c_details[0]->TaxYear : ''; ?></h2>
                                    <h5 class="red">1099-MISC</h5>
                                </div>
                            </div>
                            <div class="rightBottomPart">
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1" id="DivOtherIncome">
                                    <label class="formLabel"><span class="mRight5">3 </span> Other income</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B3OtherIncome taR redDollar" data-val="true" data-val-number="The field B3OtherIncome must be a number." data-val-required="The B3OtherIncome field is required." id="FormDetails_B3OtherIncome" maxlength="13" value="<?php echo isset($v_details[0]->other_income) ? $v_details[0]->other_income : ''; ?>" name="other_income" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox" id="DivFedIncometaxWithheld">
                                    <label class="formLabel"><span class="mRight5">4 </span> Federal income tax withheld</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B4FITWithheld taR redDollar" data-val="true" data-val-number="The field B4FITWithheld must be a number." data-val-required="The B4FITWithheld field is required." id="FormDetails_B4FITWithheld" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->fed_income_with_held) ? $v_details[0]->fed_income_with_held : ''; ?>" name="fed_income_with_held" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1" id="DivFishingboatproceeds">
                                    <label class="formLabel"><span class="mRight5">5 </span> Fishing boat proceeds</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B5FishingBoat taR redDollar" data-val="true" data-val-number="The field B5FishingBoat must be a number." data-val-required="The B5FishingBoat field is required." id="FormDetails_B5FishingBoat" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->fishing_boat_proceed) ? $v_details[0]->fishing_boat_proceed : ''; ?>" name="fishing_boat_proceed" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox" id="DivMedHealth">
                                    <label class="formLabel"><span class="mRight5">6 </span> Medical and health care payments</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B6MedicalAndHealth taR redDollar" data-val="true" data-val-number="The field B6MedicalAndHealth must be a number." data-val-required="The B6MedicalAndHealth field is required." id="FormDetails_B6MedicalAndHealth" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->mad_helthcare_pmts) ? $v_details[0]->mad_helthcare_pmts : ''; ?>" name="mad_helthcare_pmts" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1" id="DivNonEmpCompensation">
                                    <label class="formLabel"><span class="mRight5">7 </span> Nonemployee compensation</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B7NonRecipientCompensation taR redDollar" data-val="true" data-val-number="The field B7NonEmployeeCompensation must be a number." data-val-required="The B7NonEmployeeCompensation field is required." id="FormDetails_B7NonEmployeeCompensation" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->non_emp_compansation) ? $v_details[0]->non_emp_compansation : ''; ?>" name="non_emp_compansation" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox" id="DivSubstitutePayments">
                                    <label class="formLabel" style="letter-spacing: -0.5px;"><span class="mRight5">8 </span> Substitute payments in lieu of dividends or interest</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B8SubstitutePayments taR redDollar" data-val="true" data-val-number="The field B8SubstitutePayments must be a number." data-val-required="The B8SubstitutePayments field is required." id="FormDetails_B8SubstitutePayments" maxlength="13" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->pmts_in_lieu) ? $v_details[0]->pmts_in_lieu : ''; ?>" name="pmts_in_lieu" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1" id="DivPayMade">
                                    <div class="valueL">
                                        <label class="formLabel cursorP" for="FormDetails_B9PayerMadeDirectSales">
                                            <span class="mRight5">9 </span>Payer made direct sales of
                                            $5,000 or more of consumer
                                            products to a buyer
                                            (recipient) for resale
                                            ▶
                                        </label>
                                    </div>
                                    <input type="checkbox" style="position: absolute;
     left: 207px !important;
     opacity: 1 !important;" class="check" <?php echo !empty($v_details) && !empty($v_details[0]->consumer_products_for_resale) ? 'checked' : ''; ?> />
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox" id="DivCrop">
                                    <label class="formLabel"><span class="mRight5">10 </span>Crop insurance proceeds</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B10CropInsuranceProceeds taR redDollar" data-val="true" data-val-number="The field B10CropInsuranceProceeds must be a number." data-val-required="The B10CropInsuranceProceeds field is required." id="FormDetails_B10CropInsuranceProceeds" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->crop_Insurance_proceeds) ? $v_details[0]->crop_Insurance_proceeds : ''; ?>" name="crop_Insurance_proceeds" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1 bg_red">
                                    <label class="formLabel pLeft0 pTop0 "><span class="mRight5" style="background: #fff;padding: 2px 5px;">11 </span></label>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox bg_red">
                                    <label class="formLabel pLeft0 pTop0"><span class="mRight5" style="background: #fff;padding: 2px 5px;">12 </span></label>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1 borderBN borderB1024" id="DivExcess">
                                    <label class="formLabel"><span class="mRight5">13 </span>Excess golden parachute payments</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B13ExcessGoldenParachute taR redDollar" data-val="true" data-val-number="The field B13ExcessGoldenParachute must be a number." data-val-required="The B13ExcessGoldenParachute field is required." id="FormDetails_B13ExcessGoldenParachute" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->excess_golden_par_pmts) ? $v_details[0]->excess_golden_par_pmts : ''; ?>" name="excess_golden_par_pmts" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderBN borderB1024" id="DivGross">
                                    <label class="formLabel" style="letter-spacing: -0.5px;"><span class="mRight5">14 </span> Gross proceeds paid to an attorney</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B14GrossProceedsPaid taR redDollar" data-val="true" data-val-number="The field B14GrossProceedsPaid must be a number." data-val-required="The B14GrossProceedsPaid field is required." id="FormDetails_B14GrossProceedsPaid" maxlength="13"value="<?php echo !empty($v_details) && !empty($v_details[0]->gross_paid_to_an_attomey) ? $v_details[0]->gross_paid_to_an_attomey : ''; ?>" name="gross_paid_to_an_attomey" type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-12 formRightHalf borderT borderR">
                        <div class="">
                            <div class="row border-bottom">
                                <div class="col-lg-12 col-md-6 col-sm-6 mis_income">
                                    <h4 class="red" style="font-weight: 900; font-size: 17px !important;">Miscellaneous Income</h4>
                                </div>
                            </div>
                            <div class="row borderT">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row3 fullBorder2 borderR1024">
                    <div class="disFlex1">
                        <div class="col-lg-4 col-md-12 rightSingleBox borderBN borderR borderR640N">
                            <div>
                                <div class="col-md-6 col-sm-6 pad0" id="DivSec409ADef">
                                    <label class="formLabel"><span class="mRight5">15a </span> Section 409A deferrals</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B15aSection409ADeferrals taR redDollar" data-val="true" data-val-number="The field B15aSection409ADeferrals must be a number." data-val-required="The B15aSection409ADeferrals field is required." id="FormDetails_B15aSection409ADeferrals" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->sec_409a_deferrals) ? $v_details[0]->sec_409a_deferrals : ''; ?>" name="sec_409a_deferrals" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 pad0" id="DivSec409AIncome">
                                    <label class="formLabel"><span class="mRight5">15b </span> Section 409A income</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B15bSection409AIncome taR redDollar" data-val="true" data-val-number="The field B15bSection409AIncome must be a number." data-val-required="The B15bSection409AIncome field is required." id="FormDetails_B15bSection409AIncome" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->sec_409a_Income) ? $v_details[0]->sec_409a_Income : ''; ?>" name="sec_409a_Income" type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 pad0 disFlex980N  borderTN">
                            <div class="rightSingleBox stateBox2 col-lg-6 col-md-6 col-sm-6" id="DivStateTax">
                                <label class="formLabel"><span class="mRight5">16 </span> State tax withheld.</label>
                                <div class="pLeft10 pRight10">
                                    <input class="B16StateTaxWithheld taR redDollar" data-val="true" data-val-number="The field B16StateTaxWithheld must be a number." data-val-required="The B16StateTaxWithheld field is required." id="FormDetails_B16StateTaxWithheld" maxlength="13" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->state_tax_with_held) ? $v_details[0]->state_tax_with_held : ''; ?>" name="state_tax_with_held" />
                                </div>
                            </div>
                            <div class="rightSingleBox  stateBox2 col-lg-6 col-md-6 col-sm-6" id="DivStatePayer">
                                <label class="formLabel"><span class="mRight5">17 </span> State/Payer’s state no.</label>
                                <div class="pLeft10 pRight10">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <input id="FormDetails_B17PayerStateNo" maxlength="20" value="<?php echo !empty($v_details) && !empty($v_details[0]->payer_state_no) ? $v_details[0]->payer_state_no : ''; ?>" name="payer_state_no" type="text" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="rightSingleBox stateBox2 col-lg-6 col-md-6 col-sm-6" id="DivStateIncome">
                                <label class="formLabel"><span class="mRight5">18 </span> State income</label>
                                <div class="pLeft10 pRight10">
                                    <input class="B18StateIncome taR redDollar" data-val="true" data-val-number="The field B18StateIncome must be a number." data-val-required="The B18StateIncome field is required." id="FormDetails_B18StateIncome" maxlength="13" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->state_Income) ? $v_details[0]->state_Income : ''; ?>" name="state_Income" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear spacer5"></div>
            <div class="clear spacer10"></div>
            <div class="formFooter">
            <center><input type="checkbox" name="foreign_add" class="check" value="1" <?php echo !empty($v_details) && !empty($v_details[0]->foreign_add) ? 'checked' : ''; ?> onClick="forclick()" />
Foreign Address</center>
            <center>
<select name="stateList" size="1" class="smallpara" onChange="put()">
<option>Select State</option>
<option value="AL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AL') {echo "selected";}?>>AL Alabama</option>
<option value="AK" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AK') {echo "selected";}?>>AK Alaska</option>
<option value="AS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AS') {echo "selected";}?>>AS Amercn Samoa</option>
<option value="AZ" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AZ') {echo "selected";}?>>AZ Arizona</option>
<option value="AR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AR') {echo "selected";}?>>AR Arkansas</option>
<option value="CA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CA') {echo "selected";}?>>CA California</option>
<option value="CO" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CO') {echo "selected";}?>>CO Colorado</option>
<option value="CT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CT') {echo "selected";}?>>CT Connecticut</option>
<option value="DE" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'DE') {echo "selected";}?>>DE Delaware</option>
<option value="DC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'DC') {echo "selected";}?>>DC Dst of Colum</option>
<option value="FM" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'FM') {echo "selected";}?>>FM Micronesia</option>
<option value="FL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'FL') {echo "selected";}?>>FL Florida</option>
<option value="GA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'GA') {echo "selected";}?>>GA Georgia</option>
<option value="GU" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'GU') {echo "selected";}?>>GU Guam</option>
<option value="HI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'HI') {echo "selected";}?>>HI Hawaii</option>
<option value="ID" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ID') {echo "selected";}?>>ID Idaho</option>
<option value="IL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IL') {echo "selected";}?>>IL Illinois</option>
<option value="IN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IN') {echo "selected";}?>>IN Indiana</option>
<option value="IA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IA') {echo "selected";}?>>IA Iowa</option>
<option value="KS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'KS') {echo "selected";}?>>KS Kansas</option>
<option value="KY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'KY') {echo "selected";}?>>KY Kentucky</option>
<option value="LA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'LA') {echo "selected";}?>>LA Louisiana</option>
<option value="ME" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ME') {echo "selected";}?>>ME Maine</option>
<option value="MH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MH') {echo "selected";}?>>MH Marshall Is.</option>
<option value="MD" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MD') {echo "selected";}?>>MD Maryland</option>
<option value="MA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MA') {echo "selected";}?>>MA Massachusett</option>
<option value="MI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MI') {echo "selected";}?>>MI Michigan</option>
<option value="MN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MN') {echo "selected";}?>>MN Minnesota</option>
<option value="MS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MS') {echo "selected";}?>>MS Mississippi</option>
<option value="MO" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MO') {echo "selected";}?>>MO Missouri</option>
<option value="MT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MT') {echo "selected";}?>>MT Montana</option>
<option value="NE" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NE') {echo "selected";}?>>NE Nebraska</option>
<option value="NV" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NV') {echo "selected";}?>>NV Nevada</option>
<option value="NH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NH') {echo "selected";}?>>NH New Hampshir</option>
<option value="NJ" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NJ') {echo "selected";}?>>NJ New Jersey</option>
<option value="NM" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NM') {echo "selected";}?>>NM New Mexico</option>
<option value="NY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NY') {echo "selected";}?>>NY New York</option>
<option value="NC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NV') {echo "selected";}?>>NC N. Carolina</option>
<option value="ND" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ND') {echo "selected";}?>>ND North Dakota</option>
<option value="MP" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MP') {echo "selected";}?>>MP Mariana Is.</option>
<option value="OH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OH') {echo "selected";}?>>OH Ohio</option>
<option value="OK" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OK') {echo "selected";}?>>OK Oklahoma</option>
<option value="OR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OR') {echo "selected";}?>>OR Oregon</option>
<option value="PA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'PA') {echo "selected";}?>>PA Pennsylvania</option>
<option value="PR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'PR') {echo "selected";}?>>PR Puerto Rico</option>
<option value="RI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'RI') {echo "selected";}?>>RI Rhode Island</option>
<option value="SC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'SC') {echo "selected";}?>>SC So. Carolina</option>
<option value="SD" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'SD') {echo "selected";}?>>SD South Dakota</option>
<option value="TN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'TN') {echo "selected";}?>>TN Tennessee</option>
<option value="TX" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'TX') {echo "selected";}?>>TX Texas</option>
<option value="UT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'UT') {echo "selected";}?>>UT Utah</option>
<option value="VT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VT') {echo "selected";}?>>VT Vermont</option>
<option value="VA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VA') {echo "selected";}?>>VA Virginia</option>
<option value="VI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VI') {echo "selected";}?>>VI Virgin Is.</option>
<option value="WA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WA') {echo "selected";}?>>WA Washington</option>
<option value="WV" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WV') {echo "selected";}?>>WV West Virgina</option>
<option value="WI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WI') {echo "selected";}?>>WI Wisconsin</option>
<option value="WY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WY') {echo "selected";}?>>WY Wyoming</option>
</select> Use Select State to find the correct 2 character state code.</center>
            </div>


        </div>
    </div>
</form>

<script type="text/javascript">
</script>
                </div>
            </div>
        </div>
    </div>

          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
<style>.page-loader-wrapper {
    display: none;
}
ul#myTab41 h4 {
    font-weight: 500;
    color: #000000;
    background: #6d6d6d33;
    padding: 6px 20px;
    margin-left: -20px;
    margin-top: 15px;
}
ul#myTab41 li b {
    font-weight: 500;
    color: #006dbb;
}
ul#myTab41 li {
    line-height: 30px;
}
</style>