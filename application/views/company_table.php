<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h2>
               <?php echo lang("companys"); ?>
            </h2>
            <ul class="header-dropdown m-r--5">
            <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul
          ></div>
          <!-- /.box-header -->
          <div class="body table-responsive">
                <div class="row filter-row">
                  <div class="col-md-3">
                    <div class="form-group form-float">
                      <div class="form-line">
                        <select name="Files___CustID___filter" class="filter-field form-control">
                          <option value="all">All</option>
                          <?php echo isset($f_option) ? $f_option : ''; ?>
                        </select>
                        <label class="form-label" style="margin-top: -10px;">Select Users</label>
                      </div>
                    </div>
                  </div>
                  <?php //echo $obj->get_filter_html(); ?>
                </div>
            <table id="example1" class="table table-bordered table-striped table-hover delSelTable">
              <thead>
                <tr>
                <th>
                <input type="checkbox" class="selAll" id="basic_checkbox_1mka" />
                <label for="basic_checkbox_1mka"></label>
                </th>
                <th><?php echo lang('name') ?></th>
                <th>Year<?php //echo lang('email') ?></th>
                <th><?php echo lang('ein') ?></th>
                <th><?php echo lang('telephone') ?></th>
                <th>Form Type</th>
                <th>Form Type</th>
                <th><?php echo lang('create_date') ?></th>
                <?php $cf = getCustomField('user');
if (is_array($cf) && !empty($cf)) {
    foreach ($cf as $cfkey => $cfvalue) {
        echo '<th>' . lang(getLang($cfvalue->rel_crud) . '_' . getLang($cfvalue->name)) . '</th>';
    }
}
?>
                <th><?php echo lang('Action'); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
  <!-- /.content -->
<div class="modal fade" id="nameModal_user" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="defaultModalLabel"><?php echo lang('user_form'); ?></h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div><!--End Modal Crud -->
<script type="text/javascript">
  $(document).ready(function() {
    if($('.filter-row').find('.filter-field').length > 0){
      setTimeout(function() {
        $('.filter-row').find('.filter-field').first().trigger('change');
      }, 500);
    } else {
      data_table('', '');
    }

    setTimeout(function() {
      var add_width = $('.dataTables_filter').width()+$('.box-body .dt-buttons').width()+10;
      $('.table-date-range').css('right',add_width+'px');

        $('.dataTables_info').before('<button data-del-url="<?php echo base_url() . 'user/deletecompany/'; ?>" rel="delSelTable" class="btn btn-default btn-sm delSelected pull-left"> <i class="material-icons col-red">delete</i> </button><br><br>');
    }, 300);
    $("button.closeTest, button.close").on("click", function (){});


      $("body").on("change", '.filter-field', function() {
        $sVal = $('.dataTables_filter').find('input[type="search"]').val();
            if(typeof(table) != "undefined" && table !== null) {
              table.destroy();
        }
        $filter = {};
        $('select.filter-field, input.filter-field').each(function(){
          $filter[$(this).attr('name')] = $(this).val();
        });
            var dateRange = $(this).val();
            data_table($filter, $sVal);
            get_grid_info_box_val('test');
            $.post('<?php echo base_url() . 'user/set_filter_cookie' ?>', {filter_coo: $filter});
      });

  });



  var data_table = function($filter, $search) {
  var url = "<?php echo base_url(); ?>";
  return table = $("#example1").DataTable({
    "dom": 'lfBrtip',
          "buttons": ['copy','excel','print'],
    "processing": true,
        "serverSide": true,
        "ajax": {
          "url" : url + "user/companyTable",
        "data": function ( d ) {
          if($filter != '') {
            $.each($filter, function(index, val) {
              d[index] = val;
            });
          }
          <?php if (isset($submodule) && $submodule != '') {?>
                  d.submodule = '<?php echo $submodule; ?>';
          <?php }?>
              }
        },
        "sPaginationType": "full_numbers",
        "language": {
      "search": "_INPUT_",
      "searchPlaceholder": "<?php echo lang('search'); ?>",
      "paginate": {
          "next": '<i class="material-icons">keyboard_arrow_right</i>',
          "previous": '<i class="material-icons">keyboard_arrow_left</i>',
          "first": '<i class="material-icons">first_page</i>',
          "last": '<i class="material-icons">last_page</i>'
      }
    },
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25, 50, 100,500,-1], [10, 25, 50,100,500,"<?php echo lang('all'); ?>"]],
    "columnDefs" : [
      {
        "orderable": false,
        "targets": [0]
      },
      {
                "targets": [6],
                "visible": false,
                "searchable": false
            }
    ],
    "order": [[1, 'asc']],
    "oSearch": {"sSearch": $search}
  });
}
</script>