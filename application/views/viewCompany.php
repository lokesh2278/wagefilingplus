<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
          <?php $FormType = isset($c_companies[0]->FormType) ? $c_companies[0]->FormType : '';?>
            <h2>Company Name : <?php echo isset($c_companies[0]->c_name) ? $c_companies[0]->c_name : ''; ?>( Form <?php echo $FormType; ?>)</h2>
            <ul class="header-dropdown m-r--5">
            <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
            </div>
          <!-- /.box-header -->
          <div class="body">
             <div class="row">
              <div class="col-lg-11">
                    <ul id="myTab41" class="">
 <?php if (!empty($FormType) && (trim($FormType) == '1099-Misc')) {
    ?>
<li><h4>File Setup - Form 1099 Misc </h4></li>
<li><b>SSN or EIN</b> : <?php echo isset($c_companies[0]->c_ssn_ein) ? $c_companies[0]->c_ssn_ein : ''; ?>/<?php echo isset($c_companies[0]->c_employee_ein) ? $c_companies[0]->c_employee_ein : ''; ?></li>
<li><b>Tax Year</b> : <?php echo isset($c_companies[0]->c_tax_year) ? $c_companies[0]->c_tax_year : ''; ?></li>
<li><b>Filer Name (Company)</b> : <?php echo isset($c_companies[0]->c_name) ? $c_companies[0]->c_name : ''; ?></li>
<li><b>Filer Is Foregin</b> : <?php echo isset($c_companies[0]->c_is_foreign) && $c_companies[0]->c_is_foreign == 0 ? 'No' : 'Yes'; ?></li>
<li><b>Filer Name 2(Optional)</b> : <?php echo isset($c_companies[0]->c_filer_name_2) ? $c_companies[0]->c_filer_name_2 : ''; ?></li>
<li><b>Address</b> : <?php echo isset($c_companies[0]->c_address) ? $c_companies[0]->c_address : ''; ?></li>
<li><b>Address 2(Optional)</b> : <?php echo isset($c_companies[0]->c_address_2) ? $c_companies[0]->c_address_2 : ''; ?></li>
<li><b>City</b> : <?php echo isset($c_companies[0]->c_city) ? $c_companies[0]->c_city : ''; ?></li>
<li><b>State</b> : <?php echo isset($c_companies[0]->c_state) ? $c_companies[0]->c_state : ''; ?></li>
<li><b>Zip Code</b> : <?php echo isset($c_companies[0]->c_zip) ? $c_companies[0]->c_zip : ''; ?></li>

<li><h4>File Setup - 1099 Series - Some Option Fields</h4></li>

<li><b>Contact</b> : <?php echo isset($c_companies[0]->c_contact) ? $c_companies[0]->c_contact : ''; ?></li>
<li><b>Title</b> : <?php echo isset($c_companies[0]->c_title) ? $c_companies[0]->c_title : ''; ?></li>
<li><b>Email</b> : <?php echo isset($c_companies[0]->c_email) ? $c_companies[0]->c_email : ''; ?></li>
<li><b>Phone</b> : <?php echo isset($c_companies[0]->c_telephone) ? $c_companies[0]->c_telephone : ''; ?></li>
<li><b>Fax</b> : <?php echo isset($c_companies[0]->c_fax) ? $c_companies[0]->c_fax : ''; ?></li>
<li><b>Form Type</b> : <?php echo isset($c_companies[0]->FormType) ? $c_companies[0]->FormType : ''; ?></li>
<li><b>user Id</b> : <?php echo isset($c_companies[0]->CustID) ? $c_companies[0]->CustID : ''; ?></li>
<li><b>Create Date </b> : <?php echo isset($c_companies[0]->created_at) ? $c_companies[0]->created_at : ''; ?></li>

<!-- <li><b>FileStr </b>: <?php //echo "<pre>";
    //print_r(explode("|", $c_companies[0]->FileStr)); ?></li> -->


<li class="linkbtnam"><h4><a href="<?php echo base_url(); ?>user/f1099data/<?php echo isset($c_companies[0]->FileSequence) ? $c_companies[0]->FileSequence : ''; ?>" >Go to  1099-Misc Form data </a></h4></li>

<?php } else if (!empty($FormType) && (trim($FormType) == '1099-Div')) {
    ?>
<li><h4>File Setup - Form 1099 Div </h4></li>
<li><b>SSN or EIN</b> : <?php echo isset($c_companies[0]->c_ssn_ein) ? $c_companies[0]->c_ssn_ein : ''; ?>/<?php echo isset($c_companies[0]->c_employee_ein) ? $c_companies[0]->c_employee_ein : ''; ?></li>
<li><b>Tax Year</b> : <?php echo isset($c_companies[0]->c_tax_year) ? $c_companies[0]->c_tax_year : ''; ?></li>
<li><b>Filer Name (Company)</b> : <?php echo isset($c_companies[0]->c_name) ? $c_companies[0]->c_name : ''; ?></li>
<li><b>Filer Is Foregin</b> : <?php echo isset($c_companies[0]->c_is_foreign) && $c_companies[0]->c_is_foreign == 0 ? 'No' : 'Yes'; ?></li>
<li><b>Filer Name 2(Optional)</b> : <?php echo isset($c_companies[0]->c_filer_name_2) ? $c_companies[0]->c_filer_name_2 : ''; ?></li>
<li><b>Address</b> : <?php echo isset($c_companies[0]->c_address) ? $c_companies[0]->c_address : ''; ?></li>
<li><b>Address 2(Optional)</b> : <?php echo isset($c_companies[0]->c_address_2) ? $c_companies[0]->c_address_2 : ''; ?></li>
<li><b>City</b> : <?php echo isset($c_companies[0]->c_city) ? $c_companies[0]->c_city : ''; ?></li>
<li><b>State</b> : <?php echo isset($c_companies[0]->c_state) ? $c_companies[0]->c_state : ''; ?></li>
<li><b>Zip Code</b> : <?php echo isset($c_companies[0]->c_zip) ? $c_companies[0]->c_zip : ''; ?></li>

<li><h4>File Setup - 1099 Series - Some Option Fields</h4></li>

<li><b>Contact</b> : <?php echo isset($c_companies[0]->c_contact) ? $c_companies[0]->c_contact : ''; ?></li>
<li><b>Title</b> : <?php echo isset($c_companies[0]->c_title) ? $c_companies[0]->c_title : ''; ?></li>
<li><b>Email</b> : <?php echo isset($c_companies[0]->c_email) ? $c_companies[0]->c_email : ''; ?></li>
<li><b>Phone</b> : <?php echo isset($c_companies[0]->c_telephone) ? $c_companies[0]->c_telephone : ''; ?></li>
<li><b>Fax</b> : <?php echo isset($c_companies[0]->c_fax) ? $c_companies[0]->c_fax : ''; ?></li>
<li><b>Form Type</b> : <?php echo isset($c_companies[0]->FormType) ? $c_companies[0]->FormType : ''; ?></li>
<li><b>user Id</b> : <?php echo isset($c_companies[0]->CustID) ? $c_companies[0]->CustID : ''; ?></li>
<li><b>Create Date </b> : <?php echo isset($c_companies[0]->created_at) ? $c_companies[0]->created_at : ''; ?></li>

<!-- <li><b>FileStr </b>: <?php //echo "<pre>";
    //print_r(explode("|", $c_companies[0]->FileStr)); ?></li> -->


<li class="linkbtnam"><h4><a href="<?php echo base_url(); ?>user/div1099data/<?php echo isset($c_companies[0]->FileSequence) ? $c_companies[0]->FileSequence : ''; ?>" >Go to  1099-Div Form data </a></h4></li>

<?php } else if (!empty($FormType) && (trim($FormType) == 'W-2')) {
    ?>

<li><h4>Employer Tax Year and EIN information </h4></li>


<li><b>Tax Year</b> : <?php echo isset($c_companies[0]->c_tax_year) ? $c_companies[0]->c_tax_year : ''; ?></li>

<li><b>You have:</b> : <?php echo isset($c_companies[0]->c_you_have) && $c_companies[0]->c_you_have == 1 ? 'An EIN ' : 'Applied for an EIN'; ?></li>

<li><b>The EIN is for:</b> : <?php if (isset($c_companies[0]->c_ein_for)) {
        if ($c_companies[0]->c_ein_for == 1) {echo 'A Regular Employer';}
        if ($c_companies[0]->c_ein_for == 2) {echo 'A Common Paymaster';}
        if ($c_companies[0]->c_ein_for == 3) {echo 'Filer is Form 2678 Agent';}
        if ($c_companies[0]->c_ein_for == 4) {echo 'Filer is Section 3504 Agent';}

    }?></li>


<li><h4>An EIN or "Applied For" must appear on all W-2 Forms. SSNs are not allowed.</h4></li>

<li><b>Employee EIN</b> : <?php echo isset($c_companies[0]->c_employee_ein) ? $c_companies[0]->c_employee_ein : ''; ?></li>
<li><b>Other EIN</b> : <?php echo isset($c_companies[0]->c_other_ein) ? $c_companies[0]->c_other_ein : ''; ?></li>

<li><h4>If you file multiple payrolls for the same EIN:</h4></li>
<li><b>Establishment Number:(designate the store, factory or type of payroll. Up to 4 characters.)</b> : <?php echo isset($c_companies[0]->c_est_number) ? $c_companies[0]->c_est_number : ''; ?></li>


<li><h4>Employer Company Information</h4></li>

<li><b>Company</b> : <?php echo isset($c_companies[0]->c_company) ? $c_companies[0]->c_company : isset($c_companies[0]->c_name) ? $c_companies[0]->c_name : ''; ?></li>
<li><b>Address 1</b> : <?php echo isset($c_companies[0]->c_address) ? $c_companies[0]->c_address : ''; ?></li>
<li><b>Address 2(Optional)</b> : <?php echo isset($c_companies[0]->c_address_2) ? $c_companies[0]->c_address_2 : ''; ?></li>
<li><b>City</b> : <?php echo isset($c_companies[0]->c_city) ? $c_companies[0]->c_city : ''; ?></li>
<li><b>State</b> : <?php echo isset($c_companies[0]->c_state) ? $c_companies[0]->c_state : ''; ?></li>
<li><b>Zip Code 5</b> : <?php echo isset($c_companies[0]->c_zip) ? $c_companies[0]->c_zip : ''; ?></li>
<li><b>Zip Code 4</b> : <?php echo isset($c_companies[0]->c_zip_2) ? $c_companies[0]->c_zip_2 : ''; ?></li>



<!--<li><b>Filer Is Foregin</b> : <?php echo isset($c_companies[0]->c_is_foreign) && $c_companies[0]->c_is_foreign == 0 ? 'No' : 'Yes'; ?></li>
<li><b>Filer Name 2(Optional)</b> : <?php echo isset($c_companies[0]->c_filer_name_2) ? $c_companies[0]->c_filer_name_2 : ''; ?></li>-->
<li><h4>Contact Person - Optional Fields</h4></li>

<li><b>Name</b> : <?php echo isset($c_companies[0]->c_name) ? $c_companies[0]->c_name : ''; ?></li>
<li><b>Title</b> : <?php echo isset($c_companies[0]->c_title) ? $c_companies[0]->c_title : ''; ?></li>
<li><b>Email</b> : <?php echo isset($c_companies[0]->c_email) ? $c_companies[0]->c_email : ''; ?></li>
<li><b>Phone</b> : <?php echo isset($c_companies[0]->c_telephone) ? $c_companies[0]->c_telephone : ''; ?></li>
<li><b>Fax</b> : <?php echo isset($c_companies[0]->c_fax) ? $c_companies[0]->c_fax : ''; ?></li>


<li><h4>Employer Code</h4></li>
<li><b>Select Code:</b> : <?php if (isset($c_companies[0]->c_code)) {
        if ($c_companies[0]->c_code == 1) {echo 'R Regular Form 941 Employer';}
        if ($c_companies[0]->c_code == 2) {echo 'A Agriculture Form 943 Employer
                                   ';}
        if ($c_companies[0]->c_code == 3) {echo 'H Household Employer
                                   ';}
        if ($c_companies[0]->c_code == 4) {echo 'M Military Employer
                                   ';}
        if ($c_companies[0]->c_code == 5) {echo 'Q Medicare Qualified Goverment Employer
                                   ';}
        if ($c_companies[0]->c_code == 6) {echo 'X Railroad CT-1 (RRTA) Employer
                                   ';}
        if ($c_companies[0]->c_code == 7) {echo 'M Regular Form 944 Employer
                                   ';}

    }?></li>


<li><h4>Employer Foreign Address Information OPTIONAL</h4></li>
<li><b>Foreign State(Optional)</b> : <?php echo isset($c_companies[0]->c_foreign_state) ? $c_companies[0]->c_foreign_state : ''; ?></li>
<li><b>Foreign Zip(Optional)</b> : <?php echo isset($c_companies[0]->c_foreign_zip) ? $c_companies[0]->c_foreign_zip : ''; ?></li>
<li><b>Foreign Country Code</b> : <?php echo isset($c_companies[0]->c_foreign_country_code) ? $c_companies[0]->c_foreign_country_code : ''; ?></li>


<li><h4>Employer State Constants - Repeats in Box 15 without typing OPTIONAL.</h4></li>
<li><b>State Code</b> : <?php echo isset($c_companies[0]->c_state_code) ? $c_companies[0]->c_state_code : ''; ?></li>
<li><b>State ID No</b> : <?php echo isset($c_companies[0]->c_state_id_no) ? $c_companies[0]->c_state_id_no : ''; ?></li>

<li><b>Third Party Sick Pay</b> : <?php echo isset($c_companies[0]->third_party_sick_pay) && $c_companies[0]->third_party_sick_pay == 1 ? 'Yes' : 'No'; ?></li>
<li><b>Terminating Business this Tax Year.</b> : <?php echo isset($c_companies[0]->terminating_business) && $c_companies[0]->third_party_sick_pay == 2 ? 'Yes' : 'No'; ?></li>


<li><h4>W-3 Box 13 for third-party sick pay use only (see instructions) OPTIONAL</h4></li>
<li><b>W-3 Box 13(Optional)</b> : <?php echo isset($c_companies[0]->w3_box_13) ? $c_companies[0]->w3_box_13 : ''; ?></li>

<li><h4>W-3 Box 14 Income tax withheld by payer of 3rd party sick pay (see instructions).</h4></li>
<li><b>W3-Box 14(Optional)</b> : <?php echo isset($c_companies[0]->w3_box_14) ? $c_companies[0]->w3_box_14 : ''; ?></li>

<li><b>user Id</b> : <?php echo isset($c_companies[0]->CustID) ? $c_companies[0]->CustID : ''; ?></li>
<li><b>Create Date </b> : <?php echo isset($c_companies[0]->created_at) ? $c_companies[0]->created_at : ''; ?></li>

<li class="linkbtnam"><h4><a href="<?php echo base_url(); ?>user/w2data/<?php echo isset($c_companies[0]->FileSequence) ? $c_companies[0]->FileSequence : ''; ?>" >Go to W-2 Form data </a></h4></li>
<?php } elseif (!empty($FormType) && (trim($FormType) == 'W-9s')) {
    ?>
    <li><h4>W-9s Information</h4></li>

    <li><b>Company Name</b> : <?php echo isset($c_companies[0]->c_name) ? $c_companies[0]->c_name : ''; ?></li>
    <li><b>EIN No.</b> : <?php echo isset($c_companies[0]->c_ssn_ein) ? $c_companies[0]->c_ssn_ein : ''; ?></li>
    <li><b>Street</b> : <?php echo isset($c_companies[0]->c_street) ? $c_companies[0]->c_street : ''; ?></li>
    <li><b>City</b> : <?php echo isset($c_companies[0]->c_street) ? $c_companies[0]->c_city : ''; ?></li>
    <li><b>State</b> : <?php echo isset($c_companies[0]->c_street) ? $c_companies[0]->c_state : ''; ?></li>
    <li><b>Zip</b> : <?php echo isset($c_companies[0]->c_street) ? $c_companies[0]->c_zip : ''; ?></li>
    <li><b>Email</b> : <?php echo isset($c_companies[0]->c_email) ? $c_companies[0]->c_email : ''; ?></li>
    <li><b>Phone</b> : <?php echo isset($c_companies[0]->c_telephone) ? $c_companies[0]->c_telephone : ''; ?></li>
    <li class="linkbtnam"><h4><a href="<?php echo base_url(); ?>user/vendorlist/<?php echo isset($c_companies[0]->FileSequence) ? $c_companies[0]->FileSequence : ''; ?>" >Go to <?php echo isset($c_companies[0]->c_name) ? $c_companies[0]->c_name : ''; ?> Vendors data </a></h4></li>
    <?php
}?>
                    </ul>
              </div>
             </div>
             <?php // echo"<PRE>"; print_r($c_companies);echo"</PRE>";?>
           </div>

          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
<style>.page-loader-wrapper {
    display: none;
}
ul#myTab41 h4 {
    font-weight: 500;
    color: #000000;
    background: #6d6d6d33;
    padding: 6px 20px;
    margin-left: -20px;
    margin-top: 15px;
}
ul#myTab41 li b {
    font-weight: 500;
    color: #006dbb;
}
ul#myTab41 li {
    line-height: 30px;
}
</style>