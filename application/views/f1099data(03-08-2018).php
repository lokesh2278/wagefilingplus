<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h2>Form Data of 1099</h2>

            <ul class="header-dropdown m-r--5">
              <a class="btn btn-lg btn-primary waves-effect" href='<?=base_url()?>user/export1099CSV'>Export</a>
              <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
          </div>
          <!-- /.box-header -->
          <div class="body table-responsive">
            <div class="row filter-row">
              <div class="col-md-3">
                <div class="form-group form-float">
                  <div class="form-line">
    <select name="ia_vendors___c_id___filter" id="selectcityAmin" class="filter-field form-control uniform-multiselect">
      <option value="all">All </option>
      <?php if (isset($c_data) && !empty($c_data)) {

    foreach ($c_data as $cid => $daaacc) {
        $s = $this->uri->segment(3);
        ?><option <?php if ($s == $daaacc->FileSequence) {echo 'selected="selected" ';}?> value="<?php echo isset($daaacc->FileSequence) ? $daaacc->FileSequence : ''; ?>" > <!-- <?php //echo isset($daaacc->c_name) ? $daaacc->c_name : ''; ?> --> <?php echo isset($daaacc->FileName) ? $daaacc->FileName : ''; ?> (<?php echo isset($daaacc->TaxYear) ? $daaacc->TaxYear : ''; ?>)</option>
    <?php }
}?>
    </select>
    <label class="form-label" style="margin-top: -10px;">Select Company</label>
                  </div>
                </div>
              </div>
            </div>
            <table id="example1" class="table table-bordered table-striped table-hover delSelTable">
              <thead>
                <tr>

<th>Id</th>
<th><?php echo lang('name') ?></th>
<th>Address</th>
<th>Company Id</th>
<th><?php echo lang('create_date') ?></th>
<th><?php echo lang('Action'); ?></th>
                </tr>
              </thead>
              <tbody>
              <?php if (isset($f1099data) && !empty($f1099data)) {

    foreach ($f1099data as $key => $datava) {?>
					  <tr>

<td><?php echo isset($datava->recepient_id_no) ? $datava->recepient_id_no : ''; ?></td>

<td><?php echo isset($datava->first_name) ? $datava->first_name : ''; ?> <?php echo isset($datava->last_name) ? $datava->last_name : ''; ?></td>

<td><?php echo isset($datava->address) ? $datava->address : ''; ?> <br /><?php echo isset($datava->city) ? $datava->city : ''; ?>
<?php echo isset($datava->state) ? $datava->state : ''; ?>
<?php echo isset($datava->zipcode) ? $datava->zipcode : ''; ?></td>
<td><?php echo isset($datava->FileSequence) ? $datava->FileSequence : ''; ?></td>
<td><?php echo isset($datava->created_at) ? date('Y-m-d', strtotime($datava->created_at)) : ''; ?></td>
<td><a style="cursor:pointer;" class="hrefid mClass1" href="<?php echo base_url(); ?>user/viewForm/<?php echo isset($datava->FileSequence) ? $datava->FileSequence : ''; ?>/<?php echo isset($datava->misc_id) ? $datava->misc_id : ''; ?>" title="View"><i class="material-icons font-20">visibility</i></a></td>
                </tr>

				<?php }

}?>
              </tbody>
            </table>
            <?php //echo "<PRE>";print_r($f1099data);echo "</PRE>";?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
  <!-- /.content -->
<!--End Modal Crud -->
<script>
    $(function(){
      // bind change event to select
      $('#selectcityAmin').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url && url!='all') { // require a URL
		  url ='<?php echo base_url(); ?>user/f1099data/'+url;
          window.location.href = url; // redirect
          }else{
			  window.location.href ='<?php echo base_url(); ?>user/f1099data/';
		}
          return false;
      });
    });
</script>
<script type="text/javascript">

  jQuery(document).ready(function($) {
	 // $('#example1').DataTable();
       var table = $('#example1').DataTable({
          "sPaginationType": "full_numbers",
          "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "<?php echo lang('search'); ?>",
            "paginate": {
                "next": '<i class="material-icons">keyboard_arrow_right</i>',
                "previous": '<i class="material-icons">keyboard_arrow_left</i>',
                "first": '<i class="material-icons">first_page</i>',
                "last": '<i class="material-icons">last_page</i>'
            }
          },
          "iDisplayLength": 10,
          "aLengthMenu": [[10, 25, 50, 100,500,-1], [10, 25, 50,100,500,"<?php echo lang('all'); ?>"]],
          "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
          ]
      });

	  });

</script>