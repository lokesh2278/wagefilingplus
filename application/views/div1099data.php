<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h2>Form Data of 1099-Div</h2>
            <ul class="header-dropdown m-r--5">
              <button class="btn btn-lg btn-success waves-effect" id='export'>Export</button>

              <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
          </div>
          <!-- /.box-header -->
          <div class="body table-responsive">
            <div class="row filter-row">
              <div class="col-md-2">
                <div class="form-group form-float">
                  <div class="form-line">
    <select name="ia_vendors___c_id___filter" id="selectcityAmin" class="filter-field form-control uniform-multiselect" >
      <option value="">All </option>
      <?php if (isset($c_data) && !empty($c_data)) {

    foreach ($c_data as $cid => $daaacc) {
        $s = $this->uri->segment(3);
        ?><option <?php if ($s == $daaacc->FileSequence) {echo 'selected="selected" ';}?> value="<?php echo isset($daaacc->FileSequence) ? $daaacc->FileSequence : ''; ?>" > <?php echo isset($daaacc->c_name) ? $daaacc->c_name : ''; ?> <?php echo isset($daaacc->FileName) ? $daaacc->FileName : ''; ?> (<?php echo isset($daaacc->TaxYear) ? $daaacc->TaxYear : ''; ?>)</option>
    <?php }
}?>
    </select>
    <label class="form-label" style="margin-top: -10px;">Select Company</label>
                  </div>
                </div>
              </div>
             <div class="col-md-2" hidden="hidden">
                <div class="form-group form-float">
                  <div class="form-line">
    <select value="PM" name="filetype" id="filetype" class="filter-field form-control uniform-multiselect" required="required">
      <!-- <option value="">Filed Type</option> -->
      <!-- <option value="E">E-Filed</option> -->
      <option value="PM">E-Filed, Print, Mail</option>
    </select>
    <label class="form-label" style="margin-top: -10px;">Select File Type</label>
                  </div>
                </div>
              </div>
               <div class="col-md-2">
                <div class="form-group form-float">
                  <div class="form-line">
    <select name="status" id="status" class="filter-field form-control uniform-multiselect" required="required">
      <option value="">Export Status</option>
      <option value="unexported">Un-Exported</option>
      <option value="exported">Exported</option>
    </select>
    <label class="form-label" style="margin-top: -10px;">Select Status</label>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="col-md-5" align="center">
                  <input placeholder="Start Date" type="text" onfocus = "(this.type = 'date')" class="form-control" name="startdate" id="startdate" />
                </div>
                    <div align="center" class="col-md-1"><span>To</span></div>
                <div class="col-md-5" align="center">
                      <input placeholder = "End Date" type="text" onfocus = "(this.type = 'date')" class="form-control col-md-3" name="enddate" id="enddate" />
                    </div>
              </div>
              <div class="col-md-2">
                <button class="btn btn-lg btn-primary waves-effect" id='filter'><i class="material-icons">filter_list</i> Filter Result</button>
              </div>
            </div>
            <table id="example1" class="table table-bordered table-striped table-hover delSelTable">
              <thead>
                <tr>
<th>No.</th>
<th>Recepient id no</th>
<th>Recepient Name</th>
<th>Address</th>
<th>Company Id</th>
<th><?php echo lang('create_date') ?></th>
<th><?php echo lang('Action'); ?></th>
                </tr>
              </thead>
              <tbody>
              <?php if (isset($div1099) && !empty($div1099)) {
    $i = 0;
    foreach ($div1099 as $key => $datava) {
        ?>
            <tr>
<td><?php echo ++$i; ?></td>
<td><?php echo isset($datava->recepient_id_no) ? $datava->recepient_id_no : ''; ?></td>

<td><?php echo isset($datava->first_name) ? $datava->first_name : ''; ?></td>

<td><?php echo isset($datava->address) ? $datava->address : ''; ?> <br /><?php echo isset($datava->city) ? $datava->city : ''; ?>
<?php echo isset($datava->state) ? $datava->state : ''; ?>
<?php echo isset($datava->zipcode) ? $datava->zipcode : ''; ?></td>
<td><?php print_r(compName($datava->FileSequence));?></td>
<td><?php echo isset($datava->created_at) ? date('Y-m-d', strtotime($datava->created_at)) : ''; ?></td>
<td><a style="cursor:pointer;" class="hrefid mClass1" href="<?php echo base_url(); ?>user/viewDivdata/<?php echo isset($datava->FileSequence) ? $datava->FileSequence : ''; ?>/<?php echo isset($datava->div_id) ? $datava->div_id : ''; ?>" title="View"><i class="material-icons font-20">visibility</i></a></td>
                </tr>

        <?php }

}?>
              </tbody>
            </table>
            <?php // echo "<PRE>";print_r($div1099);echo "</PRE>";?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
  <!-- /.content -->
<!--End Modal Crud -->
<script>
$(document).ready(function(){
  $('select').on('change', function (e) {
      $("#export").attr("disabled", true);
  });
  function getUrlVars()
   {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
  var id = getUrlVars()["id"];
  var type = getUrlVars()["type"];
  var status = getUrlVars()["status"];
  var from = getUrlVars()["from"];
  var to = getUrlVars()["to"];
          $('#selectcityAmin').val(id);
          $('#filetype').val(type).attr('selected', true);
          $('#status').val(status).attr('selected', true);
          $('#startdate').val(from).attr('selected', true);
          $('#enddate').val(to).attr('selected', true);
          //alert(status);
});
    $(function(){

          var tbody = $("#example1 tbody");
          if (tbody.children().length == 0) {
              $("#export").css("display", "none");
          }

      $('#filter').on('click', function () {
          var id = $('#selectcityAmin').val();
          var type = $('#filetype').val();
          var status = $('#status').val();
          var from = $('#startdate').val(); // get selected value
          var to = $('#enddate').val();
     // alert(url);
          if (id!='' && type!='' && status!='' & from!='' & to!='') { // require a URL
          url ='<?php echo base_url(); ?>user/div1099data/?id='+id+'&type='+type+'&status='+status+'&from='+from+'&to='+to;
          window.location.href = url; // redirect

          } else if (id!='' && type=='' && status=='' & from=='' & to=='') { // require a URL
          url ='<?php echo base_url(); ?>user/div1099data/?id='+id;
          window.location.href = url; // redirect

          } else if (id!='' && type!='' && status!='' & from=='' & to=='') { // require a URL
          url ='<?php echo base_url(); ?>user/div1099data/?id='+id+'&type='+type+'&status='+status;
          window.location.href = url; // redirect
          }
           else if(id=='' && type!='' && status=='' && from=='' && to=='') {
            url ='<?php echo base_url(); ?>user/div1099data/?type='+type;
            window.location.href = url;
          } else if(id=='' && type!='' && status!='' && from=='' && to=='') {

            url ='<?php echo base_url(); ?>user/div1099data/?type='+type+'&status='+status;
            window.location.href = url;
          } else if(id!='' && type!='' && status =='' && from=='' && to=='') {

            url ='<?php echo base_url(); ?>user/div1099data/?id='+id+'&type='+type;
            window.location.href = url;
          } else if(id!='' && type=='' && status!='' && from=='' && to=='') {

            url ='<?php echo base_url(); ?>user/div1099data/?id='+id+'&status='+status;
            window.location.href = url;
          } else if(id=='' && type== '' && status!='' && from=='' && to=='') {
            url ='<?php echo base_url(); ?>user/div1099data/?id='+id+'&type='+type+'&status='+status;
            window.location.href = url;
          }
          else if(id=='' && type!== '' && status!='' && from!=='' && to!=='') {
            //alert('gggg');
            url ='<?php echo base_url(); ?>user/div1099data/?type='+type+'&status='+status+'&from='+from+'&to='+to;
            window.location.href = url;
          }
          else if(id=='' && type== '' && status=='' && from!='' && to!='') {
            url ='<?php echo base_url(); ?>user/div1099data/?from='+from+'&to='+to;
            window.location.href = url;
          } else if(id!='' && type== '' && status=='' && from!='' && to!='') {

            url ='<?php echo base_url(); ?>user/div1099data/?id='+id+'&from='+from+'&to='+to;
            window.location.href = url;
          } else if(id!='' && type!= '' && status=='' && from!='' && to!='') {

            url ='<?php echo base_url(); ?>user/div1099data/?id='+id+'&type='+type+'&from='+from+'&to='+to;
            window.location.href = url;
          } else if(id=='' && type== '' && status!='' && from!='' && to!='') {
            url ='<?php echo base_url(); ?>user/div1099data/?status='+status+'&from='+from+'&to='+to;
            window.location.href = url;
          } else if(id!='' && type== '' && status!='' && from!='' && to!='') {

            url ='<?php echo base_url(); ?>user/div1099data/?id='+id+'&status='+status+'&from='+from+'&to='+to;
            window.location.href = url;
          } else{
        window.location.href ='<?php echo base_url(); ?>user/div1099data/';
    }
          //return false;
      });
    });
$('#export').click( function () {
          var id = $('#selectcityAmin').val();
          var type = $('#filetype').val();// get selected value
          var status = $('#status').val();
          var from = $('#startdate').val(); // get selected value
          var to = $('#enddate').val();
          if (id!='' || id=='') { // require a URL
            var table = $('#example1').dataTable();
            alert(table.fnGetData().length+' '+' Recipients data will be exported');
           url ='<?php echo base_url(); ?>user/export1099div?id='+ id +'&type='+ type +'&status='+status+'&from='+from+'&to='+to;
           window.location.href = url;
          } else {
            var table = $('#example1').dataTable();
            alert(table.fnGetData().length+' '+' Recipients data will be exported');
            window.location.href ='<?php echo base_url(); ?>user/export1099div';
    }
          return false;
      });
</script>
<script type="text/javascript">

  jQuery(document).ready(function($) {
   // $('#example1').DataTable();
       var table = $('#example1').DataTable({
          "sPaginationType": "full_numbers",
          "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "<?php echo lang('search'); ?>",
            "paginate": {
                "next": '<i class="material-icons">keyboard_arrow_right</i>',
                "previous": '<i class="material-icons">keyboard_arrow_left</i>',
                "first": '<i class="material-icons">first_page</i>',
                "last": '<i class="material-icons">last_page</i>'
            }
          },
          "iDisplayLength": 10,
          "aLengthMenu": [[10, 25, 50, 100,500,-1], [10, 25, 50,100,500,"<?php echo lang('all'); ?>"]],
          "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
          ]
        //   ,
        //   dom: 'Bfrtip',
        // buttons: [
        //     'csvHtml5',
        // ]
      });

    });

</script>