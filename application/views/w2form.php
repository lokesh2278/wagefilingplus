<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h4>W2 Payee Form Data</h4>
            <ul class="header-dropdown m-r--5">
            <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
            </div>
          <!-- /.box-header -->
          <div class="body">
             <div class="row">








<style>

.textEntry {
BORDER-TOP: 0px solid #333333;
    BORDER-RIGHT: 0px solid #000000;
    BORDER-BOTTOM: 1px solid #333333;
    BORDER-LEFT: 0px solid #333333;
    PADDING-TOP: 0px;
    PADDING-RIGHT: 0px;
    PADDING-BOTTOM: 0px;
    PADDING-LEFT: 5px;
    MARGIN: 0px;
    FONT-WEIGHT: normal;
    FONT-SIZE: 10pt;
    COLOR: #000000;
    FONT-STYLE: normal;
    FONT-FAMILY: Arial;
    margin-top: 12px;
    HEIGHT: 12px;
    TEXT-DECORATION: none;
	}

.smallEntry {
	BORDER-LEFT: 0px solid #333333;
	BORDER-RIGHT: 0px solid #000000;
	BORDER-TOP: 0px solid #333333;
	BORDER-BOTTOM: 1px solid #333333;
	PADDING-LEFT: 5px;
    PADDING-RIGHT: 0px;
	PADDING-TOP: 0px;
    PADDING-BOTTOM: 0px;
	MARGIN: 0px;
	FONT-FAMILY: Arial;
	FONT-WEIGHT: normal;
	FONT-SIZE: 08pt;
	FONT-STYLE: normal;
	TEXT-DECORATION: none;
	}
.ssn{padding-left: 3px; padding-top: 23px;}
.no{padding-top: 23px;color: black;}
.nnn td{line-height: 15px!important;}
.VerdanaSmall {    font-size: 12px;}
.Tiny {font-family: Verdana; font-size: 9px;}
.Tiny2 {font-family: Verdana; font-size: 8px;}

.BlueButton {width: 100px; white-space: nowrap; text-align: center; border-left: 2px solid #d6d6d6;
           border-right: 3px solid #797979; border-top: 2px solid #d6d6d6; border-bottom: 3px solid #797979;
           margin: 5px; padding-left: 8px; padding-right: 8px; padding-top: 1px; padding-bottom: 1px;
           background: #145aa3}

.WhiteButtonText {font-weight: bolder; font-size: 13px; color: #fff; font-family: verdana;  text-decoration: none}
input.check {
    left: auto !important;
    opacity: 1 !important;
	margin: 0 0 0 -40px;
}
</style>

<!-- Include File="prebody_inc.asp" -->

<form name="mainform" method="post" action="addformw2" onSubmit="return valall();" onKeydown="FormChanged=true;">
  <input type="hidden" name="w2_id" value="<?=!empty($v_details[0]->w2_id) ? $v_details[0]->w2_id : '';?>" />
      <input type="hidden" name="c_id" value="<?=!empty($c_details[0]->CustID) ? $c_details[0]->CustID : '';?>"/>
 <table class="VerdanaSmall" border="0" background="<?php echo str_replace('index.php/', '', base_url()); ?>assets/images/W2-14.gif" width="870" cellspacing="0" cellpadding="0" style=" no-repeat;background-size:870px 680px; background-color:#FFFFFF; margin: auto;background-repeat: no-repeat;">
        <tr valign="bottom">
          <td width="11" height="39"><img src="images/spacer.gif" width="11" height="39" /></td>
          <td width="53" height="39">&nbsp;</td>
          <td width="144" height="39">&nbsp;</td>
          <td height="39" colspan="3" class="ssn">
			<input type="text" maxlength="11" value="<?php echo !empty($v_details) && !empty($v_details[0]->SSN) ? $v_details[0]->SSN : ''; ?>" name="SSN" size="20" class="textEntry" style="margin-left: 11px;width: 171px;"  onChange="valssn()" tabindex="1" /></td>
          <td width="85" height="39">&nbsp;</td>
          <td width="22" height="39">&nbsp;</td>
          <td width="121" height="39">Calculate Boxes 3, 4, 5, 6 <input type="checkbox" name="calcbox" value="unchecked" onClick="calclick()" /></td>
          <td width="6" height="39">&nbsp;</td>
          <td width="49" height="39">&nbsp;</td>
          <td width="79" height="39">Void <input type="checkbox" class="check" name="voidbox" value="1" <?php if (isset($v_details[0]->voidbox) && $v_details[0]->voidbox == '1') {echo 'checked';} else {echo '';}?> /></td>
          <td height="39" width="81">Delete<input type="checkbox" class="check" name="deletebox" value="1" <?php if (isset($v_details[0]->is_deleted) && $v_details[0]->is_deleted == '0') {echo '';} else {echo 'disabled';}?>>

          </td>
        </tr>
        <tr valign="bottom">
          <td width="11"><img src="images/spacer.gif" width="1" height="38" /></td>
          <td colspan="2" class="no"><?=$c_details[0]->c_employee_ein?></td>
          <td width="62">&nbsp;</td>
          <td width="21">&nbsp;</td>
          <td width="140">&nbsp;</td>
          <td width="75">&nbsp;</td>
          <td colspan="2"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box1) ? $v_details[0]->box1 : ''; ?>" name="box1" style="width: 152px;" size="20" class="textEntry"  onchange="valdigits(document.mainform.box1)" onch tabindex="13" /></td>
          <td width="6">&nbsp;</td>
          <td colspan="3"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box2) ? $v_details[0]->box2 : ''; ?>" name="box2" size="20" class="textEntry"	onchange="valdigits(document.mainform.box2)" onFocus="enterbox2()" tabindex="14" /></td>
        </tr>
        <tr valign="bottom">
          <td width="11" height="40">&nbsp;</td>
          <td height="115" colspan="5" rowspan="3" style="line-height: 22px!important;">
            <p><?=$c_details[0]->FileName?><?=$c_details[0]->c_name?></p>
            <p><?=$c_details[0]->c_address?>&nbsp;,<?=$c_details[0]->c_address_2?></p>
            <p><?=$c_details[0]->c_state?>&nbsp;,<?=$c_details[0]->c_zip?></p>
          </td>
          <td width="85" height="40">&nbsp;</td>
          <td height="40" colspan="2" style="padding-top: 21px;"><input style="width: 155px;" type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box3) ? $v_details[0]->box3 : ''; ?>" name="box3" size="20" class="textEntry"
		onchange="valdigits(document.mainform.box3)" tabindex="15" /></td>
          <td width="6" height="40"><img src="images/spacer.gif" width="1" height="42" /></td>
          <td height="40" colspan="3"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box4) ? $v_details[0]->box4 : ''; ?>" name="box4" size="20" class="textEntry"
		onchange="valdigits(document.mainform.box4)" tabindex="16" /></td>
        </tr>
        <tr valign="bottom">
          <td width="11" height="37">&nbsp;</td>
          <td width="85" height="37">&nbsp;</td>
          <td height="37" colspan="2" style="padding-top: 22px;"><input type="text" style="width: 160px;" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box5) ? $v_details[0]->box5 : ''; ?>" name="box5" size="20" class="textEntry"
		onchange="valdigits(document.mainform.box5)" tabindex="17" /></td>
          <td width="6" height="37"><img src="images/spacer.gif" width="1" height="37" /></td>
          <td height="37" colspan="3"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box6) ? $v_details[0]->box6 : ''; ?>" name="box6" size="20" class="textEntry"
		onchange="valdigits(document.mainform.box6)" tabindex="18" /></td>
        </tr>
        <tr valign="bottom">
          <td width="11" height="38">&nbsp;</td>
          <td width="85" height="38">&nbsp;</td>
          <td height="38" colspan="2" style="padding-top: 22px;"><input type="text" style="width: 160px;" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box7) ? $v_details[0]->box7 : ''; ?>" name="box7" size="20" class="textEntry"
		onchange="valdigits(document.mainform.box7)" tabindex="19" /></td>
          <td width="6" height="38"><img src="images/spacer.gif" width="1" height="38" /></td>
          <td height="38" colspan="3"><input type="text" maxlength="14" value="<?php echo !empty($v_details[0]->box8) ? $v_details[0]->box8 : ''; ?>" name="box8" size="20" class="textEntry"
		onchange="valdigits(document.mainform.box8)" tabindex="20" /></td>
        </tr>
        <tr valign="bottom">
          <td width="11" height="39"><img src="images/spacer.gif" width="1" height="39" /></td>
          <td height="39" colspan="2"><input type="text" maxlength="14" value="<?php echo isset($v_details[0]->boxA) ? $v_details[0]->boxA : ''; ?>" name="boxA" size="20" class="textEntry"  /></td>
          <td width="62" height="39">&nbsp;</td>
          <td width="21" height="39">&nbsp;</td>
          <td width="140" height="39">&nbsp;</td>
          <td width="85" height="39">&nbsp;</td>
          <td height="39" colspan="2" style="padding-top: 22px;"><input style="width: 160px;" type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box9) ? $v_details[0]->box9 : ''; ?>" name="box9" size="20" class="textEntry" readonly
		onchange="valdigits(document.mainform.box9)"  /></td>
          <td width="6" height="39">&nbsp;</td>
          <td height="39" colspan="3"><input type="text" maxlength="14" value="<?php echo isset($v_details[0]->box10) ? $v_details[0]->box10 : ''; ?>" name="box10" size="20" class="textEntry"
		onchange="valdigits(document.mainform.box10)"  /></td>
        </tr>
        <tr valign="bottom">
          <td width="11" height="38"><img src="images/spacer.gif" width="1" height="38" /></td>
          <td height="38" colspan="3">&nbsp;
              <input type="text" maxlength="15" value="<?php echo !empty($v_details) && !empty($v_details[0]->FName) ? $v_details[0]->FName : ''; ?>" name="FName" size="16" class="textEntry" tabindex="2" />
            &nbsp;|
            <input type="text" maxlength="15" value="<?php echo !empty($v_details) && !empty($v_details[0]->MName) ? $v_details[0]->MName : ''; ?>" name="MName" size="2" class="textEntry" tabindex="3" /></td>
          <td height="38" colspan="2">
			<input type="text" maxlength="20" value="<?php echo !empty($v_details) && !empty($v_details[0]->LName) ? $v_details[0]->LName : ''; ?>" name="LName" size="20" class="textEntry" tabindex="4" /></td>
          <td width="85" height="38"><div class="Tiny2">Suffix<br />
          </div>
              <input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->Suf) ? $v_details[0]->Suf : ''; ?>" name="Suf" size="2" class="textEntry" tabindex="5" /></td>
          <td height="38" colspan="2"><input type="text" maxlength="14" value="<?php echo isset($v_details[0]->box11) ? $v_details[0]->box11 : ''; ?>"  name="box11" size="20" class="textEntry" onChange="valdigits(document.mainform.box11)" style="width:167px;margin-left:0px;"/></td>
          <td width="6" height="38">&nbsp;</td>
          <td width="49" height="38"><input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12A) ? $v_details[0]->box12A : ''; ?>" name="box12A" size="2" class="textEntry"
		       onchange="valbox12(document.mainform.box12A)" /></td>
          <td height="38" colspan="2" style="padding-left: 7px;"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12AM) ? $v_details[0]->box12AM : ''; ?>" name="box12AM" size="14" class="textEntry"
		onchange="valdigits(document.mainform.box12AM)" /></td>
        </tr>
        <tr valign="bottom">
          <td height="130">&nbsp;</td>
          <td colspan="12" valign="top"><table width="860" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="431" valign="top" style="padding-top:5px">


              <table class="tiny2 nnn" style="margin-top:15px" border="0" width="425" cellspacing="0" cellpadding="0">
                  <tr class="Tiny2">
                    <td width="258"> Attn, Suite, Room<br />
                        <input type="text" maxlength="22" value="<?php echo !empty($v_details) && !empty($v_details[0]->Addr1) ? $v_details[0]->Addr1 : ''; ?>" name="Addr1" style="width:200px; height:13px" class="smallEntry" tabindex="7" />                    </td>
                    <td width="57">&nbsp;</td>
                    <td width="46">&nbsp;</td>
                    <td width="64">&nbsp;</td>
                  </tr>
                  <tr class="Tiny2">
                    <td>Street or PO Box <br />
                        <input type="text" maxlength="22" value="<?php echo !empty($v_details) && !empty($v_details[0]->Addr2) ? $v_details[0]->Addr2 : ''; ?>" name="Addr2" style="width:200px; height:13px" class="smallEntry" tabindex="8" />
                      &nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr class="Tiny2">
                    <td>City<br />
                        <input type="text" maxlength="22" value="<?php echo !empty($v_details) && !empty($v_details[0]->FName) ? $v_details[0]->FName : ''; ?>" name="City" style="width:200px; height:13px" class="smallEntry" TabIndex="9"/>
                      &nbsp;</td>
                    <td>State<br />
                        <input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->State) ? $v_details[0]->State : ''; ?>" name="State" size="2" class="smallEntry" style="height:13px" onChange="valstate(document.mainform.State)" tabindex="10" /></td>
                    <td>Zip5<br />
                        <input type="text" maxlength="5" value="<?php echo !empty($v_details) && !empty($v_details[0]->Zip1) ? $v_details[0]->Zip1 : ''; ?>" name="Zip1" size="2" class="smallEntry" style="height:13px" onChange="valzip1()" tabindex="11" /></td>
                    <td>Zip4<br />
                        <input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->Zip2) ? $v_details[0]->Zip2 : ''; ?>" name="Zip2" size="2" class="smallEntry" style="height:13px" tabindex="12" /></td>
                  </tr>
                  <tr class="Tiny2">
                    <td>Foreign State<br />
                      <input type="text" maxlength="22" value="<?php echo !empty($v_details) && !empty($v_details[0]->FState) ? $v_details[0]->FState : ''; ?>" name="FState" style="width:170px; height:13px" class="smallEntry" />
                      &nbsp;</td>
                    <td colspan="2"> Foreign Zip<br />
                      &nbsp;
                        <input type="text" maxlength="15" value="<?php echo !empty($v_details) && !empty($v_details[0]->FZip) ? $v_details[0]->FZip : ''; ?>" name="FZip" size="10" style="height:13px" class="smallEntry" />
                      &nbsp;</td>
                    <td>Country CD
                      <br />
                      <input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->FCode) ? $v_details[0]->FCode : ''; ?>" name="FCode" size="2" class="smallEntry" style="height:13px" onChange="valcountry(document.mainform.FCode)" />
                      &nbsp;</td>
                  </tr>
              </table>
              </td>
              <td width="218" valign="top" style="padding-top:24px">

              <table  border="0"  cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="230">
                    <div align="right" style="padding-top: 7px;">
                      <input type="radio" name="statutory_retirement_thirdparty" class="check" value="1" <?php if (isset($v_details[0]->statutory_retirement_thirdparty) && $v_details[0]->statutory_retirement_thirdparty == '1') {echo 'checked';} else {echo '';}?>/>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="radio" name="statutory_retirement_thirdparty" class="check" value="2" <?php if (isset($v_details[0]->statutory_retirement_thirdparty) && $v_details[0]->statutory_retirement_thirdparty == '2') {echo 'checked';} else {echo '';}?>/>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="radio" name="statutory_retirement_thirdparty" class="check" value="3" <?php if (isset($v_details[0]->statutory_retirement_thirdparty) && $v_details[0]->statutory_retirement_thirdparty == '3') {echo 'checked';} else {echo '';}?>/>  &nbsp;&nbsp;&nbsp; </div></td>
                  </tr>
                  <tr>
                    <td style="padding-top:15px"><div align="right" style="padding-left: 55px;">
                        <input type="text" maxlength="15" value="<?php echo !empty($v_details) && !empty($v_details[0]->box14A) ? $v_details[0]->box14A : ''; ?>" name="box14A" size="20" class="textEntry" />
                        <input type="text" maxlength="15" value="<?php echo !empty($v_details) && !empty($v_details[0]->box14B) ? $v_details[0]->box14B : ''; ?>" name="box14B" size="20" class="textEntry" />
                        <input type="text" maxlength="15" value="<?php echo !empty($v_details) && !empty($v_details[0]->box14C) ? $v_details[0]->box14C : ''; ?>" name="box14C" size="20" class="textEntry" />
                        <input type="text" maxlength="15" value="<?php echo !empty($v_details) && !empty($v_details[0]->box14D) ? $v_details[0]->box14D : ''; ?>" name="box14D" size="20" class="textEntry" />
                    </div></td>
                  </tr>
              </table>


              </td>
              <td width="214" valign="top" style="padding-top:13px">
              <table   border="0"  cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="61" style="padding-top: 4px;padding-left: 10px; "><input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12B) ? $v_details[0]->box12B : ''; ?>" name="box12B" size="2" class="textEntry"
   		       onchange="valbox12(document.mainform.box12B)" /></td>
                    <td width="129"><input type="text" maxlength="14" style=" width: 112px;margin-left: 15px;
    margin-top: 7px;" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12BM) ? $v_details[0]->box12BM : ''; ?>" name="box12BM" size="14" class="textEntry"
		onchange="valdigits(document.mainform.box12BM)" /></td>
                  </tr>
                  <tr>
                    <td  style="padding-top: 22px;padding-left: 10px;"><input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12C) ? $v_details[0]->box12C : ''; ?>" name="box12C" size="2" class="textEntry"
               onchange="valbox12(document.mainform.box12C)" /></td>
                    <td valign="bottom"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12CM) ? $v_details[0]->box12CM : ''; ?>" style=" width: 116px;margin-left: 12px;" name="box12CM" size="14" class="textEntry"
		onchange="valdigits(document.mainform.box12CM)" /></td>
                  </tr>
                  <tr>
                    <td style="padding-top: 22px;padding-left: 10px;"><input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12D) ? $v_details[0]->box12D : ''; ?>" name="box12D" size="2" class="textEntry"
		onchange="valbox12(document.mainform.box12D)" /></td>
                    <td valign="bottom"><input type="text" style="width: 115px;margin-left: 12px;" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12DM) ? $v_details[0]->box12DM : ''; ?>" name="box12DM" size="14" class="textEntry"
		onchange="valdigits(document.mainform.box12DM)" /></td>
                  </tr>
              </table></td>
            </tr>
          </table></td>
          </tr>
        <tr valign="bottom">
          <td width="11"><img src="images/spacer.gif" width="1" height="38" /></td>
          <td width="53"><input type="text" maxlength="3" value="<?php echo !empty($v_details) && !empty($v_details[0]->box15A) ? $v_details[0]->box15A : ''; ?>" name="box15A"  style="margin-top: 22px;width: 36px;" size="1" class="textEntry"
		onchange="valstate(document.mainform.box15A)" Tabindex="21" /></td>
          <td colspan="3"><input type="text" maxlength="20" value="<?php echo !empty($v_details) && !empty($v_details[0]->box15B) ? $v_details[0]->box15B : ''; ?>" name="box15B" size="20" class="textEntry" tabindex="22" /></td>
          <td width="140"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box16) ? $v_details[0]->box16 : ''; ?>" name="box16" style="width: 120px;margin-left: 14px;" size="14" class="textEntry"
		onchange="valdigits(document.mainform.box16)" tabindex="23" /></td>
          <td colspan="2"><input type="text" maxlength="14" style="width:106px;" value="<?php echo !empty($v_details) && !empty($v_details[0]->box17) ? $v_details[0]->box17 : ''; ?>" name="box17" size="14" class="textEntry"
		onchange="valdigits(document.mainform.box17)" tabindex="24" /></td>
          <td width="121"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box18) ? $v_details[0]->box18 : ''; ?>" name="box18" size="14" class="textEntry"
		onchange="valdigits(document.mainform.box18)" tabindex="25"/></td>
          <td colspan="3"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->box19) ? $v_details[0]->box19 : ''; ?>" name="box19" size="14" class="textEntry"
		onchange="valdigits(document.mainform.box19)" tabindex="26" style="width: 108px;margin-right: 24px;"/></td>
          <td width="81"><input type="text" maxlength="4" value="<?php echo !empty($v_details) && !empty($v_details[0]->box20) ? $v_details[0]->box20 : ''; ?>"  name="box20" size="2" class="textEntry" /></td>
        </tr>
        <tr valign="bottom">
          <td width="11" height="38">&nbsp;</td>
          <td width="53" height="38" style="padding-bottom: 1px">
          <input type="text"  maxlength="3" value="<?php echo isset($v_details[0]->box21A) ? $v_details[0]->box21A : ''; ?>" name="box21A" size="1" class="textEntry"		onchange="valstate(document.mainform.box21A)" style="margin-top: 22px;width: 36px;" /></td>
          <td height="38" colspan="3">
          <input type="text" maxlength="20" value="<?php echo isset($v_details[0]->box21B) ? $v_details[0]->box21B : ''; ?>" name="box21B" style="margin-top: 23px;" size="20" class="textEntry" /></td>
          <td width="140" height="38">
          <input type="text" maxlength="14" value="<?php echo isset($v_details[0]->box22) ? $v_details[0]->box22 : ''; ?>" name="box22" size="14" class="textEntry"		onchange="valdigits(document.mainform.box22)" style="width:120px;margin-left: 14px !important;"/></td>
          <td height="38" colspan="2">
          <input type="text" maxlength="14" value="<?php echo isset($v_details[0]->box23) ? $v_details[0]->box23 : ''; ?>" name="box23" size="14" class="textEntry"		onchange="valdigits(document.mainform.box23)"  style="width: 113px;margin-left: 2px !important;"/></td>
          <td width="121" height="38">
          <input type="text" maxlength="14" value="<?php echo isset($v_details[0]->box24) ? $v_details[0]->box24 : ''; ?>" name="box24" size="14" class="textEntry"		onchange="valdigits(document.mainform.box24)" style="width: 121px;margin-left: 8px !important;"/></td>
          <td height="38" colspan="3">
          <input type="text" maxlength="14" value="<?php echo isset($v_details[0]->box25) ? $v_details[0]->box25 : ''; ?>" name="box25" size="14" class="textEntry"		onchange="valdigits(document.mainform.box25)" style="width: 110px;margin-left: 0px !important;"/></td>
          <td height="38" width="81">
          <input type="text" maxlength="4" value="<?php echo isset($v_details[0]->box26) ? $v_details[0]->box26 : ''; ?>" name="box26" size="2" class="textEntry" /></td>
        </tr>
        <tr valign="bottom" style="height:111px;">
          <td width="11" height="75">&nbsp;</td>
          <td width="53" height="75">&nbsp;</td>
          <td width="144" height="75">&nbsp;</td>
          <td width="62" height="75">&nbsp;</td>
          <td width="21" height="75">&nbsp;</td>
          <td width="140" height="75">&nbsp;</td>
          <td width="20" height="75" style="color: red;" align="center">
          <div style="font-size: 30px;font-weight:bold;"><?=$c_details[0]->TaxYear?></div><br /><p style="font-size: 20px; text-align:center;">1129</p></td>
        </tr>
      </table>

</form>

<!-- INCLUDE FILE="footer_inc.asp" -->








 </div>
             <?php // echo"<PRE>"; print_r($c_companies);echo"</PRE>";?>
           </div>

          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
<style>.page-loader-wrapper {
    display: none;
}
ul#myTab41 h4 {
    font-weight: 500;
    color: #000000;
    background: #6d6d6d33;
    padding: 6px 20px;
    margin-left: -20px;
    margin-top: 15px;
}
ul#myTab41 li b {
    font-weight: 500;
    color: #006dbb;
}
ul#myTab41 li {
    line-height: 30px;
}
</style>