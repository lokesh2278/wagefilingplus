<footer class="footer-2 text-center-xs">
    <div class="row text-center">
      <div class="col-sm-12">
        <span class="fade-half">
          © Copyright 2018 W-9's.com LLC All rights reserved.
        </span>
      </div>
    </div>
  <?php
if (!empty($this->session->get_userdata()['alert_msg'])) {
    ?>
  <script>
  $(document).ready(function() {

  $msg = '<?php echo isset($this->session->get_userdata()['alert_msg']['msg']) ? $this->session->get_userdata()['alert_msg']['msg'] : ""; ?>';
  $typ = '<?php echo isset($this->session->get_userdata()['alert_msg']['type']) ? $this->session->get_userdata()['alert_msg']['type'] : ""; ?>';
  notiCustom($msg, $typ);
  });
  function notiCustom($msg, $type) {
  $.notify({
  message: $msg,
  },{
  type: $type,
    delay: 0,
    clickToHide: true,
  });
  }
  </script>
  <?php
unset($_SESSION['alert_msg']);
}?>
</footer>
<style>
  #main{
    top: 40px;
  }
  .right-side{
    min-height: 386px;
  }
  .btn{
    padding: 0px 12px;
  }
</style>
<script src="<?php echo iaBase(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo iaBase(); ?>assets/js/jquery.tablesorter.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?=iaBase()?>assets/js/tablefilter.js"></script>
<script src="<?=iaBase()?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="<?=iaBase()?>assets/js/flexslider.min.js"></script>
<script src="<?=iaBase()?>assets/js/parallax.js"></script>
<script src="<?=iaBase()?>assets/js/scripts.js"></script>
</body>
</html>