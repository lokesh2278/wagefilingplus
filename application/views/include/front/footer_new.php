<footer class="footer-2 bg-dark text-center-xs">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <a href="#"><img class="image-xxs fade-half" alt="Pic" src="<?php echo iaBase(); ?>assets/images/logo-new-md-nq8_1519468661.png"></a>
      </div>
      <div class="col-sm-4 text-center">
        <span class="fade-half">
          © Copyright 2018 WageFilingPlus LLC All rights reserved.
        </span>
      </div>
      <div class="col-sm-4 text-right text-center-xs">
        <ul class="list-inline social-list">
          <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
          <li><a href="#"><i class="ti-facebook"></i></a></li>
          <li><a href="#"><i class="ti-dribbble"></i></a></li>
          <li><a href="#"><i class="ti-vimeo-alt"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <?php
if (!empty($this->session->get_userdata()['alert_msg'])) {
    ?>
  <script>
  $(document).ready(function() {

  $msg = '<?php echo $this->session->get_userdata()['alert_msg']['msg']; ?>';
  $typ = '<?php echo $this->session->get_userdata()['alert_msg']['type']; ?>';
  notiCustom($msg, $typ);
  });
  function notiCustom($msg, $type) {
  $.notify({
  message: $msg,
  },{
  type: $type,
  delay: 15000,
  });
  }
  </script>
  <?php
unset($_SESSION['alert_msg']);
}?>
</footer>
<script src="<?php echo iaBase(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo iaBase(); ?>assets/js/jquery.tablesorter.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?=iaBase()?>assets/js/tablefilter.js"></script>
<script src="<?=iaBase()?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="<?=iaBase()?>assets/js/flexslider.min.js"></script>
<script src="<?=iaBase()?>assets/js/parallax.js"></script>
<script src="<?=iaBase()?>assets/js/scripts.js"></script>
</body>
</html>