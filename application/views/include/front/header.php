<!DOCTYPE html>

<html>

  <head>



    <meta charset="UTF-8">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">



   <!--  <meta name="keywords" content=" W9, w-9, irs form w-9, issue w-9, contractor forms, w-9 form online, how to complete w-9, report w9, 2018 w9, send contractor w-9, w9 pdf, fillable w9, when do I issue W-9, w9 solicitation, w9 mailing service, electronic w9, file w-9 online, w9s, W-9 instructions, how to fill out at W-9, non-profit w-9, contractor w-9 form, who issues a w-9, Information about Form W-9, Fill out W-9 Form instantly! Download printable"> -->



    <meta name="description" content="Instantly Prepare, Generate and e-File 1099-MISC Forms, Corrections and Past Year Forms. No Forms or Software Required - IRS Approved.  " />

    <?php $setting = getSetting();?>

    <title>WageFilingPlus.com | Easily Prepare W-2 / 1099-MISC Forms</title>

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/home.css">

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/application.css">

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/blaze.css">

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/scss/style.css?v=1.2">

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/style_table.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/theme.css">

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/flexslider.css">

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/themify-icons.css">

    <link href='https://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/frontcustom.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="/favicon.ico" rel="shortcut icon">

    <script src="<?php echo iaBase(); ?>assets/js/jquery.min.js"></script>
<link href="<?php echo iaBase(); ?>assets/css/site.css" rel="stylesheet"/>
    <!-- <link href="<?php echo iaBase(); ?>assets/css/jquery.dataTables.css" rel="stylesheet"/> -->
    <link href="<?php echo iaBase(); ?>assets/css/responsive.css" rel="stylesheet"/>
    <style> ?>

    .lagAm .btn-group.bootstrap-select {margin-top: 8%;}

    #rightsidebar .slimScrollDiv{height: 540px !important;}

    #rightsidebar .slimScrollDiv ul{height: 540px !important;}

    .sidebar .menu .list a.right0{width: 13% !important; right: 0;}

    </style>

  </head>

  <!-- Google Tag Manager -->

    <script>

      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

      new Date().getTime(),event:'gtm.js'});var

      f=d.getElementsByTagName(s)[0],

      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

        })(window,document,'script','dataLayer','GTM-WCPF79T');

      </script>

      <!-- End Google Tag Manager -->

  <?php

$this->load->helper('cookie');

$cookie = get_cookie('theme_color');

if ((!isset($cookie)) || (isset($cookie) && empty($cookie))) {$cookie = 'theme-blue';}

?>

  <body class="<?php echo $cookie; ?>" data-base-url="<?php echo base_url(); ?>">



    <!-- Google Tag Manager (noscript) -->

    <noscript><iframesrc="https://www.googletagmanager.com/ns.html?id=GTM-WCPF79T" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <!-- End Google Tag Manager (noscript) -->



    <!-- Page Loader -->

    <div class="nav-container">

      <nav>

        <div class="nav-bar">

          <div class="module left">

            <a href="<?=base_url();?>">

              <img class="logo logo-light visible-sm visible-xs" alt="Foundry" src="<?php echo iaBase(); ?>assets/images/wf-header-logo.png">

              <img class="logo logo-dark hidden-sm hidden-xs" alt="Foundry" src="<?php echo iaBase(); ?>assets/images/wf-header-logo.png">

            </a>

          </div>

          <div class="module widget-handle mobile-toggle right visible-sm visible-xs">

            <i class="ti-menu"></i>

          </div>

          <div class="module-group right">

            <div class="module left">

              <ul class="menu">
                <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>>
                  <a href="<?=base_url()?>">Home</a>
                </li>
               <!--  <li>

                  <a href="<?=base_url('services')?>">Services</a>

                </li> -->

                 <?php if (isset($this->session->get_userdata()['user_details'][0]->CustID)) {?>

                <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>><a href="<?=base_url('blaze');?>"> Main Menu</a></li>
                 <?php } else {?>
                 <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>><a href="<?php echo base_url('signup'); ?>">Start Filing</a></li>
            <?php }?>

                 <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>>
                   <a href="<?=base_url('file-a-correction')?>">File A Correction</a>
                 </li>

                 <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>>
                   <a href="<?=base_url('filepastyear')?>">File Past Year Form</a>
                 </li>

                 <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>>
                   <a href="<?=base_url('tinmatching')?>">Tin Checking</a>
                 </li>
                 <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>>
                   <a href="<?=base_url('issue-w9')?>">Issue W9</a>
                 </li>
                <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>>
                  <a href="<?=base_url('faq')?>">Faq</a>
                </li>
                <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>>
                  <a href="<?=base_url('contact-us')?>">Contact Us</a>
                </li>
                <?php if (isset($this->session->get_userdata()['user_details'][0]->CustID)) {?>
                <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>><a href="<?php echo base_url('logout'); ?>">Sign Out</a></li>
                 <?php } else {?>
                 <li <?php if (!empty($this->session->userdata('admin_back'))) { echo 'hidden'; }?>><a href="<?php echo base_url('login'); ?>">Login</a></li>
            <?php }?>
             <?php if (!empty($this->session->userdata('admin_back'))) {?>

                <li><a class="btn btn-sm" style="color: #ffffff !important; background-color: #337ab7 !important; margin-top: 10px !important;" href="<?=base_url('adminBack');?>/<?=$this->session->userdata('admin_back')?>"><i class="glyphicon glyphicon-chevron-left"></i> Back Admin</a></li>



             <?php }?>

              </ul>

            </div>

          </div>

        </div>

      </nav>

    </div>