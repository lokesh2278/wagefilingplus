<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $setting = getSetting();?>
    <title>onlineW-9</title>
    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/home.css">
    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/application.css">
    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/blaze.css">
    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/style_table.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/theme.css">
    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/themify-icons.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/frontcustom.css">
    <script src="<?php echo iaBase(); ?>assets/js/jquery.min.js"></script>
    <style> ?>
    .lagAm .btn-group.bootstrap-select {margin-top: 8%;}
    #rightsidebar .slimScrollDiv{height: 540px !important;}
    #rightsidebar .slimScrollDiv ul{height: 540px !important;}
    .sidebar .menu .list a.right0{width: 13% !important; right: 0;}
    </style>
  </head>
  <?php
$this->load->helper('cookie');
$cookie = get_cookie('theme_color');
if ((!isset($cookie)) || (isset($cookie) && empty($cookie))) {$cookie = 'theme-blue';}
?>
  <body class="<?php echo $cookie; ?>" data-base-url="<?php echo base_url(); ?>">
    <!-- Page Loader -->
    <div class="nav-container">
      <nav>
        <div class="nav-utility">
          <div class="module left">
            <i class="ti-location-arrow">&nbsp;</i>
            <span class="sub">Lorem ipsum dolor sit amet</span>
          </div>
          <div class="module left">
            <i class="ti-email">&nbsp;</i>
            <span class="sub">hello@example.com</span>
          </div>
          <div class="module right">
            <?php if (isset($this->session->get_userdata()['user_details'][0]->CustID)) {?>
            <a class="btn btn-sm" href="<?php echo base_url('logout'); ?>">Sign Out</a>
            <?php } else {?>
            <a class="btn btn-sm" href="<?php echo base_url('login'); ?>">Sign In</a>
            <?php }?>
          </div>
        </div>
        <div class="nav-bar">
          <div class="module left">
            <a href="<?=base_url();?>">
              <img class="logo logo-light" alt="Foundry" src="<?php echo iaBase(); ?>assets/images/logo-new-md-nq8.png">
              <img class="logo logo-dark" alt="Foundry" src="<?php echo iaBase(); ?>assets/images/logo-new-md-nq8.png">
            </a>
          </div>
          <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
            <i class="ti-menu"></i>
          </div>
          <div class="module-group right">
            <div class="module left">
              <ul class="menu">
                <li>
                  <a href="<?=base_url('how-it-works')?>">How it works</a>
                </li>
                <li>
                  <a href="<?=base_url('pricing')?>">Pricing</a>
                </li>
                <li>
                  <a href="<?=base_url('contact-us')?>">Contact Us</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>