<style>
  .modal .modal-footer {
    background-color: transparent !important;
}
.modal-footer {
    padding: 15px;
    text-align: right;
    border-top: 1px solid #e5e5e5 !important;
}
.modal-header h4 {
  font-family: "Raleway","Helvetica Neue",Helvetica,Arial,sans-serif !important;
  font-size: 24px !important;
  font-weight: 100 !important;
}
.modal-content {
    position: relative;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px !important;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
}
</style>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h2>Bulk Tin Request Data</h2>
            <ul class="header-dropdown m-r--5">
              <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
          </div>
          <!-- /.box-header -->
          <div class="body table-responsive">
            <div class="row filter-row">
              <div class="col-md-3">
                <div class="form-group form-float">
                  <div class="form-line">
    <select name="userid" id="userid" class="filter-field form-control uniform-multiselect" >
      <option value="">All </option>
      <?php if (isset($c_data) && !empty($c_data)) {

    foreach ($c_data as $cid => $daaacc) {
        $s = $this->uri->segment(3);
        ?><option <?php if ($s == $daaacc->CustID) {echo 'selected="selected" ';}?> value="<?php echo isset($daaacc->CustID) ? $daaacc->CustID : ''; ?>" > <?php echo isset($daaacc->name) ? $daaacc->name : ''; ?> <?php echo isset($daaacc->Ucompany) ? $daaacc->Ucompany : ''; ?></option>
    <?php }
}?>
    </select>
    <label class="form-label" style="margin-top: -10px;">Select User</label>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group form-float">
                  <div class="form-line">
    <select name="status" id="status" class="filter-field form-control uniform-multiselect" >
      <option value="">Status</option>
      <option value="success">Success</option>
      <option value="pending">Pending</option>
      <option value="in-progress">In Progress</option>
    </select>
    <label class="form-label" style="margin-top: -10px;">Select Status</label>
                  </div>
                </div>
              </div>
              <div class="com-md-3">
                <button type="button" id="filter" class="btn btn-sm btn-primary waves-effect"> <i class="fa fa-filter" aria-hidden="true"></i> Filter</button>
              </div>

            </div>
            <table id="example1" class="table table-bordered table-striped table-hover delSelTable">
              <thead>
                <tr>
<th>S.no</th>
<th><?php echo lang('name') ?></th>
<th>Email</th>
<th>Phone</th>
<th>Status</th>
<th><?php echo lang('create_date') ?></th>
<th><?php echo lang('download'); ?></th>
                </tr>
              </thead>
              <tbody>
              <?php if (isset($w2data) && !empty($w2data)) {
    $i = 0;
    foreach ($w2data as $key => $datava) {
        $fileid = encrypt_decrypt('encrypt', $datava->bulk_req_id);

        ?>
        <!-- if ($datava->status == 'pending' || $datava->status == 'in-progress') -->
					  <tr>
<td><?php echo ++$i; ?></td>
<td><?php echo isset($datava->name) ? $datava->name : ''; ?></td>
<td><?php echo isset($datava->email) ? $datava->email : ''; ?></td>
<td><?php echo isset($datava->phone) ? $datava->phone : ''; ?></td>
<td>
  <?php echo isset($datava->status) ? (ucwords(str_replace("-", " ", $datava->status))) . ' ' : ''; ?>
  <?php if ($datava->status == 'error' || $datava->status == 'pending') {?>
<a href="<?=base_url()?>user/changeBulkStatus/<?=$datava->bulk_req_id?>/in-progress"><i class="fa fa-pencil" title="Change Status"></i></a><?php } else {?>
<a href="#" class="btn btn-link btn-sm btn-status-link" data-toggle="modal" data-target="#status-upload<?=$datava->bulk_req_id?>"><i class="fa fa-pencil" title="Change Status"></i></a>
<?php }?>
<!-- Modal -->
    <div class="modal fade" id="status-upload<?=$datava->bulk_req_id?>" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

          <div class="modal-header" style="padding: 15px; background-color: #25a9bd !important; color: #ffffff !important; border-bottom: 1px solid #e5e5e5; border-top-left-radius: 5px !important; border-top-right-radius: 5px !important;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Upload File For Bulk Tin Check</h4>
          </div>
          <div class="modal-body">

            <?php echo form_open_multipart('user/do_upload'); ?>
            <form action="" method="post" name="form1">
              <div class="row">
                <div class="col-md-6 col-md-offset-3">
                  <div class="input-group">
                    <input type="hidden" class="form-control" name="fileid" value="<?=$datava->bulk_req_id?>" />
                    <input type="hidden" class="form-control" name="email" value="<?=$datava->email?>" />
                    <select class="form-control" name="status-type" required="required">
                      <option value="">--Select Status Type--</option>
                      <option value="success">Success</option>
                      <option value="error">Error</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 col-md-offset-3">
                  <div class="form-group">
                    <?php //echo form_open_multipart('upload/upload_file'); ?>
                     <input type="file" class="form-control" name="userfile" size="20" required="required" />
                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" style="background-color: #00A6B5; width: 150px; height: 40px; border: none; color: white; padding: 5px; font-size: 16px; cursor: pointer;" data-dismiss="modal">Close</button>
              <button type="submit" name="submit" class="" style="background-color: #00A6B5; width: 150px; height: 40px; border: none; color: white; padding: 5px; font-size: 16px; cursor: pointer;"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
              </div>
              </form>
          </div>

        </div>

      </div>
    </div>
</td>
<td><?php echo isset($datava->created_at) ? date('m-d-Y', strtotime($datava->created_at)) : ''; ?></td>
<td>
  <a style="cursor:pointer; margin-left: 10px;" class="hrefid mClass1" href="<?php echo base_url() . 'user/bulkTinDownload/' . $fileid; ?>" title="Download File"><i class="fa fa-download" aria-hidden="true"></i></a>
</td>
                </tr>


				<?php }

}?>
              </tbody>
            </table>

            <?php // echo "<PRE>";print_r($w2data);echo "</PRE>";?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
  <!-- /.content -->
<!--End Modal Crud -->
<script>
$(document).ready(function(){
  function getUrlVars()
   {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
  var id = getUrlVars()["id"];
  var status = getUrlVars()["status"];
          $('#userid').val(id).attr('selected', true);
          $('#status').val(status).attr('selected', true);
});
 $(function(){
      // bind change event to select
     $('#filter').on('click', function () {
          var url = $("#userid").val();
          var status = $("#status").val() // get selected value
     // alert(url);
          if (url && url!='' && status=='') { // require a URL
      url ='<?php echo base_url(); ?>user/bulktinreq/?id='+url;
          window.location.href = url; // redirect
          } else if(url!='' && status!=''){
             url ='<?php echo base_url(); ?>user/bulktinreq/?id='+url+'&status='+status;
          window.location.href = url; // redirect
          } else if(url=='' && status!='') {
            url ='<?php echo base_url(); ?>user/bulktinreq/?status='+status;
          window.location.href = url;
          } else{
        window.location.href ='<?php echo base_url(); ?>user/bulktinreq/';
    }
          //return false;
      });
    });

</script>
<script type="text/javascript">

  jQuery(document).ready(function($) {
	 // $('#example1').DataTable();
       var table = $('#example1').DataTable({
          "sPaginationType": "full_numbers",
          "language": {
            "search": "_INPUT_",
            "searchPlaceholder": "<?php echo lang('search'); ?>",
            "paginate": {
                "next": '<i class="material-icons">keyboard_arrow_right</i>',
                "previous": '<i class="material-icons">keyboard_arrow_left</i>',
                "first": '<i class="material-icons">first_page</i>',
                "last": '<i class="material-icons">last_page</i>'
            }
          },
          "iDisplayLength": 10,
          "aLengthMenu": [[10, 25, 50, 100,500,-1], [10, 25, 50,100,500,"<?php echo lang('all'); ?>"]],
          "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
          ]
        //   ,
        //   dom: 'Bfrtip',
        // buttons: [
        //     'csvHtml5',
        // ]
      });
$("#example1").parent().css({"overflow-x": "visible"});
	  });

</script>