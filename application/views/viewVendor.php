<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h4>W-9 Form Data</h4>
            <ul class="header-dropdown m-r--5">
            <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
            </div>
          <!-- /.box-header -->
           <div class="body">
             <div class="row">
              <?php if (!empty($vendors[0]->pdf)) {?>
              <embed src="<?php echo !empty($vendors[0]->pdf) ? $vendors[0]->pdf : ''; ?>#toolbar=0&navpanes=0&scrollbar=0&zoom=100" type="application/pdf" width="100%" height="800px" />
                <?php } else {
    echo '<p class="lead" align="center">No data found for these vendor!</p>';
}?>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
