<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h4>1099-Div Payee Form Data</h4>
            <ul class="header-dropdown m-r--5">
            <button type="button" class="btn btn-lg btn-primary waves-effect" onclick="window.history.back();"><i class="material-icons">arrow_back</i> Back</button>
            </ul>
            </div>
          <!-- /.box-header -->
          <body>
<div class="page">
        <div class="row">
            <div class="col-xs-12">
                <div class="containerApp">
<form name="RcmForm" method="post" action="addform1099div"
      onsubmit="return valall();"
      onKeydown="FormChanged=true;">    <div>
        <div class="maxwidth1320">
            <div class="clear"></div>
            <div class="clear spacer30 hidden-xs"></div>
            <div class="nameDiv" style="width: 100%;"  id="DivDocumentName">
                            <table class="displayTable">
                                <tbody><tr align="center">
                                    <td><input style="margin-right: 5px;" type="checkbox" class="check" name="void" value="1" <?php echo !empty($v_details) && !empty($v_details[0]->void) ? 'checked' : ''; ?>  onClick="voidclick()" /><label> Void</label></td>
                                    <td><input style="margin-right: 5px;" type="checkbox" class="check" name="corrected" value="1" <?php echo !empty($v_details) && !empty($v_details[0]->corrected) ? 'checked' : ''; ?>  onClick="corclick()" /><label> Corrected</label></td>
                                </tr>
                            </tbody></table>
                        </div>
            <div class="clearfix"></div>

            <div class="clear spacer10"></div>
            <div class="clear spacer10"></div>
            <div class="formFill form1099MISC">
                <div class="row2  borderB1024N borderL borderL980N disFlex1024N floatL ">
                    <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12 formRightHalfTab">
                        <div class="">
                            <div class="row border-bottom">
                                <div class="col-lg-12 col-md-6 col-sm-12 mis_income floatR">
                                    <h3 class="red">Miscellaneous Income</h3>
                                </div>
                            </div>
                            <div class="row formNY show640 taC borderT pad0">
                                <span>OMB No. 1545-0110</span>
                                <img src="https://secure.expressefile.com/Content/Images/2017Y.png">
                                <p>Form <b>1099-DIV</b></p>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6  pad0 formLeftHalf borderR980 borderT borderL980">
                        <div class="signleBox" id="DivAddress1">
                            <label class="formLabel"> PAYER’S name, street address, city or town, state or province, country, ZIP or foreign postal code, and telephone no.</label>
                            <div class="">
                                <div class="pos-rel pLeft10 pRight10">
                                    <?php echo !empty($c_details[0]->c_name) ? $c_details[0]->c_name : ''; ?>
                                </div>
                                <div class="pos-rel pLeft10 pRight10">
                                    <?php echo !empty($c_details[0]->c_filer_name_2) ? $c_details[0]->c_filer_name_2 : ''; ?>
                                </div>
                                <div class="pos-rel pLeft10 pRight10">
                                    <?php echo !empty($c_details[0]->c_address) ? $c_details[0]->c_address : ''; ?>
                                </div>
                                <div class="pos-rel pLeft10 pRight10">
                                    <?php echo !empty($c_details[0]->c_address_2) ? $c_details[0]->c_address_2 : ''; ?>
                                </div>
                                <div class="pos-rel pLeft10 pRight10">
                                    <?php echo !empty($c_details[0]->c_city) ? $c_details[0]->c_city : ''; ?>
                                </div>
                                <div class="row pad0 mar0">
                                    <div class="col-md-6 mBot5  pos-rel pLeft10 pRight10">
                                        <?php echo !empty($c_details[0]->c_state) ? $c_details[0]->c_state : ''; ?>
                                    </div>
                                    <div class="col-md-6  pos-rel pLeft10 pRight10">
                                        <?php echo !empty($c_details[0]->c_zip) ? $c_details[0]->c_zip : ''; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="signleBox1 disFlex1">
                            <div class="col-md-6 col-sm-6 pad0 borderR1" id="DivPayerEINNumber">
                                <div class="pBot5">
                                    <label class="formLabel">
                                        PAYER’S federal identification number
                                    </label>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <?php echo !empty($c_details[0]->c_ssn_ein) ? $c_details[0]->c_ssn_ein : ''; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 pad0" id="DivRecipientEINNumber">
                                <div class="pBot5">
                                    <label class="formLabel">
                                        RECIPIENT’S identification number
                                    </label>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <input  maxlength="11" size="20" id="recepient_id_no" name="recepient_id_no" placeholder="SSN" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->recepient_id_no) ? $v_details[0]->recepient_id_no : ''; ?>" />
                                        <label id="SSNNumberEr" style="display:none" class="error"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="signleBox" id="DivRecipientName">
                            <label class="formLabel"> RECIPIENT’S name</label>
                            <div class="pLeft10 pRight10">
                                <input class="mBot5" maxlength="40" value="<?php echo !empty($v_details) && !empty($v_details[0]->first_name) ? $v_details[0]->first_name : ''; ?>" name="first_name" placeholder="First & Last Name" type="text" />
                                <label id="Recipient_NameEr" style="display:none" class="error"></label>
                            </div>
                            <div class="pLeft10 pRight10">
                                <input class="mBot5" id="Recipient_Name" maxlength="40" value="<?php echo !empty($v_details) && !empty($v_details[0]->last_name) ? $v_details[0]->last_name : ''; ?>" name="last_name" onchange="ChangeDocumentName()" placeholder="dba, partners, etc." type="text" />
                                <label id="Recipient_NameEr" style="display:none" class="error"></label>
                            </div>
                        </div>
                        <div class="signleBox" id="DivStreetAddress">
                            <label class="formLabel"> Street address (including apt. no.)</label>
                            <div class="pLeft10 pRight10">
                                <input class="mBot5" id="RecipientAddress1" maxlength="40" placeholder="Address1" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->address) ? $v_details[0]->address : ''; ?>" name="address" />
                                <label id="RecipientAddress1Er" style="display:none" class="error"></label>
                            </div>
                            <div class="pLeft10 pRight10">
                                <input class="mBot5" id="RecipientAddress1" maxlength="40" placeholder="Address1" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->address2) ? $v_details[0]->address2 : ''; ?>" name="address2" />
                                <label id="RecipientAddress1Er" style="display:none" class="error"></label>
                            </div>
                        </div>
                        <div class="signleBox">
                            <label class="formLabel"> City or town, state or province, country, and ZIP or foreign postal code</label>
                            <div>
                                <div class="pLeft10 pRight10 pos-rel">
                                    <input class="mBot5" id="RecipientCity" maxlength="22" placeholder="City" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->city) ? $v_details[0]->city : ''; ?>" name="city" />
                                    <label id="RecipientCityEr" style="display:none" class="error"></label>
                                </div>
                                    <div class="pLeft10 pRight10 pos-rel">
                                       <input class="mBot5"data-val="true" data-val-number="The field StateId must be a number." data-val-required="The StateId field is required." id="RecipientStateId" placeholder="State" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->state) ? $v_details[0]->state : '' ?>" name="state" data-fillr-id="112043" data-fillr="bound" autocomplete="off"/>
                                        <label id="RecipientStateIdEr" style="display:none" class="error"></label>
                                    </div>
                                    <div class="pLeft10 pRight10 pos-rel">
                                       <input class="mBot5" id="Cuntry" placeholder="country" type="text" value="US" name="country" disabled />
                                        <label id="Country" style="display:none" class="error"></label>
                                    </div>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <input  class="mBot5" id="RecipientZip" minlength="5" placeholder="Zip Code" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->zipcode) ? $v_details[0]->zipcode : ''; ?>" name="zipcode" />
                                        <label id="RecipientZipEr" style="display:none" class="error"></label>
                                    </div>
                            </div>
                        </div>
                        <div class="signleBox1 disFlex1 borderRT borderBN mBot0">
                            <div class="col-md-8 col-sm-12 col-xs-12 pad0 mBot0 bg_red">
                                <label class="formLabel pLeft0 pTop0 "></label>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 pad0" id="DivAccountNum">
                                <div class="pBot5">
                                    <label class="formLabel"> FATCA filing requirement</label>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <input id="AcctNumber" maxlength="20" name="account_number12" placeholder="Account Number" type="checkbox" value="<?php echo !empty($v_details) && !empty($v_details[0]->account_number) ? $v_details[0]->account_number : ''; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="signleBox1">
                            <div class="col-md-12 col-sm-12 col-xs-12 pad0" id="DivAccountNum">
                                <div class="pBot5">
                                    <label class="formLabel"> Account number (see instructions)</label>
                                    <div class="pLeft10 pRight10 pos-rel">
                                        <input id="AcctNumber" maxlength="20" name="account_number" placeholder="Account Number" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->account_number) ? $v_details[0]->account_number : ''; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 borderT1024 borderT floatL borderL980">
                        <div class="row">
                            <div class="col-md-12 col-xs-12 pad0 disFlex1 borderR1024 borderB borderRT">
                                <div class="col-md-6 col-sm-6 borderR640 pad0">
                                    <div class="rightSingleBox borderR1" id="DivRents">
                                        <label class="formLabel"><span class="mRight5">1a </span>Total ordinary dividends</label>
                                        <div class="pLeft10 pRight10">
                                            <input class="B1Rents taR redDollar" data-val="true" data-val-number="The field B1Rents must be a number." data-val-required="The B1Rents field is required." id="FormDetails_B1Rents" maxlength="13" type="text" value="<?php echo isset($v_details[0]->box1a) ? $v_details[0]->box1a : ''; ?>"  name="box1a">
                                        </div>
                                    </div>
                                    <div class="rightSingleBox borderR1 borderBN" id="DivRoyalties">
                                        <label class="formLabel"><span class="mRight5">1b </span>Qualified dividends</label>
                                        <div class="pLeft10 pRight10">
                                            <input class="B2Royalties taR redDollar" data-val="true" data-val-number="The field B2Royalties must be a number." data-val-required="The B2Royalties field is required." id="FormDetails_B2Royalties" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box1b) ? $v_details[0]->box1b : ''; ?>" name="box1b" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 borderR1024N formNY taC padtop0 hide640 ">
                                    <h5 class="red">OMB No. 1545-0110<h5>
                                    <h2 style="font-weight: 900;" class="red"><?php echo !empty($c_details[0]->TaxYear) ? $c_details[0]->TaxYear : ''; ?></h2>
                                    <h5 class="red">1099-DIV</h5>
                                </div>
                            </div>
                            <div class="rightBottomPart">
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1" id="DivOtherIncome">
                                    <label class="formLabel"><span class="mRight5">2a </span> Total capital gain distr</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B3OtherIncome taR redDollar" data-val="true" data-val-number="The field B3OtherIncome must be a number." data-val-required="The B3OtherIncome field is required." id="FormDetails_B3OtherIncome" maxlength="13" value="<?php echo isset($v_details[0]->box2a) ? $v_details[0]->box2a : ''; ?>" name="box2a" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox" id="DivFedIncometaxWithheld">
                                    <label class="formLabel"><span class="mRight5">2b </span> Unrecap. Sec. 1250 gain</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B4FITWithheld taR redDollar" data-val="true" data-val-number="The field B4FITWithheld must be a number." data-val-required="The B4FITWithheld field is required." id="FormDetails_B4FITWithheld" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box2b) ? $v_details[0]->box2b : ''; ?>" name="box2b" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1" id="DivFishingboatproceeds">
                                    <label class="formLabel"><span class="mRight5">2c </span> Section 1202 gain</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B5FishingBoat taR redDollar" data-val="true" data-val-number="The field B5FishingBoat must be a number." data-val-required="The B5FishingBoat field is required." id="FormDetails_B5FishingBoat" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box2c) ? $v_details[0]->box2c : ''; ?>" name="box2c" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox" id="DivMedHealth">
                                    <label class="formLabel"><span class="mRight5">2d </span> Collectibles (28%) gain</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B6MedicalAndHealth taR redDollar" data-val="true" data-val-number="The field B6MedicalAndHealth must be a number." data-val-required="The B6MedicalAndHealth field is required." id="FormDetails_B6MedicalAndHealth" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box2d) ? $v_details[0]->box2d : ''; ?>" name="box2d" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1" id="DivNonEmpCompensation">
                                    <label class="formLabel"><span class="mRight5">3 </span> Nondividend distributions</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B7NonRecipientCompensation taR redDollar" data-val="true" data-val-number="The field B7NonEmployeeCompensation must be a number." data-val-required="The B7NonEmployeeCompensation field is required." id="FormDetails_B7NonEmployeeCompensation" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box3) ? $v_details[0]->box3 : ''; ?>" name="box3" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox" id="DivSubstitutePayments">
                                    <label class="formLabel" style="letter-spacing: -0.5px;"><span class="mRight5">4 </span> Federal income tax withheld</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B8SubstitutePayments taR redDollar" data-val="true" data-val-number="The field B8SubstitutePayments must be a number." data-val-required="The B8SubstitutePayments field is required." id="FormDetails_B8SubstitutePayments" maxlength="13" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->box4) ? $v_details[0]->box4 : ''; ?>" name="box4" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1 bg_red" id="DivPayMade">
                                    <div class="valueL">
                                        <label class="formLabel cursorP" for="FormDetails_B9PayerMadeDirectSales">

                                        </label>
                                    </div>
                                    <div class="pLeft10 pRight10"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox" id="DivCrop">
                                    <label class="formLabel"><span class="mRight5">5 </span>Investment expenses</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B10CropInsuranceProceeds taR redDollar" data-val="true" data-val-number="The field B10CropInsuranceProceeds must be a number." data-val-required="The B10CropInsuranceProceeds field is required." id="FormDetails_B10CropInsuranceProceeds" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box5) ? $v_details[0]->box5 : ''; ?>" name="box5" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1">
                                    <div class="valueL">
                                        <label class="formLabel cursorP" for="FormDetails_B9PayerMadeDirectSales">
                                            <span class="mRight5">6 </span>Foreign tax paid
                                        </label>
                                    </div>
                                    <div class="pLeft10 pRight10">
                                    <input data-val="true" data-val-required="The B9PayerMadeDirectSales field is required." id="FormDetails_B9PayerMadeDirectSales" name="box6" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->box6) ? $v_details[0]->box6 : ''; ?>" data-fillr-id="115852" data-fillr="bound" autocomplete="off">

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox">
                                   <div class="valueL">
                                        <label class="formLabel cursorP" for="FormDetails_B9PayerMadeDirectSales">
                                            <span class="mRight5">7 </span>Foreign country or U.S. possession
                                        </label>
                                    </div>
                                    <div class="pLeft10 pRight10">
                                    <input data-val="true" data-val-required="The B9PayerMadeDirectSales field is required." id="FormDetails_B9PayerMadeDirectSales" name="box7" type="text" value="<?php echo !empty($v_details) && !empty($v_details[0]->box7) ? $v_details[0]->box7 : ''; ?>" data-fillr-id="115852" data-fillr="bound" autocomplete="off">

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1 borderB1024" id="DivExcess">
                                    <label class="formLabel"><span class="mRight5">8 </span>Cash liquidation distributions</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B13ExcessGoldenParachute taR redDollar" data-val="true" data-val-number="The field B13ExcessGoldenParachute must be a number." data-val-required="The B13ExcessGoldenParachute field is required." id="FormDetails_B13ExcessGoldenParachute" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box8) ? $v_details[0]->box8 : ''; ?>" name="box8" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderB1024" id="DivGross">
                                    <label class="formLabel" style="letter-spacing: -0.5px;"><span class="mRight5">9 </span> Noncash liquidation distributions </label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B14GrossProceedsPaid taR redDollar" data-val="true" data-val-number="The field B14GrossProceedsPaid must be a number." data-val-required="The B14GrossProceedsPaid field is required." id="FormDetails_B14GrossProceedsPaid" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box9) ? $v_details[0]->box9 : ''; ?>" name="box9" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1 borderB1024" id="DivGross">
                                    <label class="formLabel" style="letter-spacing: -0.5px;"><span class="mRight5">10 </span> Exempt-interest dividends</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B14GrossProceedsPaid taR redDollar" data-val="true" data-val-number="The field B14GrossProceedsPaid must be a number." data-val-required="The B14GrossProceedsPaid field is required." id="FormDetails_B14GrossProceedsPaid" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box10) ? $v_details[0]->box10 : ''; ?>" name="box10" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderB1024" id="DivGross">
                                    <label class="formLabel" style="letter-spacing: -0.5px;"><span class="mRight5">11 </span> Specified private activity
bond interest dividends</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B14GrossProceedsPaid taR redDollar" data-val="true" data-val-number="The field B14GrossProceedsPaid must be a number." data-val-required="The B14GrossProceedsPaid field is required." id="FormDetails_B14GrossProceedsPaid" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box11) ? $v_details[0]->box11 : ''; ?>" name="box11" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 rightSingleBox borderR1 borderB1024" id="DivExcess">
                                    <label class="formLabel"><span class="mRight5">12 </span>State</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B13ExcessGoldenParachute taR redDollar" data-val="true" data-val-number="The field B13ExcessGoldenParachute must be a number." data-val-required="The B13ExcessGoldenParachute field is required." id="FormDetails_B13ExcessGoldenParachute" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box12) ? $v_details[0]->box12 : ''; ?>" name="box12" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 rightSingleBox borderR1 borderB1024" id="DivExcess">
                                    <label class="formLabel"><span class="mRight5">13 </span>State Identification no.</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B13ExcessGoldenParachute taR redDollar" data-val="true" data-val-number="The field B13ExcessGoldenParachute must be a number." data-val-required="The B13ExcessGoldenParachute field is required." id="FormDetails_B13ExcessGoldenParachute" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box13) ? $v_details[0]->box13 : ''; ?>" name="box13" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 rightSingleBox borderR1 borderB1024" id="DivGross">
                                    <label class="formLabel" style="letter-spacing: -0.5px;"><span class="mRight5">14 </span> State tax withheld</label>
                                    <div class="pLeft10 pRight10">
                                        <input class="B14GrossProceedsPaid taR redDollar" data-val="true" data-val-number="The field B14GrossProceedsPaid must be a number." data-val-required="The B14GrossProceedsPaid field is required." id="FormDetails_B14GrossProceedsPaid" maxlength="13" value="<?php echo !empty($v_details) && !empty($v_details[0]->box14) ? $v_details[0]->box14 : ''; ?>" name="box14" type="text" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-12 formRightHalf borderT borderR">
                        <div class="">
                            <div class="row border-bottom">
                                <div class="col-lg-12 col-md-6 col-sm-6 mis_income" style="margin: 2px -13px -27px !important;">
                                    <h4 class="red" style="font-weight: 900;">Dividends and Distributions</h4>
                                </div>
                            </div>
                            <div class="row borderT" style="text-align: right; padding: 16px;">
                   <p class="red" style="font-weight: 900;">Copy A
                     For
                       Internal Revenue
                       Service Center</p>
                   <p class="red">File with Form 1096.</p>
                   <p class="red lead">For Privacy Act
                    and Paperwork
                      Reduction Act
                      Notice, see the</p>
                   <p class="red lead" style="font-weight: 700;">
                    2018 General
                    Instructions for
                    Certain
                    Information
                    Returns.
                   </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear spacer5"></div>
            <div class="clear spacer10"></div>
            <div class="formFooter">
            <div class="pull-left"><h5 class="red">Form 1099-DIV</h5></div>
            <center>
<select name="stateList" size="1" class="smallpara" onChange="put()">
<option>Select State</option>
<option value="AL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AL') {echo "selected";}?>>AL Alabama</option>
<option value="AK" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AK') {echo "selected";}?>>AK Alaska</option>
<option value="AS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AS') {echo "selected";}?>>AS Amercn Samoa</option>
<option value="AZ" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AZ') {echo "selected";}?>>AZ Arizona</option>
<option value="AR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AR') {echo "selected";}?>>AR Arkansas</option>
<option value="CA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CA') {echo "selected";}?>>CA California</option>
<option value="CO" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CO') {echo "selected";}?>>CO Colorado</option>
<option value="CT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CT') {echo "selected";}?>>CT Connecticut</option>
<option value="DE" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'DE') {echo "selected";}?>>DE Delaware</option>
<option value="DC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'DC') {echo "selected";}?>>DC Dst of Colum</option>
<option value="FM" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'FM') {echo "selected";}?>>FM Micronesia</option>
<option value="FL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'FL') {echo "selected";}?>>FL Florida</option>
<option value="GA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'GA') {echo "selected";}?>>GA Georgia</option>
<option value="GU" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'GU') {echo "selected";}?>>GU Guam</option>
<option value="HI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'HI') {echo "selected";}?>>HI Hawaii</option>
<option value="ID" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ID') {echo "selected";}?>>ID Idaho</option>
<option value="IL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IL') {echo "selected";}?>>IL Illinois</option>
<option value="IN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IN') {echo "selected";}?>>IN Indiana</option>
<option value="IA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IA') {echo "selected";}?>>IA Iowa</option>
<option value="KS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'KS') {echo "selected";}?>>KS Kansas</option>
<option value="KY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'KY') {echo "selected";}?>>KY Kentucky</option>
<option value="LA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'LA') {echo "selected";}?>>LA Louisiana</option>
<option value="ME" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ME') {echo "selected";}?>>ME Maine</option>
<option value="MH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MH') {echo "selected";}?>>MH Marshall Is.</option>
<option value="MD" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MD') {echo "selected";}?>>MD Maryland</option>
<option value="MA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MA') {echo "selected";}?>>MA Massachusett</option>
<option value="MI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MI') {echo "selected";}?>>MI Michigan</option>
<option value="MN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MN') {echo "selected";}?>>MN Minnesota</option>
<option value="MS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MS') {echo "selected";}?>>MS Mississippi</option>
<option value="MO" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MO') {echo "selected";}?>>MO Missouri</option>
<option value="MT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MT') {echo "selected";}?>>MT Montana</option>
<option value="NE" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NE') {echo "selected";}?>>NE Nebraska</option>
<option value="NV" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NV') {echo "selected";}?>>NV Nevada</option>
<option value="NH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NH') {echo "selected";}?>>NH New Hampshir</option>
<option value="NJ" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NJ') {echo "selected";}?>>NJ New Jersey</option>
<option value="NM" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NM') {echo "selected";}?>>NM New Mexico</option>
<option value="NY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NY') {echo "selected";}?>>NY New York</option>
<option value="NC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NV') {echo "selected";}?>>NC N. Carolina</option>
<option value="ND" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ND') {echo "selected";}?>>ND North Dakota</option>
<option value="MP" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MP') {echo "selected";}?>>MP Mariana Is.</option>
<option value="OH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OH') {echo "selected";}?>>OH Ohio</option>
<option value="OK" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OK') {echo "selected";}?>>OK Oklahoma</option>
<option value="OR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OR') {echo "selected";}?>>OR Oregon</option>
<option value="PA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'PA') {echo "selected";}?>>PA Pennsylvania</option>
<option value="PR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'PR') {echo "selected";}?>>PR Puerto Rico</option>
<option value="RI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'RI') {echo "selected";}?>>RI Rhode Island</option>
<option value="SC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'SC') {echo "selected";}?>>SC So. Carolina</option>
<option value="SD" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'SD') {echo "selected";}?>>SD South Dakota</option>
<option value="TN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'TN') {echo "selected";}?>>TN Tennessee</option>
<option value="TX" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'TX') {echo "selected";}?>>TX Texas</option>
<option value="UT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'UT') {echo "selected";}?>>UT Utah</option>
<option value="VT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VT') {echo "selected";}?>>VT Vermont</option>
<option value="VA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VA') {echo "selected";}?>>VA Virginia</option>
<option value="VI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VI') {echo "selected";}?>>VI Virgin Is.</option>
<option value="WA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WA') {echo "selected";}?>>WA Washington</option>
<option value="WV" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WV') {echo "selected";}?>>WV West Virgina</option>
<option value="WI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WI') {echo "selected";}?>>WI Wisconsin</option>
<option value="WY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WY') {echo "selected";}?>>WY Wyoming</option>
</select> Use Select State to find the correct 2 character state code.</center>

            </div>
            <div class="clear spacer10"></div>
        </div>
    </div>
</form>

<script type="text/javascript">
</script>
                </div>
            </div>
        </div>
    </div>
</body>
</div></div></div></div>
</section>