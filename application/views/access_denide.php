<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Instantly Prepare, Generate and e-File 1099-MISC Forms, Corrections and Past Year Forms. No Forms or Software Required - IRS Approved.  " />
    <?php $setting = getSetting();?>
    <title>WageFilingPlus.com | Easily Prepare W-2 / 1099-MISC Forms</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/theme.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/favicon.ico" rel="shortcut icon">
    <script src="<?php echo iaBase(); ?>assets/js/jquery.min.js"></script>
 <style type="text/css">
      body {
        background: #333333;
      }
      .box {
        background: #ffffff;
        color: #333333;
        padding: 10px;
        height: 150px;
        box-shadow: 8px 8px 8px #000000;
      }
    </style>
  </head>
  <body>
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3" align="center" style="margin-top: 120px;">
      <div class="box">
          <img src="<?php echo iaBase() . 'assets/images/wf-header-logo_1532083636.png'; ?>">
        <p class="lead">
          Sorry! You Don't have permission to access this file.
        </p>
      </div>
    </div>
  </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>