<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h2>Import Data <a href="<?php echo iaBase(); ?>assets/template/Table2Files.xlsx" class="btn btn-info btn-sm pull-right">Download Sample File</a></h2>
          </div>
          <!-- /.box-header -->
          <div class="row">
            <div class="col-md-6 col-md-offset-3" style="margin-top: 30px;">
              <?php if (!empty($this->session->flashdata('success'))) {?>
                    <div class="alert alert-success">
                      <?php echo $this->session->flashdata('success'); ?>
                    </div>
              <?php }?>
              <?php if (!empty($this->session->flashdata('danger'))) {?>
                    <div class="alert alert-danger">
                      <?php echo $this->session->flashdata('danger'); ?>
                    </div>
              <?php }?>
             
                    <div class="panel panel-info">
                                    <div class="panel-heading">
                                      <h3 class="panel-title">Select Excel File</h3>
                                    </div>
                                    <div class="panel-body">
                                      <form method="post" class="form" id="import_form" action="importExcel" enctype="multipart/form-data">
                                         <input type="file" name="file" class="form-control" id="file" required accept=".xls, .xlsx" /></p>
                                         <br />
                                         <input type="submit" name="import" value="Import" class="btn btn-info" />
                                      </form>
                                    </div>
                                  </div>
              
                  
            </div>
          </div>
          <!-- /.box-body -->
          
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
  </div>
  <!-- /.row -->
</section>
