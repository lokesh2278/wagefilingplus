<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="header-box">
				<div class="col-md-8">
					<h2>File <span class="h2-light">Corrections</span></h2>
					<h4>Instantly file 1099-MISC corrections even if you did not originally file with us! </h4>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
	</div>
</div>
