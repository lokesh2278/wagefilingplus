<?php
defined('BASEPATH') or exit('No direct script access allowed');

define('DOMPDF_ENABLE_AUTOLOAD', false);
require_once "./vendor/dompdf/dompdf/dompdf_config.inc.php";

class Pdfgenerator
{

    public function generate($html, $filename = '', $stream = true, $paper = 'A4', $orientation = "portrait")
    {
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper($paper, $orientation);
        $dompdf->render();
        return $dompdf->output();
    }
}
