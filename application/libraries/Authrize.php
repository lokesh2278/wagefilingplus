<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
// REMOVE THIS BLOCK - used for DataTables test environment only!
// $file = $_SERVER['DOCUMENT_ROOT'].'/datatables/mysql.php';
// if ( is_file( $file ) ) {
//     include( $file );
// }
require './vendor/autoload.php';

class Authrize
{
    public $merchantAuthentication;
    public $refId;

    public function __construct()
    {
        $this->merchantAuthentication = new net\authorize\api\contract\v1\MerchantAuthenticationType();
        $this->merchantAuthentication->setName(MERCHANT_LOGIN_ID);
        $this->merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
        $this->refId = 'ref' . time();
    }

    public function chargeCraditCard($data)
    {
        $amount = $data['amount'];
        // Create the payment data for a credit card
        $creditCard = new net\authorize\api\contract\v1\CreditCardType();
        $creditCard->setCardNumber($data['card_number']);
        $creditCard->setExpirationDate($data['expire_date']);
        $creditCard->setCardCode($data['card_code']);

        // Add the payment data to a paymentType object
        $paymentOne = new net\authorize\api\contract\v1\PaymentType();
        $paymentOne->setCreditCard($creditCard);

        // Create order information
        /*$order = new net\authorize\api\contract\v1\OrderType();
        $order->setInvoiceNumber("10101");
        $order->setDescription("Golf Shirts");*/

        // Set the customer's Bill To address
        $customerAddress = new net\authorize\api\contract\v1\CustomerAddressType();
        $customerAddress->setFirstName($data['customer_name']);
        $customerAddress->setLastName("");
        $customerAddress->setCompany($data['comp_name']);
        $customerAddress->setAddress("");
        $customerAddress->setCity("");
        $customerAddress->setState("");
        $customerAddress->setZip("");
        $customerAddress->setCountry("");

        // Set the customer's identifying information
        $customerData = new net\authorize\api\contract\v1\CustomerDataType();
        $customerData->setType("individual");
        $customerData->setId($data['customer_id']);
        $customerData->setEmail($data['customer_email']);

        // Add values for transaction settings
        /*$duplicateWindowSetting = new net\authorize\api\contract\v1\SettingType();
        $duplicateWindowSetting->setSettingName("duplicateWindow");
        $duplicateWindowSetting->setSettingValue("60");*/

        // Add some merchant defined fields. These fields won't be stored with the transaction,
        // but will be echoed back in the response.
        /*$merchantDefinedField1 = new net\authorize\api\contract\v1\UserFieldType();
        $merchantDefinedField1->setName("customerLoyaltyNum");
        $merchantDefinedField1->setValue("1128836273");

        $merchantDefinedField2 = new net\authorize\api\contract\v1\UserFieldType();
        $merchantDefinedField2->setName("favoriteColor");
        $merchantDefinedField2->setValue("blue");*/

        // Create a TransactionRequestType object and add the previous objects to it
        $transactionRequestType = new net\authorize\api\contract\v1\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        //$transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);
        $transactionRequestType->setBillTo($customerAddress);
        $transactionRequestType->setCustomer($customerData);
        //$transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
        //$transactionRequestType->addToUserFields($merchantDefinedField1);
        //$transactionRequestType->addToUserFields($merchantDefinedField2);

        // Assemble the complete transaction request
        $request = new net\authorize\api\contract\v1\CreateTransactionRequest();
        $request->setMerchantAuthentication($this->merchantAuthentication);
        $request->setRefId($this->refId);
        $request->setTransactionRequest($transactionRequestType);

        // Create the controller and get the response
        $controller = new net\authorize\api\controller\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        $result = [];
        $result['success'] = false;
        if ($response != null) {
            // Check to see if the API request was successfully received and acted upon
            if ($response->getMessages()->getResultCode() == RESPONSE_OK) {
                // Since the API request was successful, look for a transaction response
                // and parse it to display the results of authorizing the card
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {
                    $result['success'] = true;
                    $result['transaction_id'] = $tresponse->getTransId();
                    $result['auth_code'] = $tresponse->getAuthCode();

                } else {

                    if ($tresponse->getErrors() != null) {
                        $result['error_code'] = $tresponse->getErrors()[0]->getErrorCode();
                        $result['error_message'] = $tresponse->getErrors()[0]->getErrorText();
                    }
                }
                // Or, print errors if the API request wasn't successful
            } else {

                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $result['error_code'] = $tresponse->getErrors()[0]->getErrorCode();
                    $result['error_message'] = $tresponse->getErrors()[0]->getErrorText();

                } else {
                    $result['error_code'] = $response->getMessages()->getMessage()[0]->getCode();
                    $result['error_message'] = $response->getMessages()->getMessage()[0]->getText();
                }
            }
        } else {
            $result['error_message'] = "No response returned";
        }

        return json_encode($result);
    }
}
