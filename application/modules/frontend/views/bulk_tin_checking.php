<div id="app-container">

  <div id="page" class="page">

    <div id="blaze-banner">

      <div class="container">

        <div class="navigation-container">

          <div class="wrap wrap-one-logo">



            <a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>

            </div>

			</div>

      </div>

    </div>



<style>

.smallpara {

    vertical-align: text-top;

    width: 100%;

}

.btn:last-child {

    margin:0px 20px 30px 0px !important;

}
.dark {
  font-weight: 700 !important;
}
.margin-top {
      margin-top: 30px !important;
}
</style>

 <div class="container">

  <div class="row">



    <div class="col-md-12">

     <div class="row">

     <center>

        <h4>Bulk Tin Checking</h4><h4>Logged in as:

<?php echo isset($user_d[0]->Ucontact) ? $user_d[0]->Ucontact : ''; ?>, <?php echo isset($user_d[0]->Ucompany) ? $user_d[0]->Ucompany : ''; ?>, <?php echo isset($user_d[0]->Uphone) ? $user_d[0]->Uphone : ''; ?>, <?php echo isset($user_d[0]->UEmail) ? $user_d[0]->UEmail : ''; ?></h4>

        </center>
<?php if ($this->session->flashdata('error')) {?>
<div class="alert alert-warning">
  <?=$this->session->flashdata('error')?>
</div>
<?php }?>
<?php if ($this->session->flashdata('message')) {?>
<div class='alert alert-info'> <?=$this->session->flashdata('message')?> </div>
<?php }?>
      </div>



      <div class="table-container" style="width:100%">

        <div class="table-responsive">

                <table class="w9-table table table-striped table-hover table-responsive  table-bordered" name='example-table'>

                <thead style="background-color: #337ab7; color: #ffffff;">

                <tr class="toptable-text">
                    <td width="35">NAME</td>
                    <td width="120">EMAIL</td>
                    <td width="35">PHONE</td>
                    <td width="85">REQUESTED FILE</td>
                    <td width="35">STATUS</td>
                    <td width="35">DATE</td>
                  </tr>

                </thead>

                <tbody class="forms-container">
                   <?php

if (isset($v_details) && !empty($v_details)) {

    //echo "<PRE>";print_r($v_details);echo "</PRE>";

    foreach ($v_details as $v_detail) {

        // print_r($v_detail->status);

        $id = isset($v_detail->bulk_req_id) ? $v_detail->bulk_req_id : '';
        $status = isset($v_detail->status) ? $v_detail->status : '';
        $name = isset($v_detail->name) ? $v_detail->name : '';
        $email = isset($v_detail->email) ? $v_detail->email : '';
        $phone = isset($v_detail->phone) ? $v_detail->phone : '';
        $created_at = isset($v_detail->created_at) ? $v_detail->created_at : '';
        $bulk_tin_file = isset($v_detail->bulk_tin_file) ? $v_detail->bulk_tin_file : '';
        $status_file = isset($v_detail->status_file) ? $v_detail->status_file : '';
        $CustID = isset($v_detail->CustID) ? $v_detail->CustID : '';
        $fileid = encrypt_decrypt('encrypt', $id);

        ?>

                        <tr>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $email; ?></td>
                        <td><?php echo $phone; ?></td>
                        <td><?php echo $bulk_tin_file; ?>
                          <a style="cursor:pointer; margin-left: 10px;" class="hrefid mClass1" href="<?php echo base_url() . 'user/bulkTinDownload/' . $fileid; ?>" title="Download File"><i class="fa fa-download" aria-hidden="true"></i></a>
                        </td>
                        <td><?php echo $status; ?>
                          <?php if ($status == 'success' || $status == 'error') {?>
                          <a style="cursor:pointer; margin-left: 10px;" class="hrefid mClass1" href="<?php echo base_url() . 'user/downloadTinresponse/' . $fileid; ?>" title="Download File"><i class="fa fa-download" aria-hidden="true"></i></a>
                          <?php }?>
                        </td>
                        <td><?php echo $created_at; ?></td>
                        </tr>

                        <?php

    }

} else {?>
                <tr>
                  <td colspan="6" align="center"><h5>No data available in table</h5></td>
                </tr>
<?php }?>
                </tbody>
                </table>
<div class="col-md-12">
  <div class="row" align="center">
    <p class="lead">To perform Bulk Tin Request, Upload your spreadsheet with your data. If you need the proper template you can download it <a target="_blank" href="<?php echo iaBase(); ?>assets/template/TIN Matching Import Template 2018.xlsx">here</a> and <a target="_blank" href="<?php echo iaBase(); ?>assets/template/General Rules on TIN Maching Template.xlsx">rule/guide</a>.<br>
    Billing is done in 10,000 name increments, so if you have 100-10,000 the price is $74.00<br>
    Your files will be processed and returned within 72 hours with a full report from the IRS.</p>
  </div>
</div>
                <div class="col-md-8 col-md-offset-2">
                  <table width="100%" border="0" align="center">

<tbody>
<tr>
<td><button type="button" name="mainMENU" style="background-color: #00A6B5; width: 225px; height: 45px; border: none; color: white; padding: 12px 16px; font-size: 16px; cursor: pointer;" class="delete-active">Submit TIN Request</button></td>

<td align="left" class="smallpara" style="padding-left: 30px; vertical-align: inherit; font-size: 14px;"><img src="<?php echo iaBase(); ?>assets/images/green-arrow.jpg" width="35px" height="36px"><span> Verify contractor Tax Identification number with IRS database</span></td>

</tr>
</tbody>
</table>

<!-- Modal -->
  <div class="modal fade" id="checkout-modal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-header" style="padding: 15px; background-color: #25a9bd !important; color: #ffffff !important; border-bottom: 1px solid #e5e5e5; border-top-left-radius: 5px !important; border-top-right-radius: 5px !important;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload File For Bulk Tin Check</h4>
        </div>
        <div class="modal-body">

          <?php echo form_open_multipart('frontend/do_upload'); ?>
          <form action="" method="post" name="form1">
            <div class="row">
              <div class="col-md-6 col-md-offset-3">

                <div class="form-group">
                  <?php echo form_open_multipart('upload/upload_file'); ?>
                   <input type="file" class="form-control" name="userfile" size="20" required="required" />
                </div>

              </div>
            </div>
            <div class="modal-footer">
              <button type="button" style="background-color: #00A6B5; width: 150px; height: 40px; border: none; color: white; padding: 5px; font-size: 16px; cursor: pointer;" data-dismiss="modal">Close</button>
            <button type="submit" name="submit" class="" style="background-color: #00A6B5; width: 150px; height: 40px; border: none; color: white; padding: 5px; font-size: 16px; cursor: pointer;"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
            </div>
            </form>
        </div>

      </div>

    </div>
  </div>
                </div>
            </div>
          </div>
        </div>


      </div>

    </div>
<div class="container">
  <div class="row margin-top">
    <div class="col-md-12">
      <p class="lead dark">Q1. How do we perform a Tin/Name Match?</p>
      <p class="lead">Ans. Prepare an excel spreadsheet With two columns: Name and Tax ID for tin match. If we are also mailing your W-9 solicitation, in include the address(as show below)</p>
      <p class="lead">Name your spreadsheet must have UNIQUE Filename: something like John-Corp-Mary-Smith-2011-Tin-Match.xls, Do not send spreadsheet named TinMatch.xls or other common generic name.</p>
      <p class="lead">Send your spreadsheet to us using  the SEND LINK at your secure Service Bureau at www.1099express.com/service_Bureau.asp After sending the spreadsheet, click there link to NOTIFY US. Please call us at 36-884-1500, IF you have questions. It is your responsibility to be sure we are notified about and that you receive the results.</p>
      <hr>

      <p class="lead dark">Q2. Will the IRS Tell us the correct name if we have a SSN or  EIN ?</p>
      <p class="lead">Ans: No the IRS only say Match or No-Match</p>
      <hr>

      <p class="lead dark">Q3. Will the IRS Tell us the correct Tin if we have a name ?</p>
      <p class="lead">Ans: No, Same as above. Only if it's a match or not.</p>
      <hr>

      <p class="lead dark">Q3. Will the IRS Tell us if it's a  SSN or EIN ?</p>
      <p class="lead">Ans: Yes, If you score a match, they will tell you it comes from  either IRS database to the SSN database, See <a target="_blank" href="<?php echo iaBase(); ?>assets/pdf/Bulk-Tin-Results-Sample-Wagefiling.pdf">Sample Tin Match Report</a></p>
      <hr>
      <p class="lead dark">Q4. What do you need for TIN/Name Matching only ?</p>
      <p class="lead">Ans: If we are performing just a TIN/Name matching, then all we need  is the payees names and tax ID numbers, Nothing else is required. It can look like this:</p>
      <table class="table table-bordered">
        <tbody>
          <tr><td>John Doe</td><td>72-3556789</td></tr>
          <tr><td>John K Doe</td><td>72-3556789</td></tr>
          <tr><td>Doe John</td><td>723-55-6789</td></tr>
          <tr><td>Dr. John Doe</td><td>72-3556789</td></tr>
          <tr><td>Ajax Co. LLC</td><td>73-3556789</td></tr>
        </tbody>
      </table>
      <hr>

      <p class="lead dark">Q5. What format should names and  TINs be in ?</p>
      <p class="lead">Ans: The data can be in an Excel spreadsheet, with the 2 required columns: Name, and Tin. Column cab be in ant order, If dashes are present in your data, just leave them, It is not necessary to remove dashes, Dashes are ignored, If there are no dashes, that's OK, do not add them, First and Last name can be in any order.</p>
      <hr>
      <p class="lead dark">Q6. That's all? What about the vendor number ?</p>
      <p class="lead">Ans: Yes, you can give us the vendor number, account number,etc. If this helps you, again columns in any order.</p>
      <table class="table table-bordered">
        <tbody>
          <tr><td>John Doe</td><td>72-3556789</td><td>783333</td></tr>
          <tr><td>John K Doe</td><td>72-3556789</td><td>34722A</td></tr>
          <tr><td>Doe John</td><td>723-55-6789</td><td>3788</td></tr>
          <tr><td>Dr. John Doe</td><td>72-3556789</td><td>A227Z34</td></tr>
          <tr><td>Ajax Co. LLC</td><td>73-3556789</td><td>K224</td></tr>
        </tbody>
      </table>
</div>
  </div>
</div>
  </div>

</div>
<script type="text/javascript">
$(document).ready(function() {
      $('.delete-active').click(function(){
          $("#checkout-modal").modal();
     });
    });
</script>

