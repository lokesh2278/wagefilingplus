<?php
$total = array();
if (isset($v_details) && !empty($v_details)) {

    if (isset($c_details[0]->FormType) && $c_details[0]->FormType == '1099-Misc') {

        foreach ($v_details as $v_detail) {

            if ($v_detail->void != 1) {
                $total[] = 1;
            }

        }

    } else {

        foreach ($v_details as $v_detail) {

            if ($v_detail->void != 1) {
                $total[] = 1;
            }

        }

    }
}

if ($c_details[0]->FormType == '1099-Misc') {
    $count = misc_count($c_details[0]->FileSequence);
    $count += div_count($c_details[0]->FileSequence);

} else if ($c_details[0]->FormType == '1099-Div') {
    $count = misc_count($c_details[0]->FileSequence);
    $count += div_count($c_details[0]->FileSequence);
} else {
    $count = w2_count($c_details[0]->FileSequence);
}

?>
<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
            <div class="row">
              <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
                <h1><a class="company-link"><span class="company-name"><?=companyName($c_details[0]->FileSequence)?></span></a></h1>
              </div>
            </div>
            <a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
            </div>
      </div>
      </div>
    </div>


 <div class="container">
  <div class="row">

    <div class="col-md-12">
          <div class="table-container" style="width:100%">
            <div class="table-responsive">
                <?php if ($c_details[0]->eFiled == 1) {?>
                <p>Dear WageFilingPlus.com customer,</p>
                <p>Your file has been successfully e-Filed to the IRS/SSA. E-Filing includes the electronic version of Copy A of all forms, and the transmittal forms W-3 and/or 1096. This is your tracking report and confirmation from the IRS/SSA. Please keep this as proof of filing.</p>
                <p>You can also find a copy of this report in your account at all times. Simply login, select the File Manager button and then click the View Totals button with your selected company file.</p>
                <p>It would be appreciated if you would reply to this e-Mail, so we know you received it.</p>
                <br>
                <hr>
                <center><h4>Tracking Report</h4></center>
                <p>
                    <span>Filer Name:</span> <?=$c_details[0]->FileName?><br>

                    <span>Email:</span> <?=$c_details[0]->c_email?><br>
                    <span>Tax Year:</span> <?=$c_details[0]->TaxYear?><br>
                    <span>Filer TIN:</span> <?=$c_details[0]->c_ssn_ein?><br>
                    <span>Form Type:</span> <?=$c_details[0]->FormType?><br>

                    <span>E-Filing Date:</span> <?=$v_details[0]->efile_date?><br>
                    <span>Tracking Number:</span> <?=$v_details[0]->irs_file_number?><br>
                    <span>Mailing Date:</span> <?=$v_details[0]->mailed_date?><br>
                    <span>IRS Acceptance Date:</span> <?=$v_details[0]->irs_accepted_date?><br>
                    <span>Filer Address:</span> <?=$c_details[0]->c_address?>, <?=$c_details[0]->c_city?>, <?=$c_details[0]->c_state?> <?=$c_details[0]->c_zip?> <br>
                    <span>Number of Payees:</span> <?php echo $count; ?><br>
                    <br>

                    Thank you for using www.WageFilingPlus.com<br>
                    WageFilingPlus.com, LLC<br>
                    E-Mail: Support@WageFilingPlus.com<br>
                </p>
                <p><img src="<?php echo iaBase(); ?>assets/images/ef.gif">Powered by 1099Express.com<br>
                An Authorised IRS e-file and software provider.</p>
              <?php } else {?>
                <div class="alert alert-warning"><p class="lead">This file is not paid nor e-Filed, thus no confirmation available. File = <?=$c_details[0]->FileSequence?></p></div>
                <?php }?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
