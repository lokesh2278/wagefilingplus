<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: linear-gradient(to top, #F6F6F6 , #E3E3E3); background-size: 100%; height: 125px;">
				<div class="col-md-12" style="margin-top: 30px;">
					<h3>Top 4 Tax Mistakes <span class="light"> that Will Cost You Big for Your Small Business</span></h3>
				</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">

			<h4>Top 4 Little Tax Mistakes that Will Cost You Big for Your Small Business</h4>
			<p class="lead">There isn’t a small business owner on the planet that doesn’t dread tax season. They figure that waiting until the last minute is the best idea, because, number one, they don’t want to think about it, and number two, nothing is organized and ready to file! That is your first two mistakes; you should always be organized and never wait until the last minute to file the taxes for your small business, whether online or at a brick and mortar tax agency. There are many, many mistakes that can be made when filing your taxes, read on below for the top little tax mistakes that will cost your small business big if you aren’t careful.</p>

			<h4>Trying to do it Yourself</h4>
			<p class="lead">There are very few small business owners out there who have the tax knowledge needed to do their taxes themselves. You have to remember that tax laws are changing on a yearly basis, and unless you are trained to do taxes, you can easily get confused and mess something up. If you don’t know a ton about taxes, you risk getting yourself in tons of hot water with the IRS, Internal Revenue Service, by trying to do them yourself. Be on the safe side, and have a company you trust help you do your taxes online at least.</p>

			<h4>Making Everyone an Independent Contractor</h4>
			<p class="lead">Everyone knows that to a small business; independent contractors are cheaper than hiring employees. The downfall of this is that you have to treat an independent contractor as an independent contractor. In other words, they are not on your payroll, so they decide when they work, have control over where they work, and how the work assigned is completed. If at some point, an independent contractor says you didn’t allow these things, you could have to pay the government for not collecting social security taxes and pay workman’s comp up to a certain amount as well.</p>

			<h4>You Can’t Afford to Hire Your Own Children</h4>
			<p class="lead">Of course, you can afford to hire your own kids! Make sure you pay them a reasonable wage and then you can claim it as a business expense. If you, then, in turn, have them use that money to pay for college, you can use it as a college expense on your taxes as well and earn the college tuition deductible.</p>

			<h4>Thinking Your Bookkeeper Would Never Steal</h4>
			<p class="lead">This is one of the most common tax mistakes small businesses make. It is important to have one person who writes the checks and another person who handles bookkeeping and accounts. There are many, many business owners who have lost everything because of an embezzler. Make sure to keep the two separate and you should have nothing to worry about.</p>

			<p class="lead">These are just a few of the biggest mistakes that many small business owners make when it comes to filing taxes. Your best bet is to contact Wage Filing for more information and to file your taxes the right way this upcoming tax season.</p>
			<br />
			<hr>
			<h4>Contact Us</h4>
			<h5>WageFilingPlus. LLC.</h5>
			<h5>Telephone: 616-325-9332</h5>
			<h5>E-mail: <a href="mailto:support@Wagefilingplus.com">support@WageFilingPlus.com</a></h5>
			<h5>Trusted by</h4>
			<img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
