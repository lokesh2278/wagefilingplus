<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: url('../assets/images/statefilingheader.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 50px;">
					<h4>State Filing <span class="light">Requirements</span></h4>
				</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<p class="lead">Each state has individual filing deadlines for reporting  W-2 and 1099-MISC forms. Below are a current deadlines in 2014 for 2013 reporting.
Take a look below to determine the requirements for the states where you have employees or contractors.</p>
<img src="<?php echo iaBase(); ?>assets/images/statefilingchar.jpg">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<hr>
			<h4>Contact Us</h4>
			<h5>WageFilingPlus. LLC.</h5>
			<h5>Telephone: 616-325-9332</h5>
			<h5>E-mail: <a href="mailto:support@wagefilingplus.com">support@Wagefilingplus.com</a></h5>
			<h5>Trusted by</h4>
			<img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
