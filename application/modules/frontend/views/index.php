
<!-- <!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
    </head>
    <body>

		<div class="nav-container">




		    <nav>
		        <div class="nav-bar">
		            <div class="module left">
		                <a href="index.html">
		                    <img class="logo logo-light" alt="Foundry" src="img/logo-light.png">
		                    <img class="logo logo-dark" alt="Foundry" src="img/logo-dark.png">
		                </a>
		            </div>
		            <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
		                <i class="ti-menu"></i>
		            </div>
		            <div class="module-group right">
		                <div class="module left">
		                    <ul class="menu">
		                        <li>
		                            <a href="#">Single</a>
		                        </li>
		                        <li class="has-dropdown">
		                            <a href="#">
		                                Mega Menu
		                            </a>
		                            <ul class="mega-menu">
		                                <li>
		                                    <ul>
		                                        <li>
		                                            <span class="title">Column 1</span>
		                                        </li>
		                                        <li>
		                                            <a href="#">Single</a>
		                                        </li>
		                                    </ul>
		                                </li>
		                                <li>
		                                    <ul>
		                                        <li>
		                                            <span class="title">Column 2</span>
		                                        </li>
		                                        <li>
		                                            <a href="#">Single</a>
		                                        </li>
		                                    </ul>
		                                </li>
		                            </ul>
		                        </li>
		                        <li class="has-dropdown">
		                            <a href="#">
		                                Single Dropdown
		                            </a>
		                            <ul>
		                                <li class="has-dropdown">
		                                    <a href="#">
		                                        Second Level
		                                    </a>
		                                    <ul>
		                                        <li>
		                                            <a href="#">
		                                                Single
		                                            </a>
		                                        </li>
		                                    </ul>
		                                </li>
		                            </ul>
		                        </li>
		                    </ul>
		                </div>

		                <div class="module widget-handle language left">
		                    <ul class="menu">
		                        <li class="has-dropdown">
		                            <a href="#">ENG</a>
		                            <ul>
		                                <li>
		                                    <a href="#">French</a>
		                                </li>
		                                <li>
		                                    <a href="#">Deutsch</a>
		                                </li>
		                            </ul>
		                        </li>
		                    </ul>
		                </div>
		            </div>

		        </div>
		    </nav>


















		</div>
 -->
 <style type="text/css">

 </style>
		<div class="main-container">
			<section class="cover fullscreen header-block-container">

		                <div class="background-image-holder">
		                    <img alt="image" class="background-image" src="<?php echo iaBase(); ?>assets/images/wf-homepage-hero_v2.jpg">
		                </div>
		                <div class="container">
		                    <div class="row">
		                        <div class="col-md-8 col-sm-8 header-block">
		                            <h3 class="mb40 mb-xs-16 large header-text">"This easy-to-use online service lets you print, produce and file your own W-2 or 1099-MISC forms for only $3.49 per file”</h3>
		                            <div class="caption ">
									<div class="fusion-title-sc-wrapper" style="">
										<div class="fusion-title title fusion-sep-none fusion-title-size-three fusion-border-below-title" style="margin-top:0px;margin-bottom:0px;"><h3 class="title-heading-left" style="color: rgb(255, 255, 255); font-size: 24px; line-height: 28px;" data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="24" data-lineheight="28"><div style="font-weight: 600; line-height: 34px; letter-spacing: 1px;">— Jonathan Blum, <a style="color: #00A8BA !important;" href="https://www.entrepreneur.com/article/219330" target="_blank">Entrepreneur Magazine</a></div>

<p></p><p></p><div class="caption"><h4>WageFiling is the only &nbsp;filing service with a Quality Supplier rating from the IRS since 1996</h4></div></h3></div>									</div>
								</div>
		                            <a class="btn btn-lg wfp-btn" href="<?php echo base_url('signup'); ?>">Start Filing</a>
		                            <a class="btn btn-lg wfp-btn" href="<?php echo base_url('how_it_works'); ?>" target="_blank">Learn More</a>
		                        </div>
		                    </div>

		                </div>

		    </section>
		    <section class="grey-bg">
		    <div class="container">
		        <div class="row">
		            <div class="logo-carousel">
		                <ul class="slides">
		                    <li>
		                            <img alt="Logo" src="<?php echo iaBase(); ?>assets/images/irs-logo.png">
		                    </li>
		                    <li>
		                            <img alt="Logo" src="<?php echo iaBase(); ?>assets/images/entrepreneur-logo.png">
		                    </li>
		                    <li>
		                            <img alt="Logo" src="<?php echo iaBase(); ?>assets/images/ssa-logo.png">
		                    </li>
		                    <li>
		                            <img alt="Logo" src="<?php echo iaBase(); ?>assets/images/morningstar-logo.png">
		                    </li>
		                    <li>
		                            <img alt="Logo" src="<?php echo iaBase(); ?>assets/images/cpa-logo.png">
		                    </li>
		                </ul>
		            </div>

		        </div>

		    </div>

		</section>
		<div class="container print-mail-block pt65">
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center">WageFiling Plus offers all  necessary services to file your 1099-Misc and W-2's quickly and correctly from start to finish.</h3>
				</div>
			</div>
		    <!-- <div class="row">
		        <div class="col-sm-4 col-md-offset-4">
		            <img src="<?php echo iaBase(); ?>assets/images/print_mail_icon.png">
		                <h4 class="mb8">Print & Mail</h4>
		                <p class="lead mb40">Lookin gfor additional forms1098, 1095-C, 1099-Div and mailing services</p>
		                <a class="btn btn-lg btn-block btn-black uppercase mb0" href="#">Get Started</a>
		        </div>
		    </div> -->
		    <div class="row pt65">
		                <div class="col-sm-3 text-center">
		                    <div class="feature">
		                        <img width="135px" height="95px" class="icon inline-block mb16" src="<?php echo iaBase(); ?>assets/images/print_irs.png"/>
		                        <h4>Electronic <br/>W-9 Request</h4>
		                        <p class="lead">
		                            Issue your contractor a W-9 via email that they can e-sign.  Once signed, it will save into your account instantly as a W-9 and 1099-MISC
		                        </p>
		                    </div>
		                </div>
		                <div class="col-sm-3 text-center">
		                    <div class="feature">
		                        <img width="95px" height="95px" class="icon inline-block mb16" src="<?php echo iaBase(); ?>assets/images/Checklist.png"/>
		                        <h4>Tax Identification Number Verification</h4>
		                        <p class="lead">
		                            We can cross-check your W-9 info with the IRS database. This helps ensure the information you submit to the IRS is correct and help you avoid errors and fines during reporting.
		                        </p>
		                    </div>
		                </div>
		                <div class="col-sm-3 text-center">
		                    <div class="feature">
		                        <img width="95px" height="95px" class="icon inline-block mb16" src="<?php echo iaBase(); ?>assets/images/desktop_excel.png"/>
		                        <h4>Enter or Upload your 1099/W-2 info</h4>
		                        <p class="lead">
		                            Simply upload an excel file or manually enter your 1099/W-2 data into the system.  Once loaded you can view all forms and totals before submission.
		                        </p>
		                    </div>
		                </div>
		                <div class="col-sm-3 text-center">
		                    <div class="feature">
		                        <img width="95px" height="95px" class="icon inline-block mb16" src="<?php echo iaBase(); ?>assets/images/print_and_mail.png"/>
		                        <h4>Print, Mail<br/> & e-Filing</h4>
		                        <p class="lead">
		                           We offer the option to handle all the print and mailing for you.  You can also instantly print recipient copies on plain paper or save as pdf.  e-Filing to the SSA/IRS is done automatically by us.
		                        </p>
		                    </div>
		                </div>
		            </div>
		</div>
		<section class="pt-xs-80 pb-xs-80 testomonial-block">
		        <div class="container">
		        	<div class="row">
		        		<div class="col-md-12">
		            		<h6 style="font-size: 30px; line-height: 48px; font-weight: 300; text-align: center;">SECURITY FEATURES</h6>
		            		<hr class="feature-block-hr">
		            	</div>
		        	</div>
		            <div class="row">
		                <div class="col-md-10 col-md-offset-1">
		                        <div class="feature feature-3">
		                        	<div class="left">
		                        	    <i class="fa fa-circle-o" aria-hidden="true"></i>
		                        	</div>
		                            <div class="right">
		                              <p>
		                                WageFiling Plus has been approved by the IRS to e-file 1099's  and by the Social Security Administration using government secure protocols since 1996
		                              </p>
		                            </div>
		                        </div>
		                        <div class="feature feature-3">
		                        	<div class="left">
		                        	    <i class="fa fa-circle-o" aria-hidden="true"></i>
		                        	</div>
		                            <div class="right">
		                              <p>
		                                We use the highest industry standard, 256-bit Secure Socket Layer (SSL) encryption for the entire web site
		                              </p>
		                            </div>
		                        </div>
		                        <div class="feature feature-3">
		                        	<div class="left">
		                        	    <i class="fa fa-circle-o" aria-hidden="true"></i>
		                        	</div>
		                            <div class="right">
		                              <p>
		                                All recipient copies are mailed First Class from our SOC certified data centre
		                              </p>
		                            </div>
		                        </div>
		                        <div class="feature feature-3">
		                        	<div class="left">
		                        	    <i class="fa fa-circle-o" aria-hidden="true"></i>
		                        	</div>
		                            <div class="right">
		                              <p>
		                                Our credit card processing uses a third party provider, <a href="https://www.stripe.com/" target="_blank">Stripe</a>, who is PCI-compliant. We do not store your full credit card number on our servers.
		                              </p>
		                            </div>
		                        </div>
		                        <div class="feature feature-3">
		                        	<div class="left">
		                        	    <i class="fa fa-circle-o" aria-hidden="true"></i>
		                        	</div>
		                            <div class="right">
		                              <p>
		                                Our servers are hosted on Digital Ocean,<a href="https://www.digitalocean.com/legal/compliance/" target="_blank">SOC 1 Type II, SOC 2 Type II, ISO/IEC 27001:2013</a>.
		                              </p>
		                            </div>
		                        </div>
		                </div>
		            </div>
		        </div>
		    </section>
		    <section class="how-east-block">
		        <div class="container">
		            <div class="row mb64 mb-xs-24">
		                <div class="col-sm-12 col-md-10 col-md-offset-1 blue-heading">
		                    <h1>How easy is it?</h1>
		                </div>
		            </div>

		            <div class="row print-mail-block">
		                <div class="col-sm-4 text-center">
		                    <div class="feature">
		                        <div class="text-center">
		                            <i style="border-color:rgba(255,255,255,0);border-width:0px;background-color:#24a9bd;box-sizing:content-box;height:48px;width:48px;line-height:48px;border-radius:50%;color:#ffffff;font-size:24px;margin-bottom:15px;" class="fontawesome-icon  fa fa-pencil-square-o circle-yes"></i>

		                            <h4>Enter your W-2/1099 Data</h4>
		                        </div>
		                        <p class="lead">
		                            Our easy to use online software makes entering W-2 / 1099 data simple and secure.
		                        </p>
		                    </div>

		                </div>
		                <div class="col-sm-4 text-center">
		                    <div class="feature">
		                        <div class="text-center">
		                            <i style="border-color:rgba(255,255,255,0);border-width:0px;background-color:#24a9bd;box-sizing:content-box;height:48px;width:48px;line-height:48px;border-radius:50%;color:#ffffff;font-size:24px;margin-bottom:15px;" class="fontawesome-icon  fa fa-envelope-o circle-yes"></i>

		                            <h4>Email or mail recipient copies</h4>
		                        </div>
		                        <p class="lead">
		                            You’ll be provided a PDF of your recipient copies for you to either mail or e-mail.
		                        </p>
		                    </div>
		                </div>
		                <div class="col-sm-4 text-center">
		                    <div class="feature">
		                        <div class="text-center">
		                            <i style="border-color:rgba(255,255,255,0);border-width:0px;background-color:#24a9bd;box-sizing:content-box;height:48px;width:48px;line-height:48px;border-radius:50%;color:#ffffff;font-size:24px;margin-bottom:15px;" class="fontawesome-icon  fa fa-pencil-square-o circle-yes"></i>

		                            <h4>We e-file to the IRS</h4>
		                        </div>
		                        <p class="lead">
		                           The moment you checkout we submit your transmittal's and Federal copies directly to the IRS/SSA  and your e-filing is complete
		                        </p>
		                    </div>

		                </div>
		            </div>

		        </div>

		    </section>
<div class="container text-center">
	<h1 class="feature-block-title">Recent posts</h1>
	<hr class="feature-block-hr">
</div>
		<div class="container pt120 pt65 image-bg feature-block">
		        <div class="background-image-holder">
		            <img alt="Background" class="background-image" src="<?php echo iaBase(); ?>assets/images/blog-img-2-1100x733.jpg">
		        </div>
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-10 mb-xs-80 push-left">
		                	<h5>FEATURED ARTICLE</h5>
		                    <h2 class="mb16">Top 4 Tax Mistakes that Will Cost You Big for Your Small Business</h2>
		                    <p class="lead mb32">Top 4 Little Tax Mistakes that Will Cost You Big for Your Small Business There isn’t a small business owner on…

</p>
		                    <a class="btn btn-lg wfp-btn-black" href="<?=base_url('top_4_tax_mistakes')?>" target="_blank">Read More</a>
		                </div>
		            </div>

		        </div>

		    </div>
		    <section class="post-block">
		        <div class="container">
		            <div class="row">
		                <div class="col-md-4 col-sm-6">
		                    <div class="image-tile outer-title">
		                        <img alt="Pic" src="<?php echo iaBase(); ?>assets/images/post_img1.jpg">
		                        <div class="title mb16">
		                            <h5 style="font-size: 16px !important;">TIN Verification Services – Did your contractor provide you with the correct info on their W-9?</h5>
		                            <span>August 27th, 2018</span>
		                        </div>
		                        <p class="lead mb0">
		                            For most of us that that hire contract labor, we typically send our contractors a W-9 and use that information to
		                        </p>
		                    </div>
		                </div>
		                <div class="col-md-4 col-sm-6">
		                    <div class="image-tile outer-title">
		                        <img alt="Pic" src="<?php echo iaBase(); ?>assets/images/post_img2.jpg">
		                        <div class="title mb16">
		                            <h5>IRS eliminates automatic 1099-MISC & W-2 extensions for 2018</h5>
		                            <span>August 3rd, 2018</span>
		                        </div>
		                        <p class="lead mb0">
		                            Not great news from the IRS for any business that has contractors or employees! IRS UPDATE AS OF 08/02/2018 The IRS
		                        </p>
		                    </div>
		                </div>
		                <div class="col-md-4 col-sm-6">
		                    <div class="image-tile outer-title">
		                        <img alt="Pic" src="<?php echo iaBase(); ?>assets/images/post_img3.png">
		                        <div class="title mb16">
		                            <h5>Hire a contractor for your business this year!</h5>
		                            <span>July 26th, 2018

</span>
		                        </div>
		                        <p class="lead mb0">
		                            Not every aspect of your business requires a permanent employee. For many business owners, hiring a contractor is a great way
		                        </p>
		                    </div>
		                </div>
		            </div>

		        </div>

		    </section>
		    <section class="subscribe-box">
		        <div class="container">
		            <div class="row mb48 mb-xs-24">
		                <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 text-center">
		                    <h4>Subscribe to our Newsletter</h4>
		                </div>
		            </div>

		            <div class="row mb40 mb-xs-24">
		                <div class="col-sm-10 col-md-offset-2">
		                    <form class="form-newsletter halves" data-success="Thanks for your registration, we will be in touch shortly." data-error="Please fill all fields correctly.">
		                        <input type="text" name="email" class="form-control email mb0 transparent validate-email validate-required signup-email-field" placeholder="Insert your email">
		                        <button type="submit" class="btn btn-lg btn-block btn-black uppercase mb0" style="width: 20% !important;">Send</button>
		                        <iframe srcdoc="<!-- Begin MailChimp Signup Form --><link href=&quot;https://cdn-images.mailchimp.com/embedcode/classic-081711.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;><style type=&quot;text/css&quot;>    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }    /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */</style><div id=&quot;mc_embed_signup&quot;><form action=&quot;//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=94d040322a&quot; method=&quot;post&quot; id=&quot;mc-embedded-subscribe-form&quot; name=&quot;mc-embedded-subscribe-form&quot; class=&quot;validate&quot; target=&quot;_blank&quot; novalidate>    <div id=&quot;mc_embed_signup_scroll&quot;>    <h2>Subscribe to our mailing list</h2><div class=&quot;indicates-required&quot;><span class=&quot;asterisk&quot;>*</span> indicates required</div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-EMAIL&quot;>Email Address  <span class=&quot;asterisk&quot;>*</span></label>    <input type=&quot;email&quot; value=&quot;&quot; name=&quot;EMAIL&quot; class=&quot;required email&quot; id=&quot;mce-EMAIL&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-FNAME&quot;>First Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;FNAME&quot; class=&quot;&quot; id=&quot;mce-FNAME&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-LNAME&quot;>Last Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;LNAME&quot; class=&quot;&quot; id=&quot;mce-LNAME&quot;></div>    <div id=&quot;mce-responses&quot; class=&quot;clear&quot;>        <div class=&quot;response&quot; id=&quot;mce-error-response&quot; style=&quot;display:none&quot;></div>        <div class=&quot;response&quot; id=&quot;mce-success-response&quot; style=&quot;display:none&quot;></div>    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->    <div style=&quot;position: absolute; left: -5000px;&quot;><input type=&quot;text&quot; name=&quot;b_77142ece814d3cff52058a51f_94d040322a&quot; tabindex=&quot;-1&quot; value=&quot;&quot;></div>    <div class=&quot;clear&quot;><input type=&quot;submit&quot; value=&quot;Subscribe&quot; name=&quot;subscribe&quot; id=&quot;mc-embedded-subscribe&quot; class=&quot;button&quot;></div>    </div></form></div><script type='text/javascript' src='https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script><!--End mc_embed_signup-->" class="mail-list-form">
		                        </iframe>
		                    </form>
		                </div>
		            </div>
		        </div>

		    </section>
		    <section class="help-block-home">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-6 text-center">
		                    <div class="feature">
		                        <h5 class="uppercase">Need help? Give us a call.</h5>
		                        <p class="lead">English: 616-325-9332<br>Español: 361-884-1500</p>
		                    </div>
		                </div>
		                <div class="col-sm-6 text-center">
		                    <div class="feature">
		                        <h5 class="uppercase">Headquarters</h5>
		                        <p class="lead">2250 Sprucewood Ct, NE<br>Belmont, MI 49306</p>
		                    </div>
		                </div>
		            </div>

		        </div>

		    </section><!-- <footer class="footer-2 bg-dark text-center-xs">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<a href="#"><img class="image-xxs fade-half" alt="Pic" src="img/logo-light.png"></a>
						</div>

						<div class="col-sm-4 text-center">
							<span class="fade-half">
								© Copyright 2015 Medium Rare - All Rights Reserved
							</span>
						</div>

						<div class="col-sm-4 text-right text-center-xs">
							<ul class="list-inline social-list">
								<li><a href="#"><i class="ti-twitter-alt"></i></a></li>
								<li><a href="#"><i class="ti-facebook"></i></a></li>
								<li><a href="#"><i class="ti-dribbble"></i></a></li>
								<li><a href="#"><i class="ti-vimeo-alt"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
		</div>


	<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/flexslider.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html> -->
