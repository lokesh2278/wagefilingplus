<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: url('../assets/images/filecorrections.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 30px;">
					<h3>File <span class="light">Corrections</span></h3>
					<h5>Instantly file 1099-MISC corrections even if you did not originally file with us! </h5>
				</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<h4>Filing Correction Forms:</h4>
			<p class="lead">Make a mistake on a 1099-MISC that has already been filed?  Take a look at the common errors and identify which  which type of error was made and how to correct.  You can utilize this service even if you filed in the past or with another company.</p>
			<br>
			<h5>There are 2 Types of common errors that can be corrected with WageFilingPlus.</h5>
			<ul class="list-unstyled">
				<li>You must decide if you have a Type 1 or Type 2 correction before starting.</li>
				<li>Please read what Type 1 and Type 2 covers. </li>
				<li>Wrong Money Amount Entered - Type 1 Error</li>
				<li>Wrong Tax ID entered - Type 2 Error</li>
                <li>Sent a 1099 to his company, but should have been to him individually, or vice-versa - Type 2</li>
                <li>Sent a 1099 but should have not done so - Type 1</li>
			</ul>
			<br>
			<h5>Correction Type 1 (covers incorrect money amount, incorrect address, incorrect payee name or the form should not have been filed)</h5>
			<p class="lead">IMPORTANT! Most important when you begin to do a correction is you have to re-enter your information. DO NOT Bring Files Forward, this will effect any other filings associated.  If you have questions always feel free to contact us</p>
			<ul class="list-unstyled dot">
				<li>From the home page Login with your e-Mail and password.</li>
				  <li>From the Main Menu, click NEW COMPANY Select 1099-Misc.</li>
				  <li>Enter the Company/Filer information EXACTLY THE SAME as the previously submitted file.</li>
				  <li>From the Main Menu select that company by clicking on the GREEN UNDERLINED name.</li>
				  <li>Click Add a Payee. CHECK THE CORRECTIONS checkbox at the top of the red form.</li>
				  <li>Enter all information EXACTLY as on the previously submitted form, including the wrong Tax ID, etc.</li>
				  <li>Enter ZERO in the Money amount, and Save this payee.</li>
				  <li>Click Add a Payee again. This time DO NOT check the corrections checkbox.</li>
				  <li>Enter the correct Tax ID, Name, address, amount, etc. for this second form.</li>

			</ul>
			<ul>
				<li>To correct a MONEY amount: change the money to the correct amount.</li>
				<li>To correct an address: change the address.</li>
				<li>To correct payee name: change the payee name.</li>
				<li>A return was filed when one should not have been filed:  put ZERO in the money amount.</li>
			</ul>
			<p style="font-size: 14px;">Finally, go to the Main Menu, and checkout to submit and e-File this new file. Print the corrected copy and mail it to the recipient. You are now finished. The correction will be e-Filed to the IRS.</p>

			<h5>Correction Type 2 (covers incorrect Tax ID, or both name and address were wrong)</h5>
			<ul class="list-unstyled dot">
				<li>From the home page Login with your e-Mail and password.</li>
				<li>From the Main Menu, click NEW COMPANY Select 1099-Misc.</li>
				<li>Enter the Company/Filer information EXACTLY THE SAME as the previously submitted file.</li>
				<li>From the Main Menu select that company by clicking on the GREEN UNDERLINED name.</li>
				<li>Click Add a Payee. CHECK THE CORRECTIONS checkbox at the top of the red form.</li>
				<li>Enter all information EXACTLY as on the previously submitted form, including the wrong Tax ID, etc.</li>
                <li>Enter ZERO in the Money amount, and Save this payee.</li>
                <li>Click Add a Payee again. This time DO NOT check the corrections checkbox.</li>
                <li>Enter the correct Tax ID, Name, address, amount, etc. for this second form.
</li>
			</ul>
			<p class="lead">Finally, go to the Main Menu, checkout to submit and e-File this new file. Print the corrected copies and mail them to the recipient. You may wish to send the recipient a letter explaining that the first 1099 form with the correction box checked and zero in the money amount will cancel the form e-Filed previously with the wrong Tax ID. The second 1099 form, with no correction checked, is the corrected form. You are now finished. The Type 2 correction will be e-Filed to the IRS.</p>
			<hr>
			<h4>Contact Us</h4>
			<h5>WageFilingPlus. LLC.</h5>
			<h5>Telephone: 616-325-9332</h5>
			<h5>E-mail: <a href="mailto:support@wagefilingplus.com">support@Wagefilingplus.com</a></h5>
			<h5>Trusted by</h4>
			<img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
