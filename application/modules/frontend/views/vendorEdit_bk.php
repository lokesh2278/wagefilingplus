<div id="blaze-banner">
      <div class="container">
        <div class="navigation-container"><div class="wrap wrap-one-logo">
<!-- <a href="/blaze/" id="blaze-logos" class="w9">
  <img id="w9-logo" src="<?= iaBase() ?>assets/images/WageFilingw9-Logo-XSmall.png" alt="WageFiling W-9">
</a> -->
<div class="row">
  <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
    <?php $cid = !empty($vendor[0]->c_id)?$vendor[0]->c_id:''?>
    <h1><a class="company-link" href="<?= base_url('vendor')?>/<?= $cid ?>"><span class="company-name">You are now issuing W-9 Request for  <?= companyName($cid) ?></span></a></h1>
  </div>
  <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>
</div>
<a class="back-link" href="<?= base_url('blaze')?>" style="display: block;">
  <i class="glyphicon glyphicon-chevron-left"></i> Main Menu </a>
</div>
</div>
        <div id="alerts-container" class="alerts-container"><div></div></div>
      </div>
    </div>
<div class="container">
  <div class="row">
    <div class="show-sidebar col-md-3 col-md-push-9" style="display: block;">
      <div class="sidebar">
        <div class="btn-group btn-group-shift-md show-print" style="display: none;">
          <button type="button" class="btn btn-primary btn-lg print-button"><i class="glyphicon glyphicon-print"></i> Print PDF</button>
        </div>
        <div class="btn-group btn-group-shift-md show-resend">
          <button type="button" class="btn btn-primary btn-lg resend-button"><i class="glyphicon glyphicon-envelope"></i> Resend Request</button>
        </div>
        <div class="btn-group btn-group-shift-md show-update" style="display: none;">
          <button type="button" class="btn btn-primary btn-lg update-button"><i class="glyphicon glyphicon-envelope"></i> Request Update</button>
        </div>
        <p><span class="status-name">Requested</span> on <span class="status-date"><?= date('m/d/Y',strtotime($vendor[0]->created_at)) ?></span></p>
      </div>
    </div>
  <form class="editForm" id="editForm" action="" accept-charset="UTF-8" method="post">
    <input id="c110_v_id" class="form-control" name="v_id" maxlength="40" type="hidden" value="<?= !empty($vendor[0]->v_id)?$vendor[0]->v_id:''?>">
    <input id="c110_c_id" class="form-control" name="c_id" maxlength="40" type="hidden" value="<?= !empty($vendor[0]->c_id)?$vendor[0]->c_id:''?>">
    <div class="col-md-9 col-lg-8 col-md-pull-3">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title form-title">Edit W-9 Request</h3>
        </div>
        <div class="panel-body panel-body-extra">
          <div class="form-container">
            <div class="row">
  <div class="col-sm-6" data-fields="name">
    <div class="form-group field-name">      
      <label class="control-label" for="c110_name">
        <span class="label-inner">Vendor Name</span>
      </label>              
      <div data-editor="">
        <input id="c110_name" class="form-control" name="v_name" maxlength="40" type="text" value="<?= !empty($vendor[0]->v_name)?$vendor[0]->v_name:''?>">
      </div>            
      <p class="help-block" data-error=""></p>      
      <p class="help-block"></p>    
    </div>
  </div>
  <div class="col-sm-6" data-fields="email"><div class="form-group field-email">      
    <label class="control-label" for="c110_email">
      <span class="label-inner">Email</span>
    </label>              
    <div data-editor="">
      <input id="c110_email" class="form-control" name="v_email" type="text" value="<?= !empty($vendor[0]->v_email)?$vendor[0]->v_email:''?>">
    </div>            
    <p class="help-block" data-error=""></p>      
    <p class="help-block"></p>    
  </div>
</div>
 <!--  <div class="col-sm-4" data-fields="account_number">
    <div class="form-group field-account_number">      
      <label class="control-label" for="c110_account_number">
        <span class="label-inner">Account No. (optional)</span>
      </label>              
      <div data-editor="">
      
      </div>            
      <p class="help-block" data-error=""></p>      
      <p class="help-block"></p>    
    </div>
  </div> -->
</div>
</div>
        </div>
        <div class="panel-footer save-footer" style="display: block;">
          <button type="submit" class="btn btn-lg btn-primary save-button">Save</button>
        </div>
      </div>
    </div>
  </form>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
    $('#editForm').validate({ // initialize the plugin
        rules: {         
            v_name: {
                required: true,                        
            },          
            v_email: {
                required: true, 
                email: true   
            },
           
        },
        messages: {
            v_email: {
                required: 'Required',
                email: 'Invalid email address'               
            },          
            v_name : {
                required: 'Required', 
            },        
       }
    });

    $('button.resend-button').click(function(){
        var v_email = $('#c110_email').val();
        var c_id = $('#c110_c_id').val();
        var v_name = $('#c110_name').val();
        var v_id = $('#c110_v_id').val();
        $(this).attr('disabled','disabled');
        var dataNew = { v_email : v_email,c_id : c_id ,v_id : v_id ,v_name :v_name }; 
        var baseUrl = '<?php echo base_url('resendemail')  ?>';
         $.ajax({
            url: baseUrl, 
            type: "POST",             
            data: dataNew,             
            success: function(data) {
                 $('#alerts-container').html('<div class="alert alert-success">Resent request for '+v_name+'</div>');
            }
          });
    });

});
  
</script>