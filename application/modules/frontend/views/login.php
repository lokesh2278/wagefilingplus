<div id="main" class="main container">
        <div id="right" class="right-side">
<div><p></p>
<?php
if (!empty($this->session->flashdata('item'))) {
    $message = $this->session->flashdata('item');
    ?>
 <div class="alert alert-success">
  <!-- <h3><?php echo $message['message']; ?></h3> -->
  <!-- <p>For Security purposes, <br/> an activation link has been send to the email address you used to create your account,<br> Please check your mailbox and find the message from support@wagefilingplus.com</p> -->
  <h3>Success :</h3>
  <p>Successfully Registered, now you can Sign In.</p>
 </div>
<?php
}
?>
<?php
if (!empty($this->session->flashdata('active'))) {
    $message = $this->session->flashdata('active');
    ?>
 <div class="alert alert-success">
  <h3><?php echo $message['message']; ?></h3>
  <p>Your account has been activated, now you can Sign In.</p>
 </div>
<?php
}
?>
<div class="loginform white-box" style="width: 100%; max-width: 420px; margin-top: 20px; margin-bottom: 20px;">
  <h1>Welcome Back</h1>
<div>
<form id="loginform" action="" accept-charset="UTF-8" method="post">

  <div id="form">
    <div data-fieldsets="">
      <fieldset data-fields="">
        <div class="form-group field-email">
          <label class="control-label" for="email">
            <span class="label-inner">Email</span>
          </label>
          <div data-editor="">
            <input id="email" class="form-control" name="email" type="email">
          </div>
        </div>
        <div class="form-group field-password">
          <label class="control-label" for="password">
            <span class="label-inner">Password</span>
          </label>
          <div data-editor="">
            <input id="password" class="form-control" name="password" type="password">
          </div>
        </div>
      </fieldset>
    </div>
  </div>
  <p>
    <input type="submit" name="commit" value="Sign In" class="btn btn-lg btn-primary" data-disable-with="Sign In">&nbsp; &nbsp; &nbsp;
    <a href="<?=base_url('forgotpassword')?>">Forgot password?</a>

  </p>
</form>
</div>

        </div>
    </div>

  </div>
</div>


  <script>
  $(document).ready(function () {
    $('#loginform').validate({ // initialize the plugin
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            },

        },
        messages: {
            email: {
                required: 'Required',
                email: 'Invalid email address'
            },
            password: {
                required: 'Required',
            },
       }
    });

});

  </script>