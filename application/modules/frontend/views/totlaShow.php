<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
            <div class="row">
              <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
                <h1><a class="company-link"><span class="company-name"><?=companyName($cid)?></span></a></h1>
              </div>
            </div>
            <a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
            </div>
			</div>
      </div>
    </div>


 <div class="container">
  <div class="row">

    <div class="col-md-12">
     <div class="row">
     <center>
        <h4>WageFilingPlus.com</h4><h4>1099/W-2 style Totals Summary and Reconciliation page</h4>
        <span style="font-family: Courier New; font-size: normal; font-weight: 600 !important;">(Keep For Your Records - Do not file, We e-file this for you)</span>
        </center>

      </div>
      <div class="table-container" style="width:100%">
        <div class="table-responsive">
<table class="w9-table table table-striped table-hover table-responsive  table-bordered" name='example-table'>
<thead>
<tr>
 <th colspan="2">
<span><h5>Tax Year: <?php echo isset($c_details[0]->TaxYear) ? $c_details[0]->TaxYear : ''; ?></h5></span>
File Number: &nbsp;&nbsp;  <span><?php echo isset($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : ''; ?> </span><br>
Form Type:&nbsp; <span><?php echo isset($c_details[0]->FileName) ? $c_details[0]->FileName : ''; ?> Form  <?php echo isset($c_details[0]->FormType) ? $c_details[0]->FormType : ''; ?></span><br>

Payer:<br>
<span><?php echo isset($c_details[0]->FileName) ? $c_details[0]->FileName : ''; ?><br>
<?php echo isset($c_details[0]->c_address) ? $c_details[0]->c_address : ''; ?><?php echo isset($c_details[0]->c_address_2) ? ', ' . $c_details[0]->c_address_2 : ''; ?><br>
<?php echo isset($c_details[0]->c_city) ? $c_details[0]->c_city : ''; ?>, <?php echo isset($c_details[0]->c_state) ? $c_details[0]->c_state : ''; ?> <?php echo isset($c_details[0]->c_zip) ? $c_details[0]->c_zip : ''; ?></span><br>
Phone: <span><?php echo isset($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : ''; ?></span> E-Mail: <span><?php echo !empty($c_details[0]->c_email) ? $c_details[0]->c_email : 'Optional'; ?></span><br>

   </th>
  </tr>
  <tr class="float-delete-button" style="background-color: #337ab7; color: #ffffff;">
    <th colspan="2">TOTALS :</th>
  </tr>
</thead>
<tbody class="forms-container">
<?php

$counter = 0;
$Box1 = 0.00;
$Box1a = 0.00;
$Box1b = 0.00;
$Box2a = 0.00;
$Box2b = 0.00;
$Box2c = 0.00;
$Box2d = 0.00;
$Box2 = 0.00;
$Box3 = 0.00;
$Box4 = 0.00;
$Box5 = 0.00;
$Box6 = 0.00;
$Box7 = 0.00;
$Box8 = 0.00;
$Box9 = 0.00;
$Box10 = 0.00;
$Box11 = 0.00;
$Box11a = 0.00;
$Box12 = 0.00;
$Box12a = 0.00;
$Box12b = 0.00;
$Box12c = 0.00;
$Box13 = 0.00;
$Box14 = 0.00;
$Box15a = 0.00;
$Box15b = 0.00;
$Box16 = 0.00;
$Box16 = 0.00;
$Box17 = 0.00;
$Box18 = 0.00;
$Box19 = 0.00;
$Box22 = 0.00;
$Box23 = 0.00;
$Box24 = 0.00;
$Box25 = 0.00;

$total = array();

if (isset($v_details) && !empty($v_details)) {

    if (isset($c_details[0]->FormType) && $c_details[0]->FormType == '1099-Misc' || $c_details[0]->FormType == '1099-Div') {

        foreach ($v_details as $v_detail) {

            if ($v_detail->void != 1) {
                $total[] = 1;
                $Box1 += isset($v_detail->rents) ? $v_detail->rents : 0;
                $Box2 += isset($v_detail->royalties) ? $v_detail->royalties : 0;
                $Box3 += isset($v_detail->other_income) ? $v_detail->other_income : 0;
                $Box4 += isset($v_detail->fed_income_with_held) ? $v_detail->fed_income_with_held : 0;
                $Box5 += isset($v_detail->fishing_boat_proceed) ? $v_detail->fishing_boat_proceed : 0;
                $Box6 += isset($v_detail->mad_helthcare_pmts) ? $v_detail->mad_helthcare_pmts : 0;
                $Box7 += isset($v_detail->non_emp_compansation) ? $v_detail->non_emp_compansation : 0;
                $Box8 += isset($v_detail->pmts_in_lieu) ? $v_detail->pmts_in_lieu : 0;
                $Box10 += isset($v_detail->crop_Insurance_proceeds) ? $v_detail->crop_Insurance_proceeds : 0;
                $Box13 += isset($v_detail->excess_golden_par_pmts) ? $v_detail->excess_golden_par_pmts : 0;
                $Box14 += isset($v_detail->gross_paid_to_an_attomey) ? $v_detail->gross_paid_to_an_attomey : 0;
                $Box16 += isset($v_detail->state_tax_with_held) ? $v_detail->state_tax_with_held : 0;
                $Box15a += isset($v_detail->sec_409a_deferrals) ? $v_detail->sec_409a_deferrals : 0;
                $Box15b += isset($v_detail->sec_409a_Income) ? $v_detail->sec_409a_Income : 0;
            }

        }
        ?>
        <tr><td>Box1</td><td>$<?php echo $Box1; ?></td></tr>
        <tr><td>Box2</td><td>$<?php echo $Box2; ?></td></tr>
        <tr><td>Box3</td><td>$<?php echo $Box3; ?></td></tr>
        <tr><td>Box4</td><td>$<?php echo $Box4; ?></td></tr>
        <tr><td>Box5</td><td>$<?php echo $Box5; ?></td></tr>
        <tr><td>Box6</td><td>$<?php echo $Box6; ?></td></tr>
        <tr><td>Box7</td><td>$<?php echo $Box7; ?></td></tr>
        <tr><td>Box8</td><td>$<?php echo $Box8; ?></td></tr>
        <tr><td>Box10</td><td>$<?php echo $Box10; ?></td></tr>
        <tr><td>Box13</td><td>$<?php echo $Box13; ?></td></tr>
        <tr><td>Box14</td><td>$<?php echo $Box14; ?></td></tr>
        <tr><td>Box16</td><td>$<?php echo $Box16; ?></td></tr>
        <tr><td>Box15a</td><td>$<?php echo $Box15a; ?></td></tr>
        <tr><td>Box15b</td><td>$<?php echo $Box15b; ?></td></tr>
        <?php

    } else {

        foreach ($v_details as $v_detail) {

            if ($v_detail->voidbox != 1) {
                $total[] = 1;
                $Box1 += isset($v_detail->box1) ? $v_detail->box1 : 0;
                $Box2 += isset($v_detail->box2) ? $v_detail->box2 : 0;
                $Box3 += isset($v_detail->box3) ? $v_detail->box3 : 0;
                $Box4 += isset($v_detail->box4) ? $v_detail->box4 : 0;
                $Box5 += isset($v_detail->box5) ? $v_detail->box5 : 0;
                $Box6 += isset($v_detail->box6) ? $v_detail->box6 : 0;
                $Box7 += isset($v_detail->box7) ? $v_detail->box7 : 0;
                $Box8 += isset($v_detail->box8) ? $v_detail->box8 : 0;
                $Box9 += isset($v_detail->box9) ? $v_detail->box9 : 0;
                $Box10 += isset($v_detail->box10) ? $v_detail->box10 : 0;
                $Box11 += isset($v_detail->box11) ? $v_detail->box11 : 0;
                $Box11a += isset($v_detail->box11a) ? $v_detail->box11a : 0;
                $Box12 += isset($v_detail->box12AM) ? $v_detail->box12AM : 0;
                $Box12a += isset($v_detail->box12BM) ? $v_detail->box12BM : 0;
                $Box12b += isset($v_detail->box12CM) ? $v_detail->box12CM : 0;
                $Box12c += isset($v_detail->box12DM) ? $v_detail->box12DM : 0;
                $Box13 += isset($v_detail->box13) ? $v_detail->box13 : 0;
                $Box16 += isset($v_detail->box16) ? $v_detail->box16 : 0;
                $Box17 += isset($v_detail->box17) ? $v_detail->box17 : 0;
                $Box18 += isset($v_detail->box18) ? $v_detail->box18 : 0;
                $Box19 += isset($v_detail->box19) ? $v_detail->box19 : 0;
                $Box22 += isset($v_detail->box22) ? $v_detail->box22 : 0;
                $Box23 += isset($v_detail->box23) ? $v_detail->box23 : 0;
                $Box24 += isset($v_detail->box24) ? $v_detail->box24 : 0;
                $Box25 += isset($v_detail->box25) ? $v_detail->box25 : 0;
            }

        }

        ?>
        <tr><td>Box1</td><td>$<?php echo $Box1; ?></td></tr>
        <tr><td>Box2</td><td>$<?php echo $Box2; ?></td></tr>
        <tr><td>Box3</td><td>$<?php echo $Box3; ?></td></tr>
        <tr><td>Box4</td><td>$<?php echo $Box4; ?></td></tr>
        <tr><td>Box5</td><td>$<?php echo $Box5; ?></td></tr>
        <tr><td>Box6</td><td>$<?php echo $Box6; ?></td></tr>
        <tr><td>Box7</td><td>$<?php echo $Box7; ?></td></tr>
        <tr><td>Box8</td><td>$<?php echo $Box8; ?></td></tr>
        <tr><td>Box9</td><td>$<?php echo $Box9; ?></td></tr>
        <tr><td>Box10</td><td>$<?php echo $Box10; ?></td></tr>
        <tr><td>Box11</td><td>$<?php echo $Box11; ?></td></tr>
        <tr><td>Box11a</td><td>$<?php echo $Box11a; ?></td></tr>
        <tr><td>Box12</td><td>$<?php echo $Box12; ?></td></tr>
        <tr><td>Box12</td><td>$<?php echo $Box12a; ?></td></tr>
        <tr><td>Box12</td><td>$<?php echo $Box12b; ?></td></tr>
        <tr><td>Box12</td><td>$<?php echo $Box12c; ?></td></tr>
        <tr><td>Box16</td><td>$<?php echo $Box16; ?></td></tr>
        <tr><td>Box17</td><td>$<?php echo $Box17; ?></td></tr>
        <tr><td>Box18</td><td>$<?php echo $Box18; ?></td></tr>
        <tr><td>Box19</td><td>$<?php echo $Box19; ?></td></tr>
        <tr><td>Box22</td><td>$<?php echo $Box22; ?></td></tr>
        <tr><td>Box23</td><td>$<?php echo $Box23; ?></td></tr>
        <tr><td>Box24</td><td>$<?php echo $Box24; ?></td></tr>
        <tr><td>Box25</td><td>$<?php echo $Box25; ?></td></tr>
        <?php

    }
}?>
</tbody>
<tfoot style="background-color: #F2F2F2; color: #333333;"><tr><td colspan="2">Total Payees: <?php echo count($total); ?></td></tr>
    <tr><td colspan="2">FileIndex=<?php echo isset($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : ''; ?></td></tr>
    <tr><td colspan="2"><a href="<?=base_url('confirmation')?>/<?=$c_details[0]->FileSequence?>">View e-Filing Confirmation</a></td></tr>
</tfoot>
</table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
