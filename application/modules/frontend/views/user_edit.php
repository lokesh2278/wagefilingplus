<style type="text/css">
  .require {  /* Marker for required fields */
   color: red;
}
</style>
<div id="blaze-banner">
  <div class="container">

    <div class="navigation-container">
      <div class="wrap wrap-one-logo text-center">
        <a class="back-link" href="<?=base_url('blaze');?>" style="display: block; margin-left: 128px; "><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
        <div class="row">
          <div id="nav-company" class="col-sm-12 col-md-12 col-lg-12">
            <h1 style="font-size: 29px; "><a class="company-link"><span class="company-name">Edit Account</span></a></h1>
            <p class="lead" style="text-align: center;">One Account Handles Unlimited Clients</p>
          </div>
          <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>
        </div>
      </div>
    </div>
    <div id="alerts-container" class="alerts-container"></div>
  </div>
</div>
<div class="content-container" id="main-view-content">
  <div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">CHANGE User Account - All Fields are Required</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <?php foreach ($users as $user) {
    ?>
                  <form class="new_user" id="new_user" action="edituserdata" accept-charset="UTF-8" method="post">
  <div id="user-form">
   <div class="error" style="margin-bottom:10px;"> <?php echo !empty($this->session->get_userdata()['alert_msg']['msg']) ? $this->session->get_userdata()['alert_msg']['msg'] : ''; ?> </div>
   <?php if(!empty($this->session->set_userdata)){ ?>
   <?php $this->session->set_userdata('alert_msg', array('msg' => ''));?>
   <? } ?>
    <div>
  <div class="row">
    <div class="col-sm-12" data-fields="full_name"><div class="form-group field-full_name">
      <label class="control-label" for="full_name"><span class="label-inner">Full Name</span><span class="require">*</span></label>              <div data-editor="">
      <input id="full_name" class="form-control" name="full_name" value="<?=$user->name?>" type="text" required="required"/>
       </div>
      </div>
     </div>

   </div>
  <div class="row">
    <div class="col-sm-6" data-fields="email"><div class="form-group field-email">
      <label class="control-label" for="email"><span class="label-inner">Email</span><span class="require">*</span></label>              <div data-editor="">
      <input id="email" class="form-control" name="email" type="email" value="<?=$user->UEmail?>" required="required"/>
       </div>
      </div>
     </div>
    <div class="col-sm-6" data-fields="password"><div class="form-group field-password">      <label class="control-label" for="c-email"><span class="label-inner">Confirm Email</span><span class="require">*</span></label>              <div data-editor="">
      <input id="cemail" class="form-control" name="cemail" value="<?=$user->UEmail?>" type="email" required="required"/>
      </div>
      </div>
     </div>

   </div>
   <div class="row">
    <div class="col-sm-12" data-fields="email"><div class="form-group field-email">
      <label class="control-label" for="email"><span class="label-inner">Company</span><span class="require">*</span></label>              <div data-editor="">
      <input id="company" class="form-control" name="company" type="text" value="<?=$user->Ucompany?>" required="required"/>
       </div>
      </div>
     </div>
    <!-- <div class="col-sm-6" data-fields="contact"><div class="form-group field-password">      <label class="control-label" for="contact"><span class="label-inner">Contact</span></label>              <div data-editor="">
      <input id="contact" class="form-control" name="contact" type="text" value="<?=$user->contact?>" required="required"/>
      </div>
      </div>
     </div> -->

   </div>
  <!--  <div class="row">
    <div class="col-sm-6" data-fields="title"><div class="form-group field-email">
      <label class="control-label" for="title"><span class="label-inner">Title</span></label>              <div data-editor="">
      <input id="title" class="form-control" name="title" type="text" value="<?=$user->title?>" required="required"/>
       </div>
      </div>
     </div>
    <div class="col-sm-6" data-fields="address"><div class="form-group field-password">      <label class="control-label" for="address"><span class="label-inner">Address</span></label>              <div data-editor="">
      <input id="address" class="form-control" name="address" type="text" value="<?=$user->address?>" required="required"/>
      </div>
      </div>
     </div>

   </div> -->
   <!-- <div class="row">
    <div class="col-sm-6" data-fields="city"><div class="form-group field-email">
      <label class="control-label" for="city"><span class="label-inner">City</span></label>              <div data-editor="">
      <input id="city" class="form-control" name="city" type="text" value="<?=$user->city?>" required="required"/>
       </div>
      </div>
     </div>
    <div class="col-sm-6" data-fields="state"><div class="form-group field-password">      <label class="control-label" for="state"><span class="label-inner">State</span></label>              <div data-editor="">
      <input id="state" class="form-control" name="state" type="text"  value="<?=$user->state?>" required="required"/>
      </div>
      </div>
     </div>

   </div> -->

  <div class="row">
    <!-- <div class="col-sm-6" data-fields="zip"><div class="form-group field-phone_number">      <label class="control-label" for="zip"><span class="label-inner">Zip</span></label>              <div data-editor="">
       <input id="zip" class="form-control" name="zip" type="number" value="<?=$user->zip?>" required="required"/>
       </div>            <p class="help-block" data-error=""></p>      <p class="help-block"></p>
      </div>
     </div> -->
    <div class="col-sm-12" data-fields="phone_number"><div class="form-group field-phone_number">      <label class="control-label" for="phone_number"><span class="label-inner">Phone</span><span class="require">*</span></label>              <div data-editor="">
       <input id="phone_number" class="form-control" name="phone_number" value="<?=$user->Uphone?>" type="text" required="required">
       </div>         <p class="help-block" data-error=""></p>      <p class="help-block"></p>
      </div>
     </div>

   </div>
   <div class="row">
    <!-- <div class="col-sm-6" data-fields="fax"><div class="form-group field-phone_number">      <label class="control-label" for="fax"><span class="label-inner">Fax</span></label>              <div data-editor="">
       <input id="fax" class="form-control" name="fax" type="number" value="<?=$user->fax?>" required="required"/>
       </div>            <p class="help-block" data-error=""></p>      <p class="help-block"></p>
      </div>
     </div> -->

   </div>
   <div class="row">
    <div class="col-sm-6" data-fields="password">
    <div class="form-group field-password">
    <label class="control-label" for="password"><span class="label-inner">Change Password</span></label>
    <div data-editor="">
       <input id="password" class="form-control" name="password" type="password"   pattern="^\S{6,}$" >
       </div>
       <p class="help-block" data-error=""></p>
       <p class="help-block"></p>
      </div>
     </div>
    <div class="col-sm-6" data-fields="cpassword">
    <div class="form-group field-cpassword">
    <label class="control-label" for="cpassword"><span class="label-inner">Confirm Change Password</span><span class="require">*</span></label>
    <div data-editor="">
       <input id="cpassword" class="form-control" name="cpassword" type="password"  pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : '');" />
       </div>
       <p class="help-block" data-error=""></p>
       <p class="help-block"></p>
      </div>
     </div>
   </div>
  </div>
 </div>
  <p>
    <button type="submit" class="btn btn-primary">Change Account</button>
    <button class="btn btn-primary"><a href="blaze" style="color: #ffffff !important;">Cancel</a></button>
  </p>
</form>
<?php }?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title">ONE ACCOUNT HANDLES ALL CLIENTS</h4>
      </div>
      <div class="panel-body">
        <p>This page allows you, the account holder, to change your e-Mail, password, or any other item above. /p>
        <p>Changing your user account information DOES NOT change your 1099 or W-2 data files. Any 1099 or W-2 data files already entered will remain under this account. Be sure to write down and keep your new EMAIL and/or PASSWORD.</p>
      </div>
    </div>
  </div>
</div>
  </div>
</div>
<script>
  $(document).ready(function () {
       //event.preventDefault();

        $('#new_user').validate({ // initialize the plugin
        rules: {
            full_name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
                // minlength: 7,
                // maxlength: 15,
            },
            cemail: {
              equalTo: '[name="email"]',
            },
            phone_number: {
                required: true,
                digits : true,
                minlength: 10,
                maxlength: 10,
            },
            contact: {
                required: true,
            },
            password: {
               // required: true,
                // minlength: 3,
                // maxlength: 18,

            },
            cpassword: {
                    equalTo: "#password",
            },
             company: {
                required: true,
            },
            accept: {
              required: true,
            }
        },
      messages: {
             full_name: {
                required: 'Required',
            },
            email: {
                required: 'Required',
                email: 'Invalid email address',
                // minlength: 'Must be 7 characters',
                // maxlength: 'Must be 15 characters',
            },
             cemail: "Confirm email must be Same as email",
            phone_number: {
                required: 'Required',
                digits : 'Must be 10 digits',
                minlength: 'Must be 10 digits',
                maxlength: 'Must be 10 digits',
            },
            password: {
                required: 'Required',
                minlength:'Minimum 3 characters',
                maxlength: 'Must be 18 characters',
            },
            cpassword: " Enter Confirm Password Same as Password",
             company: {
                required: 'Required',
            },
            accept: {
                required: 'You must accept terms & condition',
            }
       }
    });

});

  </script>