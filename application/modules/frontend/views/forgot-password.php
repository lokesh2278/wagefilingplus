<link rel="stylesheet" type="text/css" href="<?= iaBase();?>/assets/css/application-d1.css">

<div id="main" class="main container">
  <?php if(empty($show)){ ?>
        <div id="right" class="right-side">
          



<div><p></p>

<div class="loginform white-box" style="width: 100%; max-width: 420px; margin-top: 50px; margin-bottom: 50px;">
  <h1>Forgot Password?</h1>
     <div class="error" style="margin-bottom:10px;"> <?php echo !empty($this->session->get_userdata()['alert_msg']['msg'])?$this->session->get_userdata()['alert_msg']['msg']:''; ?> </div>
   <?php $this->session->set_userdata('alert_msg',array('msg'=>''));?>
    <div>
<form id="loginform" style="" action="" accept-charset="UTF-8" method="post">

  <div id="form">
    <div data-fieldsets="">
      <fieldset data-fields="">          
        <div class="form-group field-email">      
          <label class="control-label" for="email">
            <span class="label-inner">Enter your account email address</span>
          </label>              
          <div data-editor="">
            <input id="email" class="form-control" name="email" type="email">
          </div> 
        </div>  
      </fieldset>
    </div>
  </div>
  <p>
    <input type="submit" name="commit" value="Reset Password" class="btn btn-primary" data-disable-with="Reset Password">&nbsp; &nbsp; &nbsp;
    
    
  </p>
</form>
</div>

        </div>
    </div>
<?php } ?>
<?php if(!empty($show)){ ?>
 <div id="right" class="right-side" style="width: 55%;margin: 0 auto;">          
<div class="alert alert-password white-box alert-dismissable">
  <h5>We have emailed you a link to reset your password!</h5>
  <p><b>If you don’t receive the email, please do the following:</b></p>
  <p>1. Check your Spam folder</p>
  <p>2. If it's not there, you signed up with a different email. Please <a href="<?= base_url('forgotpassword')?>">try another email.</a></p>
  </div>
</div>
<?php } ?>
<p></p>
  </div>
</div>
  <script>
  $(document).ready(function () {
    $('#loginform').validate({ // initialize the plugin
        rules: {         
            email: {
                required: true,
                email: true               
            },          
            password: {
                required: true, 
            },
           
        },
        messages: {
            email: {
                required: 'Required',
                email: 'Invalid email address'               
            },          
            password: {
                required: 'Required', 
            },        
       }
    });

});
  
  </script>