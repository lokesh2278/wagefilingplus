<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: url('../assets/images/tinmatching.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 30px;">
					<h4>Form <span class="light">1099-MISC</span></h4>
					<h5>Everything you need to know about the IRS 1099-MISC Form</h5>
				</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<img src="<?php echo iaBase(); ?>assets/images/f1099msc-2.gif">
			<p></p>
			<p class="lead">Make a mistake on a 1099-MISC that has already been filed?  Take a look at the common errors and identify which  which type of error was made and how to correct.  You can utilize this service even if you filed in the past or with another company.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<div>
                    <div>
                      <h3>Box - Payer's Name, Address, Phone</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p><span class="smallpara">Fill in the legal business name and address of the company issuing the 1099-MISC</span><br>
                    </p>
                    <div>
                      <h3>Box - Payer's Federal Identification Number</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p><span class="smallpara">Fill in the Federal Employer Identification Number of the company issuing the 1099-MISC</span><br>
                    </p>
                    <div>
                      <h3>Box - Recipient's Identification Number</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter the recipient's identification number. Hyphens or symbols are not required or allowed, but it will show correctly on the recipient copy printed.</p>
                    <div>
                      <h3>Box - Recipient's Name, Address</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p><span class="smallpara">Fill in the name and street address, city and state for the recipient</span><br>
                    </p>
                    <div>
                      <h3>Box - Account Number</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">The account number is required if you have multiple accounts for a recipient for whom you are filing more than one Form 1099-MISC. Additionally, the IRS encourages you to designate an account number for all Forms 1099-MISC that you file. See part L in the General Instructions for Certain Information Returns.</p>
                    <div>
                      <h3>Box - 2nd TIN not.</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">You may enter an "X" in this box if you were notified by the IRS twice within 3 calendar years that the payee provided an incorrect TIN. If you mark this box, the IRS will not send you any further notices about this account. However, if you received both IRS notices in the same year, or if you received them in different years but they both related to information returns filed for the same year, do not check the box at this time. For purposes of the two-notices-in-3-years rule, you are considered to have received one notice. You are not required to send a second "B" notice upon receipt of the second notice. See part N in the General Instructions for Certain Information Returns for more information.</p>
                    <h3>Box 1 - Rents</h3>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter amounts of $600 or more for all types of rents, such as any of the following:</p>
                    <ul>
                      <li class="smallpara">Real estate rentals paid for office spaces. However, you do not have to report these payments on Form 1099-MISC if you paid them to a real estate agent. But the real estate agent must use Form 1099-MISC to report the rent paid over to the property owner.</li>
                      <li class="smallpara">Machine rentals (for example, renting a bulldozer to level your parking lot). If the machine rental is part of a contract that includes both the use of the machine and the operator, prorate the rental between the rent of the machine (report that in box 1) and the operator's charge (report that as nonemployee compensation in box 7).</li>
                      <li class="smallpara">Pasture rentals (for example, farmers paying for the use of grazing land).</li>
                    </ul>
                    <p class="smallpara">Public housing agencies must report in box 1 rental assistance payments made to owners of housing projects.</p>
                    <p class="smallpara"><strong>Coin operated amusements.</strong>&nbsp;If an arrangement between an owner of coin-operated amusements and an owner of a business establishment where the amusements are placed is a lease of the amusements or the amusement space, the owner of the amusements or the owner of the space, whoever makes the payments, must report the lease payments in box 1 of Form 1099-MISC if the payments total at least $600. However, if the arrangement is a joint venture, the joint venture must file a Form 1065, U.S. Return of Partnership Income, and provide each partner with the information necessary to report the partner's share of the taxable income. Coin-operated amusements include video games, pinball machines, jukeboxes, pool tables, slot machines, and other machines and gaming devices operated by coins or tokens inserted into the machines by individual users.</p>
                    <div>
                      <h3>Box 2 - Royalties</h3>
                    </div>
                    <h4 class="smallpara">IRS directions</h4>
                    <p class="smallpara">Enter gross royalty payments (or similar amounts) of $10 or more. Report royalties from oil, gas, or other mineral properties before reduction for severance and other taxes that may have been withheld and paid. Do not include surface royalties. They should be reported in box 1. Do not report oil or gas payments for a working interest in box 2; report payments for working interests in box 7. Do not report timber royalties made under a pay-as-cut contract; report these timber royalties on Form 1099-S, Proceeds from Real Estate Transactions.</p>
                    <p class="smallpara">Use box 2 to report royalty payments from intangible property such as patents, copyrights, trade names, and trademarks. Report the gross royalties (before reduction for fees, commissions or expenses) paid by a publisher directly to an author or literary agent, unless the agent is a corporation. The literary agent (whether or not a corporation) that receives the royalty payment on behalf of the author must report the gross amount of royalty payments to the author on Form 1099-MISC whether or not the publisher reported the payment to the agent on its Form 1099-MISC.</p>
                    <div>
                      <h3>Box 3 - Other Income</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter other income of $600 or more required to be reported on Form 1099-MISC that is not reportable in one of the other boxes on the form.</p>
                    <p class="smallpara">Also enter in box 3 prizes and awards that are not for services performed. Include the fair market value (FMV) of merchandise won on game shows. Also include amounts paid to a winner of a sweepstakes not involving a wager. If a wager is made, report the winnings on form W-2G, Certain Gambling Winnings.</p>
                    <p class="smallpara">Do not include prizes and awards paid to your employees. Report these on Form W-2. Do not include in box 3 prizes and awards for services performed by nonemployees, such as an award for the top commission salesperson. Report them in box 7.</p>
                    <p class="smallpara">Prizes and awards received in recognition of past accomplishments in religious, charitable, scientific, artistic, educational, literary, or civic fields are not reportable if:</p>
                    <ul>
                      <li class="smallpara">The winners are chosen without action on their part.</li>
                      <li class="smallpara">The winners are not expected to perform future services.</li>
                      <li class="smallpara">The payer transfers the prize or award to a charitable organization or governmental unit under a designation made by the recipient.</li>
                    </ul>
                    <p class="smallpara">Other items required to be reported in box 3 include the following:</p>
                    <ol>
                      <li class="smallpara">Payments as explained on page MISC-2 under Deceased employee's wages.</li>
                      <li class="smallpara">Payments as explained on page MISC-3 under Indian gaming profits, payments to tribal members.</li>
                      <li class="smallpara">A payment or series of payments made to individuals for participating in a medical research study or studies.</li>
                      <li><span class="smallpara">Termination payments to former self-employed insurance salespeople. These payments are not subject to self-employment tax and are reportable in box 3 (rather than box 7) if the following apply:
                        </span>
                        <ol>
                          <li class="smallpara">The payments are received from an insurance company because of services performed as an insurance salesperson for the company.</li>
                          <li class="smallpara">The payments are received after termination of the salesperson's agreement to perform services for the company.</li>
                          <li class="smallpara">The salesperson did not perform any services for the company after termination and before the end of the year.</li>
                          <li class="smallpara">The salesperson enters into a covenant not to compete against the company for at least 1 year after the date of termination.</li>
                          <li class="smallpara">The amount of payments depends primarily on policies sold by the salesperson or credited to the salesperson's account during the last year of the service agreement or to the extent those policies remain in force for some period after termination, or both.</li>
                          <li class="smallpara">The amount of the payments does not depend at all on length of service or overall earnings from the company (regardless of whether eligibility for payment depends on length of service).</li>
                        </ol>
                        <span class="smallpara">                        If the termination payments do not meet all these requirements, report them in box 7.</span></li>
                      <li><span class="smallpara">Generally, all punitive damages, any damages for nonphysical injuries or sickness, and any other taxable damages. Report punitive damages even if they relate to physical injury or physical sickness. Generally, report all compensatory damages for nonphysical injuries or sickness, such as employment discrimination or defamation. However, do not report damages (other than punitive damages):
                        </span>
                        <ol>
                          <li class="smallpara">Received on account of personal physical injuries or physical sickness.</li>
                          <li class="smallpara">That do not exceed the amount paid for medical care or emotional distress.</li>
                          <li class="smallpara">Received on account of nonphysical injuries (for example, emotional distress) under a written binding agreement, court decree, or mediation award in effect on or issued by September 13, 1995.</li>
                          <li class="smallpara">That are for a replacement of capital, such as damages paid to a buyer by a contractor who failed to complete construction of a building.</li>
                        </ol>
                      </li>
                    </ol>
                    <p class="smallpara">Damages received on account of emotional distress, including physical symptoms such as insomnia, headaches, and stomach disorders, are not considered received for a physical injury or physical sickness and are reportable unless described in b or c above. However, damages received on account of emotional distress due to physical injuries or physical sickness are not reportable.</p>
                    <p class="smallpara">Also report liquidated damages received under the Age Discrimination in Employment Act of 1967.</p>
                    <p class="smallpara"><strong>Foreign agricultural workers.</strong>&nbsp;Report in box 3 compensation of $600 or more paid in a calendar year to an H-2A visa agricultural worker who did not give you a valid taxpayer identification number. You must also withhold federal income tax under the backup withholding rules. For more information, go to IRS.gov and enter "foreign agricultural workers" in the search box.</p>
                    <p><br>
                    </p>
                    <div>
                      <h3>Box 4 - Federal Income Tax Withheld</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter backup withholding. For example, persons who have not furnished their TIN to you are subject to withholding on payments required to be reported in boxes 1, 2 (net of severance taxes), 3, 5 (to the extent paid in cash), 6, 7 (except fish purchases for cash), 8, 10, and 14. For more information on backup withholding, including the rate, see part N in the General Instructions for Certain Information Returns.</p>
                    <p class="smallpara">Also enter any income tax withheld from payments to members of Indian tribes from the net revenues of class II or class III gaming activities conducted or licensed by the tribes.</p>
                    <p class="smallpara"><strong>Exception:</strong>&nbsp;Regulations section 31.3406(g)-1(f) provides that backup withholding is not required for certain payment card transactions.</p>
                    <h4><br>
                    </h4>
                    <div>
                      <h3>Box 5 - Fishing Boat Proceeds</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter the individual's share of all proceeds from the sale of a catch or the FMV of a distribution in kind to each crew member of fishing boats with normally fewer than 10 crew members. A fishing boat has normally fewer than 10 crew members if the average size of the operating crew was fewer than 10 on trips during the preceding 4 calender quarters.</p>
                    <p class="smallpara">In addition, report cash payments of up to $100 per trip that are contingent on a minimum catch and are paid solely for additional duties (such as mate, engineer or cook) for which additional cash payments are traditional in the industry. However, do not report on Form 1099-MISC any wages reportable on Form W-2.</p>
                    <p><br>
                    </p>
                    <div>
                      <h3>Box 6 - Medical and Health Care Payments</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter payments of $600 or more made in the course of your trade or business to each physician or other supplier or provider of medical or health care services. Include payments made by medical and health care insurers under health, accident, and sickness insurance programs. If payment is made to a corporation, list the corporation as the recipient rather than the individual providing the services. Payments to persons providing health care services often include charges for injections, drugs, dentures and similar items. In these cases the entire payment is subject to information reporting. You are not required to report payments to pharmacies for prescription drugs.</p>
                    <p class="smallpara">The exemption from issuing Form 1099-MISC to a corporation does not apply to payments for medical or health care services provided by corporations, including professional corporations. However, you are not required to report payments made to a tax-exempt hospital or extended care facility or to a hospital or extended care facility owned and operated by the United States (or its possessions), a state, the District of Columbia, or any of their political subdivisions, agencies or instrumentalities.</p>
                    <div>
                      <h3>Box 7 - Nonemployee Compensation</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter nonemployee compensation of $600 or more. Include fees, commission, prizes and awards for services performed as a nonemployee, other forms of compensation for services performed for your trade or business by an individual who is not your employee, and fish purchases for cash. Include oil and gas payments for a working interest, whether or not services are performed. Also include expenses incurred for the use of an entertainment facility that you treat as compensation to a nonemployee. Federal executive agencies that make payments to vendors for services, including payments to corporations, must report the payments in this box.</p>
                    <p class="smallpara"><strong>What is nonemployee compensation?</strong>&nbsp;If the following four conditions are met, you must generally report a payment as nonemployment compensation.</p>
                    <ul>
                      <li class="smallpara">You made the payment to someone who is not your employee;</li>
                      <li class="smallpara">You made the payment for services in the course of your trade or business (including government agencies and nonprofit organizations);</li>
                      <li class="smallpara">You made the payment to an individual, partnership, estate, or, in some cases, a corporation; and</li>
                      <li class="smallpara">You made payments to the payee of at least $600 during the year.</li>
                    </ul>
                    <p class="smallpara"><strong>Self-employment tax.</strong>&nbsp;Generally, amounts reportable in box 7 are subject to self-employment tax. If payments to individuals are not subject to this tax and are not reportable elsewhere on Form 1099-MISC, report the payments in box 3. However, report section 530 (of the Revenue Act of 1978) worker payments in box 7.</p>
                    <p class="smallpara"><strong>Nonqualified deferred compensation (Section 409A) income.</strong>&nbsp;Include in box 7 the amount of all deferrals (plus earnings), reported in box 15b that are includible in gross income because the nonqualified deferred compensation (NQDC) plan fails to satisfy the requirements of section 409A.</p>
                    <p class="smallpara"><strong>Golden parachute payments.</strong>&nbsp;A parachute payment is any payment that meets all of the following conditions.</p>
                    <ol>
                      <li class="smallpara">The payment is in the nature of compensation.</li>
                      <li class="smallpara">The payment is to, or for the benefit of, a disqualified individual.</li>
                      <li class="smallpara">The payment is contingent on a change in the ownership of a corporation, the effective control of a corporation, or the ownership of a substantial portion of the assets of a corporation (a change of ownership or control).</li>
                      <li class="smallpara">The payment has (together with other payments described in 1, 2, and 3 above made to the same individual) an aggregate present value of at least 3 times the individual's base amount.</li>
                    </ol>
                    <p class="smallpara">A disqualified individual is one who at any time during the 12-month period prior to and ending on the date of the change in ownership or control of the corporation (the disqualified individual determination period) was an employee or independent contractor and was, in regard to that corporation, a shareholder, an officer or a highly compensated individual.</p>
                    <p class="smallpara"><strong>Independent contractor.</strong>&nbsp;Enter in box 7 the total compensation, including any golden parachute payment. For excess golden parachute payments, see box 13 reporting instructions. For employee reporting of these payments, see Pub. 15-A.</p>
                    <p class="smallpara"><strong>Payments not reported in box 7</strong>. Do not report in box 7, nor elsewhere on Form 1099-MISC, the cost of current life insurance protection (report on Form W-2 or Form 1099-R); an employee's wages, travel or auto allowance, or bonuses (report on Form W-2); or the cost of group-term life insurance paid on behalf of a former employee (report on Form W-2).</p>
                    <p><br>
                    </p>
                    <div>
                      <h3>Box 8 - Substitute Payments in Lieu of Dividends or Interest</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter aggregate payments of at least $10 received by a broker for a customer in lieu of dividends or tax-exempt interest as a result of a loan of a customer's securities. For this purpose, a customer includes an individual, trust, estate, partnership, association, company, or corporation. It does not include a tax-exempt organization, the United States, any state, the District of Columbia, a U.S. possession, or a foreign government. File Form 1099-MISC with the IRS and furnish a copy to the customer for whom you received the payment. Also, file Form 1099-MISC for and furnish a copy to an individual for whom you received a payment in lieu of tax-exempt interest.</p>
                    <p class="smallpara">Substitute payment means a payment in lieu of (a) a dividend or (b) tax-exempt interest to the extent that interest (including OID) has accrued while the securities were on loan.</p>
                    <p><br>
                    </p>
                    <div>
                      <h3>Box 9 - Payer Made Direct Sales of $5,000 or More</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter an "X" in the checkbox for sales by you of $5,000 or more of consumer products to a person on a buy-sell, deposit-commission, or other commission basis for resale (by the buyer or any other person) anywhere other than in a permanent retail establishment. Do not enter a dollar amount in this box.</p>
                    <p class="smallpara">If you are reporting an amount in box 7, you may also check box 9 on the same Form 1099-MISC.</p>
                    <p><span class="smallpara">The report you must give to the recipient for these direct sales need not be made on the official form. It may be in the form of a letter showing this information along with commissions, prizes, awards, etc.&nbsp;</span><br>
                    </p>
                    <div>
                      <h3>Box 10 - Crop Insurance Proceeds</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter crop insurance proceeds of $600 or more paid to farmers by insurance companies unless the farmer has informed the insurance company that expenses have been capitalized under section 278, 263a, or 447.</p>
                    <br>
                    <div>
                      <h3>Box 13 - Excess Golden Parachute Payments</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter any excess golden parachute payments. An excess parachute payment is the amount of the excess of any parachute payment over the base amount (the average annual compensation for services including in the individual's gross income over the most recent 5 tax years).</p>
                    <p><br>
                    </p>
                    <div>
                      <h3>Box 14 - Gross Proceeds Paid to an Attorney</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter gross proceeds of $600 or more paid to an attorney in connection with legal services (regardless of whether the services are performed for the payer).</p>
                    <div>
                      <h3>Box 15a - Section 409A Deferrals</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">You do not have to complete this box. If you complete this box, enter the total amount deferred during the year of at least $600 for the nonemployee under all nonqualified plans. The deferrals during the year include earnings on the current year and prior year deferrals. For deferrals and earnings under NQDC plans for employees, see the Instructions for Forms W-2 and W-3.</p>
                    <h4><br>
                    </h4>
                    <div>
                      <h3>Box 15b - Section 409A Income</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">Enter all amounts deferred (including any earnings on amounts deferred) that are includible in income under section 409A because the NQDC plan fails to satisfy the requirements of the section 409A. Do not include amounts properly reported on a Form 1099-MISC, Form W-2, or Form W-2c for a prior year. Also, do not include amounts that are considered to be subject to a substantial risk of forfeiture for purposes of section 409A. The amount included in box 15b is also includible in box 7.</p>
                    <div>
                      <h3>Box 16 - State Information</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">This box, and Copies 1 and 2, are provided for your convenience only and need not be completed for the IRS. Use the state information boxes to report payments for up to two states. Keep the information for each state separated by the dash line. If you withheld state income tax on this payment, you may enter it in box 16. Use Copy 1 to provide information to the state tax department. Give Copy 2 to the recipient for use in filing the recipient's state income tax return.</p>
                    <p><br>
                    </p>
                    <div>
                      <h3>Box 18 - State Information</h3>
                    </div>
                    <h4>IRS directions</h4>
                    <p class="smallpara">This box, and Copies 1 and 2, are provided for your convenience only and need not be completed for the IRS. Use the state information boxes to report payments for up to two states. Keep the information for each state separated by the dash line. If you withheld state income tax on this payment, in box 18 you may enter the amount of the state payment. Use Copy 1 to provide information to the state tax department. Give Copy 2 to the recipient for use in filing the recipient's state income tax return.</p>
                  </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<hr>
			<h4>Contact Us</h4>
			<h5>WageFilingPlus. LLC.</h5>
			<h5>Telephone: 616-325-9332</h5>
			<h5>E-mail: <a href="mailto:support@wagefilingplus.com">support@Wagefilingplus.com</a></h5>
			<h5>Trusted by</h4>
			<img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
