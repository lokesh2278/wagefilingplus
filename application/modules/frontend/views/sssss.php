<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
            <div class="row">
              <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
                <h1><a class="company-link"><span class="company-name"><?=companyName($cid)?></span></a></h1>
              </div>
            </div>
            <a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
            </div>
			</div>
      </div>
    </div>


 <div class="container">
  <div class="row">

    <div class="col-md-12">
     <div class="row">
     <center>
        <h4>WageFilingPlus.com</h4><h4>1096/W-3 style Totals Summary and Reconciliation page</h4>
        <span style="font-family: Courier New; font-size: x-small;">(Keep For Your Records - Do not file)</span>
        </center>

      </div>
      <div class="table-container" style="width:100%">
        <div class="table-responsive">
<table class="w9-table table table-striped table-hover table-responsive  table-bordered" name='example-table'>
<thead>
<tr>
 <th colspan="4">
<span><h5>Tax Year: <?php echo isset($c_details[0]->c_tax_year) ? $c_details[0]->c_tax_year : ''; ?></h5></span>
<span>File Number: &nbsp;&nbsp;  <?php echo isset($c_details[0]->c_id) ? $c_details[0]->c_id : ''; ?> </span><br>
<span>Form Type:&nbsp; <?php echo isset($c_details[0]->c_name) ? $c_details[0]->c_name : ''; ?> Form  <?php echo isset($c_details[0]->FormType) ? $c_details[0]->FormType : ''; ?></span><br>

<span>Payer:</span><br>
<?php echo isset($c_details[0]->c_name) ? $c_details[0]->c_name : ''; ?><?php echo isset($c_details[0]->c_filer_name_2) ? $c_details[0]->c_filer_name_2 : ''; ?><br>
<?php echo isset($c_details[0]->c_address) ? $c_details[0]->c_address : ''; ?> <?php echo isset($c_details[0]->c_address_2) ? $c_details[0]->c_address_2 : ''; ?> <?php echo isset($c_details[0]->c_state) ? $c_details[0]->c_state : ''; ?> <?php echo isset($c_details[0]->c_zip) ? $c_details[0]->c_zip : ''; ?><br>
<span>Phone: </span><?php echo isset($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : ''; ?> <span>E-Mail:</span> <?php echo isset($c_details[0]->c_email) ? $c_details[0]->c_email : ''; ?><br>

   </th>
  </tr>
  <tr class="float-delete-button">
   <th>box</th>
    <th>Totals</th>
  </tr>
</thead>
<tbody class="forms-container">
<?php

if (!empty($v_details)) {
    $counter = 0;
    if ($c_details[0]->FormType == '1099-Misc') {
        $Box1 = 0.00;
        $Box2 = 0.00;
        $Box3 = 0.00;
        $Box4 = 0.00;
        $Box5 = 0.00;
        $Box6 = 0.00;
        $Box7 = 0.00;
        $Box8 = 0.00;
        $Box10 = 0.00;
        $Box13 = 0.00;
        $Box14 = 0.00;
        $Box16 = 0.00;
        $Box15a = 0.00;
        $Box15b = 0.00;
        foreach ($v_details as $v_detail) {
            if ($v_detail->void != 1) {
                //echo "<PRE>";print_r($v_detail);
                $Box1 += isset($v_detail->rents) ? $v_detail->rents : 0;
                $Box2 += isset($v_detail->royalties) ? $v_detail->royalties : 0;
                $Box3 += isset($v_detail->other_income) ? $v_detail->other_income : 0;
                $Box4 += isset($v_detail->fed_income_with_held) ? $v_detail->fed_income_with_held : 0;
                $Box5 += isset($v_detail->fishing_boat_proceed) ? $v_detail->fishing_boat_proceed : 0;
                $Box6 += isset($v_detail->mad_helthcare_pmts) ? $v_detail->mad_helthcare_pmts : 0;
                $Box7 += isset($v_detail->non_emp_compansation) ? $v_detail->non_emp_compansation : 0;
                $Box8 += isset($v_detail->pmts_in_lieu) ? $v_detail->pmts_in_lieu : 0;
                $Box10 += isset($v_detail->crop_Insurance_proceeds) ? $v_detail->crop_Insurance_proceeds : 0;
                $Box13 += isset($v_detail->excess_golden_par_pmts) ? $v_detail->excess_golden_par_pmts : 0;
                $Box14 += isset($v_detail->gross_paid_to_an_attomey) ? $v_detail->gross_paid_to_an_attomey : 0;
                $Box16 += isset($v_detail->state_tax_with_held) ? $v_detail->state_tax_with_held : 0;
                $Box15a += isset($v_detail->sec_409a_deferrals) ? $v_detail->sec_409a_deferrals : 0;
                $Box15b += isset($v_detail->sec_409a_Income) ? $v_detail->sec_409a_Income : 0;
            }

        }
        ?>
        <tr><td>Box1</td><td>$<?php echo $Box1; ?></td></tr>
        <tr><td>Box2</td><td>$<?php echo $Box2; ?></td></tr>
        <tr><td>Box3</td><td>$<?php echo $Box3; ?></td></tr>
        <tr><td>Box4</td><td>$<?php echo $Box4; ?></td></tr>
        <tr><td>Box5</td><td>$<?php echo $Box5; ?></td></tr>
        <tr><td>Box6</td><td>$<?php echo $Box6; ?></td></tr>
        <tr><td>Box7</td><td>$<?php echo $Box7; ?></td></tr>
        <tr><td>Box8</td><td>$<?php echo $Box8; ?></td></tr>
        <tr><td>Box10</td><td>$<?php echo $Box10; ?></td></tr>
        <tr><td>Box13</td><td>$<?php echo $Box13; ?></td></tr>
        <tr><td>Box14</td><td>$<?php echo $Box14; ?></td></tr>
        <tr><td>Box16</td><td>$<?php echo $Box16; ?></td></tr>
        <tr><td>Box15a</td><td>$<?php echo $Box15a; ?></td></tr>
        <tr><td>Box15b</td><td>$<?php echo $Box15b; ?></td></tr>
        <?php

    } else {

        foreach ($v_details as $v_detail) {
            ?>
    <tr data-id="<?=!empty($v_detail->w2_id) ? $v_detail->w2_id : '';?>">
    <td class="company">Company</td>
    <td><?php echo ++$counter; ?></td>
  </tr>
  <?php }
    }
}?>
</tbody>
</table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
