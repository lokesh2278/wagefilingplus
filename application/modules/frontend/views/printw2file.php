<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title><?=$c_details[0]->FileName?></title> <!-- Print3m.asp -->
<style>

@media print {
	.pagebreak { display: block; page-break-before: always; }
}
.indent1  {font-Family: arial; font-size: 14px; text-align: left; margin-left:30px}
.sm {font-Family: arial; font-size: 9px; text-align: left;}
.big {font-family: ARIAL; font-size: 13px; margin-left:8; margin-top: 0; margin-bottom: 0}
.med {font-family: ARIAL; font-size: 15px; margin-left:8; margin-top:-4px;
      PADDING-BOTTOM: 0px;}
.inst {font-Family: arial; font-size: 9px; text-align: left; margin-top: 0; margin-bottom: -0; line-height:9px;}
</style>

</head>
<body>
<?php foreach ($v_details as $v_detail) {
    // print_r($v_detail);die;
    ?>
<table width=100%>
<tr>
<td width=60%>
<p class=indent1>
<br>

       <?=$c_details[0]->FileName?><br>
       <?=$c_details[0]->c_address?> <br>
       <?=$c_details[0]->c_city?>, <?=$c_details[0]->c_state?> <?=$c_details[0]->c_zip?>
<!-- Legend -->
<br><br><br><br><br><br><br>
<table border="2" cellpadding="0" cellspacing="0" bordercolor="#000000">
      <tr ><td width="100%"  align="center">
     <font face="Arial" size="3">&nbsp;Important Tax Return Document Enclosed&nbsp;&nbsp;</font>
    </td>      </tr>
</table>

		<?=$v_detail[0]->FName?> <?=$v_detail[0]->LName?><br>
		<?=$v_detail[0]->Addr1?><br>
		<?=$v_detail[0]->City?>, <?=$v_detail[0]->State?> <?=$v_detail[0]->Zip1?>
<br><br><br><br>
<!-- End of Mailing Lables -->
</p>
</td>


<!-- Instructions at the Top Right -->
<td width=40% valign=top>
<?php if ($copyprint == 1) {?>
<p style="font-Family: arial; font-size: 12px; text-align: left; margin-top: 3; margin-bottom: 3">

<b>Instructions for Recipients</b>
</p>
<p class="inst">
Account number. May show an account or other unique number the payer
assigned to distinguish your account. Amounts shown may be subject to
self-employment (SE) tax. If your net income from self-employment is
$400 or more, you must file a return and compute your SE tax on
Schedule SE (Form 1040). See Pub. 334, Tax Guide for Small Business,
for more information. If no income or social security and Medicare
taxes were withheld and you are still receiving these payments, see
Form 1040-ES, Estimated Tax for Individuals. Individuals must report
as explained for box 7 below. Corporations, fiduciaries, or partner-
ships report the amounts on the proper line of your return. For 2008:<br>
<br>
</p>
<?php } else {?>

<?php }?>
</td>
</tr>
</table>


<div class=sm>
<center>
- - - - - - - - - - - - - - - - - - - - - Fold - - - - - - - - - - - - - - - - - - - - -
</center>
</div>
<br>
<!-- 1099-Misc Forms -->


<table border="2" width="100%" cellspacing="0" bordercolor="#000000" height="1"
     style="font-family: Arial; font-size: 10px">
	<tr>
		<td width="44%" height="1" colspan="3" rowspan="3" valign="top">
      &nbsp;PAYER'S name, street address, city, state and ZIP code
<p class=big>
       <?=$c_details[0]->FileName?><br>
       <?=$c_details[0]->c_address?><br>
       <?=$c_details[0]->c_city?>, <?=$c_details[0]->c_state?> <?=$c_details[0]->c_zip?>
</p>


       </td>
		<td width="18%" height="1" valign="top">&nbsp;1 Wages, Tips, other compensation<p class=big><?php if (!empty($v_detail[0]->box1)) {echo '$' . ' ' . $v_detail[0]->box1;} else {echo " ";}?></p></td>

		<td width="18%" height="1" valign="top">2 Federal Incomtax with held<p class=big><?php if (!empty($v_detail[0]->box2)) {echo '$' . ' ' . $v_detail[0]->box2;} else {echo " ";}?></p></td>
		<td width="15%" rowspan="8" colspan="2" valign="top" align=right>
<?php if ($copyprint == 1) {?>

		<b><font size="2">Copy B</font></b><br>
		<b><font size="1">For Recipient</font></b><br>
		<br>
		This is important tax information and is being furnished to the Internal
		Revenue Service. If you are required to file a return, a negligence
		penalty or other sanction may be imposed on you if this income is
		taxable and the IRS determines that it has not been reported.

<?php } else if ($copyprint == 2) {?>
		<b><font size="2">Copy 2</font></b><br>
		<br>
		To be filed with recipient's state income tax return when required.<br><br><br><br><br><br><br><br><br>

<?php } else if ($copyprint == 3) {?>
		<b><font size="2">Copy 1</font></b><br>
		<b><font size="1">For State Tax
		Department</font></b><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<?php } else if ($copyprint == 4) {?>
		<b><font size="2">Copy C</font></b><br>
				<font size="1"><b>
				For Payer<br><br></b>
				<br>
				For Privacy Act<br>
				and Paperwork<br>
				Reduction Act<br>
				Notice, see the<br>
				<b>2017 General<br>
				Instructions for<br>
				Form 1099, 1098<br>
				3921, 3922,5498<br>
				and W-2G<br></b>

				</font>




<?php }?>
		</td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">3 Social Security Wages<p class=big><?php if (!empty($v_detail[0]->box3)) {echo '$' . ' ' . $v_detail[0]->box3;} else {echo " ";}?></p></td>
		<td width="18%" height="1" valign="top">4 social security tax with held<p class=big><?php if (!empty($v_detail[0]->box4)) {echo '$' . ' ' . $v_detail[0]->box4;} else {echo " ";}?></p></td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">5 Madicare wages and tips<p class=big><?php if (!empty($v_detail[0]->box5)) {echo '$' . ' ' . $v_detail[0]->box5;} else {echo " ";}?></p></td>
		<td width="19%" height="1" valign="top">6 Medicare wages withheld<p class=big><?php if (!empty($v_detail[0]->box6)) {echo '$' . ' ' . $v_detail[0]->box6;} else {echo " ";}?></p></td>

	</tr>
	<tr>
		<td width="22%" height="1" colspan="1" valign="top">PAYER'S Federal Tax
		ID<p class=big>	<?=$c_details[0]->c_employee_ein?> </p>
		</td>
		<td width="22%" height="1" colspan="2" valign="top">&nbsp;RECIPIENT'S
		identification No.<p class=big>
			<?php if ($ssnprint == 2) {
        $abc = $v_detail[0]->SSN;
        echo substr_replace($abc, 'XXX-XX-', 0, 5);
    } else {echo $v_detail[0]->SSN;}?>
        </p>
		</td>
		<td width="18%" height="1" valign="top">7 Social security tips<p class=big> <?php if (!empty($v_detail[0]->box7)) {echo '$' . ' ' . $v_detail[0]->box7;} else {echo " ";}?></p></td>
		<td width="19%" height="1" valign="top">8 Allocated tips<p class=big><?php if (!empty($v_detail[0]->box8)) {echo '$' . ' ' . $v_detail[0]->box8;} else {echo " ";}?></p></td>
	</tr>
	<tr>
		<td width="44%" colspan="3" rowspan="4" valign="top">
		RECIPIENT'S Name and Address<br>
		<p class=big>
		<?=$v_detail[0]->FName?> <?=$v_detail[0]->LName?><br>
		<?=$v_detail[0]->Addr1?><br>
		<?=$v_detail[0]->City?>, <?=$v_detail[0]->State?> <?=$v_detail[0]->Zip1?></p><p></p><p></p><p></p><p></p><p></p>

		</td>
		<td width="18%" height="1" valign="top">9 Advance EIC payment
		<p class=big><?php if (!empty($v_detail[0]->box9)) {echo '$' . ' ' . $v_detail[0]->box9;} else {echo " ";}?></p>
		</td>
		<td width="19%" height="1" valign="top">10 Dependent care benefits
		<p class=big><?php if (!empty($v_detail[0]->box10)) {echo '$' . ' ' . $v_detail[0]->box10;} else {echo " ";}?></p></td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">11 Nonqualified plans <?php if (!empty($v_detail[0]->box11)) {echo '$' . ' ' . $v_detail[0]->box11;} else {echo " ";}?></td>
		<td width="19%" height="1" rowspan="3" valign="top">
			12a <p class=big><?php if (!empty($v_detail[0]->box12A)) {echo '$' . ' ' . $v_detail[0]->box12A;} else {echo " ";}?> <?php if (!empty($v_detail[0]->box12AM)) {echo '$' . ' ' . $v_detail[0]->box12AM;} else {echo " ";}?></p>
			12b<p class=big><?php if (!empty($v_detail[0]->box12B)) {echo '$' . ' ' . $v_detail[0]->box12B;} else {echo " ";}?> <?php if (!empty($v_detail[0]->box12BM)) {echo '$' . ' ' . $v_detail[0]->box12BM;} else {echo " ";}?></p>
	        12c<p class=big><?php if (!empty($v_detail[0]->box12C)) {echo '$' . ' ' . $v_detail[0]->box12C;} else {echo " ";}?> <?php if (!empty($v_detail[0]->box12CM)) {echo '$' . ' ' . $v_detail[0]->box12CM;} else {echo " ";}?></p>
            12d<p class=big><?php if (!empty($v_detail[0]->box12D)) {echo '$' . ' ' . $v_detail[0]->box12D;} else {echo " ";}?> <?php if (!empty($v_detail[0]->box12DM)) {echo '$' . ' ' . $v_detail[0]->box12DM;} else {echo " ";}?></p>
		</td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">
		13 Statutary Retierment Thirs-Party<p class=big><?php if (!empty($v_detail[0]->statutory)) {echo '$' . ' ' . $v_detail[0]->statutory;} else {echo " ";}?><?php if (!empty($v_detail[0]->retirement)) {echo '$' . ' ' . $v_detail[0]->retirement;} else {echo " ";}?><?php if (!empty($v_detail[0]->thirdparty)) {echo '$' . ' ' . $v_detail[0]->thirdparty;} else {echo " ";}?></p>
		</td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">
		14 Other<p class=big><?php if (!empty($v_detail[0]->box14A)) {echo '$' . ' ' . $v_detail[0]->box14A;} else {echo " ";}?></p>
		<p class=big><?php if (!empty($v_detail[0]->box14B)) {echo '$' . ' ' . $v_detail[0]->box14B;} else {echo " ";}?></p>
		<p class=big><?php if (!empty($v_detail[0]->box14C)) {echo '$' . ' ' . $v_detail[0]->box14C;} else {echo " ";}?></p>
		<p class=big><?php if (!empty($v_detail[0]->box14D)) {echo '$' . ' ' . $v_detail[0]->box14D;} else {echo " ";}?></p>
	</td>
	</tr>
	<tr>
		<td width="17%" valign="top">15a<p class=big><?=$v_detail[0]->box15A?></p></td>
		<td width="17%" valign="top">Employe's state id number<p class=big><?=$v_detail[0]->box16?></p></td>
		<td width="14%" valign="top">16 State wages,tips etc <p class=big> <?php if (!empty($v_detail[0]->box16)) {echo '$' . ' ' . $v_detail[0]->box16;} else {echo " ";}?></p></td>
		<td width="13%" valign="top">17 State Incometax<p class=big> <?php if (!empty($v_detail[0]->box17)) {echo '$' . ' ' . $v_detail[0]->box17;} else {echo " ";}?></p></td>
		<td width="18%" valign="top">18 Local wages, tips etc<p class=big> <?php if (!empty($v_detail[0]->box18)) {echo '$' . ' ' . $v_detail[0]->box18;} else {echo " ";}?></p></td>
		<td width="19%" valign="top">19 Local Income tax<p class=big><?php if (!empty($v_detail[0]->box19)) {echo ' ' . $v_detail[0]->box19;} else {echo " ";}?></p></td>
		<td width="15%" valign="top">20 Locality<p class=big><?php if (!empty($v_detail[0]->box20)) {echo '$' . ' ' . $v_detail[0]->box20;} else {echo " ";}?></p></td>
	</tr>
	<tr>
		<td width="17%" valign="top">15b<p class=big><?=$v_detail[0]->box15B?></p></td>
		<td width="17%" valign="top"><p class=big><?=$v_detail[0]->box22?></p></td>
		<td width="14%" valign="top"><p class=big> <?php if (!empty($v_detail[0]->box23)) {echo '$' . ' ' . $v_detail[0]->box23;} else {echo " ";}?></p></td>
		<td width="13%" valign="top"><p class=big> <?php if (!empty($v_detail[0]->box24)) {echo '$' . ' ' . $v_detail[0]->box24;} else {echo " ";}?></p></td>
		<td width="18%" valign="top"><p class=big> <?php if (!empty($v_detail[0]->box25)) {echo '$' . ' ' . $v_detail[0]->box25;} else {echo " ";}?></p></td>
		<td width="19%" valign="top"><p class=big><?php if (!empty($v_detail[0]->box26)) {echo '$' . ' ' . $v_detail[0]->box26;} else {echo " ";}?></p></td>
		<td width="15%" valign="top"><p class=big><?php if (!empty($v_detail[0]->box21B)) {echo '$' . ' ' . $v_detail[0]->box21B;} else {echo " ";}?></p></td>
	</tr>
</table>


<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>

    <td width="22%"><font face="Arial" size="1">
    Form</font><font face="Arial" size="2">
      <b>W-2</b></font>
   </td>

    <td width="34%" align=center><font face="Arial" size="2">

      </font>
    </td>

    <td width="43%" align=right>
    <font face="Arial" size="1">Department of the Treasury - Internal Revenue Service</font>
    </td>
  </tr>
</table>


<!-- Instructions to Recipients -->

<br>
<?php if ($copyprint == 1) {?>
<p style="font-Family: arial; font-size: 11px; text-align: left; margin-top: 3; margin-bottom: 3">
<b>Instructions for Recipients (Cont.) </b>
</p>

<p class="inst">

<b>Box 7.</b> Shows nonemployee compensation. If you are in the trade or business
of catching fish, box 7 may show cash you received for the sale of fish. If
payments in this box are SE income, report this amount on Schedule C, C-EZ,
or F (Form 1040), and complete Schedule SE (Form 1040). You received this
form instead of Form W-2 because the payer did not consider you an employee
and did not withhold income tax or social security and Medicare tax. Contact
the payer if you believe this form is incorrect or has been issued in error.
If you believe you are an employee and cannot get this form corrected, re-
port the amount from box 7 on Form 1040, line 7 (or Form 1040 NR, line 8).
You must also complete and attach to your return Form 8919, Uncollected
Social Security and Medicare Tax on Wages.<br>

<!--/div-->

<div class="pagebreak"> </div>
<!--div id=layer2 style="top: 650; left:0; z-index: 1"-->
<?php }?>

<?php }?>
</body>
</html>

