<?php foreach ($users as $user) {?>
<div id="blaze-banner">
  <div class="container">

    <div class="navigation-container">
      <div class="wrap wrap-one-logo text-center">
        <a class="back-link" href="<?=base_url('blaze');?>" style="display: block; margin-left: 128px; "><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
        <div class="row">
          <div id="nav-company" class="col-sm-12 col-md-12 col-lg-12">
            <h1 style="font-size: 29px; "><a class="company-link"><span class="company-name">Updated Data</span></a></h1>
          </div>
          <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>
        </div>
      </div>
    </div>
    <div id="alerts-container" class="alerts-container"></div>
  </div>
</div>
<div class="content-container" id="main-view-content">
  <div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <p class="lead">Your account has been Updated! You are ready to enter your 1099s and W-2s. This account can have multiple filers or companys, each with many forms. There is no need to set up additional accounts. All your 1099s can be filed from this account.</p>
          <p class="lead">
            <strong>Your username is: <?=$user->email?></strong><br />
            <strong>Your Password is: <?=$user->password?></strong>
          </p>
          <address>
            <strong>Your Account Info:</strong><br/>
            <p class="lead"><?=$user->name?></br>
            <?=$user->phone?></br>
            <?=$user->address?></br>
            <?=$user->city?>, <?=$user->state?>, <?=$user->zip?></p>
          </address>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
              <div class="row">
                <div class="col-md-6">
                  <a class="btn btn-block btn-info" href="#">Print This Page</a>
                </div>
                <div class="col-md-6">
                  <a class="btn btn-block btn-info" href="blaze">Continue</a>
              </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php }?>