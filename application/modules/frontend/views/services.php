<div id="app-container">
    <div id="page" class="page">
       <div id="blaze-banner">
          <div class="container">
             <div class="navigation-container">
                <div class="wrap wrap-one-logo">
                   <div class="row">
                      <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
                         <h1><a class="company-link"><span class="company-name">Services </span></a></h1>
                      </div>

                   </div>
                </div>
             </div>
             <div id="alerts-container" class="alerts-container">
                <div></div>
             </div>
          </div>
       </div>
       <div class="content-container" id="main-view-content">
       	<h4 class="red">We have a full catalog of services that are dedicated to all aspects of W-2/1099 reporting.</br>
       	Contact us at any time for more information</br></h4>
       	<h5 class="red"><a href="mailto:support@WageFilingPlus.com">support@WageFilingPlus.com</a></h5>
            <section class="section-p-0">
		        <div class="container">
		        	<div class="col-sm-6">
                        <div class="feature feature-1 boxed ">
                            <div class="text-center">
                            	<img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/correct.png">
                                            <!-- <i class="ti-dashboard icon"></i> -->
                                <h4 class="red">TIN Checking/Matching
                                </h4>
                            </div>
                            <p>Submitting incorrect information on a 1099 can result in fines from the IRS of up to $260 per error.  Our Tax Identification Number Checking service can help safeguard against those fines.  For $79, you can use our service to crosscheck the IRS TIN database before you even file. Results are returned in 24 hours or less and you can process up to 10,000 files a time.
                            </p>
                        </div>

                	</div>
                	<div class="col-sm-6">
                        <div class="feature feature-1 boxed extra-padding">
                            <div class="text-center">
                                <i class="fa fa-envelope-o icon-size" aria-hidden="true"></i>
                                            <!-- <i class="ti-dashboard icon"></i> -->
                                <h4 class="red">W-2/1099 Print and Mail Services</h4>
                            </div>
                            <p>If you have a large number of forms and you prefer to have them mailed to your recipients, we can help. Our Print and Mail facility is endorsed with SOC and HIPPA certification offering top level security from start to finish.  We also offer the full series of 1099, W-2, 1040s and ACA forms.
                            </p>
                        </div>

                	</div>
                	<div class="col-sm-6">
                        <div class="feature feature-1 boxed second-padding ">
                            <div class="text-center">
                            	<img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/incorrect.png">
                            	<img class="arrow-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/rightarrow.png">
                            	<img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/correct.png">
                                            <!-- <i class="ti-dashboard icon"></i> -->
                                <h4 class="red">W-2 Form Corrections</h4>
                            </div>
                            <p>If you find out that an error has been made on a form (even if you did not originally file with us) you can correct it in minutes.
                            </p>
                        </div>

                	</div>
                	<div class="col-sm-6">
                        <div class="feature feature-1 boxed ">
                            <div class="text-center">
                            	<i class="fa fa-gavel icon-size" aria-hidden="true"></i> <!-- <img class="ic-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/hammerGavel.png">
                                            <! <i class="ti-dashboard icon"></i> -->
                                <h4 class="red">Labor Law Poster Services</h4>
                            </div>
                            <p>Take the guess work out of keeping up with State and Federal Labor Law changes. Our subscription services sends posters out to you and all your locations automatically whenever there is a change.
                            </p>
                        </div>

                	</div>
		            <!-- <div class="row">
		                <div class="">
		                    <h3>TIN Checking/Matching <img class="ic-wid" src="<?php//echo str_replace("index.php/", "", base_url()); ?>/assets/images/formcheck2.png"></h3>
		                    <p class="lead mb40">
		                        Submitting incorrect information on a 1099 can result in fines from the IRS of up to $260 per error.  Our Tax Identification Number Checking service can help safeguard against those fines.  For $79, you can use our service to crosscheck the IRS TIN database before you even file. Results are returned in 24 hours or less and you can process up to 10,000 files a time.
		                    </p> -->
		                    <!-- <div class="overflow-hidden mb32 mb-xs-24">
		                        <i class="ti-package icon icon-sm pull-left"></i>
		                        <h6 class="uppercase mb0 inline-block p32">All Inclusive Package</h6>
		                    </div>
		                    <div class="overflow-hidden mb32 mb-xs-24">
		                        <i class="ti-medall-alt icon icon-sm pull-left"></i>
		                        <h6 class="uppercase mb0 inline-block p32">Foundry Club Access</h6>
		                    </div> -->
		                <!-- </div> -->

		                <!-- <div class="col-md-4 col-sm-6">
		                    <div class="pricing-table pt-1 text-center emphasis">
		                        <h5 class="uppercase">Admit One</h5>
		                        <span class="price">$1.49</span>
		                        <p class="lead">Per Form</p>
		                        <a class="btn btn-white btn-lg" href="#">Request W-9 Form</a>
		                       <!  <p>
		                            <a href="#">Contact Us for</a>
		                            <br> large ticket volumes
		                        </p> -->
		                    <!-- </div> -->

		                <!-- </div> -->
		            <!-- </div> -->
		            <!-- <div class="row">
		                <div class="">
		                    <h3>W-2/1099 Print and Mail Services <img class="ic-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/Envelope2.png"></h3>
		                    <p class="lead mb40">
		                        If you have a large number of forms and you prefer to have them mailed to your recipients, we can help. Our Print and Mail facility is endorsed with SOC and HIPPA certification offering top level security from start to finish.  We also offer the full series of 1099, W-2, 1040s and ACA forms.
		                    </p> -->
		                    <!-- <div class="overflow-hidden mb32 mb-xs-24">
		                        <i class="ti-package icon icon-sm pull-left"></i>
		                        <h6 class="uppercase mb0 inline-block p32">All Inclusive Package</h6>
		                    </div>
		                    <div class="overflow-hidden mb32 mb-xs-24">
		                        <i class="ti-medall-alt icon icon-sm pull-left"></i>
		                        <h6 class="uppercase mb0 inline-block p32">Foundry Club Access</h6>
		                    </div> -->
		                <!-- </div> -->

		                <!-- <div class="col-md-4 col-sm-6">
		                    <div class="pricing-table pt-1 text-center emphasis">
		                        <h5 class="uppercase">Admit One</h5>
		                        <span class="price">$1.49</span>
		                        <p class="lead">Per Form</p>
		                        <a class="btn btn-white btn-lg" href="#">Request W-9 Form</a>
		                       <!  <p>
		                            <a href="#">Contact Us for</a>
		                            <br> large ticket volumes
		                        </p> -->
		                    <!-- </div> -->

		                <!-- </div> -->
		            <!-- </div> -->
		            <!-- <div class="row">
		                <div class="">
		                    <h3>W-2 Form Corrections <img class="ic-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/incorrect.png"><img class="arrow-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/rightarrow.png"><img class="ic-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/correct.png"></h3>
		                    <p class="lead mb40">
		                        If you find out that an error has been made on a form (even if you did not orignally file with us) you can correct it in minutes.
		                    </p> -->
		                    <!-- <div class="overflow-hidden mb32 mb-xs-24">
		                        <i class="ti-package icon icon-sm pull-left"></i>
		                        <h6 class="uppercase mb0 inline-block p32">All Inclusive Package</h6>
		                    </div>
		                    <div class="overflow-hidden mb32 mb-xs-24">
		                        <i class="ti-medall-alt icon icon-sm pull-left"></i>
		                        <h6 class="uppercase mb0 inline-block p32">Foundry Club Access</h6>
		                    </div> -->
		                <!-- </div> -->

		                <!-- <div class="col-md-4 col-sm-6">
		                    <div class="pricing-table pt-1 text-center emphasis">
		                        <h5 class="uppercase">Admit One</h5>
		                        <span class="price">$1.49</span>
		                        <p class="lead">Per Form</p>
		                        <a class="btn btn-white btn-lg" href="#">Request W-9 Form</a>
		                       <!  <p>
		                            <a href="#">Contact Us for</a>
		                            <br> large ticket volumes
		                        </p> -->
		                    <!-- </div> -->

		                <!-- </div> -->
		            <!-- </div>
		            <div class="row">
		                <div class="">
		                    <h3>Labor Law Poster Services <img class="ic-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/gavel-icon-3.png"></h3>
		                    <p class="lead mb40">
		                        Take the guess work out of keeping up with State and Federal Labor Law changes. Our subscription services sends posters out to you and all your locations automatically whenever there is a change.
		                    </p> -->
		                    <!-- <div class="overflow-hidden mb32 mb-xs-24">
		                        <i class="ti-package icon icon-sm pull-left"></i>
		                        <h6 class="uppercase mb0 inline-block p32">All Inclusive Package</h6>
		                    </div>
		                    <div class="overflow-hidden mb32 mb-xs-24">
		                        <i class="ti-medall-alt icon icon-sm pull-left"></i>
		                        <h6 class="uppercase mb0 inline-block p32">Foundry Club Access</h6>
		                    </div> -->
		                <!-- </div> -->

		                <!-- <div class="col-md-4 col-sm-6">
		                    <div class="pricing-table pt-1 text-center emphasis">
		                        <h5 class="uppercase">Admit One</h5>
		                        <span class="price">$1.49</span>
		                        <p class="lead">Per Form</p>
		                        <a class="btn btn-white btn-lg" href="#">Request W-9 Form</a>
		                       <!  <p>
		                            <a href="#">Contact Us for</a>
		                            <br> large ticket volumes
		                        </p> -->
		                    <!-- </div> -->

		                <!-- </div> -->
		            <!-- </div> -->

		        </div>

		    </section>
       </div>
    </div>
    <style>
    	.ic-wid{
    		width:55px;
    		margin-bottom: 16px;
    	}
    	.arrow-wid
    	{
    		width: 30px;
    	}
    	.red
      {
        color:red;
        text-align: center;
      }
      .extra-padding
      {
      	padding: 40px !important;
      }
      .second-padding
      {
      	padding: 49px !important;
      }
      .icon-size
      {
      	color:#565656 !important;
      	font-size: 64px;
      }
    </style>


