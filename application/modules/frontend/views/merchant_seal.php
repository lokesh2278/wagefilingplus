

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Authorize.Net Verified Merchant Seal</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="anetseal.css" rel="stylesheet" type="text/css">
    <style>
        body
        {
            display: none;
        }
    </style>
</head>
<form name="ANetSeal" method="post" action="./?pid=85955859-ffa5-494c-9646-aaefd48103ae&amp;rurl=https%3a%2f%2fsecure.wagefilingplus.com%2fDoFiles.asp" id="ANetSeal">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zKQKRkgjAWGZd2mJKTI++SDQuh9+sNx3leMBBw8tv1xFO3oXka/zaS/oQ9BAVwDROvj5KtzIflE+8WNj71bRfhjnbLUnpoQFjlWO0ay8r1jZhr6TWnb/JreTFyd8a758Ybz3p8EDJAvuZkE3S/pyj282IkcYNx1JLFhalhbgzje+kqBCYBOQ0o0KsCH3Vat0IH7x60wGohnzxjPl7xYrk9TTdoWd5eSWRd2dQbRS5QhBDv9lTUbdd8w9a01+vSWWx6pNi8Rpq9Kn8THwD+yBthaChm3q7OQTlj79vOLxqUviLLYl7K9FUIngi1PhgehxVz811QimpYIPEo6tUkqHotKflXKg/ro1jAdTT4UPhlMxXDHAhZOos2eGeLuWaE0AF12Mlo3tIeH60DMrHIcEKsLehd9qQx7ozCmS4532kSijAuhKfdAYtWiM4IfoF0CVkLpKfmSWSIgkK/e58x9caKtGvB5d1SAosUIK1nv9Ob2hWdGLOx6r/Lo4uvQYxvvZ1zd52wI0B6gJHAcEBtUHLYCyAYFXenLwdsX35sKMOYa8rpqhZk3OWuHiwZo6rckzV4UjKorA8bMIe1xDjYFqcYfmeAMtNkCuuw6/5daGVDpt14Lw" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['ANetSeal'];
if (!theForm) {
    theForm = document.ANetSeal;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/AnetSeal/WebResource.axd?d=IJy04JDrw-reDM4P57VEOKq-B8QvN0gp5ZI4aDWU_gwy7PxZ9kb1aio5rannxsHrjTbfawXub9m2IY3WrPyQnHo2zh41&amp;t=636480331623431523" type="text/javascript"></script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="564AE344" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="7f5mGmYKPUrC8VXGhnM6cC5JY2eXLN5wjWiKdJNeZ3JHGQKEbDUhMDlo2edm8u74T6aLcp73Ex1D9yZLEPtfhK6bv/x2Pfz8mI8zJTHJJPAZmLru34WBkirvQeHNg2aKpl96QRxm/BeQYco2qQ5S/qj5Vvg=" />
</div>
<body ms_positioning="GridLayout">
    <script>
        if (self == top) {
            var theBody = document.getElementsByTagName('body')[0];
            theBody.style.display = "block";
        } else {
            top.location = self.location;
        }
    </script>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <div id="pnlSuccess">

            <tr>
                <td class="PageHeaderFont" align="left">
                    <img src="images/SquareIcon.gif">&nbsp;&nbsp;Merchant Verified
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td>

<span class="SmalFont">May 29, 2018 13:20 PM</span>
                </td>
            </tr>
            <tr>
                <td class="MainHeaderFont">
                    <a href="https://secure.wagefilingplus.com/DoFiles.asp">
                        secure.wagefilingplus.com</a> is a verified Authorize.Net merchant.
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    Please confirm that the verified URL above matches the Web site you are visiting.
                    This seal is only valid if the URL of this window begins with <b>http://verify.authorize.net</b>
                    or <b>https://verify.authorize.net</b>.
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="AnetTableParent">
                    <table width="100%" class="AnetTable" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="DefaultSpacing" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p>
                                    You can be confident in knowing that <a href="https://secure.wagefilingplus.com/DoFiles.asp">
                                        secure.wagefilingplus.com</a> is a verified Authorize.Net merchant.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="DefaultSpacing" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p>

Authorize.Net is committed to providing its merchant customers with the highest level of transaction processing security, safeguarding customer information and combating fraud. More merchants trust Authorize.Net than any other payment gateway to process their ecommerce transactions securely.
                                    For more information about the benefits of Authorize.Net’s secure transaction processing,
                                    please visit the <a href="http://www.authorize.net" target="_blank">Authorize.Net Web
                                        site</a>.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="DefaultSpacing" colspan="2">
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td colspan="2">
                                <a id="lbtMisuse_1" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;lbtMisuse_1&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, true))">Report Seal Misuse</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="right" rowspan="2" valign="bottom">
                                <img src="images/Seal_Bottom.gif" border="0" alt="Secure Online Credit Card">
                            </td>
                        </tr>
                        <tr>
                            <td class="MainHeaderFontSmall">
                                <span class="MainHeaderFontSmall">Disclaimer: </span><span class="SmalFont">Merchant
                                    verification refers to the merchant's status as an [active] Authorize.Net customer.
                                    Authorize.Net does not guarantee, represent or warrant the performance of the merchant
                                    or that the merchant will securely or accurately process all of its transactions
                                    through the Authorize.Net Payment Gateway, or complies with applicable federal or
                                    state data protection laws.</span>
                                <br>
                                <br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

</div>






    </table>
</form>
</body>
</html>
