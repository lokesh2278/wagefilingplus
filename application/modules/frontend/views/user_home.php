<?php foreach ($users as $user) {?>
<div id="blaze-banner">
  <div class="container">

    <div class="navigation-container">
      <div class="wrap wrap-one-logo text-center">
        <a class="back-link" href="<?=base_url('blaze');?>" style="display: block; margin-left: 128px; "><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
        <div class="row">
          <div id="nav-company" class="col-sm-12 col-md-12 col-lg-12">
            <h1 style="font-size: 29px; "><a class="company-link"><span class="company-name">Welcome <?=$user->Ucompany?></span></a></h1>
          </div>
          <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>
        </div>
      </div>
    </div>
    <div id="alerts-container" class="alerts-container"></div>
  </div>
</div>
<div class="content-container" id="main-view-content">
  <div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">You are logged in as <?=$user->Ucompany?></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-bordered">

                    <tbody>
                      <tr>
                        <td>Customer ID</td>
                        <td><?=$user->CustID?></td>
                      </tr>
                      <tr>
                        <td>EMail</td>
                        <td><?=$user->UEmail?></td>
                      </tr>
                      <tr>
                        <td>Company</td>
                        <td><?=$user->Ucompany?></td>
                      </tr>
                      <!-- <tr>
                        <td>Contact</td>
                        <td><?=$user->contact?></td>
                      </tr>
                      <tr>
                        <td>Title</td>
                        <td><?=$user->title?></td>
                      </tr>
                      <tr>
                        <td>Address</td>
                        <td><?=$user->address?></td>
                      </tr>
                      <tr>
                        <td>City, State, Zip</td>
                        <td><?=$user->city?>, <?=$user->state?>, <?=$user->zip?></td>
                      </tr> -->
                      <tr>
                        <td>Phone</td>
                        <td><?=$user->Uphone?></td>
                      </tr>
                      <!-- <tr>
                        <td>Fax</td>
                        <td><?=$user->fax?></td>
                      </tr> -->
                    </tbody>

                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
              <div class="row">
                <div class="col-md-6">
                  <a class="btn btn-block btn-info" style="line-height: 30px !important;" href="blaze">Continue to Main Menu</a>
                </div>
                <div class="col-md-6">
                  <a class="btn btn-block btn-info" style="line-height: 30px !important;" href="useredit">Update Account Details</a>
              </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php }?>