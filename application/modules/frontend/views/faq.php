<style>
    .light {
        color: #cccccc;
    }
    .header-box{
        padding: 10px;
        box-shadow: 8px 8px 8px #dddddd;
        border: 1px solid #ccc;
        margin: 20px;
    }
    .dot li {
        list-style: disc;
        font-size: 14px;
    }
    .faq-list li {list-style-type: circle !important;
        margin-left: 20px;}
</style>
<div class="container">
    <div class="row header-box" style="">
                    <div class="col-md-10">
                        <h3>Frequently Asked <span class="light">Questions</span></h3>
                    </div>
                    <!-- <h5>Instantly file 1099-MISC corrections even if you did not originally file with us! </h5> -->
                    <!-- <div class="col-md-2"><a href="#"><img alt="Foundry" src="<?php echo iaBase(); ?>assets/images/print_icon.gif">HELP FILE</a></div> -->
                </div>
    <div class="row">
        <div class="col-md-12" style="padding: 40px;">
            <p class="lead">We try hard to make filing your W-2/1099-MISC forms as easy as possible and we are here to help. Many of our clients have use this section of our top frequently asked questions to help with filing.</p>
            <p class="lead">Click the other topics below to expand:<br/>
            <a href="<?php echo base_url('save-as-pdf'); ?>">Save Recipient 1099-MISC/W-2 to PDF</a></p>
            <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    Bring Files Foward From a Previous Year (Click to Expand)</a>
                  </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p class="lead">Do you want to keep the 1099's you filed last year and just change the dollar amounts, edit or delete the files without re-typing? Take a look at this quick tutorial on how to get it done quickly and easily.<p/>
                    <a href="https://www.youtube.com/embed/Hz1BiEmJfLQ?autoplay=1" rel="shadowbox;height=720;width=1280">
                        <img src="<?php echo iaBase(); ?>assets/images/bring-files-foward-ss.jpg" alt="View demo of how to file 1099-MISC and W-2 Forms  online with Wagefiling.com" width="314" height="192" class="pd10" border="0">
                    </a>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    Printing Recipient Copies</a>
                  </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="faq-list">
                        <li>At the MAIN MENU, click A Paid Print/View A in blue.</li>
                        <li>Click the A View/Print A button (you can select other copies or select specific forms).</li>
                    </ul>
                    <h4>Printing Details:</h4>
                    <p class="lead">Click the BROWSER's FILE Menu, then PRINT PREVIEW. This will show how many pages will be printed, what will be on each page. A You can also print selected pages this way. Then click PRINT in your BROWSER for the hard copies.A </p>
                    <p class="lead"><strong>Note 1:</strong> A Hard copies are designed to fold into standard No. 9 or No. 10 single or double window envelopes. Special envelopes are not required. All copies are IRS compliant 1099 and W-2 substitute forms for the recipients</p>
                    <p class="lead"><strong>Note 2:</strong> A We e-file Copy A to the IRS or SSA soon after you checked out. Your e-filing confirmation will be sent by e-Mail.</p>

                </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                    Confirmation Email and filing timeline</a>
                  </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                  <div class="panel-body">
                      <p class="lead">
                          We will e-File to the IRS or SSA during the regular season, January to April usually within 2 to 12 days after you checkout and pay for them, but they may be filed to the IRS/SSA the same day you checkout.A
                      </p>
                      <p class="lead">
                          The e-Mail confirmation is e-Mailed to you usually within 1 to 2 days after e-Filing to the IRS/SSA, but you may receive the confirmation the same day you checkout. Email is sometimes undependable, due to SPAM filters, etc. It is your responsibility to be certain you receive the confirmation, see below.A
                      </p>
                      <p class="lead">
                          For tax year , the e-Filing deadline is March 31, 1, or the next business day, if that day is a legal holiday. Files submitted after March 26, will be e-Filed as soon as possible, but we can not guarantee they will be e-Filed by the deadline. You must submit any late 1099s and W-2s as soon as possible after the deadline.Â
                      </p>
                      <p class="lead">
                          All forms e-Filed to the IRS/SSA after the deadline may be subject to a late filing penalty by the IRS. The penalty is payable by the "Filer/Company" who submitted the forms. WageFiling.com, your authorized Transmitter/Submitter, is never liable for late filing penalties, or any other penalties, under any circumstances. If you do not receive your confirmation for any reason, it is your responsibility to contact us for an emergency extension of time, or allow us sufficient time to search for, or resubmit your files before the deadline. Thanks you for using WageFiling.com, we appreciate your business.
                      </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                    Filing 1099 Corrections</a>
                  </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                  <div class="panel-body">
                      <p class="lead">
                          Before using corrections:</br>
                          If you are currently entering 1099 information and have NOT checked out, doÂ DO NOT USE THE CORRECTIONÂ checkbox. Instead, use the VOID or DELETE checkbox to remove the form. Then, if necessary, choose ADD Payee to enter the correct form. Do not read or follow this procedure. Deleted or Voided forms are never e-Filed, and you may choose not to print them.
                      </p>
                      <p class="lead">Do not use this corrections procedure if you checked out, then discover you FORGOT to enter, or LEFT OUT some 1099 forms. If a form for somebody was left out, start a new file by adding aÂ New CompanyÂ with exactly the same company information as before, then add the forms you left out, then checkout and print the forms. Do not use the corrections checkbox for forms you forgot to enter.</p>
                      <p class='lead'>Use the CORRECTIONS checkboxÂ ONLYÂ if you have already paid/checked-out, and later discover an error has been made inside one of the forms. In that case, start a New Company with the same company as before, then retype ONLY the FORMS in error. DO NOT pay for and e-file the ENTIRE GROUP again! If you bring a file forward, to avoid re-typing, pay for and e-File only those needing correcting, delete all the others that were correct.</p>
                      <p class="lead"><strong>WARNING:A</strong> You must decide if you have a Type 1 or Type 2 correction before starting. To decide, read the what Type 1 and Type 2 covers.A </p>
                      <p class="lead">Example 1: Wrong money amount, use Type 1</br>
                         Example 2: Wrong Tax ID was entered, use Type 2</br>
                         Example 3: You sent a 1099 to his company, but should have been to him individually, or vice-versa. use Type 2</br>
                         Example 4: Sent a 1099 but should have not done so: use Type 1</p>
                      <p class="lead"><strong>Correction Type 1A</strong> (coversA <strong>incorrect money amount, incorrect address, incorrect payee name or the form should not have been filed</strong>)</p>
                      <ul class="faq-list">
                        <li>A From the home page Login with your e-Mail and password..</li>
                        <li>A From the Main Menu, click NEW FILER Select 1099-Misc.</li>
                        <li>A Enter the Company/Filer information EXACTLY THE SAME as the previously submitted file.</li>
                        <li>A From the Main Menu select that company by clicking on the GREEN UNDERLINED name.</li>
                        <li>A Click ADD A PAYEE.A <strong>CHECK THE CORRECTIONS checkbox</strong>A at the top of the red form.</li>
                        <li>A Enter all information EXACTLY as on the previously submitted form, except:<br>
                        </li>
                      </ul>
                      <p class="lead"><strong>To correct a MONEY amount:</strong>A change the money to the correct amount.</br>
                         <strong>To correct an address:</strong>A change the address.</br>
                         <strong>To correct payee name:</strong>A change the payee name.</br>
                         <strong>A return was filed when one should not have been filed:</strong> A put ZERO in the money amount.</p>
                         <p class="lead">Finally, go to the Main Menu, and checkout to submit and e-File this new file. Print the corrected copy and mail it to the recipient. You are now finished. The correction will be e-Filed to the IRS.</p>
                    <p class="lead">
                        <strong>Correction Type 2</strong>A (coversA incorrect Tax ID, or both name and address were wrong)
                        <ul class="faq-list">
                        <li>From the home page Login with your e-Mail and password.</li>
                        <li>From the Main Menu, click NEW FILER Select 1099-Misc.</li>
                        <li>Enter the Company/Filer information EXACTLY THE SAME as the previously submitted file.</li>
                        <li>From the Main Menu select that company by clicking on the GREEN UNDERLINED name.</li>
                        <li>Click Add a Payee.A <strong>CHECK THE CORRECTIONS checkbox</strong>A at the top of the red form.</li>
                        <li>Enter all informationA <strong>EXACTLY</strong>A as on the previously submitted form,A <strong>including the wrong Tax ID</strong>, etc.</li>
                        <li>Enter ZERO in the Money amount, and Save this payee.</li>
                        <li>Click Add a Payee again. This timeA <strong>DO NOTA </strong>check the corrections checkbox.</li>
                        <li>Enter the correct Tax ID, Name, address, amount, etc. for this second form.</li>
                      </ul>
                    </p>
                    <p class="lead">
                        Finally, go to the Main Menu, checkout to submit and e-File this new file. Print the corrected copies and mail them to the recipient. You may wish to send the recipient a letter explaining that the first 1099 form with the correction box checked and zero in the money amount will cancel the form e-Filed previously with the wrong Tax ID. The second 1099 form, with no correction checked, is the corrected form. You are now finished. The Type 2 correction will be e-Filed to the IRS.
                    </p>
                    <p class="lead">
                        For more information about corrections please see A <a href="http://www.irs.gov/pub/irs-pdf/i1099gi.pdf">1099 General Instructions</a> A Section H on ~ page 6 titled Corrected Returns on A <strong>Paper</strong> A Forms. Although WageFiling.com automatically e-files your forms, the procedures described in Section H use the same IRS rules described above.
                    </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                    Filing W-2 Corrections</a>
                  </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse">
                  <div class="panel-body">
                      <p class="lead">
                          If you need to make W-2 corrections, please go to the Social Security Web site link shown below.
                          </br>
                          At the SSAÂ link you can:
                          </br>
                          - LOGIN (if you have your account already setup) or</br>
                          - REGISTER (if this is your first time).</br>
                          You can e-file and printÂ up to 50 W-2 corrections at one time. And best of all its FREE!</br>
                          Here is the link:</br>
                          <a href="https://www.ssa.gov/bso/bsowelcome.htm">https://www.ssa.gov/bso/bsowelcome.htm</a>
                      </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                    Official 1099 Reference Info</a>
                  </h4>
                </div>
                <div id="collapse6" class="panel-collapse collapse">
                  <div class="panel-body">
                      <p class="lead">
                          For general information about who must file, <strong>deadlines</strong>, penalties, <strong>corrections</strong>, etc. from the IRS please see <a href="http://www.irs.gov/pub/irs-pdf/i1099gi.pdf">1099 General Instructions</a>. For information about which boxes to use on IRS form 1099-Miscellaneous, see <a href="http://www.irs.gov/pub/irs-pdf/i1099msc.pdf">1099-Misc Instructions</a>.
                      </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                    Official W-2 Reference Info</a>
                  </h4>
                </div>
                <div id="collapse7" class="panel-collapse collapse">
                  <div class="panel-body">
                      <p class="lead">
                          Information about SSA Forms W-2 and W-3 please see W-2 and <a href="http://www.irs.gov/pub/irs-pdf/iw2w3_09.pdf">W-2 Instructions.</a>.
                      </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                    What are the steps to enter forms?</a>
                  </h4>
                </div>
                <div id="collapse8" class="panel-collapse collapse">
                  <div class="panel-body">
                      <ul class="faq-list">
                        <li> Set up an Account. Click <strong>START FILING</strong> on the menu bar. Follow the prompts, then <strong>LOGIN</strong>.</li>
                        <li> You are transferred to the MAIN MENU. </li>
                        <li> Click the <strong>Setup Filer</strong> button. Select the Form Type, then enter the Company/Filer Information.A </li>
                        <li> At the MAIN MENU click the <u>Filer/Company NAME</u> link (it's underlined).                   This takes you to the DISPLAY PAYEES MENU. </li>
                        <li> Click the <strong>Add a Payee</strong> button, and fill out <strong>Exact Replica Form</strong>. When done, click SAVE, and return to                   the DISPLAY PAYEES MENU. </li>
                        <li> Click <strong>Add a Payee</strong> again, to add more payee forms.</li>
                        <li> When done, click <strong>Main Menu</strong>.A </li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                    How Do I View or Modify a form entered previously?</a>
                  </h4>
                </div>
                <div id="collapse9" class="panel-collapse collapse">
                  <div class="panel-body">
                     <ul class="faq-list">
                        <li>Login and get to the MAIN MENU. Click a Filing Company (in                  red).</li>
                        <li>At the Display Payees page, click on a <u>payee's name</u> (it's BLUE).</li>
                        <li>Change the form, then click SAVE.</li>
                        <li>Click CANCEL to view the form without making changes.</li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
                    How do I delete a form entered previously?</a>
                  </h4>
                </div>
                <div id="collapse10" class="panel-collapse collapse">
                  <div class="panel-body">
                     <ul class="faq-list">
                        <li>Login or get to the MAIN MENU. Click a Filing Company (in                   red). see the DISPLAY PAYEES MENU.</li>
                        <li>At the Display Payees page, click on a <u>payee's name</u> (it's BLUE).</li>
                        <li>Check the VOID checkbox, then click SAVE. The form will be                  seen but never e-Filed.</li>
                        <li>If you are in doubt, VOID it then you can UN-VOID it later.</li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
                    How do I checkout, print and e-file the payee forms?</a>
                  </h4>
                </div>
                <div id="collapse11" class="panel-collapse collapse">
                  <div class="panel-body">
                     <ul class="faq-list">
                        <li>LoginPlease be sure to e-Mail before calling these numbers. or transfer to the MAIN MENU. One or more company/filers                should be waiting (in-red).</li>
                        <li>Select the one or more company/filer(2) by <strong>CHECKING THE SELECT box</strong>.</li>
                        <li>Click the <strong>Checkout</strong> button. </li>
                        <li>At the checkout page, read all notices, complete the credit card information, and click <strong>Charge Card</strong>. </li>
                        <li>Follow the prompts being sure to click the <strong>FINISH</strong> button. Print your credit card receipt.</li>
                        <li>Your data files will be e-Filed to the IRS or SSA. </li>
                        <li>You are returned to the MAIN MENU and <strong>your company/filer should be GONE (no red link).</strong></li>
                        <li>At the MAIN MENU, click the <strong>Print Forms</strong> button. </li>
                        <li>Click your Filer/Company shown in <strong>BLUE</strong>.</li>
                        <li>Click the <strong>View/Print</strong> button (after making your selections).A </li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">
                    How do I print my "Filer" copies?</a>
                  </h4>
                </div>
                <div id="collapse12" class="panel-collapse collapse">
                  <div class="panel-body">
                     <ul class="faq-list">
                        <li>Follow the topic above and </li>
                        <li>Select the Filer/Employer's copy.</li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">
                    How do I know the laser printed forms are approved by the IRS and SSA</a>
                  </h4>
                </div>
                <div id="collapse13" class="panel-collapse collapse">
                  <div class="panel-body">
                     <ul class="faq-list">
                        <li>Laser printed "Substitute" W-2 forms are authorized by the <a href="http://www.irs.gov/pub/irs-pdf/p1141.pdf">IRS in Publication 1141</a> at document page 11 under Section 2 <em> Requirements for Substitute Forms Furnished to Employees (Copies B, C and 2 of Form W-2)</em>. </li>
                        <li>Laser printed "Substitute" 1099 forms are approved by the <a href="http://www.irs.gov/pub/irs-pdf/p1179.pdf">IRS in Publication 1179</a> at document page 19, under Part 4 Sections 4.1 through 4.5, <em>Substitute Statements to Form Recipients and Form Recipient Copies</em>.</li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">
                    Exactly who gets what 1099 copy?</a>
                  </h4>
                </div>
                <div id="collapse14" class="panel-collapse collapse">
                  <div class="panel-body">
                     <ul class="faq-list">
                        <li>1099-Misc Copy A is from you, the filer, to the IRS. <strong>This copy is never printed.</strong> Instead, WageFiling.com e-files this copy to the IRS. Then we send you a confirmation.</li>
                        <li>1099 Form 1096 is a paper transmittal and is <strong>not required</strong> because we file Copy A electronically.A  </li>
                        <li>1099-Misc Copy 1 is from you to the state tax department, <strong> when required by the state</strong>. Send them all in one envelope to the state, when required. See <a href="https://secure.wagefiling.com/images/filingmap.jpg">state requirements No. 16</a> here. </li>
                        <li>1099-Misc Copy B is from you to the recipient for his records. The "Instructions to Recipient" are automatically printed on this copy by WageFiling.com. </li>
                        <li>1099-Misc Copy 2 is from you to the recipient, for the recipient's state tax return (when required), or as an extra copy for the recipient.</li>
                        <li>1099-Misc Copy C is your copy, for your records.A </li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse15">
                    Exactly who gets what W-2 copy?</a>
                  </h4>
                </div>
                <div id="collapse15" class="panel-collapse collapse">
                  <div class="panel-body">
                     <ul class="faq-list">
                        <li>Form W-2 Copy A is from you, the employer, to the Social Security Administration. <strong>This copy is never printed.</strong> Instead, WageFiling.com e-files this copy to the SSA. Then we send you a confirmation.</li>
                        <li>Form W-3 is a paper transmittal and is <strong>not required</strong> because we file Copy A electronically.A </li>
                        <li>Form W-2 Copy 1 is from you to the state, city, or local tax department, <strong>when required by the state</strong>. Send them all in one envelope to the state. </li>
                        <li>Form W-2 Copy B is from you to the employee for filing with the employee's federal tax return. </li>
                        <li>Form W-2 Copy C is from you to the employee for his records. The <a href="https://secure.wagefiling.com/w2Inst14.pdf">"Instructions to Recipient"</a>, must be printed on the back of this form, OR enclosed on a separate page. </li>
                        <li>Form W-2 Copy 2 is from you to the employee. Copy 2 is to be filed with the employee's state, city, or local income tax                  return, <strong>when required by the state</strong>. You should send the employee additional Copy 2's if there are multiple states, a city or local taxing jurisdiction.</li>
                        <li>Form W-2 Copy D is your copy, for the employer.A </li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse16">
                    How do I add more forms to a company after I've checked out?</a>
                  </h4>
                </div>
                <div id="collapse16" class="panel-collapse collapse">
                  <div class="panel-body">
                     <p class="lead">Get to the Main Menu. Click the New Company button. Enter the same company TaxID, Name, etc. as before. Add the next batch of forms or just one form. Then checkout. You can do as many batches for the same company as you wish, just don't duplicate any payees. This works for both 1099s and W-2s.</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse17">
                    How to create 1096 or W-2 style totals</a>
                  </h4>
                </div>
                <div id="collapse17" class="panel-collapse collapse">
                  <div class="panel-body">
                     <p class="lead">From the <strong>MAIN MENU</strong>, click click on the <strong>File Manager</strong> button. Then click <strong>View/Print</strong> Totals (after selecting the file you wish to total).</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse18">
                    Add, change or delete forms after checkout</a>
                  </h4>
                </div>
                <div id="collapse18" class="panel-collapse collapse">
                  <div class="panel-body">
                     <p class="lead">After checkout and e-Filing, you can not make changes to the data already sent. Just as if you had mailed the forms to the IRS, the only way to make changes is file something again.</p>
                     <p class="lead">To ADD more forms to a company already checked out: Create more forms under the same company, then checkout again. For more, see No. 17 above.</p>
                     <p class="lead">You can only <strong>DELETE</strong>, or <strong>CHANGE</strong> a form after checkout, <strong>by submitting a correction</strong>. To create corrections, you must carefully read Topic 14 about corrections. Then, create a new file for the same company, <strong>Add the Payee</strong> with the corrected form, then checkout. For example, <strong>to DELETE</strong> a certain form already e-Filed, file the same form again under the same company as <strong>Type 1 correction</strong>, with zero in the money amount. Do not guess about corrections. Always read Topic 14 and be certain you understand Type 1 and Type 2 corrections.</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse19">
                    Deleting entire files</a>
                  </h4>
                </div>
                <div id="collapse19" class="panel-collapse collapse">
                  <div class="panel-body">
                     <p class="lead">From the <strong>MAIN MENU</strong>, click click on the <strong>File Manager</strong> button. Then check the file to DELETE, then Click the DELETE button.
Deleting a file will remove it permanently. It can not be un-deleted. The data will be lost forever. You will be charged again to print and/or e-File the file. Do not even consider this unless you are absolutely certain.</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse20">
                    Totaling all Payees in a file</a>
                  </h4>
                </div>
                <div id="collapse20" class="panel-collapse collapse">
                  <div class="panel-body">
                     <p class="lead">From the <strong>MAIN MENU</strong>, click click on the <strong>File Manager</strong> button. Then click View/Print Total.</p>
                  </div>
                </div>
              </div>
               <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse21">
                    Common Problems and Fixes</a>
                  </h4>
                </div>
                <div id="collapse21" class="panel-collapse collapse">
                  <div class="panel-body">
                     <p class="lead">If you have a Non-Alligned, bad looking, Data Input Screen for W-2 or 1099s in WageFiling.com, it's easy to fix.
The problem is "LARGE TEXT" set in Microsoft Internet Explorer. To fix, click on the <strong>VIEW Menu</strong>, then click <strong>TEXT SIZE</strong>, then Choose <strong>MEDIUM</strong>.</p>
                     <p class="lead"><strong>BAD PRINTING</strong></br>
                        Having "LARGE TEXT" will also cause the Forms to Print poorly and unacceptably. On the 2nd and following pages, the last form will be partly on the next page. This is unacceptable. To fix, choose <strong>MEDIUM</strong> size test as shown above.</p>
                     <p class="lead"><strong>UNWANTED HEADERS AND FOOTERS</strong></br>
                        Get rid of the unwanted Headers and Footers on W-2 and/or 1099 Forms printed from your browser! Click on File/Page Setup. Select "Empty" for Headers, Footers or both, if that's what you prefer.
A
                     </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse22">
                    Definitions</a>
                  </h4>
                </div>
                <div id="collapse22" class="panel-collapse collapse">
                  <div class="panel-body">
                     <ul class="faq-list">
                        <li><strong>Filer/Company</strong> is the Employer in the case of W-2 forms, and the Payor in the case of 1099 Forms. The Filer/Company is also sometimes called the Client. Filer is the company or person who paid the money and is responsible for filing to the IRS or SSA and responsible for IRS and SSA compliance activities.</li>
                        <li>The <strong>Payee</strong> is the person, estate, trust or other entity who received the money. The payee is also sometimes called the recipient or employee.</li>
                        <li> <strong>e-Filing</strong> or electronic filing, means sending Copy a of Form 1099 or W-2 to the IRS in a formless way using electronic transfer. Transmittal Forms 1096 or W-3 are never needed when e-Filing.</li>
                      </ul>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse23">
                    Prices and checkout Procedure for Wagefiling.com</a>
                  </h4>
                </div>
                <div id="collapse23" class="panel-collapse collapse">
                  <div class="panel-body">
                     <p class="lead">Prices for 1099-Misc or W-2 forms are: Original (Normal): <strong>$3.49 per form</strong>, Previous Years: <strong>$3.99 per form</strong>, Corrections: <strong>$4.49 per form</strong></p>
                     <p class="lead">We e-File Copy A to the IRS or SSA for you. You must print the recipient copies and mail them. You may print your copies for your records. If your state requires copies, you should print and mail those copies for the states, when required. If you have questions about which copies you need to send, or questions about a particular state, or the IRS Combined Federal/State Filing program for 1099 forms, please click HELP at the main menu.</p>
                     <p class="lead">You will be able to <strong>print these forms immediately</strong> after checkout. Forms are created on blank paper, using your printer. It is not necessary to order <forms class=""></forms></p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse24">
                    Features of Wagefiling.com</a>
                  </h4>
                </div>
                <div id="collapse24" class="panel-collapse collapse">
                  <div class="panel-body">
                     <div class="row">
                         <div class="col-md-6">
                             <h4><strong>Easy online filing of W-2 and 1099 Forms</strong></h4>
                             <ul class="faq-list">
                                <li>No software to purchase or learn to use.</li>
                                <li>Try before you buy, entering forms is <strong>FREE</strong>.</li>
                                <li>If you like our service, it's <strong>just $3.49 per form.</strong></li>
                                <li>You can print forms INSTANTLY on your printer (like an airline boarding pass).</li>
                                <li>You mail them immediately, to meet a deadline or just to know it's done.</li>
                                <li>Or, we will print and mail all forms to the payees, if you choose that option.</li>
                                <li>We always e-File for you to the IRS or SSA when you check out.</li>
                              </ul>
                         </div>
                         <div class="col-md-6">
                             <h4><strong>Advantages of Wage Filing Online</strong></h4>
                             <ul class="faq-list">
                                <li>No FORMS to purchase.</li>
                                <li>No SPECIAL envelopes required. Use standard No. 10 double or single window.</li>
                                <li>We always e-file your forms to the IRS or SSA, but not until you <br>
                                  decide to make the purchase and check out.</li>
                                <li>We always provide a confirmation of the e-Filing, so you know it was done.</li>
                                <li>We will print and mail payee copy, if you select that option.</li>
                                <li>Save keystrokes, key in the Filer/Company information, just once.</li>
                              </ul>
                         </div>
                     </div>
                  </div>
                </div>
              </div>
            </div>
<p class="lead">Click these links below for more information:</p>
<div class="list-group">
  <a href="<?=base_url('form1099misc')?>" class="list-group-item">IRS Form 1099-MISC</a>
  <a href="<?=base_url('statefiling')?>" class="list-group-item">State W-2/1099-MISC Filing Deadlines</a>
  <a href="<?=base_url('combinedfederalstatefiling')?>" class="list-group-item">Combined Federal and State Filing Participants</a>
  <a href="<?=base_url('efile1099miscforms')?>" class="list-group-item">e-File 1099-MISC Forms A</a>
</div>
<hr/>
            <h4>Contact Us</h4>
            <h5>WageFilingPlus. LLC.</h5>
            <h5>Telephone: 616-325-9332</h5>
            <h5>E-mail: <a href="mailto:support@wagefilingplus.com">support@Wagefilingplus.com</a></h5>
            <h5>Trusted by</h5>
            <img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
        </div>
    </div>
</div>
