<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
            <div class="row">
              <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
                <h1><a class="company-link"><span class="company-name"><?=companyName($cid)?></span></a></h1>
              </div>
            </div>
            <a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
            </div>
            </div>
      </div>
    </div>


 <div class="container">
  <div class="row">

    <div class="col-md-12">
     <div class="row">
     <center>
        <h4>WageFilingPlus.com</h4><h4>1099/W-2 style Totals Summary and Reconciliation page</h4>
        <span style="font-family: Courier New; font-size: normal; font-weight: 600 !important;">(Keep For Your Records - Do not file, We e-file this for you)</span>
        </center>

      </div>
      <div class="table-container" style="width:100%">
        <div class="table-responsive">
<table class="w9-table table table-striped table-hover table-responsive  table-bordered" name='example-table'>
<thead>
<tr>
 <th colspan="2">
<span><h5>Tax Year: <?php echo isset($c_details[0]->TaxYear) ? $c_details[0]->TaxYear : ''; ?></h5></span>
File Number: &nbsp;&nbsp;  <span><?php echo isset($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : ''; ?> </span><br>
Form Type:&nbsp; <span><?php echo isset($c_details[0]->c_name) ? $c_details[0]->c_name : ''; ?> Form  <?php echo isset($c_details[0]->FormType) ? $c_details[0]->FormType : ''; ?></span><br>

Payer:<br>
<span><?php echo isset($c_details[0]->c_name) ? $c_details[0]->c_name . ' ' : ''; ?><?php echo isset($c_details[0]->c_filer_name_2) ? $c_details[0]->c_filer_name_2 : ''; ?><br>
<?php echo isset($c_details[0]->c_address) ? $c_details[0]->c_address : ''; ?><?php echo isset($c_details[0]->c_address_2) ? ', ' . $c_details[0]->c_address_2 : ''; ?><br>
<?php echo isset($c_details[0]->c_city) ? $c_details[0]->c_city : ''; ?>,  <?php echo isset($c_details[0]->c_state) ? $c_details[0]->c_state : ''; ?> <?php echo isset($c_details[0]->c_zip) ? $c_details[0]->c_zip : ''; ?></span><br>
Phone: <span><?php echo isset($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : ''; ?></span> E-Mail: <span><?php echo !empty($c_details[0]->c_email) ? $c_details[0]->c_email : 'Optional'; ?></span><br>

   </th>
  </tr>
  <tr class="float-delete-button" style="background-color: #337ab7; color: #ffffff;">
    <th colspan="2">TOTALS :</th>
  </tr>
</thead>

<tbody class="forms-container">
<?php

$counter = 0;
$Box1a = 0.00;
$Box1b = 0.00;
$Box2a = 0.00;
$Box2b = 0.00;
$Box2c = 0.00;
$Box2d = 0.00;
$Box3 = 0.00;
$Box4 = 0.00;
$Box5 = 0.00;
$Box6 = 0.00;

$Box8 = 0.00;
$Box9 = 0.00;
$Box10 = 0.00;
$Box11 = 0.00;

$Box13 = 0.00;
$Box14 = 0.00;

$total = array();

if (isset($v_details) && !empty($v_details)) {

    if (isset($c_details[0]->FormType) && $c_details[0]->FormType == '1099-Misc' || $c_details[0]->FormType == '1099-Div') {

        foreach ($v_details as $v_detail) {
            if ($v_detail->void != 1) {
                $total[] = 1;
                $Box1a += isset($v_detail->box1a) ? $v_detail->box1a : 0;
                $Box1b += isset($v_detail->box1b) ? $v_detail->box1b : 0;
                $Box2a += isset($v_detail->box2a) ? $v_detail->box2a : 0;
                $Box2b += isset($v_detail->box2b) ? $v_detail->box2b : 0;
                $Box2c += isset($v_detail->box2c) ? $v_detail->box2c : 0;
                $Box2d += isset($v_detail->box2d) ? $v_detail->box2d : 0;
                $Box3 += isset($v_detail->box3) ? $v_detail->box3 : 0;
                $Box4 += isset($v_detail->box4) ? $v_detail->box4 : 0;
                $Box5 += isset($v_detail->box5) ? $v_detail->box5 : 0;
                $Box6 += isset($v_detail->box6) ? $v_detail->box6 : 0;
                $Box8 += isset($v_detail->box8) ? $v_detail->box8 : 0;
                $Box9 += isset($v_detail->box9) ? $v_detail->box9 : 0;
                $Box10 += isset($v_detail->box10) ? $v_detail->box10 : 0;
                $Box11 += isset($v_detail->box11) ? $v_detail->box11 : 0;
                $Box14 += isset($v_detail->box14) ? $v_detail->box14 : 0;

            }
        }

    }
    ?>
        <tr><td>Box1a</td><td>$<?php echo $Box1a; ?></td></tr>
        <tr><td>Box1b</td><td>$<?php echo $Box1b; ?></td></tr>
        <tr><td>Box2a</td><td>$<?php echo $Box2a; ?></td></tr>
        <tr><td>Box2b</td><td>$<?php echo $Box2b; ?></td></tr>
        <tr><td>Box2c</td><td>$<?php echo $Box2c; ?></td></tr>
        <tr><td>Box2d</td><td>$<?php echo $Box2d; ?></td></tr>

        <tr><td>Box3</td><td>$<?php echo $Box3; ?></td></tr>
        <tr><td>Box4</td><td>$<?php echo $Box4; ?></td></tr>
        <tr><td>Box5</td><td>$<?php echo $Box5; ?></td></tr>
        <tr><td>Box6</td><td>$<?php echo $Box6; ?></td></tr>

        <tr><td>Box8</td><td>$<?php echo $Box8; ?></td></tr>
        <tr><td>Box9</td><td>$<?php echo $Box9; ?></td></tr>
        <tr><td>Box10</td><td>$<?php echo $Box10; ?></td></tr>
        <tr><td>Box11</td><td>$<?php echo $Box13; ?></td></tr>
        <tr><td>Box14</td><td>$<?php echo $Box14; ?></td></tr>

        <?php

}?>
</tbody>
<tfoot style="background-color: #F2F2F2; color: #333333;"><tr><td colspan="2">Total Payees: <?php echo count($total); ?></td></tr>
    <tr><td colspan="2">FileIndex=<?php echo isset($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : ''; ?></td></tr>
    <tr><td colspan="2"><a href="<?=base_url('confirmation')?>/<?=$c_details[0]->FileSequence?>">View e-Filing Confirmation</a></td></tr>
</tfoot>
</table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>