<div id="blaze-banner">
  <div class="container">
    <div class="navigation-container">
      <div class="wrap wrap-one-logo text-center">
        <a class="back-link" href="<?=base_url('blaze');?>" style="display: block; margin-left: 128px; "><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
        <div class="row">
          <div id="nav-company" class="col-sm-12 col-md-12 col-lg-12">
            <h1 style="font-size: 29px; "><a class="company-link"><span class="company-name">Filer Setup</span></a></h1>
          </div>
          <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>
        </div>
      </div>
    </div>
    <div id="alerts-container" class="alerts-container"></div>
  </div>
</div>
<div class="content-container" id="main-view-content">
  <div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">Select the Form Type for this new Filer...</h3>
            </div>
            <div class="panel-body">
              <div class="row">
              	<!-- <div class="col-md-4">
                  <center><a href="companyW9s"><img src="<?php //echo iaBase(); ?>assets/images/w9s.PNG" style="border-radius:13px;"/></a></center>
                </div> -->
                <div class="col-md-4">
                  <center><a href="company_filer"><img src="<?php echo iaBase(); ?>assets/images/1099-misc-button.jpg" /></a></center>
                </div>
                <div class="col-md-4">
                  <center><a href="compdiv1099"><img src="<?php echo iaBase(); ?>assets/images/1099 misc button 2.png" style="border-radius:13px;"/></a></center>
                </div>
                <div class="col-md-4">
                  <center><a href="employee_setup"><img src="<?php echo iaBase(); ?>assets/images/frorm-w2-button.jpg" /></a></center>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h3 style="text-align: center;">Helpful Hints:</h3>
          <!-- <div class="row">
            <div class="col-md-1"><span style="float: right;"><img src="<?php //echo iaBase(); ?>assets/images/question_button.png" width="50px" /></span></div>
            <div class="col-md-11"><p class="lead">If you are filing for 2014 and want to use 2013 info watch this video now! Click here to watch now.</p></div>
          </div> -->
          <div class="row">
            <div class="col-md-1"><span style="float: right;"><img src="<?php echo iaBase(); ?>assets/images/bullet_check.gif" width="" /></span></div>
            <div class="col-md-11"><p class="lead">The next screen sets up the new FILER. The filer is not necessarily you, the person logged in. The filer is the company, person, estate, partnership, or other entity that is legally responsible for filing the 1099 or W-2 forms.
            </p></div>
          </div>
          <div class="row">
            <div class="col-md-1"><span style="float: right;"><img src="<?php echo iaBase(); ?>assets/images/bullet_check.gif" width="" /></span></div>
            <div class="col-md-11"><p class="lead">To ADD MORE FORMS to a company previously CHECKED OUT, just enter the same company as before, same company name, tax-ID, etc. Then add the next batch of forms. See Help Topic 17.</p></div>
          </div>
          <div class="row">
            <div class="col-md-1"><span style="float: right;"><img src="<?php echo iaBase(); ?>assets/images/bullet_check.gif" width="" /></span></div>
            <div class="col-md-11"><p class="lead">You may setup more filers under your account by returning to this screen. If the filer has multiple forms types (for example: 1099-Misc and W-2 Forms) you must return here and set up the filer again for each type of form.</p></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>