<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style type="text/css">
a, a:visited {
    /*color: green !important;*/
}
p span {
  display: block !important;
}
.title-text {
  color: #1a5d8a;
}
</style>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
<!-- <a href="#" id="blaze-logos">
  <img id="w9-logo" src="<?php echo iaBase(); ?>assets/images/WageFilingw9-Logo-XSmall.png" alt="WageFiling W-9">
</a> -->
<div class="row">
  <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
    <h1><a class="company-link"><span class="company-name">WageFiling.com Checkout</span></a></h1>
  </div>

            </div>
         </div>
       </div>
     </div>
    </div>

<div class="content-container" id="main-view-content"><div>
  <div class="company-index-container"><div>
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4" style="text-align: center;">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Logged In As</h3>
            </div>
            <div class="panel-body">
              <p class="lead">
                <?=$u_details[0]->Ucompany?> <?=$u_details[0]->Uphone?> <?=$u_details[0]->Utitle?>
                <?=$u_details[0]->Uaddr?> <?=$u_details[0]->Ustate?> <?=$u_details[0]->Uzip?>
                <?=$u_details[0]->CustID?> <?=$u_details[0]->UEmail?>
              </p>
            </div>
          </div>
        </div>


  </div>
  <div class="row">
    <div class="col-md-6">
      <div style="border:  1px solid #dddddd; box-shadow:  8px 8px 8px #ddd; padding:  14px; margin: 0px;">
        <h4 class="title-text"><span><img src="<?=iaBase()?>assets/images/plus-green.gif" alt=""></span> 1099-MISC/W-2 Form Pricing for 2014 Filings</h4><br/>
        <p class="lead"><span>Current Year Forms: $3.49 per form</span>
                        <span>Previous Year Forms: $3.99 per form</span>
                        <span>1099-MISC Corrections: $4.49 per form</span>
        </p>
      </div>
    </div>
    <div class="col-md-6">
      <div style="border:  1px solid #dddddd; box-shadow:  8px 8px 8px #ddd; padding:  14px; margin: 0px;">
        <h4 class="title-text"><span><img src="<?=iaBase()?>assets/images/plus-green.gif" alt=""></span> Before You Checkout</h4><br/>
        <p class="lead">Make sure you have reviewed your data, once the files are paid for they can not be altered. If you find a mistake after checkout you will need to file an official correction.</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div style="border:  1px solid #dddddd;box-shadow:  8px 8px 8px #ddd;padding:  14px;margin: 15px 0px 15px 0px;">
      <h4 class="title-text"><span><img src="<?=iaBase()?>assets/images/plus-green.gif" alt=""></span> What Happens After Checkout</h4><br/>
      <p class="lead">Immediately after checkout you will be able to print your recipient copies on plain paper or save them as pdf. You are responsible for making sure the recipient get's their copy by the deadlines posted by the IRS and SSA to avoid penalties. We will electronically file the Federal copies and Transmittal's (1096/W-3 summary) to the IRS/SSA. You have the option to print State copies if required as well.</p>

      <p class="lead">We will file your data instantly to the IRS when you file on or before January 31st at 11pm Eastern. Your account will be automatically updated with a filing status and confirmation code from the IRS. You will also be contacted via email with filing status. Please feel free to contact us with any questions!</p>
       </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">

        <?php
foreach ($c_details as $c_detail) {
    $countmisc = misc_count($c_detail[0]->FileSequence);
    $countw2 = w2_count($c_detail[0]->FileSequence);
    if ($countmisc == '0' && $countw2 == '0') {
        echo '<div class="alert alert-warning">';
        echo 'NO PAYEES IN THE FILE: ' . $c_detail[0]->FileName . ' ' . $c_detail[0]->c_ssn_ein . '<br/>';
        echo '<b style="color: red;">CLICK BACK</b> and Un-Check this file....';
        echo '</div>';
    } else {echo '';}
}
?>
      <table class="table table-bordered">
        <thead style="background-color: #337ab7; color: #ffffff;">
          <tr>
            <th>File</th>
            <th>Tax Year</th>
            <th>Filling Company</th>
            <th>Form Type</th>
            <th>Code</th>
            <th>Count</th>
            <th>Cost Per Form</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>

<?php
$total = 0;
$filesIds = '';
foreach ($c_details as $c_detail) {

    if ($c_detail[0]->FormType == '1099-Misc') {
        $countam = misc_count($c_detail[0]->FileSequence);
        $cid = $c_detail[0]->FileSequence;
        if (!empty($c_details)) {
            $countof_uncorrected = corrected_count(isset($c_detail[0]->FileSequence) ? $c_detail[0]->FileSequence : '');
            if ($countof_uncorrected == 0) {
                $prevam = '4.99';
            } else if ($c_detail[0]->TaxYear == '2017') {

                $prevam = '3.49';
            } else {
                $prevam = '3.99';
            }
        }

    } else {

        $countam = w2_count($c_detail[0]->FileSequence);
        if ($c_detail[0]->TaxYear == '2017') {
            $prevam = '3.49';

        } else {
            $prevam = '3.99';
        }
    }
    $countam = isset($countam) ? $countam : 0;

    $total += $countam * $prevam;
    $filesIds .= isset($c_detail[0]->FileSequence) ? $c_detail[0]->FileSequence : '';
    $filesIds .= '_';
    ?>

          <tr>
            <td align="center"><?=$c_detail[0]->FileSequence;?></td>
            <td align="center" class="textSmall"><?=$c_detail[0]->TaxYear?></td>
            <td class="textSmall"><?=$c_detail[0]->FileName . ' '?><?=$c_detail[0]->c_employee_ein?></td>
            <td align="center"><?=$c_detail[0]->FormType?></td>
            <?php
if ($c_detail[0]->FormType == '1099-Misc') {
        ?>
        <td align="center"><?php

        $void = misc_void_count($c_detail[0]->FileSequence);
        $corrected = misc_corrected_count($c_detail[0]->FileSequence);
        if ($corrected > 0) {echo 'CORR';} else if ($void > 0) {echo 'VOID';} else if ($c_detail[0]->TaxYear < '2017') {echo 'Prev Year';} else {echo 'ORIG';}
        ?></td><?php
} else {
        ?>
<td align="center"><?php foreach ($v2_details as $v_detail) {
            foreach ($v_detail as $k => $v) {
                ?>
                  <?php if ($v->voidbox == 1) {echo 'VOID';} else if ($c_detail[0]->TaxYear < '2017') {echo 'Prev Year';} else {echo 'ORIG';}?>
              <?php }
        }?></td><?php
}
    ?>
            <td align="center"><div align="center"><span class="count"> <?php echo $countam; ?></span></div></td>
            <td align="center">$ <span class="form_cost"><?php echo $prevam; ?></span></td>
            <td align="center">$ <span class="total"><?php echo $countam * $prevam; ?></span></td>
          </tr>
<?php
}?>
        </tbody>
        <thead>
          <tr>
            <td colspan="6" align="right">Files can not be changed after checkout! </td>
            <td colspan="2" style="background-color: #dddddd;">Total: $<?php echo isset($total) ? $total : 0; ?></td>
          </tr>
        </thead>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="row">
        <?php
$check = 1;
// echo "<pre>";
// print_r($c_details);

foreach ($c_details as $c_detail) {
    if ($c_detail[0]->FormType == '1099-Misc') {
        $countmisc = misc_count($c_detail[0]->FileSequence);

    } else {
        $countmisc = w2_count($c_detail[0]->FileSequence);
    }

    if ($countmisc == 0) {
        $check = 0;
        break;
    }
}

// var_dump($check);

if ($check == 0) {?>
                 <div class="col-md-12" align="center">
                 <a href="javascript:window.history.go(-1);" style="color: #ffffff !important;" class="btn btn-primary btn-lg">Back</a>
                 </div>
            <?php } else {?>

        <div class="col-md-6" align="center">
        <form  action="<?php echo base_url(); ?>checkout1099" method="post">
  		<!--<script  src="https://checkout.stripe.com/v2/checkout.js" class="stripe-button"   data-key="pk_test_G5YhIkq2PEq84lwU064TZENT"  data-amount="2000"
    data-name="Demo Site" data-description="2 widgets ($20.00)" data-email="amin@ibrinfotech.com" data-image="<?php echo iaBase(); ?>/assets/images/secure90x72.gif"></script>-->
<input type="hidden" name="id" value="<?php echo $filesIds; ?>" />
<input type="hidden" name="email" value="<?=$u_details[0]->UEmail?>" />
<input type="hidden" name="customer_id" value="<?=$u_details[0]->CustID?>" />
<input type="hidden" name="company" value=" <?=$u_details[0]->Ucompany?> " />
<input type="hidden" name="amount" value="<?php echo isset($total) ? $total : 0; ?>" />
<input type="submit" name="pay" value="pay" class="btn btn-primary btn-lg btn-block"/>

</form>

          <p class="lead">Proceed to Payment Screen</p>
        </div>
        <div class="col-md-6" align="center">
          <a href="<?=base_url('blaze');?>" style="color: #ffffff !important;" class="btn btn-primary btn-lg btn-block">Cancel</a>
        </div>
        <?php }
?>


      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="row">
        <div class="col-md-4" align="center">
          <a href="" target="_blank" onclick="window.open('https://seal.godaddy.com/verifySeal?sealID=UsBeGnmQkH8N14QVtdLkbeo55kiQIV3oLTpMsiFsB20vJS4nAAM', '_blank','height=450,width=600');"><img src="<?php echo iaBase(); ?>assets/images/siteseal_gd_3_h_l_m.gif">
          <img src="<?php echo iaBase(); ?>assets/images/siteseal_gd_3_h_l_m.gif"></a>
        </div>
        <div class="col-md-4" align="center"><img src="<?php echo iaBase(); ?>assets/images/cards.GIF"></div>
        <div class="col-md-4" align="center"><a href="" target="_blank" onclick="window.open('https://verify.authorize.net/anetseal/?pid=85955859-ffa5-494c-9646-aaefd48103ae&rurl=https%3A//secure.wagefiling.com/DoFiles.asp', '_blank','height=400,width=600');"><img src="<?php echo iaBase(); ?>assets/images/secure90x72.gif"></a></div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      var sum = 0;
      // iterate through each td based on class and add the values
      $(".total").each(function() {

          var value = $(this).html();
          // add only if the value is number
          if(!isNaN(value) && value.length != 0) {
              sum += parseFloat(value);
          }
      });
      var grandTotal = sum.toFixed(2)
      $(".sub-total").html(grandTotal);
   });
</script>
