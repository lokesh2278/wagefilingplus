<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: url('../assets/images/tinmatching.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 30px;">
					<h3>e-File <span class="light">1099-MISC Forms</span></h3>
					<h5>Everything you need to know about e-Filing 1099-MISC forms on Wagefiling.com </h5>
				</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">

			<p class="lead">Efiling of your 1099-MISC forms is all part of the service included with WageFiling.  Instead of having to mail in your paper copies of the 1099-MISC Federal Copy A and 1096 transmittal to the IRS we do it electronically.</p>

            <p class="lead">Once your data is entered on the site, it's converted and submitted to the IRS within 48 hours. Your account will automatically be updated with a confirmation number for your records. All of your account data is stored securely online and accessible anytime.</p>

			<br />
			<hr>
			<h4>Contact Us</h4>
			<h5>WageFilingPlus. LLC.</h5>
			<h5>Telephone: 616-325-9332</h5>
			<h5>E-mail: <a href="mailto:support@wagefilingplus.com">support@WageFilingPlus.com</a></h5>
			<h5>Trusted by</h4>
			<img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
