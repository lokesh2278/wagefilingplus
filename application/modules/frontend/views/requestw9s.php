<div id="app-container">

  <div id="page" class="page">

    <div id="blaze-banner">

      <div class="container">

        <div class="navigation-container">

          <div class="wrap wrap-one-logo">



<div class="row">

  <div id="nav-company" class="col-md-12 col-lg-12">

    <h1><a class="company-link" href="<?=base_url('vendorw9s')?>/<?=$c_details[0]->FileSequence?>"><span class="company-name">You are now issuing W-9 Request for <?=!empty($c_details[0]->c_name) ? $c_details[0]->c_name : ''?></span></a></h1>

  </div>



</div>

<a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i>Main Menu</a>

</div></div>

      </div>

    </div>



    <div class="content-container" id="main-view-content"><div><div class="container">

  <div class="row">



<form class="bs-example bs-example-form requestForm" id="requestForm" action="<?php echo base_url() . 'payment' ?>" accept-charset="UTF-8" method="post">

    <div class="col-md-10 col-lg-10 manual-container col-md-offset-1">

      <div>

        <div class="panel panel-info panel-brdr-green">

  <div class="panel-heading panel-bg-green">

    <h2 class="panel-title">Enter the name and email address of the Contractor below </h2>

  </div>

  <div class="panel-body">

    <ul class="requests-container list-unstyled">

      <li class="panel-stretch alternating">

        <div class="form-container">

          <div class="row">

            <input type="hidden" value="<?=!empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : ''?>" name="c_id"/>

            <div class="col-sm-6" data-fields="name">

              <div class="form-group field-name">

                <label class="control-label" for="c183_name">

                   <span class="label-inner">Contractor Name</span>

                </label>

                <div data-editor="">

                  <input id="c183_name" class="form-control" name="v_name[]" maxlength="40" type="text" required>

                </div>

              </div>

            </div>

            <div class="col-sm-6" data-fields="email">

              <div class="form-group field-email">

                <label class="control-label" for="c183_email">

                  <span class="label-inner">Email</span>

                </label>

                <div data-editor="">

                  <input id="c183_email" class="form-control" name="v_email[]" type="email" required>
                  <input type="hidden" name="list_count" id="list_count" />
                </div>

              </div>

            </div>

          </div>

        </div>

      </li>

      </ul>
      <div class="row">
        <div class="col-sm-6">
           <div class="form-group field-email">
              <label class="control-label" for="c183_email">
                <span class="label-inner">Coupon</span>
              </label>
              <input id="coupon" class="form-control" name="coupon_id" type="text" >
              <input id="discount" type="hidden" name="coupon_code" >
              <span id="msg"></span>
            </div>
        </div>
      </div>

  </div>

  <div class="panel-footer">

    <button type="button" class="btn-addmore-requests btn btn-info mr-100"><i class="glyphicon glyphicon-plus"></i>Add Another</button>

      <button type="button" id="sub-btn" class="sub-btn btn btn-info">Send Requests</button>

  </div>

<style type="text/css">
  .requests-with-discount {
    position: relative !important ;
    top: -11px !important;
}
</style>

<div id="mka-payment-btn">
<?php $config = $this->load->config('config', true);
$config = $this->config;
$key = $config->config['stripe']['pk_live'];
// $key = $config->config['stripe']['pk_test'];
//echo"<pre>";print_r($my_config); ?>

      <input type="hidden" name="stripeToken" id="stripeToken">
      <script src="https://checkout.stripe.com/checkout.js"></script>

    <script>
      $('#coupon').keyup(function(){
          requestData = "coupon_id="+$('#coupon').val();
          $.ajax({
            type: "GET",
            url: '<?php echo base_url() . 'checkCoupan' ?>',
            data: requestData,
            success: function(response){
              if (response != '' && response == 100) {
                $('#msg').html("Discount added successfully");
                $('#msg').css("color", "#8DC640");
                $("#coupon").css("border", "1px solid #cccccc");
                $('#sub-btn').prop('disabled', false);
                $('.requests-with-discount').css('display','inline-block');
                $('.btn-send-requests').css('display','none');
                $('.btn-send-requests').css('margin-bottom','24px !important');
                //$("form").submit();
                //console.log(response);
                //alert(response);
              } else if(response != '' && response < 100) {
                $('#msg').html("Discount added successfully");
                $('#msg').css("color", "#8DC640");
                $("#coupon").css("border", "1px solid #cccccc");
                $('#sub-btn').prop('disabled', false);
                $('.requests-with-discount').css('display','none');
                $('.btn-send-requests').css('display','inline-block');
                // $('.btn-send-requests').css('margin-bottom','24px !important');  

                //$("form").submit();
                //console.log(response);
              } else if(!response){
                $('#msg').html("Coupon Code not applied!");
                $('#msg').css("color", "red");
                $("#coupon").css("border", "red 1px solid");
                $('.requests-with-discount').css('display','none');
                $('.btn-send-requests').css('display','inline-block');
                //$('#sub-btn').attr("disabled", "disabled");
                //console.log(response);
                //alert(response);
              }
            }
          });
        });
      
      var name = 'WageFilingPlus.com';
      var description = 'W-9 Request';
      var handler = StripeCheckout.configure({

        key: <?php echo "'$key'" ?>,

        locale: 'auto',

        name: 'WageFilingPlus.com',

        description: 'W-9 Request',

        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
        //coupon:'w9',

        //image: '<?php //echo iaBase(); ?>' +'assets/images/logoPart.jpg',
        
        token: function(token) {
          var cont_list = $('#list_count').val();
          var coupon = $('#coupon').val();
          $amount = cont_list / 100;
          var amount12 = $amount;
          //alert(amount12);
          $.ajax({
            url: '<?php echo base_url() . 'stripePost' ?>',
            type: 'POST',
            data: {stripeToken: token.id, stripeEmail: token.email, itemName: name, itemPrice: amount12, currency: 'usd', description: description, coupon:coupon},
            dataType: "json",
            
            success: function(data){
              console.log(data);
                if(data.status == 1){
                    console.log("success");

                }else {
                    console.log("fail");
                }
            },
            error: function(data) {
                console.log(data);    
                console.log("Some problem occurred, please try again.");
            }
        });
        $("form").submit();

        }

      });

    </script>

  </div>

</div>

<div id="CheckoutModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 190px;">
    <div class="modal-content">
      <div class="modal-header" style="

    padding: 15px;
    background-color: #25a9bd !important;
    color: #ffffff !important;
    border-bottom: 1px solid #e5e5e5;
    border-top-left-radius: 5px !important;
    border-top-right-radius: 5px !important;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Important Note</h3>
      </div>

      <div class="modal-body">
        <p>Once you send the forms can not be edited, please make sure your data is correct</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn-send-requests btn btn-info" style="top: none !important;">Confirm Send Requests</button>
        <button type="button" class="requests-with-discount btn btn-info" style="'margin-bottom: 24px !important; top: none !important;">Confirm Send Requests</button>
      </div>
    </div>
  </div>
</div>
</div>
</form>
</div>
  </div>
</div>

</div></div>



  </div>

  <div class="modal-container"></div>

</div>





  <script>

  $(document).ready(function () {

   $(".btn-addmore-requests").click(function(e){

    e.preventDefault();

    var lastLi = $('#requestForm').find('ul').children('li').last();

    var idVal = lastLi.find("input[type='text']" ).attr('id');

    var countField =  Number(idVal.replace(/\D/g,''))+1;

    //var nameValue = 'c'+countField+'_name';

    var key = $('li.alternating').length;

    $('.list-unstyled').append('<li class="panel-stretch alternating clearfix"><div class="form-container"><div class="row"><div class="col-sm-6" data-fields="name"><div class="form-group field-name"><label class="control-label" for="c'+countField+'_name"><span class="label-inner">Contractor Name</span></label><div data-editor=""><input id="c'+countField+'_name" class="form-control" name="v_name['+key+']" maxlength="40" type="text" required></div></div></div><div class="col-sm-6" data-fields="email"><div class="form-group field-email"><label class="control-label" for="c'+countField+'_email"><span class="label-inner">Email</span></label><div data-editor=""><input id="c'+countField+'_email" class="form-control" name="v_email['+key+']" type="email" required></div></div></div></div></div><button role="button" href="#" class="btn btn-info remove_field pull-right"><i class="glyphicon glyphicon-remove"></i> Remove This Field</button><br/></li>');

    });



  $(".list-unstyled").on("click",".remove_field", function(e){

    e.preventDefault();

    $(this).parent('li').remove();

  });





  $('.sub-btn').on('click', function() {
    var coupon_code = $("#coupon").val();
          if(coupon_code == '') {
            $('.requests-with-discount').css('display','none');
          }

      if ($('#requestForm').valid()) {

        $('#CheckoutModal').modal('show');
        requestData = "coupon_id="+$('#coupon').val();
        var coupon;
          $.ajax({
            type: "GET",
            url: '<?php echo base_url() . 'checkCoupan' ?>',
            data: requestData,
            success: function(response){

              if (response) {
                 coupon = response;
                 $('#discount').val(coupon);
                
              } else {
                $('#discount').val();
              }
              //alert(coupon);
            }
          });

      }

  });
$('.requests-with-discount').on('click',function(e){
   $("form").submit();
});


  $('.btn-send-requests').on('click', function(e) {

    $('#CheckoutModal').modal('hide');
     
   setTimeout(function() {

      $amount = 2.99 * $('li.alternating').length;
      // $amount = 1 * $('li.alternating').length;
      $amount = $amount * 100;

      $('#list_count').val($amount);
       $coupon = $('#discount').val();
      //$amount2 = $amount - ( $amount * ( 100 / 100 ) );

      handler.open({

          amount: $amount - ( $amount * ($coupon / 100 ))

      });
      
    }, 500);
   e.preventDefault();
  });

});

  </script>



  <script>

      var reader_offset = 0;    //current reader offset position

      var buffer_size = 1024;   //

      var pending_content = '';



      /**

      * Read a chunk of data from current offset.

      */

      function readAndSendChunk()

      {



        var file = $('#fileSelected')[0].files[0];



        if(file != undefined){

        var ext = file.name.split('.').pop();

        if(ext == 'csv'){

          $('.import-button').removeClass('disabled');

          $('#error-block').html( '');

          $('.error-block').hide();

          $('#filename').html(file.name);

        }else{



          $('#error-block').html( 'Please upload only "*.csv" files');

          $('.error-block').show();



          $('#filename').html('');

        }

        }else{



          $('#error-block').html( 'Please select a file');

          $('.error-block').show();

          $('#filename').html('');

        }



      }



      /**

      * Send data to server using AJAX

      */

      function sendImport()

      {

          var reader = new FileReader();

          var file = $('#fileSelected')[0].files[0];

          reader.onloadend = function(evt){



          //check for end of file

          if(evt.loaded == 0) return;



          //increse offset value

          reader_offset += evt.loaded;



          //check for only complete line

          var last_line_end = evt.target.result.lastIndexOf('\n');

          var content = pending_content + evt.target.result.substring(0, last_line_end);



          pending_content = evt.target.result.substring(last_line_end+1, evt.target.result.length);



          console.log(content);

          sendData(content);

        };

        var blob = file.slice(reader_offset, reader_offset + buffer_size);

        reader.readAsText(blob);

      }



     function sendData(content){

          var c_id= '<?=$c_details[0]->c_id?>';

          var sendData = { contents : content, c_id : c_id };

          //upload data

          $.ajax({

            url: "<?=base_url('importvendordata')?>",

            method: 'POST',

            data: sendData,

            }).done(function(response) {

            var obj = $.parseJSON(response);

            $('.import-button').removeClass('disabled');

            if(obj.status == 'fail')

            {

              $('#error-block').html( obj.message );

              $('.pending-step').show();

              $('.processing-step').hide();

              $('.error-block').show();

            }else{

              window.location.href = "<?=base_url('vendorw9s')?>/"+c_id;

            }

          //try for next chank

           });

      }

      /**

      * On page load

      */

      $(function(){

        $('.import-button').click(function(){

          $(this).addClass('disabled');

          $('.error-block').hide();

          $('.pending-step').hide();

          $('.processing-step').show();

          reader_offset = 0;

          pending_content = '';

          sendImport();

        });

        $('.hidden-inner-input').change(function(){

          readAndSendChunk();

        })

      });

    </script>