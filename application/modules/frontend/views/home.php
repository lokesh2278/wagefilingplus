<style type="text/css">
	#home-bgimage {background-image: url('<?php echo iaBase(); ?>assets/images/homepage_hero.jpg');}
</style>
<div class="main-container home-page">
	<section class="section pt0 pb0 scroll-watch light overlay image-bg" id="sw9s">
	<div class="homepage-hero" id="home-bgimage">
	<div class="container v-align-transform">
		  <div class="row text-center abs-center">
			<div class="col-md-10 col-md-offset-1">
			  <h2 class="mb-xs-16">Request, Receive and Report <br class="hidden-sm"> W-9's from anywhere to anyone for free  </h2>
			  <h1 class="hide">Request, Receive and Report <br class="hidden-sm"> W-9's from anywhere to anyone for free  </h1>
			  <a class="btn btn-lg cust-color" href="<?=base_url('signup')?>">Create Account</a>
			  <a class="btn btn-lg btn-filled cust-color" href="<?=base_url('login')?>">Returning User</a>
			</div>
		  </div>
		</div>
	</div>
  </section>
  <section class="section intro scroll-watch light blue-section" id="sw9s">
	<div class="container">
	  <div class="row rev-col">
		<div class="col-md-8">
			<h4>Instantly request, receive and report W-9 info for your Contractors without printing and mailing W-9 forms</h4>
			<div class="row mt-40">
				<div class="col-md-4">
				  <div class="text-center">
					<img src="<?php echo iaBase() . 'assets/images/1099-misc-icon.png' ?>" alt="Image">
					<h5 class="mp">Send the W-9 Request</h5>
				  </div>
				</div>
				<div class="col-md-4">
				  <div class="text-center">
					  <img src="<?php echo iaBase() . 'assets/images/step2new.png' ?>" alt="Image">
					<!-- <div class="image-div">
					</div> -->
					<h5 class="mp">Contractor receives secure link to complete</h5>
				  </div>
				</div>
				<div class="col-md-4">
				  <div class="text-center">
					  <img src="<?php echo iaBase() . 'assets/images/cloudPdf.png' ?>" alt="Image">
					<!-- <div class="image-div">
					</div> -->
					<h5 class="mp">W-9 saved to your account</h5>
				  </div>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </section>
  <section class="section w9s scroll-watch" id="sw9s">
	<div class="container">
	  <div class="row">
		<div class="col-sm-5 col-md-5 mt-40">
		  <h3>Request W-9's with ease</h3>
		  <p>No more mailing paper W-9's and waiting for them to come back. Email 1 or 1,000 contractors a W-9 request instantly from any smart device or computer.</p>
		  <!-- <p class="loopy-line"><a class="loopy-button back-left green" href="<?=base_url('signup')?>">Start 14-Day Trial</a><a class="loopy-button" href="#">Learn More</a></p>
		  <p></p> -->
		</div>
		<div class="col-sm-7 col-md-7 ">
			<img src="<?php echo iaBase(); ?>assets/images/step_1.png" class="img-responsive img-issuing-form" alt="in office sign up">
		</div>
	  </div>
	</div>
  </section>
  <section class="section w9s scroll-watch" id="sw9s">
	<div class="container">
	  <div class="row">
		<div class="col-sm-6 col-md-7">
		  <img src="<?php echo iaBase(); ?>assets/images/step_2_v2.png" class="img-responsive img-issuing-form" alt="in office sign up">
		</div>
		<div class="col-sm-6 col-md-5  mt-40">
			<h3>Secure and Simple</h3>
		 	<p>Contractor receives an email with a secure link to enter their W-9 info, e-sign and save.  The entire process is secured with SHA-2 and 2048-bit SSL encryption from start to finish</p>
		</div>
	  </div>
	</div>
  </section>
  <section class="section w9s scroll-watch" id="sw9s">
	<div class="container">
	  <div class="row">
		<div class="col-sm-5 col-md-5 mt-40">
		  <h3>All your W-9's saved in one location</h3>
		  <p>Once the contractor signs and saves, their W-9 is instantly added to your account to view, print or import to you accounting software at anytime. You can also view status of requests and resend at no charge.</p>
		</div>
		<div class="col-sm-7 col-md-7 ">
			<img src="<?php echo iaBase(); ?>assets/images/step_3.png" class="img-responsive img-issuing-form" alt="in office sign up">
		</div>
	  </div>
	</div>
  </section>
  <section class="section w9s scroll-watch" id="sw9s">
	<div class="container">
	  <div class="row">
		<div class="col-sm-6 col-md-7">
		  <img src="<?php echo iaBase(); ?>assets/images/step_4.jpg" class="img-responsive img-issuing-form" alt="in office sign up">
		</div>
		<div class="col-sm-6 col-md-5  mt-40">
			<h3>Be prepared for tax time</h3>
		 	<p>Come tax season, we can create from 1099-MISC for your contractor, email them an IRS approved Recipient copy and e-file the data to the IRS for you.</p>
		</div>
	  </div>
	</div>
  </section>
</div>