<style type="text/css">
  .require {  /* Marker for required fields */
   color: red;
}
</style>
<div id="main" class="main container">
        <div id="right" class="right-side">

<div class="sharons-pretty-form">
<form class="new_user" id="new_user" action="" accept-charset="UTF-8" method="post">
  <div id="user-form">
   <div class="error" style="margin-bottom:10px;"> <?php echo !empty($this->session->get_userdata()['alert_msg']['msg']) ? $this->session->get_userdata()['alert_msg']['msg'] : ''; ?> </div>
   <?php if(!empty($this->session->set_userdata)){ ?>
   <?php $this->session->set_userdata('alert_msg', array('msg' => ''));?>
   <? } ?>
    <div>
  <div class="row" style="text-align: center;">
    <h2>Create Account</h2>
    <p class="lead">One Account Handles Unlimited Clients</p>
  </div>
  <div class="row">
    <div class="col-sm-12" data-fields="full_name"><div class="form-group field-full_name">
      <label class="control-label" for="full_name"><span class="label-inner">Full Name</span><span class="require">*</span></label>              <div data-editor="">
      <input id="full_name" class="form-control required" name="full_name" type="text" >
       </div>
      </div>
     </div>
   </div>
  <div class="row">
    <div class="col-sm-6" data-fields="email"><div class="form-group field-email">
      <label class="control-label" for="email"><span class="label-inner">Email</span><span class="require">*</span></label>              <div data-editor="">
      <input id="email" class="form-control required" name="email" type="email" >
       </div>
      </div>
     </div>
    <div class="col-sm-6" data-fields="password"><div class="form-group field-password">      <label class="control-label" for="c-email"><span class="label-inner">Confirm Email</span><span class="require">*</span></label>              <div data-editor="">
      <input id="cemail" class="form-control required" name="cemail" type="email" />
      </div>
      </div>
     </div>

   </div>
   <div class="row">
    <div class="col-sm-12" data-fields="email"><div class="form-group field-email">
      <label class="control-label" for="email"><span class="label-inner">Company</span><span class="require">*</span></label>              <div data-editor="">
      <input id="company" class="form-control required" name="company" type="text" >
       </div>
      </div>
     </div>
    <!-- <div class="col-sm-6" data-fields="contact"><div class="form-group field-password">      <label class="control-label" for="contact"><span class="label-inner">Contact</span></label>              <div data-editor="">
      <input id="contact" class="form-control required" name="contact" type="text" >
      </div>
      </div>
     </div> -->

   </div>
   <!-- <div class="row">
    <div class="col-sm-6" data-fields="title"><div class="form-group field-email">
      <label class="control-label" for="title"><span class="label-inner">Title</span></label>              <div data-editor="">
      <input id="title" class="form-control required" name="title" type="text" >
       </div>
      </div>
     </div>
    <div class="col-sm-6" data-fields="address"><div class="form-group field-password">      <label class="control-label" for="address"><span class="label-inner">Address</span></label>              <div data-editor="">
      <input id="address" class="form-control required" name="address" type="text" >
      </div>
      </div>
     </div>

   </div> -->
   <!-- <div class="row">
    <div class="col-sm-6" data-fields="city"><div class="form-group field-email">
      <label class="control-label" for="city"><span class="label-inner">City</span></label>              <div data-editor="">
      <input id="city" class="form-control required" name="city" type="text" >
       </div>
      </div>
     </div>
    <div class="col-sm-6" data-fields="state"><div class="form-group field-password">      <label class="control-label" for="state"><span class="label-inner">State</span></label>              <div data-editor="">
      <input id="state" class="form-control required" name="state" type="text" >
      </div>
      </div>
     </div>

   </div> -->

  <div class="row">
    <!-- <div class="col-sm-6" data-fields="zip"><div class="form-group field-phone_number">      <label class="control-label" for="zip"><span class="label-inner">Zip</span></label>              <div data-editor="">
       <input id="zip" class="form-control required" name="zip" type="text" >
       </div>            <p class="help-block" data-error=""></p>      <p class="help-block"></p>
      </div>
     </div> -->
    <div class="col-sm-12" data-fields="phone_number"><div class="form-group field-phone_number">      <label class="control-label" for="phone_number"><span class="label-inner">Phone</span><span class="require">*</span></label>              <div data-editor="">
       <input id="phone_number" class="form-control required" name="phone_number" type="number" >
       </div>            <p class="help-block" data-error=""></p>      <p class="help-block"></p>
      </div>
     </div>

   </div>
   <!-- <div class="row">
    <div class="col-sm-6" data-fields="fax"><div class="form-group field-phone_number">      <label class="control-label" for="fax"><span class="label-inner">Fax</span></label>              <div data-editor="">
       <input id="fax" class="form-control required" name="fax" type="tel" >
       </div>            <p class="help-block" data-error=""></p>      <p class="help-block"></p>
      </div>
     </div>

   </div> -->
   <div class="row">
    <div class="col-sm-6" data-fields="password"><div class="form-group field-password">      <label class="control-label" for="password"><span class="label-inner">Password</span><span class="require">*</span></label>              <div data-editor="">
       <input id="password" class="form-control required" name="password" type="password" >
       </div>            <p class="help-block" data-error=""></p>      <p class="help-block"></p>
      </div>
     </div>
    <div class="col-sm-6" data-fields="cpassword"><div class="form-group field-cpassword">      <label class="control-label" for="cpassword"><span class="label-inner">Confirm Password</span><span class="require">*</span></label>              <div data-editor="">
       <input id="cpassword" name="cpassword" class="form-control required" type="password" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Please enter the same Password as above' : '');" placeholder="Verify Password" >
       </div>            <p class="help-block" data-error=""></p>      <p class="help-block"></p>
      </div>
     </div>
   </div>
   <div class="row">
    <div class="col-sm-12" data-fields="accept">
      <div class="checkbox solo">
    <label for="accept" class="accept">
      <span data-editor="">
    <div style="float:right;"> I have read and accept the <a href="<?php echo base_url('terms'); ?>" target="_blank">terms and conditions</a>
    </div>
     <input id="accept" name="accept" type="checkbox" style="float:left;">
   </label>
    </span>
   </div>
 </div></label></div>
   <div class="row">
     <div class="col-md-6 col-md-offset-3">
        <center><p class="lead"><span class="require">*</span>All fields are required</p></center>
        <button type="submit" id="abc" class="btn btn-primary btn-lg btn-block" >CREATE ACCOUNT</button>
        <p>Already have an account? <a href="<?php echo base_url('login'); ?>">Sign in</a></p>
        </div>
   </div>
 </div>
 </div>
</form>
 </div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4 class="panel-title">ONE ACCOUNT HANDLES ALL CLIENTS</h4>
      </div>
      <div class="panel-body">
        <p>Setting up your account is free. You, the ACCOUNT HOLDER are not necessarily the FILER/COMPANY/EMPLOYER. For example, the Account Holder might be a bookkeeper, or CPA firm. Account information never prints on the 1099 or W-2 forms.</p>
        <p>Using this account, the Account Holder may enter unlimited CLIENTS/FILERS/COMPANY/EMPLOYERS, each of whom need to file W-2 and/or 1099 Forms for their employees and/or payees. This account and all personal information in it, may only be accessed by you, the Account Holder. You must use caution and never disclose your password to anyone, as personal and private information must not be disclosed. See our site's e-Security link below.</p>
        <p>You are almost ready to enter forms and see how easy it is to get started. With this account  you can enter unlimited FILER/COMPANIES/EMPLOYERS and each can have UNLIMITED payees and/or employees. </p>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script>
  $(document).ready(function () {
       //event.preventDefault();
      //$("#new_user").submit(function(e){
                //e.preventDefault();
        $('#new_user').validate({ // initialize the plugin
        rules: {
            full_name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
                // minlength: 7,
                // maxlength: 15,
            },
            cemail: {
              equalTo: '[name="email"]',
            },
            phone_number: {
                required: true,
                digits : true,
                minlength: 10,
                maxlength: 10,
            },
            contact: {
                required: true,
            },
            password: {
                required: true,
                minlength: 3,
                maxlength: 18,

            },
            cpassword: {
                    equalTo: "#password",
            },
             company: {
                required: true,
            },
            accept: {
              required: true,
            }
        },
      messages: {
             full_name: {
                required: 'Required',
            },
            email: {
                required: 'Required',
                email: 'Invalid email address',
                // minlength: 'Must be 7 characters',
                // maxlength: 'Must be 15 characters',
            },
             cemail: "Confirm email must be Same as email",
            phone_number: {
                required: 'Required',
                digits : 'Must be 10 digits',
                minlength: 'Must be 10 digits',
                maxlength: 'Must be 10 digits',
            },
            password: {
                required: 'Required',
                minlength:'Minimum 3 characters',
                maxlength: 'Must be 18 characters',
            },
            cpassword: " Enter Confirm Password Same as Password",
             company: {
                required: 'Required',
            },
            accept: {
                required: 'You must accept terms & condition',
            }
       }
    });

});

     // });

  </script>