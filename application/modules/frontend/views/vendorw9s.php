<div id="app-container">

  <div id="page" class="page">

    <div id="blaze-banner">

      <div class="container">

        <div class="navigation-container">

          <div class="wrap wrap-one-logo">

<div class="row">

  <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">

    <h1><a class="company-link"><span class="company-name"><?=companyName($cid)?></span></a></h1>

  </div>

  <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"><div><form role="form" id="search-form">

  <label class="control-label sr-only" for="blaze-q">Type to search</label>

  <div class="search-form-group input-group">

    <input placeholder="Type to search" type="search" class="form-control" id="blaze-q" name="q">

    <span class="input-group-addon clear-button">

      <span style="display: block;" class="search-search glyphicon glyphicon-search form-control-feedback"></span>

      <span style="display:none" class="search-wait glyphicon glyphicon-refresh form-control-feedback"></span>

      <span style="display:none" class="search-found glyphicon glyphicon-ok form-control-feedback"></span>

      <span style="display:none" class="search-fail glyphicon glyphicon-remove form-control-feedback"></span>

    </span>

  </div>

</form>

</div></div>

</div>

<a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>

</div></div>

        <div id="alerts-container" class="alerts-container"></div>

      </div>

    </div>



    <div class="content-container" id="main-view-content"><div><div class="container">

  <div class="row">

    <div class="buttons-container col-md-3 col-md-push-9"><div><div class="sidebar">

  <div class="pagination-container pull-right unpull-md unpull-lg"><div>

</div></div>

  <div class="btn-group btn-group-shift-md" style="margin-bottom: 0px !important;">
<?php $req_cid = get_request_id($cid);
$paid_status = get_paid_status($req_cid);
if ($paid_status == '1') {
    ?>
    <button type="button" class="btn btn-default btn-hot" data-toggle="modal" data-target="#checkout-modal">
      <i class="glyphicon glyphicon-plus"></i> Add Recipient
    </button>
<?php } else {?>
    <a  class="btn btn-default btn-hot dropdown-toggle" href="<?=base_url('requestw9s')?>/<?=$cid?>"><i class="glyphicon glyphicon-plus"></i> Add Recipient </b></a>
<?php }?>
  </div>

  <div class="btn-group btn-group-shift-md">

    <button type="button" class="resend-all btn btn-default btn-hot">Resend Requests</button>

    <button type="button" class="dwn-pdfs btn btn-default btn-hot">Move</button>

    <!-- <button type="button" class="dwn-pdfs btn btn-default btn-hot">Download Files</button> -->

  </div>

</div>

</div></div>

    <div class="col-md-9 col-md-pull-3">

      <div class="table-container" style="width:100%">

        <div class="table-responsive">

        <table class="w9-table table table-striped table-hover table-responsive DeletableTable table-bordered" name='example-table'>

<thead>

  <tr class="float-delete-button">

    <th width="1" class=""  data-tsort="disabled" data-tfilter="disabled"><button type="button" class="btn btn-xs mark-all-toggle" title="Select all" style="color: #757373!important;margin-bottom: 0;"><span class="mark-box"><i class="material-icons md-18 md-icon-done"></i></span></button></th>

    <th class="sort "  data-tsort-type="string" style="white-space:nowrap"> Contractor Name</th>

    <th class="company" data-tsort="disabled" data-tfilter="disabled">Company</th>

    <th class="sort"  data-tsort-type="string">Form&nbsp;&nbsp;</th>

    <th data-tsort="disabled" data-tfilter="disabled">Create 1099-MISC</th>

    <th class="sort"  data-tsort-type="string">Status&nbsp;&nbsp;</th>

    <th class="sort"  data-tsort-type="date" >Signed&nbsp;&nbsp;</th>

    <th data-tsort="disabled" data-tfilter="disabled">W-9</th>

    <th data-tsort="disabled" data-tfilter="disabled">Move To 1099-Misc</th>

  </tr>

</thead>

<tbody class="forms-container">



<?php

if (!empty($v_details)) {

    foreach ($v_details as $v_detail) {
        ?>



        <tr data-id="<?=!empty($v_detail->v_id) ? $v_detail->v_id : '';?>">



        <?php if (isset($v_detail->v_status) && $v_detail->v_status == 'Requested') {?>

        <td class="mark-box-container">

        <button class="mark-box" type="button"><i class="material-icons md-18 md-icon-done"></i></button>

        </td>

        <?php } else {echo '<td></td>';}?>

        <td class="name"><a class="edit-link" href="<?=base_url('editw9s')?>/<?=$v_detail->v_id?>"><?=!empty($v_detail->v_name) ? $v_detail->v_name : '';?></a></td>

        <td class="company">Company</td>

        <td>W-9</td>

        <td>

        <?php if (!empty($v_detail->business_classification) && $v_detail->v_status != 'Requested') {

            ?>

        <?php

            $classifications = explode('|', $v_detail->business_classification);

            foreach ($classifications as $cla) {?>

        <input class="set1099able" type="checkbox" value="false" checked="checked"> <label> <?=$cla?></label><br><?php }}?></td>

        <td><?=!empty($v_detail->v_status) ? str_replace('Requested', 'Pending', $v_detail->v_status) : '';?></td>

        <td><?=!empty($v_detail->created_at) && $v_detail->v_status != 'Requested' ? date('d/m/Y', strtotime($v_detail->created_at)) : '--';?></td>





    <?php if (!empty($v_detail->pdf)) {?>

        <td><a class="pdf-link" href="<?=$v_detail->pdf?>" download><i class="icon-download-alt"></i> PDF </a>
         </td>



    <?php } else {?>

        <td></td>

        <?php }?>

        <td align="center">
          <?php 
          $vendor = $this->Frontend_model->getData('ia_vendors', $id, 'v_id');
          $cid = $vendor[0]->c_id;
          $companydata = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
          $checkpaid = compPaidOrNot($companydata[0]->requested_id);
          if($checkpaid == 0){
          if(isset($v_detail->v_status) && $v_detail->v_status == 'Signed' && $v_detail->move_status == Null){ ?>
            <!-- <a href="<?=base_url('move_to_1099_misc')?>/<?=$v_detail->v_id?>">Move</a> -->
            Move&nbsp;<input type="checkbox" name="formPdf[]" value="<?php echo $v_detail->v_id; ?>">
          <?php 
           } else {
               if(isset($v_detail->v_status) && $v_detail->v_status != 'Signed') {
               
                echo '---';
               } else { ?>
                <h6>Moved</h6>
            <?php   }
           }
         } else {
             echo 'Company Paid';
         }
          ?>
        </td>

        </tr>

  <?php }

} else {?>

<tr>

<td colspan="8">Nothing to show</td>

</tr>

<?php }?>

</tbody>

</table>

</div>

</div>

    </div>

  </div>

</div>

</div></div>



     </div>

  <div class="modal-container"></div>
  <!-- Modal -->
    <div class="modal fade" id="checkout-modal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      <div class="modal-dialog" style="width: 650px !important;">
        <div class="modal-content">
          <div class="modal-header" style="padding: 15px; background-color: #25a9bd !important; color: #ffffff !important; border-bottom: 1px solid #e5e5e5; border-top-left-radius: 5px !important; border-top-right-radius: 5px !important;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Important Notice</h4>
          </div>
          <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="alert alert-danger">
                      <strong>NOTE: </strong>This file has been closed out becouse this is paid,You have to attempting to request a W-9, you will need to create a New Company and the W-9 will be added there.
                    </div>

                  </div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="" style="background-color: #00A6B5; width: 150px; height: 40px; border: none; color: white; margin:0px 6px 0px 0px; padding: 5px; font-size: 16px; cursor: pointer;" data-dismiss="modal">Close</button><a href="<?=base_url('company');?>" type="button" class="" style="background-color: #00A6B5; width: 150px; height: 40px; border: none; color: white !important; margin:0px 6px 0px 0px; padding: 9px 30px 9px 30px !important; font-size: 16px; font-weight: 400 !important; cursor: pointer;">Add New Filer</a>
          </div>
        </div>

      </div>
    </div>
</div>

<form action="<?php echo base_url() . 'frontend/moveto1099_misc'; ?>" method="post" id="tempdfform"></form>

<script>
    $(document).ready(function() {
        $('table[name=example-table]').tableFilter({

     sort : true,

          callback : function() {},

          notFoundElement : ".not-found"

        });



         $('.download-csv').click(function(){

         $(this).attr('disabled','disabled');

         $('#alerts-container').html('');

           var dataNew = { type :'csv',c_id : '<?=$cid?>' };

           var baseUrl = '<?php echo base_url('vendordownload') ?>';

           $.ajax({

              url: baseUrl,

              type: "POST",

              data: dataNew,

              success: function(data) {

                var obj = $.parseJSON(data);

                $('.download-csv').removeAttr('disabled');

                if(obj.status == 'success')

                {

                  var url = '<?=iaBase()?>'+obj.path;

                  var a = document.createElement("a");

                  a.style = "display: none";

                  a.href = url;

                  a.download = obj.name;

                  document.body.appendChild(a);

                  a.click();

                  window.URL.revokeObjectURL(url);

                }else{

                  notiCustom('No Data Found', 'warning');

                }

              }

            });

        })



        $('.download-forms-individual').click(function(){

        $(this).attr('disabled','disabled');

        $('#alerts-container').html('');

           var dataNew = { type :'spdf',c_id : '<?=$cid?>' };

           var baseUrl = '<?php echo base_url('vendordownload') ?>';

           $.ajax({

              url: baseUrl,

              type: "POST",

              data: dataNew,

              success: function(data) {

                var obj = $.parseJSON(data);

                $('.download-forms-individual').removeAttr('disabled');

                if(obj.status == 'success')

                {

                  var url = '<?=iaBase()?>'+obj.path;

                  var a = document.createElement("a");

                  a.style = "display: none";

                  a.href = url;

                  a.download = obj.name;

                  document.body.appendChild(a);

                  a.click();

                  window.URL.revokeObjectURL(url);

                }else{

                  notiCustom('No Data Found', 'warning');

                }

              }

            });

        })



      $('.resend-all').click(function(){

        var cont_ids = [];

        if($('tr.row-marked').length > 0) {

        $(this).attr('disabled','disabled');

          $('tr.row-marked').each(function() {

            cont_ids.push($(this).attr('data-id'));

          });

          console.log(cont_ids);

          var dataNew = {

                          type :'resend',

                          c_id : '<?=$cid?>',

                          cont_ids: cont_ids,

                        };

          var baseUrl = '<?php echo base_url('vendordownload') ?>';

          $.ajax({

            url: baseUrl,

            type: "POST",

            data: dataNew,

            success: function(data) {

              $('.resend-all').removeAttr('disabled');

              var obj = $.parseJSON(data);

              if(obj.status == 'success') {

                notiCustom('W-9 Request resent', 'success');

              }

            }

          });

        } else {

          notiCustom('Select atleast one contractor.', 'info');

        }

      });



$('.dwn-pdfs').on('click', function() {
  $files = [];

  $('form#tempdfform').html('');

  $('input[name="formPdf[]"]').each(function() {

    if($(this).prop('checked')) {

       $files.push($(this).val());

      $('form#tempdfform').append('<input type="text" name="files[]" value="'+$(this).val()+'">');
    }

  });

  if($('input[name="files[]"]').length > 0) {

    $('form#tempdfform').submit();
   //console.log($('form#tempdfform').serialize());

  } else {

    notiCustom('Nothing selected to move', 'warning');

  }

});





   //$("table").tablesorter();

   $('.start-delete').click(function(){

      $(this).hide();

      $('.do-delete').show();

      $('.table-delete-mode').show();

   });



$('.mark-all-toggle').click(function(){

  if($(this).hasClass( "all-marked" ))

  {

      $(this).removeClass('all-marked');

      $('.do-delete').removeClass('btn-danger delete-active');

      $('.table tbody tr').removeClass('row-marked');

      $('.do-delete').addClass('btn-classic');

  }else{

      $(this).addClass('all-marked');

      $('.do-delete').addClass('btn-danger delete-active');

      $('.table tbody tr').each(function() {

        if($(this).find('td:first').html() != '') {

          $(this).addClass('row-marked');

        }

      });

      $('.do-delete').removeClass('btn-classic');

  }



});



 $('button.mark-box').click(function(){

   if($(this).parents('tr').hasClass( "row-marked" ))

   {

      $(this).parents('tr').removeClass('row-marked');

      if($('tr.row-marked').length == 0)

       {

        $('.do-delete').removeClass('btn-danger delete-active');

        $('.mark-all-toggle').removeClass('all-marked');

        $('.do-delete').addClass('btn-classic');

       }



   }else{

       $(this).parents('tr').addClass('row-marked');

       $('.do-delete').addClass('btn-danger delete-active');

       $('.do-delete').removeClass('btn-classic');

   }

 });

$('.do-delete').click(function(){

      $(this).hide();

      $('.start-delete').show();

      $('.table-delete-mode').hide();

   });

});

</script>