   <div id="page">
   <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
<a href="#" id="blaze-logos">
  <img id="w9-logo" src="<?php echo iaBase(); ?>assets/images/WageFilingw9-Logo-XSmall.png" alt="W-9's.com W-9">
</a>
<div class="row">
  <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
    <h1><a class="company-link"><span class="company-name">Company Requesting W-9</span></a></h1>
  </div>
  <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>
</div>
<a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Companies List</a>
</div>
</div>
        <div id="alerts-container" class="alerts-container"><div></div></div>
      </div>
    </div>
<div class="content-container" id="main-view-content"><div><div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="pending-step panel panel-primary">
        <div class="panel-heading">
          <h2 class="panel-title">Add Many Companies</h2>
        </div>
        <div class="panel-body">
          <p><strong>First:</strong> Use this CSV template: <a href="<?=iaBase()?>assets/csv/formate/w9_company_import_template.csv" class="btn btn-primary btn-lg" download >Companies CSV Template</a></p>
          <p><strong>Second:</strong> Add payers to the file, <strong>Save</strong> locally, then <strong>click Select File</strong> below.</p>
          <div>
            <span class="btn-choose-file btn-file-input btn btn-classic">
              <span>Select File</span>
              <div class="hidden-inner"><input class="hidden-inner-input" type="file" id="fileSelected" name="csv_file"></div>
            </span>
            <span name="filename" id="filename"></span>
            <div class="error-block" style="padding-top: 12px; display: none;"><div name="error" class="alert alert-danger" style="margin:0; display:inline-block" id="error-block"></div></div>
          </div>
        </div>
        <div class="panel-footer">
          <button type="button" class="btn btn-lg btn-primary import-button" >Import Companies</button>
        </div>
      </div>
    </div>
  </div>
  <div id="responce"></div>
</div>
</div>
</div>
</div>
<script>
      var reader_offset = 0;    //current reader offset position
      var buffer_size = 1024;   //
      var pending_content = '';

      /**
      * Read a chunk of data from current offset.
      */
      function readAndSendChunk()
      {
        var reader = new FileReader();
        var file = $('#fileSelected')[0].files[0];

        if(file != undefined){
        var ext = file.name.split('.').pop();
        if(ext == 'csv'){
          $('#filename').html(file.name);
          reader.onloadend = function(evt){

          //check for end of file
          if(evt.loaded == 0) return;

          //increse offset value
          reader_offset += evt.loaded;

          //check for only complete line
          var last_line_end = evt.target.result.lastIndexOf('\n');
          var content = pending_content + evt.target.result.substring(0, last_line_end);

          pending_content = evt.target.result.substring(last_line_end+1, evt.target.result.length);

          //upload data
          send(content);
        };
        var blob = file.slice(reader_offset, reader_offset + buffer_size);
        reader.readAsText(blob);
        }else{
          $('#filename').html('');
          $('.import-button').removeClass('disabled');
          $('#error-block').html( 'Please upload only "*.csv" files');
          $('.error-block').show();
        }
        }else{
          $('.import-button').removeClass('disabled');
          $('#filename').html('');
          $('#error-block').html( 'Please select a file');
          $('.error-block').show();
        }

      }

      /**
      * Send data to server using AJAX
      */
      function send(data_chank)
      {
        $.ajax({
          url: "<?=base_url('importdata')?>",
          method: 'POST',
          data: data_chank,
        }).done(function(response) {
          var obj = $.parseJSON(response);
          //$('.import-button').removeClass('disabled');
          if(obj.status == 'fail')
          {
            $('#error-block').html( obj.message );
            $('.error-block').show();
          }else{
            readAndSendChunk();
            window.location.href = "<?=base_url('blaze')?>"
          }
          //try for next chank

        });
      }

      /**
      * On page load
      */
      $(function(){
        $('.import-button').click(function(){
          $(this).addClass('disabled');
          $('.error-block').hide();
          reader_offset = 0;
          pending_content = '';
          readAndSendChunk();
        });
      });
    </script>