<body class="page-template-default page page-id-47 page-parent woocommerce-no-js fusion-image-hovers fusion-body ltr fusion-sticky-header no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center fusion-woo-product-design-classic mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
				<div id="wrapper" class="">


		<div id="sliders-container">
					</div>
		<main id="main" role="main" class="clearfix " style="">
			<div class="fusion-row" style="">
                 <section id="content" style="width: 100%;">
					<div id="post-47" class="post-47 page type-page status-publish hentry">
			<span class="entry-title rich-snippet-hidden">Filing Services</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.wagefiling.com/author/1099miscinstruct/" title="Posts by 1099miscinstruct" rel="author">1099miscinstruct</a></span></span><span class="updated rich-snippet-hidden">2018-04-08T14:58:32+00:00</span>
			<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'><div class="fusion-builder-row fusion-row "><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_4  fusion-three-fourth fusion-column-first 3_4"  style='margin-top:0px;margin-bottom:20px;width:75%;width:calc(75% - ( ( 4% ) * 0.75 ) );margin-right: 4%;'>
					<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
						<div class="fusion-builder-row fusion-builder-row-inner fusion-row "><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""><div class="fusion-text"><p>File Current 1090-Misc Forms</p>
</div><div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{background: #25a9bd;}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #ff8d61;}.fusion-button.button-1{width:100%;}</style><a class="fusion-button button-flat fusion-button-round button-xlarge button-custom button-1 fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%" target="_blank" rel="noopener noreferrer" href="/efiling-services/efile-current-1099s/"><span class="fusion-button-text">Learn More</span><i class=" fa fa-angle-right button-icon-right"></i></a></div></div></div><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""><div class="fusion-text"><p>File 1099-MISC Corrections</p>
</div><div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-2 .fusion-button-text, .fusion-button.button-2 i {color:#ffffff;}.fusion-button.button-2 {border-width:0px;border-color:#ffffff;}.fusion-button.button-2 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-2:hover .fusion-button-text, .fusion-button.button-2:hover i,.fusion-button.button-2:focus .fusion-button-text, .fusion-button.button-2:focus i,.fusion-button.button-2:active .fusion-button-text, .fusion-button.button-2:active{color:#ffffff;}.fusion-button.button-2:hover, .fusion-button.button-2:focus, .fusion-button.button-2:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-2{background: #25a9bd;}.fusion-button.button-2:hover,.button-2:focus,.fusion-button.button-2:active{background: #ff8d61;}.fusion-button.button-2{width:100%;}</style><a class="fusion-button button-flat fusion-button-round button-xlarge button-custom button-2 fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%" target="_blank" rel="noopener noreferrer" href="/efiling-services/efile-1099-corrections/"><span class="fusion-button-text">Learn More</span><i class=" fa fa-angle-right button-icon-right"></i></a></div></div></div><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""><div class="fusion-text"><p>File Past Year 1099 / W-2 Forms</p>
</div><div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-3 .fusion-button-text, .fusion-button.button-3 i {color:#ffffff;}.fusion-button.button-3 {border-width:0px;border-color:#ffffff;}.fusion-button.button-3 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-3:hover .fusion-button-text, .fusion-button.button-3:hover i,.fusion-button.button-3:focus .fusion-button-text, .fusion-button.button-3:focus i,.fusion-button.button-3:active .fusion-button-text, .fusion-button.button-3:active{color:#ffffff;}.fusion-button.button-3:hover, .fusion-button.button-3:focus, .fusion-button.button-3:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-3{background: #25a9bd;}.fusion-button.button-3:hover,.button-3:focus,.fusion-button.button-3:active{background: #ff8d61;}.fusion-button.button-3{width:100%;}</style><a class="fusion-button button-flat fusion-button-round button-xlarge button-custom button-3 fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%" target="_blank" rel="noopener noreferrer" href="/efiling-services/efile-past-year-forms/"><span class="fusion-button-text">Learn More</span><i class=" fa fa-angle-right button-icon-right"></i></a></div></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:20px;margin-bottom:20px;"></div><div class="fusion-builder-row fusion-builder-row-inner fusion-row "><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""><div class="fusion-text"><p>File W-2 Forms</p>
</div><div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-4 .fusion-button-text, .fusion-button.button-4 i {color:#ffffff;}.fusion-button.button-4 {border-width:0px;border-color:#ffffff;}.fusion-button.button-4 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-4:hover .fusion-button-text, .fusion-button.button-4:hover i,.fusion-button.button-4:focus .fusion-button-text, .fusion-button.button-4:focus i,.fusion-button.button-4:active .fusion-button-text, .fusion-button.button-4:active{color:#ffffff;}.fusion-button.button-4:hover, .fusion-button.button-4:focus, .fusion-button.button-4:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-4:hover .fusion-button-icon-divider, .fusion-button.button-4:hover .fusion-button-icon-divider, .fusion-button.button-4:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-4{background: #25a9bd;}.fusion-button.button-4:hover,.button-4:focus,.fusion-button.button-4:active{background: #ff8d61;}.fusion-button.button-4{width:100%;}</style><a class="fusion-button button-flat fusion-button-round button-xlarge button-custom button-4 fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%" target="_blank" rel="noopener noreferrer" href="/efiling-services/efile-w2-forms/"><span class="fusion-button-text">Learn More</span><i class=" fa fa-angle-right button-icon-right"></i></a></div></div></div><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""><div class="fusion-text"><p>Larger Filings</p>
</div><div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-5 .fusion-button-text, .fusion-button.button-5 i {color:#ffffff;}.fusion-button.button-5 {border-width:0px;border-color:#ffffff;}.fusion-button.button-5 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-5:hover .fusion-button-text, .fusion-button.button-5:hover i,.fusion-button.button-5:focus .fusion-button-text, .fusion-button.button-5:focus i,.fusion-button.button-5:active .fusion-button-text, .fusion-button.button-5:active{color:#ffffff;}.fusion-button.button-5:hover, .fusion-button.button-5:focus, .fusion-button.button-5:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-5:hover .fusion-button-icon-divider, .fusion-button.button-5:hover .fusion-button-icon-divider, .fusion-button.button-5:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-5{background: #25a9bd;}.fusion-button.button-5:hover,.button-5:focus,.fusion-button.button-5:active{background: #ff8d61;}.fusion-button.button-5{width:100%;}</style><a class="fusion-button button-flat fusion-button-round button-xlarge button-custom button-5 fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%" target="_blank" rel="noopener noreferrer" href="/efiling-services/bulk-1099-filing/"><span class="fusion-button-text">Learn More</span><i class=" fa fa-angle-right button-icon-right"></i></a></div></div></div><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""><div class="fusion-text"><p>TIN Checking</p>
</div><div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-6 .fusion-button-text, .fusion-button.button-6 i {color:#ffffff;}.fusion-button.button-6 {border-width:0px;border-color:#ffffff;}.fusion-button.button-6 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-6:hover .fusion-button-text, .fusion-button.button-6:hover i,.fusion-button.button-6:focus .fusion-button-text, .fusion-button.button-6:focus i,.fusion-button.button-6:active .fusion-button-text, .fusion-button.button-6:active{color:#ffffff;}.fusion-button.button-6:hover, .fusion-button.button-6:focus, .fusion-button.button-6:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-6:hover .fusion-button-icon-divider, .fusion-button.button-6:hover .fusion-button-icon-divider, .fusion-button.button-6:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-6{background: #25a9bd;}.fusion-button.button-6:hover,.button-6:focus,.fusion-button.button-6:active{background: #ff8d61;}.fusion-button.button-6{width:100%;}</style><a class="fusion-button button-flat fusion-button-round button-xlarge button-custom button-6 fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%" target="_blank" rel="noopener noreferrer" href="/efiling-services/tin-checking/"><span class="fusion-button-text">Learn More</span><i class=" fa fa-angle-right button-icon-right"></i></a></div></div></div></div><div class="fusion-sep-clear"></div><div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:20px;margin-bottom:20px;"></div><div class="fusion-builder-row fusion-builder-row-inner fusion-row "><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""><div class="fusion-text"><p>Identity Protection</p>
</div><div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-7 .fusion-button-text, .fusion-button.button-7 i {color:#ffffff;}.fusion-button.button-7 {border-width:0px;border-color:#ffffff;}.fusion-button.button-7 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-7:hover .fusion-button-text, .fusion-button.button-7:hover i,.fusion-button.button-7:focus .fusion-button-text, .fusion-button.button-7:focus i,.fusion-button.button-7:active .fusion-button-text, .fusion-button.button-7:active{color:#ffffff;}.fusion-button.button-7:hover, .fusion-button.button-7:focus, .fusion-button.button-7:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-7:hover .fusion-button-icon-divider, .fusion-button.button-7:hover .fusion-button-icon-divider, .fusion-button.button-7:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-7{background: #25a9bd;}.fusion-button.button-7:hover,.button-7:focus,.fusion-button.button-7:active{background: #ff8d61;}.fusion-button.button-7{width:100%;}</style><a class="fusion-button button-flat fusion-button-round button-xlarge button-custom button-7 fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%" target="_blank" rel="noopener noreferrer" href="/about/identity-protection/"><span class="fusion-button-text">Learn More</span><i class=" fa fa-angle-right button-icon-right"></i></a></div></div></div><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""><div class="fusion-text"><p>Combined State and Federal Filing</p>
</div><div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-8 .fusion-button-text, .fusion-button.button-8 i {color:#ffffff;}.fusion-button.button-8 {border-width:0px;border-color:#ffffff;}.fusion-button.button-8 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-8:hover .fusion-button-text, .fusion-button.button-8:hover i,.fusion-button.button-8:focus .fusion-button-text, .fusion-button.button-8:focus i,.fusion-button.button-8:active .fusion-button-text, .fusion-button.button-8:active{color:#ffffff;}.fusion-button.button-8:hover, .fusion-button.button-8:focus, .fusion-button.button-8:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-8:hover .fusion-button-icon-divider, .fusion-button.button-8:hover .fusion-button-icon-divider, .fusion-button.button-8:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-8{background: #25a9bd;}.fusion-button.button-8:hover,.button-8:focus,.fusion-button.button-8:active{background: #ff8d61;}.fusion-button.button-8{width:100%;}</style><a class="fusion-button button-flat fusion-button-round button-xlarge button-custom button-8 fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%" target="_blank" rel="noopener noreferrer" href="/efiling-services/combined-federal-state-filing/"><span class="fusion-button-text">Learn More</span><i class=" fa fa-angle-right button-icon-right"></i></a></div></div></div><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url=""></div></div></div><div class="fusion-clearfix"></div>

					</div>
				</div><div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last 1_5"  style='margin-top:0px;margin-bottom:20px;width:20%;width:calc(20% - ( ( 4% ) * 0.2 ) );'>
					<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
						<div class="fusion-widget-area fusion-widget-area-1 fusion-content-widget-area"><style type="text/css" scoped="scoped">.fusion-widget-area-1 {padding:0px 0px 0px 0px;}.fusion-widget-area-1 .widget h4 {color:#191919;}.fusion-widget-area-1 .widget .heading h4 {color:#191919;}.fusion-widget-area-1 .widget h4 {font-size:18px;}.fusion-widget-area-1 .widget .heading h4 {font-size:18px;}</style><div id="text-24" class="widget widget_text">			<div class="textwidget"><a href="http://secure.wagefiling.com/NewUser.asp"><img src="https://www.wagefiling.com/wp-content/uploads/2015/06/create-account-btn.png"/></a></div>
		</div><div id="nav_menu-10" class="widget widget_nav_menu"><h4 class="title">Have questions? We have answers!</h4><div class="menu-help-container"><ul id="menu-help" class="menu"><li id="menu-item-3565" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3565"><a href="https://www.wagefiling.com/help/">Help</a></li>
<li id="menu-item-3390" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3390"><a href="https://www.wagefiling.com/help/faq/">FAQ</a></li>
<li id="menu-item-3404" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3404"><a href="https://www.wagefiling.com/contact/">Contact</a></li>
<li id="menu-item-3388" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3388"><a href="https://www.wagefiling.com/help/1099-filing-instructions/">1099 Filing Instructions</a>
<ul class="sub-menu">
	<li id="menu-item-3427" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3427"><a href="https://www.wagefiling.com/help/how-to-save-recipient-copies-as-a-pdf/">How to save recipient copies as a PDF</a></li>
	<li id="menu-item-3389" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3389"><a href="https://www.wagefiling.com/efiling-services/combined-federal-state-filing/">Combined Federal &#038; State Filing Requirements</a></li>
	<li id="menu-item-3394" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3394"><a href="https://www.wagefiling.com/help/1099-filing-instructions/state-1099-filing-instructions/">State 1099-MISC and W-2 Filing Instructions and Deadlines</a></li>
	<li id="menu-item-3393" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3393"><a href="https://www.wagefiling.com/help/1099-filing-instructions/penalties-for-not-filing-1099s/">Penalties for Not Filing 1099s</a></li>
</ul>
</li>
</ul></div></div><div id="nav_menu-11" class="widget widget_nav_menu"><h4 class="title">Learn more about WageFiling</h4><div class="menu-learn-about-1099-filing-container"><ul id="menu-learn-about-1099-filing" class="menu"><li id="menu-item-3821" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3821"><a href="https://www.wagefiling.com/who-uses-1099s/">Who Uses WageFilng?</a></li>
<li id="menu-item-3386" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3386"><a href="https://www.wagefiling.com/help/what-is-a-1099-misc-form/">What is a 1099-MISC Form?</a></li>
<li id="menu-item-3411" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3411"><a href="https://www.wagefiling.com/how-it-works/">How WageFiling Works</a></li>
<li id="menu-item-3509" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3509"><a href="https://www.wagefiling.com/about/">About Us</a></li>
<li id="menu-item-3510" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3510"><a href="https://www.wagefiling.com/efiling-services/identity-protection/">SSN Masking on W-2/1099-MISC Forms</a></li>
<li id="menu-item-3511" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3511"><a href="https://www.wagefiling.com/about/go-green/">Save 3 Million</a></li>
</ul></div></div><div class="fusion-additional-widget-content"></div></div><div class="fusion-clearfix"></div>

					</div>
				</div></div></div>
							</div>
																																							</div>
		</section>

				</div>  <!-- fusion-row -->
			</main>  <!-- #main -->

								</div> <!-- wrapper -->


<script type='text/javascript' src='https://www.wagefiling.com/wp-includes/js/comment-reply.min.js?ver=4.9.8'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionVideoGeneralVars = {"status_vimeo":"1","status_yt":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"metro-white","lightbox_title":"1","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.9","lightbox_desc":"1","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"vertical","lightbox_post_images":"1","lightbox_animation_speed":"Normal"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionMapsVars = {"admin_ajax":"https:\/\/www.wagefiling.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionEqualHeightVars = {"content_break_point":"800"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionBgImageVars = {"content_break_point":"800"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionVideoBgVars = {"status_vimeo":"1","status_yt":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionTabVars = {"content_break_point":"800"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"1","slideshow_speed":"7000","pagination_video_slide":"","status_yt":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionTestimonialVars = {"testimonials_speed":"4000"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionCountersBox = {"counter_box_speed":"1000"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/vimeoPlayer.js?ver=2.2.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionVideoVars = {"status_vimeo":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionCarouselVars = {"related_posts_speed":"2750","carousel_speed":"2750"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionFlexSliderVars = {"status_vimeo":"1","page_smoothHeight":"false","slideshow_autoplay":"1","slideshow_speed":"7000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"1","slideshow_speed":"7000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1099","header_sticky_mobile":"0","header_sticky_tablet":"0","mobile_menu_design":"modern","sticky_header_shrinkage":"0","nav_height":"84","nav_highlight_border":"0","nav_highlight_style":"arrow","logo_margin_top":"31px","logo_margin_bottom":"31px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-header.js?ver=5.6.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1099","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-menu.js?ver=5.6.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var fusionTypographyVars = {"site_width":"1240px","typography_responsive":"1","typography_sensitivity":"0.6","typography_factor":"1.5","elements":"h1, h2, h3, h4, h5, h6"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaCommentVars = {"title_style_type":"single solid","title_margin_top":"0%","title_margin_bottom":"30px"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-comments.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-general-footer.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-quantity.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-scrollspy.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-select.js?ver=5.6.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1099","header_sticky_tablet":"0","sticky_header_shrinkage":"0","nav_height":"84","content_break_point":"800"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-sidebars.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-tabs-widget.js?ver=5.6.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var toTopscreenReaderText = {"label":"Go to Top"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaToTopVars = {"status_totop_mobile":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-to-top.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-drop-down.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-contact-form-7.js?ver=5.6.2'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/library/jquery.elasticslider.js?ver=5.6.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaElasticSliderVars = {"tfes_autoplay":"1","tfes_animation":"sides","tfes_interval":"3000","tfes_speed":"800","tfes_width":"150"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-elastic-slider.js?ver=5.6.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaWooCommerceVars = {"order_actions":"Details","title_style_type":"single solid","woocommerce_shop_page_columns":"4","woocommerce_checkout_error":"Not all fields have been filled in correctly.","woocommerce_single_gallery_size":"500","related_products_heading_size":"3"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/themes/Avada/assets/min/js/general/avada-woocommerce.js?ver=5.6.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var avadaFusionSliderVars = {"side_header_break_point":"1099","slider_position":"below","header_transparency":"0","header_position":"Top","content_break_point":"800","status_vimeo":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
<script type='text/javascript' src='https://www.wagefiling.com/wp-includes/js/wp-embed.min.js?ver=4.9.8'></script>
<script type="text/javascript">
_linkedin_partner_id = "415353";
window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=415353&fmt=gif" />
</noscript>	</body>
</html>
