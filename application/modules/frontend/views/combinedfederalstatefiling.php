<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: url('../assets/images/pastyear.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 30px;">
					<h3>Combined Federal and <span class="light">State Filing Requirements</span></h3>
					<h5>File Past Year W-2 and 1099-MISC from 2007-2012 Instantly! </h5>
				</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<h5>The IRS established the Combined Federal/State Filing (CF/SF) Program to simplify information returns filing for the taxpayer. Through the CF/SF Program, IRS forwards original and corrected information returns filed electronically to participating states free of charge for approved filers. Separate reporting to those states is not required. If you aren't sure additional information can be found at your State's Department of Revenue, keyword "1099 reporting".</h5>
			<h4>Current Federal/State Filing Program Participiants</h4>
			<p class="lead"><strong>Participating CF/SF States:</strong></p>
			<img src="<?php echo iaBase(); ?>assets/images/cfsf.jpg">
			<table class="table table-bordered" width="50%">
                      <tbody>
                        <tr>
                          <td width="20%" class="smallpara">Alabama</td><td width="20%" class="smallpara">Maryland</td>
                        </tr>
                        <tr>
                          <td width="20%" class="smallpara">Arizona</td><td width="20%" class="smallpara">Massachusetts</td>
                        </tr>
                        <tr>
                          <td width="20%" class="smallpara">Arkansas</td><td width="20%" class="smallpara">Minnesota</td>
                      </tr>
                      <tr>
                          <td width="20%" class="smallpara">California</td><td width="20%" class="smallpara">Mississippi</td>
                      </tr>
                      <tr>
                            <td width="20%" class="smallpara">Colorado</td><td width="20%" class="smallpara">Missouri</td>
                      </tr>
                      <tr>
                            <td width="20%" class="smallpara">Connecticut</td><td width="20%" class="smallpara">Montana</td>
                      </tr>
                      <tr>
                            <td width="20%" class="smallpara">Delaware</td><td width="20%" class="smallpara">Nebraska</td>
                            </tr>
                      <tr>
                            <td width="20%" class="smallpara">Washington D.C.</td><td width="20%" class="smallpara">New Jersey</td>
                            </tr>
                      <tr>
                            <td width="20%" class="smallpara">Georgia</td><td width="20%" class="smallpara">New Mexico</td>
                            </tr>
                      <tr>
                            <td width="20%" class="smallpara">Hawaii</td><td width="20%" class="smallpara">North Carolina</td>
                            </tr>
                      <tr>
                            <td width="20%" class="smallpara">Idaho</td><td width="20%" class="smallpara">North Dakota</td>
                            </tr>
                      <tr>
                            <td width="20%" class="smallpara">Indiana</td><td width="20%" class="smallpara">Ohio</td>
                            </tr>
                      <tr>
                            <td width="20%" class="smallpara">Iowa</td><td width="20%" class="smallpara">South Carolina</td>
                            </tr>
                      <tr>
                            <td width="20%" class="smallpara">Kansas</td><td width="20%" class="smallpara">Utah</td>
                      <tr>
                            <td width="20%" class="smallpara">Louisiana</td><td width="20%" class="smallpara">Virginia</td>
                        </tr>
                      <tr>
                            <td width="20%" class="smallpara">Maine</td><td width="20%" class="smallpara">Wisconsin</td>
                        </tr>
                      </tbody>
                    </table>
			<br />
			<hr>
			<h4>Contact Us</h4>
			<h5>WageFilingPlus. LLC.</h5>
			<h5>Telephone: 616-325-9332</h5>
			<h5>E-mail: <a href="mailto:support@wagefilingplus.com">support@WageFilingPlus.com</a></h5>
			<h5>Trusted by</h4>
			<img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
