    <!-- Stripe JavaScript library -->
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        //set your publishable key
        Stripe.setPublishableKey('pk_live_wdiFap79UuPelmF0R3bZ7OAx');

        //callback to handle the response from stripe
        function stripeResponseHandler(status, response) {
            if (response.error) {
                //enable the submit button
                $('#payBtn').removeAttr("disabled");
                //display the errors on the form
                // $('#payment-errors').attr('hidden', 'false');
                $('#payment-errors').addClass('alert alert-danger');
                $("#payment-errors").html(response.error.message);
            } else {
                var form$ = $("#paymentFrm");
                //get token id
                var token = response['id'];
                //insert the token into the form
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                //submit form to the server
                form$.get(0).submit();
            }
        }
        $(document).ready(function() {
            //on form submit
            $("#paymentFrm").submit(function(event) {
                //disable the submit button to prevent repeated clicks
                $('#payBtn').attr("disabled", "disabled");
                var card_num = $("#card_num").val();
                if (card_num.length != 16)
                {
                    $('#card_num').val('');
                    $('#card_num').focus();
                }

                //create single-use token to charge the user
                Stripe.createToken({
                    number: $('#card_num').val(),
                    cvc: $('#card-cvc').val(),
                    exp_month: $('#card-expiry-month').val(),
                    exp_year: $('#card-expiry-year').val()
                }, stripeResponseHandler);

                //submit from callback
                return false;
            });
        });
    </script>

<div class="container">
	<div class="row" >
    <br>
    <div class="col-md-12">




    </div>
    <br><br>
     <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">ORDER SUMMARY</h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Invoice Number</th>
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php $items = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
echo str_replace("_", " , ", $items);?></td>
                            <td><?php
$digits = 3;
echo 'A' . rand(pow(10, $digits - 1), pow(10, $digits) - 1);?>
                            </td>
                            <td><?php $amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : 0.00;
echo '$' . $amount;?></td>
                        </tr>
                    </tbody>
                </table>

                <div class="card">
                    <div class="card-header bg-success text-white"></div>
                    <div class="card-body bg-light">
                        <?php if (validation_errors()): ?>
                            <div class="alert alert-danger" role="alert">
                                <strong>Oops!</strong>
                                <?php echo validation_errors(); ?>
                            </div>
                        <?php endif?>
                        <div id="payment-errors"></div>
                         <form method="post" id="paymentFrm" enctype="multipart/form-data" action="<?php echo base_url(); ?>frontend/check">
                          <input type="hidden" name="stripeToken" id="stripeToken">
                          <input type="hidden" name="items" class="form-control" value="<?php echo $items; ?>"  />
                          <input type="hidden" name="amount" class="form-control" value="<?php echo $amount; ?>"  />
                          <?php $filevalue = isset($_REQUEST['filevalue']) ? $_REQUEST['filevalue'] : '';?>
                          <input type="hidden" name="filevalue" class="form-control" value="<?php echo $filevalue; ?>"  />

                             <div class="form-group">
                                <input type="number" name="card_num" id="card_num" class="form-control" placeholder="Card Number" autocomplete="off" value="<?php echo set_value('card_num'); ?>" required>
                            </div>


                            <div class="row">

                                <div class="col-sm-8">
                                     <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" name="exp_month" maxlength="2" class="form-control" id="card-expiry-month" placeholder="MM" value="<?php echo set_value('exp_month'); ?>" required>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" name="exp_year" class="form-control" maxlength="4" id="card-expiry-year" placeholder="YYYY" required value="<?php echo set_value('exp_year'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input type="text" name="cvc" id="card-cvc" maxlength="3" class="form-control" autocomplete="off" placeholder="CVC" value="<?php echo set_value('cvc'); ?>" required>
                                    </div>
                                </div>
                            </div>




                            <div class="form-group text-right">
                              <button class="btn btn-primary" style="color: #ffffff !important;" type="reset">Reset</button>
                              <button type="submit" id="payBtn" class="btn btn-primary" style="color: #ffffff !important;">Submit Payment</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        </div>

     <div class="col-md-2"></div>



    </div>
</div>

