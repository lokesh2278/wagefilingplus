<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Frontend extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Frontend_model');
        $this->lang->load('user', getSetting('language'));
        $this->load->library('encryption');
        $this->load->helper('csv');
    }

    /* Home Page */
    public function index()
    {
        $this->load->view("include/front/header");
        $this->load->view('home');
        $this->load->view("include/front/footer");
    }

    /* Signup Page */
    public function signup()
    {
        if (isset($_SESSION['user_details'])) {
            $CustID = $this->session->userdata('user_details')[0]->CustID;
            $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
            if (count($c) > 0) {
                redirect(base_url('blaze'), 'refresh');
            } else {
                redirect(base_url('company'), 'refresh');
            }
        }
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->addEdit();
        } else {
            $this->load->view('signup');
        }
        $this->load->view("include/front/footer");
    }

    /* Login Page */
    public function login()
    {
        if (isset($_SESSION['user_details'])) {
            $CustID = $this->session->userdata('user_details')[0]->CustID;
            $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
            if (count($c) > 0) {
                redirect(base_url('blaze'), 'refresh');
            } else {
                redirect(base_url('company'), 'refresh');
            }
        }
        if ($this->input->post()) {
            $return = $this->Frontend_model->authUser();
            if (empty($return)) {
                $art_msg['msg'] = lang('invalid_details');
                $art_msg['type'] = 'warning';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url('login'), 'refresh');
            } else {
                if ($return == 'not_varified') {
                    $art_msg['msg'] = lang('this_account_is_not_verified_please_contact_to_your_admin');
                    $art_msg['type'] = 'danger';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url('login'), 'refresh');
                } else {
                    $this->session->set_userdata('user_details', $return);
                }
                $CustID = $this->session->userdata('user_details')[0]->CustID;
                $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
                if (count($c) > 0) {
                    redirect(base_url('blaze'), 'refresh');
                } else {
                    redirect(base_url('company'), 'refresh');
                }
            }
        } else {
            $this->load->view("include/front/header");
            $this->load->view('login');
            $this->load->view("include/front/footer");
        }
    }

    /**
     * This function is used to logout user
     * @return Void
     */
    public function logout()
    {
        isFrontLogin();
        $this->session->unset_userdata('user_details');
        redirect(base_url('login'), 'refresh');
    }

    /* Signup Page */
    public function company($id = '')
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->addEditCompany();
        } else {
            if (!empty($id)) {
                $data['c_details'] = $this->Frontend_model->getData('Files', $id, 'c_id');
            } else {
                $data['c_details'] = array();
            }
            $this->load->view('company', $data);
        }
        $this->load->view("include/front/footer");

    }

    /* Request Page */
    public function request($cid)
    {

        isFrontLogin();
        if ($this->input->post()) {
            $this->addEditVendor();
        } else {
            $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'c_id');
            $this->load->view("include/front/header");
            $this->load->view('request', $data);
            $this->load->view("include/front/footer");
        }
    }

    /* Vendor Page */
    public function vendor($cid = '')
    {
        isFrontLogin();
        $data['cid'] = $cid;
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'c_id');
        if (!empty($data['c_details'])) {
            $data['v_details'] = $this->Frontend_model->getData('ia_vendors', $cid, 'c_id');
            $this->load->view("include/front/header");
            $this->load->view('vendor', $data);
            $this->load->view("include/front/footer");
        } else {
            $this->load->view("include/front/header");
            $this->load->view('404error');
            $this->load->view("include/front/footer");
        }

    }

    /* Company List Page */
    public function blaze()
    {
        isFrontLogin();
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $data['companies'] = $this->Frontend_model->getData('Files', $CustID, 'CustID');
        $this->load->view("include/front/header");
        $this->load->view('blaze', $data);
        $this->load->view("include/front/footer");

    }

    /* Assigned List  */
    public function assigned()
    {
        isFrontLogin();
        $data = $this->input->post();
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $assigned = $this->Frontend_model->getData('ia_vendors', $data['id'], 'c_id');
        $vendors = $this->Frontend_model->getData('ia_vendors', $CustID, 'CustID');
        $value = '';
        if (!empty($vendors)) {
            $i = 0;
            foreach ($vendors as $key => $vendor) {
                //if()
                $value .= '<label for="c8_editor_ids-' . $i . '">
          <input type="checkbox" name="editor_ids" value="' . $vendor->v_id . '" id="c8_editor_ids-' . $i . '"> &nbsp&nbsp' . $vendor->v_name . ' &lt;' . $vendor->v_email . '&gt'
                    . '</label> <br/>';
            }
        }
        echo $value;
    }

    /* W-9 Fprm Page */
    public function enter($form_id = '')
    {

        if (!empty($form_id)) {
            $data['vendors'] = $this->Frontend_model->getDataBy('ia_vendors', $form_id, 'form_id');
            $data['c_id'] = $form_id;
            //$this->load->view("include/front/header");
            $this->load->view('w9-form', $data);
            //$this->load->view("include/front/footer");

        } else {
            redirect(base_url('login'), 'refresh');
        }
    }

    /**
     * This function is used to add and update Company
     * @return Void
     */

    public function addEditCompany()
    {
        $data = $this->input->post();

        if (!empty($data['c_id'])) {
            $d = array(
                "c_name" => $data['name'],
                "c_tin" => $data['tin'],
                "c_street" => $data['shipping_address'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_email" => $data['email'],
                "c_telephone" => $data['telephone'],
            );
            $this->Frontend_model->updateRow('Files', 'c_id', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            $d = array(
                "c_name" => $data['name'],
                "c_tin" => $data['tin'],
                "c_street" => $data['shipping_address'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_email" => $data['email'],
                "c_telephone" => $data['telephone'],
                "is_deleted" => 0,
                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                "created_at" => date('Y-m-d h:i:s'),
            );
            $id = $this->Frontend_model->insertRow('Files', $d);
            redirect(base_url('request') . '/' . $id, 'refresh');
        }

    }

    /**
     * This function is used to add and update Vendor
     * @return Void
     */

    public function addEditVendor()
    {
        $data = $this->input->post();
        $c_id = $data['c_id'];
        $setting = settings();
        $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'c_id');
        $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
        $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
        $sub = $c_name . ' W-9 Request';

        $i = 0;

        foreach ($data['v_email'] as $key => $value) {
            if (!empty($data['v_email'][$i])) {
                $form_id = md5(uniqid(rand(), true));
                $d = array(
                    "v_name" => $data['v_name'][$i],
                    "v_email" => $data['v_email'][$i],
                    //"v_account" => $data['account_number'][$i],
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "c_id" => $c_id,
                    "CustID" => $this->session->userdata('user_details')[0]->CustID,
                    "form_id" => $form_id,
                    "created_at" => date('Y-m-d h:i:s'),
                );
                $email = $data['v_email'][$i];
                $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"><div class="brand"><img src="' . iaBase() . 'assets/images/WageFilingw9-Logo-XSmall.png" alt="W-9\'s.com W-9"><span class="subbrand"><i>Powered by</i><a href="' . base_url() . '"> W-9\'s.com</a></span></div><div class="sender"><h1><i>RS W-9 Request from W-9\'s.com</i></h1></div></div></div><div class="container"><p>Dear ' . $data['v_name'][$i] . ',</p><p>Comapny is requesting an IRS form W-9 from you. This form is required in order to receive payments from them. </p><p><a href="' . base_url('enter') . '/' . $form_id . '" class="go">Fill out your W-9</a></p>If you have questions regarding this request, please contact Comapny at <a href="mailto:mailto:' . $c_email . '">' . $c_email . '</a>. Comapny has partnered with W-9\'s.com to provide paper-free, online W-9 and 1099 forms.<p>Best Regards,</p><p>The Team at Comapny<br><br><a href="mailto:' . $c_email . '">' . $c_email . '</a></p></div></div></div>';

                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
                    // content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                $this->Frontend_model->insertRow('ia_vendors', $d);
            }
            $i++;
        }
        redirect(base_url('vendor') . '/' . $c_id, 'refresh');
    }

    /**
     * This function is used to add and update users
     * @return Void
     */
    public function addEdit()
    {
        $data = $this->input->post();
        $checkValue = $this->Frontend_model->checkExists('ia_users', 'email', $this->input->post('email'), '', '');
        if ($checkValue) {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $d = array(
                "name" => $data['full_name'],
                "password" => $password,
                "status" => 'active',
                "email" => $data['email'],
                "phone" => $data['phone_number'],
                "user_type" => 'user',
                "is_deleted" => 0,
                "create_date" => date('Y-m-d'),
            );
            $id = $this->Frontend_model->insertRow('ia_users', $d);
            $new_data = $this->Frontend_model->getDataBy('ia_users', $id, 'CustID');
            if (!isset($this->session->userdata('user_details')[0]->CustID)) {
                $this->session->set_userdata('user_details', $new_data);
            }
            redirect(base_url('company'), 'refresh');
        } else {
            $art_msg['msg'] = lang('this_email_already_registered_with_us');
            $art_msg['type'] = 'info';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url('signup'), 'refresh');
        }
    }
    /**
     * This function is used to add and update Request Form
     * @return Void
     */
    public function saveData()
    {
        $data = $this->input->post();

        if (isset($data['business_classification'])) {
            $data['business_classification'] = implode('|', $data['business_classification']);
        } else {
            $data['business_classification'] = '';
        }
        $form_id = md5(uniqid(rand(), true));
        $v_id = !empty($data['v_id']) ? $data['v_id'] : '';
        $saveData['v_name'] = !empty($data['name']) ? $data['name'] : '';
        $saveData['business_name'] = !empty($data['business_name']) ? $data['business_name'] : '';
        $saveData['business_classification'] = !empty($data['business_classification']) ? $data['business_classification'] : '';
        $saveData['exempt_payee_code'] = !empty($data['exempt_payee_code']) ? $data['exempt_payee_code'] : '';
        $saveData['exempt_fatca_code'] = !empty($data['exempt_fatca_code']) ? $data['exempt_fatca_code'] : '';
        //$saveData['v_account'] =  !empty($data['account_number'])?$data['account_number']:'';
        $saveData['address'] = !empty($data['address']) ? $data['address'] : '';
        $saveData['state'] = !empty($data['state']) ? $data['state'] : '';
        $saveData['city'] = !empty($data['city']) ? $data['city'] : '';
        $saveData['zip'] = !empty($data['zip']) ? $data['zip'] : '';
        $saveData['foreign_address'] = !empty($data['foreign_address']) ? $data['foreign_address'] : '';
        $saveData['v_email'] = !empty($data['email']) ? $data['email'] : '';
        $saveData['v_ssn'] = !empty($data['v_ssn']) ? $data['v_ssn'] : '';
        $saveData['v_ein'] = !empty($data['v_ein']) ? $data['v_ein'] : '';
        $saveData['other'] = !empty($data['business_other']) ? $data['business_other'] : '';
        $saveData['no_backup_withholding'] = !empty($data['no_backup_withholding']) ? $data['no_backup_withholding'] : '';
        $saveData['signature'] = !empty($data['signature']) ? $data['signature'] : '';
        $pdfName = !empty($data['form_id']) ? $data['form_id'] : $form_id;

        if (!empty($v_id)) {
            $this->Frontend_model->updateRow('ia_vendors', 'v_id', $v_id, $saveData);
        } else {
            $saveData['v_status'] = 'Manual';
            $saveData['c_id'] = !empty($data['c_id']) ? $data['c_id'] : '';
            $saveData['CustID'] = $this->session->userdata('user_details')[0]->CustID;
            $saveData["created_at"] = date('Y-m-d h:i:s');
            $saveData["form_id"] = $form_id;
            $v_id = $this->Frontend_model->insertRow('ia_vendors', $saveData);
        }

        $i_checked = '';
        $c_checked = '';
        $s_checked = '';
        $p_checked = '';
        $t_checked = '';
        $l_checked = '';
        $o_checked = '';
        if ($saveData['business_classification'] == "Individual") {
            $i_checked = 'checked';
        } else if ($saveData['business_classification'] == "C Corporation") {
            $c_checked = 'checked';
        } else if ($saveData['business_classification'] == "S Corporation") {
            $s_checked = 'checked';
        } else if ($saveData['business_classification'] == "Partnership") {
            $p_checked = 'checked';
        } else if ($saveData['business_classification'] == "Trust/estate") {
            $t_checked = 'checked';
        } else if ($saveData['business_classification'] == "LLC-C" || $saveData['business_classification'] == "LLC-S" || $saveData['business_classification'] == "LLC-P") {
            $l_checked = 'checked';
        } else if ($saveData['business_classification'] == "Other") {
            $o_checked = 'checked';
        }
        $strikeout = empty($saveData['no_backup_withholding']) ? 'style="color:red;text-decoration:line-through;"' : '';
        $html = '<table style="width:100%" id="table" border="1" style="width:100%" style="border:1px solid #000;"><tr><th width="20%" style="text-align: center;"><p>From W-9 <br/> (Rev. November 2017) Department of the Treasury<br/>Internal Revenue Services<p></th><th style="text-align: center;"> Request for Taxpayer<br/>Identification Number and Certification </th> <th width="20%" style="text-align: center;width:20%" >Give from to the requester. <br/>Do not send to the IRS.</th></tr>';
        $html .= '<tr><td colspan="3">Name (as shown on your income tax return) <br/> <b>' . $saveData["v_name"] . '</b></td></tr>';
        $html .= '<tr><td colspan="3">Business name/disregarded entity name, if different from above <br/> <b>' . $saveData["business_name"] . '</b></td></tr><tr><td colspan="2">Select one appropriate box for federal tax classification: <br/><input type="checkbox" ' . $i_checked . '>Individual/sole proprietor or single member LLC  <input type="checkbox" ' . $c_checked . '>C Corporation <input type="checkbox" ' . $s_checked . '>S Corporation <br/> <input type="checkbox" ' . $p_checked . '>Partnership <br/> <input type="checkbox" ' . $t_checked . '>Trust/estate <br/><input type="checkbox" ' . $l_checked . '>Limited liability company (C Corporation, S Corporation, Partnership) <br/><input type="checkbox" ' . $o_checked . '>Other (see instructions): ' . $saveData["other"] . ' </td> <td>Exempt payee code (if any)  <br/><b>' . $saveData["exempt_payee_code"] . '</b><br/>Exemption from FATCA reporting code (if any) <br/><b>' . $saveData["exempt_fatca_code"] . '</b></td></tr><tr><td colspan="2">Address (number, street, and apt. or suite no. <br/><b>' . $saveData["address"] . '</b></td><td rowspan="2">Requester`s name and address (optional) <br/>' . $saveData["v_name"] . ' <br/>' . $saveData["address"] . ' <br/>' . $saveData["city"] . '
     ' . $saveData["state"] . '
     ' . $saveData["zip"] . '</td></tr>';
        $html .= '<tr><td colspan="2">City or town, state, and ZIP code <br/><b>' . $saveData["city"] . ', ' . $saveData["state"] . ',  ' . $saveData["zip"] . '</b> </td> </tr>
 <tr>
 <td colspan="3">Email (optional) <br/>
 <b> ' . $saveData["v_email"] . '</b>
 </td>
 </tr>

 <tr>
 <th width="10%" >Part 1 </th>
 <th colspan="2">Taxpayer Identification Number (TIN) </th>
 </tr>
 <tr>
 <td colspan="2">Enter your TIN in the appropriate box. The TIN provided must match the name given on line 1 to avoid backup withholding. For individuals, this is generally your social security number (SSN). However, for a resident alien, sole proprietor, or disregarded entity, see the Part I instructions. For other entities, it is your employer identification number (EIN). If you do not have a number, see How to get a TIN below. </td>
 <td colspan="1" style="padding:5px;">Social Security Number <br> <input type="text" value="' . $saveData["v_ssn"] . '" class="form-control"> <br/> Employer Identification Number <br> <input type="text" value="' . $saveData["v_ein"] . '" class="form-control"> </td></tr>';
        $html .= '<tr>
 <th width="10%" >Part 2 </th><th colspan="2">Certification </th> </tr>
 <tr>
 <td colspan="3">Under penalties of perjury, I certify that: <br/>
 1. The number shown on this form is my correct taxpayer identification number (or I am waiting for a number to be issued to me); and <br/>';
        $html .= '<div ' . $strikeout . '>2. I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal Revenue Service (IRS) that I am subject to backup withholding as a result of a failure to report all interest or dividends, or (c) the IRS has notified me that I am no longer subject to backup withholding; and </div><br/>
 3. I am a U.S. citizen or other U.S. person (defined below); and
 4. The FATCA code(s) entered on this form (if any) indicating that I am exempt from FATCA reporting is correct.<br/><br/>
 You must cross out item 2 above if you have been notified by the IRS that you are currently subject to backup withholding because you have failed to report all interest and dividends on your tax return. For real estate transactions, item 2 does not apply. For mortgage interest paid, acquisition or abandonment of secured property, cancellation of debt, contributions to an individual retirement arrangement (IRA), and generally, payments other than interest and dividends, you are not required to sign the certification, but you must provide your correct TIN. See the instructions.
 <br/></td>
 </tr><tr><th >Sign Here</th><th >Signature of  U.S. Person <br/><img src="' . $saveData["signature"] . '" width="80" height="40"></th><th >Date <b>' . date("Y-m-d") . '</b></th></tr>';
        $html .= '</table>';
        $html .= '<style>table, th {
 border: 1px solid black;
 font-size:11px;
 padding:3px;
 font-family: sans-serif;
 text-align:left;
}
td{
 border: 1px solid black;
 padding-left:5px;
 padding-top:5px;
 padding-bottom:5px;
 font-size:11px;
 font-family: sans-serif;
}
#table {
 border-collapse: collapse;
} p{ font-size:12px; }
input[type="checkbox"] {
       display: inline!important;
       margin-right: 10px;
}
b{
  font-size:14px;
  font-weight:500;
}
.form-control {
    display: block;
    width: 100%;
    height: 20px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
</style>';

        $this->load->library('pdfgenerator');
        $output = $this->pdfgenerator->generate($html, 'pdfnew');
        file_put_contents("pdf/" . $pdfName . ".pdf", $output);
        $dataOutput['pdfPath'] = 'pdf/' . $pdfName . '.pdf';
        $dataOutput['v_id'] = $v_id;
        echo json_encode($dataOutput);
    }

    public function sendPdf()
    {
        $data = $this->input->post();
        $v_id = !empty($data['v_id']) ? $data['v_id'] : '';
        $saveData['pdf'] = !empty($data['pdf']) ? $data['pdf'] : '';
        if (empty($data['v_status'])) {
            $saveData['v_status'] = 'Signed';
        }
        $this->Frontend_model->updateRow('ia_vendors', 'v_id', $v_id, $saveData);
    }

    public function delete()
    {
        $data = $this->input->post();
        if (!empty($data['deleteIds'])) {
            foreach ($data['deleteIds'] as $key => $delete_id) {
                $saveData['is_deleted'] = 1;
                if ($data['table'] == 'company') {
                    $this->Frontend_model->updateRow('Files', 'c_id', $delete_id, $saveData);
                } else if ($data['table'] == 'vendor') {
                    $this->Frontend_model->updateRow('ia_vendors', 'v_id', $delete_id, $saveData);
                }
            }
        }
    }

    public function edit($id = '')
    {
        $post = $this->input->post();
        if (!empty($post['v_id'])) {
            $v_id = $post['v_id'];
            unset($post['v_id']);
            $this->Frontend_model->updateRow('ia_vendors', 'v_id', $v_id, $post);
            redirect(base_url('vendor') . '/' . $post['c_id'], 'refresh');
        } else {
            $this->load->view("include/front/header");
            $data['vendor'] = $this->Frontend_model->getData('ia_vendors', $id, 'v_id');
            if ($data['vendor'][0]->v_status == 'Signed') {
                $datas['vendors'] = $this->Frontend_model->getData('ia_vendors', $id, 'v_id');
                $this->load->view('signedForm', $datas);
            } else {
                $this->load->view('vendorEdit', $data);
            }
            $this->load->view("include/front/footer");
        }

    }

    public function resendemail()
    {
        $post = $this->input->post();
        $c_id = $post['c_id'];
        $setting = settings();
        $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'c_id');
        $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
        $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
        $sub = $c_name . ' W-9 Request';

        if (!empty($post['v_email'])) {
            $form_id = md5(uniqid(rand(), true));
            if (!empty($post['request']) && $post['request'] == 'update') {
                $d = array(
                    "v_email" => $post['v_email'],
                    "v_name" => $post['v_name'],
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "c_id" => $c_id,
                    "CustID" => $this->session->userdata('user_details')[0]->CustID,
                    "form_id" => $form_id,
                    "created_at" => date('Y-m-d h:i:s'),
                );
            } else {
                $d = array(
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "c_id" => $c_id,
                    "CustID" => $this->session->userdata('user_details')[0]->CustID,
                    "form_id" => $form_id,
                    "pdf" => '',
                    "created_at" => date('Y-m-d h:i:s'),
                );
            }
            $email = $post['v_email'];
            $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"><div class="brand"><img src="' . iaBase() . 'assets/images/WageFilingw9-Logo-XSmall.png" alt="W-9\'s.com W-9"><span class="subbrand"><i>Powered by</i><a href="' . base_url() . '"> W-9\'s.com</a></span></div><div class="sender"><h1><i>RS W-9 Request from W-9\'s.com</i></h1></div></div></div><div class="container"><p>Dear ' . $post['v_name'] . ',</p><p>Comapny is requesting an IRS form W-9 from you. This form is required in order to receive payments from them. </p><p><a href="' . base_url('enter') . '/' . $form_id . '" class="go">Fill out your W-9</a></p>If you have questions regarding this request, please contact Comapny at <a href="mailto:mailto:' . $c_email . '">' . $c_email . '</a>. Comapny has partnered with W-9\'s.com to provide paper-free, online W-9 and 1099 forms.<p>Best Regards,</p><p>The Team at Comapny<br><br><a href="mailto:' . $c_email . '">' . $c_email . '</a></p></div></div></div>';

            if ($setting['mail_setting'] == 'php_mailer') {
                $this->load->library("send_mail");
                $emm = $this->send_mail->email($sub, $body, $email, $setting);
            } else {
                // content-type is required when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                $emm = mail($email, $sub, $body, $headers);
            }
            if (!empty($post['request']) && $post['request'] == 'update') {
                return $this->Frontend_model->insertRow('ia_vendors', $d);
            } else {
                $this->Frontend_model->updateRow('ia_vendors', 'v_id', $post['v_id'], $d);
            }
        }
    }

    public function requestupdate()
    {
        $id = $this->resendemail();
        if ($id > 0) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function importCsv()
    {
        $this->load->view("include/front/header");
        $this->load->view('importcsv');
        $this->load->view("include/front/footer");
    }

    public function importdata()
    {
        $content = file_get_contents('php://input');
        $lines = explode("\n", $content);
        $row = 1;
        if (count($lines) > 1) {
            foreach ($lines as $line) {
                $csv_row = str_getcsv($line);
                $num = count($csv_row);
                if ($num == 8) {
                    if ($row == 1) {
                        if (strtolower(str_replace(' ', '', $csv_row[0])) != 'companyname') {
                            echo json_encode(array('status' => 'fail', 'message' => 'This page is for importing companies, but it looks like you`re using a different CSV template.'));
                            return false;
                        }
                    } else {
                        $d = array(
                            "c_name" => !empty($csv_row[0]) ? $csv_row[0] : '',
                            "c_tin" => !empty($csv_row[1]) ? $csv_row[1] : '',
                            "c_street" => !empty($csv_row[2]) ? $csv_row[2] : '',
                            "c_city" => !empty($csv_row[3]) ? $csv_row[3] : '',
                            "c_state" => !empty($csv_row[4]) ? $csv_row[4] : '',
                            "c_zip" => !empty($csv_row[5]) ? $csv_row[5] : '',
                            "c_email" => !empty($csv_row[6]) ? $csv_row[6] : '',
                            "c_telephone" => !empty($csv_row[7]) ? $csv_row[7] : '',
                            "is_deleted" => 0,
                            "CustID" => $this->session->userdata('user_details')[0]->CustID,
                            "created_at" => date('Y-m-d h:i:s'),
                        );
                        $this->Frontend_model->insertRow('Files', $d);
                    }
                    $row++;
                } else {
                    echo json_encode(array('status' => 'fail', 'message' => 'This page is for importing companies, but it looks like you`re using a different CSV template.'));
                    return false;
                }
            }
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'fail', 'message' => 'No companies found to import'));
            return false;
        }
    }

    public function importvendordata()
    {
        $post = $this->input->post();
        $lines = explode("\n", $post['contents']);
        $row = 1;
        if (count($lines) > 1) {
            foreach ($lines as $line) {
                $csv_row = str_getcsv($line);
                $num = count($csv_row);
                if ($num == 3) {
                    if ($row == 1) {
                        if (strtolower(str_replace(' ', '', $csv_row[0])) != 'vendorname') {
                            echo json_encode(array('status' => 'fail', 'message' => 'You`re using a different CSV template.'));
                            return false;
                        }
                    } else {
                        $v_name = !empty($csv_row[0]) ? $csv_row[0] : '';
                        $v_email = !empty($csv_row[1]) ? $csv_row[1] : '';
                        //$v_account = !empty($csv_row[2])?$csv_row[2]:'';
                        if (!empty($v_name) && !empty($v_email)) {
                            $c_id = $post['c_id'];
                            $setting = settings();
                            $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'c_id');
                            $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
                            $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
                            $sub = $c_name . ' W-9 Request';
                            $form_id = md5(uniqid(rand(), true));

                            $d = array(
                                "v_name" => $v_name,
                                "v_email" => $v_email,
                                // "v_account" => $v_account,
                                "v_status" => 'Requested',
                                "is_deleted" => 0,
                                "c_id" => $post['c_id'],
                                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                                "form_id" => $form_id,
                                "created_at" => date('Y-m-d h:i:s'),
                            );
                            $email = $v_email;
                            $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"><div class="brand"><img src="' . iaBase() . 'assets/images/WageFilingw9-Logo-XSmall.png" alt="W-9\'s.com W-9"><span class="subbrand"><i>Powered by</i><a href="' . base_url() . '"> W-9\'s.com</a></span></div><div class="sender"><h1><i>RS W-9 Request from W-9\'s.com</i></h1></div></div></div><div class="container"><p>Dear ' . $v_name . ',</p><p>Comapny is requesting an IRS form W-9 from you. This form is required in order to receive payments from them. </p><p><a href="' . base_url('enter') . '/' . $form_id . '" class="go">Fill out your W-9</a></p>If you have questions regarding this request, please contact Comapny at <a href="mailto:mailto:' . $c_email . '">' . $c_email . '</a>. Comapny has partnered with W-9\'s.com to provide paper-free, online W-9 and 1099 forms.<p>Best Regards,</p><p>The Team at Comapny<br><br><a href="mailto:' . $c_email . '">' . $c_email . '</a></p></div></div></div>';

                            if ($setting['mail_setting'] == 'php_mailer') {
                                $this->load->library("send_mail");
                                $emm = $this->send_mail->email($sub, $body, $email, $setting);
                            } else {
                                // content-type is required when sending HTML email
                                $headers = "MIME-Version: 1.0" . "\r\n";
                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                                $emm = mail($email, $sub, $body, $headers);
                            }
                            $this->Frontend_model->insertRow('ia_vendors', $d);
                        }
                    }
                    $row++;
                } else {
                    echo json_encode(array('status' => 'fail', 'message' => 'You`re using a different CSV template.'));
                    return false;
                }
            }
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'fail', 'message' => 'No companies found to import'));
            return false;
        }
    }
    public function download()
    {
        $this->load->library('zip');
        $post = $this->input->post();
        $data = array();
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $Files = $this->Frontend_model->getData('Files', $CustID, 'CustID');
        $this->zip->clear_data();
        if ($post['type'] == 'pdf') {
            $zip = 0;
            foreach ($Files as $key => $ia_company) {
                $vendors = $this->Frontend_model->getData('ia_vendors', $ia_company->c_id, 'c_id');
                foreach ($vendors as $key => $vendor) {
                    if (!empty($vendor->pdf) && $vendor->v_status == 'Signed') {
                        copy("./pdf/$vendor->form_id.pdf", "./pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                        $zip = $this->zip->read_file(FCPATH . "/pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                        unlink(FCPATH . "/pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                    }
                }
            }
            if ($zip > 0) {
                $this->zip->archive(FCPATH . 'pdf/w9-form.zip');
                $data['status'] = 'success';
                $data['path'] = 'pdf/w9-form.zip';
            } else {
                $data['status'] = 'fail';
            }
        }

        if ($post['type'] == 'csv') {
            $zip = 0;
            foreach ($Files as $key => $ia_company) {
                $this->db->select('v_name AS `Name`, business_classification AS `Tax Classification`, business_name AS `Business Name`,  address AS `Street`, city AS `City`, state AS `State`, zip AS `Zip Code`, v_email AS `Email`, v_account AS `Account Number`,  foreign_address AS `Foreign City, Province/State, Postal Code, Country`,  DATE_FORMAT(created_at, "%Y%m%d") AS `Date signed (YYYYMMDD)`', false);
                $this->db->where('c_id', $ia_company->c_id);
                $this->db->where('is_deleted', 0);
                $quer = $this->db->get('ia_vendors');
                if (count($quer->result()) > 0) {
                    query_to_csv($quer, true, FCPATH . "/assets/csv/all-W-9s-$ia_company->c_id-$ia_company->c_name.csv");
                    $zip = $this->zip->read_file(FCPATH . "/assets/csv/all-W-9s-$ia_company->c_id-$ia_company->c_name.csv");
                    unlink(FCPATH . "/assets/csv/all-W-9s-$ia_company->c_id-$ia_company->c_name.csv");

                    if ($zip > 0) {
                        $this->zip->archive(FCPATH . '/assets/csv/w9-form.zip');
                        $data['status'] = 'success';
                        $data['path'] = '/assets/csv/w9-form.zip';
                    } else {
                        $data['status'] = 'fail';
                    }
                }
            }
        }

        echo json_encode($data);
    }

    public function vendordownload()
    {
        $post = $this->input->post();
        $this->load->library('zip');
        $data = array();
        $ia_company = $this->Frontend_model->getData('Files', $post['c_id'], 'c_id');
        $c_id = $ia_company[0]->c_id;
        $c_name = $ia_company[0]->c_name;
        if ($post['type'] == 'csv') {
            $this->db->select('v_name AS `Name`, business_classification AS `Tax Classification`, business_name AS `Business Name`,  address AS `Street`, city AS `City`, state AS `State`, zip AS `Zip Code`, v_email AS `Email`, v_account AS `Account Number`,  foreign_address AS `Foreign City, Province/State, Postal Code, Country`,  DATE_FORMAT(created_at, "%Y%m%d") AS `Date signed (YYYYMMDD)`', false);
            $this->db->where('c_id', $post['c_id']);
            $this->db->where('is_deleted', 0);
            $quer = $this->db->get('ia_vendors');
            if (count($quer->result()) > 0) {
                query_to_csv($quer, true, FCPATH . "/assets/csv/company/W-9s-$c_id-$c_name.csv");
            }
            $data['status'] = 'success';
            $data['path'] = "/assets/csv/company/W-9s-$c_id-$c_name.csv";
            $data['name'] = "W-9s-$c_id-$c_name.csv";
        } else if ($post['type'] == 'spdf') {
            $this->zip->clear_data();
            $zip = 0;
            $vendors = $this->Frontend_model->getData('ia_vendors', $c_id, 'c_id');
            foreach ($vendors as $key => $vendor) {
                if (!empty($vendor->pdf) && $vendor->v_status == 'Signed') {
                    copy("./pdf/$vendor->form_id.pdf", "./pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                    $zip = $this->zip->read_file(FCPATH . "/pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                    unlink(FCPATH . "/pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                }
            }

            if ($zip > 0) {
                $this->zip->archive(FCPATH . "pdf/$c_name.zip");
                $data['status'] = 'success';
                $data['path'] = "pdf/$c_name.zip";
                $data['name'] = "$c_name.zip";

            } else {
                $data['status'] = 'fail';
            }
        } else if ($post['type'] == 'resend') {
            $c_id = $post['c_id'];
            $setting = settings();
            $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'c_id');
            $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
            $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
            $sub = $c_name . ' W-9 Request';

            $vendors = $this->Frontend_model->getData('ia_vendors', $c_id, 'c_id');
            foreach ($vendors as $key => $vendor) {
                if ($vendor->v_status == 'Requested') {

                    if (!empty($vendor->v_email)) {
                        $form_id = md5(uniqid(rand(), true));

                        $d = array(
                            "v_status" => 'Requested',
                            "is_deleted" => 0,
                            "c_id" => $c_id,
                            "CustID" => $this->session->userdata('user_details')[0]->CustID,
                            "form_id" => $form_id,
                            "pdf" => '',
                            "created_at" => date('Y-m-d h:i:s'),
                        );

                        $email = $vendor->v_email;
                        $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"><div class="brand"><img src="' . iaBase() . 'assets/images/WageFilingw9-Logo-XSmall.png" alt="W-9\'s.com W-9"><span class="subbrand"><i>Powered by</i><a href="' . base_url() . '"> W-9\'s.com</a></span></div><div class="sender"><h1><i>RS W-9 Request from W-9\'s.com</i></h1></div></div></div><div class="container"><p>Dear ' . $vendor->v_name . ',</p><p>Comapny is requesting an IRS form W-9 from you. This form is required in order to receive payments from them. </p><p><a href="' . base_url('enter') . '/' . $form_id . '" class="go">Fill out your W-9</a></p>If you have questions regarding this request, please contact Comapny at <a href="mailto:mailto:' . $c_email . '">' . $c_email . '</a>. Comapny has partnered with W-9\'s.com to provide paper-free, online W-9 and 1099 forms.<p>Best Regards,</p><p>The Team at Comapny<br><br><a href="mailto:' . $c_email . '">' . $c_email . '</a></p></div></div></div>';

                        if ($setting['mail_setting'] == 'php_mailer') {
                            $this->load->library("send_mail");
                            $emm = $this->send_mail->email($sub, $body, $email, $setting);
                        } else {
                            // content-type is required when sending HTML email
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                            $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                            $emm = mail($email, $sub, $body, $headers);
                        }
                        $this->Frontend_model->updateRow('ia_vendors', 'v_id', $vendor->v_id, $d);
                    }
                }
            }
            $data['status'] = 'success';
        }
        echo json_encode($data);
    }

    public function forgotpassword()
    {
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $setting = settings();
            $res = $this->Frontend_model->getDataBy('ia_users', $this->input->post('email'), 'email', 1);
            if (isset($res[0]->CustID) && $res[0]->CustID != '') {
                $var_key = $this->getVerificationCode();
                $this->Frontend_model->updateRow('ia_users', 'CustID', $res[0]->CustID, array('var_key' => $var_key));
                $sub = "Reset password";
                $email = $this->input->post('email');
                $data = array(
                    'user_name' => $res[0]->name,
                    'action_url' => base_url(),
                    'sender_name' => $setting['FileName'],
                    'website_name' => $setting['website'],
                    'varification_link' => base_url() . 'frontend/mailVerify?code=' . $var_key,
                    'url_link' => base_url() . 'frontend/mailVerify?code=' . $var_key,
                );
                $body = $this->Frontend_model->getTemplate('forgot_password');
                $body = $body->html;
                foreach ($data as $key => $value) {
                    $body = str_replace('{var_' . $key . '}', $value, $body);
                }
                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
                    // content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                if ($emm) {

                    $this->load->view('forgot-password', array('show' => 'msg'));

                }
            } else {

                $this->load->view('forgot-password', array('show' => 'msg'));

            }
        } else {
            $this->load->view('forgot-password');
        }
        $this->load->view("include/front/footer");
    }

    /**
     * This function is used to load view of reset password and varify user too
     * @return : void
     */
    public function mailVerify()
    {
        $return = $this->Frontend_model->mailVerify();

        if ($return) {
            $data['email'] = $return;
            $this->load->view('set-password', $data);
        } else {
            $data['email'] = 'allredyUsed';
            $this->load->view('set-password', $data);
        }
    }

    /**
     * This function is generate hash code for random string
     * @return string
     */
    public function getVerificationCode()
    {
        $pw = $this->randomString();
        return $varificat_key = password_hash($pw, PASSWORD_DEFAULT);
    }

    /**
     * This function is used to Generate a random string
     * @return String
     */
    public function randomString()
    {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = $alpha . $alpha_upper . $numeric;
        $pw = '';
        $chars = str_shuffle($chars);
        $pw = substr($chars, 8, 8);
        return $pw;
    }

    /**
     * This function is used to reset password in forget password process
     * @return : void
     */
    public function reset_password()
    {
        $return = $this->Frontend_model->ResetPpassword();
        if ($return) {
            redirect(base_url() . 'login', 'refresh');
        } else {
            redirect(base_url() . 'login', 'refresh');
        }
    }
}
