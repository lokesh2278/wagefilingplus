<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/application.css">
<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/blaze-entry.css">
<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/scss/style.css">
<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/home.css">
<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/signature-pad.css">
<style>
  span.logo-lg img{
    max-width: 165px;
  }

  #form-entry .btn:hover {
      color: #fff !important;
      background-color: #337ab7;
      font-size: 12px;
      font-weight: 700;
      text-transform: uppercase;
      letter-spacing: 1px;
      border: 2px solid #337ab7;
  }

   #form-entry .btn {
      color: #337ab7 !important;
      background-color: #fff;
      font-size: 12px;
      font-weight: 700;
      text-transform: uppercase;
      letter-spacing: 1px;
      border: 2px solid #337ab7;
  }

  .panel-primary>.panel-heading {
      color: #fff;
      background-color: #337ab7!important;
      border-color: #24a9bd!important;
  }
</style>
<div class="navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brands" href="<?php echo base_url(); ?>">
              <span class="logo-lg">
                <img src="<?php echo iaBase() . 'assets/images/wf-header-logo.png'; ?>" id="logo">
              </span>
            </a>
        </div>
        <div class="navbar-collapse collapse">

         </div><!--/.navbar-collapse -->
      </div>
    </div>


<script src="<?php echo iaBase(); ?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div id="app-container">
  <div>
    <div id="page" class="page">
  <div class="content-container">
<div id="entry-banner">
  <div class="container">
    <h1>
      <?php if ($this->uri->segment(3) == 'manually' || !empty($vendors)) {?>
      <?php $c_id = !empty($vendors[0]->c_id) ? $vendors[0]->c_id : $c_id;?>

      <span name="company_name"></span>
      <?php }?>
      <div class="e-request">Official W-9 Request<br>from <?=companyName($c_id)?></div>

    </h1>
  </div>
</div>
<?php $req_cid = get_request_id($vendors[0]->c_id);
$paid_status = get_paid_status($req_cid);
?>
<?php if ($this->uri->segment(3) != 'manually' && empty($vendors) || $paid_status == 1) {
    ?>
<div class="container">
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2 expired-message non-entry" style="">
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="panel-title">W-9 Request Expired</h3>
        </div>
        <div class="panel-body">

          <p>For security reasons, your request has expired. Please check your inbox to see if you have a newer email. If not, ask your Company to send the W-9 request again. Their information is in your email.</p>
          <p>Best,<br>The WageFilingPlus.com Team</p>
        </div>
      </div>
    </div>

  </div>
</div>

<?php } else {
    ?>

<div class="container">
  <div class="col-sm-12 col-md-10 col-md-offset-1 preview-panel non-entry" id="form-entry" style="display:none;">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Preview of W-9</h3>
        </div>
        <div class="panel-body">
          <div class="preview-wrap">
            <div class="image-layers">
              <div class="loading-message">
                <p><img src="<?=iaBase()?>assets/images/spinner-large.gif" alt=""></p>
                Generating<br>Preview...
              </div>
              <div class="image-container" id="image-container"></div>
            </div>
          </div>
          <p>
            <button type="button" class="btn btn-default btn-hot done-button"><i class="glyphicon glyphicon-download-alt"></i> Approve </button>
            <button type="button" class="btn btn-default btn-hot edit-button"> Edit </button>
            <a href="" id="download-preview" download></a>
          </p>
        </div>
      </div>
    </div>
  <div class="col-sm-8 col-sm-offset-2 success-message non-entry" style="display:none">
      <div class="panel panel-success">
        <div class="panel-heading">
          <h3 class="panel-title">You're Done!</h3>
        </div>
        <div class="panel-body">
          <p>A copy of your completed W-9 has been sent to <span name="company_name"><?=companyName($c_id)?></span>, no further action is required.</p>
          <p>Thank you!<span name="company_name"></span> </p>
          <!-- <p>WageFilingPlus.com, LLC</p> -->
          <p>Need to issue W-9's for your business?<a class="company_email" href="<?=base_url('issue-w9');?>"> Click Here <span name="company_name"></span></a> </p>
        </div>
        <div class="panel-footer">
          <button class="btn btn-success download-button"><i class="glyphicon glyphicon-download-alt"></i> Download Again</button>
          <!-- <p>Close this tab to logout.</b> -->
        </div>
      </div>
    </div>

  <form id="w9-form" action="" method="post">
  <div class="row entry-container needed-for-entry" >
  <?php if (!empty($vendors)) {
        ?>
    <div class="entry-left-col col-md-12">
      <div class="row">

        <div class="left-info col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">Welcome, <span name="display_name"><?php echo ucfirst(!empty($vendors[0]->v_name) ? $vendors[0]->v_name : ''); ?></span></h3>
            </div>
            <?php $user_id = !empty($vendors[0]->user_id) ? $vendors[0]->user_id : '';?>
            <div class="panel-body">
              <blockquote>

                <h3>Hello, <span name="display_name"><?php echo ucfirst(!empty($vendors[0]->v_name) ? $vendors[0]->v_name : ''); ?></span></h3>
              <p>On behalf of <span name="company_name"><?=companyName($c_id)?></span> we have been asked to issue a secure, electronic W-9 for you to complete and sign for their records. it only takes a moment and will save a copy for your records and update theirs as well.</p>

              <p>If you are not sure why you are receving the request please contact <a class="company_email" name="company_email" href="mailto:<?=companyEmail($c_id)?>"><?=companyEmail($c_id)?></a> or feel free to contact us.</p>
              <br>
              <p>WageFilingPlus.com, LLC</p>
              <p>Our entire process is secured with SHA-256 With RSA Encryption from start to finish.</p>
              <p style="margin-top:20px; margin-bottom:10px;">
               <span id="siteseal">
                <!-- <script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=tbTsSiEFpj8oLWLWjcupbIlY3G4NHIjA3mtBXuJD01GDUZGVT41Am3rRJSH0"></script> -->
                <img width="130px" src="<?php echo iaBase(); ?>assets/images/le-logo-wide.png"/>
              </span>
              </p>
             </blockquote>
            </div>
          </div>
        </div>


      </div>
    </div>
    <?php }?>
    <?php if (!empty($vendors)) {?>
    <div class="entry-right-col col-md-12">
        <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">IRS Form W-9</h3>
        </div>
      <?php } else {?>
    <div class="entry-right-col col-md-12 col-lg-12">
       <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Manually Enter W-9</h3>
        </div>
      <?php }?>

   <div class="panel-body panel-body-extra">
     <div class="form-container">

  <div class="row">
    <div class="col-sm-12" data-fields="name">
      <input id="c3_v_id" class="form-control" name="v_id" maxlength="40" type="hidden" value="<?php echo !empty($vendors[0]->v_id) ? $vendors[0]->v_id : ''; ?>">
      <input id="c3_c_id" class="form-control" name="c_id" maxlength="40" type="hidden" value="<?php echo !empty($c_id) ? $c_id : ''; ?>">


      <div class="form-group field-name">
          <label class="control-label" for="c3_name">
            <span class="label-inner">Name (as shown on your income tax return) (required field) <a class="help-link oneCommonClass" data-target="instructions-name" style="text-decoration:none;" href="#instructions-name"><i class="glyphicon glyphicon-question-sign"></i></a></span>
          </label>
                        <div data-editor="">
                          <input id="c3_name" class="form-control" name="name" maxlength="40" type="text" value="<?php echo !empty($vendors[0]->v_name) ? $vendors[0]->v_name : ''; ?>">
                        </div>
                      </div>
                    </div>
  </div>
  <div class="row">
    <div class="col-sm-12" data-fields="business_name">
      <div class="form-group field-business_name">
          <label class="control-label" for="c3_business_name">
            <span class="label-inner">Business name/disregarded entity name, if different from above</span>
          </label>
                        <div data-editor="">
                          <input id="c3_business_name" class="form-control" name="business_name" maxlength="40" type="text" value="<?php echo !empty($vendors[0]->business_name) ? $vendors[0]->business_name : ''; ?>">
                          <input type="hidden" id="form_id" value="<?php echo !empty($vendors[0]->form_id) ? $vendors[0]->form_id : ''; ?>" name="form_id">
                        </div>
                      </div>
                    </div>
  </div>
  <div class="row">
    <div class="col-sm-12" data-fields="business_classification">
      <div class="form-group field-business_classification">
          <label class="control-label" for="c3_business_classification">
            <span class="label-inner">Select one appropriate box for federal tax classification:</span>
            <input type="hidden" id="v_business_classification" value="<?php echo !empty($vendors[0]->business_classification) ? $vendors[0]->business_classification : ''; ?>">
          </label>
                        <div data-editor="">
                          <input type="checkbox" style="visibility: hidden; width: 0;" name="business_classification[]" class="ip_bc" required="required" value=""><br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="Individual"> Individual/sole proprietor or single member LLC<br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="C Corporation"> C Corporation<br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="S Corporation"> S Corporation<br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="Partnership"> Partnership<br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="Trust/estate"> Trust/estate<br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="LLC-C"> Limited liability company (C Corporation)<br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="LLC-S"> Limited liability company (S Corporation)<br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="LLC-P"> Limited liability company (Partnership)<br>
                          <input type="checkbox" name="business_classification[]" class="ip_bc" required="required" value="Other"> Other...<br>

                         <!--  <select id="c3_business_classification[]" class="form-control" name="business_classification">
                          <option value="">Choose...</option>
                          <option value="Individual">Individual/sole proprietor or single member LLC</option>
                          <option value="C Corporation">C Corporation</option>
                          <option value="S Corporation">S Corporation</option>
                          <option value="Partnership">Partnership</option>
                          <option value="Trust/estate">Trust/estate</option>
                          <option value="LLC-C">Limited liability company (C Corporation)</option>
                          <option value="LLC-S">Limited liability company (S Corporation)</option>
                          <option value="LLC-P">Limited liability company (Partnership)</option>
                          <option value="Other">Other...</option></select> -->


                        </div>
                      </div>
                    </div>
   <div class="col-sm-6" data-fields="business_other">
    <div class="form-group field-business_other" id="other_federal_tax" >
    </div>
  </div>
</div>
  <div class="exemptions panel-stretch">
<p><b>Exemptions</b> (codes apply only to certain entities, not individuals. See instr.): <a class="help-link oneCommonClass" data-target="instructions-Exemptions" style="text-decoration:none;" href="#instructions-Exemptions"><i class="glyphicon glyphicon-question-sign"></i></a></p>
    <div class="row">
      <div class="col-sm-5" data-fields="exempt_payee_code">
        <div class="form-group field-exempt_payee_code">
            <label class="control-label" for="c3_exempt_payee_code">
              <span class="label-inner">Exempt payee code (if any) <a class="help-link oneCommonClass" data-target="instructions-exempt_payee_code" style="text-decoration:none;" href="#instructions-exempt_payee_code"><i class="glyphicon glyphicon-question-sign"></i></a></span>
            </label>
                          <div data-editor="">
                            <input id="c3_exempt_payee_code" class="form-control" name="exempt_payee_code" maxlength="2" type="text" value="<?php echo !empty($vendors[0]->exempt_payee_code) ? $vendors[0]->exempt_payee_code : ''; ?>">
                          </div>
                        </div>
                      </div>
      <div class="col-sm-7" data-fields="exempt_fatca_code">
        <div class="form-group field-exempt_fatca_code">
            <label class="control-label" for="c3_exempt_fatca_code">
              <span class="label-inner">Exemption from FATCA reporting code (if any) <a class="help-link oneCommonClass" data-target="instructions-exempt_fatca_code" style="text-decoration:none;" href="#instructions-exempt_fatca_code"><i class="glyphicon glyphicon-question-sign"></i></a></span>
            </label>
                          <div data-editor="">
                            <input id="c3_exempt_fatca_code" class="form-control" name="exempt_fatca_code" maxlength="1" type="text" value="<?php echo !empty($vendors[0]->exempt_fatca_code) ? $vendors[0]->exempt_fatca_code : ''; ?>">
                          </div>
                        </div>
                      </div>
    </div>
  </div>
  <div class="row">
    <?php $foreign_address = !empty($vendors[0]->foreign_address) ? $vendors[0]->foreign_address : '';?>
    <div class="col-sm-6" data-fields="foreign_country_indicator">
      <div class="checkbox solo">
  <label for="c3_foreign_country_indicator">
    <span data-editor="">
      <input id="c3_foreign_country_indicator" name="foreign_country_indicator" type="checkbox" <?php if (!empty($foreign_address)) {echo 'checked';}?>>
    </span> Check for foreign address</label>
</div>
</div>
  </div>
  <div class="row">
    <div class="col-sm-12" data-fields="address">
      <div class="form-group field-address">
          <label class="control-label" for="c3_address">
            <span class="label-inner">Address</span>
          </label>
                        <div data-editor="">
                          <input id="c3_address" class="form-control" name="address" maxlength="40" type="text" value="<?php echo !empty($vendors[0]->address) ? $vendors[0]->address : ''; ?>">
                        </div>
                      </div>
                    </div>
  </div>
  <div class="row" <?php echo empty($foreign_address) ? 'style="display:none;"' : 'style="display:block;"' ?> id="foreign_address">
    <div class="col-sm-12" data-fields="foreign_address">
      <div class="form-group field-foreign_address">
        <label class="control-label" for="c3_foreign_address">
          <span class="label-inner">City, Province/State, Postal Code, Country</span>
        </label>
        <div data-editor="">
            <input id="c3_foreign_address" class="form-control" name="foreign_address" type="text" value="<?php echo !empty($vendors[0]->foreign_address) ? $vendors[0]->foreign_address : ''; ?>">
        </div>
      </div>
    </div>
  </div>
  <div class="row" <?php echo empty($foreign_address) ? 'style="display:block;"' : 'style="display:none;"' ?> id="all_address_form">

  </div>
  <div class="row">
    <div class="col-sm-12" data-fields="foreign_address"><!-- placeholder --></div>
  </div>
  <div class="row">
    <div class="col-sm-9" data-fields="email">
      <div class="form-group field-email">
          <label class="control-label" for="c3_email">
            <span class="label-inner">Email</span>
          </label>
                        <div data-editor="">
                          <input id="c3_email" class="form-control" name="email" type="text" value="<?php echo !empty($vendors[0]->v_email) ? $vendors[0]->v_email : ''; ?>">
                        </div>
                      </div>
                    </div>

  </div>
  <div class="row">
    <div class="col-sm-9" data-fields="account_number">
      <div class="form-group field-account_number">
          <label class="control-label" for="c3_account_number">
            <span class="label-inner">List Account number(s) here (optional)</span>
          </label>
                        <div data-editor="">
                          <input id="c3_account_number" class="form-control" name="account_number" type="text" value="<?php echo !empty($vendors[0]->v_account) ? $vendors[0]->v_account : ''; ?>">
                        </div>
                      </div>
                    </div>
  </div>
  <h3>Part I. Taxpayer Identification Number (TIN) <a class="help-link oneCommonClass" data-target="instructions-TIN" style="text-decoration:none;" href="#instructions-TIN"><i class="glyphicon glyphicon-question-sign"></i></a></h3>
  <p><strong>Fill in either the SSN or the EIN</strong></p>
  <div class="row">
  <?php if (!empty($vendors)) {?>
    <div class="col-lg-8">
      <p>Enter your TIN in the appropriate box.  The TIN provided must match the name given on line 1 to avoid backup withholding.  For individuals, this is generally your social security number (SSN). However, for a resident alien, sole proprietor, or disregarded entity, see the Part I instructions. For other entities, it is your employer identification number (EIN). If you do not have a number, see How to get a TIN below.</p>
      <p>Note. If the account is in more than one name, see the instructions for line 1 and the chart below for guidelines on whose number to enter.</p>
    </div>
    <div class="col-lg-4">
    <?php } else {?>
    <div class="col-lg-12">
    <?php }?>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-12">
          <div data-fields="v_ssn">
            <div class="form-group field-v_ssn">
                <label class="control-label" for="c3_v_ssn">
                  <span class="label-inner">Social Security Number</span>
                </label>
                              <div data-editor="">
                                <input id="c3_v_ssn" class="form-control group" name="v_ssn" autocomplete="" type="text" value="<?php echo !empty($vendors[0]->v_ssn) ? $vendors[0]->v_ssn : ''; ?>">
                              </div>
                            </div>
                          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-12">
          <div data-fields="v_ein">
            <div class="form-group field-v_ein">
                <label class="control-label" for="c3_v_ein">
                  <span class="label-inner">Employer Identification Number</span>
                </label>
                              <div data-editor="">
                                <input id="c3_v_ein" class="form-control group" name="v_ein" type="text" value="<?php echo !empty($vendors[0]->v_ein) ? $vendors[0]->v_ein : ''; ?>">
                              </div>
                            </div>
                          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if (!empty($vendors)) {?>
  <h3>Part II. Certification <a class="help-link oneCommonClass" data-target="instructions-Certification" style="text-decoration:none;" href="#instructions-Certification"><i class="glyphicon glyphicon-question-sign"></i></a></h3>
    <p>Under penalties of perjury, I certify that:</p>
  <ol>
    <li>The number shown on this form is my correct taxpayer identification number (or I am waiting for a number to be issued to me); and</li>
    <li class="no_backup_withholding">I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal Revenue Service (IRS) that I am subject to backup withholding as a result of a failure to report all interest or dividends, or (c) the IRS has notified me that I am no longer subject to backup withholding; and</li>
    <li>I am a U.S. citizen or other U.S. person (defined below); and</li>
    <li>The FATCA code(s) entered on this form (if any) indicating that I am exempt from FATCA reporting is correct.</li>
  </ol>
  <p><b>Certification instructions.</b>
  You must cross out item 2 above if you have been notified by the IRS that you are currently subject to backup withholding because you have failed to report all interest and dividends on your tax return. For real estate transactions, item 2 does not apply. For mortgage interest paid, acquisition or abandonment of secured property, cancellation of debt, contributions to an individual retirement arrangement (IRA), and generally, payments other than interest and dividends, you are not required to sign the certification, but you must provide your correct TIN. See the instructions.</p>
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 col-xs-12" data-fields="no_backup_withholding"><div class="checkbox solo">
  <label for="c3_no_backup_withholding">
    <span data-editor="">
    <input id="c3_no_backup_withholding" name="no_backup_withholding" type="checkbox" checked="checked">
  </span> I am not subject to backup withholding.</label>
  </div>
</div>
  </div>

  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 col-xs-12" data-fields="signature">
      <div class="form-group field-signature">
          <label class="control-label" for="c3_signature">
            <span class="label-inner">Signature of US Person</span>
          </label>

      <div data-editor="">

      <div id="signature-pad" class="signature-pad">
        <button class="clear-signature btn btn-sm btn-warning" type="button" id="btn_clear">Clear</button>
      <div class="signature-pad--body">
        <canvas width="664" height="171" style="touch-action: none;"></canvas>
      </div>
    </div>
      <input data-rule-signature="true" id="signature" name="signature" type="hidden">
     </div>
    </div>
  </div>
  </div>
<?php }?>
</div>
</div>
        <div class="panel-footer">
          <input type="submit" class="btn btn-lg btn-primary save-button" value="Continue"/>
        </div>
      </div>
    </div>
  </div>
  </form>
 <div id="c38_business_other" style="display:none">
     <label class="control-label" for="c38_business_other">
        <span class="label-inner">Other</span>
      </label>
      <div data-editor="">
        <input id="c38_business_other" class="form-control" name="business_other" type="text"
         value="<?php echo !empty($vendors[0]->other) ? $vendors[0]->other : ''; ?>"></div>
   </div>

<div id="all_address" style="display:none">
    <div class="col-sm-5" data-fields="city">
      <div class="form-group field-city">
          <label class="control-label" for="c3_city">
            <span class="label-inner">City</span>
          </label>
                        <div data-editor="">
                          <input id="c3_city" class="form-control" name="city" maxlength="40" type="text" value="<?php echo !empty($vendors[0]->city) ? $vendors[0]->city : ''; ?>">
                        </div>
                      </div>
                    </div>
    <div class="col-sm-4" data-fields="state">
      <div class="form-group field-state">
          <label class="control-label" for="c3_state">
            <span class="label-inner">State</span>
            <input id="v_state" type="hidden" value="<?php echo !empty($vendors[0]->state) ? $vendors[0]->state : ''; ?>">
          </label>
                        <div data-editor="">
                          <select id="c3_state" class="form-control" name="state">
                          <option value="">Choose...</option>
                          <option value="AA">AA - Armed Forces Americas</option>
                          <option value="AE">AE - Armed Forces Europe</option>
                          <option value="AK">AK - Alaska</option>
                          <option value="AL">AL - Alabama</option>
                          <option value="AP">AP - Armed Forces Pacific</option>
                          <option value="AR">AR - Arkansas</option>
                          <option value="AS">AS - American Samoa</option>
                          <option value="AZ">AZ - Arizona</option>
                          <option value="CA">CA - California</option>
                          <option value="CO">CO - Colorado</option>
                          <option value="CT">CT - Connecticut</option>
                          <option value="DC">DC - District of Columbia</option>
                          <option value="DE">DE - Delaware</option>
                          <option value="FL">FL - Florida</option>
                          <option value="FM">FM - Federated Micronesia</option>
                          <option value="GA">GA - Georgia</option>
                          <option value="GU">GU - Guam</option>
                          <option value="HI">HI - Hawaii</option>
                          <option value="IA">IA - Iowa</option>
                          <option value="ID">ID - Idaho</option>
                          <option value="IL">IL - Illinois</option>
                          <option value="IN">IN - Indiana</option>
                          <option value="KS">KS - Kansas</option>
                          <option value="KY">KY - Kentucky</option>
                          <option value="LA">LA - Louisiana</option>
                          <option value="MA">MA - Massachusetts</option>
                          <option value="MD">MD - Maryland</option>
                          <option value="ME">ME - Maine</option>
                          <option value="MH">MH - Marshall Islands</option>
                          <option value="MI">MI - Michigan</option>
                          <option value="MN">MN - Minnesota</option>
                          <option value="MO">MO - Missouri</option>
                          <option value="MP">MP - N. Mariana Islands</option>
                          <option value="MS">MS - Mississippi</option>
                          <option value="MT">MT - Montana</option>
                          <option value="NC">NC - North Carolina</option>
                          <option value="ND">ND - North Dakota</option>
                          <option value="NE">NE - Nebraska</option>
                          <option value="NH">NH - New Hampshire</option>
                          <option value="NJ">NJ - New Jersey</option>
                          <option value="NM">NM - New Mexico</option>
                          <option value="NV">NV - Nevada</option>
                          <option value="NY">NY - New York</option>
                          <option value="OH">OH - Ohio</option>
                          <option value="OK">OK - Oklahoma</option>
                          <option value="OR">OR - Oregon</option>
                          <option value="PA">PA - Pennsylvania</option>
                          <option value="PR">PR - Puerto Rico</option>
                          <option value="PW">PW - Palau</option>
                          <option value="RI">RI - Rhode Island</option>
                          <option value="SC">SC - South Carolina</option>
                          <option value="SD">SD - South Dakota</option>
                          <option value="TN">TN - Tennessee</option>
                          <option value="TX">TX - Texas</option>
                          <option value="UT">UT - Utah</option>
                          <option value="VA">VA - Virginia</option>
                          <option value="VI">VI - US Virgin Islands</option>
                          <option value="VT">VT - Vermont</option>
                          <option value="WA">WA - Washington</option>
                          <option value="WI">WI - Wisconsin</option>
                          <option value="WV">WV - West Virginia</option>
                          <option value="WY">WY - Wyoming</option></select>
                        </div>
                      </div>
                    </div>
    <div class="col-sm-3" data-fields="zip">
      <div class="form-group field-zip">
          <label class="control-label" for="c3_zip">
            <span class="label-inner">Zip code</span>
          </label>
                        <div data-editor="">
                          <input id="c3_zip" class="form-control" name="zip" maxlength="10" type="text" value="<?php echo !empty($vendors[0]->zip) ? $vendors[0]->zip : ''; ?>">
                        </div>
                      </div>
                    </div>
  </div>
</div>

<hr class="page-divide needed-for-entry">


        <?php }?>
        </div>
</div>
</div>
</div>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
         <h2 class="modal-title" id="exampleModalLabel" style="float: left;">W-9 Instructions</h2>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
      </div>
      <div class="container needed-for-entry">
        <div class="row">
          <div class="col-lg-9">
            <div class="">
              <!-- <div class="panel-heading">
                <h3 class="panel-title">W-9 Instructions</h3>
              </div> -->
              <div class="panel-body panel-body-extra">
                <div class="instructions-container"><div class="instructions">
              <h2>General Instructions</h2>
              <p>Section references are to the Internal Revenue Code unless otherwise noted.</p>
              <p>
              Future developments. The IRS has created a page on IRS.gov for information about Form W-9, at www.irs.gov/w9. Information about developments affecting Form W-9 (such as legislation enacted after we release it) is at www.irs.gov/fw9.
              </p>

              <h3>Purpose of Form</h3>
              <p>An individual or entity (Form W-9 requester) who is required to file an information return with the IRS must obtain your correct taxpayer identification number (TIN) which may be your social security number (SSN), individual taxpayer identification number (ITIN), adoption taxpayer identification number (ATIN), or employer identification number (EIN), to report on an information return the amount paid to you, or other amount reportable on an information return. Examples of information returns include, but are not limited to, the following:
              </p>
              <ul>
                <li>Form 1099-INT (interest earned or paid)</li>
                <li>Form 1099-DIV (dividends, including those from stocks or mutual funds)</li>
                <li>Form 1099-MISC (various types of income, prizes, awards, or gross proceeds)</li>
                <li>Form 1099-B (stock or mutual fund sales and certain other transactions by brokers)</li>
                <li>Form 1099-S (proceeds from real estate transactions)</li>
                <li>Form 1099-K (merchant card and third party network transactions)</li>
                <li>Form 1098 (home mortgage interest), 1098-E (student loan interest), 1098-T
                (tuition)</li>
                <li>Form 1099-S (proceeds from real estate transactions)</li>
                <li>Form 1099-C (canceled debt)</li>
                <li>Form 1099-A (acquisition or abandonment of secured property)</li>
              </ul>

              <p>
              Use Form W-9 only if you are a U.S. person (including a resident alien), to provide your correct TIN.
              </p>
              <p>
              If you do not return Form W-9 to the requester with a TIN, you might be subject to backup withholding. See What is backup withholding? below.
              </p>
              <p>
              By signing the filled-out form, you:
              </p>

              <ol>
              <li>Certify that the TIN you are giving is correct (or you are waiting for a number to be issued),</li>
              <li>Certify that you are not subject to backup withholding, or</li>
              <li>Claim exemption from backup withholding if you are a U.S. exempt payee. If applicable, you are also certifying that as a U.S. person, your allocable share of any partnership income from a U.S. trade or business is not subject to the withholding tax on foreign partners' share of effectively connected income, and</li>
              <li>Certify that FATCA code(s) entered on this form (if any) indicating that you are exempt from the FATCA reporting, is correct. See What is FATCA reporting? below for further information.</li>
              </ol>

              <p>
              Note. If you are a U.S. person and a requester gives you a form other than Form W-9 to request your TIN, you must use the requester’s form if it is substantially similar to this Form W-9.
              </p>

              <p>
              <strong>Definition of a U.S. person.</strong> For federal tax purposes, you are considered a U.S. person if you are:
              </p>
              <p>UL tags are used to produce bulleted lists. This is applicable to:</p>
              <ul>
              <li>An individual who is a U.S. citizen or U.S. resident alien,</li>
              <li>A partnership, corporation, company, or association created or organized in the United States or under the laws of the United States,</li>
              <li>An estate (other than a foreign estate), or</li>
              <li>A domestic trust (as defined in Regulations section 301.7701-7).</li>
              </ul>

              <p>
              <strong>Special rules for partnerships.</strong> Partnerships that conduct a trade or business in the United States are generally required to pay a withholding tax under section 1446 on any foreign partners’ share of effectively connected taxable income from such business. Further, in certain cases where a Form W-9 has not been received, the rules under section 1446 require a partnership to presume that a partner is a foreign person, and pay the section 1446 withholding tax. Therefore, if you are a U.S. person that is a partner in a partnership conducting a trade or business in the United States, provide Form W-9 to the partnership to establish your U.S. status and avoid section 1446 withholding on your share of partnership income.
              </p>
              <p>
              In the cases below, the following person must give Form W-9 to the partnership for purposes of establishing its U.S. status and avoiding withholding on its allocable share of net income from the partnership conducting a trade or business in the United States:
              </p>
              <ul>
              <li>In the case of a disregarded entity with a U.S. owner, the U.S. owner of the disregarded entity and not the entity,</li>
              <li>In the case of a grantor trust with a U.S. grantor or other U.S. owner, generally, the U.S. grantor or other U.S. owner of the grantor trust and not the trust, and</li>
              <li>In the case of a U.S. trust (other than a grantor trust), the U.S. trust (other than a grantor trust) and not the beneficiaries of the trust.</li>
              </ul>
              <p>
              <strong>Foreign person.</strong> If you are a foreign person or the U.S. branch of a foreign bank that has elected to be treated as a U.S. person, do not use Form W-9. Instead, use the appropriate Form W-8 or Form 8233 (see Publication 515, Withholding of Tax on Nonresident Aliens and Foreign Entities).
              </p>

              <strong>Nonresident alien who becomes a resident alien.</strong> Generally, only a nonresident alien individual may use the terms of a tax treaty to reduce or eliminate U.S. tax on certain types of income. However, most tax treaties contain a provision known as a “saving clause.” Exceptions specified in the saving clause may permit an exemption from tax to continue for certain types of income even after the payee has otherwise become a U.S. resident alien for tax purposes.
              <p></p>
              <p>
              If you are a U.S. resident alien who is relying on an exception contained in the saving clause of a tax treaty to claim an exemption from U.S. tax on certain types of income, you must attach a statement to Form W-9 that specifies the following five items:
              </p>

              <ol>
              <li>The treaty country. Generally, this must be the same treaty under which you claimed exemption from tax as a nonresident alien.</li>
              <li>The treaty article addressing the income.</li>
              <li>The article number (or location) in the tax treaty that contains the saving clause and its exceptions.</li>
              <li>The type and amount of income that qualifies for the exemption from tax.</li>
              <li>Sufficient facts to justify the exemption from tax under the terms of the treaty article.</li>
              </ol>

              <p>
              <strong>Example.</strong> Article 20 of the U.S.-China income tax treaty allows an exemption from tax for scholarship income received by a Chinese student temporarily present in the United States. Under U.S. law, this student will become a resident alien for tax purposes if his or her stay in the United States exceeds 5 calendar years. However, paragraph 2 of the first Protocol to the U.S.-China treaty (dated April 30, 1984) allows the provisions of Article 20 to continue to apply even after the Chinese student becomes a resident alien of the United States. A Chinese student who qualifies for this exception (under paragraph 2 of the first protocol) and is relying on this exception to claim an exemption from tax on his or her scholarship or fellowship income would attach to Form W-9 a statement that includes the information described above to support that exemption.
              </p>
              <p>
              If you are a nonresident alien or a foreign entity, give the requester the appropriate completed Form W-8 or Form 8233.
              </p>
              <p><strong>Backup Withholding</strong></p>
              <p>
              <strong>What is backup withholding?</strong> Persons making certain payments to you must under certain conditions withhold and pay to the IRS 28% of such payments. This is called “backup withholding.” Payments that may be subject to backup withholding include interest, tax-exempt interest, dividends, broker and barter exchange transactions, rents, royalties, nonemployee pay, payments made in settlement of payment card and third party network transactions, and certain payments from fishing boat operators. Real estate transactions are not subject to backup withholding.
              </p>
              <p>You will not be subject to backup withholding on payments you receive if you give the requester your correct TIN, make the proper certifications, and report all your taxable interest and dividends on your tax return.
              </p>

              <div class="panel-stretch" id="instructions-backupwithholding">
              <h3>Payments you receive will be subject to backup withholding if:</h3>
              <ol>
              <li>You do not furnish your TIN to the requester,</li>
              <li>You do not certify your TIN when required (see the Part II instructions below for details),</li>
              <li>The IRS tells the requester that you furnished an incorrect TIN,</li>
              <li>The IRS tells you that you are subject to backup withholding because you did not report all your interest and dividends on your tax return (for reportable interest and dividends only), or</li>
              <li>You do not certify to the requester that you are not subject to backup withholding under 4 above (for reportable interest and dividend accounts opened after 1983 only).</li>
              </ol>

              <p>
              Certain payees and payments are exempt from backup withholding. See Exempt payee code below and the separate Instructions for the Requester of Form W-9 for more information.
              </p>
              <p>
              Also see Special rules for partnerships above.
              </p>
              <p>
              <strong>What is FATCA reporting?</strong> The Foreign Account Tax Compliance Act (FATCA) requires a participating foreign financial institution to report all United States account holders that are specified United States persons. Certain payees are exempt from FATCA reporting. See Exemption from FATCA reporting code on page 3 and the Instructions for the Requester of Form W-9 for more information.
              </p>

              </div>

              <div class="panel-stretch" id="instructions-updates">
              <h3>Updating Your Information</h3>
              <p>
              You must provide updated information to any person to whom you claimed to be an exempt payee if you are no longer an exempt payee and anticipate receiving reportable payments in the future from this person. For example, you may need to provide updated information if you are a C corporation that elects to be an S corporation, or if you no longer are tax exempt. In addition, you must furnish a new Form W-9 if the name or TIN changes for the account, for example, if the grantor of a grantor trust dies.
              </p>

              </div>

              <div class="panel-stretch" id="instructions-penalties">
              <h3>Penalties</h3>

              <p>
              <strong>Failure to furnish TIN.</strong> If you fail to furnish your correct TIN to a requester, you are subject to a penalty of $50 for each such failure unless your failure is due to reasonable cause and not to willful neglect.
              </p>
              <p>
              <strong>Civil penalty for false information with respect to withholding.</strong> If you make a false statement with no reasonable basis that results in no backup withholding, you are subject to a $500 penalty.
              </p>
              <p>
              <strong>Criminal penalty for falsifying information.</strong> Willfully falsifying certifications or affirmations may subject you to criminal penalties including fines and/or imprisonment.
              </p>
              <p>
              <strong>Misuse of TINs.</strong> If the requester discloses or uses TINs in violation of federal law, the requester may be subject to civil and criminal penalties.
              </p>
              </div>

              <h2>Specific Instructions</h2>

              <div class="panel-stretch" id="instructions-name">
              <h3>Line 1: Name</h3>
              <p>
              You must enter one of the following on this line; do not leave this line blank. The name should match the name on your tax return.
              </p>
              <p>
              If this Form W-9 is for a joint account, list first, and then circle, the name of the person or entity whose number you entered in Part I of Form W-9.
              </p>

              <p><strong>Individual.</strong> Generally, enter the name shown on your tax return. If you have changed your last name without informing the Social Security Administration (SSA) of the name change, enter your first name, the last name as shown on your social security card, and your new last name.
              </p>

              <p><strong>Note. ITIN applicant:</strong> Enter your individual name as it was entered on your Form W-7 application, line 1a. This should also be the same as the name you entered on the Form 1040/1040A/1040EZ you filed with your application.
              </p>

              <p>
              <b>Sole proprietor or single member LLC.</b> Enter your individual name as shown on your 1040/1040A/1040EZ on line 1. You may enter your business, trade, or “doing business as” (DBA) name on line 2.
              </p>

              <p>
              <b>Partnership, LLC that is not a single-member LLC, C Corporation, or S Corporation.</b> Enter the entity's name as shown on the entity's tax return on line 1 and any business, trade, or DBA name on line 2.
              </p>
              <b>Other entities.</b> Enter your name as shown on required U.S. federal tax documents on line 1. This name should match the name shown on the charter or other legal document creating the entity. You may enter any business, trade, or DBA name on line 2.
              <p></p>
              <p>
              <b>Disregarded entity.</b> For U.S. federal tax purposes, an entity that is disregarded as an entity separate from its owner is treated as a “disregarded entity.” See Regulations section 301.7701-2(c)(2)(iii). Enter the owner's name on line 1. The name of the entity entered on line 1 should never be a disregarded entity. The name on line 1 should be the name shown on the income tax return on which the income should be reported. For example, if a foreign LLC that is treated as a disregarded entity for U.S. federal tax purposes has a single owner that is a U.S. person, the U.S. owner's name is required to be provided on line 1. If the direct owner of the entity is also a disregarded entity, enter the first owner that is not disregarded for federal tax purposes. Enter the disregarded entity's name on line 2, “Business name/disregarded entity name.” If the owner of the disregarded entity is a foreign person, the owner must complete an appropriate Form W-8 instead of a Form W-9. This is the case even if the foreign person has a U.S. TIN.
              </p>
              <p><strong>Line 2</strong></p>
              <p>
              If you have a business name, trade name, DBA name, or disregarded entity name, you may enter it on line 2.
              </p>
              <p><strong>Line 3</strong></p>
              <p>
              Check the appropriate box in line 3 for the U.S. federal tax classification of the person whose name is entered on line 1. Check only one box in line 3.
              </p>
              <p><strong>Limited Liability Company (LLC)</strong> If the name on line 1 is an LLC treated as a partnership for U.S. federal tax purposes, check the “Limited Liability Company” box and enter “P” in the space provided. If the LLC has filed Form 8832 or 2553 to be taxed as a corporation, check the “Limited Liability Company” box and in the space provided enter “C” for C corporation or “S” for S corporation. If it is a single-member LLC that is a disregarded entity, do not check the “Limited Liability Company” box; instead check the first box in line 3 “Individual/sole proprietor or single-member LLC.”
              </p>

              </div>
              <div class="panel-stretch" id="instructions-Exemptions">
              <h3>Line 4, Exemptions</h3>
              <p>If you are exempt from backup withholding and/or FATCA reporting, enter in the appropriate space in line 4 any code(s) that may apply to you.
              </p>

              <div class="panel-stretch" id="instructions-exempt_payee_code">
              <p><strong>Exempt Payee Code</strong></p>

              <ul>
              <li>Generally, individuals (including sole proprietors) are not exempt from backup withholding.</li>
              <li>Except as provided below, corporations are exempt from backup withholding for certain payments, including interest and dividends.</li>
              <li>Corporations are not exempt from backup withholding for payments made in settlement of payment card or third party network transactions.</li>
              <li>Corporations are not exempt from backup withholding with respect to attorneys' fees or gross proceeds paid to attorneys, and corporations that provide medical or health care services are not exempt with respect to payments reportable on Form 1099-MISC.</li>
              </ul>

              <p>
              The following codes identify payees that are exempt from backup withholding. Enter the appropriate code in the space in line 4.
              </p>

              <p>
              1—An organization exempt from tax under section 501(a), any IRA, or a custodial account under section 403(b)(7) if the account satisfies the requirements of section 401(f)(2)
              </p>
              <p>2—The United States or any of its agencies or instrumentalities</p>
              <p>
              3—A state, the District of Columbia, a possession of the United States, or any of their political subdivisions or instrumentalities
              </p>
              <p>
              4—A foreign government or any of its political subdivisions, agencies, or instrumentalities
              </p>
              <p>
              5—A corporation
              </p>
              <p>
              6—A dealer in securities or commodities required to register in the United States, the District of Columbia, or a possession of the United States
              </p>
              <p>
              7—A futures commission merchant registered with the Commodity Futures Trading Commission
              </p>
              <p>
              8—A real estate investment trust
              </p>
              <p>
              9—An entity registered at all times during the tax year under the Investment Company Act of 1940
              </p>
              <p>
              10—A common trust fund operated by a bank under section 584(a)
              </p>
              <p>
              11—A financial institution
              </p>
              <p>
              12—A middleman known in the investment community as a nominee or custodian
              </p>
              <p>
              13—A trust exempt from tax under section 664 or described in section 4947
              </p>
              <p>
              The following chart shows types of payments that may be exempt from backup withholding. The chart applies to the exempt payees listed above, 1 through 13.
              </p>

              <table class="table table-striped">
              <thead>
                <tr>
                  <th>IF the payment is for...</th>
                  <th>THEN the payment is exempt for...</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Interest and dividend payments</td>
                  <td>All exempt payees except for 7</td>
                </tr>
                <tr>
                  <td>Broker transactions</td>
                  <td>Exempt payees 1 through 4 and 6 through 11 and all C corporations. S corporations must not enter an exempt payee code because they are exempt only for sales of noncovered securities acquired prior to 2012.</td>
                </tr>
                <tr>
                  <td>Barter exchange transactions and patronage dividends</td>
                  <td>Exempt payees 1 through 4</td>
                 </tr>
                 <tr>
                    <td>Payments over $600 required to be reported and direct sales over $5,000 (Note 1)</td>
                    <td>Generally, exempt payees 1 through 5 (Note 2)</td>
                  </tr>
                  <tr>
                    <td>Payments made in settlement of payment card or third party network transactions</td>
                    <td>Exempt payees 1 through 4</td>
                  </tr>

              </tbody>
              </table>

              <p>
              (Note 1) See Form 1099-MISC, Miscellaneous Income, and its instructions.
              </p>
              <p>
              (Note 2) However, the following payments made to a corporation and reportable on Form 1099-MISC are not exempt from backup withholding: medical and health care payments, attorneys' fees, gross proceeds paid to an attorney reportable under section 6045(f), and payments for services paid by a federal executive agency.
              </p>

              </div>

              <div class="panel-stretch" id="instructions-exempt_fatca_code">
              <p><strong>Exemption from FATCA Reporting Code</strong></p>

              <p>The following codes identify payees that are exempt from reporting under FATCA. These codes apply to persons submitting this form for accounts maintained outside of the United States by certain foreign financial institutions. Therefore, if you are only submitting this form for an account you hold in the United States, you may leave this field blank. Consult with the person requesting this form if you are uncertain if the financial institution is subject to these requirements. A requester may indicate that a code is not required by providing you with a Form W-9 with “Not Applicable” (or any similar indication) written or printed on the line for a FATCA exemption code.
              </p>
              <p>A—An organization exempt from tax under section 501(a) or any individual retirement plan as defined in section 7701(a)(37)
              </p>
              <p>
              B—The United States or any of its agencies or instrumentalities
              </p>
              <p>
              C—A state, the District of Columbia, a possession of the United States, or any of their political subdivisions or instrumentalities
              </p>
              <p>
              D—A corporation the stock of which is regularly traded on one or more established securities markets, as described in Reg. section 1.1472-1(c)(1)(i)
              </p>
              <p>
              E—A corporation that is a member of the same expanded affiliated group as a corporation described in Reg. section 1.1472-1(c)(1)(i)
              </p>
              <p>
              F—A dealer in securities, commodities, or derivative financial instruments (including notional principal contracts, futures, forwards, and options) that is registered as such under the laws of the United States or any state
              </p>
              <p>
              G—A real estate investment trust
              </p>
              <p>
              H—A regulated investment company as defined in section 851 or an entity registered at all times during the tax year under the Investment Company Act of 1940
              </p>
              <p>
              I—A common trust fund as defined in section 584(a)
              </p>
              <p>
              J—A bank as defined in section 581
              </p>
              <p>
              K—A broker
              </p>
              <p>
              L—A trust exempt from tax under section 664 or described in section 4947(a)(1)
              </p>
              <p>
              M—A tax exempt trust under a section 403(b) plan or section 457(g) plan
              </p>
              </div>
              </div>
              <p><strong>Note.</strong> You may wish to consult with the financial institution requesting this form to determine whether the FATCA code and/or exempt payee code should be completed.
              </p>
              <p><strong>Line 5</strong></p>
              <p>
              Enter your address (number, street, and apartment or suite number). This is where the requester of this Form W-9 will mail your information returns.
              </p>
              <p><strong>Line 6</strong></p>
              <p>
              Enter your city, state, and ZIP code.
              </p>


              <div class="panel-stretch" id="instructions-TIN">
              <h3>Part I. Taxpayer Identification Number (TIN)</h3>

              <p>Enter your TIN in the appropriate box. If you are a resident alien and you do not have and are not eligible to get an SSN, your TIN is your IRS individual taxpayer identification number (ITIN). Enter it in the social security number box. If you do not have an ITIN, see How to get a TIN below.
              </p>
              <p>
              If you are a sole proprietor and you have an EIN, you may enter either your SSN or EIN. However, the IRS prefers that you use your SSN.
              </p>
              <p>
              If you are a single-member LLC that is disregarded as an entity separate from its owner (see Limited Liability Company (LLC) on this page), enter the owner’s SSN (or EIN, if the owner has one). Do not enter the disregarded entity’s EIN. If the LLC is classified as a corporation or partnership, enter the entity’s EIN.
              </p>
              <p>
              Note. See the chart for further clarification of name and TIN combinations.
              </p>


              <h3>What Name and Number To Give the Requester</h3>

              <table class="table table-striped">
              <thead>
                <tr>
                  <th>For this type of account:</th>
                  <th>Give name and SSN of:</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1. Individual</td>
                  <td>The Individual</td>
                </tr>
                <tr>
                  <td>2. Two or more individuals (joint account)</td>
                  <td>The actual owner of the account or, if combined funds, the first individual on the account (Note 1)</td>
                </tr>
                <tr>
                  <td>3. Custodian account of a minor (Uniform Gift to Minors Act)</td>
                  <td>The Minor (Note 2)</td>
                 </tr>
                 <tr>
                    <td>4a. The usual revocable savings trust (grantor is also trustee)</td>
                    <td>The grantor-trustee (Note 1)</td>
                  </tr>
                  <tr>
                    <td>4b. So-called trust account that is not a legal or valid trust under state law</td>
                    <td>The actual owner (Note 1)</td>
                  </tr>
                  <tr>
                    <td>5. Sole proprietorship or disregarded entity owned by an individual</td>
                    <td> The owner (Note 3) </td>
                  </tr>
                  <tr>
                    <td>6. Grantor trust filing under Optional Form 1099 Filing Method 1 (see Regulation section 1.671-4(b)(2)(i)(A))</td>
                    <td>The grantor (Note *)</td>
                  </tr>

              </tbody>
              </table>

              <table class="table table-striped">
              <thead>
                <tr>
                  <th>For this type of account:</th>
                  <th>Give name and EIN of:</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>7. Disregarded entity not owned by an individual</td>
                  <td>The owner</td>
                </tr>
                <tr>
                  <td>8. A valid trust, estate, or pension trust</td>
                  <td>Legal entity (Note 4)</td>
                </tr>
                <tr>
                  <td>9. Corporation or LLC electing corporate status on Form 8832 or Form 2553</td>
                  <td>TThe corporation</td>
                 </tr>
                 <tr>
                    <td>10. Association, club, religious, charitable, educational, or other tax-exempt organization</td>
                    <td>The organization</td>
                  </tr>
                  <tr>
                    <td>11. Partnership or multi-member LLC</td>
                    <td>The partnership</td>
                  </tr>
                  <tr>
                    <td>12. A broker or registered nominee</td>
                    <td>The broker or nominee</td>
                  </tr>
                  <tr>
                    <td>13. Account with the Department of Agriculture in the name of a public entity (such as a state or local government, school district, or prison) that receives agricultural program payments</td>
                    <td>The public entity</td>
                  </tr>
                  <tr>
                    <td>14. Grantor trust filing under the Form 1041 Filing Method or the Optional Form 1099 Filing Method 2 (see Regulation section 1.671-4(b)(2)(i)(B))</td>
                    <td>The trust</td>
                  </tr>

              </tbody>
              </table>
              <p><strong>Note 1</strong> List first and circle the name of the person whose number you furnish. If only one person on a joint account has an SSN, that person’s number must be furnished.
              </p>
              <p><strong>Note 2</strong> Circle the minor’s name and furnish the minor’s SSN.
              </p>
              <p><strong>Note 3</strong> You must show your individual name and you may also enter your business or DBA name on the “Business name/disregarded entity” name line. You may use either your SSN or EIN (if you have one), but the IRS encourages you to use your SSN.
              </p>
              <p><strong>Note 4</strong> List first and circle the name of the trust, estate, or pension trust. (Do not furnish the TIN of the personal representative or trustee unless the legal entity itself is not designated in the account title.) Also see Special rules for partnerships on page 2.
              </p>
              <p><strong>*Note</strong> Grantor also must provide a Form W-9 to trustee of trust.
              </p>
              <p><strong>Note</strong> If no name is circled when more than one name is listed, the number will be considered to be that of the first name listed.
              </p>
              <p>
              <strong>How to get a TIN.</strong> If you do not have a TIN, apply for one immediately. To apply for an SSN, get Form SS-5, Application for a Social Security Card, from your local SSA office or get this form online at www.ssa.gov. You may also get this form by calling 1-800-772-1213. Use Form W-7, Application for IRS Individual Taxpayer Identification Number, to apply for an ITIN, or Form SS-4, Application for Employer Identification Number, to apply for an EIN. You can apply for an EIN online by accessing the IRS website at www.irs.gov/businesses and clicking on Employer Identification Number (EIN) under Starting a Business. You can get Forms W-7 and SS-4 from the IRS by visiting IRS.gov or by calling 1-800-TAX-FORM (1-800-829-3676).
              </p>
              <p>
              If you are asked to complete Form W-9 but do not have a TIN, apply for a TIN and write “Applied For” in the space for the TIN, sign and date the form, and give it to the requester. For interest and dividend payments, and certain payments made with respect to readily tradable instruments, generally you will have 60 days to get a TIN and give it to the requester before you are subject to backup withholding on payments. The 60-day rule does not apply to other types of payments. You will be subject to backup withholding on all such payments until you provide your TIN to the requester.
              </p>
              <p>
              <strong>Note.</strong> Entering “Applied For” means that you have already applied for a TIN or that you intend to apply for one soon.
              </p>
              <p>
              <strong>Caution:</strong> A disregarded U.S. entity that has a foreign owner must use the appropriate Form W-8.
              </p>

              </div>

              <div class="panel-stretch" id="instructions-Certification">
              <h3>Part II. Certification</h3>

              <p>To establish to the withholding agent that you are a U.S. person, or resident alien, sign Form W-9. You may be requested to sign by the withholding agent even if items 1, 4, or 5 below indicate otherwise.
              </p>
              <p>
              For a joint account, only the person whose TIN is shown in Part I should sign (when required). In the case of a disregarded entity, the person identified on the “Name” line must sign. Exempt payees, see Exempt payee code earlier.
              </p>
              <p>
              <strong>Signature requirements.</strong> Complete the certification as indicated in items 1 through 5 below.
              </p>
              <ol>
              <li>Interest, dividend, and barter exchange accounts opened before 1984 and broker accounts considered active during 1983. You must give your correct TIN, but you do not have to sign the certification.</li>

              <li>Interest, dividend, broker, and barter exchange accounts opened after 1983 and broker accounts considered inactive during 1983. You must sign the certification or backup withholding will apply. If you are subject to backup withholding and you are merely providing your correct TIN to the requester, you must cross out item 2 in the certification before signing the form.</li>

              <li>Real estate transactions. You must sign the certification. You may cross out item 2 of the certification.</li>

              <li>Other payments. You must give your correct TIN, but you do not have to sign the certification unless you have been notified that you have previously given an incorrect TIN. “Other payments” include payments made in the course of the requester’s trade or business for rents, royalties, goods (other than bills for merchandise), medical and health care services (including payments to corporations), payments to a nonemployee for services, payments made in settlement of payment card and third party network transactions, payments to certain fishing boat crew members and fishermen, and gross proceeds paid to attorneys (including payments to corporations).</li>

              <li>Mortgage interest paid by you, acquisition or abandonment of secured property, cancellation of debt, qualified tuition program payments (under section 529), IRA, Coverdell ESA, Archer MSA or HSA contributions or distributions, and pension distributions. You must give your correct TIN, but you do not have to sign the certification.</li>
              </ol>
              </div>

              <p></p>
              <h3>Secure Your Tax Records from Identity Theft</h3>
              <p>
              Identity theft occurs when someone uses your personal information such as your name, social security number (SSN), or other identifying information, without your permission, to commit fraud or other crimes. An identity thief may use your SSN to get a job or may file a tax return using your SSN to receive a refund.
              </p>
              <p>
              To reduce your risk:
              </p>
              <ul>
              <li>Protect your SSN,</li>
              <li>Ensure your employer is protecting your SSN, and</li>
              <li>Be careful when choosing a tax preparer.</li>
              </ul>

              <p>
              If your tax records are affected by identity theft and you receive a notice from the IRS, respond right away to the name and phone number printed on the IRS notice or letter.
              </p>
              <p>
              If your tax records are not currently affected by identity theft but you think you are at risk due to a lost or stolen purse or wallet, questionable credit card activity or credit report, contact the IRS Identity Theft Hotline at 1-800-908-4490 or submit Form 14039.
              </p>
              <p>
              For more information, see Publication 4535, Identity Theft Prevention and Victim Assistance.
              </p>
              <p>
              Victims of identity theft who are experiencing economic harm or a system problem, or are seeking help in resolving tax problems that have not been resolved through normal channels, may be eligible for Taxpayer Advocate Service (TAS) assistance. You can reach TAS by calling the TAS toll-free case intake line at 1-877-777-4778 or TTY/TDD 1-800-829-4059.
              </p>
              <p>
              <strong>Protect yourself from suspicious emails or phishing schemes.</strong> Phishing is the creation and use of email and websites designed to mimic legitimate business emails and websites. The most common act is sending an email to a user falsely claiming to be an established legitimate enterprise in an attempt to scam the user into surrendering private information that will be used for identity theft.
              </p>

              <p>
              The IRS does not initiate contacts with taxpayers via emails. Also, the IRS does not request personal detailed information through email or ask taxpayers for the PIN numbers, passwords, or similar secret access information for their credit card, bank, or other financial accounts.
              </p>
              <p>
              If you receive an unsolicited email claiming to be from the IRS, forward this message to phishing@irs.gov. You may also report misuse of the IRS name, logo, or other IRS property to the Treasury Inspector General for Tax Administration at 1-800-366-4484. You can forward suspicious emails to the Federal Trade Commission at: spam@uce.gov or contact them at www.ftc.gov/idtheft or 1-877- IDTHEFT (1-877-438-4338).
              </p>
              <p>
              Visit IRS.gov to learn more about identity theft and how to reduce your risk.
              </p>
              <h3>Privacy Act Notice</h3>
              <p>
              Section 6109 of the Internal Revenue Code requires you to provide your correct TIN to persons (including federal agencies) who are required to file information returns with the IRS to report interest, dividends, or certain other income paid to you; mortgage interest you paid; the acquisition or abandonment of secured property; the cancellation of debt; or contributions you made to an IRA, Archer MSA, or HSA. The person collecting this form uses the information on the form to file information returns with the IRS, reporting the above information. Routine uses of this information include giving it to the Department of Justice for civil and criminal litigation and to cities, states, the District of Columbia, and U.S. commonwealths and possessions for use in administering their laws. The information also may be disclosed to other countries under a treaty, to federal and state agencies to enforce civil and criminal laws, or to federal law enforcement and intelligence agencies to combat terrorism. You must provide your TIN whether or not you are required to file a tax return. Under section 3406, payers must generally withhold a percentage of taxable interest, dividend, and certain other payments to a payee who does not give a TIN to the payer. Certain penalties may also apply for providing false or fraudulent information.
              </p>
              </div></div>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
  </div>
</div>



<?php if ($this->uri->segment(3) == 'manually') {?>
     <input type="hidden" value="Manual" id="v_status"/>
<?php } else {?>
     <input type="hidden" value="" id="v_status"/>
<?php }?>
  <script src="<?php echo iaBase(); ?>assets/js/jquery.validate.min.js"></script>
  <script src="<?php echo iaBase(); ?>assets/js/signature_pad.js"></script>
  <script src="<?php echo iaBase(); ?>assets/js/additional-methods.min.js"></script>
  <script src="<?php echo iaBase(); ?>assets/js/pdf.js"></script>


  <script type="text/javascript">
     $(document).ready(function() {



      $('#c3_no_backup_withholding').click(function(){
        if($(this).is(':checked')){
          $('li.no_backup_withholding').removeClass('strikeout');
        }else{
          $('li.no_backup_withholding').addClass('strikeout');
        }
      });
      $('#c3_business_classification').val($('#v_business_classification').val());
      if($('#v_business_classification').val() == 'Other'){

        var html =$('#c38_business_other').html();
        $('#other_federal_tax').html(html);
      }
      $('#c3_business_classification').change(function(){
        var val = $(this).val();
        if(val == 'Other'){
          var html =$('#c38_business_other').html();
          $('#other_federal_tax').html(html);
        }else{
          $('#other_federal_tax').html('');
        }
      });

      if($('#c3_foreign_country_indicator').is(':checked')){
         $('#foreign_address').show();
         $('#all_address_form').html('');
       }else{
         $('#foreign_address').hide();
         var a_html = $('#all_address').html();
         $('#all_address_form').html(a_html);
       }

      $('#c3_foreign_country_indicator').click(function(){
       if($(this).is(':checked')){
         $('#foreign_address').show();
         $('#all_address_form').html('');
       }else{
         $('#foreign_address').hide();
         var a_html = $('#all_address').html();
         $('#all_address_form').html(a_html);
       }
      });
      var wrapper = document.getElementById("signature-pad");
      if(wrapper != null){
        var canvas = wrapper.querySelector("canvas");
        var signaturePad = new SignaturePad(canvas, {
          displayOnly:true,
          penColor: "rgb(0, 0, 0)",
          backgroundColor:"rgb(255,255,255)"
        });
      }else{
        var signaturePad ='';
      }
     function download(dataURL, filename) {
      var blob = dataURLToBlob(dataURL);
      var url = window.URL.createObjectURL(blob);

      var a = document.createElement("a");
      a.style = "display: none";
      a.href = url;
      a.download = filename;

      document.body.appendChild(a);
      a.click();

      window.URL.revokeObjectURL(url);
    }

     // One could simply use Canvas#toBlob method instead, but it's just to show
    // that it can be done using result of SignaturePad#toDataURL.
    function dataURLToBlob(dataURL) {
      // Code taken from https://github.com/ebidel/filer.js
      var parts = dataURL.split(';base64,');
      var contentType = parts[0].split(":")[1];
      var raw = window.atob(parts[1]);
      var rawLength = raw.length;
      var uInt8Array = new Uint8Array(rawLength);

      for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
      }

      return new Blob([uInt8Array], { type: contentType });
    }
      function resizeCanvas() {


      var ratio =  Math.max(window.devicePixelRatio || 1, 1);

          // This part causes the canvas to be cleared
          canvas.width = (canvas.offsetWidth * ratio) ;
          canvas.height = (canvas.offsetHeight * ratio);
          canvas.getContext("2d").scale(ratio, ratio);

          signaturePad.clear();
        }
      if(wrapper != null){
        window.onresize = resizeCanvas;
        resizeCanvas();
      }
      $.validator.addMethod("signature", function(value, element, options) {
          if( signaturePad.isEmpty()){
                  $("#signature-pad").css("border-color" , '#c42421');
                  return false;
              }
              $("#signature-pad").css("border-color" , '');
          return true;
      }, "Your signature is required");

      $('#w9-form').submit(function(e) {
            e.preventDefault();
        }).validate({ // initialize the plugin
         errorPlacement: function(error, element) {
            if (element.attr("class") == "ip_bc" )
                error.insertAfter(".field-business_classification > label");
            else
                error.insertAfter(element);
        },
        ignore: [],
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true,
            },
            city: {
                required: true,
            },
            state: {
                required: true,
            },
            zip: {
               required: true,
               digits: true,
               minlength: 5,
               maxlength: 5,
            },
            v_ssn: {
                require_from_group: [1, ".group"],
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            v_ein: {
                require_from_group: [1, ".group"],
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
           /* ".ip_bc" :{
                required: true,
                atLeastOneChecked: true,
            },*/
            exempt_payee_code :{
              digits: true
            },
            exempt_fatca_code :{
              lettersonly: true
            },
            business_other :{
              required: true
            },
           signature: { signature : true }
        },
      messages: {
             name: {
                required: 'Required',
            },
            email: {
                required: 'Required',
                email: 'Invalid email address'
            },
            address: {
                required: 'Required',
            },
            city: {
                required: 'Required',
            },
            state: {
                required: 'Required',
            },
            zip: {
              required: 'Required',
              digits: 'Must be a valid U.S. ZIP code',
              minlength: 'Must be a valid U.S. ZIP code',
              maxlength: 'Must be a valid U.S. ZIP code',
            },
           /* ".ip_bc" :{
                required: 'Select one appropriate checkbox',
            },  */
            exempt_payee_code :{
              digits: 'Must be a number'
            },
            exempt_fatca_code :{
              lettersonly: 'Use code A-M'
            },
             business_other :{
              required: 'Required'
            },
            v_ssn: {
                require_from_group:'Enter One',
                digits: 'Must contain nine digits with optional dashes',
                minlength: 'Must contain nine digits with optional dashes',
                maxlength: 'Must contain nine digits with optional dashes',
            },
            v_ein: {
                require_from_group: 'Enter One',
                digits: 'Must contain nine digits with optional dashes',
                minlength: 'Must contain nine digits with optional dashes',
                maxlength: 'Must contain nine digits with optional dashes',
            },

       },
       submitHandler: function(form) {
        $('.needed-for-entry').hide();
        $('.preview-panel').show();
        $('.loading-message').show();
        if(signaturePad !=''){

          var dataURL = signaturePad.toDataURL("image/jpeg");
          $('#signature').val(dataURL);
        }
        url ='<?php echo base_url('fillform') ?>';
        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        $.ajax({
            url: url,
            type: "POST",
            data: $('form').serialize(),
            cache: false,
            processData: false,
            success: function(data) {
              var url = '<?=iaBase();?>';
              var obj = jQuery.parseJSON( data );

              $('#download-preview').attr('href',url+obj.pdfPath);
              $('#c3_v_id').val(obj.v_id);
              //console.log(dataURL);
              renderPDF(url+obj.pdfPath+'?v='+time, document.getElementById('image-container'));
              //$('#image-container').html(obj.pdfPath);
            }
        });
        return false;
       }
    });

     $('#btn_clear').click(function(){
       signaturePad.clear();
     });

      $('.edit-button').click(function(e){
        $('.image-container canvas').remove();
        $('.needed-for-entry').show();
        $('.preview-panel').hide();
        e.preventDefault();
     });

    $('.done-button').click(function(e){
        $('.image-container canvas').remove();
        $('.success-message').show();
        $('.preview-panel').hide();
         var url = $('#download-preview').attr('href');
         var filename = 'w9-'+$('#c3_name').val()+'-vendor.pdf';
         var v_id = $('#c3_v_id').val();
         var v_status = $('#v_status').val();
         var dataNew = {pdf : url,v_id : v_id,v_status:v_status};
         var baseUrl = '<?php echo base_url('sendPdf') ?>';
          $.ajax({
            url: baseUrl,
            type: "POST",
            data: dataNew,
            success: function(data) {
              var a = document.createElement("a");
              a.style = "display: none";
              a.href = url;
              a.download = filename;

              document.body.appendChild(a);
              a.click();

              window.URL.revokeObjectURL(url);
            }
          });

          e.preventDefault();

     });

     $('.download-button').click(function(){
         var url = $('#download-preview').attr('href');
         var filename = 'w9-'+$('#c3_name').val()+'-vendor.pdf';
         var a = document.createElement("a");
          a.style = "display: none";
          a.href = url;
          a.download = filename;

          document.body.appendChild(a);
          a.click();

          window.URL.revokeObjectURL(url);
     });
    $('#c3_state').val($('#v_state').val());
  });

function renderPDF(url, canvasContainer, options) {

    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    var options = options || { scale: ratio };

    function renderPage(page) {
        var viewport = page.getViewport(options.scale);
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var renderContext = {
          canvasContext: ctx,
          viewport: viewport
        };

        canvas.height = viewport.height;
        canvas.width = viewport.width;

        canvasContainer.appendChild(canvas);

        page.render(renderContext);
        $('.loading-message').hide();
    }

    function renderPages(pdfDoc) {
        for(var num = 1; num <= pdfDoc.numPages; num++)
            pdfDoc.getPage(num).then(renderPage);
    }

    PDFJS.disableWorker = true;
    PDFJS.getDocument(url).then(renderPages);

}


$('.oneCommonClass').on('click', function(){
  $('.bd-example-modal-lg').modal('show');
  var target =  $(this).attr('href');
  $(target).trigger('focus');
  /*console.log(target);*/
});

/*$('.bd-example-modal-lg').on('shown.bs.modal', function () {
  $('#myInput')
})*/

  </script>

<style type="text/css">

div#c3_signature {
    height: auto!important;
    border: 2px dotted black;
    background-color: lightgrey;
}
button.clear-signature {
    position: absolute;
    right: 20px;
    top: 30px;
    z-index: 99;
}
.panel-body-extra .panel-stretch{
    margin-left: -15px;
    margin-right: -15px;
}

input#signatureValue {
    display: none;
}
div#image-container {
    margin-bottom: 10px;
}
#entry-banner{
  background-color: $fff!important;
}
</style>

