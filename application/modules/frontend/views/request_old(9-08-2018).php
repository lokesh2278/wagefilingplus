<body onLoad="ProgramI();">
<script type="text/javascript">
  $(document).ready(function(){
    $("#clear").click(function(){
        $(".textEntry").val(" ");
        $('.check').prop('checked', false);
    });
});
var FormChanged = false;

function removechar(str,c)
{ // this is good
var str; var ln; var pos;
ln=str.length-1; pos=str.indexOf(c);
if (pos>=0) {str=str.substr(0,pos)+str.substr(pos+1,ln-pos);}
return str
}

function valssn()
{
 var fm; var x; var i; var c; var str; var n; var err;
  fm=document.RcmForm;            // get form for conveninece
   str=fm.recepient_id_no.value;
err=0; n=0;
for (i=0; i<=str.length-1; i++) {c=str.charAt(i); if (c=="-"){n++}}
if (n>2) {alert("Too many dashes"); return false;}
if (n==1) {if (str.charAt(2)!="-") {err++} }
if (n==2) {
   if (str.charAt(3)!="-") {err++}
   if (str.charAt(6)!="-") {err++}
         }
if (err>0)
 {alert("If using optional dashes, format as xx-xxxxxxx or xxx-xx-xxxx");return false;}

// Pull out the dashes, the check the length...

   str=removechar(str,'-');   str=removechar(str,'-'); // remove up to two dashes...
   i=str.length;
   if ( i!=9 ) {alert('ID Number not 9 digits'); return false;}
// Now check for all numerics
//  str=fm.ssn.value;
//for (i=0; i<=fm.ssn.value.length-1; i++)
  for (i=0; i<=8; i++)
  {
  c=str.charAt(i);    // like pascal c:=str[i];
//  if (c<'-' || c>'9')  {alert("SSN contains non-numerics"); return false;}

  if (isNaN(c) )  {alert("TaxID contains non-numerics"); return false;}
  if (c==" ") {alert("TaxID contains blanks"); return false;}
  }

  if (str=='123456789') {alert('123456789 is an invalid TaxID number'); return false;}

// check to see if this is all the same number like all zeros.
// for j:=2 to 9 do begin if ssn[1]<>ssn[j] then goto 10; end;
  for (i=1; i<=8; i++)
  {
   if (str.charAt(i)!=str.charAt(0)) break;
   if (i==8) {alert('TaxID cannot be all the same numbers '+str); return false;}
  }
  return true          // true and false are reserved words.
}  // end funcion



function valzip()
  {
   var fm; var ln; var c;
   if (document.RcmForm.foreign_add.checked) {return true} // exit if foreign. No Checking
   fm=document.RcmForm
   ln=fm.zipcode.value.length
   if (ln!=5 && ln!=9 && ln!=10)
   {   alert("Wrong length zipcode");    return false;    }

   // make sure all digits, no alpha
   str=fm.zipcode.value
  for (i=0; i<=ln-1; i++)
   {
    c=str.substr(i,1);
    if (c<"-" || c>"9") {alert("zipcode contains non-numerics"); return false;}
   }
   return true
  } // end valzip

function valstate()
{
 var fm; var str; var ln; var lst; var found;
  if (document.RcmForm.foreign_add.checked) {return true} // exit if foreign. No Checking
  fm=document.RcmForm;    ln=fm.state.value.length;
  if (ln!=2) {alert("State Code not 2 chars"); return false}
  str=fm.state.value.toUpperCase()
  document.RcmForm.state.value=str
  found=false
  // now look up state
  for (i=0; i<=59-1; i++)
  {
  lst=document.forms[0].stateList.options[i].text; // get list box items i=0,1... from list
  lst=lst.substr(0,2) // get left part...
  if (lst==str) {found=true; break;}
  }
  if (found==false) {alert("Invalid state code"); }
  return found; // returns either true of false...
} // end function


function put()
{var txt;
txt=document.forms[0].stateList.options[document.forms[0].stateList.selectedIndex].text
txt=txt.substr(0,2);
document.forms[0].state.value=txt
}
var sum = '';
function valmoney2(str)
{ if (str=="") {return true;}
//  str=removechar(str,","); // remove commas a common thing.
//  str=removechar(str,"$"); // remove dollar signs.
  if (isNaN(str)) {alert("Value in a money box contains comma, letters or other Non-Numerics:  " + str); return false;}
  sum=sum+str; // sum all money for later to see if they are all zeros..
  return true;
}

function valall()
{ var i;
// if (FormChanged==false) {window.location.href='dsprecs.asp'; return false;} // no keydown dont post
 sum = '';

 if (valssn()==false)   {return false}
 if (valzip()==false)   {return false}
 if (valstate()==false) {return false}

 i = document.RcmForm.first_name.value.length;
 if (i>40) {alert("Name1 more than 40 characters"); return false;}
 if (i<1)  {alert("Name1 field empty"); return false;}

 i = document.RcmForm.dba.value.length;
 if (i>40) {alert("Dba, partners etc. more than 40 characters"); return false;}

 i = document.RcmForm.address.value.length;
 if (i>40) {alert("Address more than 40 characters"); return false;}
 if (i<1)  {alert("Address field empty"); return false;}

 i = document.RcmForm.city.value.length;
 if (i>40) {alert("City more than 40 characters"); return false;}
 if (i<1)  {alert("City field empty"); return false;}

 if (valmoney2(document.RcmForm.rents.value)==false) {return false;}
 if (valmoney2(document.RcmForm.royalties.value)==false) {return false;}
 if (valmoney2(document.RcmForm.other_income.value)==false) {return false;}
 if (valmoney2(document.RcmForm.fed_income_with_held.value)==false) {return false;}
 if (valmoney2(document.RcmForm.fishing_boat_proceed.value)==false) {return false;}
 // if (valmoney2(document.RcmForm.box5.value)==false) {return false;}
 if (valmoney2(document.RcmForm.mad_helthcare_pmts.value)==false) {return false;}
 if (valmoney2(document.RcmForm.non_emp_compansation.value)==false) {return false;}
 if (valmoney2(document.RcmForm.pmts_in_lieu.value)==false) {return false;}
 // box9 not a money amount..
 if (valmoney2(document.RcmForm.crop_Insurance_proceeds.value)==false) {return false;}
 // Boxes 11 and 12 are greyed out on the form may come back in future.
 if (valmoney2(document.RcmForm.excess_golden_par_pmts.value)==false) {return false;}
 if (valmoney2(document.RcmForm.gross_paid_to_an_attomey.value)==false) {return false;}
 if (valmoney2(document.RcmForm.sec_409a_deferrals.value)==false) {return false;}
 if (valmoney2(document.RcmForm.sec_409a_Income.value)==false) {return false;}
 if (valmoney2(document.RcmForm.state_tax_with_held.value)==false) {return false;}  // box16 state incometax withheld money.
 // Box 17 state payers no not money..
 if (valmoney2(document.RcmForm.state_Income.value)==false) {return false;}  // box18 is state income money.

 if (document.RcmForm.misc_delete.checked==true)
 {
 var r=confirm('This record will be deleted if you continue!');
 if (r==false)
    {document.RcmForm.misc_delete.value="0"
     return false;
    }
 document.RcmForm.misc_delete.value="13"
 }
  if (sum=='') {alert("No money in any box"); return false;}
   return true
} // end function

function ProgramI()
{
  document.RcmForm.recepient_id_no.focus()
}

function consumer_products_for_resale()
{// When they click the box9, we set the return Html value=""
  var b9;
  b9=document.RcmForm.consumer_products_for_resale;
  if (b9.checked==true) {b9.value='1'}    // this works great...
  else {b9.value=''}
}

function voidclick()
{
  var vbox;
  vbox=document.RcmForm.void;
  if (vbox.checked==true) {vbox.value='1'}
  else {vbox.value=''}
}

function help14()
{
myWindow=window.open('<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/help.html','','width=600,height=400,scrollbars=yes');
myWindow.focus();

}

function corclick()
{
  var cbox;
  cbox=document.RcmForm.corrected;
  if (cbox.checked==true) {
  cbox.value='1'
  var r=confirm('Do not check this box unless:\n - You have carefully read Topic No. 14 in HELP!\n Read Help NOW???');
  if (r==true) {help14();}


  }
  else

  {cbox.value=''}



}

function forclick()
{
  var fbox;
  fbox=document.RcmForm.foreign_add;
  if (fbox.checked==true) {fbox.value='1'}
  else {fbox.value=''}
}
</script>

<style>
.textEntry {
  BORDER-TOP: 0px solid #333333;
  BORDER-RIGHT: 0px solid #000000;
  BORDER-BOTTOM: 1px solid #333333;
  BORDER-LEFT: 0px solid #333333;
  PADDING-TOP: 0px;
  PADDING-RIGHT: 0px;
  PADDING-BOTTOM: 0px;
  PADDING-LEFT: 5px;
  MARGIN: 0px;
  FONT-WEIGHT: normal;
  FONT-SIZE: 10pt;
  COLOR: #000000;
  FONT-STYLE: normal;
  FONT-FAMILY: Arial;
  HEIGHT: 24px;
  TEXT-DECORATION: none;
}
.info{line-height: 12px !important;padding-bottom: 17px;color: black;}
.acc_no{padding-bottom: 14px;padding-left: 3px;}
.lheight{line-height: 36px !important;}
.rents{line-height: 57px !important;}
.newheight{line-height: 70px !important;}
.padding{padding-top: 32px;}
.padding2{padding-bottom: 13px;}
.padding3{padding-bottom: 8px;padding-left: 3px;}
.padding4{padding-bottom: 23px;padding-left: 3px;}
.padding5{padding-bottom: 23px;}
.non_emp{padding-bottom: 21px;padding-left: 7px;}
/*.crop_insurance{display: unset; line-height: 39px!important;}*/
.excess{display: table-cell; padding-bottom: 25px!important;}
/*.pmts{display: table-cell;padding-bottom: 14px;}*/

.VerdanaSmall {font-family: Verdana; font-size: x-small;}
.Tiny {font-family: Verdana; font-size: 9px;}
.auto-style1 {
  /*margin-left: 86px;*/
}
@media only screen
and (min-device-width : 768px)
and (max-device-width : 1024px)
{
 .city_state_zip {
  margin-left: 7px;
 }
}
@media only screen
and (min-width: 1370px)
and (max-width: 1605px)
{
  .city_state_zip {
   margin-left: 42px;
  }
}
</style>
 <form name="RcmForm" method="post"  action="addform"
      onsubmit="return valall();"
      onKeydown="FormChanged=true; // alert('Key');">
                    <table width="855" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="tbl_brd"><table width="855" border="0" style="background-size: 860px 425px;" align="center" cellpadding="0" cellspacing="0" background="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/Red16.gif" bgcolor="#FFFFFF" class="VerdanaSmall">
                          <tr>
                            <td width="17" height="32">&nbsp;</td>
                            <td width="160" height="32">&nbsp;</td>
                            <td width="35" height="32">&nbsp;</td>
                            <td height="32" style="width: 98px">
                            Void <input style="margin-left: 5px;" type="checkbox" class="check" name="void" value="1"<?php echo !empty($v_details) && !empty($v_details[0]->void) ? 'checked' : ''; ?>  onClick="voidclick()" />
                            </td>
                            <td height="32" style="width: 117px">
                            Corrected <input style="margin-left: 5px;" type="checkbox" class="check" name="corrected" value="1" <?php echo !empty($v_details) && !empty($v_details[0]->corrected) ? 'checked' : ''; ?>  onClick="corclick()" />
                            </td>
                            <td width="153" height="32">Delete <input style="margin-left: 5px;" class="check" type="checkbox" value="3" name="misc_delete" <?php if (isset($v_details[0]->misc_delete) && $v_details[0]->misc_delete == '0') {echo '';} else {echo 'disabled';}?>>
              </td>
                            <td height="32" style="width: 163px">&nbsp;</td>
                            <td width="116" height="32">&nbsp;</td>
                          </tr>
                          <tr valign="bottom">
                            <td width="17" rowspan="3"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="100" /></td>
                            <td colspan="4" rowspan="3" class="info">
                                <?php echo !empty($c_details[0]->c_name) ? $c_details[0]->c_name : ''; ?><br><?php echo !empty($c_details[0]->c_filer_name_2) ? $c_details[0]->c_filer_name_2 . "<br>" : ''; ?><?php echo !empty($c_details[0]->c_address) ? $c_details[0]->c_address : ''; ?><br><?php echo !empty($c_details[0]->c_address_2) ? $c_details[0]->c_address_2 . "<br>" : ''; ?><?php echo !empty($c_details[0]->c_city) ? $c_details[0]->c_city : ''; ?>, <?php echo !empty($c_details[0]->c_state) ? $c_details[0]->c_state : ''; ?>, <?php echo !empty($c_details[0]->c_zip) ? $c_details[0]->c_zip : ''; ?><br><?php echo !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : ''; ?>
                            </td>
                            <td width="153" style="height: 35px" class="rents"><input type="text" maxlength="14" value="<?php echo isset($v_details[0]->rents) ? $v_details[0]->rents : ''; ?>"  name="rents" size="14" class="textEntry" />&nbsp;</td>
                            <td rowspan="2" style="width: 163px;padding-bottom: 37px;font-size: 23px;font-weight:bold;color: red;"><?=$c_details[0]->TaxYear?></td>
                            <td width="116" style="height: 35px"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="35" /></td>
                          </tr>
                          <tr valign="bottom">
                            <td width="153" style="height: 35px" class="padding2"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->royalties) ? $v_details[0]->royalties : ''; ?>" name="royalties" size="13" class="textEntry" /></td>
                            <td width="116" style="height: 38px"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="38" /></td>
                          </tr>
                          <tr valign="bottom">
                            <td width="153" style="height: 23px" class="padding3"><input type="text" maxlength="14" value="<?php echo isset($v_details[0]->other_income) ? $v_details[0]->other_income : ''; ?>" name="other_income" size="12" class="textEntry" /></td>
                            <td style="height: 23px; width: 163px;" class="padding3"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->fed_income_with_held) ? $v_details[0]->fed_income_with_held : ''; ?>" name="fed_income_with_held" size="12" class="textEntry" /></td>
                            <td width="116" style="height: 23px">
              <img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="37" /></td>
                          </tr>
                          <tr>
                            <td width="17" valign="bottom" style="height: 32px"></td>
                            <td width="160" style="margin-top: 9px; color:black;"><?php echo !empty($c_details[0]->c_ssn_ein) ? $c_details[0]->c_ssn_ein : ''; ?></td>
                            <td width="35" valign="bottom" style="height: 32px"></td>
                            <td colspan="2" valign="bottom"  class="padding5">
                              <input type="hidden" name="c_id" value="<?=$c_details[0]->FileSequence?>"/>
                              <input type="hidden" name="CustID" value="<?=$c_details[0]->CustID?>"/>
              <input type="hidden" name="misc_id" value="<?php echo !empty($v_details) && !empty($v_details[0]->misc_id) ? $v_details[0]->misc_id : ''; ?>"/>
              <input type="text" style="margin-top: 9px;" maxlength="11" value="<?php echo !empty($v_details) && !empty($v_details[0]->recepient_id_no) ? $v_details[0]->recepient_id_no : ''; ?>" id="recepient_id_no" name="recepient_id_no" size="20" class="textEntry" tabindex="1" /></td>
                            <td width="153" valign="bottom" style="height: 32px" class="padding4"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->fishing_boat_proceed) ? $v_details[0]->fishing_boat_proceed : ''; ?>" name="fishing_boat_proceed" size="12" class="textEntry" /></td>
                            <td valign="bottom" style="height: 32px; width: 163px;" class="padding4"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->mad_helthcare_pmts) ? $v_details[0]->mad_helthcare_pmts : ''; ?>" name="mad_helthcare_pmts" size="12" class="textEntry" /></td>
                            <td width="116" valign="bottom" style="height: 32px"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="37" /></td>
                          </tr>
                          <tr valign="bottom">
                            <td width="17" rowspan="3"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="10" /></td>
                            <td colspan="4" rowspan="3">
              <input type="text" maxlength="40" value="<?php echo !empty($v_details) && !empty($v_details[0]->first_name) ? $v_details[0]->first_name : ''; ?>" name="first_name" size="30" class="textEntry" tabindex="2" />
                                <span class="Tiny">&nbsp; First &amp; Last Name</span><br />
                                <input type="text" maxlength="40" value="<?php echo !empty($v_details) && !empty($v_details[0]->dba) ? $v_details[0]->dba : ''; ?>" name="dba" size="30" class="textEntry" tabindex="3" />
                              <span class="Tiny">&nbsp; dba, partners, etc.</span><br />
                                <input type="text" maxlength="40" value="<?php echo !empty($v_details) && !empty($v_details[0]->address) ? $v_details[0]->address : ''; ?>" name="address" size="30" class="textEntry" tabindex="4" />
                              <span class="Tiny">&nbsp; Address</span><br />
                                <input type="text" maxlength="40" value="<?php echo !empty($v_details) && !empty($v_details[0]->city) ? $v_details[0]->city : ''; ?>" name="city" size="18" class="textEntry" tabindex="5" />
                              ,
                              <input type="text" maxlength="2" value="<?php echo !empty($v_details) && !empty($v_details[0]->state) ? $v_details[0]->state : '' ?>" name="state" size="1" class="textEntry" tabindex="6" />
                                <input type="text" maxlength="11" value="<?php echo !empty($v_details) && !empty($v_details[0]->zipcode) ? $v_details[0]->zipcode : ''; ?>" name="zipcode" size="6" class="textEntry" tabindex="7" />
                              <br>
                                <span class="Tiny city_state_zip">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              City&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 State&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; zipcode</span> <br />
                              &nbsp;</td>
                            <td width="153" class="non_emp" >
              <input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->non_emp_compansation) ? $v_details[0]->non_emp_compansation : ''; ?>" name="non_emp_compansation" size="12" class="textEntry" tabindex="8" /></td>
                            <td class="non_emp"><input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->pmts_in_lieu) ? $v_details[0]->pmts_in_lieu : ''; ?>" name="pmts_in_lieu" size="12" class="textEntry" /></td>
                            <td width="116" style="height: 39px"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="35" /></td>
                          </tr>
<tr  valign="bottom">
<td width="153" align="right" class="non_emp">
<input type="checkbox" name="consumer_products_for_resale" class="check" value="1" <?php echo !empty($v_details) && !empty($v_details[0]->consumer_products_for_resale) ? 'checked' : ''; //echo  $v_details[0]->consumer_products_for_resale;       ?> onClick="consumer_products_for_resale()" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="non_emp">
<input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->crop_Insurance_proceeds) ? $v_details[0]->crop_Insurance_proceeds : ''; ?>" name="crop_Insurance_proceeds" size="12" class="textEntry" /></td>
<td width="116" style="height: 47px"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="57" /></td>
</tr>
<tr valign="bottom">
<td width="153" class="non_emp">
<input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->excess_golden_par_pmts) ? $v_details[0]->excess_golden_par_pmts : ''; ?>" name="excess_golden_par_pmts" size="12" class="textEntry" /></td>
<td class="non_emp">
<input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->gross_paid_to_an_attomey) ? $v_details[0]->gross_paid_to_an_attomey : ''; ?>" name="gross_paid_to_an_attomey" size="12" class="textEntry" /></td>
<td width="116" style="height: 40px">
<img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="24" /></td>
</tr>
<tr valign="bottom">
<td width="17" height="36"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/spacer.gif" width="1" height="38" /></td>
<td class="acc_no">
<input type="text" maxlength="20" value="<?php echo !empty($v_details) && !empty($v_details[0]->account_number) ? $v_details[0]->account_number : ''; ?>" name="account_number" size="14" class="textEntry" /></td>
<td height="36" colspan="2" class="acc_no">
<input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->sec_409a_deferrals) ? $v_details[0]->sec_409a_deferrals : ''; ?>" name="sec_409a_deferrals" size="10" class="textEntry" /></td>
<td height="36" style="width: 117px" class="acc_no">
<input type="text" maxlength="10" value="<?php echo !empty($v_details) && !empty($v_details[0]->sec_409a_Income) ? $v_details[0]->sec_409a_Income : ''; ?>" name="sec_409a_Income" size="8" class="textEntry" />&nbsp;</td>
<td width="153" height="36" class="acc_no">
<input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->state_tax_with_held) ? $v_details[0]->state_tax_with_held : ''; ?>" name="state_tax_with_held" size="12" class="textEntry" /></td>
<td height="36" style="width: 163px" class="acc_no">
<input type="text" maxlength="20" value="<?php echo !empty($v_details) && !empty($v_details[0]->payer_state_no) ? $v_details[0]->payer_state_no : ''; ?>" name="payer_state_no" size="12" class="textEntry" /></td>
<td width="116" height="36" class="acc_no">
<input type="text" maxlength="14" value="<?php echo !empty($v_details) && !empty($v_details[0]->state_Income) ? $v_details[0]->state_Income : ''; ?>" name="state_Income" size="12" class="textEntry" /></td>
</tr>
<tr>
<td width="17" height="38">&nbsp;</td>
<td width="160" height="38">&nbsp;</td>
<td height="38" colspan="3">&nbsp;
<input type="checkbox" name="foreign_add" class="check" value="1" <?php echo !empty($v_details) && !empty($v_details[0]->foreign_add) ? 'checked' : ''; ?> onClick="forclick()" />
Foreign Address</td>

<td width="153" height="38">&nbsp;</td>
<td height="38" style="width: 163px">&nbsp;</td>
<td width="116" height="38">&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
<div align="center"><br />



<table border="0" style="width: 58%" class="auto-style1">
<tr>
<td align="center" style="width: 100%;">
<input type="submit" name="saveform" value="Save" class="btn btn-primary btn-md" tabindex="9" style="cursor: pointer;">
<button type="button" id="clear" class="btn btn-primary btn-md" style="cursor: pointer; color: #ffffff;" >Cancel</button>
<a style="color: #ffffff !important;" href="<?=base_url('vendor/')?><?=$c_details[0]->FileSequence;?>" type="button" class="btn btn-primary btn-md">Checkout</a>
<button type="submit" name="addform" class="btn btn-primary btn-md">Add Another</button></td>
</tr>
<tr>
<td align="center" style="width: 30%;">
<select name="stateList" size="1" class="smallpara" onChange="put()">
<option>Select State</option>
<option value="AL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AL') {echo "selected";}?>>AL Alabama</option>
<option value="AK" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AK') {echo "selected";}?>>AK Alaska</option>
<option value="AS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AS') {echo "selected";}?>>AS Amercn Samoa</option>
<option value="AZ" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AZ') {echo "selected";}?>>AZ Arizona</option>
<option value="AR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'AR') {echo "selected";}?>>AR Arkansas</option>
<option value="CA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CA') {echo "selected";}?>>CA California</option>
<option value="CO" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CO') {echo "selected";}?>>CO Colorado</option>
<option value="CT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'CT') {echo "selected";}?>>CT Connecticut</option>
<option value="DE" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'DE') {echo "selected";}?>>DE Delaware</option>
<option value="DC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'DC') {echo "selected";}?>>DC Dst of Colum</option>
<option value="FM" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'FM') {echo "selected";}?>>FM Micronesia</option>
<option value="FL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'FL') {echo "selected";}?>>FL Florida</option>
<option value="GA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'GA') {echo "selected";}?>>GA Georgia</option>
<option value="GU" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'GU') {echo "selected";}?>>GU Guam</option>
<option value="HI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'HI') {echo "selected";}?>>HI Hawaii</option>
<option value="ID" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ID') {echo "selected";}?>>ID Idaho</option>
<option value="IL" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IL') {echo "selected";}?>>IL Illinois</option>
<option value="IN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IN') {echo "selected";}?>>IN Indiana</option>
<option value="IA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'IA') {echo "selected";}?>>IA Iowa</option>
<option value="KS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'KS') {echo "selected";}?>>KS Kansas</option>
<option value="KY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'KY') {echo "selected";}?>>KY Kentucky</option>
<option value="LA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'LA') {echo "selected";}?>>LA Louisiana</option>
<option value="ME" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ME') {echo "selected";}?>>ME Maine</option>
<option value="MH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MH') {echo "selected";}?>>MH Marshall Is.</option>
<option value="MD" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MD') {echo "selected";}?>>MD Maryland</option>
<option value="MA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MA') {echo "selected";}?>>MA Massachusett</option>
<option value="MI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MI') {echo "selected";}?>>MI Michigan</option>
<option value="MN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MN') {echo "selected";}?>>MN Minnesota</option>
<option value="MS" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MS') {echo "selected";}?>>MS Mississippi</option>
<option value="MO" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MO') {echo "selected";}?>>MO Missouri</option>
<option value="MT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MT') {echo "selected";}?>>MT Montana</option>
<option value="NE" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NE') {echo "selected";}?>>NE Nebraska</option>
<option value="NV" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NV') {echo "selected";}?>>NV Nevada</option>
<option value="NH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NH') {echo "selected";}?>>NH New Hampshir</option>
<option value="NJ" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NJ') {echo "selected";}?>>NJ New Jersey</option>
<option value="NM" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NM') {echo "selected";}?>>NM New Mexico</option>
<option value="NY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NY') {echo "selected";}?>>NY New York</option>
<option value="NC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'NV') {echo "selected";}?>>NC N. Carolina</option>
<option value="ND" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'ND') {echo "selected";}?>>ND North Dakota</option>
<option value="MP" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'MP') {echo "selected";}?>>MP Mariana Is.</option>
<option value="OH" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OH') {echo "selected";}?>>OH Ohio</option>
<option value="OK" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OK') {echo "selected";}?>>OK Oklahoma</option>
<option value="OR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'OR') {echo "selected";}?>>OR Oregon</option>
<option value="PA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'PA') {echo "selected";}?>>PA Pennsylvania</option>
<option value="PR" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'PR') {echo "selected";}?>>PR Puerto Rico</option>
<option value="RI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'RI') {echo "selected";}?>>RI Rhode Island</option>
<option value="SC" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'SC') {echo "selected";}?>>SC So. Carolina</option>
<option value="SD" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'SD') {echo "selected";}?>>SD South Dakota</option>
<option value="TN" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'TN') {echo "selected";}?>>TN Tennessee</option>
<option value="TX" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'TX') {echo "selected";}?>>TX Texas</option>
<option value="UT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'UT') {echo "selected";}?>>UT Utah</option>
<option value="VT" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VT') {echo "selected";}?>>VT Vermont</option>
<option value="VA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VA') {echo "selected";}?>>VA Virginia</option>
<option value="VI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'VI') {echo "selected";}?>>VI Virgin Is.</option>
<option value="WA" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WA') {echo "selected";}?>>WA Washington</option>
<option value="WV" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WV') {echo "selected";}?>>WV West Virgina</option>
<option value="WI" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WI') {echo "selected";}?>>WI Wisconsin</option>
<option value="WY" <?php if (isset($v_details[0]->state) && $v_details[0]->state == 'WY') {echo "selected";}?>>WY Wyoming</option>
</select> Use Select State to find the correct 2 character state code.</td>

                          <!--  <td style="width: 20%; line-height: 9px !important; padding-left: 5px;"></td> -->
                        </tr>
                      </table>





</div>
</Form>


<center>
<span Style="font-family: Verdana; font-size: small;">
<b>Enter the payee information</b>. <br>
Click SAVE to save and return. Click Back/Cancel to
return without making changes. <br>
Click for:
<a href="i1099msc_2013.pdf" Target="_blank">IRS 1099-Misc Instructions</a>.&nbsp;Click
for: <a href="i1099gi_2013.pdf" Target="_blank">IRS 1099 General Instructions</a>.
<br>
The checking "VOID" box will keep this record from printing and/or e-Filing. <br>
<br>
</span>
</center>

<!-- INCLUDE FILE="footer_inc.asp" -->