<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: url('../assets/images/pastyear.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 30px;">
					<h3>File Past <span class="light">Year Forms</span></h3>
					<h5>File Past Year W-2 and 1099-MISC from 2007-2012 Instantly! </h5>
				</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<h4>Need to create recipient copies and e-File past year W2/1099-MISC forms? No problem!</h4>
			<h5>Follow these steps:</h5>
			<p class="lead">If you are a new customer please create a New Account, if you are returning just login.</p>
			<table>
				<tr><td style="font-size: 24px;">1.</td><td style="font-size: 14px; padding-left: 12px;">Once logged in you will be prompted to create a New Company, this is where you enter the information of the tax year you want to file and the company issuing the W2/1099 form.</td></tr>
				<tr><td style="font-size: 24px;">2.</td><td style="font-size: 14px; padding-left: 12px;">You will see your company name in a Red text, click on this to add your Payee info.</td></tr>
				<tr><td style="font-size: 24px;">3.</td><td style="font-size: 14px; padding-left: 12px;">Enter your Payee information, Save and hit Checkout if you are completed.</td></tr>
				<tr><td style="font-size: 24px;">4.</td><td style="font-size: 14px; padding-left: 12px;">After checkout you will be prompted to print the receipt which contains the instructions on how to print your recipient copies. At this point we have received your files and will electronically submit the federal copies and transmittal's to the IRS/SSA.</td></tr>
				<tr><td style="font-size: 24px;">5.</td><td style="font-size: 14px; padding-left: 12px;">In 2-4 days you will receive confirmation for your records that your filing is complete</td></tr>
			</table>
			<br />
			<hr>
			<h4>Contact Us</h4>
			<h5>WageFilingPlus. LLC.</h5>
			<h5>Telephone: 616-325-9332</h5>
			<h5>E-mail: <a href="mailto:support@wageFilingPlus.com">support@WageFilingPlus.com</a></h5>
			<h5>Trusted by</h4>
			<img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
