<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/application.css">
<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/blaze-entry.css">
<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/scss/style.css">
<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/home.css">
<link rel="stylesheet" href="<?php echo iaBase(); ?>assets/css/signature-pad.css">
<style>
  span.logo-lg img{
    max-width: 165px;
  }

  #form-entry .btn:hover {
      color: #fff !important;
      background-color: #337ab7;
      font-size: 12px;
      font-weight: 700;
      text-transform: uppercase;
      letter-spacing: 1px;
      border: 2px solid #337ab7;
  }

   #form-entry .btn {
      color: #337ab7 !important;
      background-color: #fff;
      font-size: 12px;
      font-weight: 700;
      text-transform: uppercase;
      letter-spacing: 1px;
      border: 2px solid #337ab7;
  }

  .panel-primary>.panel-heading {
      color: #fff;
      background-color: #337ab7!important;
      border-color: #24a9bd!important;
  }
</style>
<div class="navbar">
      <div class="container">
        <div class="navbar-header" style="margin-top: 15px !important;">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brands" href="<?php echo base_url('blaze'); ?>">
              <span class="logo-lg">
                <img src="<?php echo iaBase() . 'assets/images/wf-header-logo.png'; ?>" id="logo">
              </span>
            </a>
        </div>
        <div class="navbar-collapse collapse">

         </div><!--/.navbar-collapse -->
      </div>
    </div>


<script src="<?php echo iaBase(); ?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div id="app-container">
  <div>
    <div id="page" class="page">
  <div class="content-container">
<div id="entry-banner">
  <div class="container">
    <h1>
      <?php if ($this->uri->segment(3) == 'manually' || !empty($vendors)) {?>
      <?php $c_id = !empty($vendors[0]->c_id) ? $vendors[0]->c_id : $c_id;?>

      <span name="FileName"></span>
      <?php }?>
      <div class="e-request">Official 1099-Misc Request<br>from <!-- <?=companyName($c_id)?> --></div>
   <!--    <div class="logos">
        <img class="w9-logo" src="<?=iaBase()?>assets/images/WageFilingw9-Logo-XSmall.png" alt="Online W-9">
        <div class="service-by">
          <span>Powered by</span>
          <a href="<?=iaBase()?>" target="_blank"><img src="<?=iaBase()?>assets/images/logo-new-md-nq8.png" class="t1099-logo" alt="wagefiling"></a>
        </div>
      </div> -->
    </h1>
  </div>
</div>

<div class="container">
  <div class="col-sm-8 col-sm-offset-2 success-message non-entry" style="display:none">
      <div class="panel panel-success">
        <div class="panel-heading">
          <h3 class="panel-title">You're Done!</h3>
        </div>
        <div class="panel-body">
          <p>A copy of your completed 1099-Misc has been sent to <span name="FileName"><?=companyName($c_id)?></span>, no further action is required.</p>
          <p>Thank you! <span name="FileName"></span>. </p>
          <!-- <p>W-9's.com, LLC</p> -->
          <p>Need to issue 1099-Misc/W-2 for your business?<a class="company_email" href="<?=base_url('blaze');?>"> Click Here <span name="FileName"></span></a> </p>
        </div>
        <div class="panel-footer">
          <button class="btn btn-success download-button"><i class="glyphicon glyphicon-download-alt"></i> Download Again</button>
          <b>Close this tab to logout.</b>
        </div>
      </div>
    </div>

  <form id="w9-form" action="addMiscData" method="post">
  <div class="row entry-container needed-for-entry" >
    <?php if (!empty($vendors)) {?>
    <div class="entry-right-col col-md-12">
        <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Miscellaneuse Income</h3>
        </div>
      <?php } else {?>
    <div class="entry-right-col col-md-12 col-lg-12">
       <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Miscellaneuse Income</h3>
        </div>
      <?php }?>
<form method="post" action="addMiscData">
   <div class="panel-body panel-body-extra">
     <div class="form-container">

  <div class="row">
    <div class="col-sm-12" data-fields="name">
       <table class="table table-bordered">
        <tbody>
          <tr>
            <td>Void <input type="checkbox" name="void"/></td>
            <td>Corrected <input type="checkbox" name="corrected"/></td>
            <td>Delete <input type="checkbox" name="delete"/></td>
          </tr>
          <tr>
            <th>Payer's Name</th><td><?php echo !empty($vendors[0]->v_name) ? $vendors[0]->v_name : ''; ?></td>
            <td rowspan="5" style="text-align: center;">
                  <h4>OMB No.</h4>
                  <p>1099-MISC</p>
                </td>
          </tr>
          <tr>
            <th>Address</th><td><?php echo !empty($vendors[0]->v_address) ? $vendors[0]->v_address : ''; ?></td>
          </tr>
          <tr>
            <th>City</th><td><?php echo !empty($vendors[0]->v_city) ? $vendors[0]->v_city : ''; ?></td>
          </tr>
          <tr>
            <th>State</th><td><?php echo !empty($vendors[0]->v_state) ? $vendors[0]->v_state : ''; ?></td>
          </tr>
          <tr>
             <th>Zipcode</th><td><?php echo !empty($vendors[0]->v_zip) ? $vendors[0]->v_zip : ''; ?></td>
          </tr>
          <tr>
            <th>Payee's Federal Tax Id</th><td>1234567</td>
            <td>
              <p>Recepeint's Identification no.</p>
              <input type="text" class="form-control" name="rent" />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-sm-12">
    <div class="">
<p>RECEPIENT'S Name and Address</p>
    <div class="row">
                  <div class="col-sm-12" data-fields="shipping_address">
                    <div class="form-group field-shipping_address">
                      <p>Street</p>
                    <div data-editor="">
                      <input id="c54_shipping_address" class="form-control" name="street" maxlength="40" type="text" value="<?php echo !empty($c_details[0]->c_street) ? $c_details[0]->c_street : '' ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-5" data-fields="city">
                  <div class="form-group field-city">
                    <p>City</p>
                  <div data-editor="">
                    <input id="c54_city" class="form-control" name="city" maxlength="40" type="text" value="<?php echo !empty($c_details[0]->c_city) ? $c_details[0]->c_city : '' ?>">
                  </div>
                </div>
              </div>
              <div class="col-sm-4" data-fields="state"><div class="form-group field-state">
                <p>State</p>
            <div data-editor="">
              <input id="c54_c_state" type="hidden" value="<?php echo !empty($c_details[0]->c_state) ? $c_details[0]->c_state : '' ?>">
              <select id="c54_state" class="form-control" name="state">
                <option value="">Choose...</option>
                <option value="AA">AA - Armed Forces Americas</option>
                <option value="AE">AE - Armed Forces Europe</option>
                <option value="AK">AK - Alaska</option>
                <option value="AL">AL - Alabama</option>
                <option value="AP">AP - Armed Forces Pacific</option>
                <option value="AR">AR - Arkansas</option>
                <option value="AS">AS - American Samoa</option>
                <option value="AZ">AZ - Arizona</option>
                <option value="CA">CA - California</option>
                <option value="CO">CO - Colorado</option>
                <option value="CT">CT - Connecticut</option>
                <option value="DC">DC - District of Columbia</option>
                <option value="DE">DE - Delaware</option>
                <option value="FL">FL - Florida</option>
                <option value="FM">FM - Federated Micronesia</option>
                <option value="GA">GA - Georgia</option>
                <option value="GU">GU - Guam</option>
                <option value="HI">HI - Hawaii</option>
                <option value="IA">IA - Iowa</option>
                <option value="ID">ID - Idaho</option>
                <option value="IL">IL - Illinois</option>
                <option value="IN">IN - Indiana</option>
                <option value="KS">KS - Kansas</option>
                <option value="KY">KY - Kentucky</option>
                <option value="LA">LA - Louisiana</option>
                <option value="MA">MA - Massachusetts</option>
                <option value="MD">MD - Maryland</option>
                <option value="ME">ME - Maine</option>
                <option value="MH">MH - Marshall Islands</option>
                <option value="MI">MI - Michigan</option>
                <option value="MN">MN - Minnesota</option>
                <option value="MO">MO - Missouri</option>
                <option value="MP">MP - N. Mariana Islands</option>
                <option value="MS">MS - Mississippi</option>
                <option value="MT">MT - Montana</option>
                <option value="NC">NC - North Carolina</option>
                <option value="ND">ND - North Dakota</option>
                <option value="NE">NE - Nebraska</option>
                <option value="NH">NH - New Hampshire</option>
                <option value="NJ">NJ - New Jersey</option>
                <option value="NM">NM - New Mexico</option>
                <option value="NV">NV - Nevada</option>
                <option value="NY">NY - New York</option>
                <option value="OH">OH - Ohio</option>
                <option value="OK">OK - Oklahoma</option>
                <option value="OR">OR - Oregon</option>
                <option value="PA">PA - Pennsylvania</option>
                <option value="PR">PR - Puerto Rico</option>
                <option value="PW">PW - Palau</option>
                <option value="RI">RI - Rhode Island</option>
                <option value="SC">SC - South Carolina</option>
                <option value="SD">SD - South Dakota</option>
                <option value="TN">TN - Tennessee</option>
                <option value="TX">TX - Texas</option>
                <option value="UT">UT - Utah</option>
                <option value="VA">VA - Virginia</option>
                <option value="VI">VI - US Virgin Islands</option>
                <option value="VT">VT - Vermont</option>
                <option value="WA">WA - Washington</option>
                <option value="WI">WI - Wisconsin</option>
                <option value="WV">WV - West Virginia</option>
                <option value="WY">WY - Wyoming</option></select></div>
              </div>
            </div>
            <div class="col-sm-3" data-fields="zip"><div class="form-group field-zip">
              <p>Zip Code</p>
          <div data-editor="">
            <input id="c54_zip" class="form-control" name="zip" maxlength="10" type="text" value="<?php echo !empty($c_details[0]->c_zip) ? $c_details[0]->c_zip : '' ?>">
          </div>
        </div>
      </div>
    </div>

  <div class="row">
    <div class="col-sm-12" data-fields="address">
      <div class="form-group field-address">
          <p>Address</p>
                        <div data-editor="">
                          <input id="c3_address" class="form-control" name="address" maxlength="40" type="text" value="<?php echo !empty($vendors[0]->address) ? $vendors[0]->address : ''; ?>">
                        </div>
                      </div>
                    </div>
                 </div>
          </div>
        </div>
      <div class="col-sm-12" data-fields="name">
      <table class="table table-bordered">
            <tbody>
              <tr>
                <td>
                  <p>1 Rents</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="rent" />
                  </div>
                </td>
                </td>
                <td>
                  <p>2 Royalties</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="royalties" />
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <p>3 Other Income</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="other_income" />
                  </div>
                </td>
                <td>
                  <p>4 Fad Income Withheld</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="fad_income_withheld" />
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <p>5 Fishing Boat Proceeds</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="fishing_boat_proceeds" />
                  </div>
                </td>
                <td>
                  <p>6 Mad & Health care pmts</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="mad_helthcare_pmts" />
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <p>7 Nonemployee Compensation</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="non_emp_compansation" />
                  </div>
                </td>
                <td>
                  <p>8 Pmts in lieu of Div or Int</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="pmts_in_lieu" />
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <p>9 Payers made direct sales of <br />$5000 or more of consumer<br /> products to a buyer<br/>(recipient) for resale</p>
                  <input type="checkbox" name="consumer_products_for_resale" value="1" />
                </td>
                <td>
                  <p>10 Crop Insurance Proceeds</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="crop_Insurance_proceeds" />
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <p>13 Excess Golden Par Pmts </p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="excess_golden_par_pmts" />
                  </div>
                </td>
                <td>
                  <p>14 Gross Paid to an attomey</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="gross_paid_to_an_attomey" />
                </div>
                </td>
              </tr>
              <tr>
                <td>
                  <p>15a Sec 409A deferrals</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="sec_409a_deferrals" />
                </div>
                </td>
                <td>
                  <p>15b Sec 409A Income</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="sec_409a_Income" />
                </div>
                </td>
              </tr>
              <tr>
                <td>
                  <p>16 State tax with held</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="state_tax_with_held" />
                </div>
                </td>
                <td>
                  <p>17 State/Payer's state no.</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="payer_state_no" />
                </div>
                </td>
              </tr>
              <tr>
                <td>
                  <p>18 State Income</p>
                  <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">$</span>
                  <input type="text" class="form-control" name="state_Income" />
                </div>
                </td>
                <td>
                  <p>Account number</p>
                  <input type="text" class="form-control" name="account_number" />
                </td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>
  </div>
        </div>


    </div>
  </div>
  <div class="panel-footer">
    <input type="submit" class="btn btn-lg btn-primary save-button" value="Continue"/>
  </div>
  </form>
</div>



<?php if ($this->uri->segment(3) == 'manually') {?>
     <input type="hidden" value="Manual" id="v_status"/>
<?php } else {?>
     <input type="hidden" value="" id="v_status"/>
<?php }?>

  <script src="<?php echo iaBase(); ?>assets/js/jquery.validate.min.js"></script>
  <script src="<?php echo iaBase(); ?>assets/js/signature_pad.js"></script>
  <script src="<?php echo iaBase(); ?>assets/js/additional-methods.min.js"></script>
  <script src="<?php echo iaBase(); ?>assets/js/pdf.js"></script>


  <script type="text/javascript">
  </script>

<style type="text/css">

div#c3_signature {
    height: auto!important;
    border: 2px dotted black;
    background-color: lightgrey;
}
button.clear-signature {
    position: absolute;
    right: 20px;
    top: 30px;
    z-index: 99;
}
.panel-body-extra .panel-stretch{
    margin-left: -15px;
    margin-right: -15px;
}

input#signatureValue {
    display: none;
}
div#image-container {
    margin-bottom: 10px;
}
#entry-banner{
  background-color: $fff!important;
}
</style>