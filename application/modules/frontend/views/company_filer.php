<script type="text/javascript">
  $("#save").click(function () {

                    var employee_ein = $('#ssn_ein').val();
                    var n1 = employee_ein.startsWith('00',0);
                    var n2 = employee_ein.startsWith('07',0);
                    var n3 = employee_ein.startsWith('08',0);
                    var n4 = employee_ein.startsWith('09',0);
                    var n5 = employee_ein.startsWith('17',0);
                    var n6 = employee_ein.startsWith('18',0);
                    var n7 = employee_ein.startsWith('19',0);
                    var n8 = employee_ein.startsWith('28',0);
                    var n9 = employee_ein.startsWith('29',0);
                    var n10 = employee_ein.startsWith('49',0);
                    var n11 = employee_ein.startsWith('69',0);
                    var n12 = employee_ein.startsWith('70',0);
                    var n13 = employee_ein.startsWith('78',0);
                    var n14 = employee_ein.startsWith('79',0);
                    var n15 = employee_ein.startsWith('89',0);
                    if(n1){
                      alert("Employee EIN not start with 00");
                      return false;
                    }
                    else if(n2){
                      alert("Employee EIN not start with 07");
                      return false;
                    }
                    else if(n3){
                      alert("Employee EIN not start with 08");
                      return false;
                    }
                    else if(n4){
                      alert("Employee EIN not start with 09");
                      return false;
                    }
                    else if(n5){
                      alert("Employee EIN not start with 17");
                      return false;
                    }
                    else if(n6){
                      alert("Employee EIN not start with 18");
                      return false;
                    }
                    else if(n7){
                      alert("Employee EIN not start with 19");
                      return false;
                    }
                    else if(n8){
                      alert("Employee EIN not start with 28");
                      return false;
                    }
                    else if(n9){
                      alert("Employee EIN not start with 29");
                      return false;
                    }
                    else if(n10){
                      alert("Employee EIN not start with 49");
                      return false;
                    }
                    else if(n11){
                      alert("Employee EIN not start with 69");
                      return false;
                    }
                    else if(n12){
                      alert("Employee EIN not start with 70");
                      return false;
                    }
                    else if(n13){
                      alert("Employee EIN not start with 78");
                      return false;
                    }
                    else if(n14){
                      alert("Employee EIN not start with 79");
                      return false;
                    }
                    else if(n15){
                      alert("Employee EIN not start with 89");
                      return false;
                    }

                });
</script>
<style>

  /* The container */

.container-radio {

    display: block;

    position: relative;

    padding-left: 35px;

    margin-bottom: 12px;

    cursor: pointer;

    font-size: 22px;

    -webkit-user-select: none;

    -moz-user-select: none;

    -ms-user-select: none;

    user-select: none;

}



/* Hide the browser's default radio button */

.container-radio input {

    position: absolute;

    opacity: 0;

    cursor: pointer;

}



/* Create a custom radio button */

.checkmark {

    position: absolute;

    top: 0;

    left: 0;

    height: 25px;

    width: 25px;

    background-color: #eee;

    border-radius: 50%;

}



/* On mouse-over, add a grey background color */

.container-radio:hover input ~ .checkmark {

    background-color: #ccc;

}



/* When the radio button is checked, add a blue background */

.container-radio input:checked ~ .checkmark {

    background-color: #2196F3;

}



/* Create the indicator (the dot/circle - hidden when not checked) */

.checkmark:after {

    content: "";

    position: absolute;

    display: none;

}



/* Show the indicator (dot/circle) when checked */

.container-radio input:checked ~ .checkmark:after {

    display: block;

}



/* Style the indicator (dot/circle) */

.container-radio .checkmark:after {

    top: 9px;

    left: 9px;

    width: 8px;

    height: 8px;

    border-radius: 50%;

    background: white;

}

</style>

<div id="blaze-banner">

  <div class="container">

    <div class="navigation-container">

      <div class="wrap wrap-one-logo text-center">

        <a class="back-link" href="<?=base_url('blaze');?>" style="display: block; margin-left: 128px; "><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>

        <div class="row">

          <div id="nav-company" class="col-sm-12 col-md-12 col-lg-12">

            <h1 style="font-size: 29px; "><a class="company-link"><span class="company-name">Setup Company/Filer - Form 1099 Misc</span></a></h1>
            <h5 style="color:#ffffff;">Filer is the company that paid the Contractor</h5>
          </div>

          <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>

        </div>

      </div>

    </div>

    <div id="alerts-container" class="alerts-container"></div>

  </div>

</div>

<div class="content-container" id="main-view-content">

  <div>

    <div class="container">

      <div class="row">

        <div class="col-md-8 col-md-offset-2">

          <form id="companyform" action="company" accept-charset="UTF-8" method="post">

            <input id="c54_c_id" type="hidden" name="c_id" value="<?php echo !empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>">

            <div class="panel panel-primary">

              <div class="panel-heading">

                <h3 class="panel-title">Filer Setup - Form 1099 MISC</h3>

              </div>

              <div class="panel-body panel-body-extra">

                <div class="form-container">

                  <div id="form">

                    <div class="row">

                      <div class="col-sm-12" data-fields="ssn_ein">

                        <div class="form-group field-ssn_ein">

                          <label class="control-label" for="ssn_ein"><span class="label-inner">SSN or EIN <!-- <help style="margin-left: 10px;">(Enter With or Without Dashes)</help> --></span>

                        </label>

                        <div data-editor="">

                          <input id="ssn_ein" class="form-control required" name="ssn_ein" maxlength="40" type="text" value="<?php echo !empty($c_details[0]->c_ssn_ein) ? $c_details[0]->c_ssn_ein : '' ?>"/>

                        </div>

                      </div>

                    </div>

                  </div>

                  <div class="row">

                      <div class="col-sm-12" data-fields="tax_year">

                        <div class="form-group field-tax_year">

                          <label class="control-label" for="tax_year"><span class="label-inner">Tax Year<help style="margin-left: 10px;">(Enter current or previous years)</help></span>

                        </label>

                        <div data-editor="">



							<select class="form-control required" name="tax_year" id="tax_year"  >

                            <?php

$startdate = 2018;

$enddate = date("Y");

$yearC = isset($c_details[0]->c_tax_year) ? $c_details[0]->c_tax_year : $enddate;

$years = range($startdate, $enddate);

foreach ($years as $year) {

    if ($year == $yearC) {$sel = 'selected="selected"';} else { $sel = '';}

    echo "<option $sel value='$year'>$year </option>";

}

?></select>

                        <!--  <input id="tax_year" class="form-control required" name="tax_year" maxlength="4" type="text" value="<?php echo !empty($c_details[0]->c_tax_year) ? $c_details[0]->c_tax_year : '' ?>"/>-->

                          </div>

                      </div>

                    </div>

                  </div>

                  <div class="row">

                      <div class="col-sm-12" data-fields="c_name">

                        <div class="form-group field-name">

                          <label class="control-label" for="c_name"><span class="label-inner">Filer Name <help style="margin-left: 10px;">(Company)</help></span>

                        </label>

                        <div data-editor="">

                          <input id="c_name" class="form-control required" name="c_name" type="text" value="<?php echo !empty($c_details[0]->FileName) ? $c_details[0]->FileName : '' ?>" placeholder="Company" maxlength="40" />

                        </div>

                      </div>

                    </div>

                  </div>

                  <div class="row">

                      <div class="col-sm-12" data-fields="is_foregin">

                        <div class="form-group field-is_foregin">

                          <label class="control-label" for="is_foregin"><span class="label-inner">Filer Is Foregin</span>

                        </label>

                        <div data-editor="">

                          <div class="row">

                            <div class="col-md-2">

                              <label class="radio-inline container-radio">Yes
                              <input type="radio" id="is_foregin" name="is_foregin" value="1" type="radio" <?php echo !empty($c_details[0]->c_is_foreign) == '1' ? 'checked' : '' ?> >

                              <span class="checkmark"></span>

                            </label>

                            </div>

                            <div class="col-md-2">

                              <label class="radio-inline container-radio">No

                              <input type="radio" id="is_foregin2" name="is_foregin" value="0" type="radio" <?php echo !empty($c_details[0]->c_is_foreign) == '0' ? 'checked' : '' ?>>

                              <span class="checkmark"></span>

                            </label>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>


                  </div>

                  <div class="row">

                    <div class="col-sm-6" data-fields="filer_name_2">

                      <div class="form-group field-filer_name_2">

                        <label class="control-label" for="filer_name_2"><span class="label-inner">Filer Name 2<help style="margin-left: 10px;">(Optional)</help></span>

                      </label>

                      <div data-editor="">

                        <input id="filer_name_21" class="form-control required" name="filer_name_2"  type="text"  value="<?php echo !empty($c_details[0]->c_filer_name_2) ? $c_details[0]->c_filer_name_2 : '' ?>" placeholder="Optional" maxlength="40"/>

                      </div>

                    </div>

                  </div>


                </div>

                <div class="row">

                    <div class="col-sm-12" data-fields="address">

                      <div class="form-group field-address">

                        <label class="control-label" for="address"><span class="label-inner">Address</span>

                      </label>

                      <div data-editor="">

                        <input id="address" class="form-control required" name="address" type="text"  value="<?php echo !empty($c_details[0]->c_address) ? $c_details[0]->c_address : '' ?>" maxlength="40"/>

                      </div>

                    </div>

                  </div>

                </div>

                <div class="row">

                    <div class="col-sm-12" data-fields="address_2">

                      <div class="form-group field-address_2">

                        <label class="control-label" for="address_2"><span class="label-inner">Address 2<help style="margin-left: 10px;">(Optional)</help></span>

                      </label>

                      <div data-editor="">

                        <input id="address_2" class="form-control" name="address_2" type="text" value="<?php echo !empty($c_details[0]->c_address_2) ? $c_details[0]->c_address_2 : '' ?>" placeholder="Optional" maxlength="40"/>

                      </div>

                    </div>

                  </div>

                </div>

                <!-- <div class="row">

                  <div class="col-sm-12" data-fields="shipping_address">

                    <div class="form-group field-shipping_address">

                      <label class="control-label" for="c54_shipping_address"><span class="label-inner">Street</span>

                    </label>

                    <div data-editor="">

                      <input id="c54_shipping_address" class="form-control required" name="shipping_address" maxlength="40" type="text" value="<?php //echo !empty($c_details[0]->c_street) ? $c_details[0]->c_street : '' ?>">

                    </div>

                  </div>

                </div>

              </div> -->

              <div class="row">

                <div class="col-sm-5" data-fields="city">

                  <div class="form-group field-city">

                    <label class="control-label" for="city"><span class="label-inner">City</span>

                  </label>

                  <div data-editor="">

                    <input id="city" class="form-control required" name="city" type="text" value="<?php echo !empty($c_details[0]->c_city) ? $c_details[0]->c_city : '' ?>"/>

                  </div>

                </div>

              </div>

              <div class="col-sm-4" data-fields="state"><div class="form-group field-state">

                <label class="control-label" for="state"><span class="label-inner">State</span>

            </label>

            <div data-editor="">

              <input id="state" type="hidden" value="<?php echo !empty($c_details[0]->c_state) ? $c_details[0]->c_state : '' ?>">

              <select id="state" class="form-control required" name="state" required>
              <?php $c_state = !empty($c_details[0]->c_state) ? $c_details[0]->c_state : '';?>
                <option value="">Choose...</option>
                <option value="AA" <?php if ($c_state == 'AA') {echo 'selected';}?> >AA - Armed Forces Americas</option>
                <option value="AE" <?php if ($c_state == 'AE') {echo 'selected';}?> >AE - Armed Forces Europe</option>
                <option value="AK" <?php if ($c_state == 'AK') {echo 'selected';}?> >AK - Alaska</option>
                <option value="AL" <?php if ($c_state == 'AL') {echo 'selected';}?> >AL - Alabama</option>
                <option value="AP" <?php if ($c_state == 'AP') {echo 'selected';}?> >AP - Armed Forces Pacific</option>
                <option value="AR" <?php if ($c_state == 'AR') {echo 'selected';}?> >AR - Arkansas</option>
                <option value="AS" <?php if ($c_state == 'AS') {echo 'selected';}?> >AS - American Samoa</option>
                <option value="AZ" <?php if ($c_state == 'AZ') {echo 'selected';}?> >AZ - Arizona</option>
                <option value="CA" <?php if ($c_state == 'CA') {echo 'selected';}?> >CA - California</option>
                <option value="CO" <?php if ($c_state == 'CO') {echo 'selected';}?> >CO - Colorado</option>
                <option value="CT" <?php if ($c_state == 'CT') {echo 'selected';}?> >CT - Connecticut</option>
                <option value="DC" <?php if ($c_state == 'DC') {echo 'selected';}?> >DC - District of Columbia</option>
                <option value="DE" <?php if ($c_state == 'DE') {echo 'selected';}?> >DE - Delaware</option>
                <option value="FL" <?php if ($c_state == 'FL') {echo 'selected';}?> >FL - Florida</option>
                <option value="FM" <?php if ($c_state == 'FM') {echo 'selected';}?> >FM - Federated Micronesia</option>
                <option value="GA" <?php if ($c_state == 'GA') {echo 'selected';}?> >GA - Georgia</option>
                <option value="GU" <?php if ($c_state == 'GU') {echo 'selected';}?> >GU - Guam</option>
                <option value="HI" <?php if ($c_state == 'HI') {echo 'selected';}?> >HI - Hawaii</option>
                <option value="IA" <?php if ($c_state == 'IA') {echo 'selected';}?> >IA - Iowa</option>
                <option value="ID" <?php if ($c_state == 'ID') {echo 'selected';}?> >ID - Idaho</option>
                <option value="IL" <?php if ($c_state == 'IL') {echo 'selected';}?> >IL - Illinois</option>
                <option value="IN" <?php if ($c_state == 'IN') {echo 'selected';}?> >IN - Indiana</option>
                <option value="KS" <?php if ($c_state == 'KS') {echo 'selected';}?> >KS - Kansas</option>
                <option value="KY" <?php if ($c_state == 'KY') {echo 'selected';}?> >KY - Kentucky</option>
                <option value="LA" <?php if ($c_state == 'LA') {echo 'selected';}?> >LA - Louisiana</option>
                <option value="MA" <?php if ($c_state == 'MA') {echo 'selected';}?> >MA - Massachusetts</option>
                <option value="MD" <?php if ($c_state == 'MD') {echo 'selected';}?> >MD - Maryland</option>
                <option value="ME" <?php if ($c_state == 'ME') {echo 'selected';}?> >ME - Maine</option>
                <option value="MH" <?php if ($c_state == 'MH') {echo 'selected';}?> >MH - Marshall Islands</option>
                <option value="MI" <?php if ($c_state == 'MI') {echo 'selected';}?> >MI - Michigan</option>
                <option value="MN" <?php if ($c_state == 'MN') {echo 'selected';}?> >MN - Minnesota</option>
                <option value="MO" <?php if ($c_state == 'MO') {echo 'selected';}?> >MO - Missouri</option>
                <option value="MP" <?php if ($c_state == 'MP') {echo 'selected';}?> >MP - N. Mariana Islands</option>
                <option value="MS" <?php if ($c_state == 'MS') {echo 'selected';}?> >MS - Mississippi</option>
                <option value="MT" <?php if ($c_state == 'MT') {echo 'selected';}?> >MT - Montana</option>
                <option value="NC" <?php if ($c_state == 'NC') {echo 'selected';}?> >NC - North Carolina</option>
                <option value="ND" <?php if ($c_state == 'ND') {echo 'selected';}?> >ND - North Dakota</option>
                <option value="NE" <?php if ($c_state == 'NE') {echo 'selected';}?> >NE - Nebraska</option>
                <option value="NH" <?php if ($c_state == 'NH') {echo 'selected';}?> >NH - New Hampshire</option>
                <option value="NJ" <?php if ($c_state == 'NJ') {echo 'selected';}?> >NJ - New Jersey</option>
                <option value="NM" <?php if ($c_state == 'NM') {echo 'selected';}?> >NM - New Mexico</option>
                <option value="NV" <?php if ($c_state == 'NV') {echo 'selected';}?> >NV - Nevada</option>
                <option value="NY" <?php if ($c_state == 'NY') {echo 'selected';}?> >NY - New York</option>
                <option value="OH" <?php if ($c_state == 'OH') {echo 'selected';}?> >OH - Ohio</option>
                <option value="OK" <?php if ($c_state == 'OK') {echo 'selected';}?> >OK - Oklahoma</option>
                <option value="OR" <?php if ($c_state == 'OR') {echo 'selected';}?> >OR - Oregon</option>
                <option value="PA" <?php if ($c_state == 'PA') {echo 'selected';}?> >PA - Pennsylvania</option>
                <option value="PR" <?php if ($c_state == 'PR') {echo 'selected';}?> >PR - Puerto Rico</option>
                <option value="PW" <?php if ($c_state == 'PW') {echo 'selected';}?> >PW - Palau</option>
                <option value="RI" <?php if ($c_state == 'RI') {echo 'selected';}?> >RI - Rhode Island</option>
                <option value="SC" <?php if ($c_state == 'SC') {echo 'selected';}?> >SC - South Carolina</option>
                <option value="SD" <?php if ($c_state == 'SD') {echo 'selected';}?> >SD - South Dakota</option>
                <option value="TN" <?php if ($c_state == 'TN') {echo 'selected';}?> >TN - Tennessee</option>
                <option value="TX" <?php if ($c_state == 'TX') {echo 'selected';}?> >TX - Texas</option>
                <option value="UT" <?php if ($c_state == 'UT') {echo 'selected';}?> >UT - Utah</option>
                <option value="VA" <?php if ($c_state == 'VA') {echo 'selected';}?> >VA - Virginia</option>
                <option value="VI" <?php if ($c_state == 'VI') {echo 'selected';}?> >VI - US Virgin Islands</option>
                <option value="VT" <?php if ($c_state == 'VT') {echo 'selected';}?> >VT - Vermont</option>
                <option value="WA" <?php if ($c_state == 'WA') {echo 'selected';}?> >WA - Washington</option>
                <option value="WI" <?php if ($c_state == 'WI') {echo 'selected';}?> >WI - Wisconsin</option>
                <option value="WV" <?php if ($c_state == 'WV') {echo 'selected';}?> >WV - West Virginia</option>
                <option value="WY" <?php if ($c_state == 'WY') {echo 'selected';}?> >WY - Wyoming</option>
                </select>

              </div>

              </div>

            </div>

            <div class="col-sm-3" data-fields="zip"><div class="form-group field-zip">

              <label class="control-label" for="zip"><span class="label-inner">Zip Code</span>

          </label>

          <div data-editor="">

            <input id="zip" class="form-control required" name="zip" maxlength="10" type="text" value="<?php echo !empty($c_details[0]->c_zip) ? $c_details[0]->c_zip : '' ?>"/>

          </div>

        </div>

      </div>

    </div>

    <div class="row">

                    <div class="col-sm-5" data-fields="tin">

                      <div class="form-group field-tin">

                        <label class="control-label" for="c54_tin"><span class="label-inner">File To States ?</span>

                      </label>

                      <div data-editor="">

                       <div class="row">

                          <div class="col-md-4">

                            <label class="radio-inline container-radio">Yes

                            <input id="file_to_state1" class="form-control required" name="file_to_states" value="1" type="radio" <?php echo !empty($c_details[0]->c_file_to_states) == '1' ? 'checked' : '' ?>>

                            <span class="checkmark"></span>

                          </label>

                          </div>

                          <div class="col-md-4">

                            <label class="radio-inline container-radio" style="float: right !important;">No

                            <input id="file_to_state2" class="form-control required" name="file_to_states" value="0" type="radio" <?php echo !empty($c_details[0]->c_file_to_states) == '0' ? 'checked' : '' ?>>

                            <span class="checkmark"></span>

                          </label>

                          </div>
                          <div class="col-md-4" >

                              <a href="#" class="btn btn-default" style="line-height: 30px !important;" onclick="window.open('https://wagefilingplus.com/more_info', '_blank','height=600,width=600');">Yes (recommended) More Info should be listed here</a>

                            </div>

                          </div>

                      </div>

                    </div>

                  </div>

                </div>

</div></div>

</div>

<!-- <div class="panel-footer">

<button type="submit" class="btn btn-lg btn-primary btn-save">Save</button>

</div> -->

</div>

<div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">File Setup - 1099 Series - Some Optional Fields</h3>

  </div>

  <div class="panel-body">

    <div class="row">

      <div class="col-md-12" data-fields="contact">

        <label class="control-label" for="contact"><span class="label-inner">Contact</span>

    </label>

    <div data-editor="">

      <input id="contact" class="form-control" name="contact" type="text" value="<?php echo !empty($c_details[0]->c_contact) ? $c_details[0]->c_contact : '' ?>" placeholder="Optional"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12" data-fields="title">

        <label class="control-label" for="title"><span class="label-inner">Title</span>

    </label>

    <div data-editor="">

      <input id="title" class="form-control" name="title" type="text" value="<?php echo !empty($c_details[0]->c_title) ? $c_details[0]->c_title : '' ?>" placeholder="Optional"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-sm-6" data-fields="email"><div class="form-group field-email">

        <label class="control-label" for="email"><span class="label-inner">Email</span>

    </label>

    <div data-editor="">

      <input id="email" class="form-control" name="email" type="email" value="<?php echo !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '' ?>" placeholder="Optional"/>

    </div>

  </div>

</div>

<div class="col-sm-6" data-fields="phone"><div class="form-group field-phone">

  <label class="control-label" for="phone"><span class="label-inner">Phone<help style="margin-left: 10px;">(* Required - printed on form)</help></span>

</label>

<div data-editor="">

<input id="phone" class="form-control required" name="phone" type="text" value="<?php echo !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '' ?>" placeholder="Required - printed on form"/>

</div>

</div>

</div>

</div>

<div class="row">

      <div class="col-md-12">

        <label class="control-label" for="c54_email"><span class="label-inner">Fax</span>

    </label>

    <div data-editor="">

      <input id="c54_email" class="form-control" name="fax" type="fax" value="<?php echo !empty($c_details[0]->c_fax) ? $c_details[0]->c_fax : '' ?>" placeholder="Optional"/>

      <input id="form_type" class="form-control required" name="form_type" value="1099-Misc" type="hidden" />

    </div>

      </div>

    </div>

  </div>

</div>

<div class="col-md-12">

  <div class="row">

    <div class="col-md-6">
    <button type="submit" class="btn btn-lg btn-block btn-primary btn-green-bg">Save Filer</button></div>
    <?php if(isset($c_details[0])){ ?>
    <div class="col-md-6"><a href="<?=base_url('vendor');?>/<?=!empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <?php } else { ?>
      <div class="col-md-6"><a href="<?=base_url('company');?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <? } ?>

  </div>

</div>

</form>

</div>

<div class="col-md-3 hide">

<div class="alert alert-panel">

<p>If you manage W-9 requests for many companies, you can import all the companies at once.</p>

<p style="margin-top:1em"><a href="<?=base_url('import');?>" class="btn btn-default btn-hot">Use CSV Import</a></p>

</div>

</div>

</div>

</div>

</div>

</div>

<script>

$(document).ready(function () {
document.getElementById("file_to_state1").checked = true;
if($('#c54_c_state').val() !=''){

$('#c54_state').val($('#c54_c_state').val());

}
// $('#companyform').submit(function (event) {
//     //event.preventDefault();
//     // Get the Login Name value and trim it
//     var c_name = $('#c_name').val();
//     var filername2 = $('$filer_name_2').val();
//     // Check if empty of not
//     if(/^[a-zA-Z0-9 ]*$/.test(c_name) == false) {
//     alert('Filer Name allow only Alpha Numeric A-Z and 0-9.');
//     return false;
// }
// if(/^[a-zA-Z0-9 ]*$/.test(filername2) == false) {
//     alert('Filer Name 2 allow only Alpha Numeric A-Z and 0-9.');
//     return false;
// }

// });

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-z0-9\_ ]+$/i.test(value);
}, "Letters, numbers, and underscores only please");

$('#companyform').validate({ // initialize the plugin

 rules: {
            ssn_ein: {
            required: true,
            digits: true,
            minlength: 9,
            maxlength: 9,
            },
            c_name: {
            required: true,
            alphanumeric: true,
            },
            filer_name_2: {
              required : false,
              alphanumeric: true,
            },
            address: {
              required: true,
            },
            email: {
                required: false,
                email: true,
                // minlength: 7,
                // maxlength: 15,
            },
            phone: {
                required: true,
                digits : true,
                minlength: 10,
                maxlength: 10
            }
        },
      messages: {
        ssn_ein: {
        required: 'Required',
        digits: 'Not valid',
        minlength: 'Must contain nine digits with optional dashes',
        maxlength: 'Must contain nine digits with optional dashes',
        },
            c_name: {
             required: 'Required',
             alphanumeric: 'Special character are not allowed',
             },
             filer_name_2: {
              alphanumeric: 'Special character are not allowed',
             },
              address: {
                required: 'Required'
            },
              email: {
                email: 'Invalid email address'
            },
            phone: {
                required: 'Required',
                digits : 'Must be 10 digits',
                minlength: 'Must be 10 digits',
                maxlength: 'Must be 10 digits'
            }
       }
});
});

</script>