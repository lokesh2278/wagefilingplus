<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: url('../assets/images/save_as_pdf.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 50px;">
					<h3>Save Copies <span class="light">as PDF</span></h3>
				</div>
	</div>
	<div class="row"  style="padding: 40px;">
		<div class="col-md-12">
			<p class="lead">With SSN Truncation you have the option to save the recipient copies as a secure pdf to be emailed.  Below are the options that allow you to do this on your own based on the operating system you are using Windows or Mac.  If you need further help, give us a call!</p>
		</div>
		<div class="col-md-5">
			<div class="row">
				<h4>How to save copies as PDF:</h4>
			<img class="img-responsive" width="75" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/windows_flag_logo.gif">
			<p class="lead">If you're using a Windows computer and you don't have a pdf writer installed, you can download Google Chrome and it will allow you to print from your browser instantly. Once you're done you can remove it if you like.</p>
			</div>
			<div class="row">
				<img class="img-responsive" width="" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/Google_Chrome_icon_and_wordmark_(2011).jpg">
				<p class="lead"><a href="https://www.google.com/intl/en/chrome/browser/desktop/index.html#brand=CHMB&utm_campaign=en&utm_source=en-ha-na-us-sk&utm_medium=ha">Google Chrome Download Link </a><br/>
					Here's how to uninstall Google Chrome<br/>
					<a href="https://support.google.com/chrome/answer/95319?hl=en">https://support.google.com/chrome/answer/95319?hl=en</a><br/>
			    </p>
			    <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    Step 1</a>
                  </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                  <div class="panel-body">
                  	<img class="img-responsive" width="" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/Step-1-Print-Google-.jpg">
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    Step 2</a>
                  </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                  	<img class="img-responsive" width="" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/Step-2-Print-Google-.jpg">
                  </div>
                </div>
              </div>
          </div>
			</div>
		</div>
		<div class="col-md-1 border-line">
			<img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/divider.png" height="100%">
		</div>
		<div class="col-md-5">
			<div class="row">
				<div class="col-md-2 center">
					<img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/780_apple-logo.gif" width="75">
				</div>
				<div class="col-md-10">
					<p class="lead">For Mac, go to File > Print and in the Print dialog box select PDF as shown below</p>
				</div>
			</div>
			<div class="row">
				<img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/savepdf-mac.jpg">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding: 40px;">
			<hr>
			<h4>Contact Us</h4>
			<h5>WageFilingPlus. LLC.</h5>
			<h5>Telephone: 616-325-9332</h5>
			<h5>E-mail: <a>support@Wagefilingplus.com</a></h5>
			<h5>Trusted by</h5>
			<img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
