<style type="text/css">
    .testomonial-block{
       padding: 12px !important;
    }
</style>
<div id="app-container">
    <div id="page" class="">
       <div id="blaze-banner">
          <div class="container">
             <div class="navigation-container">
                <div class="wrap wrap-one-logo">
                   <div class="row">
                      <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
                         <h1><a class="company-link"><span class="company-name">How It Works</span></a></h1>
                      </div>

                   </div>
                </div>
             </div>
             <div id="alerts-container" class="alerts-container">
                <div></div>
             </div>
          </div>
       </div>
       <!-- <div class="content-container" id="main-view-content"> -->
            <section class="section-p-0">
                <div class="container">
                     <div class="row mb64 mb-xs-24">
                    <div class="col-sm-12 col-md-10 col-md-offset-1 blue-heading">
                        <h2>WageFiling makes it easy for anyone to file W-2 and 1099-MISC forms in a matter of minutes.</h2>
                        <p class="lead">Our online 1099-MISC and W-2 filing service was developed specifically to make it easy for small businesses – like yours – to report just a handful of wages when tax season rolls around. No going to the store to buy forms; no throwing surplus forms away; no mailing copies to the IRS and state.</p>

                    </div>
                </div>
                <iframe width="100%" height="727" src="https://www.youtube.com/embed/iYm6vhUUhao" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </section>
            <section class="how-east-block">
            <div class="container">
                <div class="row mb64 mb-xs-24">
                    <div class="col-sm-12 col-md-10 col-md-offset-1 blue-heading">
                        <h1>How easy is it?</h1>
                    </div>
                </div>

                <div class="row print-mail-block">
                    <div class="col-sm-4 text-center">
                        <div class="feature">
                            <div class="text-center">
                                <i style="border-color:rgba(255,255,255,0);border-width:0px;background-color:#24a9bd;box-sizing:content-box;height:48px;width:48px;line-height:48px;border-radius:50%;color:#ffffff;font-size:24px;margin-bottom:15px;" class="fontawesome-icon  fa fa-pencil-square-o circle-yes"></i>

                                <h4>Enter your W-2/1099 Data</h4>
                            </div>
                            <p class="lead">
                                Our easy to use online software makes entering W-2 / 1099 data simple and secure.
                            </p>
                        </div>

                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="feature">
                            <div class="text-center">
                                <i style="border-color:rgba(255,255,255,0);border-width:0px;background-color:#24a9bd;box-sizing:content-box;height:48px;width:48px;line-height:48px;border-radius:50%;color:#ffffff;font-size:24px;margin-bottom:15px;" class="fontawesome-icon  fa fa-envelope-o circle-yes"></i>

                                <h4>Email or mail recipient copies</h4>
                            </div>
                            <p class="lead">
                                You’ll be provided a PDF of your recipient copies for you to either mail or e-mail.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="feature">
                            <div class="text-center">
                                <i style="border-color:rgba(255,255,255,0);border-width:0px;background-color:#24a9bd;box-sizing:content-box;height:48px;width:48px;line-height:48px;border-radius:50%;color:#ffffff;font-size:24px;margin-bottom:15px;" class="fontawesome-icon  fa fa-pencil-square-o circle-yes"></i>

                                <h4>We e-file to the IRS</h4>
                            </div>
                            <p class="lead">
                               The moment you checkout we submit your transmittal's and Federal copies directly to the IRS/SSA  and your e-filing is complete
                            </p>
                        </div>

                    </div>
                </div>

            </div>

        </section>
        <section class="testomonial-block">
                <div class="container"></div>
        </section>
        <section class="help-block-home">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <div class="feature">
                            <h5 class="uppercase">Need help? Give us a call.</h5>
                            <p class="lead">English: 616-325-9332<br>Español: 361-884-1500</p>
                        </div>
                    </div>
                    <div class="col-sm-6 text-center">
                        <div class="feature">
                            <h5 class="uppercase">Headquarters</h5>
                            <p class="lead">2250 Sprucewood Ct, NE<br>Belmont, MI 49306</p>
                        </div>
                    </div>
                </div>

            </div>

        </section>
       <!-- </div> -->
    </div>
