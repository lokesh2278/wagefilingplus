<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
<!-- <a href="#" id="blaze-logos" class="">
  <img id="w9-logo" src="<?php echo iaBase(); ?>assets/images/WageFilingw9-Logo-XSmall.png" alt="WageFiling W-9">
</a>  -->

<div class="row">
  <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
    <h1><a class="company-link"><span class="company-name"><?=companyName($cid)?></span></a></h1>
  </div>
  <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3">
    <!-- <div>
    <form role="form" id="search-form">
  <label class="control-label sr-only" for="blaze-q">Type to search</label>
  <div class="search-form-group input-group">
    <input placeholder="Type to search" type="search" class="form-control" id="blaze-q" name="q">
    <span class="input-group-addon clear-button">
      <span style="display: block;" class="search-search glyphicon glyphicon-search form-control-feedback"></span>
      <span style="display:none" class="search-wait glyphicon glyphicon-refresh form-control-feedback"></span>
      <span style="display:none" class="search-found glyphicon glyphicon-ok form-control-feedback"></span>
      <span style="display:none" class="search-fail glyphicon glyphicon-remove form-control-feedback"></span>
    </span>
  </div>
</form>
</div> --></div>
</div>
<a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
</div></div>
        <div id="alerts-container" class="alerts-container"></div>
      </div>
    </div>

    <div class="content-container" id="main-view-content"><div><div class="container">
  <div class="row">
    <div class="buttons-container col-md-3 col-md-push-9" style="margin-top: 75px;"><div><div class="sidebar">
  <div class="pagination-container pull-right unpull-md unpull-lg"><div>
</div></div>
  <div class="btn-group btn-group-shift-md">
    <?php
if ($c_details[0]->FormType == '1099-Misc' || $c_details[0]->FormType == '1099-Div') {?>
    <a  class="btn btn-default btn-hot dropdown-toggle"  href="<?=base_url('request')?>/<?=$cid?>"><i class="glyphicon glyphicon-plus"></i> 1099-Misc</b></a>
    <a  class="btn btn-default btn-hot dropdown-toggle"  href="<?=base_url('div1099Req')?>/<?=$cid?>"><i class="glyphicon glyphicon-plus"></i> 1099-Div</b></a><?php
} else {?>
                    <a  class="btn btn-default btn-hot dropdown-toggle"  href="<?=base_url('w2form')?>/<?=$cid?>"><i class="glyphicon glyphicon-plus"></i> Add Payee</b></a>
                 <?php }
?>
          <a href="<?=base_url('blaze');?>" class="btn  btn-default btn-hot"><i class="glyphicon glyphicon-shopping-cart"></i> CHECKOUT</a>
          <a href="<?=base_url('blaze');?>" class="btn  btn-default btn-hot"><i class="glyphicon glyphicon-list"></i> MAIN MENU</a>

          <?php
if ($c_details[0]->FormType == '1099-Misc') {?>
                  <a href="<?=base_url('company');?>/<?=$c_details[0]->FileSequence?>" class="btn  btn-default btn-hot"><i class="glyphicon glyphicon-pencil"></i> VIEW/ CHANGE FILER</a>
              <?php } else if ($c_details[0]->FormType == '1099-Div') {?>
                <a href="<?=base_url('compdiv1099');?>/<?=$c_details[0]->FileSequence?>" class="btn  btn-default btn-hot"><i class="glyphicon glyphicon-pencil"></i> VIEW/ CHANGE FILER</a>
                <?php
} else {?>
                    <a href="<?=base_url('emp_setup');?>/<?=$c_details[0]->FileSequence?>" class="btn  btn-default btn-hot"><i class="glyphicon glyphicon-pencil"></i> VIEW/ CHANGE FILER</a>
                 <?php }
?>
  </div>
  <!-- <div class="btn-group btn-group-shift-md">
    <a type="button" href="<?=base_url('enter')?>/<?=$cid?>/manually" class="btn btn-default btn-hot dropdown-toggle" >Manually Add Form </a>
  </div> -->
  <div class="btn-group btn-group-shift-md">
    <!-- <button type="button" class="resend-all btn btn-default btn-hot">Resend Requests</button>
    <button type="button" class="dwn-pdfs btn btn-default btn-hot">Download Files</button> -->
    <!-- <button type="button" class="download-csv btn btn-default btn-hot">All Forms (CSV)</button> -->
    <!--button type="button" class="download-forms-type btn btn-default btn-hot">All Forms in one PDF</button-->
    <!-- <button type="button" class="download-forms-individual btn btn-default btn-hot">All, Separate PDFs</button> -->
  </div>
</div>
</div></div>
    <div class="col-md-9 col-md-pull-3">
     <div class="row">
        <div class="col-md-12" style="text-align: center;">
          <div class="row">
            <div class="col-md-4 col-md-offset-4">

            </div>
          </div>
          <p class="lead"><strong>Logged in as:</strong> <?=$c_details[0]->c_name?>, Form: 1099 Series</br> TaxID: <?=$c_details[0]->c_ssn_ein?>, TaxYear: <?=$c_details[0]->TaxYear?></p>
        </div>

      </div>
      <div class="table-container" style="width:100%">
        <?php //print_r($c_details[0]->FormType);die;?>
        <div class="table-responsive">
        <table class="w9-table table table-striped table-hover table-responsive DeletableTable table-bordered" name='example-table'>
<thead>
  <tr class="float-delete-button">
    <!-- <th width="1" class=""  data-tsort="disabled" data-tfilter="disabled"><button type="button" class="btn btn-xs mark-all-toggle" title="Select all" style="color: #757373!important;margin-bottom: 0;"><span class="mark-box"><i class="material-icons md-18 md-icon-done"></i></span></button></th> -->
    <th data-tsort="disabled">No</th>
    <th>Tax ID Number</th>
    <th class="sort "  data-tsort-type="string" style="white-space:nowrap"> Payee Name<!-- <button type="button" class="btn btn-xs do-delete btn-classic"><i class="material-icons md-18 md-icon-delete"></i><span class="cancel-text">Cancel</span></button><button type="button" class="btn btn-xs btn-classic start-delete">Delete…</button> --></th>
    <!-- <th class="company" data-tsort="disabled" data-tfilter="disabled">Company</th>
    <th class="sort"  data-tsort-type="string">Form&nbsp;&nbsp;</th>
    <th data-tsort="disabled" data-tfilter="disabled">Create 1099-MISC</th>
    <th class="sort"  data-tsort-type="string">Status&nbsp;&nbsp;</th>
    <th class="sort"  data-tsort-type="date" >Signed&nbsp;&nbsp;</th> -->
    <th data-tsort="disabled" data-tfilter="disabled">Form-Type</th>
  </tr>
</thead>
<tbody class="forms-container">

  <?php
//var_dump($v_details);

//var_dump($c_details[0]->FormType);die;
$companyId = isset($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '';
if (!empty($v_details) || !empty($d_details)) {

    if ($c_details[0]->FormType == '1099-Misc') {
        $counter = 0;
        // die("mmm");
        //echo "<PRE>";print_r($v_details[0]->FileSequence);

        foreach ($v_details as $v_detail) {

            ?>
  <tr data-id="<?=!empty($v_detail->misc_id) ? $v_detail->misc_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($v_detail->recepient_id_no) ? $v_detail->recepient_id_no : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><a class="edit-link" href="<?=base_url('request')?>/<?=$v_detail->FileSequence?>/<?=$v_detail->misc_id?>"><?=!empty($v_detail->first_name) ? $v_detail->first_name : '';?></a><?php if ($v_detail->void == 1) {
                    echo '-void-';
                }
                if ($v_detail->corrected == 1) {
                    echo '-corr-';
                }
                ?></td>
    <?php }?>
    <td><?=!empty($v_detail->FormType) ? $v_detail->FormType : '';?></td>
  </tr>
  <?php }
        foreach ($d_details as $d_detail) {

            ?>
  <tr data-id="<?=!empty($d_detail->div_id) ? $d_detail->div_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($d_detail->recepient_id_no) ? $d_detail->recepient_id_no : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><a class="edit-link" href="<?=base_url('div1099Req')?>/<?=$d_detail->FileSequence?>/<?=$d_detail->div_id?>"><?=!empty($d_detail->first_name) ? $d_detail->first_name : '';?></a><?php if ($d_detail->void == 1) {
                    echo '-void-';
                }
                if ($d_detail->corrected == 1) {
                    echo '-corr-';
                }
                ?></td>
    <?php }?>
    <td><?=!empty($d_detail->FormType) ? $d_detail->FormType : '';?></td>
  </tr>
  <?php }

    }
    //for w2_form
    else if ($c_details[0]->FormType == 'W-2') {
        // print_r('w2');die;
        $counter = 0;
        foreach ($v_details as $v_detail) {

            ?>
    <tr data-id="<?=!empty($v_detail->w2_id) ? $v_detail->w2_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($v_detail->SSN) ? $v_detail->SSN : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><a class="edit-link" href="<?=base_url('w2form')?>/<?=$v_detail->FileSequence?>/<?=$v_detail->w2_id?>"><?=!empty($v_detail->FName) ? $v_detail->FName . ' ' . $v_detail->LName : '';?></a><?php if ($v_detail->voidbox == 1) {
                    echo '-void-';
                }
                ?></td>
    <?php }?>
    <td><?=!empty($v_detail->FormType) ? $v_detail->FormType : '';?></td>
  </tr>

  <?php }
    } else if (trim($c_details[0]->FormType) == '1099-Div') {
        //die("lovcxvxczv");

        $counter = 0;
        foreach ($d_details as $d_detail) {

            ?>
  <tr data-id="<?=!empty($d_detail->div_id) ? $d_detail->div_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($d_detail->recepient_id_no) ? $d_detail->recepient_id_no : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><a class="edit-link" href="<?=base_url('div1099Req')?>/<?=$d_detail->FileSequence?>/<?=$d_detail->div_id?>"><?=!empty($d_detail->first_name) ? $d_detail->first_name : '';?></a><?php if ($d_detail->void == 1) {
                    echo '-void-';
                }
                if ($d_detail->corrected == 1) {
                    echo '-corr-';
                }
                ?></td>
    <?php }?>
    <td><?=!empty($d_detail->FormType) ? $d_detail->FormType : '';?></td>
  </tr>
  <?php }
        foreach ($v_details as $v_detail) {

            ?>
  <tr data-id="<?=!empty($v_detail->misc_id) ? $v_detail->misc_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($v_detail->recepient_id_no) ? $v_detail->recepient_id_no : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><a class="edit-link" href="<?=base_url('request')?>/<?=$v_detail->FileSequence?>/<?=$v_detail->misc_id?>"><?=!empty($v_detail->first_name) ? $v_detail->first_name : '';?></a><?php if ($v_detail->void == 1) {
                    echo '-void-';
                }
                if ($v_detail->corrected == 1) {
                    echo '-corr-';
                }
                ?></td>
    <?php }?>
    <td><?=!empty($v_detail->FormType) ? $v_detail->FormType : '';?></td>
  </tr>
  <?php }
    }
} else {?>
  <tr>
    <td colspan="7">Nothing to show</td>
  </tr>
  <?php }?>
</tbody>
<tfoot>
  <tr>
    <?php
if ($v_detail->FormType == 'W-2') {?>
    <td class="totlaForm" colspan="4"><a href="<?=base_url('frontend/vendorTotlaShow')?>/<?php echo $companyId; ?>">W-2 Totals</a></td>
    <?php } else {?>
    <td class="totlaForm" colspan="2"><a href="<?=base_url('frontend/vendorTotlaShow')?>/<?php echo $companyId; ?>">1099-Misc Totals</a></td>
    <td class="totlaForm" colspan="2"><a href="<?=base_url('frontend/vendorDivTotal')?>/<?php echo $companyId; ?>">1099-Div Totals</a></td>
    <?php }?>
  </tr>
</tfoot>
</table>
</div>
</div>
    </div>
  </div>
</div>
</div></div>

     </div>
  <div class="modal-container"></div>
</div>

<form action="<?php echo base_url() . 'frontend/downloadPdfs'; ?>" method="post" id="tempdfform">

</form>
<script>
         $(document).ready(function() {

        $('table[name=example-table]').tableFilter({

          //input : "input[type=search]", Default element

         /* trigger : {

            event   : "keyup",
            //element : "button[name=btn-filtro]"
          },*/

          //timeout: 80,

          sort : true,

          //caseSensitive : false, Default

          callback : function() { /* Callback após o filtro */

          },

          notFoundElement : ".not-found"
        });

         $('.download-csv').click(function(){
         $(this).attr('disabled','disabled');
         $('#alerts-container').html('');
           var dataNew = { type :'csv',c_id : '<?=$cid?>' };
           var baseUrl = '<?php echo base_url('vendordownload') ?>';
           $.ajax({
              url: baseUrl,
              type: "POST",
              data: dataNew,
              success: function(data) {
                var obj = $.parseJSON(data);
                $('.download-csv').removeAttr('disabled');
                if(obj.status == 'success')
                {
                  var url = '<?=iaBase()?>'+obj.path;
                  var a = document.createElement("a");
                  a.style = "display: none";
                  a.href = url;
                  a.download = obj.name;
                  document.body.appendChild(a);
                  a.click();
                  window.URL.revokeObjectURL(url);
                }else{
                  //$('#alerts-container').html('<div class="alert-success"> No Data Found. </div>');
                  notiCustom('No Data Found', 'warning');
                }
              }
            });
        })

        $('.download-forms-individual').click(function(){
        $(this).attr('disabled','disabled');
        $('#alerts-container').html('');
           var dataNew = { type :'spdf',c_id : '<?=$cid?>' };
           var baseUrl = '<?php echo base_url('vendordownload') ?>';
           $.ajax({
              url: baseUrl,
              type: "POST",
              data: dataNew,
              success: function(data) {
                var obj = $.parseJSON(data);
                $('.download-forms-individual').removeAttr('disabled');
                if(obj.status == 'success')
                {
                  var url = '<?=iaBase()?>'+obj.path;
                  var a = document.createElement("a");
                  a.style = "display: none";
                  a.href = url;
                  a.download = obj.name;
                  document.body.appendChild(a);
                  a.click();
                  window.URL.revokeObjectURL(url);
                }else{
                  //$('#alerts-container').html('<div class="alert-success"> No Data Found. </div>');
                  notiCustom('No Data Found', 'warning');
                }
              }
            });
        })

      $('.resend-all').click(function(){
        var cont_ids = [];
        if($('tr.row-marked').length > 0) {
        $(this).attr('disabled','disabled');
          $('tr.row-marked').each(function() {
            cont_ids.push($(this).attr('data-id'));
          });
          console.log(cont_ids);
          var dataNew = {
                          type :'resend',
                          c_id : '<?=$cid?>',
                          cont_ids: cont_ids,
                        };
          var baseUrl = '<?php echo base_url('vendordownload') ?>';
          $.ajax({
            url: baseUrl,
            type: "POST",
            data: dataNew,
            success: function(data) {
              $('.resend-all').removeAttr('disabled');
              var obj = $.parseJSON(data);
              if(obj.status == 'success') {
                notiCustom('Resend Email Requests', 'success');
              }
            }
          });
        } else {
          notiCustom('Select atleast one contractor.', 'info');
        }
      });

$('.dwn-pdfs').on('click', function() {
  //$files = [];
  $('form#tempdfform').html('');
  $('input[name="formPdf[]"]').each(function() {
    if($(this).prop('checked')) {
      //$files.push($(this).val());
      $('form#tempdfform').append('<input type="hidden" name="files[]" value="'+$(this).val()+'">');
    }
  });
  if($('input[name="files[]"]').length > 0) {
    $('form#tempdfform').submit();
  } else {
    notiCustom('Files not selected', 'warning');
  }
});


   //$("table").tablesorter();
   $('.start-delete').click(function(){
      $(this).hide();
      $('.do-delete').show();
      $('.table-delete-mode').show();
   });

$('.mark-all-toggle').click(function(){
  if($(this).hasClass( "all-marked" ))
  {
      $(this).removeClass('all-marked');
      $('.do-delete').removeClass('btn-danger delete-active');
      $('.table tbody tr').removeClass('row-marked');
      $('.do-delete').addClass('btn-classic');
  }else{
      $(this).addClass('all-marked');
      $('.do-delete').addClass('btn-danger delete-active');
      $('.table tbody tr').each(function() {
        if($(this).find('td:first').html() != '') {
          $(this).addClass('row-marked');
        }
      });
      $('.do-delete').removeClass('btn-classic');

  }

});


/*$('button.delete-active').click(function(){
  var deleteIds = [];
  $('.row-marked').each(function() {
             deleteIds.push($(this).attr('data-id'));
             $(this).remove();
    });
  var dataNew = {deleteIds : deleteIds, table :'vendor'};
  var baseUrl = '<?php //echo base_url('delete') ?>';
     $.ajax({
        url: baseUrl,
        type: "POST",
        data: dataNew,
        success: function(data) {

        }
      });
 });*/


 $('button.mark-box').click(function(){
   if($(this).parents('tr').hasClass( "row-marked" ))
   {
      $(this).parents('tr').removeClass('row-marked');
      if($('tr.row-marked').length == 0)
       {
        $('.do-delete').removeClass('btn-danger delete-active');
        $('.mark-all-toggle').removeClass('all-marked');
        $('.do-delete').addClass('btn-classic');
       }

   }else{
       $(this).parents('tr').addClass('row-marked');
       $('.do-delete').addClass('btn-danger delete-active');
       $('.do-delete').removeClass('btn-classic');
       /*$('button.delete-active').click(function(){
       var deleteIds = [];
       $('.row-marked').each(function() {
               deleteIds.push($(this).attr('data-id'));
               $(this).remove();
       });
       var dataNew = {deleteIds : deleteIds, table :'vendor'};
       var baseUrl = '<?php //echo base_url('delete') ?>';
       $.ajax({
          url: baseUrl,
          type: "POST",
          data: dataNew,
          success: function(data) {

          }
        });
   });*/
   }
 });
$('.do-delete').click(function(){
      $(this).hide();
      $('.start-delete').show();
      $('.table-delete-mode').hide();
   });
});
</script>