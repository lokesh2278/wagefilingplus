<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
</style>
<div class="container">
	<div class="row header-box" style="background-image: url('../assets/images/tinmatching.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 30px;">
					<h3>Bulk Tin <span class="light">Checking Services</span></h3>
					<h5>Check Tax ID's for Contractors or Vendors in Seconds!</h5>
				</div>
	</div>
	<div class="row" style="padding: 40px;">
		<div class="col-md-12">
			<p class="lead">Our Service Bureau is approved by the IRS for "BULK TIN/Name Matching program". "Bulk TIN/Name Matching" verifies the SSN or EIN is correct and matches the person's name or company name as shown on the application when the SSN or EIN was applied for. Any mismatch can result in a notice or possible penalty of $100 per mismatch from the IRS.</p>
            <table class="table table-bordered">
				<thead style="background-color: #337ab7; color: #ffffff;">
					<tr>
						<th>MIS-MATCHED / WRONG TAX ID</th>
						<th>NEW PENALTY</th>
						<th>NEW MAX.</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Tier 1(30 Days)</td><td>$30 per form</td><td>$250,000</td>
					</tr>
					<tr>
						<td>Tier 2(60 Days)</td><td>$60 per form</td><td>$500,00</td>
					</tr>
					<tr>
						<td>Tier 3(August)</td><td>$100 per form</td><td>$1,500.00</td>
					</tr>
					<tr>
						<td>Intentional Disregard</td><td>$250 per form</td><td>No Limit</td>
					</tr>
				</tbody>
			</table>
			</div>
			<div class="col-md-12">
				<div class="row">
								<div class="col-md-6">
								<h4>Our Labor Includes:</h4>
								<p class="lead">Receiving your spreadsheet from our secure server and setting up a job name, formatting your spreadsheet or 1099 Express source file to IRS specifications for Bulk TIN Matching (including filtering out invalid characters, removing duplicate Tax ID numbers), performing the upload to the IRS, waiting for the IRS response, downloading and receiving IRS results, converting the results into user friendly text file you can understand with IRS codes and additional detailed explanations to make corrections easier, uploading the results to our secure server, notifying you by e-Mail that a special file is available for you. </p>
								<p class="lead">The turn-around time is often about 6 hours or less,  but can be 24 to 48 hours.</p>
					            <p class="lead">Pricing: We perform all labor for a fee of $74.00 per file up to 10,000 names per file. To get started contact us for details and special pricing.</p>
					            <p class="lead">For a detailed guide on uploading files click the links below:</p>
											<table class="table table-bordered" style="text-align: center; font-size: 16px;">
												<tbody>
													<tr>
														<td>
															<!-- <img src="<?php //echo iaBase(); ?>assets/images/gavel-icon-3.png" width="35px" height="36px"> -->
															<a href="assets/pdf/Instructions-for-Wagefiling-Bulk-TIN-Matching-New.pdf" target="_blank">Submitting Files To How</a>
														</td>
													</tr>
													<tr>
														<td>
															<!-- <img src="<?php //echo iaBase(); ?>assets/images/gavel-icon-3.png" width="35px" height="36px"> -->
															<a href="assets/pdf/Bulk-Tin-Results-Sample-Wagefiling.pdf" target="_blank">View Sample Report</a>
														</td>
													</tr>
												</tbody>
											</table>
									</div>
									<div class="col-md-6">
										<iframe src="https://wagefiling.securevdr.com/remoteupload/remoteuploadform.aspx?id=746570a5-aa34-4a8e-a187-65d63295f522" frameborder="0" width="100%" height="750px" scrolling="auto" id="sfRemoteUploadFrame"></iframe>
									</div>
				</div>
			</div>
            <hr>
            <h4>Contact Us</h4>
            <h5>WageFilingPlus. LLC.</h5>
            <h5>Telephone: 616-325-9332</h5>
            <h5>E-mail: <a href="mailto:support@WageFilingPlus.com">support@WageFilingPlus.com</a></h5>
            <h5>Trusted by</h4>
            <img class="img-responsive" width="100%" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/CLOGO.gif">
		</div>
	</div>
</div>
