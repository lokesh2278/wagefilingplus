<div id="blaze-banner">
  <div class="container">
    <div class="navigation-container">
      <div class="wrap wrap-one-logo text-center">
        <a class="back-link" href="<?=base_url('blaze');?>" style="display: block; margin-left: 128px; "><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
        <div class="row">
          <div id="nav-company" class="col-sm-12 col-md-12 col-lg-12">
            <h1 style="font-size: 29px; "><a class="company-link"><span class="company-name">Step 1 Enter the information of the Company Requesting The W-9.</span></a></h1>
          </div>
          <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>
        </div>
      </div>
    </div>
    <div id="alerts-container" class="alerts-container"></div>
  </div>
</div>
<div class="content-container" id="main-view-content">
  <div>
    <div class="container">
      <div class="row">
      <?php //print_r($c_details[0]->FileSequence);?>
        <div class="col-md-8 col-md-offset-2">
          <form id="companyform" action="companyW9s" accept-charset="UTF-8" method="post">
            <input id="c54_c_id" type="hidden" name="c_id" value="<?php echo !empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>">
            <div class="panel panel-primary">
              <div class="panel-body panel-body-extra">
                <div class="form-container">
                  <div id="form">
                    <div class="row">
                      <div class="col-sm-12" data-fields="name">
                        <div class="form-group field-name">
                          <label class="control-label" for="c54_name"><span class="label-inner">Company Name</span>
                        </label>
                        <div data-editor="">
                          <!-- <input id="c54_name" class="form-control" name="c_name" maxlength="40" type="text" value="<?php //echo !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '' ?>" required> -->
                          <?php
$companyId = isset($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '';
if ($companyId == '') {
    ?>

                          <select id="c_name" class="form-control" onchange="yesnoCheck(this);"  name="c_name">
                          <option value="" selected>Select Company</option>
                          <option value="other">Add Another</option>
                            <?php foreach ($company as $k => $v) {?>
                            <option value="<?php print_r($v->FileSequence);?>" <?php if ($v->FileSequence == $companyId && !empty($companyId)) {echo 'selected';}?> <?php if ($v->Paid == 1) {echo 'disabled';}?>><?php print_r($v->FileName.' '.$v->TaxYear);?><?php if ($v->Paid == 1) {echo ' ' . '(Paid)';}?></option>
                            <?php }?>
                          </select>
                        <?php } else {?>
                        <input type="text" class="form-control" name="c_name" id="c_name" value="<?php print_r($c_details[0]->FileName);?>" disabled />
                    <?php }?>
                        </div>
                      </div>
                      <input type="hidden" name="cname_new" id="cname_new" />
                      <div class="form-group" id="ifYes" style="display: none;">
                        <div>
                            <input type="text" id="c54_name" name="c_name"  class="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-5" data-fields="tin">
                      <div class="form-group field-tin">
                        <label class="control-label" for="c54_tin"><span class="label-inner">Company EIN</span></label>
                      <div data-editor="">
                        <input id="c54_tin" class="form-control" name="c_tin" maxlength="11" type="text" value="<?php echo !empty($c_details[0]->c_employee_ein) ? $c_details[0]->c_employee_ein : '' ?>" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12" data-fields="address">
                    <div class="form-group field-shipping_address">
                      <label class="control-label" for="c54_address"><span class="label-inner">Address</span>
                    </label>
                    <div data-editor="">
                      <input id="c54_address" class="form-control" name="c_address" maxlength="40" type="text" value="<?php echo !empty($c_details[0]->c_address) ? $c_details[0]->c_address : '' ?>" required>
                    </div>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="col-sm-12" data-fields="shipping_address">
                    <div class="form-group field-shipping_address">
                      <label class="control-label" for="c54_shipping_address"><span class="label-inner">Street</span>
                    </label>
                    <div data-editor="">
                      <input id="c54_shipping_address" class="form-control" name="c_street" maxlength="40" type="text" value="<?php echo !empty($c_details[0]->c_street) ? $c_details[0]->c_street : '' ?>" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-5" data-fields="city">
                  <div class="form-group field-city">
                    <label class="control-label" for="c54_city"><span class="label-inner">City</span></label>
                  <div data-editor="">
                    <input id="c54_city" class="form-control" name="c_city" maxlength="40" type="text" value="<?php echo !empty($c_details[0]->c_city) ? $c_details[0]->c_city : '' ?>" required>
                  </div>
                </div>
              </div>
              <div class="col-sm-4" data-fields="state"><div class="form-group field-state">
              <label class="control-label" for="c54_state"><span class="label-inner">State</span></label>
            <div data-editor="">
              <select id="c54_state" class="form-control" name="c_state" required>
              <?php $c_state = !empty($c_details[0]->c_state) ? $c_details[0]->c_state : '';?>
                <option value="">Choose...</option>
                <option value="AA" <?php if ($c_state == 'AA') {echo 'selected';}?> >AA - Armed Forces Americas</option>
                <option value="AE" <?php if ($c_state == 'AE') {echo 'selected';}?> >AE - Armed Forces Europe</option>
                <option value="AK" <?php if ($c_state == 'AK') {echo 'selected';}?> >AK - Alaska</option>
                <option value="AL" <?php if ($c_state == 'AL') {echo 'selected';}?> >AL - Alabama</option>
                <option value="AP" <?php if ($c_state == 'AP') {echo 'selected';}?> >AP - Armed Forces Pacific</option>
                <option value="AR" <?php if ($c_state == 'AR') {echo 'selected';}?> >AR - Arkansas</option>
                <option value="AS" <?php if ($c_state == 'AS') {echo 'selected';}?> >AS - American Samoa</option>
                <option value="AZ" <?php if ($c_state == 'AZ') {echo 'selected';}?> >AZ - Arizona</option>
                <option value="CA" <?php if ($c_state == 'CA') {echo 'selected';}?> >CA - California</option>
                <option value="CO" <?php if ($c_state == 'CO') {echo 'selected';}?> >CO - Colorado</option>
                <option value="CT" <?php if ($c_state == 'CT') {echo 'selected';}?> >CT - Connecticut</option>
                <option value="DC" <?php if ($c_state == 'DC') {echo 'selected';}?> >DC - District of Columbia</option>
                <option value="DE" <?php if ($c_state == 'DE') {echo 'selected';}?> >DE - Delaware</option>
                <option value="FL" <?php if ($c_state == 'FL') {echo 'selected';}?> >FL - Florida</option>
                <option value="FM" <?php if ($c_state == 'FM') {echo 'selected';}?> >FM - Federated Micronesia</option>
                <option value="GA" <?php if ($c_state == 'GA') {echo 'selected';}?> >GA - Georgia</option>
                <option value="GU" <?php if ($c_state == 'GU') {echo 'selected';}?> >GU - Guam</option>
                <option value="HI" <?php if ($c_state == 'HI') {echo 'selected';}?> >HI - Hawaii</option>
                <option value="IA" <?php if ($c_state == 'IA') {echo 'selected';}?> >IA - Iowa</option>
                <option value="ID" <?php if ($c_state == 'ID') {echo 'selected';}?> >ID - Idaho</option>
                <option value="IL" <?php if ($c_state == 'IL') {echo 'selected';}?> >IL - Illinois</option>
                <option value="IN" <?php if ($c_state == 'IN') {echo 'selected';}?> >IN - Indiana</option>
                <option value="KS" <?php if ($c_state == 'KS') {echo 'selected';}?> >KS - Kansas</option>
                <option value="KY" <?php if ($c_state == 'KY') {echo 'selected';}?> >KY - Kentucky</option>
                <option value="LA" <?php if ($c_state == 'LA') {echo 'selected';}?> >LA - Louisiana</option>
                <option value="MA" <?php if ($c_state == 'MA') {echo 'selected';}?> >MA - Massachusetts</option>
                <option value="MD" <?php if ($c_state == 'MD') {echo 'selected';}?> >MD - Maryland</option>
                <option value="ME" <?php if ($c_state == 'ME') {echo 'selected';}?> >ME - Maine</option>
                <option value="MH" <?php if ($c_state == 'MH') {echo 'selected';}?> >MH - Marshall Islands</option>
                <option value="MI" <?php if ($c_state == 'MI') {echo 'selected';}?> >MI - Michigan</option>
                <option value="MN" <?php if ($c_state == 'MN') {echo 'selected';}?> >MN - Minnesota</option>
                <option value="MO" <?php if ($c_state == 'MO') {echo 'selected';}?> >MO - Missouri</option>
                <option value="MP" <?php if ($c_state == 'MP') {echo 'selected';}?> >MP - N. Mariana Islands</option>
                <option value="MS" <?php if ($c_state == 'MS') {echo 'selected';}?> >MS - Mississippi</option>
                <option value="MT" <?php if ($c_state == 'MT') {echo 'selected';}?> >MT - Montana</option>
                <option value="NC" <?php if ($c_state == 'NC') {echo 'selected';}?> >NC - North Carolina</option>
                <option value="ND" <?php if ($c_state == 'ND') {echo 'selected';}?> >ND - North Dakota</option>
                <option value="NE" <?php if ($c_state == 'NE') {echo 'selected';}?> >NE - Nebraska</option>
                <option value="NH" <?php if ($c_state == 'NH') {echo 'selected';}?> >NH - New Hampshire</option>
                <option value="NJ" <?php if ($c_state == 'NJ') {echo 'selected';}?> >NJ - New Jersey</option>
                <option value="NM" <?php if ($c_state == 'NM') {echo 'selected';}?> >NM - New Mexico</option>
                <option value="NV" <?php if ($c_state == 'NV') {echo 'selected';}?> >NV - Nevada</option>
                <option value="NY" <?php if ($c_state == 'NY') {echo 'selected';}?> >NY - New York</option>
                <option value="OH" <?php if ($c_state == 'OH') {echo 'selected';}?> >OH - Ohio</option>
                <option value="OK" <?php if ($c_state == 'OK') {echo 'selected';}?> >OK - Oklahoma</option>
                <option value="OR" <?php if ($c_state == 'OR') {echo 'selected';}?> >OR - Oregon</option>
                <option value="PA" <?php if ($c_state == 'PA') {echo 'selected';}?> >PA - Pennsylvania</option>
                <option value="PR" <?php if ($c_state == 'PR') {echo 'selected';}?> >PR - Puerto Rico</option>
                <option value="PW" <?php if ($c_state == 'PW') {echo 'selected';}?> >PW - Palau</option>
                <option value="RI" <?php if ($c_state == 'RI') {echo 'selected';}?> >RI - Rhode Island</option>
                <option value="SC" <?php if ($c_state == 'SC') {echo 'selected';}?> >SC - South Carolina</option>
                <option value="SD" <?php if ($c_state == 'SD') {echo 'selected';}?> >SD - South Dakota</option>
                <option value="TN" <?php if ($c_state == 'TN') {echo 'selected';}?> >TN - Tennessee</option>
                <option value="TX" <?php if ($c_state == 'TX') {echo 'selected';}?> >TX - Texas</option>
                <option value="UT" <?php if ($c_state == 'UT') {echo 'selected';}?> >UT - Utah</option>
                <option value="VA" <?php if ($c_state == 'VA') {echo 'selected';}?> >VA - Virginia</option>
                <option value="VI" <?php if ($c_state == 'VI') {echo 'selected';}?> >VI - US Virgin Islands</option>
                <option value="VT" <?php if ($c_state == 'VT') {echo 'selected';}?> >VT - Vermont</option>
                <option value="WA" <?php if ($c_state == 'WA') {echo 'selected';}?> >WA - Washington</option>
                <option value="WI" <?php if ($c_state == 'WI') {echo 'selected';}?> >WI - Wisconsin</option>
                <option value="WV" <?php if ($c_state == 'WV') {echo 'selected';}?> >WV - West Virginia</option>
                <option value="WY" <?php if ($c_state == 'WY') {echo 'selected';}?> >WY - Wyoming</option>
                </select>
                </div>
              </div>
            </div>
            <div class="col-sm-3" data-fields="zip"><div class="form-group field-zip">
            <label class="control-label" for="c54_zip"><span class="label-inner">Zip Code</span></label>
          <div data-editor="">
            <input id="c54_zip" class="form-control" name="c_zip" maxlength="10" type="text" value="<?php echo !empty($c_details[0]->c_zip) ? $c_details[0]->c_zip : '' ?>">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-9" data-fields="email"><div class="form-group field-email">
      <label class="control-label" for="c54_email"><span class="label-inner">Email (for direct questions by W-9 Recipient)</span></label>
    <div data-editor="">
      <input id="c54_email" class="form-control" name="c_email" type="email" value="<?php echo isset($c_details[0]->c_email) ? $c_details[0]->c_email : ''; ?>">
    </div>
  </div>
</div>
<div class="col-sm-3" data-fields="telephone"><div class="form-group field-telephone">
<label class="control-label" for="c54_telephone"><span class="label-inner">Phone</span></label>
<div data-editor="">
<input id="c54_telephone" class="form-control" name="c_telephone" maxlength="15" type="text" value="<?php echo !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '' ?>">
<input type="hidden" name="requested_id" id="requested_id" />
</div>
</div>
</div>
</div>
</div></div>
</div>
<div class="panel-footer">
<p class="lead" id="result"></p>
<button type="submit" value="submit" name="w9s" class="btn btn-lg btn-primary btn-save">Save</button>
</div>
</div>
</form>
</div>
<div class="col-md-3 hide">
<div class="alert alert-panel">

<p>If you manage W-9 requests for many companies, you can import all the companies at once.</p>
<p style="margin-top:1em"><a href="<?=base_url('import');?>" class="btn btn-default btn-hot">Use CSV Import</a></p>
</div>
</div>
</div>
</div>
</div></div>
<script>
 function yesnoCheck(that) {
  var url = "<?php echo base_url(); ?>";
   var id = that.value;
              $.ajax({
                      type: "GET",
                      url: url + "frontend/File_ajax/"+ id,
                      dataType: 'json',
                       success: function(data)
                      {
                          $("#c54_name").val(data.FileName);
                          $("#cname_new").val(data.FileName);
                          $("#c54_tin").val(data.c_ssn_ein);
                          $("#c54_address").val(data.c_address);
                          $("#c54_shipping_address").val(data.c_street);
                          $("#c54_city").val(data.c_city);
                          $("#c54_state").val(data.c_state);
                          $("#c54_zip").val(data.c_zip);
                          $("#c54_email").val(data.c_email);
                          $("#c54_telephone").val(data.c_telephone);
                          $("#requested_id").val(data.FileSequence);
                      }
                  });


        if (that.value == "other") {
           // alert("check");
            document.getElementById("ifYes").style.display = "block";
            $("#cname_new").val('');
            $("#c_name").attr('name','selection');
            $("#c54_name").attr('name','c_name');
            $("#c54_name").val('');
            $("#c54_tin").val('');
            $("#c54_address").val('');
            $("#c54_shipping_address").val('');
            $("#c54_city").val('');
            $("#c54_state").val('');
            $("#c54_zip").val('');
            $("#c54_email").val('');
            $("#c54_telephone").val('');
            $("#requested_id").val('');
        } else {
            document.getElementById("ifYes").style.display = "none";
            $("#c_name").attr('name','c_name');
              //alert(that.value);
        }

    }
$(document).ready(function () {
  var comp = $("#c_name").val();
  if(comp != "other"){
    $("#c54_name").attr('name','selection');
  }

$('#companyform').validate({ // initialize the plugin
rules: {
name: {
	required: true,
},
tin: {
required: true,
	digits: true,
	minlength: 9,
	maxlength: 9,
},
address: {
  required: true,
},
	shipping_address: {
	required: true,
},
city: {
required: true,
},
state :{
required: true,
},
zip:{
required: true,
digits: true,
minlength: 5,
maxlength: 5,
},
email: {
required: true,
email: true,
},
telephone :{
required: true,
digits: true,
minlength: 10,
maxlength: 10,
},
},
messages: {
name: {
required: 'Required',
},
tin: {
required: 'Required',
digits: 'Not valid',
minlength: 'Must contain nine digits with optional dashes',
maxlength: 'Must contain nine digits with optional dashes',
},
address: {
required: 'Required',
},
shipping_address: {
required: 'Required',
},
city: {
required: 'Required',
},
state :{
required: 'Required',
},
zip:{
required: 'Required',
digits: 'Must be a valid U.S. ZIP code',
minlength: 'Must be a valid U.S. ZIP code',
maxlength: 'Must be a valid U.S. ZIP code',
},
email: {
required: 'Required',
email: 'Invalid email address',
},
telephone :{
required: 'Required',
digits: 'Must be 10 digits',
minlength: 'Must be 10 digits',
maxlength: 'Must be 10 digits',
},
}
});
});
</script>