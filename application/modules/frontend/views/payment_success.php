   <!-- Begin page content -->
    <div class="container">
        <div class="row">
            <!-- <div class="col-sm-4"></div> -->
            <div class="col-md-4 col-md-offset-4">
                <div class="card">
                    <img class="card-imgs-top" src="https://cdn.dribbble.com/users/411286/screenshots/2619563/desktop_copy.png" alt="Card image cap" width="349" height="250">
                    <div class="card-block" style="padding: 20px;">
                        <h4 class="card-title">Payment Successful!</h4>
                        <p class="card-text">Your payment has been processed and a copy of the copy of the reciept has been emailed to you for your records.</p>
                        <p class="card-text">Please click on the Main Menu button to return to your account.</p>
                        <a href="<?=base_url('blaze');?>" style="color:#ffffff !important;" class="btn btn-primary btn-sm float-right">Main Menu</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

