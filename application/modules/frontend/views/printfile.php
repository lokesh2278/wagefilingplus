<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title><?=$c_details[0]->FileName?></title> <!-- Print3m.asp -->
<style>

@media print {
	.pagebreak { display: block; page-break-before: always; }
}
.indent1  {font-Family: arial; font-size: 14px; text-align: left; margin-left:30px}
.sm {font-Family: arial; font-size: 9px; text-align: left;}
.big {font-family: ARIAL; font-size: 13px; margin-left:8; margin-top: 0; margin-bottom: 0}
.med {font-family: ARIAL; font-size: 15px; margin-left:8; margin-top:-4px;
      PADDING-BOTTOM: 0px;}
.inst {font-Family: arial; font-size: 9px; text-align: left; margin-top: 0; margin-bottom: -0; line-height:9px;}
</style>

</head>
<body>

<?php
if (!empty($v_details)) {
    foreach (array_filter($v_details) as $v_detail) {
        // print_r($v_detail);die;
        ?>
<table width=100%>
<tr>
<td width=60%>
<p class=indent1>
<br>

       <?=$c_details[0]->FileName?><br>
       <?=$c_details[0]->c_address?>, <?=$c_details[0]->c_address_2?><br>
       <?=$c_details[0]->c_city?>, <?=$c_details[0]->c_state?> <?=$c_details[0]->c_zip?>
<!-- Legend -->
<br><br><br><br><br><br><br>
<table border="2" cellpadding="0" cellspacing="0" bordercolor="#000000">
      <tr ><td width="100%"  align="center">
     <font face="Arial" size="3">&nbsp;Important Tax Return Document Enclosed&nbsp;&nbsp;</font>
    </td>      </tr>
</table>

		<?=$v_detail[0]->first_name?><br>
		<?=$v_detail[0]->address?><br>
		<?=$v_detail[0]->city?>, <?=$v_detail[0]->state?> <?=$v_detail[0]->zipcode?>
<br><br><br><br>
<!-- End of Mailing Lables -->
</p>
</td>


<!-- Instructions at the Top Right -->
<td width=40% valign=top>
<?php if ($copyprint == 1) {?>
<p style="font-Family: arial; font-size: 12px; text-align: left; margin-top: 3; margin-bottom: 3">

<b>Instructions for Recipients</b>
</p>
<p class="inst">
Account number. May show an account or other unique number the payer
assigned to distinguish your account. Amounts shown may be subject to
self-employment (SE) tax. If your net income from self-employment is
$400 or more, you must file a return and compute your SE tax on
Schedule SE (Form 1040). See Pub. 334, Tax Guide for Small Business,
for more information. If no income or social security and Medicare
taxes were withheld and you are still receiving these payments, see
Form 1040-ES, Estimated Tax for Individuals. Individuals must report
as explained for box 7 below. Corporations, fiduciaries, or partner-
ships report the amounts on the proper line of your return. For 2008:<br>
<br>
</p>
<?php } else {?>

<?php }?>
</td>
</tr>
</table>


<div class=sm>
<center>
- - - - - - - - - - - - - - - - - - - - - Fold - - - - - - - - - - - - - - - - - - - - -
</center>
</div>
<br>
<!-- 1099-Misc Forms -->


<table border="2" width="100%" cellspacing="0" bordercolor="#000000" height="1"
     style="font-family: Arial; font-size: 10px">
	<tr>
		<td width="44%" height="1" colspan="4" rowspan="3" valign="top">
      &nbsp;PAYER'S name, street address, city, state and ZIP code
<p class=big>
       <?=$c_details[0]->FileName?><br>
       <?=$c_details[0]->c_address?>, <?=$c_details[0]->c_address_2?><br>
       <?=$c_details[0]->c_city?>, <?=$c_details[0]->c_state?> <?=$c_details[0]->c_zip?>
</p>


       </td>
		<td width="18%" height="1" valign="top">&nbsp;1 Rents<p class=big><?php if (!empty($v_detail[0]->rents)) {echo '$' . ' ' . $v_detail[0]->rents;} else {echo " ";}?></p></td>

		<td width="14%" height="1" rowspan="2" valign="top">
		<center>
         OMB No. 1545-0115<br>
		<h4 style="font-size: 32px; -webkit-margin-before: 0.2em !important;-webkit-margin-after: 0.2em !important;"><?=$c_details[0]->TaxYear?></h4><br>
         <b><font size="2"> 1099-MISC</font></b>
         </center>
        </td>


		<td width="20%" height="1" colspan="2" rowspan="2" align="right"
		style="font-family: Arial; font-size: 18px;">
		Miscellaneous&nbsp;&nbsp; <br>Income&nbsp;&nbsp; </td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">2 Royalties<p class=big><?php if (!empty($v_detail[0]->royalties)) {echo '$' . ' ' . $v_detail[0]->royalties;} else {echo " ";}?></p></td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">3 Other Income<p class=big><?php if (!empty($v_detail[0]->other_income)) {echo '$' . ' ' . $v_detail[0]->other_income;} else {echo " ";}?></p></td>
		<td width="19%" height="1" colspan="2" valign="top">4 Fed income tax withheld<p class=big><?php if (!empty($v_detail[0]->fed_income_with_held)) {echo '$' . ' ' . $v_detail[0]->fed_income_with_held;} else {echo " ";}?></p></td>


		<td width="15%" rowspan="5" valign="top" align=right>
<?php if ($copyprint == 1) {?>

		<b><font size="2">Copy B</font></b><br>
		<b><font size="1">For Recipient</font></b><br>
		<br>
		This is important tax information and is being furnished to the Internal
		Revenue Service. If you are required to file a return, a negligence
		penalty or other sanction may be imposed on you if this income is
		taxable and the IRS determines that it has not been reported.

<?php } else if ($copyprint == 2) {?>
		<b><font size="2">Copy 2</font></b><br>
		<br>
		To be filed with recipient's state income tax return when required.<br><br><br><br><br><br><br><br><br>

<?php } else if ($copyprint == 3) {?>
		<b><font size="2">Copy 1</font></b><br>
		<b><font size="1">For State Tax
		Department</font></b><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

<?php } else if ($copyprint == 4) {?>
		<b><font size="2">Copy C</font></b><br>
				<font size="1"><b>
				For Payer<br><br></b>
				<br>
				For Privacy Act<br>
				and Paperwork<br>
				Reduction Act<br>
				Notice, see the<br>
				<b>2017 General<br>
				Instructions for<br>
				Form 1099, 1098<br>
				3921, 3922,5498<br>
				and W-2G<br></b>

				</font>




<?php }?>
		</td>


	</tr>
	<tr>
		<td width="22%" height="1" colspan="2" valign="top">PAYER'S Federal Tax
		ID<p class=big>	<?=$c_details[0]->c_ssn_ein?> </p>
		</td>
		<td width="22%" height="1" colspan="2" valign="top">&nbsp;RECIPIENT'S
		identification No.<p class=big>
			<?php if ($ssnprint == 2) {
            $abc = $v_detail[0]->recepient_id_no;
            echo substr_replace($abc, 'XXX-XX-', 0, 5);
        } else {echo $v_detail[0]->recepient_id_no;}?>
        </p>
		</td>
		<td width="18%" height="1" valign="top">5 Fishing boat proceeds<p class=big> <?php if (!empty($v_detail[0]->fishing_boat_proceed)) {echo '$' . ' ' . $v_detail[0]->fishing_boat_proceed;} else {echo " ";}?></p></td>
		<td width="19%" height="1" colspan="2" valign="top">6 Med &amp; Health care pmts<p class=big><?php if (!empty($v_detail[0]->mad_helthcare_pmts)) {echo '$' . ' ' . $v_detail[0]->mad_helthcare_pmts;} else {echo " ";}?></p></td>
	</tr>
	<tr>
		<td width="44%" colspan="4" rowspan="3" valign="top">
		RECIPIENT'S Name and Address<br>
		<p class=big>
		<?=$v_detail[0]->first_name?> <br><?=$v_detail[0]->address?><br><?=$v_detail[0]->city?>, <?=$v_detail[0]->state?> <?=$v_detail[0]->zipcode?></p><p></p><p></p><p></p><p></p><p></p>

		</td>
		<td width="18%" height="1" valign="top">7 Nonemployee Compensation
		<p class=big><?php if (!empty($v_detail[0]->non_emp_compansation)) {echo '$' . ' ' . $v_detail[0]->non_emp_compansation;} else {echo " ";}?></p>
		</td>
		<td width="19%" height="1" colspan="2" valign="top">8 Pmts in lieu of Div or Int
		<p class=big><?php if (!empty($v_detail[0]->pmts_in_lieu)) {echo '$' . ' ' . $v_detail[0]->pmts_in_lieu;} else {echo " ";}?></p></td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">9 Payer made direct sales
		of $5000 or more of consumer products to a buyer for resale <?php if (!empty($v_detail[0]->consumer_products_for_resale)) {echo '$' . ' ' . $v_detail[0]->consumer_products_for_resale;} else {echo " ";}?></td>
		<td width="19%" height="1" colspan="2" valign="top">10 Crop
		Insurance proceeds<p class=big><?php if (!empty($v_detail[0]->crop_Insurance_proceeds)) {echo '$' . ' ' . $v_detail[0]->crop_Insurance_proceeds;} else {echo " ";}?></p></td>
	</tr>
	<tr>
		<td width="18%" height="1" valign="top">
		13 Excess Golden Par Pmts<p class=big><?php if (!empty($v_detail[0]->excess_golden_par_pmts)) {echo '$' . ' ' . $v_detail[0]->excess_golden_par_pmts;} else {echo " ";}?></p>
		<p></p></td>
		<td width="19%" height="1" colspan="2" valign="top">
		14 Gross paid to attorney<p class=big><?php if (!empty($v_detail[0]->gross_paid_to_an_attomey)) {echo '$' . ' ' . $v_detail[0]->gross_paid_to_an_attomey;} else {echo " ";}?></p><p></p></td>
	</tr>
	<tr>
		<td width="17%" valign="top">Account Number<p class=big><?=$v_detail[0]->account_number?></p></td>
		<td width="14%" colspan="2" valign="top">15a Sec 409A def <p class=big> <?php if (!empty($v_detail[0]->sec_409a_deferrals)) {echo '$' . ' ' . $v_detail[0]->sec_409a_deferrals;} else {echo " ";}?></p></td>
		<td width="13%" valign="top">15b Sec 409A inc<p class=big> <?php if (!empty($v_detail[0]->sec_409a_Income)) {echo '$' . ' ' . $v_detail[0]->sec_409a_Income;} else {echo " ";}?></p></td>
		<td width="18%" valign="top">16 State tax withheld<p class=big> <?php if (!empty($v_detail[0]->state_tax_with_held)) {echo '$' . ' ' . $v_detail[0]->state_tax_with_held;} else {echo " ";}?></p></td>
		<td width="19%" colspan="2" valign="top">17 State/Payer's state no.<p class=big><?php if (!empty($v_detail[0]->payer_state_no)) {echo ' ' . $v_detail[0]->payer_state_no;} else {echo " ";}?></p></td>
		<td width="15%" valign="top">18 State income<p class=big><?php if (!empty($v_detail[0]->state_Income)) {echo '$' . ' ' . $v_detail[0]->state_Income;} else {echo " ";}?></p></td>
	</tr>
</table>


<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>

    <td width="22%"><font face="Arial" size="1">
    Form</font><font face="Arial" size="2">
      <b>1099-MISC</b></font>
   </td>

    <td width="34%" align=center><font face="Arial" size="2">

      </font>
    </td>

    <td width="43%" align=right>
    <font face="Arial" size="1">Department of the Treasury - Internal Revenue Service</font>
    </td>
  </tr>
</table>


<!-- Instructions to Recipients -->

<br>
<?php if ($copyprint == 1) {?>
<p style="font-Family: arial; font-size: 11px; text-align: left; margin-top: 3; margin-bottom: 3">
<b>Instructions for Recipients (Cont.) </b>
</p>

<p class="inst">

<b>Box 7.</b> Shows nonemployee compensation. If you are in the trade or business
of catching fish, box 7 may show cash you received for the sale of fish. If
payments in this box are SE income, report this amount on Schedule C, C-EZ,
or F (Form 1040), and complete Schedule SE (Form 1040). You received this
form instead of Form W-2 because the payer did not consider you an employee
and did not withhold income tax or social security and Medicare tax. Contact
the payer if you believe this form is incorrect or has been issued in error.
If you believe you are an employee and cannot get this form corrected, re-
port the amount from box 7 on Form 1040, line 7 (or Form 1040 NR, line 8).
You must also complete and attach to your return Form 8919, Uncollected
Social Security and Medicare Tax on Wages.<br>

<!--/div-->
<?php }?>
<div class="pagebreak"> </div>
<!--div id=layer2 style="top: 650; left:0; z-index: 1"-->
<?php
}
}
if (!empty($d_details)) {
    // echo '<pre>';
    // print_r($d_details);
    foreach (array_filter($d_details) as $d_detail) {
        ?>
    	<table width=100%>
    	<tr>
    	<td width=60%>
    	<p class=indent1>
    	<br>

    	       <?=$c_details[0]->FileName?><br>
    	       <?=$c_details[0]->c_address?>, <?=$c_details[0]->c_address_2?><br>
    	       <?=$c_details[0]->c_city?>, <?=$c_details[0]->c_state?> <?=$c_details[0]->c_zip?>
    	<!-- Legend -->
    	<br><br><br><br><br><br><br>
    	<table border="2" cellpadding="0" cellspacing="0" bordercolor="#000000">
    	      <tr ><td width="100%"  align="center">
    	     <font face="Arial" size="3">&nbsp;Important Tax Return Document Enclosed&nbsp;&nbsp;</font>
    	    </td>      </tr>
    	</table>

    			<?=$d_detail[0]->first_name?><br>
    			<?=$d_detail[0]->address?> <?=$d_detail[0]->address2?><br>
    			<?=$d_detail[0]->city?>, <?=$d_detail[0]->state?> <?=$d_detail[0]->zipcode?>
    	<br><br><br><br>
    	<!-- End of Mailing Lables -->
    	</p>
    	</td>


    	<!-- Instructions at the Top Right -->
    	<td width=40% valign=top>
    	<?php if ($copyprint == 1) {?>
    	<p style="font-Family: arial; font-size: 12px; text-align: left; margin-top: 3; margin-bottom: 3">

    	<b>Instructions for Recipients</b>
    	</p>
    	<p class="inst">
    	Account number. May show an account or other unique number the payer
    	assigned to distinguish your account. Amounts shown may be subject to
    	self-employment (SE) tax. If your net income from self-employment is
    	$400 or more, you must file a return and compute your SE tax on
    	Schedule SE (Form 1040). See Pub. 334, Tax Guide for Small Business,
    	for more information. If no income or social security and Medicare
    	taxes were withheld and you are still receiving these payments, see
    	Form 1040-ES, Estimated Tax for Individuals. Individuals must report
    	as explained for box 7 below. Corporations, fiduciaries, or partner-
    	ships report the amounts on the proper line of your return. For 2008:<br>
    	<br>
    	</p>
    	<?php } else {?>
				<p></p>
    	<?php }?>
    	</td>
    	</tr>
    	</table>


    	<div class=sm>
    	<center>
    	- - - - - - - - - - - - - - - - - - - - - Fold - - - - - - - - - - - - - - - - - - - - -
    	</center>
    	</div>
    	<br>
    	<!-- 1099-Misc Forms -->


    	<table border="2" width="100%" cellspacing="0" bordercolor="#000000" height="1"
    	     style="font-family: Arial; font-size: 10px">
    		<tr>
    			<td width="44%" height="1" colspan="4" rowspan="3" valign="top">
    	      &nbsp;PAYER'S name, street address, city, state and ZIP code
    	<p class=big>
    	       <?=$c_details[0]->FileName?><br>
    	       <?=$c_details[0]->c_address?>, <?=$c_details[0]->c_address_2?><br>
    	       <?=$c_details[0]->c_city?>, <?=$c_details[0]->c_state?> <?=$c_details[0]->c_zip?>
    	</p>


    	       </td>
    			<td width="18%" height="1" valign="top">&nbsp;1a Total ordinary dividends<p class=big><?php if (!empty($d_detail[0]->box1a)) {echo '$' . ' ' . $d_detail[0]->box1a;} else {echo " ";}?></p></td>

    			<td width="14%" height="1" rowspan="2" valign="top">
    			<center>
    	         OMB No. 1545-0115<br>
    			<h4 style="font-size: 32px; -webkit-margin-before: 0.2em !important;-webkit-margin-after: 0.2em !important;"><?=$c_details[0]->TaxYear?></h4><br>
    	         <b><font size="2"> 1099-DIV</font></b>
    	         </center>
    	        </td>


    			<td width="20%" height="1" colspan="2" rowspan="2" align="right"
    			style="font-family: Arial; font-size: 18px;">
    			Dividends and&nbsp;&nbsp; <br>Distributions&nbsp;&nbsp; </td>
    		</tr>
    		<tr>
    			<td width="18%" height="1" valign="top">1b Qualified dividends<p class=big><?php if (!empty($d_detail[0]->box1b)) {echo '$' . ' ' . $d_detail[0]->box1b;} else {echo " ";}?></p></td>
    		</tr>
    		<tr>
    			<td width="18%" height="1" valign="top">2a Total capital gain distr<p class=big><?php if (!empty($d_detail[0]->box2a)) {echo '$' . ' ' . $d_detail[0]->box2a;} else {echo " ";}?></p></td>
    			<td width="19%" height="1" colspan="2" valign="top">2b Unrecap. Sec. 1250 gain<p class=big><?php if (!empty($d_detail[0]->box2b)) {echo '$' . ' ' . $d_detail[0]->box2b;} else {echo " ";}?></p></td>


    			<td width="15%" rowspan="5" valign="top" align=right>
    	<?php if ($copyprint == 1) {?>

    			<b><font size="2">Copy B</font></b><br>
    			<b><font size="1">For Recipient</font></b><br>
    			<br>
    			This is important tax information and is being furnished to the Internal
    			Revenue Service. If you are required to file a return, a negligence
    			penalty or other sanction may be imposed on you if this income is
    			taxable and the IRS determines that it has not been reported.

    	<?php } else if ($copyprint == 2) {?>
    			<b><font size="2">Copy 2</font></b><br>
    			<br>
    			To be filed with recipient's state income tax return when required.<br><br><br><br><br><br><br><br><br>

    	<?php } else if ($copyprint == 3) {?>
    			<b><font size="2">Copy 1</font></b><br>
    			<b><font size="1">For State Tax
    			Department</font></b><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

    	<?php } else if ($copyprint == 4) {?>
    			<b><font size="2">Copy C</font></b><br>
    					<font size="1"><b>
    					For Payer<br><br></b>
    					<br>
    					For Privacy Act<br>
    					and Paperwork<br>
    					Reduction Act<br>
    					Notice, see the<br>
    					<b>2017 General<br>
    					Instructions for<br>
    					Form 1099, 1098<br>
    					3921, 3922,5498<br>
    					and W-2G<br></b>

    					</font>




    	<?php }?>
    			</td>


    		</tr>
    		<tr>
    			<td width="22%" height="1" colspan="2" valign="top">PAYER'S Federal Tax
    			ID<p class=big>	<?=$c_details[0]->c_ssn_ein?> </p>
    			</td>
    			<td width="22%" height="1" colspan="2" valign="top">&nbsp;RECIPIENT'S
    			identification No.<p class=big>
    				<?php if ($ssnprint == 2) {
            $abc = $d_detail[0]->recepient_id_no;
            echo substr_replace($abc, 'XXX-XX-', 0, 5);
        } else {echo $d_detail[0]->recepient_id_no;}?>
    	        </p>
    			</td>
    			<td width="18%" height="1" valign="top">2c Section 1202 gain<p class=big> <?php if (!empty($d_detail[0]->box2c)) {echo '$' . ' ' . $d_detail[0]->box2c;} else {echo " ";}?></p></td>
    			<td width="19%" height="1" colspan="2" valign="top">2d Collectibles (28%) gain<p class=big><?php if (!empty($d_detail[0]->box2d)) {echo '$' . ' ' . $d_detail[0]->box2d;} else {echo " ";}?></p></td>
    		</tr>
    		<tr>
    			<td width="44%" colspan="4" rowspan="3" valign="top">
    			RECIPIENT'S Name and Address<br>
    			<p class=big>
    			<?=$d_detail[0]->first_name?> <br><?=$d_detail[0]->address?><br><?=$d_detail[0]->city?>, <?=$d_detail[0]->state?> <?=$d_detail[0]->zipcode?></p><p></p><p></p><p></p><p></p><p></p>

    			</td>
    			<td width="18%" height="1" valign="top">3 Nondividend distributions
    			<p class=big><?php if (!empty($d_detail[0]->box3)) {echo '$' . ' ' . $d_detail[0]->box3;} else {echo " ";}?></p>
    			</td>
    			<td width="19%" height="1" colspan="2" valign="top">4 Federal income tax withheld
    			<p class=big><?php if (!empty($d_detail[0]->box4)) {echo '$' . ' ' . $d_detail[0]->box4;} else {echo " ";}?></p></td>
    		</tr>
    		<tr>
    			<td width="18%" height="1" valign="top">5 Investment expenses<p class=big><?php if (!empty($d_detail[0]->box5)) {echo '$' . ' ' . $d_detail[0]->box5;} else {echo " ";}?></p></td>
    			<td width="19%" height="1" colspan="2" valign="top">6 Foreign tax paid<p class=big><?php if (!empty($d_detail[0]->box6)) {echo '$' . ' ' . $d_detail[0]->box6;} else {echo " ";}?></p></td>
    		</tr>
    		<tr>
    			<td width="18%" height="1" valign="top">
    			7 Foreign country or U.S. possession<p class=big><?php if (!empty($d_detail[0]->box7)) {echo ' ' . $d_detail[0]->box7;} else {echo " ";}?></p>
    			<p></p></td>
    			<td width="19%" height="1" colspan="2" valign="top">
    			8 Cash liquidation distributions<p class=big><?php if (!empty($d_detail[0]->box8)) {echo '$' . ' ' . $d_detail[0]->box8;} else {echo " ";}?></p><p></p></td>
    		</tr>
    		<tr>
    			<td width="17%" valign="top">Account Number<p class=big><?=$d_detail[0]->account_number?></p></td>
    			<td width="14%" colspan="2" valign="top">9 Noncash liquidation distributions<p class=big> <?php if (!empty($d_detail[0]->box9)) {echo '$' . ' ' . $d_detail[0]->box9;} else {echo " ";}?></p></td>
    			<td width="13%" valign="top">10 Exempt-interest dividends<p class=big> <?php if (!empty($d_detail[0]->box10)) {echo '$' . ' ' . $d_detail[0]->box10;} else {echo " ";}?></p></td>
    			<td width="18%" valign="top">11 Specified private activity bond interest dividends<p class=big> <?php if (!empty($d_detail[0]->box11)) {echo '$' . ' ' . $d_detail[0]->box11;} else {echo " ";}?></p></td>
    			<td width="15%" valign="top">12 State<p class=big><?php if (!empty($d_detail[0]->box12)) {echo ' ' . $d_detail[0]->box12;} else {echo " ";}?></p></td>
    			<td width="15%" valign="top">13 State Identification no.<p class=big><?php if (!empty($d_detail[0]->box13)) {echo ' ' . $d_detail[0]->box13;} else {echo " ";}?></p></td>
    			<td width="10%" valign="top">14 State tax withheld<p class=big><?php if (!empty($d_detail[0]->box14)) {echo '$' . ' ' . $d_detail[0]->box14;} else {echo " ";}?></p></td>
    		</tr>
    	</table>


    	<table border="0" cellpadding="0" cellspacing="1" width="100%">
    	  <tr>

    	    <td width="22%"><font face="Arial" size="1">
    	    Form</font><font face="Arial" size="2">
    	      <b>1099-DIV</b></font>
    	   </td>

    	    <td width="34%" align=center><font face="Arial" size="2">

    	      </font>
    	    </td>

    	    <td width="43%" align=right>
    	    <font face="Arial" size="1">Department of the Treasury - Internal Revenue Service</font>
    	    </td>
    	  </tr>
    	</table>
    	<!-- Instructions to Recipients -->
    	<br>
    	<!--/div-->

    	<div class="pagebreak"> </div>
    	<?php
}
}?>
</body>
</html>

