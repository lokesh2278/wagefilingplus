<script type="text/javascript">

  $(document).ready(function () {

                $("#next").click(function () {

                    var taxyear = $('#tax_year').val();

                    var employee_ein = $('#employee_ein').val();
                    var employee_ein2 = $('#other_ein').val();

                    var emp_ein = /[0-9]/;

                    var n1 = employee_ein.startsWith('00',0);
                    var nn1 = employee_ein2.startsWith('00',0);

                    var n2 = employee_ein.startsWith('07',0);
                    var nn2 = employee_ein2.startsWith('07',0);

                    var n3 = employee_ein.startsWith('08',0);
                    var nn3 = employee_ein2.startsWith('08',0);

                    var n4 = employee_ein.startsWith('09',0);
                    var nn4 = employee_ein2.startsWith('09',0);

                    var n5 = employee_ein.startsWith('17',0);
                    var nn5 = employee_ein2.startsWith('17',0);

                    var n6 = employee_ein.startsWith('18',0);
                    var nn6 = employee_ein2.startsWith('18',0);

                    var n7 = employee_ein.startsWith('19',0);
                    var nn7 = employee_ein2.startsWith('19',0);

                    var n8 = employee_ein.startsWith('28',0);
                    var nn8 = employee_ein2.startsWith('28',0);

                    var n9 = employee_ein.startsWith('29',0);
                    var nn9 = employee_ein2.startsWith('29',0);

                    var n10 = employee_ein.startsWith('49',0);
                    var nn10 = employee_ein2.startsWith('49',0);

                    var n11 = employee_ein.startsWith('69',0);
                    var nn11 = employee_ein2.startsWith('69',0);

                    var n12 = employee_ein.startsWith('70',0);
                    var nn12 = employee_ein2.startsWith('70',0);

                    var n13 = employee_ein.startsWith('78',0);
                    var nn13 = employee_ein2.startsWith('78',0);

                    var n14 = employee_ein.startsWith('79',0);
                    var nn14 = employee_ein2.startsWith('79',0);

                    var n15 = employee_ein.startsWith('89',0);
                    var nn15 = employee_ein2.startsWith('89',0);

                    if (taxyear.length == 0) {

                      alert("Tax Year is empty");

                      //$("#tax_year").focus();

                      return false;

                      //$("#employee_ein").focus();

                      //return false;

                    }

                    else if (taxyear < 2018) {

                    alert("Tax Year equal to 2018");

                    //$("#tax_year").focus();

                    return false;

                    }

                    else if (taxyear > 2018) {

                    alert("Invalid future year");

                    //$("#tax_year").focus();

                    return false;

                    }

                    else if (taxyear.length != 4) {

                    alert("Tax Year Must contain 4 digit");

                    //$("#tax_year").focus();

                    return false;

                    }



                    else if (!employee_ein.match(emp_ein)) {

                    alert("Employer EIN is must be digits");

                    //$("#tax_year").focus();

                    return false;

                    }

                    else if (employee_ein.length == 0) {

                    alert("Employer EIN is empty");

                    //$("#tax_year").focus();

                    return false;

                    }

                    else if (employee_ein.length < 9) {

                    alert("Employer EIN is must be 9 digit");

                    //$("#tax_year").focus();

                    return false;

                    }

                    else if(n1 || nn1){
                      alert("Employee EIN not start with 00");
                      return false;
                    }
                    else if(n2 || nn2){
                      alert("Employee EIN not start with 07");
                      return false;
                    }
                    else if(n3 || nn3){
                      alert("Employee EIN not start with 08");
                      return false;
                    }
                    else if(n4 || nn1){
                      alert("Employee EIN not start with 09");
                      return false;
                    }
                    else if(n5 || nn5){
                      alert("Employee EIN not start with 17");
                      return false;
                    }
                    else if(n6 || nn6){
                      alert("Employee EIN not start with 18");
                      return false;
                    }
                    else if(n7 || nn7){
                      alert("Employee EIN not start with 19");
                      return false;
                    }
                    else if(n8 || nn8){
                      alert("Employee EIN not start with 28");
                      return false;
                    }
                    else if(n9 || nn9){
                      alert("Employee EIN not start with 29");
                      return false;
                    }
                    else if(n10 || nn10){
                      alert("Employee EIN not start with 49");
                      return false;
                    }
                    else if(n11 || nn11){
                      alert("Employee EIN not start with 69");
                      return false;
                    }
                    else if(n12 || nn12){
                      alert("Employee EIN not start with 70");
                      return false;
                    }
                    else if(n13 || nn13){
                      alert("Employee EIN not start with 78");
                      return false;
                    }
                    else if(n14 || nn14){
                      alert("Employee EIN not start with 79");
                      return false;
                    }
                    else if(n15 || nn15){
                      alert("Employee EIN not start with 89");
                      return false;
                    }

                    else {

                    $("#first-panel").hide();

                    $("#title-A").hide();

                    $("#second-panel").removeClass("hide");

                    $("#title-B").removeClass("hide");

                  }

                });

                $("#next-2").click(function () {

                    var company = $('#company').val();

                    var address_1 = $('#address_1').val();

                    var city = $('#city').val();

                    var state = $('#state').val();

                    var zip = $('#zip').val();

                    var phoneno = /^\d{10}$/;

                    var phone = $('#phone').val();

                    if (company.length == 0) {

                      alert("Company is empty");

                      return false;

                    } 
                    else if (address_1.length == 0) {

                    alert("Address 1 is empty");

                    return false;

                    } 
                    else if (city.length == 0) {

                    alert("City is empty");

                    return false;

                    } else if (state.length == 0) {

                    alert("State is empty");

                    return false;

                    }

                    else if (zip.length != 5 && zip.length == 0) {

                    alert("Zip5 fields not 5 digits");

                    return false;

                    }

                    // else if (!phone.match(phoneno)) {

                    // alert("Invalid phone number");

                    // return false;

                    // }

                    else {

                    $("#second-panel").hide();

                    $("#title-B").hide();

                    $("#third-panel").removeClass("hide");

                    $("#title-C").removeClass("hide");

                  }

                });

                $("#next-3").click(function () {

                  //var code = $('#code').val();

                  var code = document.getElementById("code").checked;

                  var code2 = document.getElementById("code2").checked;

                  var code3 = document.getElementById("code3").checked;

                  var code4 = document.getElementById("code4").checked;

                  var code5 = document.getElementById("code5").checked;

                  var code6 = document.getElementById("code6").checked;

                  var code7 = document.getElementById("code7").checked;

                  if (!code && !code2 && !code3 && !code4 && !code5 && !code6 && !code7) {

                    alert("You must check an Employer Type");

                    return false;

                    } else {

                    $("#third-panel").hide();

                    $("#title-C").hide();

                    $("#forth-panel").removeClass("hide");

                    $("#title-D").removeClass("hide");

                  }

                });

                $("#next-4").click(function () {

                    $("#forth-panel").hide();

                    $("#title-D").hide();

                    $("#fifth-panel").removeClass("hide");

                    $("#title-E").removeClass("hide");

                });

              });

</script>

<style>

  /* The container */

.container-radio {

    display: block;

    position: relative;

    padding-left: 35px;

    margin-bottom: 12px;

    cursor: pointer;

    font-size: 14px;

    -webkit-user-select: none;

    -moz-user-select: none;

    -ms-user-select: none;

    user-select: none;

}



/* Hide the browser's default radio button */

.container-radio input {

    position: absolute;

    opacity: 0;

    cursor: pointer;

}



/* Create a custom radio button */

.checkmark {

    position: absolute;

    top: 0;

    left: 0;

    height: 25px;

    width: 25px;

    background-color: #eee;

    border-radius: 50%;

}



/* On mouse-over, add a grey background color */

.container-radio:hover input ~ .checkmark {

    background-color: #ccc;

}



/* When the radio button is checked, add a blue background */

.container-radio input:checked ~ .checkmark {

    background-color: #2196F3;

}



/* Create the indicator (the dot/circle - hidden when not checked) */

.checkmark:after {

    content: "";

    position: absolute;

    display: none;

}



/* Show the indicator (dot/circle) when checked */

.container-radio input:checked ~ .checkmark:after {

    display: block;

}



/* Style the indicator (dot/circle) */

.container-radio .checkmark:after {

    top: 9px;

    left: 9px;

    width: 8px;

    height: 8px;

    border-radius: 50%;

    background: white;

}

</style>

<div id="blaze-banner">

  <div class="container">

    <div class="navigation-container">

      <div class="wrap wrap-one-logo text-center">

        <a class="back-link" href="<?=base_url('blaze');?>" style="display: block; margin-left: 128px; "><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>

        <div class="row">

          <div id="nav-company" class="col-sm-12 col-md-12 col-lg-12">

            <h1 style="font-size: 29px; " id="title-A"><a class="company-link"><span class="company-name">Employer Setup - Form W-2 - Page A</span></a></h1>

            <h1 style="font-size: 29px; " id="title-B" class="hide"><a class="company-link"><span class="company-name">Employer Setup - Form W-2 - Page B</span></a></h1>

            <h1 style="font-size: 29px; " id="title-C" class="hide"><a class="company-link"><span class="company-name">Employer Setup - Form W-2 - Page C</span></a></h1>

            <h1 style="font-size: 29px; " id="title-D" class="hide"><a class="company-link"><span class="company-name">Employer Setup - Form W-2 - Page D - OPTIONAL (all may be empty)</span></a></h1>

            <h1 style="font-size: 29px; " id="title-E" class="hide"><a class="company-link"><span class="company-name">Employer Setup - Form W-2 - Page E (all may be empty)</span></a></h1>

          </div>

          <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3"></div>

        </div>

      </div>

    </div>

    <div id="alerts-container" class="alerts-container"></div>

  </div>

</div>

<div class="content-container" id="main-view-content">

  <div>

    <div class="container">

      <div class="row">

        <div class="col-md-8 col-md-offset-2">

          <form id="companyform" action="employee_setup" accept-charset="UTF-8" method="post">

            <input id="c54_c_id" type="hidden" name="c_id" value="<?php echo !empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>">

            <div id="first-panel">

            <div class="panel panel-primary">

              <div class="panel-heading">

                <h3 class="panel-title">Employer Tax Year and EIN information</h3>

              </div>

              <div class="panel-body panel-body-extra">

                <div class="form-container">

                  <div id="form">

                     <div class="row">

                      <div class="col-sm-12" data-fields="name">

                        <div class="form-group field-name">

                          <p id="head"></p>

                          <label class="control-label" for="tax_year"><span class="label-inner">Tax Year<help style="margin-left: 10px;">(Enter four digit tax year, previous or current year.)</help></span>

                        </label>

                        <div data-editor="">

                          <!--<input id="tax_year" class="form-control" name="tax_year" maxlength="4" value="<?php echo !empty($c_details[0]->c_tax_year) ? $c_details[0]->c_tax_year : '' ?>" type="number" min="2006" required/>-->

                          <select class="form-control" name="tax_year" id="tax_year"  required >

                            <?php

$startdate = 2018;

$enddate = date("Y");

$yearC = isset($c_details[0]->TaxYear) ? $c_details[0]->TaxYear : $startdate;

$years = range($startdate, $enddate);

foreach ($years as $year) {

    if ($year == $yearC) {$sel = 'selected="selected"';} else { $sel = '';}

    echo "<option $sel value='$year'>$year </option>";

}

?></select>



                        </div>

                      </div>

                    </div>

                  </div>

                   <div class="row">

                    <div class="col-sm-5" data-fields="tin">

                      <div class="form-group field-tin">

                        <label class="control-label" for="you_have">You have:

                      </label>

                      <div data-editor="">

                           <input id="" name="you_have" value="1" type="radio" <?php if (isset($c_details[0]->c_you_have) && $c_details[0]->c_you_have == '1') {echo 'checked';} else {echo '';}?>>An EIN <br/>

                           <input id="" name="you_have" value="0" type="radio" <?php if (isset($c_details[0]->c_you_have) && $c_details[0]->c_you_have == '0') {echo 'checked';} else {echo '';}?>>Applied for an EIN

                      </div>

                    </div>

                  </div>

                 </div>

                 <div class="row">

                    <div class="col-sm-5" data-fields="tin">

                      <div class="form-group field-tin">

                        <label class="control-label" for="ein_for">The EIN is for:

                      </label>

                      <div data-editor="">

                           <input id="ein_for" name="ein_for" value="1" type="radio" <?php if (isset($c_details[0]->c_ein_for) && $c_details[0]->c_ein_for == '1') {echo 'checked';} else {echo '';}?>>  A Regular Employer<br/>

                           <input id="ein_for" name="ein_for" value="2" type="radio" <?php if (isset($c_details[0]->c_ein_for) && $c_details[0]->c_ein_for == '2') {echo 'checked';} else {echo '';}?>> A Common Paymaster<br/>

                           <input id="ein_for" name="ein_for" value="3" type="radio" <?php if (isset($c_details[0]->c_ein_for) && $c_details[0]->c_ein_for == '3') {echo 'checked';} else {echo '';}?>> Filer is Form 2678 Agent<br/>

                           <input id="ein_for" name="ein_for" value="4" type="radio" <?php if (isset($c_details[0]->c_ein_for) && $c_details[0]->c_ein_for == '4') {echo 'checked';} else {echo '';}?>>  Filer is Section 3504 Agent

                      </div>

                    </div>

                  </div>

                 </div>

                </div>

              </div>

</div>

<!-- <div class="panel-footer">

<button type="submit" class="btn btn-lg btn-primary btn-save">Save</button>

</div> -->

</div>

<div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">An EIN or "Applied For" must appear on all W-2 Forms. SSNs are not allowed.</h3>

  </div>

  <div class="panel-body">

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="employee_ein"><span class="label-inner">Employer EIN</span>

    </label>

    <div data-editor="">

      <input id="employee_ein" class="form-control" name="employee_ein" type="text" value="<?php echo !empty($c_details[0]->c_employee_ein) ? $c_details[0]->c_employee_ein : '' ?>" required/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="other_ein"><span class="label-inner">Other EIN</span>

    </label>

    <div data-editor="">

      <input id="other_ein" class="form-control" name="other_ein" type="text" value="<?php echo !empty($c_details[0]->c_other_ein) ? $c_details[0]->c_other_ein : '' ?>"/>

    </div>

      </div>

    </div>

  </div>

</div>

<div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">If you file multiple payrolls for the same EIN:</h3>

  </div>

  <div class="panel-body">

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="est_number"><span class="label-inner">Establishment Number:<help style="margin-left: 10px;">(designate the store, factory or type of payroll. Up to 4 characters.)</help></span>

    </label>

    <div data-editor="">

      <input id="est_number" class="form-control" name="est_number" type="text" value="<?php echo !empty($c_details[0]->c_est_number) ? $c_details[0]->c_est_number : '' ?>"/>



    </div>

      </div>

    </div>

  </div>

</div>

<div class="col-md-12">

  <div class="row">

    <div class="col-md-6"><button type="button" id="next" class="btn btn-lg btn-block btn-primary btn-green-bg">Next</button></div>

    <?php if(isset($c_details[0])){ ?>
    <div class="col-md-6"><a href="<?=base_url('vendor');?>/<?=!empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <?php } else { ?>
      <div class="col-md-6"><a href="<?=base_url('company');?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <? } ?>

  </div>

</div>

</div>

<div id="second-panel" class="hide">

  <div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">Employer Company Information</h3>

  </div>

  <div class="panel-body">

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="company"><span class="label-inner">Company</span>

    </label>

    <div data-editor="">

      <input id="company" class="form-control" name="company" type="text" value="<?php echo !empty($c_details[0]->FileName) ? $c_details[0]->FileName : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="address"><span class="label-inner">Address 1<!-- <help style="margin-left: 10px;">(Optional Attention Line)</help> --></span>

    </label>

    <div data-editor="">

      <input id="address_1" class="form-control" name="address" type="text" value="<?php echo !empty($c_details[0]->c_address) ? $c_details[0]->c_address : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="address_2"><span class="label-inner">Address 2<help style="margin-left: 10px;">(Enter Street, PO Box)</help></span>

    </label>

    <div data-editor="">

      <input id="address_2" class="form-control" name="address_2" type="text" value="<?php echo !empty($c_details[0]->c_address_2) ? $c_details[0]->c_address_2 : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="c54_email"><span class="label-inner">City</span>

    </label>

    <div data-editor="">

      <input id="city" class="form-control" name="city" type="text" value="<?php echo !empty($c_details[0]->c_city) ? $c_details[0]->c_city : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="city"><span class="label-inner">State</span></label>

         <div data-editor="">

          <input id="c_state_lokesh" type="hidden" value="<?php echo !empty($c_details[0]->c_state) ? $c_details[0]->c_state : '' ?>">

                          <select id="state" class="form-control" name="state">

                          <option value="">Choose...</option>

                          <option value="AA">AA - Armed Forces Americas</option>

                          <option value="AE">AE - Armed Forces Europe</option>

                          <option value="AK">AK - Alaska</option>

                          <option value="AL">AL - Alabama</option>

                          <option value="AP">AP - Armed Forces Pacific</option>

                          <option value="AR">AR - Arkansas</option>

                          <option value="AS">AS - American Samoa</option>

                          <option value="AZ">AZ - Arizona</option>

                          <option value="CA">CA - California</option>

                          <option value="CO">CO - Colorado</option>

                          <option value="CT">CT - Connecticut</option>

                          <option value="DC">DC - District of Columbia</option>

                          <option value="DE">DE - Delaware</option>

                          <option value="FL">FL - Florida</option>

                          <option value="FM">FM - Federated Micronesia</option>

                          <option value="GA">GA - Georgia</option>

                          <option value="GU">GU - Guam</option>

                          <option value="HI">HI - Hawaii</option>

                          <option value="IA">IA - Iowa</option>

                          <option value="ID">ID - Idaho</option>

                          <option value="IL">IL - Illinois</option>

                          <option value="IN">IN - Indiana</option>

                          <option value="KS">KS - Kansas</option>

                          <option value="KY">KY - Kentucky</option>

                          <option value="LA">LA - Louisiana</option>

                          <option value="MA">MA - Massachusetts</option>

                          <option value="MD">MD - Maryland</option>

                          <option value="ME">ME - Maine</option>

                          <option value="MH">MH - Marshall Islands</option>

                          <option value="MI">MI - Michigan</option>

                          <option value="MN">MN - Minnesota</option>

                          <option value="MO">MO - Missouri</option>

                          <option value="MP">MP - N. Mariana Islands</option>

                          <option value="MS">MS - Mississippi</option>

                          <option value="MT">MT - Montana</option>

                          <option value="NC">NC - North Carolina</option>

                          <option value="ND">ND - North Dakota</option>

                          <option value="NE">NE - Nebraska</option>

                          <option value="NH">NH - New Hampshire</option>

                          <option value="NJ">NJ - New Jersey</option>

                          <option value="NM">NM - New Mexico</option>

                          <option value="NV">NV - Nevada</option>

                          <option value="NY">NY - New York</option>

                          <option value="OH">OH - Ohio</option>

                          <option value="OK">OK - Oklahoma</option>

                          <option value="OR">OR - Oregon</option>

                          <option value="PA">PA - Pennsylvania</option>

                          <option value="PR">PR - Puerto Rico</option>

                          <option value="PW">PW - Palau</option>

                          <option value="RI">RI - Rhode Island</option>

                          <option value="SC">SC - South Carolina</option>

                          <option value="SD">SD - South Dakota</option>

                          <option value="TN">TN - Tennessee</option>

                          <option value="TX">TX - Texas</option>

                          <option value="UT">UT - Utah</option>

                          <option value="VA">VA - Virginia</option>

                          <option value="VI">VI - US Virgin Islands</option>

                          <option value="VT">VT - Vermont</option>

                          <option value="WA">WA - Washington</option>

                          <option value="WI">WI - Wisconsin</option>

                          <option value="WV">WV - West Virginia</option>

                          <option value="WY">WY - Wyoming</option></select>

                        </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="zip"><span class="label-inner">Zip 5</span>

    </label>

    <div data-editor="">

      <input id="zip" class="form-control" name="zip" type="text" value="<?php echo !empty($c_details[0]->c_zip) ? $c_details[0]->c_zip : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="zip_2"><span class="label-inner">Zip 4</span>

    </label>

    <div data-editor="">

      <input id="zip_2" class="form-control" name="zip_2" type="text" value="<?php echo !empty($c_details[0]->c_zip_2) ? $c_details[0]->c_zip_2 : '' ?>"/>

    </div>

      </div>

    </div>

  </div>

</div>

<div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">Contact Person - Optional Fields</h3>

  </div>

  <div class="panel-body">

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="name"><span class="label-inner">Name</span>

    </label>

    <div data-editor="">

      <input id="name" class="form-control" name="name" type="text" value="<?php echo !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="title"><span class="label-inner">Title</span>

    </label>

    <div data-editor="">

      <input id="title" class="form-control" name="title" type="text" value="<?php echo !empty($c_details[0]->c_title) ? $c_details[0]->c_title : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="phone"><span class="label-inner">Phone</span>

    </label>

    <div data-editor="">

      <input id="phone" class="form-control" name="phone" type="tel" pattern="^\d{10}$" value="<?php echo !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '' ?>" required />

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="extention"><span class="label-inner">Extension</span>

    </label>

    <div data-editor="">

      <input id="extention" class="form-control" name="extention" type="text" value="<?php echo !empty($c_details[0]->c_extention) ? $c_details[0]->c_extention : '' ?>"/>

    </div>

      </div>

    </div>

  <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="fax"><span class="label-inner">Fax</span>

    </label>

    <div data-editor="">

      <input id="fax" class="form-control" name="fax" type="text" value="<?php echo !empty($c_details[0]->c_fax) ? $c_details[0]->c_fax : '' ?>"/>

    </div>

      </div>

    </div>

  <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="email"><span class="label-inner">Email</span>

    </label>

    <div data-editor="">

      <input id="email" class="form-control" name="email" type="text" value="<?php echo !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '' ?>"/>

    </div>

      </div>

    </div>

  </div>

</div>

<div class="col-md-12">

  <div class="row">

    <div class="col-md-6"><button type="button" id="next-2" class="btn btn-lg btn-block btn-primary btn-green-bg">Next</button></div>

    <?php if(isset($c_details[0])){ ?>
    <div class="col-md-6"><a href="<?=base_url('vendor');?>/<?=!empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <?php } else { ?>
      <div class="col-md-6"><a href="<?=base_url('company');?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <? } ?>

  </div>

</div>

</div>

<div id="third-panel" class="hide">

  <div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">Employer Code</h3>

  </div>

  <div class="panel-body">

    <div class="row">

     <div class="col-sm-12" data-fields="name">

                             <div class="form-group field-name">

                               <label class="control-label" for="code">Select Code

                             </label>

                             <div data-editor="">



                                   <label class="radio-inline container-radio">R Regular Form 941 Employer

                                   <input type="radio" id="code" name="code" value="1" type="radio" <?php if (isset($c_details[0]->c_code) && $c_details[0]->c_code == '1') {echo 'checked';} else {echo '';}?>>

                                   <span class="checkmark"></span>

                                   </label>



                                   <label class="radio-inline container-radio">A Agriculture Form 943 Employer

                                   <input type="radio" id="code2" name="code" value="2" type="radio" <?php if (isset($c_details[0]->c_code) && $c_details[0]->c_code == '2') {echo 'checked';} else {echo '';}?>>

                                   <span class="checkmark"></span>

                                   </label>



                                   <label class="radio-inline container-radio">H Household Employer

                                   <input type="radio" id="code3" name="code" value="3" type="radio" <?php if (isset($c_details[0]->c_code) && $c_details[0]->c_code == '3') {echo 'checked';} else {echo '';}?>>

                                   <span class="checkmark"></span>

                                   </label>



                                   <label class="radio-inline container-radio">M Military Employer

                                   <input type="radio" id="code4" name="code" value="4" type="radio" <?php if (isset($c_details[0]->c_code) && $c_details[0]->c_code == '4') {echo 'checked';} else {echo '';}?>>

                                   <span class="checkmark"></span>

                                   </label>



                                   <label class="radio-inline container-radio">Q Medicare Qualified Goverment Employer

                                   <input type="radio" id="code5" name="code" value="5" type="radio" <?php if (isset($c_details[0]->c_code) && $c_details[0]->c_code == '5') {echo 'checked';} else {echo '';}?>>

                                   <span class="checkmark"></span>

                                   </label>



                                   <label class="radio-inline container-radio">X Railroad CT-1 (RRTA) Employer

                                   <input type="radio" id="code6" name="code" value="6" type="radio" <?php if (isset($c_details[0]->c_code) && $c_details[0]->c_code == '6') {echo 'checked';} else {echo '';}?>>

                                   <span class="checkmark"></span>

                                   </label>



                                   <label class="radio-inline container-radio">M Regular Form 944 Employer

                                   <input type="radio" id="code7" name="code" value="7" type="radio" <?php if (isset($c_details[0]->c_code) && $c_details[0]->c_code == '7') {echo 'checked';} else {echo '';}?>>

                                   <span class="checkmark"></span>

                                   </label>



                             </div>

                           </div>

                         </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <div class="alert alert-info">

          <p>Employers must select a code. If your company has more than one code, then set up multiple employers for each type code.</p>

        </div>

      </div>

    </div>

    <div class="col-md-12">

  <div class="row">

    <div class="col-md-6"><button type="button" id="next-3" class="btn btn-lg btn-block btn-primary btn-green-bg">Next</button></div>

    <?php if(isset($c_details[0])){ ?>
    <div class="col-md-6"><a href="<?=base_url('vendor');?>/<?=!empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <?php } else { ?>
      <div class="col-md-6"><a href="<?=base_url('company');?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <? } ?>

  </div>

</div>

  </div>

</div>

</div>

<div id="forth-panel" class="hide">

  <div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">Employer Foreign Address Information OPTIONAL</h3>

  </div>

  <div class="panel-body">

      <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="foreign_state"><span class="label-inner">Foreign State<help style="margin-left: 10px;">(Optional)</help></span>

    </label>

    <div data-editor="">

      <input id="foreign_state" class="form-control" name="foreign_state" type="text" value="<?php echo !empty($c_details[0]->c_foreign_state) ? $c_details[0]->c_foreign_state : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="foreign_zip"><span class="label-inner">Foreign Zip<help style="margin-left: 10px;">(Optional)</help></span>

        </label>

    <div data-editor="">

      <input id="foreign_zip" class="form-control" name="foreign_zip" type="text"

      <input id="foreign_state" class="form-control" name="foreign_state" type="text" value="<?php echo !empty($c_details[0]->c_foreign_zip) ? $c_details[0]->c_foreign_zip : '' ?>"/>

    </div>

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

         <label class="control-label" for="foreign_country_code"><span class="label-inner">Foreign Country Code</span>

            </label>

            <div data-editor="">

              <input id="c54_country_code" type="hidden" value="<?php echo !empty($c_details[0]->foreign_country_code) ? $c_details[0]->foreign_country_code : '' ?>">

              <select class="form-control" id="foreign_country_code" name="foreign_country_code">

                  <option value="0">Select Foreign Country</option>

                  <option value="AF">AF Afghanistan</option>

                  <option value="AX">AX Akrotiri Base Area</option>

                  <option value="AL">AL Albania</option>

                  <option value="AG">AG Algeria</option>

                  <option value="AN">AN Andorra</option>

                  <option value="AO">AO Angola</option>

                  <option value="AV">AV Anguilla</option>

                  <option value="AY">AY Antarctica</option>

                  <option value="AC">AC Antigua Barbuda</option>

                  <option value="AR">AR Argentina</option>

                  <option value="AM">AM Armenia</option>

                  <option value="AA">AA Aruba</option>

                  <option value="AT">AT Ashmore Cartier Islands</option>

                  <option value="AS">AS Australia</option>

                  <option value="AU">AU Austria</option>

                  <option value="AJ">AJ Azerbaijan</option>

                  <option value="BF">BF Bahamas, The</option>

                  <option value="BA">BA Bahrain</option>

                  <option value="FQ">FQ Baker Island</option>

                  <option value="BG">BG Bangladesh</option>

                  <option value="BB">BB Barbados</option>

                  <option value="BS">BS Bassas da India</option>

                  <option value="BO">BO Belarus</option>

                  <option value="BE">BE Belgium</option>

                  <option value="BH">BH Belize</option>

                  <option value="BN">BN Benin</option>

                  <option value="BD">BD Bermuda</option>

                  <option value="BT">BT Bhutan</option>

                  <option value="BL">BL Bolivia</option>

                  <option value="BK">BK Bosnia-Herzegovina</option>

                  <option value="BC">BC Botswana</option>

                  <option value="BV">BV Bouvet Island</option>

                  <option value="BR">BR Brazil</option>

                  <option value="IO">IO Indian Ocean Territory</option>

                  <option value="BX">BX Brunei</option>

                  <option value="B">BU Bulgaria</option>

                  <option value="UV">UV Burkina Faso</option>

                  <option value="BM">BM Burma</option>

                  <option value="BY">BY Burundi</option>

                  <option value="CB">CB Cambodia</option>

                  <option value="CM">CM Cameroon</option>

                  <option value="CA">CA Canada</option>

                  <option value="CV">CV Cape Verde</option>

                  <option value="CJ">CJ Cayman Islands</option>

                  <option value="CT">CT Cent African Republic</option>

                  <option value="CD">CD Chad</option>

                  <option value="CI">CI Chile</option>

                  <option value="CH">CH China, PRO</option>

                  <option value="KT">KT Christmas Is Indian Ocean</option>

                  <option value="IP">IP Clipperton Island</option>

                  <option value="CK">CK Cocos (Keeling) Islands</option>

                  <option value="CO">CO Colombia</option>

                  <option value="CN">CN Comoros</option>

                  <option value="CG">CG Congo (Democratic Republic of)</option>

                  <option value="CF">CF Congo (Republic of)</option>

                  <option value="CW">CW Cook Islands</option>

                  <option value="CR">CR Coral Sea Islands Territory</option>

                  <option value="CS">CS Costa Rica</option>

                  <option value="IV">IV Cote d’ivoire (Ivory Coast)</option>

                  <option value="HR">HR Croatia</option>

                  <option value="CU">CU Cuba</option>

                  <option value="CY">CY Cyprus</option>

                  <option value="EZ">EZ Czech Republic</option>

                  <option value="DA">DA Denmark</option>

                  <option value="DX">DX Dhekelia Sovereign Base Area</option>

                  <option value="DJ">DJ Djibouti</option>

                  <option value="DO">DO Dominica</option>

                  <option value="DR">DR Dominican Republic</option>

                  <option value="TT">TT East Timor</option>

                  <option value="EC">EC Ecuador</option>

                  <option value="EG">EG Egypt</option>

                  <option value="ES">ES El Salvador</option>

                  <option value="UK">UK England</option>

                  <option value="EK">EK Equatorial Guinea</option>

                  <option value="ER">ER Eritrea</option>

                  <option value="EN">EN Estonia</option>

                  <option value="ET">ET Ethiopia</option>

                  <option value="">EU Europa Island</option>

                  <option value="">FK Falkland Islands (Islas Malvinas)</option>

                  <option value="">FO Faroe Islands</option>

                  <option value="">FJ Fiji</option>

                  <option value="">FI Finland</option>

                  <option value="">FR France</option>

                  <option value="">FG French Guiana</option>

                  <option value="">FP French Polynesia</option>

                  <option value="">FS French South Antarctic</option>

                  <option value="">GB Gabon</option>

                  <option value="">GA Gambia, The</option>

                  <option value="">GZ Gaza Strip</option>

                  <option value="">GG Georgia</option>

                  <option value="">GM Germany</option>

                  <option value="">GH Ghana</option>

                  <option value="">GI Gibraltar</option>

                  <option value="">GO Glorioso Islands</option>

                  <option value="">GR Greece</option>

                  <option value="">GL Greenland</option>

                  <option value="">GJ Grenada</option>

                  <option value="">GP Guadeloupe</option>

                  <option value="">GT Guatemala</option>

                  <option value="">GK Guernsey</option>

                  <option value="">GV Guinea</option>

                  <option value="">PU Guinea-Bissau</option>

                  <option value="">GY Guyana</option>

                  <option value="">HA Haiti</option>

                  <option value="">HM Heard Island McDonald Island</option>

                  <option value="">HO Honduras</option>

                  <option value="">HK Hong Kong</option>

                  <option value="">HQ Howland Island</option>

                  <option value="">HU Hungary</option>

                  <option value="">IC Iceland</option>

                  <option value="">IN India</option>

                  <option value="">ID Indonesia</option>

                  <option value="">IR Iran</option>

                  <option value="">IZ Iraq</option>

                  <option value="">EI Ireland</option>

                  <option value="">IS Israel</option>

                  <option value="">IT Italy</option>

                  <option value="">JM Jamaica</option>

                  <option value="">JN Jan Mayan</option>

                  <option value="">JA Japan</option>

                  <option value="">DQ Jarvis Island</option>

                  <option value="">JE Jersey</option>

                  <option value="">JQ Johnston Atoll</option>

                  <option value="">JO Jordan</option>

                  <option value="">JU Juan de Nova Island</option>

                  <option value="">KZ Kazakhstan</option>

                  <option value="">KE Kenya</option>

                  <option value="">KQ Kingman Reef</option>

                  <option value="">KR Kiribati</option>

                  <option value="">KN Korea, Repl of (North)</option>

                  <option value="">KS Korea, Repl of (South)</option>

                  <option value="">KU Kuwait</option>

                  <option value="">KG Kyrgyzstan</option>

                  <option value="">LA Laos</option>

                  <option value="">LG Latvia</option>

                  <option value="">LE Lebanon</option>

                  <option value="">LT Lesotho</option>

                  <option value="">LI Liberia</option>

                  <option value="">LY Libya</option>

                  <option value="">LS Liechtenstein</option>

                  <option value="">DE COUNTRY CO</option>

                  <option value="">LH Lithuania</option>

                  <option value="">LU Luxembourg</option>

                  <option value="">MC Macau</option>

                  <option value="">MK Macedonia</option>

                  <option value="">MA Madagascar</option>

                  <option value="">MI Malawi</option>

                  <option value="">MY Malaysia</option>

                  <option value="">MV Maldives</option>

                  <option value="">ML Mali</option>

                  <option value="">MT Malta</option>

                  <option value="">IM Man, Isle of</option>

                  <option value="">RM Marshall Islands</option>

                  <option value="">MB Martinique</option>

                  <option value="">MR Mauritania</option>

                  <option value="">MP Mauritius</option>

                  <option value="">MF Mayotte</option>

                  <option value="">MX Mexico</option>

                  <option value="">FM Micronesia, Federated States of</option>

                  <option value="">MQ Midway Islands</option>

                  <option value="">MD Moldova</option>

                  <option value="">MN Monaco</option>

                  <option value="">MG Mongolia</option>

                  <option value="">MJ Montenegro</option>

                  <option value="">MH Montserrat</option>

                  <option value="">MO Morocco</option>

                  <option value="">MZ Mozambique</option>

                  <option value="">WA Nambia</option>

                  <option value="">NR Nauru</option>

                  <option value="">BQ Navassa Island</option>

                  <option value="">NP Nepal</option>

                  <option value="">NL Netherlands</option>

                  <option value="">NT Netherlands Antilles</option>

                  <option value="">NC New Caledonia</option>

                  <option value="">NZ New Zealand</option>

                  <option value="">NU Nicaragua</option>

                  <option value="">NG Niger</option>

                  <option value="">NI Nigeria</option>

                  <option value="">NE Niue</option>

                  <option value="">NM No Man’s Land</option>

                  <option value="">NF Norfolk Island</option>

                  <option value="">UK Northern Ireland</option>

                  <option value="">NO Norway</option>

                  <option value="">MU Oman</option>

                  <option value="">PK Pakistan</option>

                  <option value="">PS Palau</option>

                  <option value="">LQ Palmyra Atoll</option>

                  <option value="">PM Panama</option>

                  <option value="">PP Papua New Guinea</option>

                  <option value="">PF Paracel Islands</option>

                  <option value="">PA Paraguay</option>

                  <option value="">PE Peru</option>

                  <option value="">RP Philippines</option>

                  <option value="">PC Pitcairn Island</option>

                  <option value="">PL Poland</option>

                  <option value="">PO Portugal</option>

                  <option value="">QA Qatar</option>

                  <option value="">RE Reunion</option>

                  <option value="">RO Romania</option>

                  <option value="">RS Russia</option>

                  <option value="">RW Rwanda</option>

                  <option value="">SC St Kitts and Nevis</option>

                  <option value="">SH St Helena</option>

                  <option value="">ST St Lucia</option>

                  <option value="">SB St Pierre and Miquelon</option>

                  <option value="">VC St Vincent Grenadines</option>

                  <option value="">WS Samoa</option>

                  <option value="">SM San Marino</option>

                  <option value="">TP Sao Tome and Principe</option>

                  <option value="">SA Saudi Arabia</option>

                  <option value="">UK Scotland</option>

                  <option value="">SG Senegal</option>

                  <option value="">RB Serbia</option>

                  <option value="">SE Seychelles</option>

                  <option value="">SL Sierra Leone</option>

                  <option value="">SN Singapore</option>

                  <option value="">LO Slovakia</option>

                  <option value="">SI Slovenia</option>

                  <option value="">BP Solomon Islands</option>

                  <option value="">SO Somalia</option>

                  <option value="">SF South Africa</option>

                  <option value="">SX South GA South Sandwich Islands</option>

                  <option value="">SP Spain</option>

                  <option value="">PG Spratly Islands</option>

                  <option value="">CE Sri Lanka</option>

                  <option value="">SU Sudan</option>

                  <option value="">NS Suriname</option>

                  <option value="">SV Svalbard</option>

                  <option value="">WZ Swaziland</option>

                  <option value="">SW Sweden</option>

                  <option value="">SZ Switzerland</option>

                  <option value="">SY Syria</option>

                  <option value="">TW Taiwan</option>

                  <option value="">TI Tajikistan</option>

                  <option value="">TZ Tanzania, United Republic of</option>

                  <option value="">TH Thailand</option>

                  <option value="">TO Togo</option>

                  <option value="">TL Tokelau</option>

                  <option value="">TN Tonga</option>

                  <option value="">TD Trinidad and Tobago</option>

                  <option value="">TE Tromelin Island</option>

                  <option value="">TS Tunisia</option>

                  <option value="">TU Turkey</option>

                  <option value="">TX Turkmenistan</option>

                  <option value="">TK Turks and Caicos Islands</option>

                  <option value="">TV Tuvalu</option>

                  <option value="">UG Uganda</option>

                  <option value="">UP Ukraine</option>

                  <option value="">AE United Arab Emirates</option>

                  <option value="">UK United Kingdom</option>

                  <option value="">UY Uruguay</option>

                  <option value="">UZ Uzbekistan</option>

                  <option value="">NH Vanuatu</option>

                  <option value="">VT Vatican City</option>

                  <option value="">VE Venezuela</option>

                  <option value="">VM Vietnam</option>

                  <option value="">VI Virgin Islands (British)</option>

                  <option value="">WQ Wake Island</option>

                  <option value="">UK Wales</option>

                  <option value="">WF Wallis and Futuna</option>

                  <option value="">WE West Bank</option>

                  <option value="WI">WI Western Sahara</option>

                  <option value="YM">YM Yemen</option>

                  <option value="ZA">ZA Zambia</option>

                  <option value="ZI">ZI Zimbabwe</option>

                  <option value="OC">OC Other Countries</option>

                </select>

              </div>`

     </div>

   </div>

 </div>

</div>

<div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">Employer State Constants - Repeats in Box 15 without typing OPTIONAL.</h3>

  </div>

  <div class="panel-body">

    <div class="row">

      <div class="col-md-12">

         <label class="control-label" for="state_code"><span class="label-inner">State Code</span>

            </label>

            <div data-editor="">

              <input id="c54_state_code" type="hidden" value="<?php echo !empty($c_details[0]->c_state_code) ? $c_details[0]->c_state_code : '' ?>">

              <select id="state_code" class="form-control" name="state_code">

                <option value="">Choose...</option>

                <option value="AA">AA - Armed Forces Americas</option>

                <option value="AE">AE - Armed Forces Europe</option>

                <option value="AK">AK - Alaska</option>

                <option value="AL">AL - Alabama</option>

                <option value="AP">AP - Armed Forces Pacific</option>

                <option value="AR">AR - Arkansas</option>

                <option value="AS">AS - American Samoa</option>

                <option value="AZ">AZ - Arizona</option>

                <option value="CA">CA - California</option>

                <option value="CO">CO - Colorado</option>

                <option value="CT">CT - Connecticut</option>

                <option value="DC">DC - District of Columbia</option>

                <option value="DE">DE - Delaware</option>

                <option value="FL">FL - Florida</option>

                <option value="FM">FM - Federated Micronesia</option>

                <option value="GA">GA - Georgia</option>

                <option value="GU">GU - Guam</option>

                <option value="HI">HI - Hawaii</option>

                <option value="IA">IA - Iowa</option>

                <option value="ID">ID - Idaho</option>

                <option value="IL">IL - Illinois</option>

                <option value="IN">IN - Indiana</option>

                <option value="KS">KS - Kansas</option>

                <option value="KY">KY - Kentucky</option>

                <option value="LA">LA - Louisiana</option>

                <option value="MA">MA - Massachusetts</option>

                <option value="MD">MD - Maryland</option>

                <option value="ME">ME - Maine</option>

                <option value="MH">MH - Marshall Islands</option>

                <option value="MI">MI - Michigan</option>

                <option value="MN">MN - Minnesota</option>

                <option value="MO">MO - Missouri</option>

                <option value="MP">MP - N. Mariana Islands</option>

                <option value="MS">MS - Mississippi</option>

                <option value="MT">MT - Montana</option>

                <option value="NC">NC - North Carolina</option>

                <option value="ND">ND - North Dakota</option>

                <option value="NE">NE - Nebraska</option>

                <option value="NH">NH - New Hampshire</option>

                <option value="NJ">NJ - New Jersey</option>

                <option value="NM">NM - New Mexico</option>

                <option value="NV">NV - Nevada</option>

                <option value="NY">NY - New York</option>

                <option value="OH">OH - Ohio</option>

                <option value="OK">OK - Oklahoma</option>

                <option value="OR">OR - Oregon</option>

                <option value="PA">PA - Pennsylvania</option>

                <option value="PR">PR - Puerto Rico</option>

                <option value="PW">PW - Palau</option>

                <option value="RI">RI - Rhode Island</option>

                <option value="SC">SC - South Carolina</option>

                <option value="SD">SD - South Dakota</option>

                <option value="TN">TN - Tennessee</option>

                <option value="TX">TX - Texas</option>

                <option value="UT">UT - Utah</option>

                <option value="VA">VA - Virginia</option>

                <option value="VI">VI - US Virgin Islands</option>

                <option value="VT">VT - Vermont</option>

                <option value="WA">WA - Washington</option>

                <option value="WI">WI - Wisconsin</option>

                <option value="WV">WV - West Virginia</option>

                <option value="WY">WY - Wyoming</option>

              </select>

              </div>`

      </div>

    </div>

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="state_id_no"><span class="label-inner">State ID No</span>

        </label>

    <div data-editor="">

      <input id="c54_email" class="form-control" name="state_id_no" type="text" value="<?php echo !empty($c_details[0]->c_state_id_no) ? $c_details[0]->c_state_id_no : '' ?>"/>

    </div>

      </div>

    </div>

 </div>

</div>

<div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">Employer State Constants - Repeats in Box 15 without typing OPTIONAL.</h3>

  </div>

  <div class="panel-body">

    <div class="row">

      <div class="col-md-12">

        <div class="form-group field-name">

             <label class="control-label" for="c54_name">Select Code

              </label>

              <div data-editor="">

                    <label>

                       <input type="checkbox" name="third_party_sick_pay" value="1" type="radio"<?php echo !empty($c_details[0]->third_party_sick_pay) == '1' ? 'checked' : '' ?>>

                        Third Party Sick Pay

                    </label>

                    <br/>

                    <label>

                     <input type="checkbox" name="terminating_business" value="2" type="radio"<?php echo !empty($c_details[0]->terminating_business) == '2' ? 'checked' : '' ?>>

                      Terminating Business this Tax Year.

                    </label>

              </div>

            </div>

        </div>

    </div>

 </div>

</div>

<div class="col-md-12">

  <div class="row">

    <div class="col-md-6"><button type="button" id="next-4" class="btn btn-lg btn-block btn-primary btn-green-bg">Next</button></div>

    <?php if(isset($c_details[0])){ ?>
    <div class="col-md-6"><a href="<?=base_url('vendor');?>/<?=!empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <?php } else { ?>
      <div class="col-md-6"><a href="<?=base_url('company');?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <? } ?>

  </div>

</div>

</div>

<div id="fifth-panel" class="hide">

  <div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">W-3 Box 13 for third-party sick pay use only (see instructions) OPTIONAL</h3>

  </div>

  <div class="panel-body">

    <div class="row">

      <div class="col-md-12">

        <label class="control-label" for="w3_box_13"><span class="label-inner">W-3 Box 13<help style="margin-left: 10px;">(Optional)</help></span>

        </label>

        <div data-editor="">

          <input id="c54_email" class="form-control" name="w3_box_13" type="text" value="<?php echo !empty($c_details[0]->w3_box_13) ? $c_details[0]->w3_box_13 : '' ?>"/>

        </div>

      </div>

    </div>

  </div>

 </div>

 <div class="panel panel-primary">

  <div class="panel-heading">

    <h3 class="panel-title">W-3 Box 14 Income tax withheld by payer of 3rd party sick pay (see instructions).

</h3>

  </div>

  <div class="panel-body">

    <div class="row">

          <div class="col-md-12">

            <label class="control-label" for="w3_box_14"><span class="label-inner">W3-Box 14<help style="margin-left: 10px;">(Optional)</help></span>

            </label>

            <div data-editor="">

              <input id="c54_email" class="form-control" name="w3_box_14" type="text" value="<?php echo !empty($c_details[0]->w3_box_14) ? $c_details[0]->w3_box_14 : '' ?>"/>

               <input id="c54_email" class="form-control" name="form_type" value="W-2" type="hidden" />

              <p class="help-block">Box 14 is included in electronic file to the SSA</p>

            </div>

          </div>

        </div>

  </div>

 </div>

 <div class="col-md-12">

  <div class="row">

    <div class="col-md-6"><button type="submit" class="btn btn-lg btn-block btn-primary btn-green-bg">Save and Complete New Filer</button></div>
    <?php if(isset($c_details[0])){ ?>
    <div class="col-md-6"><a href="<?=base_url('vendor');?>/<?=!empty($c_details[0]->FileSequence) ? $c_details[0]->FileSequence : '' ?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <?php } else { ?>
      <div class="col-md-6"><a href="<?=base_url('company');?>" class="btn btn-lg btn-block btn-primary btn-red-bg" style="color: #ffffff !important;">Cancel</a></div>
    <? } ?>

  </div>

</div>

</div>

</form>

</div>

<div class="col-md-3 hide">

<div class="alert alert-panel">

<p>If you manage W-9 requests for many companies, you can import all the companies at once.</p>

<p style="margin-top:1em"><a href="<?=base_url('import');?>" class="btn btn-default btn-hot">Use CSV Import</a></p>

</div>

</div>

</div>

</div>

</div>

</div>

<script>

$(document).ready(function () {

if($('#c54_c_state').val() !=''){

$('#c54_state').val($('#c54_c_state').val());

}

if($('#c_state_lokesh').val() !=''){

$('#state').val($('#c_state_lokesh').val());

}

if($('#c54_state_code').val() !=''){

$('#state_code').val($('#c_state_lokesh').val());

}

if($('#c54_country_code').val() !=''){

$('#foreign_country_code').val($('#c_state_lokesh').val());

}


$('#companyform').validate({ // initialize the plugin

rules: {

name: {

required: true,

},

tin: {

required: true,
number: true,
matches: "[0-9]-",
minlength: 9,
maxlength: 9,

},

shipping_address: {

required: true,

},

city: {

required: true,

},

state :{

required: true,

},

zip:{

required: true,

digits: true,

minlength: 5,

maxlength: 5,

},

email: {

required: true,

email: true,

},

telephone :{

required: true,

digits: true,

minlength: 10,

maxlength: 10,

},

},

messages: {

name: {

required: 'Required',

},

tin: {

required: 'Required',

digits: 'Not valid',

minlength: 'Must contain nine digits with optional dashes',

maxlength: 'Must contain nine digits with optional dashes',

},

shipping_address: {

required: 'Required',

},

city: {

required: 'Required',

},

state :{

required: 'Required',

},

zip:{

required: 'Required',

digits: 'Must be a valid U.S. ZIP code',

minlength: 'Must be a valid U.S. ZIP code',

maxlength: 'Must be a valid U.S. ZIP code',

},

email: {

required: 'Required',

email: 'Invalid email address',

},

telephone :{

required: 'Required',

digits: 'Must be 10 digits',

minlength: 'Must be 10 digits',

maxlength: 'Must be 10 digits',

},

}

});

});

</script>