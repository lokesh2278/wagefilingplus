<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<style type="text/css">
a, a:visited {
    color: green !important;
}
</style>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<div id="app-container">
  <div id="page" class="page">

    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
<div class="row">
  <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
    <h1><a class="company-link"><span class="company-name">Main Menu</span></a></h1>
  </div>

</div>
</div>
</div>

      </div>
    </div>

    <div class="content-container" id="main-view-content">
    <div>
    <div class="company-index-container">
    <div>
    <div class="container">

  <div class="row">
     <div class="col-md-12">
      <div class="row">
        <div class="col-md-12" style="text-align: center;">
          <div class="row">
            <div class="col-md-4 col-md-offset-4">
<label>Earliest Tax Year:</label>
<select class="form-control" name="TaxYear" id="Taxyr123"  >
<?php $yearC = isset($_GET['year']) ? $_GET['year'] : '0000'; //$this->uri->segment(2); ?>
<option <?php if (0000 == $yearC) {echo 'selected';}?>  value="0000" >Select Tax Year</option>
<?php
$startdate = 2005;
$enddate = date("Y") - 1;
$years = range($startdate, $enddate);
foreach ($years as $year) {
    if ($year == $yearC) {$sel = 'selected';} else { $sel = '';}
    echo "<option " . $sel . "  value='" . $year . "'>" . $year . " </option>";
}?>
</select>

              <br />
            </div>
          </div>
          <p class="lead"><strong>Logged in as:</strong><?=$u_details[0]->Ucompany?> <?=$u_details[0]->Uphone?>, <?=$u_details[0]->UEmail?> CustID: <?=$u_details[0]->CustID?></p>
        </div>

      </div>
      <div class="table-responsive">
      <div class="not-found" style="display: none;">No Result Found</div>
      <table name="example-table" class="company-table table table-striped table-hover blazing-money DeletableTable table-bordered">
        <thead style="background-color: #337ab7; color: #ffffff;">
          <tr>
            <th width="2" data-tsort-type="number">FILE NUMBER</th>
            <th width="2" data-tsort-type="number">TAX YEAR</th>
            <th width="3" data-tsort-type="string">FILING COMPANY</th>
            <th width="2" data-tsort-type="string" >FORM TYPE</th>
            <th width="2" data-tsort-type="string" >MOVE FILES TO CHECK OUT</th>
            <th width="1" data-tsort-type="string" >E-FILED</th>
            <th width="1" data-tsort-type="number" style="text-align:right">STATUS</th>
          </tr>
        </thead>
        <tbody class="companies-container">

     <?php foreach ($companies as $company) {
    //echo"<PRE>";print_r($company);
    if ($yearC <= $company->TaxYear || $company->FormType == 'W-9s') {
        ?>
      <tr data-id="<?php echo isset($company->FileSequence) ? $company->FileSequence : ''; ?>">
            <td class="edit-link">
			<?php if ($company->FormType == 'W-9s') {?>
				<a class="show-company-lnk" href="<?=base_url('companyW9s')?>/<?=$company->FileSequence?>"><?php echo isset($company->FileSequence) ? $company->FileSequence : ''; ?></a>
			<?php } else {echo isset($company->FileSequence) ? $company->FileSequence : '';}?></td>
            <td class="name"><?php echo isset($company->TaxYear) ? $company->TaxYear : ''; ?></td>
            <td class="name">
             <?php if ($company->eFiled == 0) {?>

              <?php if ($company->FormType == 'W-9s') {?>
				<a class="show-company-lnk" name="name" href="<?=base_url('vendorw9s')?>/<?=$company->FileSequence?>"><?=$company->FileName . ' '?><?=$company->c_employee_ein?></a>
			<?php } else {?>

			<a class="show-company-lnk" name="name" href="<?=base_url('vendor')?>/<?=$company->FileSequence?>"><?=$company->FileName . ' '?><?=$company->c_employee_ein?></a>

			<?php }?>





              <?php } else {
            ?>
              <?php echo isset($company->FileName) ? $company->FileName : '';
            echo isset($company->c_employee_ein) ? $company->c_employee_ein : ''; ?>
              <?php }?>
            </td>
            <td style="text-align:right">
              <?=$company->FormType . ' '?><span class="badge" name="w9_count">
                <?php if ($company->FormType == 'W-9s') {
            echo w9_count($company->FileSequence);} else if ($company->FormType == '1099-Misc') {
            echo misc_count($company->FileSequence);} else {
            echo w2_count($company->FileSequence);
        }
        ?></span></b>
            </td>
            <td>



			<?php if ($company->FormType == 'W-9s') {?>

            <!-- <a style="color: #337ab7 !important;" href="<?php //echo base_url() . "frontend/copyFormTo/$company->FileSequence/1099-Misc/"; ?>">Copy to 1099 form</a> -->

			<?php } else {?>
            <input type="checkbox" class="chk" name="checkout[]" value="<?=$company->FileSequence?>" <?php if ($company->eFiled == '1') {echo 'disabled';} else {echo ' ';}?> />
            <?php } /* if ($company->FormType == '1099-Misc') { ?>

        <a href="<?php echo base_url()."frontend/copyFormTo/$company->FileSequence/W-9s/";?>">Copy to W-9s</a>

        <?php } */?>

            </td>
            <td>
            <?php if ($company->FormType != 'W-9s') {?><input type="checkbox" <?php if ($company->eFiled == '1') {echo 'checked';} else {echo ' ';}?> disabled="disabled"/><?php }?> </td>
            <td name="signed_count" style="text-align:right">
            <?php if ($company->FormType != 'W-9s') {
            ?>
			<?php
if ($company->eFiled == '1') {?>
  <a href="<?=base_url('printview')?>/<?=$company->FileSequence?>">Paid Print/View</a>
<?php } else if ($company->eFiled == 0) {
                echo 'Waiting-UnPaid';
            } else if ($company->status == 0) {echo 'Waiting-UnPaid';}?>

        <?php }?>
        </td>

        </tr>
      <?php
}

}?>
    </tbody>
</table>
</div>
    </div>

  </div>
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <p class="lead" style="text-align: center;">Click the <strong style="color: green;">GREEN LINK ABOVE</strong> to Add/Edit that Company's forms.</p>
      <p class="lead" style="text-align: center;">If you have completed a filing company and are ready to checkout, please check the box associated with that company and click Checkout.</p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-11 col-md-offset-1">
      <table>
        <tr>
          <td style="padding: 12px;"><a href="<?=base_url('company');?>"><img src="<?php echo iaBase(); ?>assets/images/new-company.jpg" width="225px" height="45px" /></a></td>
          <td><p class="lead"><img src="<?php echo iaBase(); ?>assets/images/green-arrow.jpg" width="35px" height="36px"> Start here to setup a NEW company for any Tax Year <span style="color: green; cursor: pointer;" id="new-company-info">(more info)</span></p></td>
        </tr>
        <tr>
          <td style="padding: 12px;"><!-- <button class="btn  btn-default btn-hot delete-active"><i class="glyphicon glyphicon-shopping-cart"></i> CHECKOUT</button> --><img src="<?php echo iaBase(); ?>assets/images/checkout.jpg" width="225px" height="45px" class="delete-active" style="cursor: pointer;" /></td>
          <td><p class="lead"><img src="<?php echo iaBase(); ?>assets/images/grey-arrow.jpg" width="35px" height="36px"> Select files above to pay and print for your forms <span style="color: green; cursor: pointer;" id="checkout-info">(more info)</span></p></td>
        </tr>
        <tr>
          <td style="padding: 12px;"><a href="<?=base_url('filemanager')?>"><img src="<?php echo iaBase(); ?>assets/images/file-manager.jpg" width="225px" height="45px" /></a></td>
          <td><p class="lead"><img src="<?php echo iaBase(); ?>assets/images/grey-arrow.jpg" width="35px" height="36px"> Click here to View 1096/W-3's, Bring Files Forward and View e-File Confirmations <span style="color: green; cursor: pointer;" id="filemanager-info">(more info)</span></p></td>
        </tr>
        <tr>
          <td style="padding: 12px;"><a href="<?=base_url('useredit');?>"><img src="<?php echo iaBase(); ?>assets/images/edit-account.jpg" width="225px" height="45px" /></a></td>
          <td><p class="lead"><img src="<?php echo iaBase(); ?>assets/images/grey-arrow.jpg" width="35px" height="36px"> Edit or change your login credentials</p></td>
        </tr>

        <tr>
          <td style="padding: 12px;"><a href="<?=base_url('companyW9s');?>"><img src="<?php echo iaBase(); ?>assets/images/w9snewcompany.jpg" width="225px" height="45px" /></a></td>
          <td><p class="lead"><img src="<?php echo iaBase(); ?>assets/images/grey-arrow.jpg" width="35px" height="36px"> Add new Company Requesting W-9 form</p></td>
        </tr>
      </table>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Assign CPAs to Comapny</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <label class="checkall"><input class="checkall" type="checkbox"> Select all</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary save">Save Changes</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
      $('.delete-active').click(function(){
      var chkArray = [];
	  var ids ='';var ids1 ='';
      if($(".chk").is(':checked')) {
          $(".chk:checked").each(function() {
            chkArray.push($(this).val());
				ids += $(this).val() + "_";
				ids1 += $(this).val() + "_";
          });
		  ids = ids.slice(0, -1);
        var dataNew = {chkArray : chkArray};
        var baseUrl = '<?php echo base_url('dofiles') ?>?chkArray='+ ids1+'&ids='+ ids;
        window.location.href = baseUrl;
      }
       else {
          alert('Please check any one checkbox');
       }
     });
      $("#new-company-info").click(function(){
        alert("Enter the New Company issuing the W-2 or 1099 for originals or corrections.  Click the FILE MANAGER button for companies already setup.");
      });
      $("#checkout-info").click(function(){
        alert("Once your 1099-MISC or W-2 forms are complete, select those files in Main Menu by clicking the small box and a checkmark will appear - Then hit Checkout.");
      });
      $("#filemanager-info").click(function(){
        alert("Allows you to:   \n- Create a new file with a previous company for entering more originals or corrections.\n- Bring and entire file forward from last year when payees are mostly the same to avoid detailed re-typing\n- Print totals and other options.");
      });
  });
</script>

<script>
	$(function(){
	  // bind change event to select
	  $('#Taxyr123').on('change', function () {
		  var url = $(this).val();
		 // alert(url);
		  if (url && url!=0000) {
		 	 window.location.href = '<?php echo base_url(); ?>blaze?year='+url;
			 return true;
		  }else{
			  window.location.href ='<?php echo base_url(); ?>blaze';
			  return false;
		}
		  return false;
	  });
	});
</script>