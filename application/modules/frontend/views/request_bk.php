<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">

<div class="row">
  <div id="nav-company" class="col-md-12 col-lg-12">
    <h1><a class="company-link" href="<?=base_url('vendor')?>/<?=$c_details[0]->c_id?>"><span class="company-name">You are now issuing W-9 Request for <?=!empty($c_details[0]->c_name) ? $c_details[0]->c_name : ''?></span></a></h1>
  </div>
 <!--  <div id="nav-widget" class="col-md-6 col-lg-5 widget-wider">
   <div>
     <div class="tab-prompt">Choose a form:</div>
     <nav class="subnav-tabs btn-toolbar">

     <span class="border-button-wrap"><a class="btn btn-lg btn-default border-active" href="#" aria-controls="main-view-content" role="tab">W-9</a></span>

     </nav>
  </div>
 </div> -->
</div>
<a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i>Main Menu</a>
</div></div>
        <!--div id="alerts-container" class="alerts-container"><div><div><div class="alert alert-success" role="alert">Created company Comapny</div>
</div></div></div-->
      </div>
    </div>

    <div class="content-container" id="main-view-content"><div><div class="container">
  <div class="row">

   <!-- <div class="col-md-4 col-lg-5 import-container col-md-push-8 col-lg-push-7">
      <div>
        <div class="pending-step panel panel-primary" style="display: block;">
          <div class="panel-heading">
            <h2 class="panel-title">Request Many Vendor W-9s</h2>
          </div>
          <div class="panel-body">
            <p><strong>First:</strong> Use this CSV template: <a href="<?=iaBase()?>assets/csv/formate/csv_template_w9_vendor.csv" class="btn btn-primary btn-lg">W-9 CSV Template</a></p>
            <p><strong>Second:</strong> Add vendors to the file, <strong>Save</strong> locally, then <strong>click Select File</strong> below.</p>
            <div>
              <span class="btn-choose-file btn-file-input btn btn-classic">
                <span>Select File</span>
                <div class="hidden-inner">
                <input class="hidden-inner-input" type="file" id="fileSelected" name="csv_file"></div>
              </span>
              <span name="filename" id="filename">None selected</span>
              <div class="error-block" style="padding-top: 12px; display: none;"><div name="error" class="label label-danger" id="error-block"></div></div>
            </div>
         </div>
         <div class="panel-footer">
           <button type="button" class="btn btn-lg btn-primary import-button disabled">Import CSV</button>
         </div>
       </div>

      <div class="pending-step panel panel-primary processing-step" style="display: none;">
        <div class="panel-heading">
          <h2 class="panel-title">Request Many Vendor W-9s</h2>
        </div>
        <div class="panel-body">
          <p><strong>Status:</strong> <span name="description">Uploading file...</span></p>
          <p>
            </p><div class="progress progress-striped active">
              <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 1%;"></div>
            </div>
          <p></p>
        </div>
      </div>
<!--div class="pending-step panel panel-primary results-step" style="display: none;">
  <div class="panel-heading">
    <h2 class="panel-title">Request Many Vendor W-9s</h2>
  </div>
  <div class="panel-body">
    <div class="results-errors" style="display: none;">
      <p class="text-danger">
        <strong>Bad Records<span class="error-counts" style="display: none;"> (first <span class="error-limit"></span> of <span name="num_errors"></span>)</span>:</strong>
      </p>
      <div class="row">
        <div class="errors-container col-xs-12"></div>
      </div>
    </div>
    <p>
      <strong>Good Records<span class="record-counts" style="display: none;"> (first <span class="record-limit"></span> of <span name="num_records"></span>)</span>:</strong>
    </p>
    <div class="row">
      <div class="records-container col-xs-12"></div>
    </div>
  </div>
  <div class="panel-footer">
    <button type="button" class="continue-button btn btn-primary btn-lg" style="display: none;">Finish and Send</button>
    <button type="button" class="cancel-button see-bindings-for-display-classes btn btn-lg btn-primary">Cancel</button>
  </div>
</div>

<div class="pending-step panel panel-danger error-step" style="display: none;">
  <div class="panel-heading">
    <h2 class="panel-title">Request Many Vendor W-9s</h2>
  </div>
  <div class="panel-body">
    <div class="error-block" style="display: none;">
      <p name="error"></p>
      <p name="error_result"></p>
    </div>
  </div>
</div-->
<!-- </div>
</div> -->

<form class="requestForm" id="requestForm" action="" accept-charset="UTF-8" method="post">
    <div class="col-md-10 col-lg-10 manual-container col-md-offset-1">
      <div>
        <div class="panel panel-info">
  <div class="panel-heading">
    <h2 class="panel-title">Enter the name and email address of the Contractor below </h2>
  </div>
  <div class="panel-body">
    <ul class="requests-container list-unstyled">
      <li class="panel-stretch alternating">
        <div class="form-container">
          <div class="row">
            <input type="hidden" value="<?=!empty($c_details[0]->c_id) ? $c_details[0]->c_id : ''?>" name="c_id"/>
            <div class="col-sm-6" data-fields="name">
              <div class="form-group field-name">
                <label class="control-label" for="c183_name">
                   <span class="label-inner">Contractor Name</span>
                </label>
                <div data-editor="">
                  <input id="c183_name" class="form-control" name="v_name[]" maxlength="40" type="text">
                </div>
              </div>
            </div>
            <div class="col-sm-6" data-fields="email">
              <div class="form-group field-email">
                <label class="control-label" for="c183_email">
                  <span class="label-inner">Email</span>
                </label>
                <div data-editor="">
                  <input id="c183_email" class="form-control" name="v_email[]" type="text">
                </div>
              </div>
            </div>
           <!--  <div class="col-sm-4" data-fields="account_number">
             <div class="form-group field-account_number">
               <label class="control-label" for="c183_account_number">
                 <span class="label-inner">Account No. (optional)</span>
               </label>
                   <div data-editor="">
                     <input id="c183_account_number" class="form-control" name="account_number[]" type="text">
                   </div>
                </div>
              </div> -->
          </div>
        </div>
      </li>
      </ul>
  </div>
  <div class="panel-footer">
    <button type="button" class="btn-addmore-requests btn btn-info btn-lg mr-100"><i class="glyphicon glyphicon-plus"></i>Add Another</button>
    <button class="btn-send-requests btn btn-info btn-lg">Checkout and Send Requests</button>
  </div>
</div>
</div>
</form>
</div>
  </div>
</div>
</div></div>

  </div>
  <div class="modal-container"></div>
</div>


  <script>
  $(document).ready(function () {
    $('.btn-addmore-requests').on('click', function(){
      var lastLi = $('#requestForm').find('ul').children('li').last();
      var idVal = lastLi.find("input[name='v_name[]']" ).attr('id');
      var countField =  Number(idVal.replace(/\D/g,''))+1;
      var nameValue = 'c'+countField+'_name';
      //alert(lastLi); console.log(nameValue);
      var newLi = '<li class="panel-stretch alternating"><div class="form-container"><div class="row"><div class="col-sm-6" data-fields="name"><div class="form-group field-name"><label class="control-label" for="'+nameValue+'"><span class="label-inner">Contractor Name</span></label><div data-editor=""><input id="'+nameValue+'" class="form-control" name="v_name[]" maxlength="40" type="text"></div></div></div><div class="col-sm-6" data-fields="email"><div class="form-group field-email"><label class="control-label" for="'+nameValue+'"><span class="label-inner">Email</span></label><div data-editor=""><input id="'+nameValue+'" class="form-control" name="v_email[]" type="text"></div></div></div></div></div></li>';
      $('#requestForm').find('ul').append(newLi);
    });

    $('#requestForm').validate({ // initialize the plugin
        rules: {
            'v_name[]': {
                required: true,
            },
            'v_email[]': {
                required: true,
                email: true
            },

        },
        messages: {
            'v_email[]': {
                required: 'Required',
                email: 'Invalid email address'
            },
            'v_name[]': {
                required: 'Required',
            },
       }
    });

});

  </script>

  <script>
      var reader_offset = 0;    //current reader offset position
      var buffer_size = 1024;   //
      var pending_content = '';

      /**
      * Read a chunk of data from current offset.
      */
      function readAndSendChunk()
      {

        var file = $('#fileSelected')[0].files[0];

        if(file != undefined){
        var ext = file.name.split('.').pop();
        if(ext == 'csv'){
          $('.import-button').removeClass('disabled');
          $('#error-block').html( '');
          $('.error-block').hide();
          $('#filename').html(file.name);
        }else{

          $('#error-block').html( 'Please upload only "*.csv" files');
          $('.error-block').show();
           $('#filename').html('');
        }
        }else{

          $('#error-block').html( 'Please select a file');
          $('.error-block').show();
          $('#filename').html('');
        }

      }

      /**
      * Send data to server using AJAX
      */
      function sendImport()
      {
          var reader = new FileReader();
          var file = $('#fileSelected')[0].files[0];
          reader.onloadend = function(evt){

          //check for end of file
          if(evt.loaded == 0) return;

          //increse offset value
          reader_offset += evt.loaded;

          //check for only complete line
          var last_line_end = evt.target.result.lastIndexOf('\n');
          var content = pending_content + evt.target.result.substring(0, last_line_end);

          pending_content = evt.target.result.substring(last_line_end+1, evt.target.result.length);

          console.log(content);
          sendData(content);
        };
        var blob = file.slice(reader_offset, reader_offset + buffer_size);
        reader.readAsText(blob);
      }

     function sendData(content){
          var c_id= '<?=$c_details[0]->c_id?>';
          var sendData = { contents : content, c_id : c_id };
          //upload data
          $.ajax({
            url: "<?=base_url('importvendordata')?>",
            method: 'POST',
            data: sendData,
            }).done(function(response) {
            var obj = $.parseJSON(response);
            $('.import-button').removeClass('disabled');
            if(obj.status == 'fail')
            {
              $('#error-block').html( obj.message );
              $('.pending-step').show();
              $('.processing-step').hide();
              $('.error-block').show();
            }else{
              window.location.href = "<?=base_url('vendor')?>/"+c_id;
            }
          //try for next chank
           });
      }
      /**
      * On page load
      */
      $(function(){
        $('.import-button').click(function(){
          $(this).addClass('disabled');
          $('.error-block').hide();
          $('.pending-step').hide();
          $('.processing-step').show();
          reader_offset = 0;
          pending_content = '';
          sendImport();
        });
        $('.hidden-inner-input').change(function(){
          readAndSendChunk();
        })
      });
    </script>