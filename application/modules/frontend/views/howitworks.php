<style type="text/css">
  iframe{
    min-height: 500px;
  }

  @media only screen and (max-width: 600px) {
    body {
        background-color: lightblue;
    }

    iframe{
    min-height: 250px;
  }
 }
</style>
<div id="app-container">
    <div id="page" class="page">
       <div id="blaze-banner">
          <div class="container">
             <div class="navigation-container">
                <div class="wrap wrap-one-logo">               
                   <div class="row">
                      <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
                         <h1><a class="company-link"><span class="company-name">How It Works</span></a></h1>
                      </div>
                     
                   </div>
                </div>
             </div>
             <div id="alerts-container" class="alerts-container">
                <div></div>
             </div>
          </div>
       </div>
       <div class="content-container" id="main-view-content">
        <h3 class="red">Request, Recieve and Report W-9's to anyone, anywhere for $1.49</h3>
        <img id="new" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/irs2.png" style="display: none;">
        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
        </div>
            <section class="section-p-0">
                        <div class="container">
                          <div>
                            <div class="feature feature-1 text-center">
                              <iframe width="95%" src="https://www.youtube.com/embed/kPnx-iMqhwE">
                              </iframe>
                            </div>
                          </div>

                            <div class="row">
                                <div class="col-sm-6">
                                  <div class="feature feature-1 boxed">
                                    <div class="circle-steps">1</div>
                                        <div class="text-center">
                                          <div class="icon-tab">
                                          <i class="fa fa-desktop" aria-hidden="true"></i>
                                          <i class="fa fa-tablet" aria-hidden="true"></i>
                                          <i class="fa fa-mobile" aria-hidden="true"></i>
                                          <i class="fa fa-lock" aria-hidden="true"></i>
                                        </div>


                                          
                                          <!-- <img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/computer.png"><img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/smartphone.png"><img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/lock.png"> -->
                                            <!-- <i class="ti-pulse icon"></i> -->
                                            <h4 class="red">Start to finish securely from any
                                                <br class="hidden-sm"> computer, tablet or smart device</h4>
                                        </div>
                                        <p>
                                            To issue your first W-9, create your free account from any device. The entire process is protected under our 2048-bit SSL. One account will allow you to create as many companies as you like and issue unlimited W-9 requests.
                                        </p>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                  <div class="feature feature-1 boxed ">
                                    <div class="circle-steps">2</div>
                                        <div class="text-center">
                                          <div class="icon-tab">
                                          <i class="fa fa-mobile" aria-hidden="true"></i>
                                          <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                          <i class="fa fa-user" aria-hidden="true"></i>
                                          
                                        </div>

                                          <!-- <img class="ic-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/smartphone.png"><img class="arrow-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/rightarrow.png"><img class="ic-wid" src="<?php //echo str_replace("index.php/", "", base_url()); ?>/assets/images/people.png"> -->
                                            <!-- <i class="ti-dashboard icon"></i> -->
                                            <h4 class="red">All you need to issue the W-9 is their email
                                                <br class="hidden-sm">address</h4>
                                        </div>
                                        <p>
                                            Once your account is created, just enter the Contractors email address, checkout and send.  Your Contractor instantly receives the W-9 request along with instruction. <a id="myImg">See a sample W-9 request</a>
                                        </p>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                  <div class="feature feature-1 boxed">
                                    <div class="circle-steps">3</div>
                                        <div class="text-center">
                                          <div class="icon-tab">
                                          <i class="fa fa-user" aria-hidden="true"></i>
                                          <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                          <i class="fa fa-bell" aria-hidden="true"></i>
                                          
                                        </div>
                                          <!-- <img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/personenvelop.png"> -->
                                            <!-- <i class="ti-layers icon"></i> -->
                                            <h4 class="red">Contractor receives secure link to complete via email</h4>
                                        </div>
                                        <p>
                                            Contractor enters their data and can e-sign the W-9 with their mouse or fingertip. That form is then instantly saved to your account and you are notified via email that the form has been completed.
                                        </p>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <div class="feature feature-1 boxed">
                                      <div class="circle-steps">4</div>
                                        <div class="text-center">
                                          <div class="icon-tab">
                                          <i class="fa fa-users" aria-hidden="true"></i>
                                
                                          
                                        </div>
                                          <!-- <img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/people.png"> -->
                                            <!-- <i class="ti-package icon"></i> -->
                                            <h4 class="red">All your contractor W-9's saved in one location
                                                </h4>
                                        </div>
                                        <p>
                                            Within your account you can see all your signed W-9s and any that are still pending. You can also resend pending W-9's at no charge. Your W-9's can be downloaded as a pdf, or exported as a CSV at any time.
                                        </p>
                                    </div>
                                    
                                </div>
                            </div>
                            
                              <div>
                                    <div class="feature feature-1 boxed">
                                      <div class="circle-steps">5</div>
                                        <div class="text-center">
                                          <div class="icon-tab ">
                                             <img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/w9.png">
                                             <img class="ic-wid" style="width: 42px;" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/if_Arrow_Forward_1063879.png">
                                             <img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/1099.png">
                                          <img class="ic-wid" src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/adc.gif">
                                            
                                
                                          
                                        </div>

                                            <!-- <i class="ti-package icon"></i> -->
                                            <h4 class="red spacing">Contractor W-9 moved to 1099-Misc
                                                </h4>
                                        </div>
                                        <p>
                                           Since issuing a W-9 is the first step in reporting form 1099-MISC, we can move that data into a 1099-MISC for you. At the end of the year just need to add the amount paid and we'll e-file to the IRS, State and email your Contractor their recipient copy.
                                        </p>
                                    </div>
                                    
                                </div>                                                                               
                        </div>
                        
                    </section>
       </div>
    </div>
 
    <style>
      .spacing{
        margin-top: 16px ;
      }
      .red
      {
        color:red;
        text-align: center;
      }
      .circle-steps
      {
        /*background-color: #287ebb;
        border-radius: 50%;
        width: 55px;
        height: 46px;
        color: white;
        position: relative;
        text-align: center;
        font-size: 28px;
        line-height: 42px;
        margin-left: -26px;
        margin-top: -24px;*/
            background-color: #ffffff;
    border-radius: 50%;
    width: 47px;
    height: 45px;
    color: #4baef0;
    position: relative;
    text-align: center;
    font-size: 23px;
    line-height: 42px;
    margin-left: -26px;
    margin-top: -24px;
      }
     .ic-wid{
        width:55px;
      }
      .arrow-wid
      {
        width: 30px;
      }
      .fa{
        font-size: 55px;
        /*color: #337ab7 !important;*/
        color: #565656 !important;

            margin-right: 18px;

      }
      .icon-tab{
        position: relative;
        bottom: 9px;

      }
      .red{
      	color: #000 !important;
      }
      #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.49); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    /*margin: auto;*/
    display: block;
    width: 80%;
    max-width: 700px;
    top: -9%;
    left: 25%;
}

/* Caption of Modal Image */


/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
    opacity: 0.7;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
    opacity: 1;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
    </style>
    <script> 
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var newa = document.getElementById('new');
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = newa.src;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
modal.onclick = function() { 
    modal.style.display = "none";
}
</script>
