<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
<!-- <a href="#" id="blaze-logos" class="">
  <img id="w9-logo" src="<?php echo iaBase(); ?>assets/images/WageFilingw9-Logo-XSmall.png" alt="WageFiling W-9">
</a>  -->

<div class="row">
  <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
    <h1><a class="company-link"><span class="company-name"><?=companyName($cid)?></span></a></h1>
  </div>
</div>
<a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i> Main Menu</a>
</div>
</div>
        <div id="alerts-container" class="alerts-container"></div>
      </div>
    </div>

<div class="content-container" id="main-view-content"><div><div class="container">
  <div class="row">
    <div class="col-md-12">
     <div class="row">
      <div class="col-md-6 col-md-offset-3" align="center">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><?=$c_details[0]->c_name?></h3>
          </div>
          <div class="panel-body">
            <p class="lead">Form: <?=$c_details[0]->FormType?> File:<?=$c_details[0]->FileSequence?></br>  TaxID: <?=$c_details[0]->c_ssn_ein?>, TaxYear: <?=$c_details[0]->TaxYear?></p>
          </div>
        </div>
        <p class="lead">Check the Person to View and/or Print</p>

      </div>
    <div class="col-md-12">
      <div class="alert alert-info">
        <p class="lead"><strong>NOTE:</strong> On preview page you can choose option for print and download PDF.</p>
      </div>
      <div class="table-container" style="width:100%">
        <div class="table-responsive">
        <table class="w9-table table table-striped table-hover table-responsive DeletableTable table-bordered" name='example-table'>
          <thead style="background-color: #1a5d8a; color: #ffffff;">
            <tr class="float-delete-button">
    <th data-tsort="disabled">NO</th>
    <th>TAX ID NUMBER</th>
    <th class="sort" data-tsort-type="string"> PAYEE NAME</th>
    <th class="sort" data-tsort-type="string"> IRS File Number</th>
    <th class="sort" data-tsort-type="string"> E-FileDate</th>
    <th class="sort" data-tsort-type="string"> E-File Status</th>
    <th class="sort" data-tsort-type="string"> Mailed Date</th>
    <th class="sort" data-tsort-type="string"> IRS Accepted Date</th>
    <th data-tsort="disabled" data-tfilter="disabled">DOWNLOAD ALL <input type="Checkbox" name="prnt" id="printall" /></th>
  </tr>
</thead>
<tbody class="forms-container">

  <?php
// echo '<pre>';
// print_r($v_details);die;
$companyId = '';
if (!empty($v_details) && empty($d_details) || empty($v_details) && !empty($d_details) || !empty($v_details) && !empty($d_details)) {

    $companyId = isset($v_details[0]->FileSequence) ? $v_details[0]->FileSequence : '';
    if ($c_details[0]->FormType == '1099-Misc') {
        $counter = 0;
        //echo "<PRE>";print_r($v_details[0]->c_id);

        foreach ($v_details as $v_detail) {

            ?>
  <tr data-id="<?=!empty($v_detail->misc_id) ? $v_detail->misc_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($v_detail->recepient_id_no) ? $v_detail->recepient_id_no : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><?=!empty($v_detail->first_name) ? $v_detail->first_name : '';?></td>
    <?php }?>
    <td><a href="<?=base_url('confirmation')?>/<?=$c_details[0]->FileSequence?>/<?=$v_detail->misc_id?>"><?=!empty($v_detail->irs_file_number) ? $v_detail->irs_file_number : '';?></a></td>
    <td><?=!empty($v_detail->efile_date) ? $v_detail->efile_date : '';?></td>
    <td><?=!empty($v_detail->eFile_status) ? $v_detail->eFile_status : '';?></td>
    <td><?=!empty($v_detail->mailed_date) ? $v_detail->mailed_date : '';?></td>
    <td><?=!empty($v_detail->irs_accepted_date) ? $v_detail->irs_accepted_date : '';?></td>
    <td class="name"><input type="hidden" value="<?=$c_details[0]->FileSequence?>" id="cid" name=""><input type="Checkbox" class="chk" value="<?=$v_detail->recepient_id_no?>" class="checkbox" name="chk[]" ></td>
  </tr>
  <?php }
        foreach ($d_details as $d_detail) {

            ?>
  <tr data-id="<?=!empty($d_detail->div_id) ? $d_detail->div_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($d_detail->recepient_id_no) ? $d_detail->recepient_id_no : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><?=!empty($d_detail->first_name) ? $d_detail->first_name : '';?></td>
    <?php }?>
    <td><a href="<?=base_url('confirmation')?>/<?=$c_details[0]->FileSequence?>/<?=$v_detail->div_id?>"><?=!empty($d_detail->irs_file_number) ? $d_detail->irs_file_number : '';?></a></td>
    <td><?=!empty($v_detail->efile_date) ? $v_detail->efile_date : '';?></td>
    <td><?=!empty($v_detail->eFile_status) ? $v_detail->eFile_status : '';?></td>
    <td><?=!empty($v_detail->mailed_date) ? $v_detail->mailed_date : '';?></td>
    <td><?=!empty($v_detail->irs_accepted_date) ? $v_detail->irs_accepted_date : '';?></td>
    <td class="name"><input type="hidden" value="<?=$c_details[0]->FileSequence?>" id="cid" name=""><input type="Checkbox" class="chk" value="<?=$d_detail->recepient_id_no?>" class="checkbox" name="chk[]" ></td>
  </tr>
  <?php }

    } else if ($c_details[0]->FormType == '1099-Div') {
        $counter = 0;
        //echo "<PRE>";print_r($v_details[0]->c_id);
        foreach ($v_details as $v_detail) {

            ?>
  <tr data-id="<?=!empty($v_detail->misc_id) ? $v_detail->misc_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($v_detail->recepient_id_no) ? $v_detail->recepient_id_no : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><?=!empty($v_detail->first_name) ? $v_detail->first_name : '';?></td>
    <?php }?>
    <td><a href="<?=base_url('confirmation')?>/<?=$c_details[0]->FileSequence?>/<?=$v_detail->misc_id?>"><?=!empty($v_detail->irs_file_number) ? $v_detail->irs_file_number : '';?></a></td>
    <td><?=!empty($v_detail->efile_date) ? $v_detail->efile_date : '';?></td>
    <td><?=!empty($v_detail->eFile_status) ? $v_detail->eFile_status : '';?></td>
    <td><?=!empty($v_detail->mailed_date) ? $v_detail->mailed_date : '';?></td>
    <td><?=!empty($v_detail->irs_accepted_date) ? $v_detail->irs_accepted_date : '';?></td>
    <td class="name"><input type="hidden" value="<?=$c_details[0]->FileSequence?>" id="cid" name=""><input type="Checkbox" class="chk" value="<?=$v_detail->recepient_id_no?>" class="checkbox" name="chk[]" ></td>
  </tr>
  <?php }
        foreach ($d_details as $d_detail) {

            ?>
  <tr data-id="<?=!empty($d_detail->div_id) ? $d_detail->div_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($d_detail->recepient_id_no) ? $d_detail->recepient_id_no : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><?=!empty($d_detail->first_name) ? $d_detail->first_name : '';?></td>
    <?php }?>
    <td><a href="<?=base_url('confirmation')?>/<?=$c_details[0]->FileSequence?>/<?=$d_detail->div_id?>"><?=!empty($d_detail->irs_file_number) ? $d_detail->irs_file_number : '';?></a></td>
    <td><?=!empty($d_detail->efile_date) ? $d_detail->efile_date : '';?></td>
    <td><?=!empty($d_detail->eFile_status) ? $d_detail->eFile_status : '';?></td>
    <td><?=!empty($d_detail->mailed_date) ? $d_detail->mailed_date : '';?></td>
    <td><?=!empty($d_detail->irs_accepted_date) ? $d_detail->irs_accepted_date : '';?></td>
    <td class="name"><input type="hidden" value="<?=$c_details[0]->FileSequence?>" id="cid" name=""><input type="Checkbox" class="chk" value="<?=$d_detail->recepient_id_no?>" class="checkbox" name="chk[]" <?php if ($d_detail->eFile_status != 'Accepted') {echo 'disabled';}?>></td>
  </tr>
  <?php }

    }
    //for w2_form
    else {
        // print_r($v_details);die;
        $counter = 0;
        foreach ($v_details as $v_detail) {

            ?>
    <tr data-id="<?=!empty($v_detail->w2_id) ? $v_detail->w2_id : '';?>">
    <td class="company">Company</td>
    <td><?=++$counter;?></td>
    <td class="name"><?=!empty($v_detail->SSN) ? $v_detail->SSN : '';?></td>
    <?php foreach ($c_details as $c_data) {
                ?>
    <td class="name"><?=!empty($v_detail->FName) ? $v_detail->FName . ' ' . $v_detail->LName : '';?><?php if ($v_detail->voidbox == 1) {
                    echo '-void-';
                }
                ?></td>
    <?php }?>
    <td><a href="<?=base_url('confirmation')?>/<?=$c_details[0]->FileSequence?>/<?=$v_detail->misc_id?>"><?=!empty($v_detail->irs_file_number) ? $v_detail->irs_file_number : '';?></a></td>
    <td><?=!empty($v_detail->efile_date) ? $v_detail->efile_date : '';?></td>
    <td><?=!empty($v_detail->eFile_status) ? $v_detail->eFile_status : '';?></td>
    <td><?=!empty($v_detail->mailed_date) ? $v_detail->mailed_date : '';?></td>
    <td><?=!empty($v_detail->irs_accepted_date) ? $v_detail->irs_accepted_date : '';?></td>
    <td class="name"><input type="hidden" value="<?=$c_details[0]->FileSequence?>" id="cid" name=""><input type="Checkbox" name="chk[]" class="chk" value="<?=$v_detail->w2_id?>" class="checkbox" ></td>
  </tr>

  <?php }
    }
} else {?>
  <tr>
    <td colspan="7">Nothing to show</td>
  </tr>
  <?php }?>
</tbody>
</table>
</div>
</div>
    </div>
    <div class="col-md-12" align="center">
      <p class="lead">Select the Copy to View and/or Print<br/>
      <input type="radio" class="ssn_print" name="ssn_print" value="1" checked="checked"> Print payee's full SSN (no security)<br/>
      <input type="radio" class="ssn_print" name="ssn_print" value="2"> Mask payee's SSN as XXX-XX-1234 (recommended) <a style="cursor: pointer;" onclick="doc()">More Info.</a> </p>
      <hr>
    </div>
    <div class="col-md-12" align="left">
      <p class="lead">
      <input type="radio" class="copy_print" name="copy_print" checked="checked" value="1"> Copy B for recipient with instructions.<br/>
      <input type="radio" class="copy_print" name="copy_print" value="2"> Copy 2 for recipient's state tax return, when required.<br/>
      <input type="radio" class="copy_print" name="copy_print" value="3"> Copy 1 from payer to state, when required.<br/>
      <input type="radio" class="copy_print" name="copy_print" value="4"> Copy C for payers (your) records.</p>

    </div>
    <hr/>
    <div class="col-md-8 col-md-offset-2">
      <div class="row">
          <div class="col-md-6" align="center">
            <button class="btn btn-primary btn-block view-print" style="color: #ffffff;"><i class="glyphicon glyphicon-download"></i> DOWNLOAD PDF </button>
          </div>
          <div class="col-md-6" align="center">
          <a href="<?=base_url('blaze');?>" class="btn btn-primary btn-block" style="color: #ffffff !important; line-height: 35px !important;"><i class="glyphicon glyphicon-list"></i> MAIN MENU</a>
          </div>
      </div>
    </div>
    <div class="col-md-12" align="center">
      <p class="lead">Note: On the next page, use File/Print Preview before printing.<br/>
      Use File/Page Setup to change Margins for envelope sizing.</p>
      <p class="lead">View our FAQ if you have questions about who gets which copy</p>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    //alert(checkBox);
    var select_all = document.getElementById("printall"); //select all checkbox
    var checkboxes = document.getElementsByClassName("chk"); //checkbox items

    //select all checkboxes
    select_all.addEventListener("change", function(e){
        for (i = 0; i < checkboxes.length; i++) {
            checkboxes[i].checked = select_all.checked;
        }
    });


    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].addEventListener('change', function(e){
            if(this.checked == false){
                select_all.checked = false;
            }

            if(document.querySelectorAll('.chk:checked').length == checkboxes.length){
                select_all.checked = true;
            }
        });
    }

      $('.view-print').click(function(){
      var Cid = $("#cid").val();
      var Pid = [];
      if($(".chk").is(':checked')) {
          $(".chk:checked").each(function() {
            Pid.push($(this).val());
            // $("#cid").val();
          });
        var ssnprint = [];
        $(".ssn_print:checked").each(function() {
           ssnprint.push($(this).val());
        });
        var copyprint = [];
        $(".copy_print:checked").each(function() {
           copyprint.push($(this).val());
        });
        //alert(copyprint);
        var dataNew = {Pid : Pid};
        //alert(Cid); die;
        var baseUrl = '<?php echo base_url('printfile') ?>?Cid='+ Cid +'&Pid='+ Pid +'&ssnprint=' + ssnprint + '&copyprint=' + copyprint;
        window.open(baseUrl,'_blank');
      }
       else {
          alert('You must check at least ONE PERSON to print');
       }
     });
  });
  function doc(){
    myWindow=window.open('<?php echo str_replace("index.php/", "", base_url()); ?>assets/images/help1.html#25','','width=600,height=400,scrollbars=yes');
    myWindow.focus();
  }
</script>