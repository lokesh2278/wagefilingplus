<style>
  .light {
    color: #cccccc;
  }
  .header-box{
    padding: 10px;
    box-shadow: 8px 8px 8px #dddddd;
    border: 1px solid #ccc;
    margin: 20px;
  }
  .dot li {
    list-style: disc;
    font-size: 14px;
  }
  .round-image h4 {
    font-size: 18px !important;
    line-height: 25px !important;
}
/*.round-image p.lead {
    font-size: 14px;
    font-weight: 400;
    line-height: 28px;
}*/
.faq-list li {list-style-type: circle !important;
        margin-left: 20px;}
</style>
<div class="container-fluid">
  <div class="row header-box" style="background-image: url('../assets/images/contactimg.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
        <div class="col-md-12" style="margin-top: 50px;">
          <h3>Contact <span class="light">Us</span></h3>
        </div>
  </div>
  <div class="row">
    <div class="col-md-12" style="padding: 40px;">
      <div class="row round-image" align="center">
        <div class="col-md-3">
          <img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/Bob-Miner-profile-200x200.jpg" class="img img-circle">
          <h4>Bob Miner,<br/> co-Founder</h4>
          <p class="lead"><i class="fa fa-envelope" aria-hidden="true"></i>
            <a href="mailto:bob@wagefiling.com">bob@wagefiling.com</a></p>
        </div>
        <div class="col-md-3">
          <img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/Scott-Zubrickas-profile-200x200.jpg" class="img img-circle">
          <h4>Scott Zubrickas,<br/> CoFounder</h4>
          <p class="lead"><i class="fa fa-envelope" aria-hidden="true"></i>
          <a href="mailto:scott@wagefiling.com">scott@wagefiling.com</a></p>
        </div>
        <div class="col-md-3">
          <img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/stephanie-lopez-profile-200x200.jpg" class="img img-circle">
          <h4 style="font-size: 16px !important;">Stephanie Lopez,<br/>
          Customer Service & Bilingual Support</h4>
          <p class="lead"><i class="fa fa-envelope" aria-hidden="true"></i>
          <a href="mailto:stephanie.lopez@wagefiling.com">stephanie.lopez@wagefiling.com</a></p>
        </div>
        <div class="col-md-3">
          <img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/megan-zubrickas-profile.png" class="img img-circle" width="200px">
          <h4>Megan Zubrickas,<br/> Customer Support</h4>
          <p class="lead"><i class="fa fa-envelope" aria-hidden="true"></i>
          <a href="mailto:support@wagefiling.com">support@wagefiling.com</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid testomonial-block">
  <section class="">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                    <h6 style="font-size: 22px; line-height: 48px; font-weight: 300; text-align: center;">Send us an e-mail and receive a response in minutes!</h6>
                    <hr class="feature-block-hr">
                  </div>
              </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="col-md-2"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/phone-round_318-27998.jpg"></div>
                      <div class="col-md-10">
                        <p class="lead">Call us!<br/>
                        Main: 616-325-9332<br/>
                        Bilingual Support: 361-884-1500</p>
                      </div>
                    </div>
                    <div class="col-md-6" align="center">
                      <div class="col-md-2"><img src="<?php echo str_replace("index.php/", "", base_url()); ?>/assets/images/chat-4-512.png"></div>
                      <div class="col-md-10">
                        <p>Our LiveChat is available 9-9 Eastern Time<br/>
                        <a href="http://www.livehelpnow.net/livechat.asp?c=19561&zzwindow=0&d=0&custom1=&custom2=&custom3=">Click here to chat now!</a></p>
                      </div>
                    </div>
                </div>
            </div>
        </section>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12" style="padding: 40px;">
      <h4>Popular FAQs</h4>
      <p class="lead">We try hard to make filing your W-2/1099-MISC forms as easy as possible and we are here to help. Many of our clients have use this section of our top 5 frequently asked questions to help with filing.</p>
      <div class="panel-group" id="accordion">

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    State Requirements</a>
                  </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p class="lead">
                      Please refer to the map below to see whether or not a specific state requires W-2 & 1099-MISC wage and information reporting.
                    </p>
                    <img src="<?php echo iaBase(); ?>assets/images/filingmap.jpg">
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    Bring Files Foward From a Previous Year (Click to Expand)</a>
                  </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p class="lead">From the <strong>MAIN MENU</strong>, click click on the <strong>File Manager</strong> button. Then check the file to bring forward. Check only one file, then click "Bring Selected File Forward". This process copies the entire file and all payees from the selected file to another new file. The new file has a new File Sequence Number.</p>
                    <p class="lead">If you have the same company and many of the same payees, as last year, you can bring a file forward to tax year . This process saves re-typing the payee names, social security numbers, addresses, etc. After bringing a file forward, just change the money amounts, add few payees if necessary. Void or delete some payees if necessary. This process copies the file you select, gives it a new sequence number, and changes the tax year to , and sets up the file so you can access it from the Main Menu.</p>
                  </div>
                </div>
              </div>
             <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                    Printing Recipient Copies</a>
                  </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul class="faq-list">
                        <li>At the MAIN MENU, click A Paid Print/View A in blue.</li>
                        <li>Click the A View/Print A button (you can select other copies or select specific forms).</li>
                    </ul>
                    <h4>Printing Details:</h4>
                    <p class="lead">Click the BROWSER's FILE Menu, then PRINT PREVIEW. This will show how many pages will be printed, what will be on each page. A You can also print selected pages this way. Then click PRINT in your BROWSER for the hard copies.A </p>
                    <p class="lead"><strong>Note 1:</strong> A Hard copies are designed to fold into standard No. 9 or No. 10 single or double window envelopes. Special envelopes are not required. All copies are IRS compliant 1099 and W-2 substitute forms for the recipients</p>
                    <p class="lead"><strong>Note 2:</strong> A We e-file Copy A to the IRS or SSA soon after you checked out. Your e-filing confirmation will be sent by e-Mail.</p>

                </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                    Confirmation Email and filing timeline</a>
                  </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                  <div class="panel-body">
                      <p class="lead">
                          We will e-File to the IRS or SSA during the regular season, January to April usually within 2 to 12 days after you checkout and pay for them, but they may be filed to the IRS/SSA the same day you checkout.A
                      </p>
                      <p class="lead">
                          The e-Mail confirmation is e-Mailed to you usually within 1 to 2 days after e-Filing to the IRS/SSA, but you may receive the confirmation the same day you checkout. Email is sometimes undependable, due to SPAM filters, etc. It is your responsibility to be certain you receive the confirmation, see below.A
                      </p>
                      <p class="lead">
                          For tax year , the e-Filing deadline is March 31, 1, or the next business day, if that day is a legal holiday. Files submitted after March 26, will be e-Filed as soon as possible, but we can not guarantee they will be e-Filed by the deadline. You must submit any late 1099s and W-2s as soon as possible after the deadline.Â
                      </p>
                      <p class="lead">
                          All forms e-Filed to the IRS/SSA after the deadline may be subject to a late filing penalty by the IRS. The penalty is payable by the "Filer/Company" who submitted the forms. WageFiling.com, your authorized Transmitter/Submitter, is never liable for late filing penalties, or any other penalties, under any circumstances. If you do not receive your confirmation for any reason, it is your responsibility to contact us for an emergency extension of time, or allow us sufficient time to search for, or resubmit your files before the deadline. Thanks you for using WageFiling.com, we appreciate your business.
                      </p>
                  </div>
                </div>
              </div>
             <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                    Filing 1099 Corrections</a>
                  </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse">
                  <div class="panel-body">
                      <p class="lead">
                          Before using corrections:</br>
                          If you are currently entering 1099 information and have NOT checked out, doÂ DO NOT USE THE CORRECTIONÂ checkbox. Instead, use the VOID or DELETE checkbox to remove the form. Then, if necessary, choose ADD Payee to enter the correct form. Do not read or follow this procedure. Deleted or Voided forms are never e-Filed, and you may choose not to print them.
                      </p>
                      <p class="lead">Do not use this corrections procedure if you checked out, then discover you FORGOT to enter, or LEFT OUT some 1099 forms. If a form for somebody was left out, start a new file by adding aÂ New CompanyÂ with exactly the same company information as before, then add the forms you left out, then checkout and print the forms. Do not use the corrections checkbox for forms you forgot to enter.</p>
                      <p class='lead'>Use the CORRECTIONS checkboxÂ ONLYÂ if you have already paid/checked-out, and later discover an error has been made inside one of the forms. In that case, start a New Company with the same company as before, then retype ONLY the FORMS in error. DO NOT pay for and e-file the ENTIRE GROUP again! If you bring a file forward, to avoid re-typing, pay for and e-File only those needing correcting, delete all the others that were correct.</p>
                      <p class="lead"><strong>WARNING:A</strong> You must decide if you have a Type 1 or Type 2 correction before starting. To decide, read the what Type 1 and Type 2 covers.A </p>
                      <p class="lead">Example 1: Wrong money amount, use Type 1</br>
                         Example 2: Wrong Tax ID was entered, use Type 2</br>
                         Example 3: You sent a 1099 to his company, but should have been to him individually, or vice-versa. use Type 2</br>
                         Example 4: Sent a 1099 but should have not done so: use Type 1</p>
                      <p class="lead"><strong>Correction Type 1A</strong> (coversA <strong>incorrect money amount, incorrect address, incorrect payee name or the form should not have been filed</strong>)</p>
                      <ul class="faq-list">
                        <li>A From the home page Login with your e-Mail and password..</li>
                        <li>A From the Main Menu, click NEW FILER Select 1099-Misc.</li>
                        <li>A Enter the Company/Filer information EXACTLY THE SAME as the previously submitted file.</li>
                        <li>A From the Main Menu select that company by clicking on the GREEN UNDERLINED name.</li>
                        <li>A Click ADD A PAYEE.A <strong>CHECK THE CORRECTIONS checkbox</strong>A at the top of the red form.</li>
                        <li>A Enter all information EXACTLY as on the previously submitted form, except:<br>
                        </li>
                      </ul>
                      <p class="lead"><strong>To correct a MONEY amount:</strong>A change the money to the correct amount.</br>
                         <strong>To correct an address:</strong>A change the address.</br>
                         <strong>To correct payee name:</strong>A change the payee name.</br>
                         <strong>A return was filed when one should not have been filed:</strong> A put ZERO in the money amount.</p>
                         <p class="lead">Finally, go to the Main Menu, and checkout to submit and e-File this new file. Print the corrected copy and mail it to the recipient. You are now finished. The correction will be e-Filed to the IRS.</p>
                    <p class="lead">
                        <strong>Correction Type 2</strong>A (coversA incorrect Tax ID, or both name and address were wrong)
                        <ul class="faq-list">
                        <li>From the home page Login with your e-Mail and password.</li>
                        <li>From the Main Menu, click NEW FILER Select 1099-Misc.</li>
                        <li>Enter the Company/Filer information EXACTLY THE SAME as the previously submitted file.</li>
                        <li>From the Main Menu select that company by clicking on the GREEN UNDERLINED name.</li>
                        <li>Click Add a Payee.A <strong>CHECK THE CORRECTIONS checkbox</strong>A at the top of the red form.</li>
                        <li>Enter all informationA <strong>EXACTLY</strong>A as on the previously submitted form,A <strong>including the wrong Tax ID</strong>, etc.</li>
                        <li>Enter ZERO in the Money amount, and Save this payee.</li>
                        <li>Click Add a Payee again. This timeA <strong>DO NOTA </strong>check the corrections checkbox.</li>
                        <li>Enter the correct Tax ID, Name, address, amount, etc. for this second form.</li>
                      </ul>
                    </p>
                    <p class="lead">
                        Finally, go to the Main Menu, checkout to submit and e-File this new file. Print the corrected copies and mail them to the recipient. You may wish to send the recipient a letter explaining that the first 1099 form with the correction box checked and zero in the money amount will cancel the form e-Filed previously with the wrong Tax ID. The second 1099 form, with no correction checked, is the corrected form. You are now finished. The Type 2 correction will be e-Filed to the IRS.
                    </p>
                    <p class="lead">
                        For more information about corrections please see A <a href="http://www.irs.gov/pub/irs-pdf/i1099gi.pdf">1099 General Instructions</a> A Section H on ~ page 6 titled Corrected Returns on A <strong>Paper</strong> A Forms. Although WageFiling.com automatically e-files your forms, the procedures described in Section H use the same IRS rules described above.
                    </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                    Filing W-2 Corrections</a>
                  </h4>
                </div>
                <div id="collapse6" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p class="lead">W-2 corrections are under construction, and not currently supported. However, you may submit W-2 corrections by hand with a ball point pen by ordering forms W-2C and W-3C from WageFiling.com technical support.</p>
                  </div>
                </div>
              </div>
    </div>
  </div>
</div>
</div>