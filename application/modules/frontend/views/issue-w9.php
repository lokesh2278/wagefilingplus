<style>
	.light {
		color: #cccccc;
	}
	.header-box{
		padding: 10px;
		box-shadow: 8px 8px 8px #dddddd;
		border: 1px solid #ccc;
		margin: 20px;
	}
	.dot li {
		list-style: disc;
		font-size: 14px;
	}
	.shadow-content-box {
		padding: 40px; box-shadow: 0px 0px 8px 4px #cccccc;margin: 25px 0px 25px 0px;
	}
	.image-margin {
		margin-top: 100%;
	}
	.icon-tab .fa {
		font-size: 100px !important;
	}
	.icon-tab {
		margin-top: 18% !important;
	}
</style>
<div class="container">
	<!-- <div class="row header-box" style="background-image: url('../assets/images/filecorrections.jpg'); background-repeat: no-repeat; background-size: 100%; height: 165px;">
				<div class="col-md-12" style="margin-top: 30px;">
					<h3>File <span class="light">Corrections</span></h3>
					<h5>Instantly file 1099-MISC corrections even if you did not originally file with us! </h5>
				</div>
	</div> -->
	<div class="row">
		<div class="col-md-12 shadow-content-box">
			<h3>Instructions for issuing W-9 request:</h3>
			<h4>How It Works</h4>
			<p class="lead">Request, Recieve and Report W-9's to anyone, anywhere for $2.95</p>
		</div>
		<div class="row">
			<div class="col-md-12">
				<iframe width="100%" height="500" src="https://www.youtube.com/embed/kPnx-iMqhwE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7">
				<div class="shadow-content-box">
					<h4><strong>1.</strong> Start to finish securely from any computer, tablet or smart device</h4>
					<p>To issue your first W-9, create your free account from any device. The entire process is protected under our 2048-bit SSL. One account will allow you to create as many companies as you like and issue unlimited W-9 requests.</p>
				</div>
			</div>
			<div class="col-md-1">
				<img src="../assets/images/right-arrow.png" class="image-margin" />
			</div>
			<div class="col-md-4">
				<div class="">
					<div class="icon-tab" style="float: left;">
                        <i class="fa fa-desktop" aria-hidden="true"></i>
                        <i class="fa fa-tablet" aria-hidden="true"></i>
                        <i class="fa fa-mobile" aria-hidden="true"></i>
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </div>
				</div>
			</div>
        </div>
        <div class="row">
			<div class="col-md-4">
				<div class="">
					<div class="icon-tab" style="float: right;">
                        <i class="fa fa-mobile" aria-hidden="true"></i>
                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
				</div>
			</div>
			<div class="col-md-1">
				<img src="../assets/images/left-arrow.png" class="image-margin" />
			</div>
			<div class="col-md-7">
				<div class="shadow-content-box">
					<h4><strong>2.</strong> All you need to issue the W-9 is their email address</h4>
					<p>Once your account is created, just enter the Contractors email address, checkout and send. Your Contractor instantly receives the W-9 request along with instruction. See a sample W-9 request</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7">
				<div class="shadow-content-box">
					<h4><strong>3.</strong> Contractor receives secure link to complete via email</h4>
					<p>Contractor enters their data and can e-sign the W-9 with their mouse or fingertip. That form is then instantly saved to your account and you are notified via email that the form has been completed.</p>
				</div>
			</div>
			<div class="col-md-1">
				<img src="../assets/images/right-arrow.png" class="image-margin" />
			</div>
			<div class="col-md-4">
				<div class="">
					<div class="icon-tab" style="float: left;">
			            <i class="fa fa-user" aria-hidden="true"></i>
			            <i class="fa fa-envelope-o" aria-hidden="true"></i>
			            <i class="fa fa-bell" aria-hidden="true"></i>
                    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="">
					<div class="icon-tab" style="float: right;">
                        <i class="fa fa-users" aria-hidden="true"></i>
					</div>
				</div>
			</div>
			<div class="col-md-1">
				<img src="../assets/images/left-arrow.png" class="image-margin" />
			</div>
			<div class="col-md-7">
				<div class="shadow-content-box">
					<h4><strong>4.</strong> All your contractor W-9's saved in one location</h4>
					<p>Within your account you can see all your signed W-9s and any that are still pending. You can also resend pending W-9's at no charge. Your W-9's can be downloaded as a pdf, or exported as a CSV at any time.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7">
				<div class="shadow-content-box">
					<h4><strong>5.</strong> Contractor W-9 moved to 1099-Misc</h4>
					<p>Since issuing a W-9 is the first step in reporting form 1099-MISC, we can move that data into a 1099-MISC for you. At the end of the year just need to add the amount paid and we'll e-file to the IRS, State and email your Contractor their recipient copy.</p>
				</div>
			</div>
			<div class="col-md-1">
				<img src="../assets/images/right-arrow.png" class="image-margin" />
			</div>
			<div class="col-md-4">
				<div class="icon-tab" style="float: left; margin-top: 18% !important;">
                    <img class="ic-wid" src="https://www.w-9s.com//assets/images/w9.png">
                    <img class="ic-wid" style="width: 42px;" src="https://www.w-9s.com//assets/images/if_Arrow_Forward_1063879.png">
                    <img class="ic-wid" src="https://www.w-9s.com//assets/images/1099.png">
                    <img class="ic-wid" src="https://www.w-9s.com//assets/images/adc.gif" width="60px">
                </div>
			</div>
		</div>
	</div>
</div>
