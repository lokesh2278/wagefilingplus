<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<div id="app-container">
  <div id="page" class="page">

    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">
<!-- <a href="#" id="blaze-logos">
  <img id="w9-logo" src="<?php echo iaBase(); ?>assets/images/WageFilingw9-Logo-XSmall.png" alt="WageFiling W-9">
</a> -->
<div class="row">
  <div id="nav-company" class="col-sm-7 col-md-8 col-lg-9">
    <h1><a class="company-link"><span class="company-name">Main Menu</span></a></h1>
  </div>
<!--   <div id="nav-widget" class="col-sm-5 col-md-4 col-lg-3">
    <div>
      <form role="form" id="search-form">
        <label class="control-label sr-only" for="blaze-q">Type to search</label>
        <div class="search-form-group input-group">
          <input placeholder="Type to search" type="search" class="form-control" id="blaze-q" name="q">
          <span class="input-group-addon clear-button">
          <span style="" class="search-search glyphicon glyphicon-search form-control-feedback"></span>
          <span style="display:none" class="search-wait glyphicon glyphicon-refresh form-control-feedback"></span>
          <span style="display:none" class="search-found glyphicon glyphicon-ok form-control-feedback"></span>
          <span style="display:none" class="search-fail glyphicon glyphicon-remove form-control-feedback"></span>
          </span>
        </div>
      </form>
    </div>
  </div> -->

</div>
</div>
</div>
        <div id="alerts-container" class="alerts-container"><div></div></div>
      </div>
    </div>

    <div class="content-container" id="main-view-content"><div><div class="company-index-container"><div><div class="container">
  <style>

  </style>
  <div class="row">

      <div class="buttons-container col-md-3 col-md-push-9">
      <div class="sidebar">

        <div class="btn-group btn-group-shift-lg">
          <button type="button" class="download-csv btn btn-default btn-hot"><i class="glyphicon glyphicon-download-alt"></i> Download All (CSV)</button>
          <button type="button" class="download-pdf btn btn-default btn-hot"><i class="glyphicon glyphicon-download-alt"></i> Download All (PDF)</button>
        </div>
        <div class="btn-group btn-group-shift-lg">
          <a href="<?=base_url('company');?>" class="btn  btn-default btn-hot"><i class="glyphicon glyphicon-plus"></i> Add Company</a>
          <button type="button" class="btn btn-xs do-delete btn-default" style="display: none;"><i class="glyphicon glyphicon-remove md-icon-delete"></i> Cancel </button><button type="button" class="btn btn-xs btn-default start-delete"> <i class="glyphicon glyphicon-trash"></i> Delete</button>
        </div>
        <!--div class="btn-group btn-group-shift-lg">
          <button type="button" class="transfer-from-blaze btn btn-default btn-hot"><i class="glyphicon glyphicon-transfer"></i> Transfer to 1099s/W-2s</button>
        </div>
        <div class="btn-group btn-group-shift-lg">
         <a href="/blaze/b-notices" class="btn btn-default btn-hot"><i class="glyphicon glyphicon-envelope"></i> B-Notices</a>
        </div>
        <div class="btn-group btn-group-shift-lg">
          <div class="btn btn-default btn-large btn-hot">
            Email when Signed?<br><label>Yes <input type="radio" class="notify_signed" name="notify_w9" value="true"></label>
            <label>No  <input type="radio" class="notify_signed" name="notify_w9" value="false" checked="checked"></label>
          </div>
        </div-->
      </div>
    </div>
     <div class="col-md-9 col-md-pull-3">
      <div class="table-responsive">
      <div class="not-found" style="display: none;">No Result Found</div>
      <table name="example-table" class="company-table table table-striped table-hover blazing-money DeletableTable table-bordered">
        <thead>
          <tr>
            <th width="1" class="table-delete-mode" data-tsort="disabled" data-tfilter="disabled">
              <button type="button" class="mark-all-toggle" title="Select all"><span class="mark-box"><i class="material-icons md-18 md-icon-done"></i></span></button></th>
              <th data-tsort="disabled" data-tfilter="disabled"><i class="edit-ok glyphicon glyphicon-pencil" style="display: inline;"></i></th>
            <th width="5" data-tsort-type="string" style="white-space:nowrap" >Filing Company <!-- <button type="button" class="btn btn-xs do-delete btn-classic" style="width: initial;"><i class="material-icons md-18 md-icon-delete"></i><span class="cancel-text"> Cancel </span></button><button type="button" class="btn btn-xs btn-classic start-delete">Delete</button> --></th>
            <th width="1" data-tsort-type="number" >Form Type</th>
            <!-- <th width="2" data-tsort-type="number" style="text-align:right;line-height:18px;padding-bottom:8px;    width: 20%;">Entered Manually</th> -->
            <th width="2" data-tsort-type="number" style="text-align:right">Requested</th>
            <th width="2" data-tsort-type="number" style="text-align:right">Received</th>
            <!--th width="2" style="text-align:right">Bounced</th-->
            <!--th width="2" class="for-super">
              CPA
            </th-->
          </tr>
        </thead>
        <tbody class="companies-container">

     <?php foreach ($companies as $company) {
    ?>
      <tr data-id="<?=$company->c_id?>">
            <td class="mark-box-container table-delete-mode">
              <button class="mark-box" type="button"><i class="material-icons md-18 md-icon-done"></i></button>
            </td>
            <td class="edit-link">
              <?php
if ($company->c_form_type == '1099-Misc') {?>
                  <a class="edit-company-lnk" href="<?=base_url('company');?>/<?=$company->c_id?>"><i class="edit-ok glyphicon glyphicon-pencil" style="display: inline;"></i><i class="edit-error glyphicon glyphicon-warning-sign" style="color: red; display: none;"></i></a>
              <?php } else {?>
                    <a class="edit-company-lnk" href="<?=base_url('emp_setup');?>/<?=$company->c_id?>"><i class="edit-ok glyphicon glyphicon-pencil" style="display: inline;"></i><i class="edit-error glyphicon glyphicon-warning-sign" style="color: red; display: none;"></i></a>
                 <?php }
    ?>
            </td>
            <td class="name">
              <a class="show-company-lnk" name="name" href="<?=base_url('vendor')?>/<?=$company->c_id?>"><?=$company->c_name?></a>
            </td>
            <td style="text-align:right">
              <a class="forms-link label label-default w9-count-label" href="<?=base_url('vendor')?>/<?=$company->c_id?>" style="display: table-row;"><b><?=$company->c_form_type?></b> <b><span class="badge" name="w9_count"><?=w9_count($company->c_id)?></span></b></a>
            </td>
            <!-- <td name="manual_count" style="text-align:right"><?=manual_count($company->c_id)?></td> -->
            <td name="requested_count" style="text-align:right"><?=requested_count($company->c_id)?></td>
            <td name="signed_count" style="text-align:right"><?=signed_count($company->c_id)?></td>
            <!--td name="bounced_count" style="text-align:right">0</td-->
            <!--td class="for-super">
              <a href="javascript:void(0);" class="assign" data-id="<?=$company->c_id;?>"><i class="glyphicon glyphicon-user"></i> Assign</a>
              <?php
$assigned = assigned($company->c_id);
    foreach ($assigned as $key => $assign) {
        ?>
                <div class="assigned"><?=$assign->v_name;?></div>
              <?php }?>

            </td-->
        </tr>
      <?php }?>
    </tbody>
</table>
</div>
    </div>

  </div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Assign CPAs to Comapny</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <label class="checkall"><input class="checkall" type="checkbox"> Select all</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary save">Save Changes</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">

        $(document).ready(function() {

        $('table[name=example-table]').tableFilter({

          //input : "input[type=search]", Default element

          trigger : {

            event   : "keyup",
            //element : "button[name=btn-filtro]"
          },

          //timeout: 80,

          sort : true,

          //caseSensitive : false, Default

          callback : function() { /* Callback após o filtro */

          },

          notFoundElement : ".not-found"
        });

        $('.download-pdf').click(function(){
          $(this).attr('disabled','disabled');
          $('#alerts-container').html('');
           var dataNew = { type :'pdf'};
           var baseUrl = '<?php echo base_url('download') ?>';
           $.ajax({
              url: baseUrl,
              type: "POST",
              data: dataNew,
              success: function(data) {
                var obj = $.parseJSON(data);
                $('.download-pdf').removeAttr('disabled');
                if(obj.status == 'success')
                {
                  var url = '<?=iaBase()?>'+obj.path;
                  var a = document.createElement("a");
                  a.style = "display: none";
                  a.href = url;
                  a.download = 'w9-form.zip';
                  document.body.appendChild(a);
                  a.click();
                  window.URL.revokeObjectURL(url);
                }
              }
            });
        })

  $('.download-csv').click(function(){
  $(this).attr('disabled','disabled');
  $('#alerts-container').html('');
           var dataNew = { type :'csv'};
           var baseUrl = '<?php echo base_url('download') ?>';
           $.ajax({
              url: baseUrl,
              type: "POST",
              data: dataNew,
              success: function(data) {
                $('.download-csv').removeAttr('disabled');
                var obj = $.parseJSON(data);
                if(obj.status == 'success')
                {
                  var url = '<?=iaBase()?>'+obj.path;
                  var a = document.createElement("a");
                  a.style = "display: none";
                  a.href = url;
                  a.download = 'w9-form.zip';
                  document.body.appendChild(a);
                  a.click();
                  window.URL.revokeObjectURL(url);
                }
              }
            });
        })
      });


  $('.assign').click(function(){
    var id = $(this).attr('data-id');
    var dataNew = {id : id};
    var baseUrl = '<?php echo base_url('assigned') ?>';
         $.ajax({
            url: baseUrl,
            type: "POST",
            data: dataNew,
            success: function(data) {
              $('.modal-body').html(data);
              $("#myModal").modal();
            }
          });
  });

$('.start-delete').click(function(){
      $(this).hide();
      $('.do-delete').show();
      $('.table-delete-mode').show();
   });

$('.mark-all-toggle').click(function(){
  if($(this).hasClass( "all-marked" ))
  {
      $(this).removeClass('all-marked');
      $('.do-delete').removeClass('btn-danger delete-active');
      $('.table tbody tr').removeClass('row-marked');
      //$('.do-delete').addClass('btn-classic');
      $('.do-delete').html('<i class="glyphicon glyphicon-remove"></i> Cancel');
  }else{
      $(this).addClass('all-marked');
      $('.do-delete').addClass('btn-danger delete-active');
      $('.table tbody tr').addClass('row-marked');
      //$('.do-delete').removeClass('btn-classic');
      $('.do-delete').html('<i class="glyphicon glyphicon-trash"></i> Delete record');
      $('button.delete-active').click(function(){
      var deleteIds = [];
      $('.row-marked').each(function() {
                 deleteIds.push($(this).attr('data-id'));
                 $(this).remove();
        });
      var dataNew = {deleteIds : deleteIds, table :'vendor'};
      var baseUrl = '<?php echo base_url('delete') ?>';
         $.ajax({
            url: baseUrl,
            type: "POST",
            data: dataNew,
            success: function(data) {

            }
          });
     });
  }

});
 $('button.mark-box').click(function(){
   if($(this).parents('tr').hasClass( "row-marked" ))
   {
      $(this).parents('tr').removeClass('row-marked');
      if($('tr.row-marked').length == 0)
       {
        $('.do-delete').removeClass('btn-danger delete-active');
        $('.mark-all-toggle').removeClass('all-marked');
        $('.do-delete').addClass('btn-default');
        $('.do-delete').html('<i class="glyphicon glyphicon-remove"></i> Cancel');
       }

   }else{
       $(this).parents('tr').addClass('row-marked');
       $('.do-delete').addClass('delete-active');
       //$('.do-delete').removeClass('btn-classic');
       $('.do-delete').html('<i class="glyphicon glyphicon-trash"></i> Delete record');
       $('button.delete-active').click(function(){
       var deleteIds = [];
       $('.row-marked').each(function() {
               deleteIds.push($(this).attr('data-id'));
               $(this).remove();
       });
       var dataNew = {deleteIds : deleteIds, table :'company'};
       var baseUrl = '<?php echo base_url('delete') ?>';
       $.ajax({
          url: baseUrl,
          type: "POST",
          data: dataNew,
          success: function(data) {

          }
        });
   });
   }
 });
$('.do-delete').click(function(){
      $(this).hide();
      $('.start-delete').show();
      $('.table-delete-mode').hide();
   });
</script>
