<style>
  .amt-div {
    margin-top: 14px;
  }
</style>

<div id="app-container">
  <div id="page" class="page">
    <div id="blaze-banner">
      <div class="container">
        <div class="navigation-container">
          <div class="wrap wrap-one-logo">

<div class="row">
  <div id="nav-company" class="col-md-12 col-lg-12">
    <h1><a class="company-link" href="<?=base_url('vendor')?>/<?=$c_details[0]->c_id?>"><span class="company-name">You are now issuing W-9 Request for <?=!empty($c_details[0]->c_name) ? $c_details[0]->c_name : ''?></span></a></h1>
  </div>

</div>
<a class="back-link" href="<?=base_url('blaze');?>" style="display: block;"><i class="glyphicon glyphicon-chevron-left"></i>Main Menu</a>
</div></div>

      </div>
    </div>

    <div class="content-container" id="main-view-content"><div><div class="container">
  <div class="row">

<form class="requestForm" id="requestForm" action="<?php echo base_url() . 'payment' ?>" accept-charset="UTF-8" method="post">
  <div class="col-md-10 col-lg-10 manual-container col-md-offset-1">
    <div>

<div class="panel panel-info">
  <div class="panel-heading">
    <h2 class="panel-title">Card Details </h2>
  </div>
  <div class="panel-body">
  <input type="hidden" value='<?=!empty($post_data) ? serialize($post_data) : ''?>' name="post_data"/>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label class="control-label" >
          <span class="label-inner">Card Number</span>
        </label>
        <div data-editor="">
          <input class="form-control" name="card_number" type="number" value="">
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label class="control-label" >
          <span class="label-inner">Card Type</span>
        </label>
        <div data-editor="">
          <select class="form-control" name="card_type" id="">
            <option value="visa">Visa</option>
            <option value="mastercard">MasterCard</option>
            <option value="americanexpress">AmericanExpress</option>
            <option value="discover">Discover</option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label class="control-label" >
          <span class="label-inner">Expire Date</span>
        </label>
        <div class="row">
          <div class="col-sm-6">
            <select class="form-control" name="expire_date_m" id="">
<?php
for ($i = 1; $i <= 12; $i++) {
    echo '<option value="' . str_pad($i, 2, "0", STR_PAD_LEFT) . '">' . str_pad($i, 2, "0", STR_PAD_LEFT) . '</option>';
}?>
            </select>
          </div>
          <div class="col-sm-6">
            <select class="form-control" name="expire_date_y" id="">
<?php $year = date('Y', strtotime('now')) - 2;
for ($i = $year; $i <= $year + 12; $i++) {
    echo '<option value="' . $i . '">' . $i . '</option>';
}?>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label class="control-label" >
          <span class="label-inner">Card Code</span>
        </label>
        <div data-editor="">
          <input class="form-control" name="card_code" type="number" value="">
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">

      </div>
    </div>
  </div>
  </div>
  <div class="panel-footer">
    <button class="btn btn-info btn-lg">Checkout</button>
    <div  class="amt-div pull-right" >
      <p>
        <strong>Amount :</strong>
        <span><i class="glyphicon glyphicon-usd"></i><?php echo $amount; ?></span>
      </p>
    </div>
  </div>

</div>


</div>
</form>

<!-- <form action="charge.php" method="POST">
    <script
      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
      data-key="pk_test_kr3AYh8FUFgqnajNvwY2phO0" // your publishable keys
      data-image="logo.png" // your company Logo
      data-name="PHPGang.com"
      data-description="Download Script ($15.00)"
      data-amount="1500">
    </script>
  </form> -->
</div>
  </div>
</div>
</div></div>

  </div>
  <div class="modal-container"></div>
</div>


<script>
  $(document).ready(function () {

    $('#requestForm').validate({ // initialize the plugin
        rules: {
            'card_number': {
                required: true,
            },
            'card_code': {
                required: true,
            }

        },
    });

});
</script>