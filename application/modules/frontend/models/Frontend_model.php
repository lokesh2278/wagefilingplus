<?php
class Frontend_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->CustID = isset($this->session->get_userdata()['user_details'][0]->CustID) ? $this->session->get_userdata()['user_details'][0]->CustID : '1';
    }

    /**
     * This function is used authenticate user at login
     */
    public function authUser()
    {

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $this->db->where("is_deleted='0' AND (name='$email' OR UEmail='$email')");
        $result = $this->db->get('users')->result();
        if (!empty($result)) {
            //die($result[0]->validate);
            if (password_verify($password, $result[0]->Upassword)) {
                if ($result[0]->status != 'active') {
                    return 'not_varified';
                }
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete user
     * @param: $id - id of user table
     */
    public function delete($id = '')
    {
        $this->db->where('CustID', $id);
        $this->db->delete('users');
    }

    /**
     * This function is used to load view of reset password and varify user too
     */
    public function mailVerify()
    {
        $ucode = $this->input->get('code');
        $this->db->select('UEmail as e_mail');
        $this->db->from('users');
        $this->db->where('var_key', $ucode);
        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result->e_mail)) {
            return $result->e_mail;
        } else {
            return false;
        }
    }

    public function verifyEmail($var_key, $email)
    {
        //die(md5($var_key));
        $data = array('status' => 'active');
        $this->db->where('UEmail', $email);
        $this->db->where('var_key', md5($var_key));
        return $this->db->update('users', $data);
    }
    /**
     * This function is used Reset password
     */
    public function ResetPpassword()
    {
        $email = $this->input->post('email');
        if ($this->input->post('password_confirmation') == $this->input->post('password')) {
            $npass = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $data['Upassword'] = $npass;
            $data['visible_password'] = $this->input->post('password');
            $data['var_key'] = '';
            return $this->db->update('users', $data, "UEmail = '$email'");
        }
    }

    /**
     * This function is used to select data form table
     */
    public function getDataBy($tableName = '', $value = '', $colum = '', $condition = '')
    {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $this->db->select('*');
        $this->db->where('is_deleted', 0);
        $this->db->from($tableName);
        $query = $this->db->get();

        return $query->result();
    }

    public function getCompanyData($tableName = '', $value = '', $colum = '', $condition = '')
    {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $this->db->select('*');
        $this->db->where('is_deleted', 0);
        $this->db->where('FormType!=', 'W-9s');
        $this->db->where('FormType!=', 'W-2');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * This function is used to select data form table
     */
    public function getData($tableName = '', $value = '', $colum = '', $condition = '')
    {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('CustID', $CustID);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    } 

    public function getDataByYear($tableName = '', $value = '', $colum = '',$value2 = '', $colum2='', $condition = '')
    { 
        //print_r($colum2."+".$value2);die;
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where($colum2, $value2);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }
    public function findId($tableName = '', $value = '', $colum = '', $condition = '')
    {
        
        //$CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('id_exist', 1 );
        $this->db->where('FormType', '1099-Misc');
        $this->db->where($colum, $value);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    public function getStatusData($tableName = '', $value = '', $colum = '', $value2 = '', $colum2 = '', $condition = '')
    {
        // print_r($value2);die;
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('CustID', $CustID);
        $this->db->where($colum2, $value2);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataCustFile($tableName = '', $value = '', $colum = '', $colum2 = '', $condition = '')
    {
       

        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('CustID', $CustID);
        $this->db->where('FileSequence', $colum2);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();

        
        return $query->result();
    }
    public function getDataCustFilewww($tableName = '', $value = '', $colum = '', $colum2 = '', $condition = '')
    {
        //print_r($value); die('kjj');
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('CustID', $CustID);
        $this->db->where('FileSequence', $colum2);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();

        
        return $query->result();
    }
    public function getDataById($tableName = '', $value = '', $colum = '', $condition = '')
    {
        $this->db->select('*');
        $this->db->from($tableName);
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('CustID', $CustID);

        $query = $this->db->get();
        return $query->result();
    }
    public function getPayee($tableName = '', $value = '', $colum = '', $condition = '')
    {
        if ((!empty($value)) && (!empty($colum))) {
            $this->db->where($colum, $value);
        }
        print_r($value);die;
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('CustID', $CustID);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    public function get1099($tableName = '', $value = '', $colum = '', $condition = '')
    {

        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('misc_id', $value);
        $this->db->where('CustID', $CustID);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    public function get1099div($tableName = '', $value = '', $colum = '', $condition = '')
    {

        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        $this->db->where('div_id', $value);
        $this->db->where('CustID', $CustID);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }

    public function getw2($tableName = '', $value = '', $colum = '', $condition = '')
    {

        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $this->db->where('is_deleted', 0);
        //$this->db->where('deletebox', 0);
        $this->db->where('w2_id', $value);
        $this->db->where('CustID', $CustID);
        $this->db->select('*');
        $this->db->from($tableName);
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to check user is alredy exist or not
     */
    public function checkExists($table = '', $colom = '', $colomValue = '', $id = '', $id_CheckCol = '')
    {
        $this->db->where($colom, $colomValue);
        if (!empty($id) && !empty($id_CheckCol)) {
            $this->db->where($id_CheckCol . ' !=', $id);
        }
        $res = $this->db->get($table)->row();
        if (!empty($res)) {return false;} else {return true;}
    }

    /**
     * This function is used to get users detail
     */
    public function getUsers($userID = '')
    {
        $this->db->where('is_deleted', '0');
        if (isset($userID) && $userID != '') {
            $this->db->where('CustID', $userID);
        } else if ($this->session->userdata('user_details')[0]->user_type == 'admin') {
            $this->db->where('user_type', 'admin');
        } else {
            $this->db->where('users.CustID !=', '1');
        }
        $result = $this->db->get('users')->result();
        return $result;
    }

    /**
     * This function is used to get email template
     */
    public function getTemplate($code)
    {
        $this->db->where('code', $code);
        return $this->db->get('ia_email_templates')->row();
    }

    /**
     * This function is used to Insert record in table
     */
    public function insertRow($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function FileStr($table, $data, $cid)
    {
        $this->db->select('FileStr');
        $this->db->where('FileSequence', $cid);
        $d = $this->db->get($table)->row();
        print_r($d);die;
        $d .= 'B|A|';
        $d .= implode("|", $data);
        $this->db->where('FileSequence', $cid);
        $this->db->update('Files', array('FileStr' => $d));
    }

    public function insertMisc($table, $d)
    {
        $this->db->insert('ia_1099_misc', $d);
        return $this->db->insert_id();
    }

    public function insertFiler($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    /**
     * This function is used to Update record in table
     */
    public function updateRow($table, $col, $colVal, $data)
    {
        $this->db->where($col, $colVal);
        $this->db->update($table, $data);
        return true;
    }

    public function deleterow($table, $col, $colVal)
    {
        $this->db->where($col, $colVal);
        $this->db->delete($table);
    }

    public function insertSetup($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

/*      public function get_list_box_data($qr) {
$exe = $this->db->query($qr);
return $exe->result();
}
 */
    public function getQrResult($qr)
    {
        // echo"<pre>"; print_r($qr); die;
        $exe = $this->db->query($qr);
        if (!is_bool($exe)) {
            return $exe->result();
        }
    }

    public function getBarChartData($qr)
    {
        $exe = $this->db->query($qr);
        $res = $exe->result();
        $result = [];
        $i = 1;
        while ($i <= 12) {
            $result[$i] = 0;
            foreach ($res as $key => $value) {
                if ($value->months == $i) {
                    //$result[$i] += $value->mka_sum;
                    $result[$i] += (int) str_replace(',', '', $value->mka_sum);
                }
            }
            $i++;
        }
        return implode(',', $result);
    }

    public function registrationMailVerify()
    {
        $ucode = $this->input->get('code');
        if ($ucode == '') {
            return false;
        }
        $this->db->select('UEmail as e_mail');
        $this->db->from('users');
        $this->db->where('var_key', $ucode);
        $query = $this->db->get();
        $result = $query->row();
        if (!empty($result->e_mail)) {
            $data['var_key'] = '';
            $data['status'] = 'active';
            if (getSetting('admin_approval') == 1) {
                $data['status'] = 'varified';
            }
            $this->db->update('users', $data, "UEmail = '$result->e_mail'");
            return $result->e_mail;
        } else {
            return false;
        }
    }
}
