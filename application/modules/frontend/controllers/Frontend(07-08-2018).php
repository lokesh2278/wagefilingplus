<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Frontend extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Frontend_model');
        $this->lang->load('user', getSetting('language'));
        $this->load->library('encryption');
        $this->load->helper('csv');
        $this->load->helper('string');
        $this->CustID = isset($this->session->userdata('user_details')[0]->CustID) ? $this->session->userdata('user_details')[0]->CustID : '';
    }
    /* Home Page */
    public function index()
    {
        redirect('login');
    }
    /* Signup Page */
    public function signup()
    {
        //die('lokesh');
        if (isset($_SESSION['user_details'])) {
            $CustID = $this->session->userdata('user_details')[0]->CustID;
            $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
            if (count($c) > 0) {
                redirect(base_url('blaze'), 'refresh');
            } else {
                redirect(base_url('company'), 'refresh');
            }
        }
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->addEdit();
        } else {
            $this->load->view('signup');
        }
        $this->load->view("include/front/footer");
    }
/* Login Page */
    public function login()
    {
        if (isset($_SESSION['user_details'])) {
            $CustID = $this->session->userdata('user_details')[0]->CustID;
            $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
            if (count($c) > 0) {
                redirect(base_url('user_home'), 'refresh');
            } else {
                redirect(base_url('user_home'), 'refresh');
            }
        }
        if ($this->input->post()) {
            $return = $this->Frontend_model->authUser();
            if (empty($return)) {
                $art_msg['msg'] = lang('invalid_details');
                $art_msg['type'] = 'warning';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url('login'), 'refresh');
            } else {
                if ($return == 'not_varified') {
                    $art_msg['msg'] = lang('this_account_is_not_verified_please_contact_to_your_admin');
                    $art_msg['type'] = 'danger';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url('login'), 'refresh');
                } else {
                    $this->session->set_userdata('user_details', $return);
                    $this->session->set_userdata('admin_back', '');
                }
                $CustID = $this->session->userdata('user_details')[0]->CustID;
                $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
                if (count($c) > 0) {
                    redirect(base_url('user_home'), 'refresh');
                } else {
                    redirect(base_url('user_home'), 'refresh');
                }
            }
        } else {
            $this->load->view("include/front/header");
            $this->load->view('login');
            $this->load->view("include/front/footer");
        }
    }
/**
 * This function is used to logout user
 * @return Void
 */
    public function logout()
    {
        isFrontLogin();
        $this->session->unset_userdata('user_details');
        $this->session->unset_userdata('admin_back');
        redirect(base_url('login'), 'refresh');
    }
/* Signup Page */
    public function company($id = '')
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->add_filer();
        } else {
            if (!empty($id)) {
                $data['c_details'] = $this->Frontend_model->getData('Files', $id, 'FileSequence');
                if (empty($data['c_details'])) {
                    $this->load->view('404error');
                } else {
                    $this->load->view('company_filer', $data);
                }

            } else {
                $data['c_details'] = array();
                $this->load->view('company', $data);
            }

        }
        $this->load->view("include/front/footer");
    }

    public function employee_setup($id = '')
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->emp_setup();
        } else {
            if (!empty($id)) {
                $data['c_details'] = $this->Frontend_model->getData('Files', $id, 'FileSequence');
                if (empty($data['c_details'])) {
                    $this->load->view('404error');
                } else {
                    $this->load->view('employer_setup', $data);
                }

            } else {
                $data['c_details'] = array();
                $this->load->view('employer_setup', $data);
            }

        }
        $this->load->view("include/front/footer");
    }
    public function company_filer()
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        $this->load->view('company_filer');
        $this->load->view("include/front/footer");
    }

    public function add_filer()
    {
        // die('lokesh');
        $data = $this->input->post();
        if (!empty($data['c_id'])) {
            $d = array(
                "c_ssn_ein" => $data['ssn_ein'],
                "TaxYear" => $data['tax_year'],
                //"c_name" => $data['c_name'],
                "FileName" => $data['c_name'],
                "chDate" => date('Y-m-d'),
                "c_is_foreign" => $data['is_foregin'],
                "c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_file_to_states" => $data['file_to_states'],
                "c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_fax" => $data['fax'],
            );
            $this->Frontend_model->updateRow('Files', 'FileSequence', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            $d = array(
                "c_ssn_ein" => $data['ssn_ein'],
                "TaxYear" => $data['tax_year'],
                "c_name" => $data['c_name'],
                "FileName" => $data['c_name'],
                "c_is_foreign" => $data['is_foregin'],
                "c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_file_to_states" => $data['file_to_states'],
                "c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_fax" => $data['fax'],
                "FormType" => $data['form_type'],
                "is_deleted" => 0,
                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                "eFiled" => 0,
                "status" => 'Waiting-UnPaid',
                "created_at" => date('Y-m-d h:i:s'),
            );
            // print_r($d);die;
            $id = $this->Frontend_model->insertFiler('Files', $d);
            redirect(base_url('blaze'), 'refresh');
        }
    }

    public function emp_setup()
    {
        //die('lokesh');
        $data = $this->input->post();
        //print_r($data);die;
        if (!empty($data['c_id'])) {
            $d = array(
                "TaxYear" => $data['tax_year'],
                "c_you_have" => $data['you_have'],
                //"c_ein_for" => $data['ein_for'],
                "c_employee_ein" => $data['employee_ein'],
                "c_other_ein" => $data['other_ein'],
                "c_est_number" => $data['est_number'],
                "FileName" => $data['company'],
                "chDate" => date('Y-m-d'),
                "c_name" => $data['company'],
                //1 "c_is_foreign" => $data['is_foregin'],
                //"c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_zip_2" => $data['zip_2'],
                //"c_file_to_states" => $data['file_to_states'],
                //"c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_extention" => $data['extention'],
                "c_fax" => $data['fax'],
                //"c_code" => $data['code'],
                "c_foreign_state" => $data['foreign_state'],
                "c_foreign_zip" => $data['foreign_zip'],
                "c_foreign_country_code" => $data['foreign_country_code'],
                "c_state_code" => $data['state_code'],
                "c_state_id_no" => $data['state_id_no'],
                //"third_party_sick_pay" => $data['third_party_sick_pay'],
                //"terminating_business" => $data['terminating_business'],
                "w3_box_13" => $data['w3_box_13'],
                "w3_box_14" => $data['w3_box_14'],
            );
            $d['c_ein_for'] = isset($data['ein_for']) ? $data['ein_for'] : 'NULL';
            $d['c_code'] = isset($data['code']) ? $data['code'] : 'NULL';
            $d['third_party_sick_pay'] = isset($data['third_party_sick_pay']) ? $data['third_party_sick_pay'] : 'NULL';
            $d['terminating_business'] = isset($data['terminating_business']) ? $data['terminating_business'] : 'NULL';
            //print_r($d['c_code']);die;
            $this->Frontend_model->updateRow('Files', 'FileSequence', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            // print_r($data);die;
            $d = array(
                "TaxYear" => $data['tax_year'],
                "c_you_have" => $data['you_have'],
                //"c_ein_for" => $data['ein_for'],
                "c_employee_ein" => $data['employee_ein'],
                "c_other_ein" => $data['other_ein'],
                "c_est_number" => $data['est_number'],
                "FileName" => $data['company'],
                "c_name" => $data['name'],
                //1 "c_is_foreign" => $data['is_foregin'],
                //"c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_zip_2" => $data['zip_2'],
                //"c_file_to_states" => $data['file_to_states'],
                //"c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_extention" => $data['extention'],
                "c_fax" => $data['fax'],
                //"c_code" => $data['code'],
                "c_foreign_state" => $data['foreign_state'],
                "c_foreign_zip" => $data['foreign_zip'],
                "c_foreign_country_code" => $data['foreign_country_code'],
                "c_state_code" => $data['state_code'],
                "c_state_id_no" => $data['state_id_no'],
                //"third_party_sick_pay" => $data['third_party_sick_pay'],
                //"terminating_business" => $data['terminating_business'],
                "w3_box_13" => $data['w3_box_13'],
                "w3_box_14" => $data['w3_box_14'],
                "FormType" => $data['form_type'],
                "is_deleted" => 0,
                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                "eFiled" => 0,
                "status" => 'Waiting-UnPaid',
                "created_at" => date('Y-m-d h:i:s'),
            );
            $d['c_ein_for'] = isset($data['ein_for']) ? $data['ein_for'] : 'NULL';
            $d['c_code'] = isset($data['code']) ? $data['code'] : 'NULL';
            $d['third_party_sick_pay'] = isset($data['third_party_sick_pay']) ? $data['third_party_sick_pay'] : 'NULL';
            $d['terminating_business'] = isset($data['terminating_business']) ? $data['terminating_business'] : 'NULL';

            $id = $this->Frontend_model->insertSetup('Files', $d);
            redirect(base_url('blaze'), 'refresh');
        }
    }

    public function payment()
    {
        $com_details = $this->Frontend_model->getData('Files', $this->input->post('c_id'), 'FileSequence');
        $qr = "SELECT * FROM `users` WHERE `CustID` = '" . $com_details[0]->CustID . "'";
        $user_details = $this->Frontend_model->getQrResult($qr);
        $art_msg['msg'] = 'the request sent successfully';
        $art_msg['type'] = 'success';
        $this->session->set_userdata('alert_msg', $art_msg);
        if (isset($com_details[0]->FormType) && $com_details[0]->FormType == 'W-9s') {
            $this->addEditVendorw9s();
        } else {
            $this->addEditVendor();
        }

    }
// Vendor Page /
    public function vendor($cid = '')
    {
        isFrontLogin();
        $data['cid'] = $cid;
        // print_r($data);die;
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
        //print_r($data['c_details'][0]->FormType);die;
        //$data['u_details'] = $this->Frontend_model->getDataBy('ia_users', $cid, 'CustID');
        if (!empty($data['c_details'])) {
            if ($data['c_details'][0]->FormType == '1099-Misc') {
                $data['v_details'] = $this->Frontend_model->getData('ia_1099_misc', $cid, 'FileSequence');
            } else if ($data['c_details'][0]->FormType == 'W-9s') {
                $data['v_details'] = $this->Frontend_model->getData('ia_vendors', $cid, 'c_id');
            } else {
                $data['v_details'] = $this->Frontend_model->getData('ia_w2', $cid, 'FileSequence');

            }
            // print_r($data);die;
            $this->load->view("include/front/header");
            if ($data['c_details'][0]->FormType == 'W-9s') {
                $this->load->view('vendorw9s', $data);
            } else {
                $this->load->view('vendor', $data);
            }
            $this->load->view("include/front/footer");
        } else {
            $this->load->view("include/front/header");
            $this->load->view('404error');
            $this->load->view("include/front/footer");
        }
    }
/* Company List Page */
    public function blaze()
    {
        isFrontLogin();
        $CustID = $this->CustID;
        $data['companies'] = $this->Frontend_model->getData('Files', $CustID, 'CustID');
        $data['u_details'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
        $this->load->view("include/front/header");
        $this->load->view('blaze', $data);
        $this->load->view("include/front/footer");
    }
/* Assigned List  */
    public function assigned()
    {
        isFrontLogin();
        $data = $this->input->post();
        $CustID = $this->CustID;
        $assigned = $this->Frontend_model->getData('ia_vendors', $data['id'], 'FileSequence');
        $vendors = $this->Frontend_model->getData('ia_vendors');
        $value = '';
        if (!empty($vendors)) {
            $i = 0;
            foreach ($vendors as $key => $vendor) {
                $value .= '<label for="c8_editor_ids-' . $i . '">
    <input type="checkbox" name="editor_ids" value="' . $vendor->v_id . '" id="c8_editor_ids-' . $i . '"> &nbsp&nbsp' . $vendor->v_name . ' &lt;' . $vendor->v_email . '&gt'
                    . '</label> <br/>';
            }
        }
        echo $value;
    }
/* W-9 Fprm Page */
    public function enter($form_id = '')
    {
        if (!empty($form_id)) {
            $data['vendors'] = $this->Frontend_model->getDataBy('ia_vendors', $form_id, 'form_id');
            $data['c_id'] = $form_id;
            $this->load->view('w9-form', $data);
        } else {
            redirect(base_url('login'), 'refresh');
        }
    }
/**
 * This function is used to add and update Company
 * @return Void
 */
    public function addEditCompany()
    {
        die('sharma');
        $data = $this->input->post();
        if (!empty($data['c_id'])) {
            $d = array(
                "c_name" => $data['c_name'],
                "c_tin" => $data['tin'],
                "c_street" => $data['shipping_address'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_email" => $data['email'],
                "c_telephone" => $data['telephone'],
            );
            $this->Frontend_model->updateRow('Files', 'FileSequence', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            $d = array(
                "c_name" => $data['c_name'],
                "c_tin" => $data['tin'],
                "c_street" => $data['shipping_address'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_email" => $data['email'],
                "c_telephone" => $data['telephone'],
                "is_deleted" => 0,
                "CustID" => $this->CustID,
                "created_at" => date('Y-m-d h:i:s'),
            );
            $id = $this->Frontend_model->insertRow('Files', $d);
            redirect(base_url('request') . '/' . $id, 'refresh');
        }
    }

/**
 * This function is used to add and update Vendor
 * @return Void
 */
    public function addEditVendor()
    {
        $data = $this->input->post();
        $c_id = $data['c_id'];
        $setting = settings();
        $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'FileSequence');
        $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
        $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
        $c_telephone = !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '';
        $sub = 'W 9s Request from (' . $c_name . ' W 9s)';
        $i = 0;
        foreach ($data['v_email'] as $key => $value) {
            if (!empty($data['v_email'][$i])) {
                $form_id = md5(uniqid(rand(), true));
                $d = array(
                    "v_name" => $data['v_name'][$i],
                    "v_email" => $data['v_email'][$i],
//"v_account" => $data['account_number'][$i],
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "FileSequence" => $c_id,
                    "CustID" => $this->CustID,
                    "user_id" => $this->CustID,
                    "form_id" => $form_id,
                    "created_at" => date('Y-m-d h:i:s'),
                );
                $email = $data['v_email'][$i];
                $setting['FileName'] = 'wagefiling.com';
                $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $data['v_name'][$i] . ',</p><p>' . $c_name . ' is requesting IRS form 1099-Misc from you. Completing this form helps enter you into their accounting system for faster payment.  Most times, companies will not issue payment until this form is returned. </p><p>' . $c_name . ' has teamed up with <a rel="nofollow" style="text-decoration:none; color:#333">wagefiling.com</a> to help simplify the process for you. Just click on this link and it will take you to our secure site to enter your info, e-sign, save and you\'re all set! </p><p><a href="' . base_url('enterw9s') . '/' . $form_id . '" class="go">Fill out your 1099-Misc/W-2</a></p><p>If you\'re not sure why you are receiving this request please contact<br><br>' . $c_name . '<br><a href="tel:' . $c_telephone . '">' . $c_telephone . '</a><br><br>Thank you<br>wagefiling.com, LLC<br><a href="tel:616.325.9332">616.325.9332</a></p></div></div></div>';
                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
// content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                $this->Frontend_model->insertRow('ia_vendors', $d);
            }
            $i++;
        }
        redirect(base_url('vendorw9s') . '/' . $c_id, 'refresh');
    }
    /**
     * This function is used to add and update users
     * @return Void
     */
    public function addEdit()
    {
        $data = $this->input->post();
        $setting = settings();
        $checkValue = $this->Frontend_model->checkExists('users', 'UEmail', $this->input->post('email'), '', '');
        if ($checkValue) {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $var_key = md5($data['email']);

            $d = array(
                "CustID" => mt_rand(10000, 99999),
                "var_key" => $var_key,
                "name" => $data['full_name'],
                "Upassword" => $password,
                "visible_password" => $data['password'],
                "status" => 'deactive',
                "UEmail" => $data['email'],
                "Ucompany" => $data['company'],
                "Ucontact" => $data['full_name'],
                // "Uaddr => $data['address'],
                // "Utitle" => $data['title'],
                // "Ucity" => $data['city'],
                // "Ustate" => $data['state'],
                // "Uzip" => $data['zip'],
                "Uphone" => $data['phone_number'],
                // "Ufax" => $data['fax'],
                "user_type" => 'user',
                "validate" => 0,
                "is_deleted" => 0,
                "UDate" => date('Y-m-d'),
            );

            $email = $data['email'];
            $setting['FileName'] = 'Account activation';
            $sub = 'Account activation';
            $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $data['full_name'] . ',</p><p>Thank you for ragister as user on wagefiling.com </p><p>For activate your account click on following</p>
                <p><a href="' . base_url() . 'verify/' . $email . '/' . $var_key . '" style="text-decoration:none; color:#333">wagefiling.com</a></p>
                <p>If you\'re not sure why you are receiving this request please contact<br><br></a>
                <br><br>
                Thank you<br>
                wagefiling.com, LLC<br>
                <a href="tel:616.325.9332">616.325.9332</a></p>
                </div>
                </div>
                </div>';
            if ($setting['mail_setting'] == 'php_mailer') {
                $this->load->library("send_mail");
                $emm = $this->send_mail->email($sub, $body, $email, $setting);
            } else {
                // content-type is required when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: WegeFiling.com' . "\r\n";
                $emm = mail($email, $sub, $body, $headers);
            }
            $id = $this->Frontend_model->insertRow('users', $d);
            // $new_data = $this->Frontend_model->getDataBy('ia_users', $id, 'CustID');
            // if (!isset($this->session->userdata('user_details')[0]->CustID)) {
            //     $this->session->set_userdata('user_details', $new_data);
            // }
            $this->session->set_flashdata('item', array('message' => 'Success'));
            redirect(base_url('login'));
        } else {
            $art_msg['msg'] = lang('this_email_already_registered_with_us');
            $art_msg['type'] = 'info';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url('signup'), 'refresh');
        }
    }
    public function verify($email, $var_key)
    {
        if ($this->Frontend_model->verifyEmail($var_key, $email)) {
            $this->session->set_flashdata('active', array('message' => 'Congratulations'));
            redirect(base_url('login'), 'refresh');
        } else {
            echo 'error' . $this->config->item('admin_email');
        }
    }

    public function user_home()
    {
        isFrontLogin();
        $CustID = $this->CustID;
        $data['users'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
        $this->load->view("include/front/header");
        $this->load->view('user_home', $data);
        $this->load->view("include/front/footer");
    }
    public function edituser()
    {
        isFrontLogin();
        $CustID = $this->CustID;
        $data['users'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
        $this->load->view("include/front/header");
        $this->load->view('user_edit', $data);
        $this->load->view("include/front/footer");
    }
    public function edituserdata()
    {
        //die('lokesh');
        isFrontLogin();
        $data = $this->input->post();
        $CustID = $this->CustID;
        //$print_r($data['id']);die;
        $d = array(
            "name" => $data['full_name'],
            "visible_password" => $data['password'],
            "status" => 'active',
            "UEmail" => $data['email'],
            "Ucompany" => $data['company'],
            // "contact" => $data['contact'],
            // "address" => $data['address'],
            // "city" => $data['city'],
            // "title" => $data['title'],
            // "state" => $data['state'],
            // "zip" => $data['zip'],
            "UPhone" => $data['phone_number'],
            // "fax" => $data['fax'],
        );
        if (isset($data['password']) && !empty($data['password'])) {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $d['Upassword'] = $password;
        }
        $this->Frontend_model->updateRow('users', 'CustID', $CustID, $d);
        redirect(base_url('user_home'), 'refresh');
    }
    /* Request Page */
    public function request($cid, $id = '')
    {

        isFrontLogin();
        $id = $this->uri->segment(3);
        //$id = $this->input->post('misc_id');
        //print_r($id);die;
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->Frontend_model->get1099('ia_1099_misc', $id, 'misc_id');

        $this->load->view("include/front/header");
        if (empty($data['c_details'])) {
            $this->load->view('404error');
        } else {
            $this->load->view('request', $data);
        }

        $this->load->view("include/front/footer");
    }

    public function addvendor()
    {

        isFrontLogin();
        $data = $this->input->post();
        if (!empty($data['misc_id'])) {

            // print_r($data['corrected']);die;
            if (isset($data['misc_delete']) && $data['misc_delete']) {
                $this->Frontend_model->deleterow('ia_1099_misc', 'misc_id', $data['misc_id']);
                redirect(base_url('vendor/' . $data['c_id']), 'refresh');

            } else {
                if (!isset($data['foreign_add'])) {
                    $data['foreign_add'] = 0;
                }
                $d = array(

                    "recepient_id_no" => $data['recepient_id_no'],
                    "first_name" => $data['first_name'],
                    //"last_name" => $data['last_name'],
                    "dba" => $data['dba'],
                    //"street" => $data['street'],
                    "city" => $data['city'],
                    "state" => $data['state'],
                    "zipcode" => $data['zipcode'],
                    "address" => $data['address'],
                    "rents" => $data['rents'],
                    "royalties" => $data['royalties'],
                    "other_income" => $data['other_income'],
                    "fed_income_with_held" => $data['fed_income_with_held'],
                    "fishing_boat_proceed" => $data['fishing_boat_proceed'],
                    "mad_helthcare_pmts" => $data['mad_helthcare_pmts'],
                    "non_emp_compansation" => $data['non_emp_compansation'],
                    "pmts_in_lieu" => $data['pmts_in_lieu'],
                    "crop_Insurance_proceeds" => $data['crop_Insurance_proceeds'],
                    "excess_golden_par_pmts" => $data['excess_golden_par_pmts'],
                    "gross_paid_to_an_attomey" => $data['gross_paid_to_an_attomey'],
                    "sec_409a_deferrals" => $data['sec_409a_deferrals'],
                    "sec_409a_Income" => $data['sec_409a_Income'],
                    "state_tax_with_held" => $data['state_tax_with_held'],
                    "payer_state_no" => $data['payer_state_no'],
                    "state_Income" => $data['state_Income'],
                    "account_number" => $data['account_number'],
                    "foreign_add" => $data['foreign_add'],

                );

                $d['void'] = isset($data['void']) ? $data['void'] : 'NULL';
                $d['corrected'] = isset($data['corrected']) ? $data['corrected'] : 'NULL';
                $d['misc_delete'] = isset($data['misc_delete']) ? $data['misc_delete'] : 'NULL';
                $d['consumer_products_for_resale'] = isset($data['consumer_products_for_resale']) ? $data['consumer_products_for_resale'] : 'NULL';

                $this->Frontend_model->updateRow('ia_1099_misc', 'misc_id', $data['misc_id'], $d);
                if (isset($_POST['saveform'])) {
                    redirect(base_url('vendor/' . $data['c_id']), 'refresh');
                } else {
                    redirect(base_url('request/' . $data['c_id']), 'refresh');
                }
            }
        } else {
            //die('lokesh');
            if (!isset($data['foreign_add'])) {
                $data['foreign_add'] = 0;
            }
            $d = array(

                "recepient_id_no" => $data['recepient_id_no'],
                "first_name" => $data['first_name'],
                // "last_name" => $data['last_name'],
                "dba" => $data['dba'],
                //"street" => $data['street'],
                "city" => $data['city'],
                "state" => $data['state'],
                "zipcode" => $data['zipcode'],
                "address" => $data['address'],
                "rents" => $data['rents'],
                "royalties" => $data['royalties'],
                "other_income" => $data['other_income'],
                "fed_income_with_held" => $data['fed_income_with_held'],
                "fishing_boat_proceed" => $data['fishing_boat_proceed'],
                "mad_helthcare_pmts" => $data['mad_helthcare_pmts'],
                "non_emp_compansation" => $data['non_emp_compansation'],
                "pmts_in_lieu" => $data['pmts_in_lieu'],
                "crop_Insurance_proceeds" => $data['crop_Insurance_proceeds'],
                "excess_golden_par_pmts" => $data['excess_golden_par_pmts'],
                "gross_paid_to_an_attomey" => $data['gross_paid_to_an_attomey'],
                "sec_409a_deferrals" => $data['sec_409a_deferrals'],
                "sec_409a_Income" => $data['sec_409a_Income'],
                "state_tax_with_held" => $data['state_tax_with_held'],
                "payer_state_no" => $data['payer_state_no'],
                "state_Income" => $data['state_Income'],
                "account_number" => $data['account_number'],
                "FileSequence" => $data['c_id'],
                "foreign_add" => $data['foreign_add'],
                "CustID" => $this->CustID,
                "export_status" => 'unexpoted',
                "created_at" => date('Y-m-d h:i:s'),
            );
            $d['void'] = isset($data['void']) ? $data['void'] : 'NULL';
            $d['corrected'] = isset($data['corrected']) ? $data['corrected'] : 'NULL';
            $d['misc_delete'] = isset($data['misc_delete']) ? $data['misc_delete'] : 'NULL';
            $d['consumer_products_for_resale'] = isset($data['consumer_products_for_resale']) ? $data['consumer_products_for_resale'] : 'NULL';
            $id = $this->Frontend_model->insertRow('ia_1099_misc', $d);
            if (isset($_POST['saveform'])) {
                redirect(base_url('vendor/' . $data['c_id']), 'refresh');
            } else {
                redirect(base_url('request/' . $data['c_id']), 'refresh');
            }
        }
    }

    public function w2form($cid, $id = '')
    {
        // $data[''] = $this->Frontend_model->getDataBy('ia_users', $use, 'CustID');
        isFrontLogin();
        $id = $this->uri->segment(3);
        //print_r($id);die;
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->Frontend_model->getw2('ia_w2', $id, 'w2_id');
        $this->load->view("include/front/header");
        if (empty($data['c_details'])) {
            $this->load->view('404error');
        } else {
            $this->load->view('w2form', $data);
        }
        $this->load->view("include/front/footer");
    }

    public function addformw2()
    {
        //die('Lokesh');
        isFrontLogin();
        $data = $this->input->post();

        if (!empty($data['w2_id'])) {
            $d = array(
                'SSN' => $data['SSN'],
                // 'calcbox'=>$data['calcbox'],
                // 'voidbox' =>$data['voidbox'],
                // 'deletebox'=>$data['deletebox'],
                'box1' => $data['box1'],
                'box2' => $data['box2'],
                'box3' => $data['box3'],
                'box4' => $data['box4'],
                'box5' => $data['box5'],
                'box6' => $data['box6'],
                'box7' => $data['box7'],
                'box8' => $data['box8'],
                'box9' => $data['box9'],
                'box10' => $data['box10'],
                'box11' => $data['box11'],
                'box12A' => $data['box12A'],
                'box12AM' => $data['box12AM'],
                'box12B' => $data['box12B'],
                'box12Bm' => $data['box12BM'],
                'box12C' => $data['box12C'],
                'box12CM' => $data['box12CM'],
                'box12D' => $data['box12D'],
                'box12DM' => $data['box12DM'],
                'boxA' => $data['boxA'],
                'box12DM' => $data['box12DM'],
                'box12DM' => $data['box12DM'],
                'box15A' => $data['box15A'],
                'box15B' => $data['box15B'],
                'box14A' => $data['box14A'],
                'box14B' => $data['box14B'],
                'box14C' => $data['box14C'],
                'box14D' => $data['box14D'],
                'box16' => $data['box16'],
                'box17' => $data['box17'],
                'box18' => $data['box18'],
                'box19' => $data['box19'],
                'box20' => $data['box20'],
                'box21A' => $data['box21A'],
                'box21B' => $data['box21B'],
                'box22' => $data['box22'],
                'box23' => $data['box23'],
                'box24' => $data['box24'],
                'box25' => $data['box25'],
                'box26' => $data['box26'],
                'FName' => $data['FName'],
                'MName' => $data['MName'],
                'LName' => $data['LName'],
                'Suf' => $data['Suf'],
                'Addr1' => $data['Addr1'],
                'Addr2' => $data['Addr2'],
                'City' => $data['City'],
                'State' => $data['State'],
                'Zip1' => $data['Zip1'],
                'Zip2' => $data['Zip2'],
                'FState' => $data['FState'],
                'FZip' => $data['FZip'],
                'FCode' => $data['FCode'],
                "is_deleted" => 1,
            );
            // $d['calcbox'] = isset($data['calcbox']) ? $data['calcbox'] : 'NULL';
            $d['voidbox'] = isset($data['voidbox']) ? $data['voidbox'] : 'NULL';
            $d['is_deleted'] = isset($data['deletebox']) ? $data['deletebox'] : 'NULL';
            $d['statutory_retirement_thirdparty'] = isset($data['statutory_retirement_thirdparty']) ? $data['statutory_retirement_thirdparty'] : 'NULL';
            $this->Frontend_model->updateRow('ia_w2', 'w2_id', $data['w2_id'], $d);
            if (isset($_POST['save'])) {
                redirect(base_url('vendor/' . $data['c_id']), 'refresh');
            } else {
                redirect(base_url('w2form/' . $data['c_id']), 'refresh');
            }

        } else {

            $d = array(
                'SSN' => $data['SSN'],
                // 'calcbox'=>$data['calcbox'],
                // 'voidbox' =>$data['voidbox'],
                // 'deletebox'=>$data['deletebox'],
                'box1' => $data['box1'],
                'box2' => $data['box2'],
                'box3' => $data['box3'],
                'box4' => $data['box4'],
                'box5' => $data['box5'],
                'box6' => $data['box6'],
                'box7' => $data['box7'],
                'box8' => $data['box8'],
                'box9' => $data['box9'],
                'box10' => $data['box10'],
                'box11' => $data['box11'],
                'box12A' => $data['box12A'],
                'box12AM' => $data['box12AM'],
                'box12B' => $data['box12B'],
                'box12Bm' => $data['box12BM'],
                'box12C' => $data['box12C'],
                'box12CM' => $data['box12CM'],
                'box12D' => $data['box12D'],
                'box12DM' => $data['box12DM'],
                'boxA' => $data['boxA'],
                'box12DM' => $data['box12DM'],
                'box12DM' => $data['box12DM'],
                'box15A' => $data['box15A'],
                'box15B' => $data['box15B'],
                'box14A' => $data['box14A'],
                'box14B' => $data['box14B'],
                'box14C' => $data['box14C'],
                'box14D' => $data['box14D'],
                'box16' => $data['box16'],
                'box17' => $data['box17'],
                'box18' => $data['box18'],
                'box19' => $data['box19'],
                'box20' => $data['box20'],
                'box21A' => $data['box21A'],
                'box21B' => $data['box21B'],
                'box22' => $data['box22'],
                'box23' => $data['box23'],
                'box24' => $data['box24'],
                'box25' => $data['box25'],
                'box26' => $data['box26'],
                'FName' => $data['FName'],
                'MName' => $data['MName'],
                'LName' => $data['LName'],
                'Suf' => $data['Suf'],
                'Addr1' => $data['Addr1'],
                'Addr2' => $data['Addr2'],
                'City' => $data['City'],
                'State' => $data['State'],
                'Zip1' => $data['Zip1'],
                'Zip2' => $data['Zip2'],
                'FState' => $data['FState'],
                'FZip' => $data['FZip'],
                'FCode' => $data['FCode'],
                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                "FileSequence" => $data['c_id'],
                "is_deleted" => 0,
                "created_at" => date('Y-m-d h:i:s'),
            );
            // $d['calcbox'] = isset($data['calcbox']) ? $data['calcbox'] : 'NULL';
            $d['voidbox'] = isset($data['voidbox']) ? $data['voidbox'] : 'NULL';
            $d['is_deleted'] = isset($data['deletebox']) ? $data['deletebox'] : 'NULL';
            $d['statutory_retirement_thirdparty'] = isset($data['statutory_retirement_thirdparty']) ? $data['statutory_retirement_thirdparty'] : 'NULL';
            $this->Frontend_model->insertRow('ia_w2', $d);
            if (isset($_POST['save'])) {
                redirect(base_url('vendor/' . $data['c_id']), 'refresh');
            } else {
                redirect(base_url('w2form/' . $data['c_id']), 'refresh');
            }

        }
    }
    public function doFiles()
    {
        isFrontLogin();
        $data = array();
        $chkArray = explode('_', $this->input->get('ids'));
        //$chkArray = explode(',', $this->input->get('chkArray'));
        $data['filevalue'] = $this->input->get('filevalue');
        //print_r($chkArray);print_r($chkArray1);die;
        foreach ($chkArray as $obj) {
            $data['c_details'][] = $this->Frontend_model->getData('Files', $obj, 'FileSequence');
            $data['v1_details'][] = $this->Frontend_model->getData('ia_1099_misc', $obj, 'FileSequence');
            $data['v2_details'][] = $this->Frontend_model->getData('ia_w2', $obj, 'FileSequence');
        }
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $data['u_details'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
        $this->load->view("include/front/header");
        $this->load->view('dofiles', $data);
        $this->load->view("include/front/footer");
    }
    public function moreInfo()
    {
        $this->load->view('more_information');
    }
/**
 * This function is used to add and update Request Form
 * @return Void
 */
    public function saveData()
    {
        $data = $this->input->post();

        if (isset($data['business_classification'])) {
            $data['business_classification'] = implode('|', $data['business_classification']);
        } else {
            $data['business_classification'] = '';
        }
        $form_id = md5(uniqid(rand(), true));
        $v_id = !empty($data['v_id']) ? $data['v_id'] : '';
        $saveData['v_name'] = !empty($data['name']) ? $data['name'] : '';
        $saveData['business_name'] = !empty($data['business_name']) ? $data['business_name'] : '';
        $saveData['business_classification'] = !empty($data['business_classification']) ? $data['business_classification'] : '';
        $saveData['exempt_payee_code'] = !empty($data['exempt_payee_code']) ? $data['exempt_payee_code'] : '';
        $saveData['exempt_fatca_code'] = !empty($data['exempt_fatca_code']) ? $data['exempt_fatca_code'] : '';
        $saveData['v_account'] = !empty($data['account_number']) ? $data['account_number'] : '';
        $saveData['address'] = !empty($data['address']) ? $data['address'] : '';
        $saveData['state'] = !empty($data['state']) ? $data['state'] : '';
        $saveData['city'] = !empty($data['city']) ? $data['city'] : '';
        $saveData['zip'] = !empty($data['zip']) ? $data['zip'] : '';
        $saveData['foreign_address'] = !empty($data['foreign_address']) ? $data['foreign_address'] : '';
        $saveData['v_email'] = !empty($data['email']) ? $data['email'] : '';
        $saveData['v_ssn'] = !empty($data['v_ssn']) ? $data['v_ssn'] : '';
        $saveData['v_ein'] = !empty($data['v_ein']) ? $data['v_ein'] : '';
        $saveData['other'] = !empty($data['business_other']) ? $data['business_other'] : '';
        $saveData['no_backup_withholding'] = !empty($data['no_backup_withholding']) ? $data['no_backup_withholding'] : '';
        $saveData['signature'] = !empty($data['signature']) ? $data['signature'] : '';
        $pdfName = !empty($data['form_id']) ? $data['form_id'] : $form_id;
        $vendor['vendordata'] = $this->Frontend_model->getDataBy('ia_vendors', $v_id, 'v_id');
        $FileSequence = $vendor['vendordata'][0]->c_id;
        $files['filedata'] = $this->Frontend_model->getDataBy('Files', $vendor['vendordata'][0]->c_id, 'FileSequence');
        if (!empty($v_id)) {
            //echo"<PRE>";print_r($saveData);die('yes');
            $this->Frontend_model->updateRow('ia_vendors', 'v_id', $v_id, $saveData);
            $files['w9id'] = $this->Frontend_model->getDataBy('Files', $vendor['vendordata'][0]->c_id, 'w9_id');
            if (empty($files['w9id'][0])) {
                $d = array(
                    "c_ssn_ein" => $files['filedata'][0]->c_ssn_ein,
                    "c_employee_ein" => $files['filedata'][0]->c_employee_ein,
                    "chDate" => $files['filedata'][0]->chDate,
                    "TaxYear" => date('Y'),
                    "c_name" => $files['filedata'][0]->FileName,
                    "c_tin" => $files['filedata'][0]->c_tin,
                    "FileName" => $files['filedata'][0]->FileName,
                    "c_street" => $files['filedata'][0]->c_street,
                    "c_city" => $files['filedata'][0]->c_city,
                    "c_state" => $files['filedata'][0]->c_state,
                    "c_zip" => $files['filedata'][0]->c_zip,
                    "c_contact" => $files['filedata'][0]->c_contact,
                    "c_title" => $files['filedata'][0]->c_title,
                    "c_email" => $files['filedata'][0]->c_email,
                    "c_telephone" => $files['filedata'][0]->c_telephone,
                    "FormType" => '1099-Misc',
                    "w9_id" => $files['filedata'][0]->FileSequence,
                    "is_deleted" => 0,
                    "CustID" => $this->session->userdata('user_details')[0]->CustID,
                    "eFiled" => 0,
                    "status" => 'Waiting-UnPaid',
                    "created_at" => date('Y-m-d h:i:s'),
                );
                // print_r($d);die;
                $fid = $this->Frontend_model->insertFiler('Files', $d);
            } else {
                $fid = $files['w9id'][0]->FileSequence;
            }
            $dataINS['recepient_id_no'] = isset($row->v_ein) ? $row->v_ein : '';
            $dataINS['void'] = 0;
            $dataINS['corrected'] = 0;
            $dataINS['misc_delete'] = 0;

            $dataINS['first_name'] = !empty($data['name']) ? $data['name'] : '';
            $dataINS['dba'] = !empty($data['business_name']) ? $data['business_name'] : '';
            $dataINS['recepient_id_no'] = !empty($data['v_ssn']) ? $data['v_ssn'] : '';
            $dataINS['address'] = !empty($data['address']) ? $data['address'] : '';
            $dataINS['city'] = !empty($data['city']) ? $data['city'] : '';
            $dataINS['state'] = !empty($data['state']) ? $data['state'] : '';
            $dataINS['zipcode'] = !empty($data['zip']) ? $data['zip'] : '';
            $dataINS['account_number'] = !empty($data['account_number']) ? $data['account_number'] : '';
            $dataINS['FileSequence'] = $fid;
            $dataINS['foreign_add'] = !empty($data['foreign_address']) ? $data['foreign_address'] : '';
            $dataINS['CustID'] = $this->session->userdata('user_details')[0]->CustID;
            $dataINS['export_status'] = 'unexpoted';
            $dataINS['is_deleted'] = 0;
            $dataINS['created_at'] = date('Y-m-d h:i:s');
            //endforeach
            $this->db->insert('ia_1099_misc', $dataINS);

        } else {
            $userId = isset($this->CustID) ? $this->CustID : isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
            $saveData['v_status'] = 'Manual';
            $saveData['c_id'] = !empty($data['c_id']) ? $data['c_id'] : '';
            $saveData['CustID'] = $userId;
            $saveData['user_id'] = $userId;
            $saveData["created_at"] = date('Y-m-d h:i:s');
            $saveData["form_id"] = $form_id;
            //echo"<PRE>";print_r($saveData);die('in');
            $v_id = $this->Frontend_model->insertRow('ia_vendors', $saveData);
        }
        $i_checked = '';
        $c_checked = '';
        $s_checked = '';
        $p_checked = '';
        $t_checked = '';
        $l_checked = '';
        $o_checked = '';
        if (!empty($saveData['business_classification'])) {
            $business_classifications = explode('|', $saveData['business_classification']);
            if (in_array("Individual", $business_classifications)) {
                $i_checked = 'checked';
            }
            if (in_array("C Corporation", $business_classifications)) {
                $c_checked = 'checked';
            }
            if (in_array("S Corporation", $business_classifications)) {
                $s_checked = 'checked';
            }
            if (in_array("Partnership", $business_classifications)) {
                $p_checked = 'checked';
            }
            if (in_array("Trust/estate", $business_classifications)) {
                $t_checked = 'checked';
            }
            if (in_array("LLC-C", $business_classifications) || in_array("LLC-S", $business_classifications) || in_array("LLC-P", $business_classifications)) {
                $l_checked = 'checked';
            }
            if (in_array("Other", $business_classifications)) {
                $o_checked = 'checked';
            }
        }

        $strikeout = empty($saveData['no_backup_withholding']) ? 'style="color:red;text-decoration:line-through;"' : '';
        $html = '<table style="width:100%" id="table" border="1" style="width:100%" style="border:1px solid #000;"><tr><th width="20%" style="text-align: center;"><p>From wagefiling <br/> (Rev. November 2017) Department of the Treasury<br/>Internal Revenue Services<p></th><th style="text-align: center;"> Request for Taxpayer<br/>Identification Number and Certification </th> <th width="20%" style="text-align: center;width:20%" >Give from to the requester. <br/>Do not send to the IRS.</th></tr>';
        $html .= '<tr><td colspan="3">Name (as shown on your income tax return) <br/> <b>' . $saveData["v_name"] . '</b></td></tr>';
        $html .= '<tr><td colspan="3">Business name/disregarded entity name, if different from above <br/> <b>' . $saveData["business_name"] . '</b></td></tr><tr><td colspan="2">Select one appropriate box for federal tax classification: <br/><input type="checkbox" ' . $i_checked . '>Individual/sole proprietor or single member LLC <br> <input type="checkbox" ' . $c_checked . '>C Corporation <br><input type="checkbox" ' . $s_checked . '>S Corporation <br/> <input type="checkbox" ' . $p_checked . '>Partnership <br/> <input type="checkbox" ' . $t_checked . '>Trust/estate <br/><input type="checkbox" ' . $l_checked . '>Limited liability company (C Corporation, S Corporation, Partnership) <br/><input type="checkbox" ' . $o_checked . '>Other (see instructions): ' . $saveData["other"] . ' </td> <td>Exempt payee code (if any)  <br/><b>' . $saveData["exempt_payee_code"] . '</b><br/>Exemption from FATCA reporting code (if any) <br/><b>' . $saveData["exempt_fatca_code"] . '</b></td></tr><tr><td colspan="2">Address (number, street, and apt. or suite no. <br/><b>' . $saveData["address"] . '</b></td><td rowspan="2">Requester`s name and address (optional) <br/>' . $saveData["v_name"] . ' <br/>' . $saveData["address"] . ' <br/>' . $saveData["city"] . '
' . $saveData["state"] . '
' . $saveData["zip"] . '</td></tr>';
        $html .= '<tr><td colspan="2">City or town, state, and ZIP code <br/><b>' . $saveData["city"] . ', ' . $saveData["state"] . ',  ' . $saveData["zip"] . '</b> </td> </tr>
<tr>
<td colspan="3">Email (optional) <br/>
    <b> ' . $saveData["v_email"] . '</b>
</td>
</tr>
<tr>
<th width="10%" >Part 1 </th>
<th colspan="2">Taxpayer Identification Number (TIN) </th>
</tr>
<tr>
<td colspan="2">Enter your TIN in the appropriate box. The TIN provided must match the name given on line 1 to avoid backup withholding. For individuals, this is generally your social security number (SSN). However, for a resident alien, sole proprietor, or disregarded entity, see the Part I instructions. For other entities, it is your employer identification number (EIN). If you do not have a number, see How to get a TIN below. </td>
<td colspan="1" style="padding:5px;">Social Security Number <br> <input type="text" value="' . $saveData["v_ssn"] . '" class="form-control"> <br/> Employer Identification Number <br> <input type="text" value="' . $saveData["v_ein"] . '" class="form-control"> </td></tr>';
        $html .= '<tr>
    <th width="10%" >Part 2 </th><th colspan="2">Certification </th> </tr>
    <tr>
        <td colspan="3">Under penalties of perjury, I certify that: <br/>
            1. The number shown on this form is my correct taxpayer identification number (or I am waiting for a number to be issued to me); and <br/>';
        $html .= '<div ' . $strikeout . '>2. I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal Revenue Service (IRS) that I am subject to backup withholding as a result of a failure to report all interest or dividends, or (c) the IRS has notified me that I am no longer subject to backup withholding; and </div><br/>
            3. I am a U.S. citizen or other U.S. person (defined below); and
            4. The FATCA code(s) entered on this form (if any) indicating that I am exempt from FATCA reporting is correct.<br/><br/>
            You must cross out item 2 above if you have been notified by the IRS that you are currently subject to backup withholding because you have failed to report all interest and dividends on your tax return. For real estate transactions, item 2 does not apply. For mortgage interest paid, acquisition or abandonment of secured property, cancellation of debt, contributions to an individual retirement arrangement (IRA), and generally, payments other than interest and dividends, you are not required to sign the certification, but you must provide your correct TIN. See the instructions.
        <br/></td>
        </tr><tr><th >Sign Here</th><th >Signature of  U.S. Person <br/><img src="' . $saveData["signature"] . '" width="80" height="40"></th><th >Date <b>' . date("Y-m-d") . '</b></th></tr>';
        $html .= '</table>';
        $html .= '<style>table, th {
    border: 1px solid black;
    font-size:11px;
    padding:3px;
    font-family: sans-serif;
    text-align:left;
    }
    td{
    border: 1px solid black;
    padding-left:5px;
    padding-top:5px;
    padding-bottom:5px;
    font-size:11px;
    font-family: sans-serif;
    }
    #table {
    border-collapse: collapse;
    } p{ font-size:12px; }
    input[type="checkbox"] {
    display: inline!important;
    margin-right: 10px;
    }
    b{
    font-size:14px;
    font-weight:500;
    }
    .form-control {
    display: block;
    width: 100%;
    height: 20px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    </style>';
        $this->load->library('pdfgenerator');
        $output = $this->pdfgenerator->generate($html, 'pdfnew');
        file_put_contents("pdf/" . $pdfName . ".pdf", $output);
        $dataOutput['pdfPath'] = 'pdf/' . $pdfName . '.pdf';
        $dataOutput['v_id'] = $v_id;
        echo json_encode($dataOutput);
    }
    public function sendPdf()
    {
        $data = $this->input->post();
        $v_id = !empty($data['v_id']) ? $data['v_id'] : '';
        $saveData['pdf'] = !empty($data['pdf']) ? $data['pdf'] : '';
        if (empty($data['v_status'])) {
            $saveData['v_status'] = 'Signed';
        }
        $this->Frontend_model->updateRow('ia_vendors', 'v_id', $v_id, $saveData);
        $this->approval_mail($v_id);
    }
    public function delete()
    {
        $data = $this->input->post();
        if (!empty($data['deleteIds'])) {
            foreach ($data['deleteIds'] as $key => $delete_id) {
                $saveData['is_deleted'] = 1;
                if ($data['table'] == 'company') {
                    $this->Frontend_model->updateRow('Files', 'FileSequence', $delete_id, $saveData);
                } else if ($data['table'] == 'vendor') {
                    $this->Frontend_model->updateRow('ia_vendors', 'v_id', $delete_id, $saveData);
                }
            }
        }
    }
    public function edit($id = '')
    {
        $post = $this->input->post();
        if (!empty($post['v_id'])) {
            $v_id = $post['v_id'];
            unset($post['v_id']);
            $this->Frontend_model->updateRow('ia_vendors', 'v_id', $v_id, $post);
            redirect(base_url('vendorw9s') . '/' . $post['c_id'], 'refresh');
        } else {
            $this->load->view("include/front/header");
            $data['vendor'] = $this->Frontend_model->getData('ia_vendors', $id, 'v_id');
            if (!empty($data['vendor'])) {
                if ($data['vendor'][0]->v_status == 'Signed') {
                    $datas['vendors'] = $this->Frontend_model->getData('ia_vendors', $id, 'v_id');
                    $this->load->view('signedForm', $datas);
                } else {
                    $this->load->view('vendorEdit', $data);
                }

            } else {
                $this->load->view('404error');
            }

            $this->load->view("include/front/footer");
        }
    }

    public function editw9s($id = '')
    {
        $post = $this->input->post();
        if (!empty($post['v_id'])) {
            $v_id = $post['v_id'];
            unset($post['v_id']);
            $this->Frontend_model->updateRow('ia_vendors', 'v_id', $v_id, $post);
            redirect(base_url('vendorw9s') . '/' . $post['c_id'], 'refresh');
        } else {
            $this->load->view("include/front/header");
            $data['vendor'] = $this->Frontend_model->getData('ia_vendors', $id, 'v_id');
            if (!empty($data['vendor'])) {
                if ($data['vendor'][0]->v_status == 'Signed') {
                    $datas['vendors'] = $this->Frontend_model->getData('ia_vendors', $id, 'v_id');
                    $this->load->view('signedFormw9s', $datas);
                } else {
                    $this->load->view('vendorEditw9s', $data);
                }

            } else {
                $this->load->view('404error');
            }

            $this->load->view("include/front/footer");
        }
    }
    public function resendemailw9s()
    {
        $post = $this->input->post();
        $c_id = $post['c_id'];
        $setting = settings();

        $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'FileSequence');
        $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
        $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
        $c_telephone = !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '';
        $sub = 'W-9 Request from (' . $c_name . ' W-9)';
        if (!empty($post['v_email'])) {
            $form_id = md5(uniqid(rand(), true));
            if (!empty($post['request']) && $post['request'] == 'update') {
                $d = array(
                    "v_email" => $post['v_email'],
                    "v_name" => $post['v_name'],
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "c_id" => $c_id,
                    "user_id" => $this->session->userdata('user_details')[0]->CustID,
                    "CustID" => $this->session->userdata('user_details')[0]->CustID,
                    "form_id" => $form_id,
                    "created_at" => date('Y-m-d h:i:s'),
                );
            } else {
                $d = array(
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "c_id" => $c_id,
                    "user_id" => $this->session->userdata('user_details')[0]->CustID,
                    "CustID" => $this->session->userdata('user_details')[0]->CustID,
                    "form_id" => $form_id,
                    "pdf" => '',
                    "created_at" => date('Y-m-d h:i:s'),
                );
            }
            $email = $post['v_email'];
            $setting['company_name'] = 'W-9s.com';
            $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $post['v_name'] . ',</p><p>' . $c_name . ' is requesting IRS form W-9 from you. Completing this form helps enter you into their accounting system for faster payment.  Most times, companies will not issue payment until this form is returned. </p><p>' . $c_name . ' has teamed up with <a rel="nofollow" style="text-decoration:none; color:#333">W-9\'s.com</a> to help simplify the process for you. Just click on this link and it will take you to our secure site to enter your info, e-sign, save and you\'re all set! </p><p><a href="' . base_url('enterw9s') . '/' . $form_id . '" class="go">Fill out your W-9</a></p><p>If you\'re not sure why you are receiving this request please contact<br><br>' . $c_name . '<br><a href="tel:' . $c_telephone . '">' . $c_telephone . '</a><br><br>Thank you<br>W-9s.com, LLC<br><a href="tel:616.325.9332">616.325.9332</a></p></div></div></div>';
            if ($setting['mail_setting'] == 'php_mailer') {
                $this->load->library("send_mail");
                $emm = $this->send_mail->email($sub, $body, $email, $setting);
            } else {
                // content-type is required when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                $emm = mail($email, $sub, $body, $headers);
            }
            if (!empty($post['request']) && $post['request'] == 'update') {
                return $this->Frontend_model->insertRow('ia_vendors', $d);
            } else {
                $this->Frontend_model->updateRow('ia_vendors', 'v_id', $post['v_id'], $d);
            }
        }
    }
    public function resendemail()
    {
        $post = $this->input->post();
        $c_id = $post['c_id'];
        $setting = settings();
        $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'FileSequence');
        $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
        $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
        $c_telephone = !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '';
        $sub = '1099_Misc/W-2 Request from (' . $c_name . ' )';
        if (!empty($post['v_email'])) {
            $form_id = md5(uniqid(rand(), true));
            if (!empty($post['request']) && $post['request'] == 'update') {
                $d = array(
                    "v_email" => $post['v_email'],
                    "v_name" => $post['v_name'],
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "FileSequence" => $c_id,
                    "CustID" => $this->CustID,
                    "form_id" => $form_id,
                    "created_at" => date('Y-m-d h:i:s'),
                );
            } else {
                $d = array(
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "FileSequence" => $c_id,
                    "CustID" => $this->CustID,
                    "form_id" => $form_id,
                    "pdf" => '',
                    "created_at" => date('Y-m-d h:i:s'),
                );
            }
            $email = $post['v_email'];
            $setting['FileName'] = 'wagefiling.com';
            $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $post['v_name'] . ',</p><p>' . $c_name . ' is requesting IRS form 1099-Misc/W-2 from you. Completing this form helps enter you into their accounting system for faster payment.  Most times, companies will not issue payment until this form is returned. </p><p>' . $c_name . ' has teamed up with <a rel="nofollow" style="text-decoration:none; color:#333">wagefiling.com</a> to help simplify the process for you. Just click on this link and it will take you to our secure site to enter your info, e-sign, save and you\'re all set! </p><p><a href="' . base_url('enter') . '/' . $form_id . '" class="go">Fill out your wagefiling</a></p><p>If you\'re not sure why you are receiving this request please contact<br><br>' . $c_name . '<br><a href="tel:' . $c_telephone . '">' . $c_telephone . '</a><br><br>Thank you<br>wagefiling.com, LLC<br><a href="tel:616.325.9332">616.325.9332</a></p></div></div></div>';
            if ($setting['mail_setting'] == 'php_mailer') {
                $this->load->library("send_mail");
                $emm = $this->send_mail->email($sub, $body, $email, $setting);
            } else {
                // content-type is required when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                $emm = mail($email, $sub, $body, $headers);
            }
            if (!empty($post['request']) && $post['request'] == 'update') {
                return $this->Frontend_model->insertRow('ia_vendors', $d);
            } else {
                $this->Frontend_model->updateRow('ia_vendors', 'v_id', $post['v_id'], $d);
            }
        }
    }

    public function requestupdate()
    {
        $id = $this->resendemailw9s();
        if ($id > 0) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }
    public function importCsv()
    {
        $this->load->view("include/front/header");
        $this->load->view('importcsv');
        $this->load->view("include/front/footer");
    }
    public function importdata()
    {
        $content = file_get_contents('php://input');
        $lines = explode("\n", $content);
        $row = 1;
        if (count($lines) > 1) {
            foreach ($lines as $line) {
                $csv_row = str_getcsv($line);
                $num = count($csv_row);
                if ($num == 8) {
                    if ($row == 1) {
                        if (strtolower(str_replace(' ', '', $csv_row[0])) != 'companyname') {
                            echo json_encode(array('status' => 'fail', 'message' => 'This page is for importing companies, but it looks like you`re using a different CSV template.'));
                            return false;
                        }
                    } else {
                        $d = array(
                            "c_name" => !empty($csv_row[0]) ? $csv_row[0] : '',
                            "c_tin" => !empty($csv_row[1]) ? $csv_row[1] : '',
                            "c_street" => !empty($csv_row[2]) ? $csv_row[2] : '',
                            "c_city" => !empty($csv_row[3]) ? $csv_row[3] : '',
                            "c_state" => !empty($csv_row[4]) ? $csv_row[4] : '',
                            "c_zip" => !empty($csv_row[5]) ? $csv_row[5] : '',
                            "c_email" => !empty($csv_row[6]) ? $csv_row[6] : '',
                            "c_telephone" => !empty($csv_row[7]) ? $csv_row[7] : '',
                            "is_deleted" => 0,
                            "CustID" => $this->session->userdata('user_details')[0]->CustID,
                            "created_at" => date('Y-m-d h:i:s'),
                        );
                        $this->Frontend_model->insertRow('Files', $d);
                    }
                    $row++;
                } else {
                    echo json_encode(array('status' => 'fail', 'message' => 'This page is for importing companies, but it looks like you`re using a different CSV template.'));
                    return false;
                }
            }
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'fail', 'message' => 'No companies found to import'));
            return false;
        }
    }
    public function importvendordata()
    {
        $post = $this->input->post();
        $lines = explode("\n", $post['contents']);
        $row = 1;
        if (count($lines) > 1) {
            foreach ($lines as $line) {
                $csv_row = str_getcsv($line);
                $num = count($csv_row);
                if ($num == 3) {
                    if ($row == 1) {
                        if (strtolower(str_replace(' ', '', $csv_row[0])) != 'vendorname') {
                            echo json_encode(array('status' => 'fail', 'message' => 'You`re using a different CSV template.'));
                            return false;
                        }
                    } else {
                        $v_name = !empty($csv_row[0]) ? $csv_row[0] : '';
                        $v_email = !empty($csv_row[1]) ? $csv_row[1] : '';
                        if (!empty($v_name) && !empty($v_email)) {
                            $c_id = $post['c_id'];
                            $setting = settings();
                            $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'FileSequence');
                            $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
                            $c_telephone = !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '';
                            $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
                            $sub = 'wagefiling Request from (' . $c_name . ' 1099-Misc/W-2)';
                            $form_id = md5(uniqid(rand(), true));
                            $d = array(
                                "v_name" => $v_name,
                                "v_email" => $v_email,
                                "v_status" => 'Requested',
                                "is_deleted" => 0,
                                "FileSequence" => $post['c_id'],
                                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                                "form_id" => $form_id,
                                "created_at" => date('Y-m-d h:i:s'),
                            );
                            $email = $v_email;
                            $setting['FileName'] = 'wagefiling.com';
                            $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $v_name . ',</p><p>' . $c_name . ' is requesting IRS form 1099-Misc/W-2 from you. Completing this form helps enter you into their accounting system for faster payment.  Most times, companies will not issue payment until this form is returned. </p><p>' . $c_name . ' has teamed up with <a rel="nofollow" style="text-decoration:none; color:#333">wagefiling.com</a> to help simplify the process for you. Just click on this link and it will take you to our secure site to enter your info, e-sign, save and you\'re all set! </p><p><a href="' . base_url('enter') . '/' . $form_id . '" class="go">Fill out your 1099-Misc/W2</a></p><p>If you\'re not sure why you are receiving this request please contact<br><br>' . $c_name . '<br><a href="tel:' . $c_telephone . '">' . $c_telephone . '</a><br><br>Thank you<br>wagefiling.com, LLC<br><a href="tel:616.325.9332">616.325.9332</a></p></div></div></div>';
                            if ($setting['mail_setting'] == 'php_mailer') {
                                $this->load->library("send_mail");
                                $emm = $this->send_mail->email($sub, $body, $email, $setting);
                            } else {
                                // content-type is required when sending HTML email
                                $headers = "MIME-Version: 1.0" . "\r\n";
                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                                $emm = mail($email, $sub, $body, $headers);
                            }
                            $this->Frontend_model->insertRow('ia_vendors', $d);
                        }
                    }
                    $row++;
                } else {
                    echo json_encode(array('status' => 'fail', 'message' => 'You`re using a different CSV template.'));
                    return false;
                }
            }
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'fail', 'message' => 'No companies found to import'));
            return false;
        }
    }
    public function download()
    {
        $this->load->library('zip');
        $post = $this->input->post();
        $data = array();
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $Files = $this->Frontend_model->getData('Files', $CustID, 'CustID');
        $this->zip->clear_data();
        if ($post['type'] == 'pdf') {
            $zip = 0;
            foreach ($Files as $key => $ia_company) {
                $vendors = $this->Frontend_model->getData('ia_vendors', $ia_company->FileSequence, 'FileSequence');
                foreach ($vendors as $key => $vendor) {
                    if (!empty($vendor->pdf) && $vendor->v_status == 'Signed') {
                        copy("./pdf/$vendor->form_id.pdf", "./pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                        $zip = $this->zip->read_file(FCPATH . "/pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                        unlink(FCPATH . "/pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                    }
                }
            }
            if ($zip > 0) {
                $this->zip->archive(FCPATH . 'pdf/w9-form.zip');
                $data['status'] = 'success';
                $data['path'] = 'pdf/w9-form.zip';
            } else {
                $data['status'] = 'fail';
            }
        }
        if ($post['type'] == 'csv') {
            $zip = 0;
            foreach ($Files as $key => $ia_company) {
                $this->db->select('v_name AS `Name`, business_classification AS `Tax Classification`, business_name AS `Business Name`, v_ssn AS `SSN TAX ID No`, v_ein AS `EIN TAX ID No`, address AS `Street`, city AS `City`, state AS `State`, zip AS `Zip Code`, v_email AS `Email`, foreign_address AS `Foreign City, Province/State, Postal Code, Country`,  DATE_FORMAT(created_at, "%Y%m%d") AS `Date signed (YYYYMMDD)`', false);
                $this->db->where('FileSequence', $ia_company->FileSequence);
                $this->db->where('is_deleted', 0);
                $quer = $this->db->get('ia_vendors');
                if (count($quer->result()) > 0) {
                    query_to_csv($quer, true, FCPATH . "/assets/csv/all-W-9s-$ia_company->FileSequence-$ia_company->c_name.csv");
                    $zip = $this->zip->read_file(FCPATH . "/assets/csv/all-W-9s-$ia_company->FileSequence-$ia_company->c_name.csv");
                    unlink(FCPATH . "/assets/csv/all-W-9s-$ia_company->FileSequence-$ia_company->c_name.csv");
                    if ($zip > 0) {
                        $this->zip->archive(FCPATH . '/assets/csv/w9-form.zip');
                        $data['status'] = 'success';
                        $data['path'] = '/assets/csv/w9-form.zip';
                    } else {
                        $data['status'] = 'fail';
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function downloadPdfs()
    {
        $this->load->library('zip');
        $post = $this->input->post();
        $data = array();
        $this->zip->clear_data();
        if (isset($post['files']) && !empty($post['files'])) {
            $zip = 0;
            foreach ($post['files'] as $key => $vendor) {
                if (!empty($post['files'][$key])) {
                    $lastPDFName = isset($post['files'][$key]) ? explode("/pdf/", $post['files'][$key])[1] : '';
                    copy("./pdf/$lastPDFName", "./pdf/w9-form/all-W-9s-$lastPDFName");
                    $zip = $this->zip->read_file(FCPATH . "/pdf/w9-form/all-W-9s-$lastPDFName");
                    unlink(FCPATH . "/pdf/w9-form/all-W-9s-$lastPDFName");
                }
            }

            if ($zip > 0) {
                //  $this->zip->archive(FCPATH . 'pdf/w9-form.zip');
                $data['status'] = 'success';
                $data['path'] = 'pdf/w9-form.zip';
                $this->zip->download('w9-form.zip');
                unlink('w9-form.zip');
            } else {
                $data['status'] = 'fail';
            }
        }
        echo json_encode($data);
        die;
    }
    public function vendordownload()
    {

        $post = $this->input->post();
        $this->load->library('zip');
        $data = array();
        $ia_company = $this->Frontend_model->getData('Files', $post['c_id'], 'FileSequence');
        $c_id = $ia_company[0]->FileSequence;
        $c_name = $ia_company[0]->c_name;
        if ($post['type'] == 'csv') {
            $this->db->select('v_name AS `Name`, business_classification AS `Tax Classification`, business_name AS `Business Name`,  address AS `Street`, city AS `City`, state AS `State`, zip AS `Zip Code`, v_email AS `Email`, v_account AS `Account Number`,  foreign_address AS `Foreign City, Province/State, Postal Code, Country`,  DATE_FORMAT(created_at, "%Y%m%d") AS `Date signed (YYYYMMDD)`', false);
            $this->db->where('c_id', $post['c_id']);
            $this->db->where('is_deleted', 0);
            $quer = $this->db->get('ia_vendors');
            if (count($quer->result()) > 0) {
                query_to_csv($quer, true, FCPATH . "/assets/csv/company/W-9s-$c_id-$c_name.csv");
            }
            $data['status'] = 'success';
            $data['path'] = "/assets/csv/company/W-9s-$c_id-$c_name.csv";
            $data['name'] = "W-9s-$c_id-$c_name.csv";
        } else if ($post['type'] == 'spdf') {
            $this->zip->clear_data();
            $zip = 0;
            $vendors = $this->Frontend_model->getData('ia_vendors', $c_id, 'c_id');
            foreach ($vendors as $key => $vendor) {
                if (!empty($vendor->pdf) && $vendor->v_status == 'Signed') {
                    copy("./pdf/$vendor->form_id.pdf", "./pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                    $zip = $this->zip->read_file(FCPATH . "/pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                    unlink(FCPATH . "/pdf/w9-form/all-W-9s-$vendor->v_id-$vendor->v_name.pdf");
                }
            }
            if ($zip > 0) {
                $this->zip->archive(FCPATH . "pdf/$c_name.zip");
                $data['status'] = 'success';
                $data['path'] = "pdf/$c_name.zip";
                $data['name'] = "$c_name.zip";
            } else {
                $data['status'] = 'fail';
            }
        } else if ($post['type'] == 'resend') {
            $c_id = $post['c_id'];
            $setting = settings();
            $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'FileSequence');
            $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
            $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
            $c_telephone = !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '';
            $sub = 'W-9 Request from (' . $c_name . ' W-9)';
            $vendors = $this->Frontend_model->getData('ia_vendors', $c_id, 'c_id');
            foreach ($vendors as $key => $vendor) {
                if ($vendor->v_status == 'Requested') {
                    if (!empty($vendor->v_email)) {
                        $form_id = md5(uniqid(rand(), true));
                        $d = array(
                            "v_status" => 'Requested',
                            "is_deleted" => 0,
                            "c_id" => $c_id,
                            "user_id" => $this->session->userdata('user_details')[0]->CustID,
                            "CustID" => $this->session->userdata('user_details')[0]->CustID,
                            "form_id" => $form_id,
                            "pdf" => '',
                            "created_at" => date('Y-m-d h:i:s'),
                        );
                        $email = $vendor->v_email;
                        $setting['company_name'] = 'W-9s.com';
                        $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $vendor->v_name . ',</p><p>' . $c_name . ' is requesting IRS form W-9 from you. Completing this form helps enter you into their accounting system for faster payment.  Most times, companies will not issue payment until this form is returned. </p><p>' . $c_name . ' has teamed up with <a rel="nofollow" style="text-decoration:none; color:#333">W-9\'s.com</a> to help simplify the process for you. Just click on this link and it will take you to our secure site to enter your info, e-sign, save and you\'re all set! </p><p><a href="' . base_url('enterw9s') . '/' . $form_id . '" class="go">Fill out your W-9</a></p><p>If you\'re not sure why you are receiving this request please contact<br><br>' . $c_name . '<br><a href="tel:' . $c_telephone . '">' . $c_telephone . '</a><br><br>Thank you<br>W-9s.com, LLC<br><a href="tel:616.325.9332">616.325.9332</a></p></div></div></div>';
                        if ($setting['mail_setting'] == 'php_mailer') {
                            $this->load->library("send_mail");
                            $emm = $this->send_mail->email($sub, $body, $email, $setting);
                        } else {
                            // content-type is required when sending HTML email
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                            $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                            $emm = mail($email, $sub, $body, $headers);
                        }
                        $this->Frontend_model->updateRow('ia_vendors', 'v_id', $vendor->v_id, $d);
                    }
                }
            }
            $data['status'] = 'success';
        }
        echo json_encode($data);

    }
    public function forgotpassword()
    {
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $setting = settings();
            $res = $this->Frontend_model->getDataBy('users', $this->input->post('email'), 'UEmail', 1);
            if (isset($res[0]->CustID) && $res[0]->CustID != '') {
                $var_key = $this->getVerificationCode();
                $this->Frontend_model->updateRow('users', 'CustID', $res[0]->CustID, array('var_key' => $var_key));
                $sub = "Reset password";
                $email = $this->input->post('email');
                $data = array(
                    'user_name' => $res[0]->name,
                    'action_url' => base_url(),
                    'sender_name' => $setting['FileName'],
                    'website_name' => $setting['website'],
                    'varification_link' => base_url() . 'frontend/mailVerify?code=' . $var_key,
                    'url_link' => base_url() . 'frontend/mailVerify?code=' . $var_key,
                );
                $body = $this->Frontend_model->getTemplate('forgot_password');
                $body = $body->html;
                foreach ($data as $key => $value) {
                    $body = str_replace('{var_' . $key . '}', $value, $body);
                }
                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
                    // content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                if ($emm) {
                    $this->load->view('forgot-password', array('show' => 'msg'));
                }
            } else {
                $this->load->view('forgot-password', array('show' => 'msg'));
            }
        } else {
            $this->load->view('forgot-password');
        }
        $this->load->view("include/front/footer");
    }
    /**
     * This function is used to load view of reset password and varify user too
     * @return : void
     */
    public function mailVerify()
    {
        $return = $this->Frontend_model->mailVerify();
        if ($return) {
            $data['email'] = $return;
            $this->load->view('set-password', $data);
        } else {
            $data['email'] = 'allredyUsed';
            $this->load->view('set-password', $data);
        }
    }
    /**
     * This function is generate hash code for random string
     * @return string
     */
    public function getVerificationCode()
    {
        $pw = $this->randomString();
        return $varificat_key = password_hash($pw, PASSWORD_DEFAULT);
    }
    /**
     * This function is used to Generate a random string
     * @return String
     */
    public function randomString()
    {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = $alpha . $alpha_upper . $numeric;
        $pw = '';
        $chars = str_shuffle($chars);
        $pw = substr($chars, 8, 8);
        return $pw;
    }
    /**
     * This function is used to reset password in forget password process
     * @return : void
     */
    public function reset_password()
    {
        $return = $this->Frontend_model->ResetPpassword();
        if ($return) {
            redirect(base_url() . 'login', 'refresh');
        } else {
            redirect(base_url() . 'login', 'refresh');
        }
    }
    public function approval_mail($v_id = '')
    {
        $setting = settings();
        if ($v_id) {
            $v_details = $this->Frontend_model->getDataBy('ia_vendors', $v_id, 'v_id');
            $c_details = $this->Frontend_model->getDataBy('Files', $v_details[0]->c_id, 'FileSequence');
        }
        $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
        $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
        $c_telephone = !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '';
        $sub = $c_name . ' form is approved';
        $setting['company_name'] = 'W-9s.com';
        $email = $c_email;
        $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $c_name . ',</p><p>' . isset($v_details[0]->v_name) ? $v_details[0]->v_name : '' . ' is approved <a href="' . base_url() . '">W-9\'s</a> Request from (' . $c_name . ' W-9)</p></div></div></div>';
        if ($setting['mail_setting'] == 'php_mailer') {
            $this->load->library("send_mail");
            $emm = $this->send_mail->email($sub, $body, $email, $setting);
        } else {
            // content-type is required when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
            $emm = mail($email, $sub, $body, $headers);
        }
    }

    public function terms()
    {
        $this->load->view("include/front/header_new");
        $this->load->view('terms');
        $this->load->view("include/front/footer_new");
    }

    public function userLogin($CustID = '')
    {

        $admin_back = $this->session->userdata('user_details')[0]->CustID;

        $return = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');

        $this->session->unset_userdata('user_details');

        $this->session->set_userdata('user_details', $return);

        $this->session->set_userdata('admin_back', $admin_back);

        $CustID = $this->session->userdata('user_details')[0]->CustID;

        $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');

        if (count($c) > 0) {
            redirect(base_url('blaze'), 'refresh');
        } else {
            redirect(base_url('company'), 'refresh');
        }
    }

    public function adminBack($CustID = '')
    {
        $return = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');

        $this->session->unset_userdata('user_details');

        $this->session->unset_userdata('admin_back');

        $this->session->set_userdata('user_details', $return);

        redirect(base_url() . 'user/userlist', 'refresh');

    }

    public function contactUs()
    {
        $this->load->view("include/front/header");
        $this->load->view('contactUs');
        $this->load->view("include/front/footer");
    }

    public function services()
    {
        $this->load->view("include/front/header");
        $this->load->view('services');
        $this->load->view("include/front/footer");
    }

    public function howitworks()
    {
        $this->load->view("include/front/header");
        $this->load->view('howitworks');
        $this->load->view("include/front/footer");
    }
// TotlaShow Page /
    public function vendorTotlaShow($cid = '', $type = '')
    {
        isFrontLogin();
        if (!empty($cid)) {
            $data['cid'] = $cid;
            $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
            if (!empty($data['c_details'])) {
                if ($data['c_details'][0]->FormType == '1099-Misc') {
                    $data['v_details'] = $this->Frontend_model->getData('ia_1099_misc', $cid, 'FileSequence');
                } else {
                    $data['v_details'] = $this->Frontend_model->getData('ia_w2', $cid, 'FileSequence');

                }
                //echo "<PRE>"; print_r($data);die;
                $this->load->view("include/front/header");
                $this->load->view('totlaShow', $data);
                $this->load->view("include/front/footer");
            }

        } else {
            redirect(base_url("blaze"), 'refresh');
        }
    }

    // filemanager Page /
    public function filemanager()
    {
        isFrontLogin();
        $CustID = $this->session->userdata('user_details')[0]->CustID; //die;
        $sqlUser = "SELECT * FROM `users` WHERE CustID= '$CustID'";
        $data['user_d'] = $this->Frontend_model->getQrResult($sqlUser);

        $sql = "SELECT * FROM `Files` WHERE (`is_deleted` = '0' OR `is_deleted` = '' OR `is_deleted` = NULL ) AND CustID= '$CustID' ORDER BY `Files`.`FileSequence` ASC ";
        $data['v_details'] = $this->Frontend_model->getQrResult($sql);
        $this->load->view("include/front/header");
        $this->load->view('filemanager', $data);
        $this->load->view("include/front/footer");
    }

    // viewBulk Page /
    public function viewBulk()
    {
        isFrontLogin();
        $CustID = $this->session->userdata('user_details')[0]->CustID; //die;
        $sqlUser = "SELECT * FROM `users` WHERE CustID= '$CustID'";
        $data['user_d'] = $this->Frontend_model->getQrResult($sqlUser);
        $sql = "SELECT * FROM `Files` WHERE `is_deleted` = 0 AND CustID= '$CustID' ORDER BY `Files`.`FileSequence` ASC ";

        $data['cid'] = $this->Frontend_model->getQrResult($sql);
        if (isset($data['cid']) && !empty($data['cid'])) {
            foreach ($data['cid'] as $keyV => $dataV) {
                if (!empty($dataV->FileSequence)) {
                    $data['c_details'][$dataV->FileSequence] = $this->Frontend_model->getData('Files', $dataV->FileSequence, 'FileSequence')[0];
                    if ($dataV->FormType == '1099-Misc') {
                        $data['v_details'][$dataV->FileSequence] = $this->Frontend_model->getData('ia_1099_misc', $dataV->FileSequence, 'FileSequence');
                    } else {
                        $data['v_details'][$dataV->FileSequence] = $this->Frontend_model->getData('ia_w2', $dataV->FileSequence, 'FileSequence');

                    }
                }

            }
        }

        $this->load->view("include/front/header");
        $this->load->view('viewBulk', $data);
        $this->load->view("include/front/footer");
    }
    /* DELETE FILEMANGER PERMANET SOFT DELET*/
    public function deletefiles($cid = '', $type = '')
    {
        $dcid = array();
        isFrontLogin();
        if (!empty($cid)) {
            $sql = "SELECT * FROM `Files` WHERE FileSequence= '$cid' ";
            $dcid = $this->Frontend_model->getQrResult($sql);
            //echo "<PRE>";print_r($dcid);die;
            if ($this->Frontend_model->updateRow('Files', 'FileSequence', $cid, array('is_deleted' => 1))) {
                if (isset($dcid[0]->FormType) && $dcid[0]->FormType == 'W-2') {
                    $this->Frontend_model->updateRow('ia_w2', 'FileSequence', $cid, array('is_deleted' => 1));
                } else {
                    $this->Frontend_model->updateRow('ia_1099_misc', 'FileSequence', $cid, array('is_deleted' => 1));
                }
                redirect(base_url("filemanager"), 'refresh');} else {redirect(base_url("filemanager"), 'refresh');}

        } else {
            redirect(base_url("blaze"), 'refresh');
        }
    }
    public function DuplicateMySQLRecord($cid = '')
    {
        isFrontLogin();
        if (!empty($cid)) {
            $table = 'Files';
            $this->db->where('FileSequence', $cid);
            $query = $this->db->get($table);
            foreach ($query->result() as $row) {
                foreach ($row as $key => $val) {
                    if ($key != 'FileSequence') {

                        $this->db->set($key, $val);
                    } //endif
                } //endforeach
            } //endforeach
            $this->db->insert($table);
            $insert_id = $this->db->insert_id();
            $current_year = date("Y") - 1;
            $data = array(
                'TaxYear' => $current_year,
                'eFiled' => 0,
                'status' => 0,
            );
            $this->db->where('FileSequence', $insert_id);
            $this->db->update($table, $data);
            // echo $insert_id;die;
            redirect(base_url('filemanager'), 'refresh');

        } else {
            redirect(base_url('filemanager'), 'refresh');
        }
    }
    public function DuplicateComapanyById($cid = '', $FormType = '')
    {
        if (!empty($cid)) {
            $table = 'Files';
            $this->db->where('FileSequence', $cid);
            $query = $this->db->get($table);
            $type = $query->row();
            foreach ($query->result() as $row) {
                foreach ($row as $key => $val) {
                    if ($key != 'FileSequence') {
                        $this->db->set($key, $val);
                    } //endif
                } //endforeach
            } //endforeach
            $this->db->insert($table);
            $insert_id = $this->db->insert_id();
            $current_year = date("Y") - 1;
            $data = array(
                'TaxYear' => $current_year,
                'eFiled' => 0,
                'status' => 0,
            );
            if (isset($FormType) && !empty($FormType)) {$data['FormType'] = $FormType;}
            $this->db->where('FileSequence', $insert_id);
            $this->db->update($table, $data);
            if (isset($FormType) && !empty($FormType)) {
                return array('FileSequence' => $insert_id, 'FormType' => $FormType);
            } else {
                return array('FileSequence' => $insert_id, 'FormType' => $type->FormType);
            }

        }
        //return false;
    }

    public function bringfileforworded($cid = '')
    {
        isFrontLogin();

        if (!empty($cid)) {

            $ddddd = $this->DuplicateComapanyById($cid);
            $c_idN = isset($ddddd['FileSequence']) ? $ddddd['FileSequence'] : '';
            //print_r($c_idN);die;
            //echo"<PRE>";print_r($ddddd);die($c_idN);
            if (isset($ddddd['FormType']) && !empty($c_idN) && trim($ddddd['FormType']) == 'W-2') {
//W2

                $table = 'ia_w2';
                $this->db->select('w2_id');
                $this->db->where('FileSequence', $cid);
                $query = $this->db->get($table);
                $result12 = $query->result();
                if (isset($result12) && !empty($result12)) {
                    foreach ($result12 as $row => $DataK) {

                        $w2_id = $DataK->w2_id;
                        $this->db->where('FileSequence', $cid);
                        $this->db->where('w2_id', $w2_id);
                        $query1 = $this->db->get($table);
                        foreach ($query1->result() as $row) {
                            foreach ($row as $key => $val) {
                                if ($key != 'w2_id') {
                                    $this->db->set($key, $val);
                                } //endif
                            } //endforeach
                        } //endforeach
                        $this->db->insert($table);
                        $insert_id12 = $this->db->insert_id();
                        $data12 = array('FileSequence' => $c_idN);
                        $this->db->where('w2_id', $insert_id12);
                        $this->db->update($table, $data12);

                    }

                }
                //W2
            } else {
                //ia_1099_misc

                $table = 'ia_1099_misc';
                $this->db->select('misc_id');
                $this->db->where('FileSequence', $cid);
                $query = $this->db->get($table);
                $result = $query->result();
                if (isset($result) && !empty($result)) {

                    foreach ($result as $row => $DataK) {

                        $misc_id = $DataK->misc_id;

                        $this->db->where('FileSequence', $cid);
                        $this->db->where('misc_id', $misc_id);
                        $query1 = $this->db->get($table);
                        //print_r($query1->result());die;
                        foreach ($query1->result() as $row) {
                            foreach ($row as $key => $val) {
                                if ($key != 'misc_id') {
                                    $this->db->set($key, $val);
                                } //endif
                            } //endforeach
                        } //endforeach
                        $this->db->insert($table);
                        $insert_id1 = $this->db->insert_id();
                        //print_r($insert_id1);die;
                        $data1 = array(
                            'FileSequence' => $c_idN,
                        );
                        //print_r($data1);die;
                        $this->db->where('misc_id', $insert_id1);
                        $this->db->update($table, $data1);
                        //echo"<BR>$cid<PRE>";
                        //print_r($ddddd);
                        //print_r($result);
                    }

                    //die($c_idN);

                }

            } //ia_1099_misc

            redirect(base_url('filemanager'), 'refresh');

        } else {

            redirect(base_url('filemanager'), 'refresh');

        }
    }
     public function checkout1099($id = NULL){
        $id = $this->input->post('id');
        $cid = explode('_', $id);
        $datetime = date('m_d_y_H_m_s');
        //$date = explode("-", $time);
        //$data1 =  $data);
        $filevalue = $this->input->post('filevalue');
        if($filevalue == '4.49'){
            $filetype = 'PM';
        } else {
            $filetype = 'E';
        }
        // $num = 1;
        // foreach ($cid as $value) {

        //     $client_s_n = sprintf("%'.05d\n", $num);
        //     $num++;
        // } 
        //$client_s_n = sprintf("%'.05d\n", $num);
        // print_r($this->input->post('filevalue')); 
        // echo '<br>';
        // print_r($prefix.' '.$client_s_n.' '.'1099-MISC'.' '.$datetime.'.xls');
        //die;
        // $num = '0';
        // $client_s_n = $num++;
        // $filename = $prefix.' '.$client_s_n.' '.'1099-MISC'.' '.$datetime.'.xls';
        //print_r($filename); 
        $d = array(
                                   'Paid' => 1,
                                   'eFiled' => 1,
                                   'status' => 1,
                               );
       
                               foreach ($cid as $value) {
                                // echo "<pre>";
                                $data = $this->Frontend_model->getData('ia_1099_misc', $value, 'FileSequence');
                                 $payee = array(
                                   'Filetype' => $filetype,
                               );
                                foreach ($data as $k => $v) {
                                    // print_r($v->misc_id);
                                    // echo "<br>";
                                    $this->Frontend_model->updateRow('ia_1099_misc', 'misc_id', $v->misc_id, $payee);
                                }
                                $this->Frontend_model->updateRow('Files', 'FileSequence', $value, $d);
                               }
                               //die;
                               redirect(base_url('blaze'), 'refresh');
    }
    public function paymentForm()
    {
        $this->load->view("include/front/header");
        $this->load->view('product_form');
        $this->load->view("include/front/footer");
    }

    public function check()
    {
        isFrontLogin();
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        $data['u_details'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
        // echo '<pre>';
        // print_r($data['u_details'][0]->name);die;
        if (!empty($_POST['stripeToken'])) {
            //get token, card and user info from the form
            $token = $_POST['stripeToken'];
            $id = $_POST['items'];
            $name = $data['u_details'][0]->name;
            $email = $data['u_details'][0]->UEmail;
            $card_num = $_POST['card_num'];
            $card_cvc = $_POST['cvc'];
            $card_exp_month = $_POST['exp_month'];
            $card_exp_year = $_POST['exp_year'];
            //str_replace("_", ",", $id);
            $cid = explode('_', $id);
            //print_r($cid);die;
            //include Stripe PHP library
            require_once APPPATH . "third_party/stripe/init.php";

            //set api key
            $stripe = array(
                "secret_key" => "sk_test_bLCs9mcAMyFIttRxTmjtH7w9",
                "publishable_key" => "pk_test_C2dxnmOB8Wf60Jzd6vOb0wWe",
            );

            \Stripe\Stripe::setApiKey($stripe['secret_key']);

            //add customer to stripe
            $customer = \Stripe\Customer::create(array(
                'email' => $email,
                'source' => $token,
            ));

            //item information
            $itemName = "Stripe Donation";
            $itemNumber = "PS123456";
            $itemPrice = 50;
            $currency = "usd";
            $orderID = "SKA92712382139";

            //charge a credit or a debit card
            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount' => $itemPrice,
                'currency' => $currency,
                'description' => $itemNumber,
                'metadata' => array(
                    'item_id' => $itemNumber,
                ),
            ));

            //retrieve charge details
            $chargeJson = $charge->jsonSerialize();

            //check whether the charge is successful
            if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {
                //order details
                $amount = $chargeJson['amount'];
                $balance_transaction = $chargeJson['balance_transaction'];
                $currency = $chargeJson['currency'];
                $status = $chargeJson['status'];
                $date = date("Y-m-d H:i:s");

                //insert tansaction data into the database
                $dataDB = array(
                    'name' => $name,
                    'email' => $email,
                    'card_num' => $card_num,
                    'card_cvc' => $card_cvc,
                    'card_exp_month' => $card_exp_month,
                    'card_exp_year' => $card_exp_year,
                    'item_name' => $itemName,
                    'item_number' => $itemNumber,
                    'item_price' => $itemPrice,
                    'item_price_currency' => $currency,
                    'paid_amount' => $amount,
                    'paid_amount_currency' => $currency,
                    'txn_id' => $balance_transaction,
                    'payment_status' => $status,
                    'created' => $date,
                    'modified' => $date,
                );

                // print_r($dataDB);die;
                if ($this->db->insert('orders', $dataDB)) {
                    if ($this->db->insert_id() && $status == 'succeeded') {
                        //$cid = $this->input->get('id');
                        $d = array(
                            'Paid' => 1,
                            'eFiled' => 1,
                            'status' => 1,
                        );
                        foreach ($cid as $value) {
                            $this->Frontend_model->updateRow('Files', 'FileSequence', $value, $d);
                        }
                        $data['insertID'] = $this->db->insert_id();
                        $this->load->view("include/front/header");
                        $this->load->view('payment_success', $data);
                        $this->load->view("include/front/footer");
                        //redirect('Welcome/payment_success', 'refresh');
                    } else {
                        echo "Transaction has been failed";
                    }
                } else {
                    echo "not inserted. Transaction has been failed";
                }

            } else {
                echo "Invalid Token";
                $statusMsg = "";
            }
        }
    }

    public function payment_success()
    {
        $this->load->view("include/front/header");
        $this->load->view('payment_success');
        $this->load->view("include/front/footer");
    }

    public function payment_error()
    {
        $this->load->view("include/front/header");
        $this->load->view('payment_error');
        $this->load->view("include/front/footer");
    }

    public function printView($cid = '')
    {
        isFrontLogin();
        if (!empty($cid)) {
            $data['cid'] = $cid;
            $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
            if (!empty($data['c_details'])) {
                if ($data['c_details'][0]->FormType == '1099-Misc') {
                    $data['v_details'] = $this->Frontend_model->getData('ia_1099_misc', $cid, 'FileSequence');
                } else if ($data['c_details'][0]->FormType == 'W-9s') {
                    $data['v_details'] = $this->Frontend_model->getData('ia_1099_misc', $cid, 'FileSequence');
                } else {
                    $data['v_details'] = $this->Frontend_model->getData('ia_w2', $cid, 'FileSequence');

                }
                //echo "<PRE>"; print_r($data);die;
                $this->load->view("include/front/header");
                $this->load->view('printview', $data);
                $this->load->view("include/front/footer");
            }

        } else {
            redirect(base_url("blaze"), 'refresh');
        }
    }
    public function printFile()
    {
        isFrontLogin();
        $data = array();
        $Pid = explode(',', $this->input->get('Pid'));
        $Cid = $this->input->get('Cid');

        foreach ($Pid as $obj) {
            $CustID = $this->session->userdata('user_details')[0]->CustID;
            $data['ssnprint'] = $this->input->get('ssnprint');
            $data['copyprint'] = $this->input->get('copyprint');
            $data['u_details'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
            $data['c_details'] = $this->Frontend_model->getData('Files', $Cid, 'FileSequence');
            $data['v_details'][] = $this->Frontend_model->getData('ia_1099_misc', $obj, 'misc_id');
            // print_r($data['v_details']);die;
        }

        $html = $this->load->view('printfile', $data, true);

        $pdfName = $data['copyprint'] . strtotime("now") . ".pdf";
        $this->load->library('pdfgenerator');
        $output = $this->pdfgenerator->generate($html, 'pdfnew');

        // If you want to save file
        // file_put_contents("printpdf/" . $pdfName . ".pdf", $output);
        // $filename = "printpdf/".$pdfName.".pdf";

        $content = $output;
        header('Content-type: application/force-download');
        header('Content-Length: ' . strlen($content));
        header('Content-disposition: inline; filename="' . $pdfName . '"');
        header('Cache-Control: public, must-revalidate, max-age=0');
        header('Pragma: public');
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        echo $content;

    }

    public function filecorrection()
    {
        $this->load->view('include/front/header');
        $this->load->view('filecorrections');
        $this->load->view('include/front/footer');
    }
    public function filepastyear()
    {
        $this->load->view('include/front/header');
        $this->load->view('filepastyear');
        $this->load->view('include/front/footer');
    }
    public function tinmatching()
    {
        $this->load->view('include/front/header');
        $this->load->view('tinmatching');
        $this->load->view('include/front/footer');
    }

    public function companyW9s($id = '')
    {
        isFrontLogin();
        if (isset($_POST['w9s']) && !empty($_POST['w9s']) && $_POST['w9s'] == 'submit') {
            $this->add_w9s();
        }
        $this->load->view("include/front/header");
        $data['c_details'] = array();
        if (!empty($id)) {
            $data['c_details'] = $this->Frontend_model->getData('Files', $id, 'FileSequence');
            if (empty($data['c_details'])) {
                $this->load->view('404error');
            } else {
                $this->load->view('companyW9s', $data);
            }

        } else {
            $this->load->view('companyW9s', $data);
        }

        $this->load->view("include/front/footer");
    }

    public function add_w9s()
    {
        //echo "<PRE>";print_r($_POST);die;
        $data = $this->input->post();
        $CustID = $this->session->userdata('user_details')[0]->CustID;
        if (!empty($data['c_id'])) {
            $d = array(

                "c_name" => $data['c_name'],
                "c_tin" => $data['c_tin'],
                "c_street" => $data['c_street'],
                "c_city" => $data['c_city'],
                "c_state" => !empty($data['c_state']) ? $data['c_state'] : '',
                "c_zip" => $data['c_zip'],
                "c_email" => $data['c_email'],
                "c_telephone" => $data['c_telephone'],
                "FileName" => $data['c_name'],
                "chDate" => date('Y-m-d'),
                "c_contact" => $data['c_telephone'],
                "c_title" => $data['c_name'],
            );
            $this->Frontend_model->updateRow('Files', 'FileSequence', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            $d = array(
                "c_name" => $data['c_name'],
                "c_tin" => $data['c_tin'],
                "c_street" => $data['c_street'],
                "c_city" => $data['c_city'],
                "c_state" => !empty($data['c_state']) ? $data['c_state'] : '',
                "c_zip" => $data['c_zip'],
                "c_email" => $data['c_email'],
                "c_telephone" => $data['c_telephone'],
                "FileName" => $data['c_name'],
                "chDate" => date('Y-m-d'),
                "c_contact" => $data['c_telephone'],
                "c_title" => $data['c_name'],
                "c_ssn_ein" => $data['c_tin'],
                "c_employee_ein" => $data['c_tin'],
                "FormType" => 'W-9s',
                "is_deleted" => 0,
                "CustID" => $CustID,
                "eFiled" => 0,
                "status" => 'Waiting-UnPaid',
                //"TaxYear" =>  date('Y'),
                "created_at" => date('Y-m-d h:i:s'),
            );
            $id = $this->Frontend_model->insertFiler('Files', $d);
            redirect(base_url('blaze'), 'refresh');
        }
    }

    public function requestw9s($cid, $id = '')
    {

        isFrontLogin();
        $id = $this->uri->segment(3);

        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');

        if ($this->input->post()) {
            $data['post_data'] = $this->input->post();
            $data['amount'] = 1.49 * count($data['post_data']['v_name']);
            $this->load->view("include/front/header");
            $this->load->view('payment', $data);
            $this->load->view("include/front/footer");
        } else {
            $this->load->view("include/front/header");
            if (empty($data['c_details'])) {
                $this->load->view('404error');
            } else {
                $this->load->view('requestw9s', $data);
            }

            $this->load->view("include/front/footer");
        }

    }

    /**
     * This function is used to add and update Vendor
     * @return Void
     */

    public function addEditVendorw9s()
    {
        $data = $this->input->post();
        $c_id = $data['c_id'];
        $setting = settings();
        $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'FileSequence');
        $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
        $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
        $c_telephone = !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '';
        $sub = 'W-9 Request from (' . $c_name . ' W-9)';
        $i = 0;
        foreach ($data['v_email'] as $key => $value) {
            if (!empty($data['v_email'][$i])) {
                $form_id = md5(uniqid(rand(), true));
                $d = array(
                    "v_name" => $data['v_name'][$i],
                    "v_email" => $data['v_email'][$i],
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "c_id" => $c_id,
                    "user_id" => $this->session->userdata('user_details')[0]->CustID,
                    "CustID" => $this->session->userdata('user_details')[0]->CustID,
                    "form_id" => $form_id,
                    "created_at" => date('Y-m-d h:i:s'),
                );
                $email = $data['v_email'][$i];
                $setting['company_name'] = 'W-9s.com';
                $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $data['v_name'][$i] . ',</p><p>' . $c_name . ' is requesting IRS form W-9 from you. Completing this form helps enter you into their accounting system for faster payment.  Most times, companies will not issue payment until this form is returned. </p><p>' . $c_name . ' has teamed up with <a rel="nofollow" style="text-decoration:none; color:#333">W-9\'s.com</a> to help simplify the process for you. Just click on this link and it will take you to our secure site to enter your info, e-sign, save and you\'re all set! </p><p><a href="' . base_url('enterw9s') . '/' . $form_id . '" class="go">Fill out your W-9</a></p><p>If you\'re not sure why you are receiving this request please contact<br><br>' . $c_name . '<br><a href="tel:' . $c_telephone . '">' . $c_telephone . '</a><br><br>Thank you<br>W-9s.com, LLC<br><a href="tel:616.325.9332">616.325.9332</a></p></div></div></div>';
                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
// content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                $this->Frontend_model->insertRow('ia_vendors', $d);
            }
            $i++;
        }
        redirect(base_url('vendorw9s') . '/' . $c_id, 'refresh');
    }

    /* Vendor Page */
    public function vendorw9s($cid = '')
    {
        isFrontLogin();
        $data['cid'] = $cid;
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
        if (!empty($data['c_details'])) {
            $data['v_details'] = $this->Frontend_model->getData('ia_vendors', $cid, 'c_id');
            $this->load->view("include/front/header");
            $this->load->view('vendorw9s', $data);
            $this->load->view("include/front/footer");
        } else {
            $this->load->view("include/front/header");
            $this->load->view('404error');
            $this->load->view("include/front/footer");
        }
    }

    /* W-9 Fprm Page */
    public function enterw9s($form_id = '')
    {
        if (!empty($form_id)) {
            $data['vendors'] = $this->Frontend_model->getDataBy('ia_vendors', $form_id, 'form_id');
            $data['c_id'] = $form_id;
            $this->load->view('w9s-form', $data);
        } else {
            redirect(base_url('login'), 'refresh');
        }
    }

    public function copyFormTo($cid = '', $formtype = '')
    {

        isFrontLogin();
        //echo"<PRE>";
        if (!empty($cid) && !empty($formtype)) {
            $ddddd = $this->DuplicateComapanyById($cid, $formtype);
            $c_idN = isset($ddddd['FileSequence']) ? $ddddd['FileSequence'] : '';
            if (isset($ddddd['FormType']) && !empty($c_idN) && trim($ddddd['FormType']) == '1099-Misc') {
                $table = 'ia_vendors';
                $this->db->select('v_id');
                $this->db->where('c_id', $cid);
                $query = $this->db->get('ia_vendors');
                $result = $query->result();
                if (isset($result) && !empty($result)) {
                    foreach ($result as $row => $DataK) {
                        $v_id = $DataK->v_id;
                        $this->db->where('c_id', $cid);
                        $this->db->where('v_id', $v_id);
                        $query1 = $this->db->get('ia_vendors');
                        $dataINS = array();
                        foreach ($query1->result() as $row) {
                            $dataINS['recepient_id_no'] = isset($row->v_ein) ? $row->v_ein : '';
                            $dataINS['void'] = 0;
                            $dataINS['corrected'] = 0;
                            $dataINS['misc_delete'] = 0;

                            $dataINS['first_name'] = isset($row->v_name) ? $row->v_name : '';
                            $dataINS['dba'] = isset($row->business_name) ? $row->business_name : '';

                            $dataINS['address'] = isset($row->address) ? $row->address : '';
                            $dataINS['city'] = isset($row->city) ? $row->city : '';
                            $dataINS['state'] = isset($row->state) ? $row->state : '';
                            $dataINS['zipcode'] = isset($row->zip) ? $row->zip : '';
                            $dataINS['account_number'] = isset($row->v_account) ? $row->v_account : '';
                            $dataINS['foreign_add'] = isset($row->foreign_address) ? $row->foreign_address : '';
                            $dataINS['CustID'] = isset($row->CustID) ? $row->CustID : '';
                            $dataINS['export_status'] = 'unexpoted';
                            $dataINS['is_deleted'] = isset($row->is_deleted) ? $row->is_deleted : '0';
                            $dataINS['created_at'] = date('Y-m-d h:i:s');
                        } //endforeach
                        $this->db->insert('ia_1099_misc', $dataINS);
                        $insert_id1 = $this->db->insert_id();
                        $data1 = array('FileSequence' => $c_idN);
                        $this->db->where('misc_id', $insert_id1);
                        $this->db->update('ia_1099_misc', $data1);
                    }
                }
            } //ia_1099_misc

            if (isset($ddddd['FormType']) && !empty($c_idN) && trim($ddddd['FormType']) == 'W-9s') {

                $this->db->select('misc_id');
                $this->db->where('FileSequence', $cid);
                $query = $this->db->get('ia_1099_misc');
                $result = $query->result();
                if (isset($result) && !empty($result)) {
                    foreach ($result as $row => $DataK) {
                        $misc_id = $DataK->misc_id;
                        $this->db->where('FileSequence', $cid);
                        $this->db->where('misc_id', $misc_id);
                        $query1 = $this->db->get('ia_1099_misc');
                        $dataINS = array();
                        foreach ($query1->result() as $row) {
                            $dataINS['v_name'] = isset($row->first_name) ? $row->first_name : isset($row->dba) ? $row->dba : '';
                            $dataINS['v_email'] = null;
                            $dataINS['v_account'] = isset($row->account_number) ? $row->account_number : '';
                            //$dataINS['v_status']     = isset($row->account_number)?$row->account_number:'';
                            $dataINS['is_deleted'] = isset($row->is_deleted) ? $row->is_deleted : '0';
                            $dataINS['CustID'] = isset($row->CustID) ? $row->CustID : '';
                            $dataINS['user_id'] = isset($row->CustID) ? $row->CustID : '';

                            //$dataINS['form_id']     = isset($row->account_number)?$row->account_number:'';
                            $dataINS['business_name'] = isset($row->dba) ? $row->dba : '';
                            $dataINS['business_classification'] = isset($row->zipcode) ? $row->zipcode : '';
                            //$dataINS['exempt_payee_code']         = isset($row->zipcode)?$row->zipcode:'';
                            //$dataINS['exempt_fatca_code']         = isset($row->zipcode)?$row->zipcode:'';
                            $dataINS['address'] = isset($row->address) ? $row->address : '';
                            $dataINS['city'] = isset($row->city) ? $row->city : '';
                            $dataINS['state'] = isset($row->state) ? $row->state : '';
                            $dataINS['zip'] = isset($row->zipcode) ? $row->zipcode : '';
                            $dataINS['foreign_address'] = isset($row->foreign_add) ? $row->foreign_add : '';
                            $dataINS['v_ssn'] = isset($row->recepient_id_no) ? $row->recepient_id_no : '';
                            $dataINS['v_ein'] = isset($row->recepient_id_no) ? $row->recepient_id_no : '';
                            $dataINS['other'] = isset($row->account_number) ? $row->account_number : '';
                            //$dataINS['no_backup_withholding']         = isset($row->zipcode)?$row->zipcode:'';
                            //$dataINS['signature']     = isset($row->zipcode)?$row->zipcode:'';
                            //$dataINS['pdf']         = isset($row->zipcode)?$row->zipcode:'';
                            $dataINS['created_at'] = date('Y-m-d h:i:s');
                        } //endforeach
                        $this->db->insert('ia_vendors', $dataINS);
                        $insert_id1 = $this->db->insert_id();
                        $data1 = array('c_id' => $c_idN);
                        $this->db->where('v_id', $insert_id1);
                        $this->db->update('ia_vendors', $data1);
                    }
                }

            } //ia_vendors

            redirect(base_url('blaze'), 'refresh');

        } else {

            redirect(base_url('blaze'), 'refresh');

        }
    }
}
