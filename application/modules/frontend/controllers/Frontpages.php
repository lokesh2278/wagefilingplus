<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Frontpages extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Frontend_model');
        $this->lang->load('user', getSetting('language'));
        $this->load->library('encryption');
        $this->load->helper('csv');
    }
/* Home Page */
    /*public function index()
    {
    $this->load->view("include/front/header");
    $this->load->view('howitworks');
    $this->load->view("include/front/footer");
    }

    public function howitworks()
    {
    $this->load->view("include/front/header");
    $this->load->view('howitworks');
    $this->load->view("include/front/footer");
    }

    public function contactUs()
    {
    $this->load->view("include/front/header");
    $this->load->view('contactUs');
    $this->load->view("include/front/footer");
    }

    public function pricing()
    {
    $this->load->view("include/front/header");
    $this->load->view('pricing');
    $this->load->view("include/front/footer");
    }*/

    public function send_mail()
    {
        //print_r($this->input->post('message')); die;
        $this->load->library("send_mail");
        $from = '';

        $to_email = 'support@w-9s.com';
        $subject = 'Mail From Website';
        $message = $this->input->post('message');
        $headers = "From: " . $this->input->post('email');
        $emm = mail($to_email, $subject, $message, $headers);
        if ($emm) {
            $this->session->set_flashdata('msg', 'Mail Send Successfully');
            /*$data['msg'] = 'Mail Send Successfully.';
        $data['class'] = 'alert-success';*/
        } else {
            $this->session->set_flashdata('msg', 'Please try again.');
            //$data['msg'] = 'Please try again.';
            //$data['class'] = 'alert-danger';
        }
        //echo $url = base_url().'contact-us';
        redirect($url);
    }
}
