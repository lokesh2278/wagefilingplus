<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Frontend extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Frontend_model');
        $this->lang->load('user', getSetting('language'));
        $this->load->library('encryption');
        $this->load->helper('csv');
        $this->load->helper('string');
        $this->CustID = isset($this->session->userdata('user_details')[0]->CustID) ? $this->session->userdata('user_details')[0]->CustID : '';
    }
    /* Home Page */
    public function index()
    {
        if (isset($_SESSION['user_details'])) {
            $CustID = $this->session->userdata('user_details')[0]->CustID;
            $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
            if (count($c) > 0) {
                redirect(base_url('user_home'), 'refresh');
            } else {

                redirect(base_url('user_home'), 'refresh');
            }
        } else {
            $this->load->view("include/front/header");
            $this->load->view('index');
            $this->load->view("include/front/footer");
            //redirect(base_url('user_home'), 'refresh');
        }

        //redirect('login');
    }
    /* Signup Page */
    public function signup()
    {
        //die('lokesh');
        if (isset($_SESSION['user_details'])) {
            $CustID = $this->session->userdata('user_details')[0]->CustID;
            $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
            if (count($c) > 0) {
                redirect(base_url('blaze'), 'refresh');
            } else {
                redirect(base_url('company'), 'refresh');
            }
        }
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->addEdit();
        } else {
            $this->load->view('signup');
        }
        $this->load->view("include/front/footer");
    }
/* Login Page */
    public function login()
    {
        if ($this->input->post()) {
            $return = $this->Frontend_model->authUser();
            if (empty($return)) {
                $art_msg['msg'] = lang('invalid_details');
                $art_msg['type'] = 'warning';
                $this->session->set_userdata('alert_msg', $art_msg);
                redirect(base_url('login'), 'refresh');
            } else {
                if ($return == 'not_varified') {
                    $art_msg['msg'] = lang('this_account_is_not_verified_please_contact_to_your_admin');
                    $art_msg['type'] = 'danger';
                    $this->session->set_userdata('alert_msg', $art_msg);
                    redirect(base_url('login'), 'refresh');
                } else {
                    $this->session->set_userdata('user_details', $return);
                    $this->session->set_userdata('admin_back', '');
                }
                $CustID = $this->session->userdata('user_details')[0]->CustID;
                $c = $this->Frontend_model->getData('Files', $CustID, 'CustID');
                if (count($c) > 0) {
                    redirect(base_url('user_home'), 'refresh');
                } else {
                    redirect(base_url('user_home'), 'refresh');
                }
            }
        } else {
            $this->load->view("include/front/header");
            $this->load->view('login');
            $this->load->view("include/front/footer");
        }
    }
/**
 * This function is used to logout user
 * @return Void
 */
    public function logout()
    {
        isFrontLogin();
        $this->session->unset_userdata('user_details');
        $this->session->unset_userdata('admin_back');
        redirect(base_url('login'), 'refresh');
    }
/* Signup Page */
    public function company($id = '')
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->add_filer();
        } else {
            if (!empty($id)) {
                $data['c_details'] = $this->Frontend_model->getData('Files', $id, 'FileSequence');
                if (empty($data['c_details'])) {
                    $this->load->view('404error');
                } else {
                    $this->load->view('company_filer', $data);
                }

            } else {
                $data['c_details'] = array();
                $this->load->view('company', $data);
            }

        }
        $this->load->view("include/front/footer");
    }

    public function employee_setup($id = '')
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->emp_setup();
        } else {
            if (!empty($id)) {
                $data['c_details'] = $this->Frontend_model->getData('Files', $id, 'FileSequence');
                if (empty($data['c_details'])) {
                    $this->load->view('404error');
                } else {
                    $this->load->view('employer_setup', $data);
                }

            } else {
                $data['c_details'] = array();
                $this->load->view('employer_setup', $data);
            }

        }
        $this->load->view("include/front/footer");
    }
    public function company_filer()
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        $this->load->view('company_filer');
        $this->load->view("include/front/footer");
    }
    public function companydiv1099()
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        $this->load->view('1099_div');
        $this->load->view("include/front/footer");
    }
    public function compdiv1099($id = '')
    {
        isFrontLogin();
        $this->load->view("include/front/header");
        if ($this->input->post()) {
            $this->add1099div();
        } else {
            if (!empty($id)) {
                $data['c_details'] = $this->Frontend_model->getData('Files', $id, 'FileSequence');
                if (empty($data['c_details'])) {
                    $this->load->view('404error');
                } else {
                    $this->load->view('1099_div', $data);
                }

            } else {
                $data['c_details'] = array();
                $this->load->view('1099_div', $data);
            }

        }
        $this->load->view("include/front/footer");

    }
    public function add1099div()
    {
        // die('lokesh');
        $data = $this->input->post();
        if (!empty($data['c_id'])) {
            $d = array(
                "c_ssn_ein" => $data['ssn_ein'],
                "TaxYear" => $data['tax_year'],
                //"c_name" => $data['c_name'],
                "FileName" => $data['c_name'],
                "chDate" => date('Y-m-d'),
                "c_is_foreign" => $data['is_foregin'],
                "c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_file_to_states" => $data['file_to_states'],
                "c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_fax" => $data['fax'],
            );
            $this->Frontend_model->updateRow('Files', 'FileSequence', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            $d = array(
                "c_ssn_ein" => $data['ssn_ein'],
                "TaxYear" => $data['tax_year'],
                "c_name" => $data['c_name'],
                "FileName" => $data['c_name'],
                "c_is_foreign" => $data['is_foregin'],
                "c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_file_to_states" => $data['file_to_states'],
                "c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_fax" => $data['fax'],
                "FormType" => $data['form_type'],
                "is_deleted" => 0,
                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                "eFiled" => 0,
                "status" => 'Waiting-UnPaid',
                "created_at" => date('Y-m-d h:i:s'),
            );
            // print_r($d);die;
            $id = $this->Frontend_model->insertFiler('Files', $d);
            redirect(base_url('blaze'), 'refresh');
        }
    }

    public function add_filer()
    {
        // die('lokesh');
        $data = $this->input->post();
        if (!empty($data['c_id'])) {
            $d = array(
                "c_ssn_ein" => $data['ssn_ein'],
                "TaxYear" => $data['tax_year'],
                //"c_name" => $data['c_name'],
                "FileName" => $data['c_name'],
                "chDate" => date('Y-m-d'),
                "c_is_foreign" => $data['is_foregin'],
                "c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_file_to_states" => $data['file_to_states'],
                "c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_fax" => $data['fax'],
            );
            $this->Frontend_model->updateRow('Files', 'FileSequence', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            $d = array(
                "c_ssn_ein" => $data['ssn_ein'],
                "TaxYear" => $data['tax_year'],
                "c_name" => $data['c_name'],
                "FileName" => $data['c_name'],
                "c_is_foreign" => $data['is_foregin'],
                "c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_file_to_states" => $data['file_to_states'],
                "c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_fax" => $data['fax'],
                "FormType" => $data['form_type'],
                "is_deleted" => 0,
                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                "eFiled" => 0,
                "status" => 'Waiting-UnPaid',
                "created_at" => date('Y-m-d h:i:s'),
            );
            // print_r($d);die;
            $id = $this->Frontend_model->insertFiler('Files', $d);
            redirect(base_url('blaze'), 'refresh');
        }
    }

    public function emp_setup()
    {
        //die('lokesh');
        $data = $this->input->post();
        //print_r($data);die;
        if (!empty($data['c_id'])) {
            $d = array(
                "TaxYear" => $data['tax_year'],
                "c_you_have" => $data['you_have'],
                //"c_ein_for" => $data['ein_for'],
                "c_employee_ein" => $data['employee_ein'],
                "c_other_ein" => $data['other_ein'],
                "c_est_number" => $data['est_number'],
                "FileName" => $data['company'],
                "chDate" => date('Y-m-d'),
                "c_name" => $data['company'],
                //1 "c_is_foreign" => $data['is_foregin'],
                //"c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_zip_2" => $data['zip_2'],
                //"c_file_to_states" => $data['file_to_states'],
                //"c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_extention" => $data['extention'],
                "c_fax" => $data['fax'],
                //"c_code" => $data['code'],
                "c_foreign_state" => $data['foreign_state'],
                "c_foreign_zip" => $data['foreign_zip'],
                "c_foreign_country_code" => $data['foreign_country_code'],
                "c_state_code" => $data['state_code'],
                "c_state_id_no" => $data['state_id_no'],
                //"third_party_sick_pay" => $data['third_party_sick_pay'],
                //"terminating_business" => $data['terminating_business'],
                "w3_box_13" => $data['w3_box_13'],
                "w3_box_14" => $data['w3_box_14'],
            );
            $d['c_ein_for'] = isset($data['ein_for']) ? $data['ein_for'] : 'NULL';
            $d['c_code'] = isset($data['code']) ? $data['code'] : 'NULL';
            $d['third_party_sick_pay'] = isset($data['third_party_sick_pay']) ? $data['third_party_sick_pay'] : 'NULL';
            $d['terminating_business'] = isset($data['terminating_business']) ? $data['terminating_business'] : 'NULL';
            //print_r($d['c_code']);die;
            $this->Frontend_model->updateRow('Files', 'FileSequence', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            // print_r($data);die;
            $d = array(
                "TaxYear" => $data['tax_year'],
                "c_you_have" => $data['you_have'],
                //"c_ein_for" => $data['ein_for'],
                "c_employee_ein" => $data['employee_ein'],
                "c_other_ein" => $data['other_ein'],
                "c_est_number" => $data['est_number'],
                "FileName" => $data['company'],
                "c_name" => $data['name'],
                //1 "c_is_foreign" => $data['is_foregin'],
                //"c_filer_name_2" => $data['filer_name_2'],
                "c_address" => $data['address'],
                "c_address_2" => $data['address_2'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_zip_2" => $data['zip_2'],
                //"c_file_to_states" => $data['file_to_states'],
                //"c_contact" => $data['contact'],
                "c_title" => $data['title'],
                "c_email" => $data['email'],
                "c_telephone" => $data['phone'],
                "c_extention" => $data['extention'],
                "c_fax" => $data['fax'],
                //"c_code" => $data['code'],
                "c_foreign_state" => $data['foreign_state'],
                "c_foreign_zip" => $data['foreign_zip'],
                "c_foreign_country_code" => $data['foreign_country_code'],
                "c_state_code" => $data['state_code'],
                "c_state_id_no" => $data['state_id_no'],
                //"third_party_sick_pay" => $data['third_party_sick_pay'],
                //"terminating_business" => $data['terminating_business'],
                "w3_box_13" => $data['w3_box_13'],
                "w3_box_14" => $data['w3_box_14'],
                "FormType" => $data['form_type'],
                "is_deleted" => 0,
                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                "eFiled" => 0,
                "status" => 'Waiting-UnPaid',
                "created_at" => date('Y-m-d h:i:s'),
            );
            $d['c_ein_for'] = isset($data['ein_for']) ? $data['ein_for'] : 'NULL';
            $d['c_code'] = isset($data['code']) ? $data['code'] : 'NULL';
            $d['third_party_sick_pay'] = isset($data['third_party_sick_pay']) ? $data['third_party_sick_pay'] : 'NULL';
            $d['terminating_business'] = isset($data['terminating_business']) ? $data['terminating_business'] : 'NULL';

            $id = $this->Frontend_model->insertSetup('Files', $d);
            redirect(base_url('blaze'), 'refresh');
        }
    }

    public function payment()
    {
        $com_details = $this->Frontend_model->getData('Files', $this->input->post('c_id'), 'FileSequence');
        $qr = "SELECT * FROM `users` WHERE `CustID` = '" . $com_details[0]->CustID . "'";
        $user_details = $this->Frontend_model->getQrResult($qr);
        $art_msg['msg'] = 'the request sent successfully';
        $art_msg['type'] = 'success';
        $this->session->set_userdata('alert_msg', $art_msg);
        if (isset($com_details[0]->FormType) && $com_details[0]->FormType == 'W-9s') {
            $this->addEditVendorw9s();
        } else {
            $this->addEditVendor();
        }

    }
// Vendor Page /
    public function vendor($cid = '')
    {
        isFrontLogin();
        $data['cid'] = $cid;
        // print_r($data);die;
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
        //print_r($data['c_details'][0]->FormType);die;
        //$data['u_details'] = $this->Frontend_model->getDataBy('ia_users', $cid, 'CustID');
        if (!empty($data['c_details'])) {
            if ($data['c_details'][0]->FormType == '1099-Misc') {
                $data['v_details'] = $this->Frontend_model->getData('ia_1099_misc', $cid, 'FileSequence');
                $data['d_details'] = $this->Frontend_model->getData('ia_1099_div', $cid, 'FileSequence');
            } else if ($data['c_details'][0]->FormType == '1099-Div') {

                $data['v_details'] = $this->Frontend_model->getData('ia_1099_misc', $cid, 'FileSequence');
                $data['d_details'] = $this->Frontend_model->getData('ia_1099_div', $cid, 'FileSequence');
            } else if ($data['c_details'][0]->FormType == 'W-9s') {
                $data['v_details'] = $this->Frontend_model->getData('ia_vendors', $cid, 'c_id');
            } else {
                $data['form_type'] = 'W-2';
                $data['v_details'] = $this->Frontend_model->getData('ia_w2', $cid, 'FileSequence');

            }
            // print_r($data);die;
            $this->load->view("include/front/header");
            if ($data['c_details'][0]->FormType == 'W-9s') {
                $this->load->view('vendorw9s', $data);
            } else {
                // echo '<pre>';
                // print_r($data);die;
                $this->load->view('vendor', $data);
            }
            $this->load->view("include/front/footer");
        } else {
            $this->load->view("include/front/header");
            $this->load->view('404error');
            $this->load->view("include/front/footer");
        }
    }
/* Company List Page */
    public function blaze()
    {
        isFrontLogin();
        $CustID = $this->CustID;
        $data['companies'] = $this->Frontend_model->getData('Files', $CustID, 'CustID');
        $data['u_details'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
        $this->load->view("include/front/header");
        $this->load->view('blaze', $data);
        $this->load->view("include/front/footer");
    }
/* Assigned List  */
    public function assigned()
    {
        isFrontLogin();
        $data = $this->input->post();
        $CustID = $this->CustID;
        $assigned = $this->Frontend_model->getData('ia_vendors', $data['id'], 'FileSequence');
        $vendors = $this->Frontend_model->getData('ia_vendors');
        $value = '';
        if (!empty($vendors)) {
            $i = 0;
            foreach ($vendors as $key => $vendor) {
                $value .= '<label for="c8_editor_ids-' . $i . '">
    <input type="checkbox" name="editor_ids" value="' . $vendor->v_id . '" id="c8_editor_ids-' . $i . '"> &nbsp&nbsp' . $vendor->v_name . ' &lt;' . $vendor->v_email . '&gt'
                    . '</label> <br/>';
            }
        }
        echo $value;
    }
/* W-9 Fprm Page */
    public function enter($form_id = '')
    {
        if (!empty($form_id)) {
            $data['vendors'] = $this->Frontend_model->getDataBy('ia_vendors', $form_id, 'form_id');
            $data['c_id'] = $form_id;
            $this->load->view('w9-form', $data);
        } else {
            redirect(base_url('login'), 'refresh');
        }
    }
/**
 * This function is used to add and update Company
 * @return Void
 */
    public function addEditCompany()
    {
        // die('sharma');
        $data = $this->input->post();
        if (!empty($data['c_id'])) {
            $d = array(
                "c_name" => $data['c_name'],
                "c_tin" => $data['tin'],
                "c_street" => $data['shipping_address'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_email" => $data['email'],
                "c_telephone" => $data['telephone'],
            );
            $this->Frontend_model->updateRow('Files', 'FileSequence', $data['c_id'], $d);
            redirect(base_url('blaze'), 'refresh');
        } else {
            $d = array(
                "c_name" => $data['c_name'],
                "c_tin" => $data['tin'],
                "c_street" => $data['shipping_address'],
                "c_city" => $data['city'],
                "c_state" => $data['state'],
                "c_zip" => $data['zip'],
                "c_email" => $data['email'],
                "c_telephone" => $data['telephone'],
                "is_deleted" => 0,
                "CustID" => $this->CustID,
                "created_at" => date('Y-m-d h:i:s'),
            );
            $id = $this->Frontend_model->insertRow('Files', $d);
            redirect(base_url('request') . '/' . $id, 'refresh');
        }
    }

/**
 * This function is used to add and update Vendor
 * @return Void
 */
    public function addEditVendor()
    {
        $data = $this->input->post();
        $c_id = $data['c_id'];
        $setting = settings();
        $c_details = $this->Frontend_model->getDataBy('Files', $c_id, 'FileSequence');
        $c_email = !empty($c_details[0]->c_email) ? $c_details[0]->c_email : '';
        $c_name = !empty($c_details[0]->c_name) ? $c_details[0]->c_name : '';
        $c_telephone = !empty($c_details[0]->c_telephone) ? $c_details[0]->c_telephone : '';
        $sub = 'W 9s Request from (' . $c_name . ' W 9s)';
        $i = 0;
        foreach ($data['v_email'] as $key => $value) {
            if (!empty($data['v_email'][$i])) {
                $form_id = md5(uniqid(rand(), true));
                $d = array(
                    "v_name" => $data['v_name'][$i],
                    "v_email" => $data['v_email'][$i],
//"v_account" => $data['account_number'][$i],
                    "v_status" => 'Requested',
                    "is_deleted" => 0,
                    "FileSequence" => $c_id,
                    "CustID" => $this->CustID,
                    "user_id" => $this->CustID,
                    "form_id" => $form_id,
                    "created_at" => date('Y-m-d h:i:s'),
                );
                $email = $data['v_email'][$i];
                $setting['FileName'] = 'WageFilingPlus.com';
                $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Hello ' . $data['v_name'][$i] . ',</p><p>' . $c_name . ' is requesting IRS form 1099-Misc from you. Completing this form helps enter you into their accounting system for faster payment.  Most times, companies will not issue payment until this form is returned. </p><p>' . $c_name . ' has teamed up with <a rel="nofollow" style="text-decoration:none; color:#333">WageFilingPlus.com</a> to help simplify the process for you. Just click on this link and it will take you to our secure site to enter your info, e-sign, save and you\'re all set! </p><p><a href="' . base_url('enterw9s') . '/' . $form_id . '" class="go">Fill out your 1099-Misc/W-2</a></p><p>If you\'re not sure why you are receiving this request please contact<br><br>' . $c_name . '<br><a href="tel:' . $c_telephone . '">' . $c_telephone . '</a><br><br>Thank you<br>WageFiling Plus, LLC<br><a href="tel:616.325.9332">616.325.9332</a></p></div></div></div>';
                if ($setting['mail_setting'] == 'php_mailer') {
                    $this->load->library("send_mail");
                    $emm = $this->send_mail->email($sub, $body, $email, $setting);
                } else {
// content-type is required when sending HTML email
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: ' . $setting['EMAIL'] . "\r\n";
                    $emm = mail($email, $sub, $body, $headers);
                }
                $this->Frontend_model->insertRow('ia_vendors', $d);
            }
            $i++;
        }
        redirect(base_url('vendorw9s') . '/' . $c_id, 'refresh');
    }
    /**
     * This function is used to add and update users
     * @return Void
     */
    public function addEdit()
    {
        $data = $this->input->post();
        $setting = settings();
        $checkValue = $this->Frontend_model->checkExists('users', 'UEmail', $this->input->post('email'), '', '');
        if ($checkValue) {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $var_key = md5($data['email']);

            $d = array(
                "CustID" => mt_rand(10000, 99999),
                "var_key" => $var_key,
                "name" => $data['full_name'],
                "Upassword" => $password,
                "visible_password" => $data['password'],
                "status" => 'deactive',
                "UEmail" => $data['email'],
                "Ucompany" => $data['company'],
                "Ucontact" => $data['full_name'],
                // "Uaddr => $data['address'],
                // "Utitle" => $data['title'],
                // "Ucity" => $data['city'],
                // "Ustate" => $data['state'],
                // "Uzip" => $data['zip'],
                "Uphone" => $data['phone_number'],
                // "Ufax" => $data['fax'],
                "user_type" => 'user',
                "validate" => 0,
                "is_deleted" => 0,
                "UDate" => date('Y-m-d'),
            );

            $email = $data['email'];
            $setting['FileName'] = 'WageFilingPlus';
            $sub = 'Activate Account';
            $body = '<div id="message" class="html"><div class="wrap"><div class="header"><div class="container"></div></div><div class="container"><p>Thank you for registering as user on WageFilingPlus.com </p><p>To activate your account, Please <a style="text-decoration: underline; color: blue;" href="' . base_url() . 'verify/' . $email . '/' . $var_key . '">Click Here</a></p>
                <p>If you\'re not sure why you are receiving this request, please contact us asap!<br><br></a>
                <br>
                Thank you,<br><br>
                WageFilingPlus, LLC<br>
                <a href="tel:616.325.9332">616.325.9332</a><br>
                support@WageFilingPlus.com<br>
                www.WageFilingPlus.com</p>
                </div>
                </div>
                </div>';
            if ($setting['mail_setting'] == 'php_mailer') {
                //die('Lokesh');
                // echo "<pre>";
                // print_r($setting);die;
                $this->load->library("send_mail");
                $emm = $this->send_mail->email($sub, $body, $email, $setting);
                //die();
            } else {
                //die('Sharma');
                // content-type is required when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: WegeFiling.com' . "\r\n";
                $emm = mail($email, $sub, $body, $headers);
            }
            //die('Sharma 123');
            $id = $this->Frontend_model->insertRow('users', $d);
            // $new_data = $this->Frontend_model->getDataBy('ia_users', $id, 'CustID');
            // if (!isset($this->session->userdata('user_details')[0]->CustID)) {
            //     $this->session->set_userdata('user_details', $new_data);
            // }
            $this->session->set_flashdata('item', array('message' => 'Success'));
            redirect(base_url('login'));
        } else {
            $art_msg['msg'] = lang('this_email_already_registered_with_us');
            $art_msg['type'] = 'info';
            $this->session->set_userdata('alert_msg', $art_msg);
            redirect(base_url('signup'), 'refresh');
        }
    }
    public function verify($email, $var_key)
    {
        if ($this->Frontend_model->verifyEmail($var_key, $email)) {
            $this->session->set_flashdata('active', array('message' => 'Congratulations'));
            redirect(base_url('login'), 'refresh');
        } else {
            echo 'error' . $this->config->item('admin_email');
        }
    }

    public function user_home()
    {
        isFrontLogin();
        $CustID = $this->CustID;
        $data['users'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
        $this->load->view("include/front/header");
        $this->load->view('user_home', $data);
        $this->load->view("include/front/footer");
    }
    public function edituser()
    {
        isFrontLogin();
        $CustID = $this->CustID;
        $data['users'] = $this->Frontend_model->getDataBy('users', $CustID, 'CustID');
        $this->load->view("include/front/header");
        $this->load->view('user_edit', $data);
        $this->load->view("include/front/footer");
    }
    public function edituserdata()
    {
        //die('lokesh');
        isFrontLogin();
        $data = $this->input->post();
        $CustID = $this->CustID;
        //$print_r($data['id']);die;
        $d = array(
            "name" => $data['full_name'],
            "status" => 'active',
            "UEmail" => $data['email'],
            "Ucompany" => $data['company'],
            // "contact" => $data['contact'],
            // "address" => $data['address'],
            // "city" => $data['city'],
            // "title" => $data['title'],
            // "state" => $data['state'],
            // "zip" => $data['zip'],
            "UPhone" => $data['phone_number'],
            // "fax" => $data['fax'],
        );
        if (isset($data['password']) && !empty($data['password'])) {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $d['Upassword'] = $password;
            $d['visible_password'] = $this->input->post('password');
        }
        $this->Frontend_model->updateRow('users', 'CustID', $CustID, $d);
        redirect(base_url('user_home'), 'refresh');
    }
    /* Request Page */
    public function request($cid, $id = '')
    {

        isFrontLogin();
        $id = $this->uri->segment(3);
        //$id = $this->input->post('misc_id');
        //print_r($id);die;
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
        //print_r($data['c_details']);die;
        $data['v_details'] = $this->Frontend_model->get1099('ia_1099_misc', $id, 'misc_id');

        $this->load->view("include/front/header");
        if (empty($data['c_details'])) {
            $this->load->view('404error');
        } else {
            $this->load->view('request', $data);
        }

        $this->load->view("include/front/footer");
    }

    public function div1099Req($cid, $id = '')
    {

        isFrontLogin();
        $id = $this->uri->segment(3);
        //die($id);
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->Frontend_model->get1099div('ia_1099_div', $id, 'div_id');
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // die;
        $this->load->view("include/front/header");
        $this->load->view('1099_div_request', $data);
        $this->load->view("include/front/footer");
    }

    public function addvendor()
    {

        isFrontLogin();
        $data = $this->input->post();
        if (!empty($data['misc_id'])) {

            // print_r($data['corrected']);die;
            if (isset($data['misc_delete']) && $data['misc_delete']) {
                $this->Frontend_model->deleterow('ia_1099_misc', 'misc_id', $data['misc_id']);
                redirect(base_url('vendor/' . $data['c_id']), 'refresh');

            } else {
                if (!isset($data['foreign_add'])) {
                    $data['foreign_add'] = 0;
                }
                $d = array(

                    "recepient_id_no" => $data['recepient_id_no'],
                    "first_name" => $data['first_name'],
                    //"last_name" => $data['last_name'],
                    "dba" => $data['dba'],
                    //"street" => $data['street'],
                    "city" => $data['city'],
                    "state" => $data['state'],
                    "zipcode" => $data['zipcode'],
                    "address" => $data['address'],
                    "rents" => $data['rents'],
                    "royalties" => $data['royalties'],
                    "other_income" => $data['other_income'],
                    "fed_income_with_held" => $data['fed_income_with_held'],
                    "fishing_boat_proceed" => $data['fishing_boat_proceed'],
                    "mad_helthcare_pmts" => $data['mad_helthcare_pmts'],
                    "non_emp_compansation" => $data['non_emp_compansation'],
                    "pmts_in_lieu" => $data['pmts_in_lieu'],
                    "crop_Insurance_proceeds" => $data['crop_Insurance_proceeds'],
                    "excess_golden_par_pmts" => $data['excess_golden_par_pmts'],
                    "gross_paid_to_an_attomey" => $data['gross_paid_to_an_attomey'],
                    "sec_409a_deferrals" => $data['sec_409a_deferrals'],
                    "sec_409a_Income" => $data['sec_409a_Income'],
                    "state_tax_with_held" => $data['state_tax_with_held'],
                    "payer_state_no" => $data['payer_state_no'],
                    "state_Income" => $data['state_Income'],
                    "account_number" => $data['account_number'],
                    "foreign_add" => $data['foreign_add'],

                );

                $d['void'] = isset($data['void']) ? $data['void'] : 'NULL';
                $d['corrected'] = isset($data['corrected']) ? $data['corrected'] : 'NULL';
                $d['misc_delete'] = isset($data['misc_delete']) ? $data['misc_delete'] : 'NULL';
                $d['consumer_products_for_resale'] = isset($data['consumer_products_for_resale']) ? $data['consumer_products_for_resale'] : 'NULL';

                $this->Frontend_model->updateRow('ia_1099_misc', 'misc_id', $data['misc_id'], $d);
                if (isset($_POST['saveform'])) {
                    redirect(base_url('vendor/' . $data['c_id']), 'refresh');
                } else {
                    redirect(base_url('request/' . $data['c_id']), 'refresh');
                }
            }
        } else {
            //die('lokesh');
            if (!isset($data['foreign_add'])) {
                $data['foreign_add'] = 0;
            }
            $d = array(

                "recepient_id_no" => $data['recepient_id_no'],
                "first_name" => $data['first_name'],
                // "last_name" => $data['last_name'],
                "dba" => $data['dba'],
                //"street" => $data['street'],
                "city" => $data['city'],
                "state" => $data['state'],
                "zipcode" => $data['zipcode'],
                "address" => $data['address'],
                "rents" => $data['rents'],
                "royalties" => $data['royalties'],
                "other_income" => $data['other_income'],
                "fed_income_with_held" => $data['fed_income_with_held'],
                "fishing_boat_proceed" => $data['fishing_boat_proceed'],
                "mad_helthcare_pmts" => $data['mad_helthcare_pmts'],
                "non_emp_compansation" => $data['non_emp_compansation'],
                "pmts_in_lieu" => $data['pmts_in_lieu'],
                "crop_Insurance_proceeds" => $data['crop_Insurance_proceeds'],
                "excess_golden_par_pmts" => $data['excess_golden_par_pmts'],
                "gross_paid_to_an_attomey" => $data['gross_paid_to_an_attomey'],
                "sec_409a_deferrals" => $data['sec_409a_deferrals'],
                "sec_409a_Income" => $data['sec_409a_Income'],
                "state_tax_with_held" => $data['state_tax_with_held'],
                "payer_state_no" => $data['payer_state_no'],
                "state_Income" => $data['state_Income'],
                "account_number" => $data['account_number'],
                "FileSequence" => $data['c_id'],
                "foreign_add" => $data['foreign_add'],
                "CustID" => $this->CustID,
                "FormType" => '1099-Misc',
                "export_status" => 'unexpoted',
                "created_at" => date('Y-m-d h:i:s'),
            );
            $d['void'] = isset($data['void']) ? $data['void'] : 'NULL';
            $d['corrected'] = isset($data['corrected']) ? $data['corrected'] : 'NULL';
            $d['misc_delete'] = isset($data['misc_delete']) ? $data['misc_delete'] : 'NULL';
            $d['consumer_products_for_resale'] = isset($data['consumer_products_for_resale']) ? $data['consumer_products_for_resale'] : 'NULL';
            $id = $this->Frontend_model->insertRow('ia_1099_misc', $d);
            if (isset($_POST['saveform'])) {
                redirect(base_url('vendor/' . $data['c_id']), 'refresh');
            } else {
                redirect(base_url('request/' . $data['c_id']), 'refresh');
            }
        }
    }

    public function w2form($cid, $id = '')
    {
        // $data[''] = $this->Frontend_model->getDataBy('ia_users', $use, 'CustID');
        isFrontLogin();
        $id = $this->uri->segment(3);
        //print_r($id);die;
        $data['c_details'] = $this->Frontend_model->getData('Files', $cid, 'FileSequence');
        $data['v_details'] = $this->Frontend_model->getw2('ia_w2', $id, 'w2_id');
        $this->load->view("include/front/header");
        if (empty($data['c_details'])) {
            $this->load->view('404error');
        } else {
            $this->load->view('w2form', $data);
        }
        $this->load->view("include/front/footer");
    }

    public function addformw2()
    {
        //die('Lokesh');
        isFrontLogin();
        $data = $this->input->post();

        if (!empty($data['w2_id'])) {
            $d = array(
                'SSN' => $data['SSN'],
                // 'calcbox'=>$data['calcbox'],
                // 'voidbox' =>$data['voidbox'],
                // 'deletebox'=>$data['deletebox'],
                'box1' => $data['box1'],
                'box2' => $data['box2'],
                'box3' => $data['box3'],
                'box4' => $data['box4'],
                'box5' => $data['box5'],
                'box6' => $data['box6'],
                'box7' => $data['box7'],
                'box8' => $data['box8'],
                'box9' => $data['box9'],
                'box10' => $data['box10'],
                'box11' => $data['box11'],
                'box12A' => $data['box12A'],
                'box12AM' => $data['box12AM'],
                'box12B' => $data['box12B'],
                'box12Bm' => $data['box12BM'],
                'box12C' => $data['box12C'],
                'box12CM' => $data['box12CM'],
                'box12D' => $data['box12D'],
                'box12DM' => $data['box12DM'],
                'boxA' => $data['boxA'],
                'box12DM' => $data['box12DM'],
                'box12DM' => $data['box12DM'],
                'box15A' => $data['box15A'],
                'box15B' => $data['box15B'],
                'box14A' => $data['box14A'],
                'box14B' => $data['box14B'],
                'box14C' => $data['box14C'],
                'box14D' => $data['box14D'],
                'box16' => $data['box16'],
                'box17' => $data['box17'],
                'box18' => $data['box18'],
                'box19' => $data['box19'],
                'box20' => $data['box20'],
                'box21A' => $data['box21A'],
                'box21B' => $data['box21B'],
                'box22' => $data['box22'],
                'box23' => $data['box23'],
                'box24' => $data['box24'],
                'box25' => $data['box25'],
                'box26' => $data['box26'],
                'FName' => $data['FName'],
                'MName' => $data['MName'],
                'LName' => $data['LName'],
                'Suf' => $data['Suf'],
                'Addr1' => $data['Addr1'],
                'Addr2' => $data['Addr2'],
                'City' => $data['City'],
                'State' => $data['State'],
                'Zip1' => $data['Zip1'],
                'Zip2' => $data['Zip2'],
                'FState' => $data['FState'],
                'FZip' => $data['FZip'],
                'FCode' => $data['FCode'],
                "is_deleted" => 1,
            );
            // $d['calcbox'] = isset($data['calcbox']) ? $data['calcbox'] : 'NULL';
            $d['voidbox'] = isset($data['voidbox']) ? $data['voidbox'] : 'NULL';
            $d['is_deleted'] = isset($data['deletebox']) ? $data['deletebox'] : 'NULL';
            $srt = isset($data['statutory_retirement_thirdparty']) ? $data['statutory_retirement_thirdparty'] : 'NULL';
            if ($srt == 1) {
                $d['statutory'] = 1;
                $d['retirement'] = 0;
                $d['thirdparty'] = 0;
            } elseif ($srt == 2) {
                $d['statutory'] = 0;
                $d['retirement'] = 2;
                $d['thirdparty'] = 0;
            } elseif ($srt == 3) {
                $d['statutory'] = 0;
                $d['retirement'] = 0;
                $d['thirdparty'] = 3;
            }
            $this->Frontend_model->updateRow('ia_w2', 'w2_id', $data['w2_id'], $d);
            if (isset($_POST['save'])) {
                redirect(base_url('vendor/' . $data['c_id']), 'refresh');
            } else {
                redirect(base_url('w2form/' . $data['c_id']), 'refresh');
            }

        } else {

            $d = array(
                'SSN' => $data['SSN'],
                // 'calcbox'=>$data['calcbox'],
                // 'voidbox' =>$data['voidbox'],
                // 'deletebox'=>$data['deletebox'],
                'box1' => $data['box1'],
                'box2' => $data['box2'],
                'box3' => $data['box3'],
                'box4' => $data['box4'],
                'box5' => $data['box5'],
                'box6' => $data['box6'],
                'box7' => $data['box7'],
                'box8' => $data['box8'],
                'box9' => $data['box9'],
                'box10' => $data['box10'],
                'box11' => $data['box11'],
                'box12A' => $data['box12A'],
                'box12AM' => $data['box12AM'],
                'box12B' => $data['box12B'],
                'box12Bm' => $data['box12BM'],
                'box12C' => $data['box12C'],
                'box12CM' => $data['box12CM'],
                'box12D' => $data['box12D'],
                'box12DM' => $data['box12DM'],
                'boxA' => $data['boxA'],
                'box12DM' => $data['box12DM'],
                'box12DM' => $data['box12DM'],
                'box15A' => $data['box15A'],
                'box15B' => $data['box15B'],
                'box14A' => $data['box14A'],
                'box14B' => $data['box14B'],
                'box14C' => $data['box14C'],
                'box14D' => $data['box14D'],
                'box16' => $data['box16'],
                'box17' => $data['box17'],
                'box18' => $data['box18'],
                'box19' => $data['box19'],
                'box20' => $data['box20'],
                'box21A' => $data['box21A'],
                'box21B' => $data['box21B'],
                'box22' => $data['box22'],
                'box23' => $data['box23'],
                'box24' => $data['box24'],
                'box25' => $data['box25'],
                'box26' => $data['box26'],
                'FName' => $data['FName'],
                'MName' => $data['MName'],
                'LName' => $data['LName'],
                'Suf' => $data['Suf'],
                'Addr1' => $data['Addr1'],
                'Addr2' => $data['Addr2'],
                'City' => $data['City'],
                'State' => $data['State'],
                'Zip1' => $data['Zip1'],
                'Zip2' => $data['Zip2'],
                'FState' => $data['FState'],
                'FZip' => $data['FZip'],
                'FCode' => $data['FCode'],
                "CustID" => $this->session->userdata('user_details')[0]->CustID,
                "FileSequence" => $data['c_id'],
                "is_deleted" => 0,
                //"created_at" => date('Y-m-d h:i:s'),
            );
            // $d['calcbox'] = isset($data['calcbox']) ? $data['calcbox'] : 'NULL';
            $d['voidbox'] = isset($data['voidbox']) ? $data['voidbox'] : 'NULL';
            $d['is_deleted'] = isset($data['deletebox']) ? $data['deletebox'] : 'NULL';
            $srt = isset($data['statutory_retirement_thirdparty']) ? $data['statutory_retirement_thirdparty'] : 'NULL';
            if ($srt == 1) {
                $d['statutory'] = 1;
                $d['retirement'] = 0;
                $d['thirdparty'] = 0;
            } elseif ($srt == 2) {
                $d['statutory'] = 0;
                $d['retirement'] = 2;
                $d['thirdparty'] = 0;
            } elseif ($srt == 3) {
                $d['statutory'] = 0;
                $d['retirement'] = 0;
                $d['thirdparty'] = 3;
            }
            $this->Frontend_model->insertRow('ia_w2', $d);
            if (isset($_POST['save'])) {
                redirect(base_url('vendor/' . $data['c_id']), 'refresh');
            } else {
                redirect(base_url('w2form/' . $data['c_id']), 'refresh');
            }

        }
    }

    public function addform1099div()
    {
        $data = $this->input->post();
        if (!empty($data['div_id'])) {
            $d = array(
                'recepient_id_no' => $data['recepient_id_no'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'address' => $data['address'],
                'address2' => $data['address2'],
                'city' => $data['city'],
                'state' => $data['state'],
                'zipcode' => $data['zipcode'],
                'account_number' => $data['account_number'],
                'box1a' => $data['box1a'],
                'box1b' => $data['box1b'],
                'box2a' => $data['box2a'],
                'box2b' => $data['box2b'],
                'box2c' => $data['box2c'],
                'box2d' => $data['box2d'],
                'box3' => $data['box3'],
                'box4' => $data['box4'],
                'box5' => $data['box5'],
                'box6' => $data['box6'],
                'box7' => $data['box7'],
                'box8' => $data['box8'],
                'box9' => $data['box9'],
                'box10' => $data['box10'],
                'box11' => $data['box11'],
                'box12' => $data['box12'],
                'box13' => $data['box13'],
                'box14' => $data['box14'],
                "FormType" => $data['FormType'],
            );
            // $d['calcbox'] = isset($data['calcbox']) ? $data['calcbox'] : 'NULL';
            $d['void'] = isset($data['void']) ? $data['void'] : 'NULL';
            $d['corrected'] = isset($data['corrected']) ? $data['corrected'] : 'NULL';
            $d['fatca_filing_requirement'] = isset($data['fatca_filing_requirement']) ? $data['fatca_filing_requirement'] : 'NULL';
            $this->Frontend_model->updateRow('ia_1099_div', 'div_id', $data['div_id'], $d);
    