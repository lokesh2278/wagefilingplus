<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|    example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|    https://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There are three reserved routes:

|

|    $route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|    $route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router which controller/method to use if those

| provided in the URL cannot be matched to a valid route.

|

|    $route['translate_uri_dashes'] = FALSE;

|

| This is not exactly a route, but allows you to automatically route

| controller and method names that contain dashes. '-' isn't a valid

| class or method name character, so it requires translation.

| When you set this option to TRUE, it will replace ALL dashes in the

| controller and method URI segments.

|

| Examples:    my-controller/index    -> my_controller/index

|        my-controller/my-method    -> my_controller/my_method

 */

$route['copyFormTo/(:any)/(:any)'] = 'frontend/copyFormTo/$1/$1';

$route['default_controller'] = 'frontend';

$route['signup'] = 'frontend/signup';

$route['user_home'] = 'frontend/user_home';

$route['useredit'] = 'frontend/edituser';

$route['edituserdata'] = 'frontend/edituserdata';

$route['company'] = 'frontend/company';

$route['company/add_filer/(:any)'] = 'frontend/add_filer/$1';

$route['company/(:any)'] = 'frontend/company/$1';

$route['companyW9s'] = 'frontend/companyW9s';

$route['companyW9s/(:any)'] = 'frontend/companyW9s/$1';

$route['login'] = 'frontend/login';

$route['blaze'] = 'frontend/blaze';

$route['logout'] = 'frontend/logout';

$route['fillform'] = 'frontend/saveData';

$route['sendPdf'] = 'frontend/sendPdf';

$route['assigned'] = 'frontend/assigned';

//Narendra

//$route['w2form'] = 'frontend/w2form';

$route['w2form/addformw2'] = 'frontend/addformw2';

$route['w2form/(:any)/addformw2'] = 'frontend/addformw2';

$route['w2form/(:any)'] = 'frontend/w2form/$1';

$route['w2form/(:any)/(:any)'] = 'frontend/w2form/$1/$1';

//$route['w2form/addformw'] = 'frontend/addformw2';

//$route['w2form/addform'] = 'frontend/addvendor';

$route['request/addform'] = 'frontend/addvendor';

$route['request/(:any)/addform'] = 'frontend/addvendor';

$route['request/(:any)'] = 'frontend/request/$1';

$route['request/(:any)/(:any)'] = 'frontend/request/$1/$1';

//1099-Div form
$route['compdiv1099'] = 'frontend/compdiv1099';
$route['compdiv1099/(:any)'] = 'frontend/compdiv1099/$1';
$route['compdiv1099/add1099div/(:any)'] = 'frontend/add1099div/$1';

$route['div1099Req/addform1099div'] = 'frontend/addform1099div';
$route['div1099Req/(:any)/addform1099div'] = 'frontend/addform1099div';
$route['div1099Req/(:any)'] = 'frontend/div1099Req/$1';
$route['div1099Req/(:any)/(:any)'] = 'frontend/div1099Req/$1/$1';
//$route['requestw9s/addform'] = 'frontend/addvendor';

//$route['requestw9s/(:any)/addform'] = 'frontend/addvendor';

$route['requestw9s/(:any)'] = 'frontend/requestw9s/$1';

$route['requestw9s/(:any)/(:any)'] = 'frontend/requestw9s/$1/$1';

$route['how-it-works'] = 'frontend/howitworks';

$route['services'] = 'frontend/services';

$route['contact-us'] = 'frontend/contactus';

$route['payment'] = 'frontend/payment';

$route['enter/(:any)'] = 'frontend/enter/$1';

$route['enter/(:any)/(:any)'] = 'frontend/enter/$1/$1';

$route['enterw9s/(:any)'] = 'frontend/enterw9s/$1';

$route['enterw9s/(:any)/(:any)'] = 'frontend/enterw9s/$1/$1';

$route['vendor/(:any)'] = 'frontend/vendor/$1';

$route['vendorw9s/(:any)'] = 'frontend/vendorw9s/$1';

$route['edit/(:any)'] = 'frontend/edit/$1';

$route['editw9s/(:any)'] = 'frontend/editw9s/$1';

$route['move_to_1099_misc/(:any)'] = 'frontend/moveto1099_misc/$1';

$route['resendemail'] = 'frontend/resendemail';

$route['requestupdate'] = 'frontend/requestupdate';

$route['resendemailw9s'] = 'frontend/resendemailw9s';

$route['requestupdatew9s'] = 'frontend/requestupdatew9s';

$route['importvendordata'] = 'frontend/importvendordata';

$route['importdata'] = 'frontend/importdata';

$route['import'] = 'frontend/importCsv';

$route['delete'] = 'frontend/delete';

$route['download'] = 'frontend/download';

$route['downloadPdfs'] = 'frontend/downloadPdfs';

$route['vendordownload'] = 'frontend/vendordownload';

$route['forgotpassword'] = 'frontend/forgotpassword';

$route['dashboard'] = 'user/dashboard';

$route['profile'] = 'user/profile';

$route['terms'] = 'frontend/terms';

$route['userLogin/(:any)'] = 'frontend/userLogin/$1';

$route['adminBack/(:any)'] = 'frontend/adminBack/$1';

$route['404_override'] = '';

$route['translate_uri_dashes'] = false;

$route['company_filer'] = 'frontend/company_filer';

$route['employee_setup'] = 'frontend/employee_setup';

$route['emp_setup/employee_setup'] = 'frontend/employee_setup';

$route['emp_setup/(:any)'] = 'frontend/employee_setup/$1';

$route['verify/(:any)/(:any)'] = 'frontend/verify/$1/$1';

$route['dofiles'] = 'frontend/doFiles';

$route['dofiles/(:any)'] = 'frontend/doFiles/$1';

$route['siteseal'] = 'frontend/siteSeal';

$route['bringfileforworded/(:any)'] = 'frontend/bringfileforworded/$1';

$route['bringCompanyforworded/(:any)'] = 'frontend/DuplicateMySQLRecord/$1';

// Bulk Tin checking
$route['bulktincheckng'] = 'frontend/bulktincheckng';
$route['importbulktin'] = 'frontend/importbulktin';
$route['bulktinreq'] = 'user/bulktinreq';

$route['downloadBulkTinFile/(:any)'] = 'user/downloadBulkTinFile/$1';
// amin khan

$route['filemanager'] = 'frontend/filemanager';

$route['bringfileforworded/(:any)'] = 'frontend/bringfileforworded/$1';

$route['check'] = 'frontend/check';
$route['checkout1099'] = 'frontend/checkout1099';

$route['paymentForm'] = 'frontend/paymentForm';

$route['payment_success'] = 'frontend/payment_success';

$route['payment_error'] = 'frontend/payment_error';

$route['printview/(:any)'] = 'frontend/printView/$1';

$route['printfile'] = 'frontend/printFile';

$route['confirmation/(:any)'] = 'frontend/confirmation/$1';
$route['confirmation/(:any)/(:any)'] = 'frontend/confirmation/$1/$1';

$route['export1099CSV'] = 'user/export1099CSV';

$route['exportw2CSV'] = 'user/exportW2CSV';

$route['w2bydate'] = 'user/w2bydate';

$route['importExcel'] = 'user/importExcel';

$route['importUser'] = 'user/importUser';

$route['file-a-correction'] = 'frontend/filecorrection';

$route['filepastyear'] = 'frontend/filepastyear';
$route['tinmatching'] = 'frontend/tinmatching';
$route['issue-w9'] = 'frontend/issue_w9';

$route['more_info'] = 'frontend/more_info';
/*-------new pages ------------*/

$route['how_it_works'] = 'frontend/how_it_works';
$route['faq'] = 'frontend/faq';
$route['save-as-pdf'] = 'frontend/save_as_pdf';
$route['form1099misc'] = 'frontend/form1099misc';
$route['statefiling'] = 'frontend/statefiling';
$route['efile1099miscforms'] = 'frontend/efile1099miscforms';
$route['combinedfederalstatefiling'] = 'frontend/combinedfederalstatefiling';
$route['top_4_tax_mistakes'] = 'frontend/top_4_tax_mistakes';

$route['stripePost'] = "frontend/stripePost";
$route['checkCoupan'] = "frontend/checkCoupan";

